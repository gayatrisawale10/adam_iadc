<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
  public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['CardName','isActive'];

	public $table = "dbo.Card";
}
