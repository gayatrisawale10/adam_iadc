<?php
namespace App\Http\Controllers\sms;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;

use App\Product;
use App\customer_job; 
use App\work;
use App\sms\smsconfig;
use App\sms\sms_log;
use Illuminate\Support\Facades\Auth;
use App\user;
use \App\send_sms\send_sms;
use \App\send_sms\sms_trigger1;

class sms_logController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        
        $sms_log = sms_log::leftjoin('users','users.id','=','sms_logs.sent_by')
                                ->select('users.name','sms_logs.*')
                                ->paginate(10);    

        return view('sms.sms_log.index', compact('sms_log'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $smsconfig_id = 1;
        //$customer = Product::all();
        $customer= customer_job::orderBy('products.id','DESC')->join('products', 'products.id', '=', 'customer_job.cust_id')->join('work', 'work.work_id', '=', 'customer_job.work_id')->join('status', 'status.s_id', '=', 'customer_job.status_id')->get();;
	
        $select_Customer =0;
        $smsType = smsconfig::all();
       
       return view('sms.sms_log.create', compact('select_Customer','customer','smsType'));
      
        //return view('sms.sms_log.create', compact('customer','smsType'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {             
        //test start
        echo '<br/>call started <br/>';
        
        $FmyFunctions2 = new sms_trigger1;        
        $send_trigger = ($FmyFunctions2->trigger_sms());
        //dd($send_trigger);
        echo '<br/>test ended'; 
        exit();
        
        
        
        
        
        $smsType= $request->input('smsType');         
        $job_id= 1;
       
        //For second sms type
        $customer_name = "shailesh";
        $type_of_work = "CIDCO";
        $date_delivered_by = "09-11-2016";        
       
        //parameter list
        $param[0]= $customer_name;
        
//        $param[0]= $job_id;
//        $param[1]= $customer_name;
//        $param[2]= $type_of_work;
//        $param[4]= $date_delivered_by;     
               
        //mobile list
        $mobList =[11111111];   
       
        $FmyFunctions1 = new send_sms;        
        $send = ($FmyFunctions1->sys_send_sms($mobile,$message));
         
        dd($send);
        echo '<br/>test ended'; 
        exit();
        //test end
        
         return redirect('sms/sms_log');
        //return view('sms.sms_log.sms_msg')->with($sms_msg);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $sms_log = sms_log::findOrFail($id);

        return view('sms.sms_log.show', compact('sms_log'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $sms_log = sms_log::findOrFail($id);

        return view('sms.sms_log.edit', compact('sms_log'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $sms_log = sms_log::findOrFail($id);
        $sms_log->update($requestData);

        Session::flash('flash_message', 'sms_log updated!');

        return redirect('sms/sms_log');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        sms_log::destroy($id);

        Session::flash('flash_message', 'sms_log deleted!');

        return redirect('sms/sms_log');
    }
    
    public function sms_msg($sms_msg)
    {
        $sms_msg="<strong>Great!</strong> SMS Sent Successfully.<br><br>";
        return view('sms.sms_msg.show', compact('$sms_msg'));
    }
}
