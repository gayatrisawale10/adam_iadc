<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdamsLineItem extends Model
{
    public $timestamps = false;
    
    protected $table = 'dbo.AdamsLineItem';
}
