<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Auth::routes();
Route::group(['middleware' => ['guest', 'disablepreventback']], function()
{
    //  Route::get('/', function () {
    //     return view('underMaintainance');
    // });

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/login', function () {
        return view('welcome');
    });

    Route::get('/register', function () {
        return view('welcome');
    });

    //ForgotPassword
    Route::get('/forgotPassword', function () {
        return view('forgotPassword');
    });

    Route::post('emailToResetPswd','Forgot_PasswordController@forgotPassword');

    //Reset password
    Route::get('/resetPasswordForm', function () {
        return view('resetPassword');
    });
    Route:: post('resetPassword', 'Forgot_PasswordController@resetPassword');


});

Route::get('dcmRedirect', function () {
    return view('dcmRedirect');
});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('getErrorExcelFIle/{Camp_ID}' , 'CampaignPublishercontroller@getErrorExcelFIle');
Route::get('getErrorExcelFIleCount' , 'CampaignPublishercontroller@getErrorExcelFIleCount');




Route::get('getLocationSheet' , 'CampaignPublishercontroller@getLocationSheet');

Route::get('alwaysOnCampaignDetails/{adamUniqueKey}/{id}' , 'AdamSheetController@alwaysOnCampaignDetails');

// ------------------------------------------------------------------------------------------
Route::group(['middleware' =>['auth','user_role']], function()
{
    Route::get('home', 'HomeController@index');
    Route::resource('/Campaign','CampaignController');
   //   Route::resource('/Campaign', [ 'middleware' => ['user_role'], 'uses' =>'CampaignController']);



    //Person routes starts
    Route::get('Person', 'PersonController@index');
    Route::post('person', 'PersonController@savePerson')->name('savePerson');
    Route::post('editPerson', 'PersonController@editPerson')->name('editPerson');
    Route::get('person/delete/{id}', 'PersonController@deletePerson')->name('PersonDelete');
    //person routes ends
    //card route
    Route::get('Card', 'cardController@index');
    Route::post('Card', 'cardController@saveCard')->name('saveCard');
    Route::post('editCard', 'cardController@editCard')->name('editCard');
    Route::get('card/delete/{id}', 'cardController@deleteCard')->name('CardDelete');
    Route::post('addCreative', 'cardController@addCreative')->name('addCreative');




    // Route::post('person', 'PersonController@savePerson')->name('savePerson');
    // Route::post('editPerson', 'PersonController@editPerson')->name('editPerson');
    // Route::get('person/delete/{id}', 'PersonController@deletePerson')->name('PersonDelete');
    //end of card route
    Route::post('/selectBrandClient','CampaignController@selectBrand');
    //CRUD:Publisher
    Route::resource('/Publisher','PublisherController');

    //CRUD:Client
    Route::resource('/Client','ClientController');
    // get Client login credentials
    Route::get('/CredentialsClient/{client_id}', ['as' => 'Client.credentials','uses' =>'ClientController@getClientCredentials']);
     // update Client login credentials
    Route::post('/saveClientCredentials/{client_id}', ['as' => 'Client.saveClientCredentials','uses' =>'ClientController@saveClientCredentials']);
    // ---------------------------------------------------------------------------------------
    // get publisher login credentials
    Route::get('/Credentials/{p_id}', ['as' => 'Publisher.credentials','uses' =>'PublisherController@getCredentials']);
    // update publisher login credentials
    Route::post('/saveCredentials/{p_id}', ['as' => 'Publisher.saveCredentials','uses' =>'PublisherController@saveCredentials']);
    // ---------------------------------------------------------------------------------------

    //View: All Brand
    Route::get('/ClientBrand/{c_id}', ['as' => 'ClientBrand.getAllBrandsToDisplay',
								  'uses' =>'ClientBrandController@getAllBrandsToDisplay']);
    // ---------------------------------------------------------------------------------------
    //Create:  Brand
    Route::get('/ClientBrand/ClientBrand/{c_id}', [ 'as' => 'ClientBrand.createBrand',
     								  			    'uses' =>'ClientBrandController@createBrand']);
    //Store:  Brand
    Route::post('/ClientBrand/storeBrand', [ 'as' => 'ClientBrand.storeBrand',
 								  			    'uses' =>'ClientBrandController@storeBrand']);
    //edit:  Brand
    Route::get('/ClientBrand/editBrand/{b_id}/{c_id}', [ 'as' => 'ClientBrand.editBrand',
 								  			    'uses' =>'ClientBrandController@editBrand']);
    //update:  Brand
    Route::patch('/ClientBrand/updateBrand/{b_id}/{c_id}', [ 'as' => 'ClientBrand.updateBrand',
 								  			    'uses' =>'ClientBrandController@updateBrand']);
    //delete:  Brand
    Route::delete('/ClientBrand/destroyBrand/{b_id}/{c_id}', [ 'as' => 'ClientBrand.destroyBrand',
 								  			    'uses' =>'ClientBrandController@destroyBrand']);
    // ---------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------
    //Excel sheet
    Route::get('DownloadLineItemExcelSheet', 'CampaignPublishercontroller@getCampaignExcel')->name('downloadlineitemexcel');
    Route::get('getErrorExcelFIle' , 'CampaignPublishercontroller@getErrorExcelFIle');
    Route::post('UploadInputLineExcel/{id}/{cid}', 'CampaignPublishercontroller@ExcelSheetUpload')->name('UploadLineItemExcel');
    //end ExcelSheet
    // ---------------------------------------------------------------------------------------


    // Route::get('/api', function () {
    //     return view('api');
    // });

    Route::get('/api','AdamAPIController@apiView');


    // =============adamAPI=========================================
        
    Route::post('/fbAPI','AdamAPIController@fbAPIProcessing');

    Route::get('/dbmBasic', 'AdamAPIController@dbmBasic');
    Route::get('/dbmAPI', 'AdamAPIController@dbmAPI');
    Route::get('/dbmCron', 'AdamAPIController@dbmCron');
    Route::get('/oauth2CallBackDBM', 'AdamAPIController@oauth2CallBackDBM');
    
    Route::post('/adamDcm', 'AdamAPIController@adamDcm');
    Route::post('/adamAdwords','AdamAPIController@adamAdwords');

    Route::post('/testAdamSizMek', 'AdamAPIController@testAdamSizMek');
    //Route::get('/oauth2CallBackDBM', [ 'as' => 'oauth2CallBackDBM',
    //                                            'uses' =>'AdamAPIController@oauth2CallBackDBM']);

    Route:: post('/saveLineItemAdsetMapValue', 'AdamSheetController@saveLineItemAdsetMapValue');

    Route::get('/adamPublisherHome','AdamPublisherController@getCompaignDetails');

    
    Route::get('/adamPublihserAdamLine/{Campaign_ID}/{PublisherId}', ['as' => 'adamPublihser.adamPublihserAdamLine','uses' =>'AdamPublisherController@AdamPublisherAllAdamLines']);

    Route::get('/adamPublihserAdamLine/{AdamsLineItem_ID}', ['as' => 'adamPublihser.adamPublihserAdamLineDaily','uses' =>'AdamPublisherController@AdamPublisherAdamLinesDailyData']);

    Route::post('/saveDailyAdamLineItems', 'AdamPublisherController@saveDailyAdamLineItems');

    Route::post('/adamBingAdsApi','AdamAPIController@adamBingAdsApi');

    Route::post('/AdamTaboola','AdamAPIController@AdamTaboola');
    Route::post('/AdamOutbrain','AdamAPIController@AdamOutbrain');

    Route::get('/adamGoogleAnalytics', 'AdamAPIController@adamGoogleAnalytics');
     Route::get('/oauth2CallBackGoogleAnalytics', 'AdamAPIController@oauth2CallBackGoogleAnalytics');



    Route::post('/uploadExcelForManualPublisher', 'AdamManualPublisherUploadController@UploadExcelForManualPublisher')->name('uploadExcelForManualPublisher');
    Route::post('/getManualPublisherExcelInputFile', 'AdamSheetController@getManualPublisherExcelInputFile');

    Route::get('/deleteManulReport/{manualReportId}', 'AdamSheetController@deleteManulReport');

    Route::post('/getAdamLineItem_URLMapping', 'AdamSheetController@getAdamLineItem_URLMapping');
    Route::post('/upload_URL_ForManualPublisher', 'AdamManualPublisherUploadController@upload_URL_ForManualPublisher')->name('upload_URL_ForManualPublisher');

    Route::post('/hotstar','AdamAPIController@hotstar');
    // =============End adamAPI=======================================
    Route::post('/dailyProgrssCheck','AdamAPIController@dailyProgrssCheck');
    Route::post('/getProgressStatus','AdamAPIController@getProgressStatus');
    Route::post('/fbApis','ApiController@addFbApi');
    Route::post('/bingAdsApi','ApiController@addBingAdsApi');
    Route::resource('/unzip', 'ApiController@extractKeyZip');
    Route::post('/taboolaApi','ApiController@addTaboolaApi');
    Route::resource('/dcmApi', 'ApiController@addDcmApi');
    Route::resource('/dcmRedirectApi', 'ApiController@dcmRedirectApi');
    Route::post('/outBrainApi', 'ApiController@addOutbrainApi');
    Route::post('/adWordsApi', 'ApiController@addAdWordsApi');
    Route::get('/getTaboolaResult', function () {
        return view('getTaboolaResult');
    });
    Route::get('/getOutBrainResult', function () {
        return view('getOutBrainResult');
    });

    Route::post('/addSizMek', 'ApiController@addSizMek');
    Route::post('/testSizMek', 'ApiController@testSizMek');
    
    Route::post('/addCityBank', 'ApiController@addCityBank');
    Route::post('/addGoogleAnalytics', 'ApiController@addGoogleAnalytics');
    
    Route::get('/addGoogleAnalytics', 'ApiController@addGoogleAnalytics');
    Route::post('/processGAReport','ApiController@processGAReport');
    
    Route::get('/oauth2CallBack', 'ApiController@oauth2CallBack');
    Route::get('/oauth2CallBackGA', 'ApiController@oauth2CallBackGA');
    //Route::post('/addDBM_Display', 'ApiController@addDBM_Display_Youtube');
    Route::get('/addDBM_Display', 'ApiController@addDBM_Display_Youtube');
    Route::get('/DBMReportProcessing','ApiController@addDBM_Display_Youtube');
    
    Route::post('/processDBM_DispalyReport','ApiController@processDBM_DispalyReport');
    Route::post('/addAdWordsY', 'ApiController@addAdWordsYApi');
   

    //Store:  CampaignPublisher
    Route::post('/CampaignPublisherStore', 'CampaignPublishercontroller@storeCP');
    Route::post('/CampaignPublisherStoreCity', 'CampaignPublishercontroller@storeCP');

   // Route::post('/CampaignPublisherUpdate', 'CampaignPublishercontroller@updateCP');
    Route::post('/CampaignPublisherDelete', 'CampaignPublishercontroller@deleteCP');
    Route::post('/selectPublisherGenreMap','CampaignPublishercontroller@selectPublisherGenreMap');
    Route::post('/selectDataTypePublisherMap','CampaignPublishercontroller@selectDataTypePublisherMap');
    Route::post('/selectCreative','CampaignPublishercontroller@selectCreative');
    Route::post('/selectCreativeView','CampaignPublishercontroller@selectCreativeView');

    Route::post('/selectAllPublisherGenreMap','CampaignPublishercontroller@selectAllPublisherGenreMap');
   
    //Route::post('/selectGenreWRTPublisher','CampaignPublishercontroller@selectGenreWRTPublisher');
    
    

    Route::get('/matricsMapping','AdamManualPublisherUploadController@MatricsMapping');
    Route::post('mapMetrices', 'AdamManualPublisherUploadController@MapMetrices')->name('mapMetrices');

    //Route::get('/LineItem/{Campaign_ID}', ['as' => 'CampaignPublisher.getCampaignPublisherdetails','uses' =>'CampaignPublishercontroller@getCampaignPublisherdetails']);

    //show report of line item
    Route::get('/LineItemReport/{Campaign_ID}', ['as' => 'CampaignPublisher.getCampaignPublisherdetailsForReport','uses' =>'CampaignPublishercontroller@getCampaignPublisherdetailsForReport']);


     Route::get('/LineItemReport/{Campaign_ID}', ['as' => 'CampaignPublisher.getCampaignPublisherdetailsInExcel','uses' =>'CampaignPublishercontroller@getCampaignPublisherdetailsInExcel']);

     Route::get('/CampaignReport/{Campaign_ID}', ['as' => 'Campaign.getPlannedValuesReportInExcel','uses' =>'CampaignController@getPlannedValuesReportInExcel']);
     

    //LineItem: CRUD throgh admin panel.
    Route::get('/LineItem/{Campaign_ID}', ['as' => 'CampaignPublisher.getCampaignPublisherdetails','uses' =>'CampaignPublishercontroller@getCampaignPublisherdetails']);

    // ==============================================================
    Route::get('/adamLineItem/{Campaign_ID}', ['as' => 'AdamSheet.getAdamLines','uses' =>'AdamSheetController@getAdamLinesDetails']); 

    Route::post('/saveAdamLineItem', 'AdamSheetController@editAdamLineItem');

    Route::post('/saveAdamLineItemRow', 'AdamSheetController@insertAdamLineItem');

    Route::get('/getDitData', 'AdamSheetController@getDIT');


    Route::post('/saveAdamDCMMApping', 'AdamSheetController@saveAdamDCMMApping')->name('saveAdamDCMMApping');

    Route::post('/uploadDCMKeyFile', 'AdamSheetController@uploadDCMKeyFile')->name('uploadDCMKeyFile');
    
    Route::post('/getAllDCMkey', 'AdamSheetController@getAllDCMkey');

    Route::post('/updateDCMMulti', 'AdamSheetController@updateDCMMulti');

    Route::post('/saveAdamAlwaysOnCampaign', 'AdamSheetController@saveAdamAlwaysOnCampaign')->name('saveAdamAlwaysOnCampaign');

    Route::post('/updateAlwaysOnCampaign', 'AdamSheetController@updateAlwaysOnCampaign');    

    Route::post('/deleteAdamLine', 'AdamSheetController@deleteAdamLine');
    Route::post('/CheckUniqueAdamLineItemNameOnEdit', 'AdamSheetController@CheckUniqueAdamLineItemNameOnEdit');

    Route::Post('/mailAdamDataToUser','AdamSheetController@mailAdamDataToUser');
    // ==============================================================

    Route::get('/EditLineItemMetrices/{LineItemID}/{Genre_ID}/{Publisher_ID}', ['as' => 'CampaignPublisher.editLineItemMetrices','uses' =>'CampaignPublishercontroller@EditLineItemMetrices']);
    Route::get('/editLineItemMetricesCityBank/{LineItemID}/{Genre_ID}/{Publisher_ID}', ['as' => 'CampaignPublisher.editLineItemMetricesCityBank','uses' =>'CampaignPublishercontroller@EditLineItemMetricesCityBank']);

    //Route::post('/CampaignPublisherEditMetrices', 'CampaignPublishercontroller@EditMetrices');


    Route::post('/selectTargetingTargetingValueMap','CampaignPublishercontroller@selectTargetingTargetingValueMap');
    Route::post('/selectFormatTypeFormatMap','CampaignPublishercontroller@selectFormatTypeFormatMap');
    Route::post('/selectObjectiveObjectiveMap','CampaignPublishercontroller@selectObjectiveObjectiveMap');

    //cp edit
    Route::post('/CampaignPublisherEdit', 'CampaignPublishercontroller@editCP');
    Route::post('/getCityLocation','CampaignPublishercontroller@getCityLocation');
    Route::post('/getTargetingData','CampaignPublishercontroller@getTargetingData');

     // ---------------------------------------------------------------------------------------
    //Location
    Route::resource('/setMapSessions', 'CampaignPublishercontroller@setMapSession');
    Route::resource('/setCitySessions', 'CampaignPublishercontroller@setCitySession');

    Route::get('/gmap', function () {
        return view('gmap');
    });

    Route::get('/city', function () {
        return view('city');
    });
    Route::get('cities',array('as'=>'autocomplete','uses'=>'LocationController@autocomplete'));
    //end Location
   Route::get('pubFastSelect',array('as'=>'autocomplete','uses'=>'CampaignPublishercontroller@autocomplete'));
            
    // ---------------------------------------------------------------------------------------

 //======================================================================================================
    //Publisher Home:Compaign Details
    Route::get('/PublisherHome','LineItemDailyController@getCompaignDetails');
       // Route::get('/PublisherHome','AdamPublisherController@getCompaignDetails');


    Route::get('/getDailyLineItemsCity','LineItemDailyController@getDailyLineItemsCity');


    Route::post('/storeDailyLineItemsCity','LineItemDailyController@storeDailyLineItemsCity');

    // Route::get('/PublisherHome/{u_id}','LineItemDailyController@getCompaignDetails');
    Route::get('/LineItemDaily/{Campaign_ID}/{lineItem_Type}/{PublisherId}', ['as' => 'LineItemDaily.getLineItemDailyDetails','uses' =>'LineItemDailyController@getLineItemDailyDetails']);

    Route::post('/saveDailyLineItems', 'LineItemDailyController@saveDailyLineItem');
    //======================================================================================================

    //Client Home:Compaign Details created for client
    Route::get('/ClientHome','LineItemDailyController@getCompaignDetailsForClient');
    Route::get('/LineItemForCity/{Campaign_ID}', ['as' => 'LineItemDaily.getLineItemDetailsForCity','uses' =>'CampaignPublishercontroller@getCampaignPublisherdetails']);

    //======================================================================================================

    
    Route::post('/getDataforVariousApi','ApiController@getDataforVariousApi');
    Route::post('/getSheetListFromExcel','AdamManualPublisherUploadController@getSheetListFromExcel');
    Route::post('/UploadMultiExcelForManualPublisher','AdamManualPublisherUploadController@UploadMultiExcelForManualPublisher');
    //Route::get('/dummy','TestController@dummy');

    //Yahoo Oauth authentication APIs(start)
    Route::get('/authenticate-yahoo-user' , 'AdamAPIController@authenticate_yahoo_user');
    Route::get('/get-yahoo-access-token/{token_type?}/{debug?}' , 'AdamAPIController@get_yahoo_access_token');
    //Yahoo Oauth authentication APIs(end)

    //Yahoo Reporting APIs(start)
    Route::post('/yahoo_report_performance_stats' , 'AdamAPIController@yahoo_report_performance_stats');
    
});
