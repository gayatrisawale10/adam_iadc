<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Course', function (Blueprint $table) {
            $table->increments('Course_ID');
            $table->string('CourseCode');
            $table->string('CourseName');
            $table->string('Description');
            $table->integer('Duration');
            $table->integer('DurationType_ID')->unsigned();
            $table->string('Prerequisites');
            $table->integer('AffiliationBody_ID')->unsigned();
            $table->timestamps();
        });
  
       Schema::table('Course', function($table) {
       $table->foreign('DurationType_ID')->references('DurationType_ID')->on('DurationType');

   });
   
    Schema::table('Course', function($table) {
       $table->foreign('AffiliationBody_ID')->references('AffiliationBody_ID')->on('affiliations');

   });
   


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Course');
    }
}
