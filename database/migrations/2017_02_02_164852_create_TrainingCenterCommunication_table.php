<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingCenterCommunicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TrainingCenterCommunication', function (Blueprint $table) {
            $table->increments('TrainingCenterCommunication_ID');
            $table->integer('TrainingCenter_ID')->unsigned();
             $table->integer('CommunicationType_ID')->unsigned();
             $table->string('value');
            $table->timestamps();
        });

         Schema::table('TrainingCenterCommunication', function($table) {
       $table->foreign('TrainingCenter_ID')->references('TrainingCenter_ID')->on('Training_center');

   });
         Schema::table('TrainingCenterCommunication', function($table) {
       $table->foreign('CommunicationType_ID')->references('CommunicationType_ID')->on('communication_types');

   });
   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TrainingCenterCommunication');
    }
}
