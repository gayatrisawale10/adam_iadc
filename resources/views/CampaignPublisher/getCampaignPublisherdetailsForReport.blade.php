<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="shortcut icon" href="./image/favicon.ico" type="image/x-icon">
  <link rel="icon" href="./image/favicon.ico" type="image/x-icon">    
  <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
  <script src="../js/app.js"></script>
  <script data-require="jquery@*" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script data-require="datatables@*" data-semver="1.10.12" src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

  <!-- datatable scripts add for excel -->
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <!--end datatable scripts add for excel -->
  
  <link data-require="datatables@*" data-semver="1.10.12" rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
  <link rel="stylesheet" href="http://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" />
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <script src="../js/sweetalert.js"></script>
  <link href="../css/sweetalert.css" rel="stylesheet">
  <!-- css links for excel-->
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <!--end css links for excel-->

  <style>
    div.addRow{
          line-height: 45px;
          background-color: #fff;
          padding-left: 10px;
          border-bottom: 1px solid;
          border-top: 1px solid #e5e5e5;
    }
    .myrightalign{
          text-align: right;
             width: 145px;
    }
  .campTable {
    table-layout: fixed;
  }  
  .setEllipsis
  {
    white-space: nowrap; 
    overflow: hidden;
    text-overflow: ellipsis;
    cursor: pointer;
  }
  .campTable
  {
    user-select: none;
  }
  .dataTables_wrapper .dt-buttons {
  float:none;  
  text-align:right;
}
  </style>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'IADC') }}</title>
    
  <script>
    window.Laravel = <?php echo json_encode([
      'csrfToken' => csrf_token(),
    ]); ?>
  </script>
  <style type="text/css">  
    .margin-top-20{
        margin-top: 20px;
    }
    .navbar-fixed-bottom, .navbar-fixed-top {
    width:100%;
    /* position: relative; */
    margin-top:30px;
    }
    
    th, td {
        text-align:center;
        padding: 20px;
    }

    
   
    #example thead th
    {
      width: 100px !important;
    }

  </style>  
</head>
<body>    

  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header"></div>
      <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <!-- Left Side Of Navbar -->
        <ul class="nav navbar-nav">
          <img class="home_logo" src="{{asset('/image/logo_final.png') }}" width='60' height="60">
        </ul>
        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
          <!-- Authentication Links -->
          <li><a href="{{ url('/Campaign') }}">Campaign</a></li>
          <li><a href="{{ url('/Publisher') }}">Publisher</a></li> 
          <li><a href="{{ url('/Client') }}">Client</a></li> 
   
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()['name'] }} <span class="caret"></span>
            </a>
            <li>
              <a href="{{ url('/logout') }}"
                   onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                    Logout
              </a>
              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
              </form>
            </li>
                 
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="container"> 
    <div class="row">
      <div class="col-lg-12">
        <div class="pull-left"><h2>LineItem Report Details</h2>
            <h4>Campaign : {{$Campaign_name}}</h4>
        </div>
          <div class="pull-right margin-top-20">
            <a class="btn btn-success" href="{{ URL::previous() }}">Back </a>
        </div>
      </div>
    </div>
    <div class="row" >
      <div class="col-lg-12">
            <div class="btn btn-danger" id="errorid" style='display:none;'></div>            
        </div>
    </div>
    <div class="row">
      <div class="col-lg-12">       
        <!-- auto scroll start -->
        <div style="width: 100%; overflow-x:auto;">
          <table id="example" class= "display campTable"   width="100%" cellspacing="0" border="1px">
            <thead>
              <tr >
               
                <th rowspan="2">LineItem Name</th>
                <th rowspan="2">Genre</th>
                <th rowspan="2">Publisher Name</th>
                <th rowspan="2">Phase</th>
                <th rowspan="2">Objective</th>
                <th rowspan="2">Objective Value</th>
                <th rowspan="2">Deal Type</th> 
                <th rowspan="2">Delivery Mode</th>
                
                <th rowspan="2">Format Type</th>
                <th rowspan="2">Format</th>


                <th colspan="{{$getArrayCount}}" style="width: 1500px !important;">Planned</th>
                <th colspan="{{$getArrayCount}}" style="width: 1500px !important;">Actual</th>
               </tr> 
              
              <tr> 

                @if(in_array(0, $getNotNullMatrices))     
                <th>NoPlannedValue</th>
                @endif


                 @if(in_array(1, $getNotNullMatrices))     
                <th>Impression</th>
                @endif
               
                @if(in_array(2, $getNotNullMatrices))
                <th>Clicks</th>
                @endif

                @if(in_array(3, $getNotNullMatrices))
                <th>Visits</th>
                @endif
               
                @if(in_array(4, $getNotNullMatrices))
                <th>Views</th>
                @endif

                @if(in_array(5, $getNotNullMatrices))
                <th>Engagement</th>
                @endif

                @if(in_array(6, $getNotNullMatrices))
                <th>Leads</th>
                @endif

                @if(in_array(7, $getNotNullMatrices))
                <th>Net Rate</th>
                @endif

                @if(in_array(8, $getNotNullMatrices))
                <th>Net Cost</th>
                @endif

                @if(in_array(9, $getNotNullMatrices))
                <th>Reach</th>
                @endif

                @if(in_array(10, $getNotNullMatrices))
                <th>Installs</th>
                @endif

                @if(in_array(11, $getNotNullMatrices))
                <th>Spots</th>
                @endif

                @if(in_array(12, $getNotNullMatrices))
                <th>SMS</th>
                @endif

                @if(in_array(13, $getNotNullMatrices))
                <th>Mailers</th>
                @endif
                 




                @if(in_array(0, $getNotNullMatrices))     
                <th>NoActualValue</th>
                @endif

                @if(in_array(1, $getNotNullMatrices))     
                <th>Impression</th>
                @endif
               
                @if(in_array(2, $getNotNullMatrices))
                <th>Clicks</th>
                @endif

                @if(in_array(3, $getNotNullMatrices))
                <th>Visits</th>
                @endif
               
                @if(in_array(4, $getNotNullMatrices))
                <th>Views</th>
                @endif

                @if(in_array(5, $getNotNullMatrices))
                <th>Engagement</th>
                @endif

                @if(in_array(6, $getNotNullMatrices))
                <th>Leads</th>
                @endif

                @if(in_array(7, $getNotNullMatrices))
                <th>Net Rate</th>
                @endif

                @if(in_array(8, $getNotNullMatrices))
                <th>Net Cost</th>
                @endif

                @if(in_array(9, $getNotNullMatrices))
                <th>Reach</th>
                @endif

                @if(in_array(10, $getNotNullMatrices))
                <th>Installs</th>
                @endif

                @if(in_array(11, $getNotNullMatrices))
                <th>Spots</th>
                @endif

                @if(in_array(12, $getNotNullMatrices))
                <th>SMS</th>
                @endif
                
                @if(in_array(13, $getNotNullMatrices))
                <th>Mailers</th>
                @endif
                 

              </tr>                      
            </thead>
              <form method='post' action=''>
               
                @foreach($getLineItemDetails as $LineItemDetails)
                  <tr >
                    <td class="setEllipsis" title='{{$LineItemDetails->LineItemName}}'> {{$LineItemDetails->LineItemName}}</td>
                    <td> {{$LineItemDetails->GenreName}}</td>
                    <td> {{$LineItemDetails->PublisherName}}</td>
                    <td> {{$LineItemDetails->PhaseName}}</td>
                    <td> {{$LineItemDetails->ObjectiveName}}</td>
                    <td> {{$LineItemDetails->ObjectiveValueName}}</td>                     
                    <td> {{$LineItemDetails->DealTypeName}}</td> 
                    <td> {{$LineItemDetails->DeliveryModeName}}</td> 
                    
                    <td> {{$LineItemDetails->FormatTypeName}}</td>
                    <td> {{$LineItemDetails->FormatName}}</td>                           
              
                    <!-- @if(in_array(0, $getNotNullMatrices))     
                    <th>NoValue</th>
                    @endif
 -->
                    @if(in_array(1, $getNotNullMatrices)) 
                    <td> {{$LineItemDetails->Impression}}</td>
                    @endif

                    @if(in_array(2, $getNotNullMatrices))       
                    <td> {{$LineItemDetails->Clicks}}</td>
                    @endif

                    @if(in_array(3, $getNotNullMatrices))       
                    <td> {{$LineItemDetails->Visits}}</td>
                     @endif

                    @if(in_array(4, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Views}}</td>
                    @endif

                    @if(in_array(5, $getNotNullMatrices))                    
                    <td> {{$LineItemDetails->Engagement}}</td>
                    @endif

                    @if(in_array(6, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Leads}}</td>
                    @endif

                    @if(in_array(7, $getNotNullMatrices))
                    <td> {{$LineItemDetails->NetRate}}</td>
                    @endif

                    @if(in_array(8, $getNotNullMatrices))
                    <td> {{$LineItemDetails->NetCost}}</td>
                    @endif

                    @if(in_array(9, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Reach}}</td>
                    @endif

                    @if(in_array(10, $getNotNullMatrices)) 
                    <td> {{$LineItemDetails->Installs}} </td>
                    @endif

                    @if(in_array(11, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Spots}}</td>
                    @endif

                    @if(in_array(12, $getNotNullMatrices))
                    <td> {{$LineItemDetails->SMS}}</td>
                    @endif

                    @if(in_array(13, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Mailers}}</td> 
                    @endif


                   <!--  @if(in_array(0, $getNotNullMatrices))     
                    <th>NoValue</th>
                    @endif
                     -->
                    @if(in_array(1, $getNotNullMatrices))                    
                    <td> {{$LineItemDetails->Lid_Impression}}</td>
                    @endif

                    @if(in_array(2, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Lid_Clicks}}</td>
                    @endif

                    @if(in_array(3, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Lid_Visits}}</td>
                    @endif

                    @if(in_array(4, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Lid_Views}}</td>
                    @endif

                    @if(in_array(5, $getNotNullMatrices))                    
                    <td> {{$LineItemDetails->Lid_Engagement}}</td>
                    @endif

                    @if(in_array(6, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Lid_Leads}}</td>
                    @endif

                    @if(in_array(7, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Lid_NetRate}}</td>
                    @endif

                    @if(in_array(8, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Lid_NetCost}}</td>
                    @endif

                    @if(in_array(9, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Lid_Reach}}</td> 
                    @endif

                    @if(in_array(10, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Lid_Installs}} </td>
                    @endif

                    @if(in_array(11, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Lid_Spots}}</td>
                    @endif

                    @if(in_array(12, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Lid_SMS}}</td>
                    @endif

                    @if(in_array(13, $getNotNullMatrices))
                    <td> {{$LineItemDetails->Lid_Mailers}}</td>
                    @endif
  
                  </tr>   

               
                 @endforeach  
              </form>
        </table>
      </div>
    </div>
  </div>    
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="{{ url('/js/LineItemDaily.js') }}" type="text/javascript"></script>


<script>
    $(document).ready( function () 
    {
        $.noConflict();
        $('#example').css('border-bottom', 'none');
        $('#example').DataTable({
            "bProcessing" : true,
            "bPaginate" : true,
            "pagingType": "simple",
            "aaSorting": [],
                dom:'lfBrtip',
                buttons: [
                           'excel'
                ]      
        });
    });
</script>


</body> 
</html>