<!DOCTYPE html>
<html>

<head>
  <script data-require="jquery@*" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script data-require="datatables@*" data-semver="1.10.12" src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  
  <link data-require="datatables@*" data-semver="1.10.12" rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
  <link rel="stylesheet" href="http://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" />
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

 <script src="../js/sweetalert.js"></script>
  <link href="../css/sweetalert.css" rel="stylesheet">
<style>
div.addRow{
      line-height: 45px;
      background-color: #fff;
      padding-left: 10px;
      border-bottom: 1px solid;
      border-top: 1px solid #e5e5e5;
}
.myrightalign{
      text-align: right;
      width: 70px;
}
</style>
</head>

<body>
  <table id="example" class="display" width="100%" cellspacing="0">
    <thead>
     <tr>
            <th width='46px'>Genre</th>
            <th width='46px'>Publisher Name</th>
            <th width='46px'>Ad Type</th>
            <th width='46px'>Ad Section</th>
            <th width='46px'>Deal Type</th>
            <th width='46px'>Delivery Mode</th>
            <th width='46px'>Planned <br/> Impression</th>
            <th width='46px'>Planned Impression DIT</th>
            <th width='46px'>Planned Clicks</th>
            <th width='46px'>Planned<br/>Clicks<br/>DIT</th>
            <th width='46px'>Planned<br/>Unique<br/>Reach</th>
            <th width='46px'>Planned<br/> Unique <br/>ReachDIT</th>
            <th width='46px'>Net <br/>Unit<br/> Rate</th>
            <th width='46px'>Net Cost</th>          
            <th width='46px'>Action</th>    </thead>
        </tr> <form method='post' action=''>
          
  </table>

   <form method='post' action=''>
  <table id="newRow" !style="display:none;">
    <tbody>
      <tr>
        <td>
          <select id="genreName" name="genreName" class="form-control map-genre-publisher">
             <option value="" data-for="selected-genre">Select</option>
                 
          </select>
        </td>
        <td>
          <select id="publisherName" name="publisherName" class="form-control map-dit-publisher publishers">
             <option value="" data-for="selected-publisher">Select</option>               
          </select>

        </td>
        <td>
           <select id="adTypes" name="adTypes" class="form-control">
             <option value="" data-for="adTypes">Select</option>
                 
          </select>
        </td>
        <td>
           <select id="adSection" name="adSection" class="form-control">
             <option value="" data-for="adSection">Select</option>
                  
          </select>
        </td>
        <td>
           <select id="dType" name="dType" class="form-control">
             <option value="" data-for="dType">Select</option>
                 
          </select>
        </td>
        <td>
           <select id="mode" name="mode" class="form-control">
             <option value="" data-for="mode">Select</option>
                 
          </select>
        </td>
        <td> <input type='text' class="myrightalign" name='PlannedImpressions' value="" id="impressions" placeholder = "Planned Impression">

        </td>
        <td>
          <select id="PIDataInputTypeID" name="PIDataInputTypeID" class="form-control">
             <option value="" data-for="PIDataInputTypeID">Select</option>
          </select>
        </td>
        

        <td><input type='text' class="myrightalign" name='PlannedClicks' value=""  id="clicks" placeholder = "PlannedClicks"></td>
        <td>
          <select id="PCDataInputTypeID" name="PCDataInputTypeID" class="form-control">
             <option value="" data-for="PCDataInputTypeID">Select</option>
          </select>
        </td>
        <td><input type='text' class="myrightalign" name='PlannedUniqueReach' value="" id="unique" placeholder = "Planned Unique Reach">
        </td>
        <td>
          <select id="PURDataInputTypeID" name="PURDataInputTypeID" class="form-control">
             <option value="" data-for="PURDataInputTypeID">Select</option>
          </select>
        </td>
        <td><input class="myrightalign" type='text' name='NetUnitRate' value="" id="NetUnitRate" placeholder = "Net Unit Rate"></td> 
        <td><input  class="myrightalign" type='text' name='NetCost' value="" id="NetCost" placeholder = "Net Cost"></td>          
       
        <td><button type="submit" class="btn green saveCP"><i  class="fa fa-save" aria-hidden="true"></i></button>
        <button type="button" class="btn btn-default ">  <i class="fa fa-minus-square removerow" aria-hidden="true"></i> </button></td>
      </tr>
    </tbody>
  </table>
 <input type='text' name='get_Campaign_id' value="" id="get_Campaign_id">
  </form>
  <script>
    $(document).ready(function() {
      var table;
      var url = '';
      table = $('#example').DataTable({

        ajax: url,
        rowReorder: {
          dataSrc: 'genre',
          selector: 'tr'
        },
        columns: [{
          data: 'genre'
        }, {
          data: 'publisher1'
        }, {
          data: 'type'
        }, {
          data: 'section'
        },{
          data: 'Dtype'
        },{
          data: 'DMode'
        },{
          data: 'Impression'
        },{
          data: 'idit'
        },{
          data: 'click'
        },{
          data: 'cdit'
        },{
          data: 'Unique'
        },{
          data: 'udit'
        },{
          data: 'NetUnit'
        },{
          data: 'NetCost'
        }, {
          data: 'delete'
        }]
      });
      $("#example").on("click", "td .removerow", function(e) {
        table.row($(this).closest("tr")).remove().draw();
         $("#addRow").show();
      });
    $('#example').css('border-bottom', 'none');
    $('<div class="addRow"><button id="addRow">Add New Row</button></div>').insertAfter('#example');

      // add row
      $('#addRow').click(function() {
        var rowHtml = $("#newRow").find("tr")[0].outerHTML
        console.log(rowHtml);
        table.row.add($(rowHtml)).draw();
        $("#addRow").hide();

      });
    });











  </script>
</body>

</html>