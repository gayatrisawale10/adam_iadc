@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="pull-left">
        <h2> API's</h2>
      </div>
    
    </div>        
  </div>
    
  @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
  @endif
   <?php $base_url= url('/'); 
?>   
  <div class="portlet-body">
    <div class="table-toolbar">
        <table class="table table-bordered">
          <thead>   
            <tr>         
              <th>API</th>
              <th>Start Date</th>             
              <th width="280px">Action</th>
            </tr>
          </thead>
          <tbody>         
          
              <tr align="center">       
              <td>Facebook<input type='hidden' id="id" name='id' value="1"> </td>
              <td>
            <input type="text" placeholder="Start Date" class="form-control" name='startDate' value=""  id="datepicker" style="background-color:white;" />   

              </td>
              <td>
                <div class="col-sm-6">
             
              <button type="submit" class="" onclick="add();">Add</button>
                </div>               
              </td>
            </tr>   
            </tbody>
        </table>
        @endsection
     
    </div>
  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
 <meta name="csrf-token" content=<?php echo "'".csrf_token()."'"; ?> >
<script type="text/javascript">  
$(function() {
   
  $("#datepicker").datepicker({ dateFormat: 'dd-mm-yy',
        minViewMode: 1,
        yearRange: '1999:2050',
        changeMonth: true,
        changeYear: true }).val();

});
function add() {

  var StartDate = $('#datepicker').val();
  var id = $('#id').val();
                              

      $.ajax({
            type: 'POST',
            url: 'api',
            data: {'id': id,'StartDate':StartDate},
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
          success: function (data) {
            console.log(data);
            // if(response.status =='success')
            // {
            //   swal(
            //         '',
            //         'Suceessfully Updated',
            //         'success'
            //       );
            //   setTimeout(function(){
            //           window.location.reload();
            //   }, 4000);
            // }
          }
       });
};

</script>