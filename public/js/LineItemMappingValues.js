function popitup(url) {
  $('#Glocation').remove();
  $('#GlocationValue').remove();
  $('#GDistance').remove();
  $('#GDistanceValue').remove();
  newwindow=window.open(url,'name','height=200,width=500,align=center');
  if (window.focus) {newwindow.focus()}
  return false;
}

function popup(url) {
  $('#incLoc').remove();
  $('.incLocValue').remove();
  $('#excLoc').remove();
  $('.excLocValue').remove();
  newwindow=window.open(url,'name','height=450,width=800,align=center');
  if (window.focus) {newwindow.focus()}
  return false;
}

 
$(document).on('change', '.pubfastSelect', function() { 
  var pulisherId=$('.multipleInput').val(); 
  $('.pubId').val(pulisherId);
});
     
// --------------------------------------------------------------------------------------------
$(document).on('change', '.map-genre-publisher', function() {
  $('.publishers').empty();
  var selectedOption = $(this).val();

  if(selectedOption === '' )
  {
    var CSRF_TOKEN=$("#token").val();
    $.ajaxSetup({
      headers: {
          'X-CSRF-Token': $('meta[name=_token]').attr('content')
      }
    });
    $.ajax({
      type: 'POST',
      url: '../selectAllPublisherGenreMap',
      data: { "_token": CSRF_TOKEN,'genre_id':selectedOption},
      success: function (response ) {

        console.log(response);
            $(".publishers").append('<option value="" data-for="selected-publisher">Select</option>');
            $.each(response.publisher, function (key, value) {
              $(".publishers").append('<option value="'+value.id+'" data-for="selected-publisher">'+value.PublisherName+'</option>');
            });
      }
    });
  }  
  // -----------------------------------------------
  var CSRF_TOKEN=$("#token").val();
  $.ajaxSetup({
    headers: {
        'X-CSRF-Token': $('meta[name=_token]').attr('content')
    }
  });
  $.ajax({
    type: 'POST',
    url: '../selectPublisherGenreMap',
    data: { "_token": CSRF_TOKEN,'genre_id':selectedOption},
    success: function (response ) {
      console.log(response);
          $(".publishers").append('<option value="" data-for="selected-publisher">Select</option>');
          $.each(response.publisher, function (key, value) {
            $(".publishers").append('<option value="'+value.id+'" data-for="selected-publisher">'+value.PublisherName+'</option>');
          });
    }
  });
});

//code to get creative on basis of card  map-card-creative
$(document).on('change', '.map-card-creative', function() {
  $('.creative').empty();
  var selectedOption = $(this).val();
  var CSRF_TOKEN=$("#token").val();
  $.ajaxSetup({
    headers: {
        'X-CSRF-Token': $('meta[name=_token]').attr('content')
    }
  });
  $.ajax({
    type: 'POST',
    url: '../selectCreative',
    data: { "_token": CSRF_TOKEN,'card_id':selectedOption},
    success: function (response) {
          $(".creative").append('<option value="" data-for="selected-creative">Select</option>');
          $.each(response.creative, function (key, value) {
            $(".creative").append('<option value="'+value.id+'" data-for="selected-publisher">'+value.CreativeName+'('+value.CardName+')'+'</option>');
          });
    }
  });
});
//end of code

$(document).on('change', '.map-dit-publisher', function() {
  $('.PIDataInputTypeID').empty();
  $('.PCDataInputTypeID').empty();
  $('.PVsDataInputTypeID').empty();
  $('.PVwDataInputTypeID').empty();
  $('.PLDataInputTypeID').empty();
  $('.PEDataInputTypeID').empty();
  $('.PNRDataInputTypeID').empty();
  $('.PNCDataInputTypeID').empty();
  $('.PRDataInputTypeID').empty();
  $('.PInDataInputTypeID').empty();
  $('.PSpDataInputTypeID').empty();
  $('.PSmDataInputTypeID').empty();
  $('.PMDataInputTypeID').empty();
  $('.PNCoDataInputTypeID').empty();
  $('.PNRADataInputTypeID').empty();
  $('.PODataInputTypeID').empty();

  var CSRF_TOKEN=$("#token").val();
  var selectedOption = $(this).val();
  $.ajaxSetup({
      headers: {
          'X-CSRF-Token': $('meta[name=_token]').attr('content')
      }
  });

  $.ajax({
    type: 'POST',
    url: '../selectDataTypePublisherMap',
    data: { "_token": CSRF_TOKEN,'p_id':selectedOption},
    success: function (response) {
      console.log(response);// return false;
      var arr = ["4","6","7","8","9","10","11"];
      var api = [];
      var nonApi = [];
      $.each(response.dataInputType, function (key, value) {
            if(jQuery.inArray(value.id,arr) != -1){
                api.push(response.dataInputType[key]);
            } else {
                nonApi.push(response.dataInputType[key]);
            }
      });

      console.log(nonApi);
      console.log(api);

      var Pid = "";
      var Pid1 = "";
      var Pcd = "";
      var Pcd1 = "";
      var PVs = '';
      var PVs1 = '';
      var PViw = '';
      var PViw1 = '';
      var PLead = '';
      var PLead1 = '';
      var PEng = '';
      var PEng1 = '';
      var PCost = '';
      var PCost1 = '';
      var PRate = '';
      var PRate1 = '';
      var PReach = '';
      var PReach1 = '';
      var PInstall = '';
      var PInstall1= '';
      var PSpot = '';
      var PSpots1 = '';
      var PSMS = '';
      var PSMS1 = '';
      var PMailers = '';
      var PMailers1 = '';
      var PCO='';
      var PCO1='';
      var PRA='';
      var PRA1='';
      var POpen='';
      var POpen1='';


      if(nonApi.length>0){
        $.each(nonApi, function (key, value) {
          Pid = Pid+'<option value="'+value.id+'" data-for="selected-PIDataInputTypeID">'+value.DataInputTypeName+'</option>';
          Pcd = Pcd+'<option value="'+value.id+'" data-for="selected-PCDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PVs = PVs+'<option value="'+value.id+'" data-for="selected-PVsDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PViw = PViw+'<option value="'+value.id+'" data-for="selected-PVwDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PLead = PLead+'<option value="'+value.id+'" data-for="selected-PLDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PEng = PEng+'<option value="'+value.id+'" data-for="selected-PEDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PRate = PRate+'<option value="'+value.id+'" data-for="selected-PNRDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PCost = PCost+'<option value="'+value.id+'" data-for="selected-PNCDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PReach = PReach+'<option value="'+value.id+'" data-for="selected-PRDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PInstall = PInstall+'<option value="'+value.id+'" data-for="selected-PInDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PSpot = PSpot+'<option value="'+value.id+'" data-for="selected-PSpDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PSMS = PSMS+'<option value="'+value.id+'" data-for="selected-PSmDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PMailers = PMailers+'<option value="'+value.id+'" data-for="selected-PMDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PCO = PCO+'<option value="'+value.id+'" data-for="selected-PNCoDataInputTypeID">'+value.DataInputTypeName+'</option>';
          PRA = PRA+'<option value="'+value.id+'" data-for="selected-PNRADataInputTypeID">'+value.DataInputTypeName+'</option>';
          POpen = POpen+'<option value="'+value.id+'" data-for="selected-PODataInputTypeID">'+value.DataInputTypeName+'</option>';
        });
      }

      console.log(POpen);

      if(api.length>0){
          Pid1 = '<optgroup label="API">';
          Pcd1 = '<optgroup label="API">';
          PVs1 = '<optgroup label="API">';
          PViw1 = '<optgroup label="API">';
          PLead1 = '<optgroup label="API">';
          PEng1 = '<optgroup label="API">';
          PRate1 = '<optgroup label="API">';
          PCost1 = '<optgroup label="API">';
          PReach1 = '<optgroup label="API">';
          PInstall1 = '<optgroup label="API">';
          PSpots1 = '<optgroup label="API">';
          PSMS1 = '<optgroup label="API">';
          PMailers1 = '<optgroup label="API">';
          PCO1 = '<optgroup label="API">';
          PRA1 = '<optgroup label="API">';
          POpen1 = '<optgroup label="API">';

          $.each(api, function (key, value) {
              Pid1 = Pid1+'<option value="'+value.id+'" data-for="selected-PIDataInputTypeID">'+value.DataInputTypeName+'</option>';
              Pcd1 = Pcd1+'<option value="'+value.id+'" data-for="selected-PCDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PVs1 = PVs1+'<option value="'+value.id+'" data-for="selected-PVsDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PViw1 = PViw1+'<option value="'+value.id+'" data-for="selected-PVwDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PLead1 = PLead1+'<option value="'+value.id+'" data-for="selected-PLDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PEng1 = PEng1+'<option value="'+value.id+'" data-for="selected-PEDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PRate1 = PRate1+'<option value="'+value.id+'" data-for="selected-PNRDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PCost1 = PCost1+'<option value="'+value.id+'" data-for="selected-PNCDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PReach1 = PReach1+'<option value="'+value.id+'" data-for="selected-PRDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PInstall1 = PInstall1+'<option value="'+value.id+'" data-for="selected-PInDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PSpots1 = PSpots1+'<option value="'+value.id+'" data-for="selected-PSpDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PSMS1 = PSMS1+'<option value="'+value.id+'" data-for="selected-PSmDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PMailers1 = PMailers1+'<option value="'+value.id+'" data-for="selected-PMDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PCO1 = PCO1+'<option value="'+value.id+'" data-for="selected-PNCoDataInputTypeID">'+value.DataInputTypeName+'</option>';
              PRA1 = PRA1+'<option value="'+value.id+'" data-for="selected-PNRADataInputTypeID">'+value.DataInputTypeName+'</option>';
              POpen1 = POpen1+'<option value="'+value.id+'" data-for="selected-PODataInputTypeID">'+value.DataInputTypeName+'</option>';
          });
          Pid1 = Pid1+'</optgroup>';
          Pcd1 = Pcd1+'</optgroup>';
          PVs1 = PVs1+'</optgroup>';
          PViw1 = PViw1+'</optgroup>';
          PLead1 = PLead1+'</optgroup>';
          PEng1 = PEng1+'</optgroup>';
          PRate1 = PRate1+'</optgroup>';
          PCost1 = PCost1+'</optgroup>';
          PReach1 = PReach1+'</optgroup>';
          PInstall1 = PInstall1+'</optgroup>';
          PSpots1 = PSpots1+'</optgroup>';
          PSMS1 = PSMS1+'</optgroup>';
          PMailers1 = PMailers1+'</optgroup>';
          PCO1 = PCO1+'</optgroup>';
          PRA1 = PRA1+'</optgroup>';
          POpen1 = POpen1+'</optgroup>';
      }
      //console.log(PSpots1);
      var PID = Pid+Pid1;
      var PCD = Pcd+Pcd1;
      var PVisit = PVs+PVs1;
      var PView = PViw+PViw1;
      var PLeads = PLead+PLead1;
      var PEngage = PEng+PEng1;
      var PRates = PRate+PRate1;
      var PCosts = PCost+PCost1;
      var PReachs = PReach+PReach1;
      var PInstalls = PInstall+PInstall1;
      var PSpots = PSpot+PSpots1;
      var Psms = PSMS+PSMS1;
      var PMailers = PMailers+PMailers1;
      var PCO = PCO+PCO1;
      var PRA = PRA+PRA1;
      var POpen = POpen+POpen1;
      //console.log(PSpot);
      $(".PIDataInputTypeID").append('<option value="" data-for="selected-PIDataInputTypeID">Select</option>').append(PID);
      $(".PCDataInputTypeID").append('<option value="" data-for="selected-PCDataInputTypeID">Select</option>').append(PCD);
      $(".PVsDataInputTypeID").append('<option value="" data-for="selected-PVsDataInputTypeID">Select</option>').append(PVisit);
      $(".PVwDataInputTypeID").append('<option value="" data-for="selected-PVwDataInputTypeID">Select</option>').append(PView);
      $(".PLDataInputTypeID").append('<option value="" data-for="selected-PLDataInputTypeID">Select</option>').append(PLeads);
      $(".PEDataInputTypeID").append('<option value="" data-for="selected-PEDataInputTypeID">Select</option>').append(PEngage);
      $(".PNRDataInputTypeID").append('<option value="" data-for="selected-PNRDataInputTypeID">Select</option>').append(PRates);
      $(".PNCDataInputTypeID").append('<option value="" data-for="selected-PNCDataInputTypeID">Select</option>').append(PCosts);
      $(".PRDataInputTypeID").append('<option value="" data-for="selected-PRDataInputTypeID">Select</option>').append(PReachs);
      $(".PInDataInputTypeID").append('<option value="" data-for="selected-PInDataInputTypeID">Select</option>').append(PInstalls);
      $(".PSpDataInputTypeID").append('<option value="" data-for="selected-PSpDataInputTypeID">Select</option>').append(PSpots);
      $(".PSmDataInputTypeID").append('<option value="" data-for="selected-PSmDataInputTypeID">Select</option>').append(Psms);
      $(".PMDataInputTypeID").append('<option value="" data-for="selected-PMDataInputTypeID">Select</option>').append(PMailers);
      $(".PODataInputTypeID").append('<option value="" data-for="selected-PODataInputTypeID">Select</option>').append(POpen);

      $(".PNCoDataInputTypeID").append('<option value="" data-for="selected-PNCoDataInputTypeID">Select</option>').append(PCO);
      $(".PNRADataInputTypeID").append('<option value="" data-for="selected-PNRADataInputTypeID">Select</option>').append(PRA);
      $(".PURDataInputTypeID").append('<option value="" data-for="selected-PURDataInputTypeID">Select</option>');
      $.each(response.dataInputType, function (key, value) {

      $(".PURDataInputTypeID").append('<option value="'+value.id+'" data-for="selected-PURDataInputTypeID">'+value.DataInputTypeName+'</option>');
      });
    }
  });
});

// ----------------------------------------------------------------------------

$(document).on('change', '.map-targeting-targetingValue', function() {
 //alert("target");
  $('.targetingValues').empty();

  $('.targetingValuesEdit').empty();


  var selectedOption = $(this).val();

  //alert(selectedOptioslen);
  var CSRF_TOKEN=$("#token").val();

  $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });

  $.ajax({
        type: 'POST',
        url: '../selectTargetingTargetingValueMap',
        data: { "_token": CSRF_TOKEN,'targeting_id':selectedOption},
        success: function (response ) {
          console.log(response);

          // $(".targetingValues").append('<option value="" data-for="selected-targetingValue">Select</option>');

          $.each(response.targetingValue, function (key, value) {

           $(".targetingValues").append('<option value="'+value.id+'" data-for="selected-targetingValue">'+value.TargetingValueName+'</option>');
          });

          //append on new secletion of edit

           // $(".targetingValuesEdit").append('<option value="" data-for="selected-targetingValue">Select</option>');

          $.each(response.targetingValue, function (key, value) {

           $(".targetingValuesEdit").append('<option value="'+value.id+'" data-for="selected-targetingValue">'+value.TargetingValueName+'</option>');
          });


        }
    });
});


$(document).on('change', '.map-formatType-format', function() {
  //alert("format");
  $('.formats').empty();
  $('.formatValue').empty();
  var selectedOption = $(this).val();
    var CSRF_TOKEN=$("#token").val();
  $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });

  $.ajax({
        type: 'POST',
        url: '../selectFormatTypeFormatMap',
        data: { "_token": CSRF_TOKEN,'formatType_id':selectedOption},
        success: function (response ) {
          //console.log(data);
          $(".formats").append('<option value="" data-for="selected-formats">Select</option>');

          $.each(response.format, function (key, value) {

           $(".formats").append('<option value="'+value.id+'" data-for="selected-formats">'+value.FormatName+'</option>');
          });

          $( ".formatValueName" ).empty();
          $( ".formatValue" ).empty();

          if(response.formatType_id==1 || response.formatType_id==2 ){
            // $( ".formatValueName" ).empty();
            // $( ".formatValue" ).empty();
            $(".formatValueName").append('L*B');
            $(".formatValue").append('<input type="text" class="form-control " value="" name="FormatValue">');
          }
          else if(response.formatType_id==3){
            // $( ".formatValueName" ).empty();
            // $( ".formatValue" ).empty();
            $(".formatValueName").append('Char');
            $(".formatValue").append('<input type="text" class="form-control " value="" name="FormatValue">');
          }
          else if(response.formatType_id==4){
            // $( ".formatValueName" ).empty();
            // $( ".formatValue" ).empty();
            $(".formatValueName").append('Seconds');
            $(".formatValue").append('<input type="text" class="form-control " value="" name="FormatValue">');
          }
        }
    });
});

$(document).on('change', '.map-objective-objectiveValue', function() {

  //alert("obj");
  $('.objectiveValues').empty();
  var selectedOption = $(this).val(); //alert('obj on add'+selectedOption);
  var CSRF_TOKEN=$("#token").val();

  $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });

  $.ajax({
    type: 'POST',
    url: '../selectObjectiveObjectiveMap',
    data: { "_token": CSRF_TOKEN,'objective_id':selectedOption},
    success: function (response ) {
    // console.log(data);
      $(".objectiveValues").append('<option value="" data-for="selected-objectiveValues">Select</option>');

      $.each(response.objectiveValue, function (key, value) {

       $(".objectiveValues").append('<option value="'+value.id+'" data-for="selected-objectiveValues">'+value.ObjectiveValueName+'</option>');
      });

    }
  });
});

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
   return true;
}

//delete lineitem
function DeleteLineItem( lineItemId)
{
  //alert(lineItemId);
var x = confirm("Are you sure you want to delete?");
if (x){
        var lineItemid=lineItemId;
        var CSRF_TOKEN=$("#token").val();

        $.ajaxSetup({
          headers: {
              'X-CSRF-Token': $('meta[name=_token]').attr('content')
          }
        });

        $.ajax({
          type: 'POST',
          url: '../CampaignPublisherDelete',
          data: { "_token": CSRF_TOKEN,'lineItemid':lineItemid},
          success: function (response ) {
            //console.log(response);

            if(response.status =='success')
            {
              swal(
                  '',
                  'Suceessfully Deleted',
                  'success'
              );


            }
            else if(response.status =='unsuccessful')
            {
              swal(
                  '',
                  'Daily Line Item entry exist, Unable to delete LineItem',
                  'error'
              );
            }

            location.reload(true);
          }
        });
}
}


$(document).on('click', '.viewLineItem', function()
{
  //alert("view");
  id = $(this).attr('data-id');
  LineItemID = $(this).attr('data-LineItemID');
  LineItemName = $(this).attr('data-LineItemName');
  Campaign_ID = $(this).attr('data-Campaign_ID');
  GenreName = $(this).attr('data-GenreName');
  PublisherName = $(this).attr('data-PublisherName');
  PhaseName = $(this).attr('data-PhaseName');
  ObjectiveValueName = $(this).attr('data-ObjectiveValueName');
  ObjectiveName = $(this).attr('data-ObjectiveName');
  MediumName = $(this).attr('data-MediumName');

  GenderName = $(this).attr('data-GenderName');

  TargetingName = $(this).attr('data-TargetingName');
  TargetingValueName = $(this).attr('data-TargetingValueName');
  FormatName = $(this).attr('data-FormatName');
  FormatTypeName = $(this).attr('data-FormatTypeName');
  DealTypeName = $(this).attr('data-DealTypeName');
  DeliveryModeName = $(this).attr('data-DeliveryModeName');
  AgeRangeFrom = $(this).attr('data-AgeRangeFrom');
  AgeRangeTo = $(this).attr('data-AgeRangeTo');
  FormatValue = $(this).attr('data-FormatValue');
  FormatValue = $(this).attr('data-FormatValue');

  Section = $(this).attr('data-SectionName');
  AdUnit = $(this).attr('data-AdUnitName');

  GoogleLocation = $(this).attr('data-GoogleLocation');
  GoogleRadius = $(this).attr('data-GoogleRadius');
  IsLocationBased = $(this).attr('data-IsLocationBased');

  Impression = $(this).attr('data-Impression');
  DITImp = $(this).attr('data-DITImp'); //alert(DITImp);

  Clicks = $(this).attr('data-Clicks');
  DitClicks = $(this).attr('data-DitClicks');

  Visits = $(this).attr('data-Visits');
  DITVisits = $(this).attr('data-DITVisits');

  Views = $(this).attr('data-Views');
  DITViews = $(this).attr('data-DITViews');

  Engagement = $(this).attr('data-Engagement');
  DITEngagement = $(this).attr('data-DITEngagement');

  Leads = $(this).attr('data-Leads');
  DITLeads = $(this).attr('data-DITLeads');

  NetRate = $(this).attr('data-NetRate');
  DITNetRate = $(this).attr('data-DITNetRate');

  NetCost = $(this).attr('data-NetCost');
  DITNetCost = $(this).attr('data-DITNetCost');

  Reach = $(this).attr('data-Reach');
  DITReach = $(this).attr('data-DITReach');

  Installs = $(this).attr('data-Installs');
  DITInstalls = $(this).attr('data-DITInstalls');

  Spots = $(this).attr('data-Spots');
  DITSpots = $(this).attr('data-DITSpots');

  SMS = $(this).attr('data-SMS');
  DITSMS = $(this).attr('data-DITSMS');

  Mailers = $(this).attr('data-Mailers');
  DITMailers = $(this).attr('data-DITMailers');

  cityINCLoc = $(this).attr('data-cityINCLoc'); //alet(cityINCLoc);
  cityEXLoc = $(this).attr('data-cityEXLoc'); //alert(cityEXLoc);

  CityINCLoc = [];
  $.each(JSON.parse(cityINCLoc), function(index,value) {
    CityINCLoc.push(value);
  });

  CityEXLoc = [];
  $.each(JSON.parse(cityEXLoc), function(index,value) {
    CityEXLoc.push(value);
  });

  // -----------------------------------------
  targeting = $(this).attr('data-TargetingName'); //alet(cityINCLoc);
  targetingValue = $(this).attr('data-TargetingValueName'); //alert(cityEXLoc);

  targetingNames = [];
  $.each(JSON.parse(targeting), function(index,value) {
    targetingNames.push(value);
  });
  //alert(targetingNames);
   targetingValueNames = [];
  $.each(JSON.parse(targetingValue), function(index,value) {
    targetingValueNames.push(value);
  });
  //alert(targetingValueNames);
  // -----------------------------------------

  $('#id').text(id);
  $('#LineItemID').text(LineItemID);
  $('#lineItemName').text(LineItemName);
  $('#Campaign_ID').text(Campaign_ID);
  $('#GenreName').text(GenreName);
  $('#PublisherName').text(PublisherName);
  $('#PhaseName').text(PhaseName);

  $('#ObjectiveValueName').text(ObjectiveValueName);
  $('#ObjectiveName').text(ObjectiveName);
  $('#MediumName').text(MediumName);
  $('#TargetingNameOther').text(TargetingName);

  $('#targetingValueGender').text(GenderName);
  $('#targetingValueOthers').text(TargetingValueName);

  $('#FormatTypeName').text(FormatTypeName);
  $('#FormatName').text(FormatName);
  $('#DealTypeName').text(DealTypeName);

   $('#Section').text(Section);
    $('#AdUnit').text(AdUnit);

  $('#DeliveryModeName').text(DeliveryModeName);
  $('#AgeRangeFrom').text(AgeRangeFrom);
  $('#AgeRangeTo').text(AgeRangeTo);
  $('#FormatValue').text(FormatValue);
  $('#gooleLoc').text(GoogleLocation);
  $('#googleRadius').text(GoogleRadius);

  $('#Impression').text(Impression);
  $('#DitIMP').text(DITImp);

  $('#Clicks').text(Clicks);
  $('#DitClicks').text(DitClicks);

  $('#Visits').text(Visits);
  $('#DITVisits').text(DITVisits);

  $('#Views').text(Views);
  $('#DITViews').text(DITViews);

  $('#Engagement').text(Engagement);
  $('#DITEngagement').text(DITEngagement);

  $('#Leads').text(Leads);
  $('#DITLeads').text(DITLeads);

  $('#NetRate').text(NetRate);
  $('#DITNetRate').text(DITNetRate);

  $('#NetCost').text(NetCost);
  $('#DITNetCost').text(DITNetCost);

  $('#Reach').text(Reach);
  $('#DITReach').text(DITReach);

  $('#Installs').text(Installs);
  $('#DITInstalls').text(DITInstalls);

  $('#Spots').text(Spots);
  $('#DITSpots').text(DITSpots);

  $('#SMS').text(SMS);
  $('#DITSMS').text(DITSMS);

  $('#Mailers').text(Mailers);
  $('#DITMailers').text(DITMailers);

  $('#cityInLoc').text(CityINCLoc.toString());
  $('#cityExLoc').text(CityEXLoc.toString());

  $('#TargetingNameOther').text(targetingNames.toString());
  $('#targetingValueOthers').text(targetingValueNames.toString());

  // if(TargetingName=='Age')
  // {
  //   $('#radiobtnAge').attr("checked", "checked");
    $("#AgeRangeFrom").show();
    $("#AgeRangeTo").show();
    $("#ageFrmLabel").show();
    $("#ageToLabel").show();

  //   $("#targetingValueGender").hide();
  //   $("#gooleLoc").hide();
  //   $("#incLocLabel").hide();
  //   $("#exLocLabel").hide();
  //   $("#TargetingNameOther").hide();
  //   $("#targetingValueOthers").hide();
  // }
  // if
  // (TargetingName=='Gender')
  // {
  //   $('#radiobtnGender').attr("checked", "checked");

  //   $("#AgeRangeFrom").hide();
  //   $("#AgeRangeTo").hide();
  //   $("#ageFrmLabel").hide();
  //   $("#ageToLabel").hide();

    $("#targetingValueGender").show();
    // $("#gooleLoc").hide();
    // $("#incLocLabel").hide();
    // $("#exLocLabel").hide();
    // $("#TargetingNameOther").hide();
    // $("#targetingValueOthers").hide();

  //}
  // if(TargetingName=='Geo' && IsLocationBased== "")
  // {
  //   $('#radiobtnGLoc').attr("checked", "checked");

  //   $("#AgeRangeFrom").hide();
  //   $("#AgeRangeTo").hide();
  //   $("#ageFrmLabel").hide();
  //   $("#ageToLabel").hide();

  //   $("#targetingValueGender").hide();
    $("#gooleLoc").show();
  //   $("#incLocLabel").hide();
  //   $("#exLocLabel").hide();
  //   $("#TargetingNameOther").hide();
  //   $("#targetingValueOthers").hide();

  // }
  // if(TargetingName=='Geo' && IsLocationBased==1)
  // {
  //   $('#radiobtnCLoc').attr("checked", "checked");

  //   $("#AgeRangeFrom").hide();
  //   $("#AgeRangeTo").hide();
  //   $("#ageFrmLabel").hide();
  //   $("#ageToLabel").hide();

  //   $("#targetingValueGender").hide();
  //   $("#gooleLoc").hide();
    $("#incLocLabel").show();
    $("#exLocLabel").show();
  //   $("#TargetingNameOther").hide();
  //   $("#targetingValueOthers").hide();

  // }

  // if(TargetingName=='Interest' || TargetingName=='Topic' ||  TargetingName=='Behavior' || TargetingName=='Contextual' || TargetingName=='InMarket')
  // {
  //   $('#rdobtnTargetingOthers').attr("checked", "checked");

  //   $("#AgeRangeFrom").hide();
  //   $("#AgeRangeTo").hide();
  //   $("#ageFrmLabel").hide();
  //   $("#ageToLabel").hide();

  //   $("#targetingValueGender").hide();
  //   $("#gooleLoc").hide();
  //   $("#incLocLabel").hide();
  //   $("#exLocLabel").hide();
    $("#TargetingNameOther").show();
    $("#targetingValueOthers").show();
  //}
});
//view city line Item
$(document).on('click', '.viewLineItemCity', function()
{
  $('.cardview').empty();
  $('.creativeview').empty();
  id = $(this).attr('data-id');
  LineItemID = $(this).attr('data-LineItemID');
  LineItemName = $(this).attr('data-LineItemName');
  Campaign_ID = $(this).attr('data-Campaign_ID');
  GenreName = $(this).attr('data-GenreName');
  PublisherName = $(this).attr('data-PublisherName');
  PhaseName = $(this).attr('data-PhaseName');
  Impression = $(this).attr('data-Impression');
  DITImp = $(this).attr('data-DITImp'); //alert(DITImp);
  Clicks = $(this).attr('data-Clicks');
  DitClicks = $(this).attr('data-DitClicks');
  Leads = $(this).attr('data-Leads');
  DITLeads = $(this).attr('data-DITLeads');
  Rate = $(this).attr('data-Rate');
  DITRate = $(this).attr('data-DITRate');
  Cost = $(this).attr('data-Cost');
  DITCost = $(this).attr('data-DITCost');
  Opens = $(this).attr('data-Opens');
  DITOpens = $(this).attr('data-DITOpens');

  $('#id').text(id);
  $('#LineItemIDCity').text(LineItemID);
  $('#lineItemNameCity').text(LineItemName);
  $('#Campaign_IDCity').text(Campaign_ID);
  $('#GenreNameCity').text(GenreName);
  $('#PublisherNameCity').text(PublisherName);


  $('#ImpressionCity').text(Impression);
  $('#DitIMPCity').text(DITImp);

  $('#ClicksCity').text(Clicks);
  $('#DitClicksCity').text(DitClicks);


  $('#LeadsCity').text(Leads);
  $('#DITLeadsCity').text(DITLeads);
  $('#CostCity').text(Cost);
  $('#DITCostCity').text(DITCost);
  $('#RateCity').text(Rate);
  $('#DITRateCity').text(DITRate);
  $('#OpensCity').text(Opens);
  $('#DITOpensCity').text(DITOpens);
  var CSRF_TOKEN=$("#token").val();
  $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
  $.ajax({
        type: 'POST',
        url: '../selectCreativeView',
        data: { "_token": CSRF_TOKEN,'lineitmeId':id},
        success: function (response) {
          $.each(response.card, function (key, value) {
            $(".cardview").append('<option value="'+value['CardName']+'" data-for="selected-publisher">'+value['CardName']+'</option>');
            $(".creativeview").append('<option value="'+value['CreativeName']+'" data-for="selected-publisher">'+value['CreativeName']+'</option>');

          });

        }
    });

});
//end of code

$(document).on('click', '.editLineItem', function() {

  LineItem_ID = $(this).attr('data-id');  //alert("LineItem_ID "+LineItem_ID);
  $('#LineItem_ID1').val(LineItem_ID);

  Campaign_ID = $(this).attr('data-Campaign_ID');
   $('#Campaign_ID1').val(Campaign_ID);

  LineItemID = $(this).attr('data-LineItemID');
   $('#LineItemID1').val(LineItemID);

  LineItemName = $(this).attr('data-LineItemName');
   $('#LineItemName1').val(LineItemName);

  //---------------------------------
  //genre value- name
  Genre_ID = $(this).attr('data-Genre_ID');
  GenreName = $(this).attr('data-GenreName');

  var genreArray=[Genre_ID,GenreName];
  $('#genreDetails').empty();
  $('#genreDetails').append($('<option>', {
        value: genreArray[0],
        text : genreArray[1]
  }));

  // publusher value- name
  Publisher_ID = $(this).attr('data-Publisher_ID');
  PublisherName = $(this).attr('data-PublisherName');

  var publisherArray=[Publisher_ID,PublisherName];
  $('#PublisherDetails').empty();
  $('#PublisherDetails').append($('<option>', {
        value: publisherArray[0],
        text : publisherArray[1]
  }));
 
  // <!-- =========================edit Genre-Publisher selection================================ -->
  var allGenreIDs=[];
  $(".allGenre_ID option").each(function(){
    var genreID= $(this).val()
    allGenreIDs.push(genreID);
  });
  Genre_ID = $(this).attr('data-Genre_ID');
  $('#Genre_ID1 option').each(function(){
    var option = $(this).val();
    if(Genre_ID==option){
      $(this).attr('selected', true);
    }
  });
 // // --------------------edit publisher----------------
  var CSRF_TOKEN=$("#token").val(); 
  $.ajaxSetup({
    headers: {
        'X-CSRF-Token': $('meta[name=_token]').attr('content')
    }
  });
  $('.selectedPublisher').empty();
  $.ajax({
    type: 'POST',
    url: '../selectPublisherGenreMap',
    data: { "_token": CSRF_TOKEN,'genre_id':Genre_ID},
    success: function (response ) {
      console.log(response);
      $(".selectedPublisher").append('<option value="" data-for="selected-Publisher">Select</option>');
      $.each(response.publisher, function (key, value) {

        $(".selectedPublisher").append('<option value="'+value.id+'" data-for="selected-Publisher"'+(value.PublisherName === PublisherName ? 'selected' : '')+'>'+value.PublisherName+'</option>');
      });
    }
  });
  //  <!-- =========================edit Genre-Publisher selection================================ -->
  //----------------------------------------------------------------------------------------------
  //complete array for phaseNames
  var AllPhaseIDs =[];
  $(".allPhaseIds option").each(function(){
    var PhaseID= $(this).val()
    AllPhaseIDs.push(PhaseID);
  });

  Phase_ID = $(this).attr('data-Phase_ID');
  $('#Phase_ID1 option').each(function(){
    var option = $(this).val();
    if(Phase_ID==option){
      $(this).attr('selected', true);
    }
  });
  //------------------------------------------------------------------------------------------------
  //complete array for MediumNames
  var AllMediumIDs =[];
  $(".allMediumIds option").each(function()
  {
    var MediumID= $(this).val();
    AllMediumIDs.push(MediumID);
  });

  Medium_ID = $(this).attr('data-Medium_ID');
   $("#Medium_ID1 option").each(function(){
    var optionM = $(this).val();
    if(Medium_ID==optionM){
      $(this).attr('selected', true);
    }
  });
  //----------------------------------------------------------------------------------------------
  var AllObjectiveIDs =[];
  $(".allObjectiveIds option").each(function()
  {
    var ObjectiveID= $(this).val();
    AllObjectiveIDs.push(ObjectiveID);
  });
  Objective_ID = $(this).attr('data-Objective_ID'); // alert('ObjId selected' + Objective_ID);
  $("#Objective_ID1 option").each(function(){
    var optionO = $(this).val();
    if(Objective_ID==optionO){
      $(this).attr('selected', true);
    }
  });
  //----------------------------------------------------------------------------------------------
  var AllDealTypeIDs =[];
  $(".allDealTypeIds option").each(function()
  {
    var DealTypeID= $(this).val();
    AllDealTypeIDs.push(DealTypeID);
  });
  DealType_ID = $(this).attr('data-DealType_ID');
  $("#DealType_ID1 option").each(function(){
    var optionD = $(this).val();
    if(DealType_ID==optionD){
      $(this).attr('selected', true);
    }
  });
  //----------------------------------------------------------------------------------------------
   var AllDeliveryModeIDs =[];
  $(".allDeliveryModeIds option").each(function()
  {
    var DeliveryModeID= $(this).val();
    AllDeliveryModeIDs.push(DeliveryModeID);
  });

  DeliveryMode_ID = $(this).attr('data-DeliveryMode_ID');
  //$('#DeliveryMode_ID1').text(DeliveryMode_ID);
   $("#DeliveryMode_ID1 option").each(function(){
    var optionDM = $(this).val();
    if(DeliveryMode_ID==optionDM){
      $(this).attr('selected', true);
    }
  });
  //----------------------------------------------------------------------------------------------
  var AllFormatTypeIDs =[];
  $(".allFormatTypeIds option").each(function()
  {
  var FormatTypeID= $(this).val();
    AllFormatTypeIDs.push(FormatTypeID);
  });

  FormatType_ID = $(this).attr('data-FormatType_ID');

  $("#FormatType_ID1 option").each(function(){
    var optionDM = $(this).val();

    //alert('formatType');
    //alert(optionDM);
    //alert(FormatType_ID);

    if(FormatType_ID==optionDM){
      $(this).attr('selected', true);
    }
  });
  // ===============================================
  
  //----------------------------------------------------------------------------------------------
// alert("aler");
  IsLocationBased = $(this).attr('data-IsLocationBased'); //alert(IsLocationBased);

  GoogleXCoord = $(this).attr('data-GoogleXCoord');
  $('#GoogleXCoord').val(GoogleXCoord);
  GoogleYCoord = $(this).attr('data-GoogleYCoord');
  $('#GoogleYCoord').val(GoogleYCoord);
  GoogleLocation = $(this).attr('data-GoogleLocation');
  $('#GlocationValue').text(GoogleLocation);
  $('#GoogleLocation1').val(GoogleLocation);
  GoogleRadius = $(this).attr('data-GoogleRadius');
  $('#GDistanceValue').text(GoogleRadius);
  $('#GoogleRadius1').val(GoogleRadius);
  // --cityLoc based----------------------

  cityINCLoc = $(this).attr('data-cityINCLoc'); //alet(cityINCLoc);
  cityEXLoc = $(this).attr('data-cityEXLoc'); //alert(cityEXLoc);

  CityINCLoc = [];
  $.each(JSON.parse(cityINCLoc), function(index,value) {
    CityINCLoc.push(value);
  });

  CityEXLoc = [];
  $.each(JSON.parse(cityEXLoc), function(index,value) {
    CityEXLoc.push(value);
  });

  // --------------------------------------------------------------
  // age all
  AgeRangeFrom = $(this).attr('data-AgeRangeFrom'); //alert(AgeRangeFrom);
  $('#AgeRangeFromE').val(AgeRangeFrom);
  AgeRangeTo = $(this).attr('data-AgeRangeTo'); //alert(AgeRangeTo);
  $('#AgeRangeToE').val(AgeRangeTo);

  // ----gender
  var AllGenderIDs =[];
  $(".allGenderIdds option").each(function()
  {
    var genderID= $(this).val();
    AllGenderIDs.push(genderID);
  });

  Gender_ID = $(this).attr('data-Gender_ID'); //alert(Gender_ID);

  $("#Gender_ID option").each(function(){
    var optionDM = $(this).val();
    if(Gender_ID==optionDM){
      $(this).attr('selected', true);
    }
  });
  // ----end gender

  // --------------------------------------------
  var AllSectionIDs =[];
  $(".allSectionIds option").each(function()
  {
    var sectionID= $(this).val();
    AllSectionIDs.push(sectionID);
  });

  Section_ID = $(this).attr('data-Section_ID'); //alert(Gender_ID);

  $("#Section_ID option").each(function(){
    var optionDM = $(this).val();
    if(Section_ID==optionDM){
      $(this).attr('selected', true);
    }
  });
  // --------------------------------------------
  var AllAdUnitIDs =[];
  $(".allAdUnitIds option").each(function()
  {
    var adUnitID= $(this).val();
    AllAdUnitIDs.push(adUnitID);
  });

  AdUnit_ID = $(this).attr('data-AdUnit_ID'); //alert(Gender_ID);

  $("#AdUnit_ID option").each(function(){
    var optionDM = $(this).val();
    if(AdUnit_ID==optionDM){
      $(this).attr('selected', true);
    }
  });
  //----------------------------------------------------------------------------------------------
  Impression = $(this).attr('data-Impression');
  $('#Impression1').val(Impression);
 
  Clicks = $(this).attr('data-Clicks');
  $('#Clicks1').val(Clicks);

  Visits = $(this).attr('data-Visits');
  $('#Visits1').val(Visits);

  Views = $(this).attr('data-Views');
  $('#Views1').val(Views);

  Engagement = $(this).attr('data-Engagement');
  $('#Engagement1').val(Engagement);

  Leads = $(this).attr('data-Leads');
  $('#Leads1').val(Leads);

  NetRate = $(this).attr('data-NetRate');
  $('#NetRate1').val(NetRate);

  NetCost = $(this).attr('data-NetCost');
  $('#NetCost1').val(NetCost);

  Reach = $(this).attr('data-Reach');
  $('#Reach1').val(Reach);

  Installs = $(this).attr('data-Installs');
  $('#Installs1').val(Installs);

  Spots = $(this).attr('data-Spots');
  $('#Spots1').val(Spots);

  SMS = $(this).attr('data-SMS');
  $('#SMS1').val(SMS);

  Mailers = $(this).attr('data-Mailers');
  $('#Mailers1').val(Mailers);

  $('#PIDataInputTypeID1').empty();
  $('#PCDataInputTypeID1').empty();
  $('#PVsDataInputTypeID1').empty();
  $('#PVwDataInputTypeID1').empty();
  $('#PEDataInputTypeID1').empty();
  $('#PLDataInputTypeID1').empty();
  $('#PNRDataInputTypeID1').empty();
  $('#PNCDataInputTypeID1').empty();
  $('#PRDataInputTypeID1').empty();
  $('#PInDataInputTypeID1').empty();
  $('#PSpDataInputTypeID1').empty();
  $('#PSmDataInputTypeID1').empty();
  $('#PMDataInputTypeID1').empty();
  //==========================================

  DITImp = $(this).attr('data-DITImp'); //alert(DITImp);
  DITClicks = $(this).attr('data-DITClicks');
  DITVisits = $(this).attr('data-DITVisits');
  DITViews = $(this).attr('data-DITViews');
  DITEngagement = $(this).attr('data-DITEngagement');
  DITLeads = $(this).attr('data-DITLeads');
  DITNetRate = $(this).attr('data-DITNetRate');
  DITNetCost = $(this).attr('data-DITNetCost');
  DITReach = $(this).attr('data-DITReach');
  DITInstalls = $(this).attr('data-DITInstalls');
  DITSpots = $(this).attr('data-DITSpots');
  DITSMS = $(this).attr('data-DITSMS');
  DITMailers = $(this).attr('data-DITMailers');
  //==========================================

  var CSRF_TOKEN=$("#token").val();
  var selectedOption = $(this).val();

  $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });

  var getUrl = window.location;

  console.log(getUrl); 

  $.ajax({
    type: 'POST',
    url: '../selectDataTypePublisherMap',
    data: { "_token": CSRF_TOKEN,'p_id':Publisher_ID},
    success: function (response) {
      console.log(response);
      var arr = ["4","6","7","8","9","10","11"];
      var api = [];
      var nonApi = [];
      $.each(response.dataInputType, function (key, value) {
            if(jQuery.inArray(value.id,arr) != -1){
                api.push(response.dataInputType[key]);
            } else {
                nonApi.push(response.dataInputType[key]);
            }
      });

      var Pid = "";
      var Pid1 = "";
      var Pcd = "";
      var Pcd1 = "";
      var PVs = '';
      var PVs1 = '';
      var PViw = '';
      var PViw1 = '';
      var PLead = '';
      var PLead1 = '';
      var PEng = '';
      var PEng1 = '';
      var PCost = '';
      var PCost1 = '';
      var PRate = '';
      var PRate1 = '';
      var PReach = '';
      var PReach1 = '';
      var PInstall = '';
      var PInstall1= '';
      var PSpot = '';
      var PSpots1 = '';
      var PSMS = '';
      var PSMS1 = '';
      var PMailers = '';
      var PMailers1 = '';
  
      if(nonApi.length>0){
        console.log("pop");
        $.each(nonApi, function (key, value) {
          Pid = Pid+'<option value="'+value.id+'" data-for="selected-PIDataInputTypeID"'+(value.DataInputTypeName === DITImp ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
        
          Pcd = Pcd+'<option value="'+value.id+'" data-for="selected-PCDataInputTypeID"'+(value.DataInputTypeName === DITClicks ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
          PVs = PVs+'<option value="'+value.id+'" data-for="selected-PVsDataInputTypeID"'+(value.DataInputTypeName === DITVisits ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
          PViw = PViw+'<option value="'+value.id+'" data-for="selected-PVwDataInputTypeID"'+(value.DataInputTypeName === DITViews ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
          PLead = PLead+'<option value="'+value.id+'" data-for="selected-PLDataInputTypeID"'+(value.DataInputTypeName === DITLeads ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
          PEng = PEng+'<option value="'+value.id+'" data-for="selected-PEDataInputTypeID"'+(value.DataInputTypeName === DITEngagement ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
          PRate = PRate+'<option value="'+value.id+'" data-for="selected-PNRDataInputTypeID"'+(value.DataInputTypeName === DITNetRate ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
          PCost = PCost+'<option value="'+value.id+'" data-for="selected-PNCDataInputTypeID"'+(value.DataInputTypeName === DITNetCost ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
          PReach = PReach+'<option value="'+value.id+'" data-for="selected-PRDataInputTypeID"'+(value.DataInputTypeName === DITReach ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
          PInstall = PInstall+'<option value="'+value.id+'" data-for="selected-PInDataInputTypeID"'+(value.DataInputTypeName === DITInstalls ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
          PSpot = PSpot+'<option value="'+value.id+'" data-for="selected-PSpDataInputTypeID"'+(value.DataInputTypeName === DITSpots ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
          PSMS = PSMS+'<option value="'+value.id+'" data-for="selected-PSmDataInputTypeID"'+(value.DataInputTypeName === DITSMS ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
          PMailers = PMailers+'<option value="'+value.id+'" data-for="selected-PMDataInputTypeID"'+(value.DataInputTypeName === DITMailers ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';

        });
      }    

      if(api.length>0){
          Pid1 = '<optgroup label="API">';
          Pcd1 = '<optgroup label="API">';
          PVs1 = '<optgroup label="API">';
          PViw1 = '<optgroup label="API">';
          PLead1 = '<optgroup label="API">';
          PEng1 = '<optgroup label="API">';
          PRate1 = '<optgroup label="API">';
          PCost1 = '<optgroup label="API">';
          PReach1 = '<optgroup label="API">';
          PInstall1 = '<optgroup label="API">';
          PSpots1 = '<optgroup label="API">';
          PSMS1 = '<optgroup label="API">';
          PMailers1 = '<optgroup label="API">';
          
          $.each(api, function (key, value) {
              Pid1 = Pid1+'<option value="'+value.id+'" data-for="selected-PIDataInputTypeID"'+(value.DataInputTypeName === DITImp ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
            
              Pcd1 = Pcd1+'<option value="'+value.id+'" data-for="selected-PCDataInputTypeID"'+(value.DataInputTypeName === DITClicks ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
              PVs1 = PVs1+'<option value="'+value.id+'" data-for="selected-PVsDataInputTypeID"'+(value.DataInputTypeName === DITVisits ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
              PViw1 = PViw1+'<option value="'+value.id+'" data-for="selected-PVwDataInputTypeID"'+(value.DataInputTypeName === DITViews ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
              PLead1 = PLead1+'<option value="'+value.id+'" data-for="selected-PLDataInputTypeID"'+(value.DataInputTypeName === DITLeads ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
              PEng1 = PEng1+'<option value="'+value.id+'" data-for="selected-PEDataInputTypeID"'+(value.DataInputTypeName === DITEngagement ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
              PRate1 = PRate1+'<option value="'+value.id+'" data-for="selected-PNRDataInputTypeID"'+(value.DataInputTypeName === DITNetRate ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
              PCost1 = PCost1+'<option value="'+value.id+'" data-for="selected-PNCDataInputTypeID"'+(value.DataInputTypeName === DITNetCost ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
              PReach1 = PReach1+'<option value="'+value.id+'" data-for="selected-PRDataInputTypeID"'+(value.DataInputTypeName === DITReach ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
              PInstall1 = PInstall1+'<option value="'+value.id+'" data-for="selected-PInDataInputTypeID"'+(value.DataInputTypeName === DITInstalls ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
              PSpots1 = PSpots1+'<option value="'+value.id+'" data-for="selected-PSpDataInputTypeID"'+(value.DataInputTypeName === DITSpots ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
              PSMS1 = PSMS1+'<option value="'+value.id+'" data-for="selected-PSmDataInputTypeID"'+(value.DataInputTypeName === DITSMS ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
              PMailers1 = PMailers1+'<option value="'+value.id+'" data-for="selected-PMDataInputTypeID"'+(value.DataInputTypeName === DITMailers ? 'selected' : '')+'>'+value.DataInputTypeName+'</option>';
              
          });

          Pid1 = Pid1+'</optgroup>';
          Pcd1 = Pcd1+'</optgroup>';
          PVs1 = PVs1+'</optgroup>';
          PViw1 = PViw1+'</optgroup>';
          PLead1 = PLead1+'</optgroup>';
          PEng1 = PEng1+'</optgroup>';
          PRate1 = PRate1+'</optgroup>';
          PCost1 = PCost1+'</optgroup>';
          PReach1 = PReach1+'</optgroup>';
          PInstall1 = PInstall1+'</optgroup>';
          PSpots1 = PSpots1+'</optgroup>';
          PSMS1 = PSMS1+'</optgroup>';
          PMailers1 = PMailers1+'</optgroup>';
      }
      //console.log(PSpots1);
      var PID = Pid+Pid1;  
      var PCD = Pcd+Pcd1;
      var PVisit = PVs+PVs1;
      var PView = PViw+PViw1;
      var PLeads = PLead+PLead1;
      var PEngage = PEng+PEng1;
      var PRates = PRate+PRate1;
      var PCosts = PCost+PCost1;
      var PReachs = PReach+PReach1;
      var PInstalls = PInstall+PInstall1;
      var PSpots = PSpot+PSpots1;
      var Psms = PSMS+PSMS1;
      var PMailers = PMailers+PMailers1;
      // var PCO = PCO+PCO1;
      // var PRA = PRA+PRA1;
      // var POpen = POpen+POpen1;  
      
      $(".PIDataInputTypeID").append('<option value="" data-for="selected-PIDataInputTypeID">Select</option>').append(PID);
      $(".PCDataInputTypeID").append('<option value="" data-for="selected-PCDataInputTypeID">Select</option>').append(PCD);
      $(".PVsDataInputTypeID").append('<option value="" data-for="selected-PVsDataInputTypeID">Select</option>').append(PVisit);
      $(".PVwDataInputTypeID").append('<option value="" data-for="selected-PVwDataInputTypeID">Select</option>').append(PView);
      $(".PLDataInputTypeID").append('<option value="" data-for="selected-PLDataInputTypeID">Select</option>').append(PLeads);
      $(".PEDataInputTypeID").append('<option value="" data-for="selected-PEDataInputTypeID">Select</option>').append(PEngage);
      $(".PNRDataInputTypeID").append('<option value="" data-for="selected-PNRDataInputTypeID">Select</option>').append(PRates);
      $(".PNCDataInputTypeID").append('<option value="" data-for="selected-PNCDataInputTypeID">Select</option>').append(PCosts);
      $(".PRDataInputTypeID").append('<option value="" data-for="selected-PRDataInputTypeID">Select</option>').append(PReachs);
      $(".PInDataInputTypeID").append('<option value="" data-for="selected-PInDataInputTypeID">Select</option>').append(PInstalls);
      $(".PSpDataInputTypeID").append('<option value="" data-for="selected-PSpDataInputTypeID">Select</option>').append(PSpots);
      $(".PSmDataInputTypeID").append('<option value="" data-for="selected-PSmDataInputTypeID">Select</option>').append(Psms);
      $(".PMDataInputTypeID").append('<option value="" data-for="selected-PMDataInputTypeID">Select</option>').append(PMailers);
    }
  });    
  //----------------------------------------------------------------------------------------------
  var CSRF_TOKEN=$("#token").val();
  var selectedOption = $(this).val();

  $.ajaxSetup({
    headers: {
        'X-CSRF-Token': $('meta[name=_token]').attr('content')
    }
  });
  //--------------------------------------------------------
  $('.objectiveValuess').empty();
  var selectedOption =Objective_ID; //alert(selectedOption);

  $.ajax({
    type: 'POST',
    url: '../selectObjectiveObjectiveMap',
    data: { "_token": CSRF_TOKEN,'objective_id':selectedOption},
    success: function (response ) {

      console.log(response);
     //$(".objectiveValuess").append('<option value="" data-for="selected-objectiveValues">Select</option>');
      $.each(response.objectiveValue, function (key, value) {

       $(".objectiveValuess").append('<option value="'+value.id+'" data-for="selected-objectiveValues">'+value.ObjectiveValueName+'</option>');
      });
    }
  });
  //----------------------------------------------------------------------------------------------

  $('.formatss').empty();
  // $('.formatValue').empty();
  var selectedOption = FormatType_ID;
  var selectedFormat = $(this).attr('data-Format_ID');;
  //alert('selectedFormat');
  //alert(selectedFormat);
  $.ajax({
        type: 'POST',
        url: '../selectFormatTypeFormatMap',
        data: { "_token": CSRF_TOKEN,'formatType_id':selectedOption},
        success: function (response ) {

          $.each(response.format, function (key, value) {
            if(selectedFormat==value.id)
            {
              $(".formatss").append('<option selected value="'+value.id+'">'+value.FormatName+'</option>');
            }
            else{
              $(".formatss").append('<option value="'+value.id+'">'+value.FormatName+'</option>');   
            }       
          });

        }
  });
  //----------------------------------------------------------------------------------------------

  FormatValue = $(this).attr('data-FormatValue'); //alert();
  $('#FormatValue1').val(FormatValue);
   // ------get City location
    if(IsLocationBased)
    {
      var selectedOption =LineItem_ID;

      $.ajax({
        type: 'POST',
        url: '../getCityLocation',
        data: { "_token": CSRF_TOKEN,'LineItem_ID':selectedOption},
        success: function (response ) {

         console.log(response);

           $.each(response.getIncLocation, function (key, value) {
             $(".incLocValue").append('<input type="text" name="incLocId[]" value="'+value.id+'" hidden >'+value.LocationName +'</br>');
           });

            $.each(response.getExcLocation, function (key, value) {
             $(".excLocValue").append('<input type="text" name="excLocId[]" value="'+value.id+'" hidden>'+value.LocationName +'</br>');
           });
        }
      });
    }

    Targeting_ID =   $(this).attr('data-Targeting_ID'); //alert("targetingValues"+Targeting_ID);

    Targeting_ID1 =   $(this).attr('data-Targeting_ID').replace("[", " ");
    Targeting_ID2 =  Targeting_ID1.replace("]", " ");

    var targetingArray = JSON.parse("[" + Targeting_ID2 + "]");
    console.log($.type(targetingArray));
    console.log(targetingArray);

    $.each(targetingArray, function( index, value ) {
      $('.targetSearch'+value).prop("selected", true);
    });
    // ----------------------------------------------------------------------------
    TargetingValue_ID = $(this).attr('data-TargetingValue_ID');
    TargetingValue_ID1 =   $(this).attr('data-TargetingValue_ID').replace("[", " ");
    TargetingValue_ID2 =  TargetingValue_ID1.replace("]", " ");

    var targetingValueArray = JSON.parse("[" + TargetingValue_ID2 + "]");

     $.each(targetingValueArray, function( index, value ) {
      $('.targetValueSearch'+value).prop("selected", true);
    });

    if(Targeting_ID)
    {
      // console.log(Targeting_ID);
      var selectedOption =LineItem_ID;
      $.ajax({
        type: 'POST',
        url: '../getTargetingData',
        data: { "_token": CSRF_TOKEN,'LineItem_ID':selectedOption, 'Targeting_ID': Targeting_ID},
        success: function (response ) {

        console.log(response);

        $.each(response.getTargetingData, function (key, value)
        {
          if(jQuery.inArray(value.id, targetingValueArray) !== -1){
            $(".TargetingValuesEdit").append('<option class="targetValueSearch'+value.id+'" value="'+value.id+'" data-for="selected-TargetingValue_ID" selected="" >'+value.TargetingValueName+'</option>');
          }else{
            $(".TargetingValuesEdit").append('<option class="targetValueSearch'+value.id+'" value="'+value.id+'" data-for="selected-TargetingValue_ID"  >'+value.TargetingValueName+'</option>');
          }

        });

        // TargetingValue_ID = $(this).attr('data-TargetingValue_ID');
        // TargetingValue_ID1 =   $(this).attr('data-TargetingValue_ID').replace("[", " ");
        // TargetingValue_ID2 =  TargetingValue_ID1.replace("]", " ");

        // var targetingValueArray = JSON.parse("[" + TargetingValue_ID2 + "]");

        //  $.each(targetingValueArray, function( index, value ) {
        //   $('.targetValueSearch'+value).prop("selected", true);
        // });
      }
    });
  }
});

//edit citybank lineItem
$(document).on('click', '.editLineItemCityBank', function() {

  $('#cityLineItem_ID1').empty();
  $('#cityCampaign_ID1').empty();
  $('#cityLineItemID1').empty();
  $('#cityLineItemName1').empty();
  $('#citygenreDetails').empty();
  $('#cityPublisherDetails').empty();

  LineItem_ID = $(this).attr('data-cityid');  //alert("LineItem_ID "+LineItem_ID);
  $('#cityLineItem_ID1').val(LineItem_ID);

  Campaign_ID = $(this).attr('data-cityCampaign_ID');
   $('#cityCampaign_ID1').val(Campaign_ID);

  LineItemID = $(this).attr('data-cityLineItemID');
   $('#cityLineItemID1').val(LineItemID);

  LineItemName = $(this).attr('data-cityLineItemName');
   $('#cityLineItemName1').val(LineItemName);

  // card_creative=$(this).attr('cardCreative');
  // alert(card_creative);
  //---------------------------------
  //genre value- name
  Genre_ID = $(this).attr('data-cityGenre_ID');
  GenreName = $(this).attr('data-cityGenreName');

  var genreArray=[Genre_ID,GenreName];
  $('#citygenreDetails').append($('<option>', {
        value: genreArray[0],
        text : genreArray[1]
  }));

  // publusher value- name
  Publisher_ID = $(this).attr('data-cityPublisher_ID');
  PublisherName = $(this).attr('data-cityPublisherName');

  var publisherArray=[Publisher_ID,PublisherName];
  $('#cityPublisherDetails').append($('<option>', {
        value: publisherArray[0],
        text : publisherArray[1]
  }));
});
//end of edit cityBank Lineitem

// ----on ccheck of selection---------

//function checkValueDitForEdit( valClass, ditClass){

  // $(document).on('blur', valClass, function() {
  //   var val=$(valClass).val(); 
  //   if(val){ 
  //     $(ditClass).prop("required", "true");
  //   }
  // }); 
  // $(document).on('blur', ditClass, function() {    
  //   var dit=$(ditClass).val();
  //   var val=$(valClass).val(); 
  //   if(dit && !val){ 
  //     alert("Please Select Values for Metrix ");
  //   }
  // }); 
// }

// checkValueDitForEdit("imp_val", "imp_val_dit");
// ======================================================

function CheckValueDit()
{
   // var Impression=$('.imp_val').val();
   // var impression_dit=$('.imp_val_dit').val();
   // if(Impression!="" )
   // {
   //   $('.imp_val_dit').prop("required", true);
   // } 

   // if(impression_dit!="")
   // {
   //    $('.imp_val').prop("required", true);
   // }

   // if(Impression=="" && impression_dit=="")
   // {
   //      $('.imp_val_dit').removeAttr('required');
   //      $('.imp_val').removeAttr('required');
   // }
}
//======================================================

$(document).on('blur', '.imp_val', function() {
  var val=$('.imp_val').val();
  var dit=$('.imp_val_dit').val();

  if(val!=""){ 
    $('.imp_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.imp_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.imp_val_dit').removeAttr('required');
   $('.imp_val').removeAttr('required');
  }
  
}); 
$(document).on('blur', '.imp_val_dit', function() {    
  var dit=$('.imp_val_dit').val();
  var val=$('.imp_val').val(); 

  if(val!=""){ 
    $('.imp_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.imp_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.imp_val_dit').removeAttr('required');
   $('.imp_val').removeAttr('required');
  }
}); 
  

//=================Click=============================== 
$(document).on('blur', '.click_val', function() {
  var val=$('.click_val').val();
  var dit=$('.click_val_dit').val();

  if(val!=""){ 
    $('.click_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.click_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.click_val_dit').removeAttr('required');
   $('.click_val').removeAttr('required');
  }
}); 
$(document).on('blur', '.click_val_dit', function() {    
  var val=$('.click_val').val();
  var dit=$('.click_val_dit').val();

  if(val!=""){ 
    $('.click_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.click_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.click_val_dit').removeAttr('required');
   $('.click_val').removeAttr('required');
  }
 }); 
// ================visit================================
$(document).on('blur', '.visit_val', function() {
  var val=$('.visit_val').val();
  var dit=$('.visit_val_dit').val();

  if(val!=""){ 
    $('.visit_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.visit_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.visit_val_dit').removeAttr('required');
   $('.visit_val').removeAttr('required');
  }
}); 
$(document).on('blur', '.visit_val_dit', function() {    
  var val=$('.visit_val').val();
  var dit=$('.visit_val_dit').val();

  if(val!=""){ 
    $('.visit_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.visit_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.visit_val_dit').removeAttr('required');
   $('.visit_val').removeAttr('required');
  }
 }); 
// =================view===============================

$(document).on('blur', '.view_val', function() {
  var val=$('.view_val').val();
  var dit=$('.view_val_dit').val();

  if(val!=""){ 
    $('.view_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.view_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.view_val_dit').removeAttr('required');
   $('.view_val').removeAttr('required');
  }
}); 
$(document).on('blur', '.view_val_dit', function() {

  var val=$('.view_val').val();
  var dit=$('.view_val_dit').val();

  if(val!=""){ 
    $('.view_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.view_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.view_val_dit').removeAttr('required');
   $('.view_val').removeAttr('required');
  }
 }); 
// ===================engagement==============================
$(document).on('blur', '.eng_val', function() {
  var val=$('.eng_val').val();
  var dit=$('.eng_val_dit').val();

  if(val!=""){ 
    $('.eng_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.eng_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.eng_val_dit').removeAttr('required');
   $('.eng_val').removeAttr('required');
  }
}); 
$(document).on('blur', '.eng_val_dit', function() {    

  var val=$('.eng_val').val();
  var dit=$('.eng_val_dit').val();

  if(val!=""){ 
    $('.eng_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.eng_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.eng_val_dit').removeAttr('required');
   $('.eng_val').removeAttr('required');
  }
 }); 
// ==================lead================================
$(document).on('blur', '.lead_val', function() {
  var val=$('.lead_val').val();
  var dit=$('.lead_val_dit').val();

  if(val!=""){ 
    $('.lead_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.lead_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.lead_val_dit').removeAttr('required');
   $('.lead_val').removeAttr('required');
  }
}); 
$(document).on('blur', '.lead_val_dit', function() {    
  var val=$('.lead_val').val();
  var dit=$('.lead_val_dit').val();

  if(val!=""){ 
    $('.lead_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.lead_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.lead_val_dit').removeAttr('required');
   $('.lead_val').removeAttr('required');
  }
 }); 
// ==================netRate=============================

$(document).on('blur', '.netRate_val', function() {
   var val=$('.netRate_val').val();
  var dit=$('.netRate_val_dit').val();

  if(val!=""){ 
    $('.netRate_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.netRate_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.netRate_val_dit').removeAttr('required');
   $('.netRate_val').removeAttr('required');
  }
}); 
$(document).on('blur', '.netRate_val_dit', function() {    
  var val=$('.netRate_val').val();
  var dit=$('.netRate_val_dit').val();

  if(val!=""){ 
    $('.netRate_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.netRate_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.netRate_val_dit').removeAttr('required');
   $('.netRate_val').removeAttr('required');
  }
 }); 
// =================netCost========================

$(document).on('blur', '.netCost_val', function() {
  var val=$('.netCost_val').val();
  var dit=$('.netCost_val_dit').val();

  if(val!=""){ 
    $('.netCost_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.netCost_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.netCost_val_dit').removeAttr('required');
   $('.netCost_val').removeAttr('required');
  }
}); 
$(document).on('blur', '.netCost_val_dit', function() {    
  var val=$('.netCost_val').val();
  var dit=$('.netCost_val_dit').val();

  if(val!=""){ 
    $('.netCost_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.netCost_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.netCost_val_dit').removeAttr('required');
   $('.netCost_val').removeAttr('required');
  }
 }); 
// =================reach================================

$(document).on('blur', '.reach_val', function() {
  var val=$('.reach_val').val();
  var dit=$('.reach_val_dit').val();

  if(val!=""){ 
    $('.reach_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.reach_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.reach_val_dit').removeAttr('required');
   $('.reach_val').removeAttr('required');
  }
}); 
$(document).on('blur', '.reach_val_dit', function() {    
  var val=$('.reach_val').val();
  var dit=$('.reach_val_dit').val();

  if(val!=""){ 
    $('.reach_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.reach_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.reach_val_dit').removeAttr('required');
   $('.reach_val').removeAttr('required');
  }
 }); 
// ===================install============================

$(document).on('blur', '.install_val', function() {
  var val=$('.install_val').val();
  var dit=$('.install_val_dit').val();

  if(val!=""){ 
    $('.install_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.install_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.install_val_dit').removeAttr('required');
   $('.install_val').removeAttr('required');
  }
}); 
$(document).on('blur', '.install_val_dit', function() {    
  var val=$('.install_val').val();
  var dit=$('.install_val_dit').val();

  if(val!=""){ 
    $('.install_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.install_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.install_val_dit').removeAttr('required');
   $('.install_val').removeAttr('required');
  }
 }); 
// =================spot===============================


$(document).on('blur', '.spot_val', function() {
  var val=$('.spot_val').val();
  var dit=$('.spot_val_dit').val();

  if(val!=""){ 
    $('.spot_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.spot_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.spot_val_dit').removeAttr('required');
   $('.spot_val').removeAttr('required');
  }
}); 
$(document).on('blur', '.spot_val_dit', function() {    
  var val=$('.spot_val').val();
  var dit=$('.spot_val_dit').val();

  if(val!=""){ 
    $('.spot_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.spot_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.spot_val_dit').removeAttr('required');
   $('.spot_val').removeAttr('required');
  }
 }); 
// =================sms=========================

$(document).on('blur', '.sms_val', function() {
  var val=$('.sms_val').val();
  var dit=$('.sms_val_dit').val();

  if(val!=""){ 
    $('.sms_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.sms_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.sms_val_dit').removeAttr('required');
   $('.sms_val').removeAttr('required');
  }
}); 
$(document).on('blur', '.sms_val_dit', function() {    
  var val=$('.sms_val').val();
  var dit=$('.sms_val_dit').val();

  if(val!=""){ 
    $('.sms_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.sms_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.sms_val_dit').removeAttr('required');
   $('.sms_val').removeAttr('required');
  }
 }); 
// ================mailer==================================

$(document).on('blur', '.mailer_val', function() {
  var val=$('.mailer_val').val();
  var dit=$('.mailer_val_dit').val();

  if(val!=""){ 
    $('.mailer_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.mailer_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.mailer_val_dit').removeAttr('required');
   $('.mailer_val').removeAttr('required');
  }
}); 
$(document).on('blur', '.mailer_val_dit', function() {    
  var val=$('.mailer_val').val();
  var dit=$('.mailer_val_dit').val();

  if(val!=""){ 
    $('.mailer_val_dit').prop("required", "true");
  }
  if(dit!=""){ 
    $('.mailer_val').prop("required", "true");
  }
  if(val=="" && dit=="")
  {
   $('.mailer_val_dit').removeAttr('required');
   $('.mailer_val').removeAttr('required');
  }
 }); 
// ======================================================



