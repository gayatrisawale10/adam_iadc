@extends('layouts.appResource')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Create Brand : {{$getClientDetails->ClientName}}</h2>
            </div>
        </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(array('route' =>array('ClientBrand.storeBrand',$c_id),'method'=>'POST')) !!}
         @include('ClientBrand.form')
    {!! Form::close() !!}
	</div>
	
@endsection
