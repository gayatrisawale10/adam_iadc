<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Users;
use App\UserRole;
use Redirect;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();


        $role = UserRole::where('id', $user->UserRole_ID)->first();

        Session::put('getRole', $role->UserRoleName);

        //dd($role);
        //dd($role->UserRoleName);
        return Redirect::to($role->URL);
    }
}
