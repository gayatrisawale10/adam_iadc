 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="cache-control" content="private, max-age=0, no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">

	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="shortcut icon" href="./image/favicon.ico" type="image/x-icon">
	<link rel="icon" href="./image/favicon.ico" type="image/x-icon">
<!-- 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
   
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'IADC') }}</title>

    <!-- Styles -->
    <link href="../css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    @if(Auth::user())

    <script type="text/javascript">
        window.location = "{{url('/Campaign')}}";//here double curly bracket
    </script>

    @endif
	<style>
	.my_logo{
		
		height:250px;
		width:250px;
		margin-left:75px;	
	}
	.job_header {
		    margin-left: 375px;
			font-size: 60px;
			/*position: absolute;*/
			
	}
	.job_logo{
	/*margin-left:25px;*/
	margin-top:70px;	
	}
	.margin-top-20{
		margin-top: 20px;
	}
	</style>
</head>
<body>
    <div id="app">
	   <nav class="navbar navbar-static-top margin-top-20">
            <div class="container">
    			<div class='job_logo'>
    		      <!-- <img src='' width="80" height="80">	 -->
    		      <strong class='job_header'> <img class="my_logo" src='image/logo_final.png'> </strong>
    		    </div> 
                <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <!--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>-->
                        <!-- Branding Image -->
                        <!--<a class="navbar-brand" href="{{ url('/') }}">
                            {{ config('app.name', 'Laravel') }}
                        </a> -->
                </div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">&nbsp;</ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                          <!--  <li><a href="{{ url('/login') }}">Login</a></li> 
                            <li><a href="{{ url('/register') }}">Register</a></li> -->
                        @else
							<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('../logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
			</nav>
			<div class="container">
        @yield('content')
    </div>
	</div>
<div class="navbar-fixed-bottom pull-left navbar-default navbar-collapse" style="padding:5px 0px; border-top:1px solid #ddd;">
        <strong> &nbsp; &nbsp;Powered by &nbsp;</strong><a href="http://www.e-stonetech.com" target="_blank"> e-Stone Information Technology Pvt. Ltd. &copy; 2016-17</a>
    
		
		</div>
    <!-- Scripts -->
    <script src="../js/app.js"></script>
</body>
</html>
