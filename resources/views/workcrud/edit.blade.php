@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>update Type of Work</h2>
            </div>
            <!-- <div class="pull-right margin-top-20">
                <a class="btn btn-primary" href="{{ route('workcrud.index') }}"> Back</a>
            </div> -->
        </div>
    
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($workedit, ['method' => 'PATCH','route' => ['workcrud.update', $workedit->work_id]]) !!}
        @include('workcrud.form')
    {!! Form::close() !!}
	
	</div>
@endsection