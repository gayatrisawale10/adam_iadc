@extends('layouts.appResource')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Update Campaign Details</h2>
            </div>
            
        </div>
    
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::model($Campaign_edit, ['method' => 'PATCH','route' => ['Campaign.update', $Campaign_edit->id]]) !!}
        @include('Campaign.form')
    {!! Form::close() !!}
	
	</div>
@endsection