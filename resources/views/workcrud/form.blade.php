<div class="row col-md-6">
	 <div class="col-xs-12 col-sm-12 col-md-12">
	  <div class="form-group">
                <strong>Type Of Work:</strong>
			</div>
	        <div class="form-group">
	           {!! Form::text('work', null, array('placeholder' => 'Type of Work','class' => 'form-control')) !!}
	        </div>
	 </div>
	 <div class="col-xs-12 col-sm-12 col-md-12">
	  <div class="form-group">
                <strong>Completion Days:</strong>
			</div>
	        <div class="form-group">
	         {!! Form::text('completion_days', null, array('placeholder' => 'Completion Days',
	         'class' =>'form-control','onkeypress'=>'return isNumber(event)')) !!}
	        </div>
	 </div>
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary pull-left">Save</button>
			 <div class="pull-left" style="padding-left:20px;">
                <a class="btn btn-primary" href="{{ route('workcrud.index') }}"> Back</a>
            </div>
    </div>
</div>
<script type = "text/javascript">


function isNumber(evt) 
{
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) 
    {
        return false;
    }
    return true;
}

</script>
