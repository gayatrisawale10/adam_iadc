<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseBatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CourseBatch', function (Blueprint $table) {
            $table->increments('CourseBatch_ID');
            $table->string('CourseBatchName');
            $table->integer('Course_ID')->unsigned();;
            $table->integer('TrainingCenter_ID')->unsigned();
            $table->integer('CourseStatusType_ID')->unsigned();
            $table->date('StartDate');
            $table->date('EndDate');
            $table->string('Venue');
            $table->integer('Capacity');
            $table->timestamps();
        });
  
       Schema::table('CourseBatch', function($table) {
       $table->foreign('Course_ID')->references('Course_ID')->on('Course');

   });
   
    Schema::table('CourseBatch', function($table) {
       $table->foreign('TrainingCenter_ID')->references('TrainingCenter_ID')->on('Training_center');

   });
    Schema::table('CourseBatch', function($table) {
       $table->foreign('CourseStatusType_ID')->references('CourseStatusType_ID')->on('CreateCourseStatusType');

   });

    
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CourseBatch');
    }
}
