<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <script src="../../js/app.js"></script>

    <script data-require="jquery@*" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script data-require="datatables@*" data-semver="1.10.12" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

    <link data-require="datatables@*" data-semver="1.10.12" rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" /> -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <script src="../../js/sweetalert.js"></script>
    <link href="../../css/sweetalert.css" rel="stylesheet">
   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!--     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

    <!-- <link href="{{asset('../css/bootstrap_v3_0_3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('../css/font-awesome-4.7.0.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('../css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" /> -->
    <link href="{{asset('../css/customStyles.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('../css/customColours.css')}}" rel="stylesheet" type="text/css" />
    <!-- <link href="{{asset('../css/sweetalert.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('../css/sweetalert.min.css')}}" rel="stylesheet" type="text/css" />
 -->
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'IADC') }}</title>

    <script  type="text/javascript">
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <style type="text/css">

        .job_header {
                margin-left: 375px;
                font-size: 60px;
                /*position: absolute;*/

        }
        .job_logo{
            padding-top:10px;
        }
        .margin-top-20{
            margin-top: 20px;
        }
        .navbar-fixed-bottom, .navbar-fixed-top {
        width:100%;
        /* position: relative; */
        margin-top:30px;
        }

        th {
            text-align:center;
        }

        .btn-default.btn-on-3.active{background-color:#337ab7;color: #fff;}
        .btn-default.btn-off-3.active{background-color: #337ab7;color: #fff;}
        
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header"></div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                       <img class="home_logo_main" src="{{asset('images/iadc-logo.png')}}" height="50">
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->                        
                        @if(Auth::user()->UserRole_ID == 5 )
                        <li><a href="{{ url('/adamPublisherHome') }}">Publisher</a></li>
                        @endif
                        
                        <li class="dropdown">
                            <a class=" dropdown-toggle" type="button" data-toggle="dropdown">{{ Auth::user()['name'] }}<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a  href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>                          
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>    
        </nav>
        <div class="container cc_tableContainer">
            @yield('content')
        </div>
    </div>
    <!-- <div class="navbar-fixed-bottom pull-left navbar-default navbar-collapse" style="padding:-100px 0px; border-top:1px solid #ddd;">
         <strong> &nbsp; &nbsp;Powered by &nbsp;</strong><a href="http://www.e-stonetech.com" target="_blank"> e-Stone Information Technology Pvt. Ltd. &copy; 2016-17</a>

    </div> -->
</body>

    <script data-require="jquery@*" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script data-require="datatables@*" data-semver="1.10.12" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="../../js/dataTables.rowReorder.js"></script>
    <link data-require="datatables@*" data-semver="1.10.12" rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="//cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script  type="text/javascript">
            $(document).ready( function ()
            {
                $.noConflict();
                $('#sample_editable_1').css('border-bottom', 'none');
                $('#sample_editable_1').DataTable({
                    "bProcessing" : true,
                    "bPaginate" : true,
                    "pagingType": "simple",
                    "aaSorting": [],

                });
            });
    </script>
    <script src="https://code.jquery.com/jquery-1.9.1.js"  type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"  type="text/javascript"></script>
</html>
