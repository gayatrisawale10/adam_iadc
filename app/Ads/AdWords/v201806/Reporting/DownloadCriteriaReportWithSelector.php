<?php
/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\Ads\AdWords\v201806\Reporting;

use autoload;

use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\Reporting\v201806\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201806\ReportDefinition;
use Google\AdsApi\AdWords\Reporting\v201806\ReportDefinitionDateRangeType;
use Google\AdsApi\AdWords\Reporting\v201806\ReportDownloader;
use Google\AdsApi\AdWords\ReportSettingsBuilder;
use Google\AdsApi\AdWords\v201806\cm\Predicate;
use Google\AdsApi\AdWords\v201806\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201806\cm\ReportDefinitionReportType;
use Google\AdsApi\AdWords\v201806\cm\Selector;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\AdWords\v201802\cm\DateRange;
use File;
use Maatwebsite\Excel\Facades\Excel;

use DB;

/**
 * Downloads CRITERIA_PERFORMANCE_REPORT for the specified client customer ID.
 */
class DownloadCriteriaReportWithSelector
{

    public static function runExample(AdWordsSession $session, $filePath,$date)
    {
        
        $PublisherID =  DB :: table('Publisher')
                                    ->where('Publisher.PublisherName','=','AdWords')
                                    ->select('Publisher.id')->get();
        $Publisher_ID = json_decode($PublisherID,true);
        $p_id = $Publisher_ID[0]['id'];
                
        $hiddenMetricsID_Out = DB :: table('HiddenMetrics')                     
                            ->where('HiddenMetrics.Publisher_ID' ,'=',$p_id)
                            ->select('HiddenMetrics.PublisherMetricsParameter')->get();
        $hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);
        
        $arrayMetricNames =  array(
                    'Date',
                    'CampaignName',
                    'AdGroupName');                 
        
        foreach($hiddenMetricsID_Result as $key=>$value) {
                array_push($arrayMetricNames,$value['PublisherMetricsParameter']);
        }
        
        $fields = array_filter($arrayMetricNames);

        echo "<pre>"; print_r($fields);// exit;

        // Create selector.
        $selector = new Selector();
        $selector->setFields($fields);

        // Use a predicate to filter out paused criteria (this is optional).
        // $selector->setPredicates(
        //     [
        //         new Predicate('Status', PredicateOperator::NOT_IN, ['PAUSED'])
        //     ]
        // );
        $tillDate = date('Ymd');

        $selector->setDateRange(new DateRange($date,$tillDate));

        // Create report definition.
        $reportDefinition = new ReportDefinition();
        $reportDefinition->setSelector($selector);        
        $reportDefinition->setReportName(
            'Criteria performance report #' . uniqid()
        );
        // $reportDefinition->setDateRangeType(
        //     ReportDefinitionDateRangeType::LAST_7_DAYS
        // );
     
        $reportDefinition->setDateRangeType(
            ReportDefinitionDateRangeType::CUSTOM_DATE
        );
        $reportDefinition->setReportType(
            ReportDefinitionReportType::ADGROUP_PERFORMANCE_REPORT
        );
        //ACCOUNT_PERFORMANCE_REPORT
        //CRITERIA_PERFORMANCE_REPORT
        //KEYWORDS_PERFORMANCE_REPORT
        $reportDefinition->setDownloadFormat(DownloadFormat::CSV);

        // Download report.
        $reportDownloader = new ReportDownloader($session);
        // Optional: If you need to adjust report settings just for this one
        // request, you can create and supply the settings override here. Otherwise,
        // default values from the configuration file (adsapi_php.ini) are used.
        $reportSettingsOverride = (new ReportSettingsBuilder())->includeZeroImpressions(false)->build();
        $reportDownloadResult = $reportDownloader->downloadReport(
            $reportDefinition,
            $reportSettingsOverride
        );
        $reportDownloadResult->saveToFile($filePath);
        printf(
            "Report with name '%s' was downloaded to '%s'.\n",
            $reportDefinition->getReportName(),
            $filePath
        );
    }

    public static function main($date)
    {
        // Generate a refreshable OAuth2 credential for authentication.
       // echo $date; exit;

        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile()->build();


        // See: AdWordsSessionBuilder for setting a client customer ID that is
        // different from that specified in your adsapi_php.ini file.
        // Construct an API session configured from a properties file and the
        // OAuth2 credentials above.
        $session = (new AdWordsSessionBuilder())->fromFile()->withOAuth2Credential($oAuth2Credential)->build();

        $filePath = sprintf(
            '%s.csv',
            tempnam(sys_get_temp_dir(), 'criteria-report-')
        );
        $filePath = public_path('/reports/adWordReports/criteria-report.csv');
        self::runExample($session, $filePath,$date);
    }


    public static function runExampleY(AdWordsSession $session, $filePath,$date)
    {
         
        $PublisherID =  DB :: table('Publisher')
                                    ->where('Publisher.PublisherName','=','AdWordsY')
                                    ->select('Publisher.id')->get();
        $Publisher_ID = json_decode($PublisherID,true);
        $p_id = $Publisher_ID[0]['id'];
                
        $hiddenMetricsID_Out = DB :: table('HiddenMetrics')                     
                            ->where('HiddenMetrics.Publisher_ID' ,'=',$p_id)
                            ->select('HiddenMetrics.PublisherMetricsParameter')->get();
        $hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);
        
        $arrayMetricNames =  array(
                    'Date',
                    'CampaignName',
                    'AdGroupName');                 
        
        foreach($hiddenMetricsID_Result as $key=>$value) {
                array_push($arrayMetricNames,$value['PublisherMetricsParameter']);
        }
        
        $fields = array_filter($arrayMetricNames);
        echo "<pre>"; print_r($fields); //exit;
        $selector = new Selector();
        $selector->setFields($fields);

        $tillDate = date('Ymd');

        $selector->setDateRange(new DateRange($date,$tillDate));

        // Create report definition.
        $reportDefinition = new ReportDefinition();
        $reportDefinition->setSelector($selector);  

        $reportDefinition->setReportName(
            'Criteria performance Youtube report #' . uniqid()
        );
        
        $reportDefinition->setDateRangeType(
            ReportDefinitionDateRangeType::CUSTOM_DATE
        );
        $reportDefinition->setReportType(
            ReportDefinitionReportType::ADGROUP_PERFORMANCE_REPORT
        );
        
        $reportDefinition->setDownloadFormat(DownloadFormat::CSV);

        // Download report.
        $reportDownloader = new ReportDownloader($session);
        $reportSettingsOverride = (new ReportSettingsBuilder())->includeZeroImpressions(false)->build();
        $reportDownloadResult = $reportDownloader->downloadReport(
            $reportDefinition,
            $reportSettingsOverride
        );
        $reportDownloadResult->saveToFile($filePath);
        printf(
            "Report with name '%s' was downloaded to '%s'.\n",
            $reportDefinition->getReportName(),
            $filePath
        );
    }

    public static function mainY($date)
    {
        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile()->build();
        $session = (new AdWordsSessionBuilder())->fromFile()->withOAuth2Credential($oAuth2Credential)->build();

        $filePath = sprintf(
            '%s.csv',
            tempnam(sys_get_temp_dir(), 'criteria-reportY-')
        );
        $filePath = public_path('/reports/adWordReports/criteria-reportY.csv');
        self::runExampleY($session, $filePath,$date);
    }
}

//DownloadCriteriaReportWithSelector::main();
