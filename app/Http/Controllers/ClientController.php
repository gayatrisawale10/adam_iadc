<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Client;
use App\UserRole;
use App\Users;
use App\UserClient;
use App\DurationType;
use App\ClientBrand;
use validator;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
  public function index(Request $request)
  {

    $ClientDetails_index = DB::table('Client')
      ->select('Client.*', 'Client.ClientName')
      ->get();
      return view('Client.index',compact('ClientDetails_index'))
                ->with('i', ($request->input('page', 1) - 1) * 5);
  }

  public function create()
  {
    $id=0;
    $getClientName='';

    return view('Client.create',compact('id','getClientName'));
  }

  public function edit($c_id)
  {

    $getClient_id=DB::table('Client')
    ->where('Client.id','=',$c_id)
    ->select('Client.id','Client.ClientName')
    ->get();

    $id = $getClient_id[0]->id;

    $getClientName=$getClient_id[0]->ClientName;
    $getClientName=$getClientName;

    $getClient_id=json_decode($getClient_id);
    $getClient_id = $getClient_id[0];

    return view('Client.edit',compact('getClient_id','id','getClientName'));
  }

  public function store(Request $request)
  {

    $Clientdetails=Client::create([
      'ClientName' => $request->input('ClientName'),
      'CreatedOn' => Carbon::now()->format('m-d-Y'),
      'ModifiedOn' => Carbon::now()->format('m-d-Y'),
      'ModifiedBy' => 'Admin',
      'isActive' => True,
      'LineItemType'=>1
    ]);

    return redirect()->route('Client.index')
          ->with('success','Client created successfully');

  }
  public function update(Request $request, $c_id)
  {

    $Client_update=Client::where([ ['id','=',$c_id],])
                          ->update(['ClientName'=>$request->get('ClientName'), ]);
    return redirect()->route('Client.index')
                     ->with('success','Client updated successfully');
  }
  public function show($work_id)
  {
     return view('Client.show',compact('workshow'));
  }
  public function destroy($c_id)
  {
    //dd($c_id);

    $findClintBrand = DB::table('ClientBrand')->where('Client_ID', '=', $c_id)->get();
    $findClintBrand=json_decode($findClintBrand);

    //dd($findClintBrand);

    if($findClintBrand){
      return redirect()->route('Client.index')
      ->with('success','Client Brand is Present, unable to delete');
    }
    else{
      Client::find($c_id)->delete();
      return redirect()->route('Client.index')
      ->with('success','Client deleted successfully');
    }
  }
  // --------------------------------------------------------

   public function getClientCredentials($client_id)
  {
    //dd($client_id);

    $getUserDetails=DB::table('UserClient')
                  ->join('users','users.id','=','UserClient.User_ID')
                  ->where('UserClient.Client_ID','=',$client_id)
                  ->get();
    $getUserDetails=json_decode($getUserDetails);
    //dd($getUserDetails);

    $getClientDetails=DB::table('Client')
                  ->where('id','=',$client_id)
                  ->get();
    $getClientDetails=json_decode($getClientDetails);
    //dd($getPublisherDetails[0]->PublisherName);

    $ClientName=$getClientDetails[0]->ClientName;


    if($getUserDetails)
    {
      $getUserName=$getUserDetails[0]->name;
      $UserName=$getUserName;
      $Password='**********';
    }
    else
    {
      $UserName=Null;
      $Password=Null;
    }
   // dd($UserName,$Password);
    return view('Client.Credentials',compact('client_id','UserName','Password','ClientName'));
  }

  public function saveClientCredentials( Request $request, $client_id)
  {

    $arr=$request->all();
    //dd($arr);
    $getUserRole=UserRole::where('UserRoleName','=','Client')->get();
    $getUserRole=json_decode($getUserRole);
    $getUserRole_id=$getUserRole[0]->id;
    $UserRole_ID=$getUserRole_id;

    $checkCredentialexist=UserClient::where('Client_ID','=',$client_id)
              ->join('users','users.id','=','UserClient.User_ID')
              ->get();
    $checkCredentialexist=json_decode($checkCredentialexist);
    //dd($checkCredentialexist);

    if($checkCredentialexist)
    {
      $UserCredentials=users::where([ ['Client_ID','=',$client_id],])
        ->join('UserClient as up','up.User_ID','=','users.id')
        ->update(
          [ 'UserRole_ID' => $UserRole_ID,
            'name' => $arr['UserName'],
            'password' => bcrypt($arr['Password']),
            'email'=> $arr['UserName'],
            'remember_token'=>$arr['_token'],

            'CreatedOn' => Carbon::now()->format('m-d-Y'),
            'ModifiedOn' => Carbon::now()->format('m-d-Y'),
            'ModifiedBy' => 1,
            'isActive' => True,
          ]);
    }
    else
    {
      $UserCredentials=Users::create([
        'UserRole_ID' => $UserRole_ID,
        'name' => $arr['UserName'],
        'password' => bcrypt($arr['Password']),
        'email'=> $arr['UserName'],
        'remember_token'=>$arr['_token'],

        'CreatedOn' => Carbon::now()->format('m-d-Y'),
        'ModifiedOn' => Carbon::now()->format('m-d-Y'),
        'ModifiedBy' => 1,
        'isActive' => True,
      ]);

      $User_ID=$UserCredentials->id;

      $UserPublisherDetails=UserClient::insert([
        'User_ID' => $User_ID,
        'Client_ID' => $client_id,

        'CreatedOn' => Carbon::now()->format('m-d-Y'),
        'ModifiedOn' => Carbon::now()->format('m-d-Y'),
        'ModifiedBy' => 1,
        'isActive' => True,

      ]);
    }

    return redirect()->route('Client.index')
    ->with('success','Log-In Credential Updated successfully');
  }

}
