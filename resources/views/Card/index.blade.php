@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-5">
			@if(Session::has('error'))
				<div class="alert alert-danger">
				  {{ Session::get('error')}}
				</div>
			@endif

			@if(Session::has('success'))
				<div class="alert alert-success">
				  {{ Session::get('success')}}
				</div>
			@endif
		</div>


	    <div class="col-lg-12 pull-left">
	            <h2>Card Details</h2>
	    </div>

		<div class="col-md-12">
			<button type="button" class="btn btn-success pull-right " data-toggle="modal" data-target=".bs-example-modal-sm">Create Card</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-success pull-right" href="{{ URL::previous() }}">Back </a>
		</div>

	</div>
	<div class="row">
		<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
			<div class="modal-dialog modal-sm" role="document">
			    <div class="modal-content" style="padding: 12px;">
			    	<form method="POST" action="{{route('saveCard')}}">
			    		{{ csrf_field() }}

					  	<div class="form-group">
						  	<label for="exampleInputEmail1">Name</label>
						    <input type="text"  class="form-control" required="" name="cardName" placeholder="Enter Card Name">
					  	</div>
					  	<button type="submit" class="btn btn-default btn-block">Submit</button>
					</form>
			    </div>
			</div>
		</div>


		<div class="row" style="margin-top: 15px;">
			<table  class="table table-bordered display" id="sample_editable_1">
				<thead class="success">
					<tr>
						<th>Sr. No.</th>
						<th>CardName</th>
						<th>Creative</th>
						<th>Action</th>
					</tr>
				</thead>
            	<?php $i= 1; $j=0;?>
				<tbody>
				@foreach($get_card as $user)
				<tr class="column{{$user->id}}">
					<td>{{$i}}</td>
					<td class="columnName">{{$user->CardName}}</td>
					<td class="columnName">{{$listofCreative[$j]}}</td>
					<td>
						<button type="button" btnid="{{$user->id}}" class="btn btn-primary btnEditUser" data-toggle="modal" data-target=".user-edit-button">Edit</button>
						<a href="{{url('/card/delete/'.$user->id)}}" class="btn btn-danger">Delete</a>
            <button type="button" btnid="{{$user->id}}" class="btn btn-primary btnCreateCreative" data-toggle="modal" data-target=".create-creative">Add Creative</button>

					</td>
				</tr>
				<?php  $i++; $j++;?>
				@endforeach
				</tbody>
			</table>
		</div>

		<div class="modal fade user-edit-button" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  		<div class="modal-dialog modal-sm" role="document">
    		<div class="modal-content">
      			<form method="POST"  action="{{route('editCard')}}" style="padding: 15px;">
    			{{ csrf_field() }}

    				<input type="hidden" name="user_id" class="user_id" >

					<div class="form-group">
					  	<label for="exampleInputEmail1">Name</label>
					    <input type="text"  class="form-control editName" required="" name="name" value="" placeholder="Full Name">
					</div>




		  			<button type="submit" class="btn btn-default btn-block">Submit</button>
				</form>
    		</div>
  		</div>
		</div>

<!--add creative-->
<div class="modal fade create-creative" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <form method="POST"  action="{{route('addCreative')}}" style="padding: 15px;">
      {{ csrf_field() }}

        <input type="hidden" name="user_id" class="user_id_creative">

      <div class="form-group">
          <label for="exampleInputEmail1">Name</label>
          <input type="text"  class="form-control editName" required="" name="creative_name" value="" placeholder="Creative Name">
      </div>




        <button type="submit" class="btn btn-default btn-block">Submit</button>
    </form>
    </div>
  </div>
</div>
<!---->
	</div>
</div>
<script>
$(document).on('click', '.btnEditUser', function(){

elem = $(this).attr('btnid');
name =  $(this).parent('td').parent('tr').find('.columnName').html();
$('.user_id').val(elem);
$('.editName').val(name);
});

$(document).on('click', '.btnCreateCreative', function(){

elem = $(this).attr('btnid');
//name =  $(this).parent('td').parent('tr').find('.columnName').html();
$('.user_id_creative').val(elem);
//$('.editName').val(name);
});
</script>


@endsection
