<?php
$upTwo = dirname(__DIR__, 2);
require_once $upTwo. '/vendor/autoload.php';
session_start();
$client = new Google_Client();
$client->setAuthConfig(__DIR__ . '/client_secrets.json');

$url = dirname(__FILE__);
$array = explode('\\',$url);
$count = count($array);

$authCallUrl='http://' . $_SERVER['HTTP_HOST'] . '/'.$array[$count-3] . '/'.$array[$count-2] . '/'.$array[$count-1] .'/oauth2callback.php'; //exit;
//echo"auth bk URL "; print_r($authCallUrl); exit;
$client->setRedirectUri($authCallUrl);
$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
$client->addScope('https://www.googleapis.com/auth/doubleclickbidmanager');
// Handle authorization flow from the server.
if (! isset($_GET['code'])) {
	//echo"in authBk If"; exit;
  $auth_url = $client->createAuthUrl();
  // echo"<pre>inElse authUrl "; print_r($auth_url); //exit;
  header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));

} else {
	//echo"in authBk else"; exit;
  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/IADC/App/dbm/DBMReportProcessing.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}