@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <h2> Campaign Details</h2>
            </div>
            <div class="pull-right margin-top-20">
              <!--  start button for  Pop up box code -->  
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#gridSystemModal">
				        Add Campaign
              </button> 
            </div>
        </div>
    </div>
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered display" id="sample_editable_1">
	    <thead>   
        <tr>         
          <th>Client Name</th>
			    <th>Brand Name</th>
           <th>Campaign</th>
			    <th>Start Date</th>
			    <th>End Date</th>
          <th>Created On</th>
			    <th width="280px">Action</th>
        </tr>
	    </thead>
	    <tbody>     
    		@foreach ($getCompaignDetails as $cntr)
    			<tr align="center">				
            <td></td>
    				<td></td>
            <td>{{$cntr->CampaignName}}</td>
    				<td>{{$cntr->StartDate}}</td>
    				<td>{{$cntr->EndDate}}</td>
    				<td>{{$cntr->created_at}}</td>
    				<td>
    					<a class="btn btn-primary" href="">Edit
    					</a>
    	        <a class="btn btn-primary" href="">Line Item</a> 
    			   
            {!! Form::open(['method' => 'DELETE','route' => [],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger', 'onclick'=>'return confirm("Are you sure you want to delete?")']) !!}
                {!! Form::close() !!} 
    				</td>
    			 </tr>
    		@endforeach
	    </tbody>
    </table>
@endsection

