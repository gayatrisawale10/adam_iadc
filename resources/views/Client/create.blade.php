@extends('layouts.appResourcePublisher')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Create Client</h2>
            </div>
        </div>
    
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(array('route' =>'Client.store','method'=>'POST')) !!}
         @include('Client.form')
    {!! Form::close() !!}
	</div>
	
@endsection
