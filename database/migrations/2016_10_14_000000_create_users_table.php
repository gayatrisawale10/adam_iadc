<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
			$table->integer('role_id')->unsigned();
            $table->rememberToken();
			$table->string('modifiedby',45);
			$table->enum('isactive', array('Y', 'N'));
            $table->timestamps();
        });
          Schema::table('users', function($table) {
       $table->foreign('role_id')->references('r_id')->on('role');
   });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
