@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left"><h2>Client Details</h2></div>
            <div class="pull-right margin-top-20">
                <a class="btn btn-success" href="{{route('Client.create') }}">Create Client </a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="portlet-body">
        <div class="table-toolbar">
            <div style="width: 100%; overflow-x:auto;margin-bottom: 50px;">
                <table class="table table-bordered display" id="sample_editable_1">
                    <thead> 
                        <tr>
                            <th>Client Name</th>
                            <th width="250px">Action</th>
                            <th width="200px">Create Brand</th>
                           

                        </tr>
                    </thead> 
                    <tbody> 
                        @foreach ($ClientDetails_index as $ClientDetails_index_data)
                        <tr align="center">
                            <td>{{$ClientDetails_index_data->ClientName}}</td>
                            
                            <td>
                               <a class="btn btn-primary" href="{{ route('Client.edit',$ClientDetails_index_data->id) }}">Edit</a>
                              
                                <a class="btn btn-primary" href="{{route('Client.credentials',$ClientDetails_index_data->id)}}">Credentials</a>


                               {!! Form::open(['method' => 'DELETE','route' => ['Client.destroy',$ClientDetails_index_data->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger', 'onclick'=>'return confirm("Are you sure you want to delete?")']) !!}
                                {!! Form::close() !!}
                            </td>
                            <td>
                                <a class="btn btn-primary" href="{{ URL::route('ClientBrand.getAllBrandsToDisplay',
                                                                   $ClientDetails_index_data->id)}}">
                                                                   Create Brand
                                </a>

                            </td>   
                        </tr>
                       @endforeach
                    </tbody>
                </table>
                @endsection
            </div>
        </div>        
    </div>    

