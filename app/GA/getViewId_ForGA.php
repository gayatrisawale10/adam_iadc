<?php

// Load the Google API PHP Client Library.
$upTwo = dirname(__DIR__, 2);
require_once $upTwo. '/vendor/autoload.php';

$analytics = initializeAnalytics();
$profile = getFirstProfileId($analytics);

function initializeAnalytics()
{
  $KEY_FILE_LOCATION = __DIR__ . '/service-account-credentials.json';
  $client = new Google_Client();
  $client->setApplicationName("Hello Analytics Reporting");
  $client->setAuthConfig($KEY_FILE_LOCATION);
  $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
  $analytics = new Google_Service_Analytics($client);

  return $analytics;
}

function getFirstProfileId($analytics) {
  global $viewId;
  $viewId=[];
  // Get the list of accounts for the authorized user.
  $accounts = $analytics->management_accounts->listManagementAccounts();
  if (count($accounts->getItems()) > 0) {
    $items = $accounts->getItems();
    for ( $rowIndex = 0; $rowIndex < count($items); $rowIndex++) {
      $accountId = $items[$rowIndex]->getId();
      $properties = $analytics->management_webproperties
          ->listManagementWebproperties($accountId);

      if (count($properties->getItems()) > 0) {
        $items1 = $properties->getItems();

        for ( $rowIndex1 = 0; $rowIndex1 < count($items1); $rowIndex1++) {
          $propertyId = $items1[$rowIndex1]->getId();

          $profiles = $analytics->management_profiles
              ->listManagementProfiles($accountId, $propertyId);

          if (count($profiles->getItems()) > 0) {
            $items2 = $profiles->getItems();
            for ( $rowIndex2 = 0; $rowIndex2 < count($items2); $rowIndex2++) {
              array_push($viewId, $items2[$rowIndex2]->getId());
            }
          } 
          else 
          {
            throw new Exception('No views (profiles) found for this user.');
          }
        }
      } 
      else 
      {
        throw new Exception('No properties found for this user.');
      }
    }
  } 
  else 
  {
    throw new Exception('No accounts found for this user.');
  }
}

function getResults($analytics, $profileId) {
  return $analytics->data_ga->get(
       'ga:' . $profileId,
       '7daysAgo',
       'today',
       'ga:sessions');
}

function printResults($results) {
  if (count($results->getRows()) > 0) {
    $profileName = $results->getProfileInfo()->getProfileName();
    $profileID = $results->getProfileInfo()->getProfileId();

    $rows = $results->getRows();
    $sessions = $rows[0][0];
  
    print "First view (profile) found: $profileID\n";
    print "Total sessions: $sessions\n";
  }
  else {
    print "No results found.\n";
  }
}