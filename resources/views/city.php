<?php $base_url= url('/'); ?> 
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Language" content="en-us">
<title>PHP MySQL Typeahead Autocomplete</title>
 <meta name="csrf-token" content=<?php echo "'".csrf_token()."'"; ?> >
<meta charset="utf-8">
<link rel="stylesheet" href="./css/bootstrap.css">
<link rel="stylesheet" href="https://rawgit.com/dbrekalo/attire/master/dist/css/build.min.css">
<script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>
<link rel="stylesheet" href="./css/fastselect.min.css">
<script src="./js/fastselect.standalone.js"></script>

<script>
$(document).ready(function() {
    $('.attireMainNav').hide();

    $('.multipleInput').fastselect({url: '<?php echo $base_url;?>/cities',
        loadOnce: false,
        apiParam: 'query',
        clearQueryOnSelect: true,
        minQueryLength: 1,
        focusFirstItem: false,
        flipOnBottom: true,
        typeTimeout: 150,
        userOptionAllowed: false,
        valueDelimiter: ',',  
        onItemSelect:null,
        placeholder: 'Choose option',
        searchPlaceholder: 'Search options',
        noResultsText: 'No results',
        userOptionPrefix: 'Add'}).bind(this);  
    });
$(document).ready(function() {
    $('.attireMainNav').hide();

    $('.multipleDynamic').fastselect({url: '<?php echo $base_url;?>/cities',
        loadOnce: false,
        apiParam: 'query',
        clearQueryOnSelect: true,
        minQueryLength: 1,
        focusFirstItem: false,
        flipOnBottom: true,
        typeTimeout: 150,
        choiceItemClass: 'fstChoiceItemEx',
        userOptionAllowed: false,
        valueDelimiter: ',',  
        onItemSelect:null,
        placeholder: 'Choose option',
        searchPlaceholder: 'Search options',
        noResultsText: 'No results',
        userOptionPrefix: 'Add'}).bind(this);
    });

</script>
<style>
h2 {
  font-weight:bold;
  width:100%;
  float:left;
  font-size:16px;
  margin: 20px 0  20px 0;
}
.content {
   margin-left: 20px; 
}
 #locDetails{
   margin:20px 0  0px 20px; 
}
.fstChoiceItemEx{
    display: inline-block;
    font-size: 1.2em;
    position: relative;
    margin: 0 .41667em .41667em 0;
    padding: .33333em .33333em .33333em 1.5em;
    float: left;
    border-radius: .25em;
    border: 1px solid #43A2F3;
    cursor: auto;
    color: #fff;
    background-color: #43A2F3;
    -webkit-animation: fstAnimationEnter 0.2s;
    -moz-animation: fstAnimationEnter 0.2s;
    animation: fstAnimationEnter 0.2s;
}
</style>
</head>

<body>
    <div class="content col-md-12">
        <div class="form-group row ">
            <div class="col-md-3">
            <h2>Include Location</h2>
                <form>
                    <input type="text" multiple id="includeLoc" name="multipleInputDynamic" class="multipleInput" placeholder="Please Enter Location" >
                </form>
            </div>
     
            <div class="col-md-3">
            <h2>Exclude Location</h2>
                <form>
                    <input type="text" multiple  id="excludeLoc" name="excludeLocation" class="multipleDynamic" placeholder="Please Enter Location" >
                </form>     
                
            </div>
        </div>
    </div>
    <div>
        <button class='btn btn-primary'  id="locDetails">Apply</button></div>
    </div>
</body>
<script>
$(document).on('click', '#locDetails', function() { 
    var incdata = [];
    var excdata = [];
    $(".fstChoiceItem").map(function() {
       incdata.push($(this).data('text'));
    });
    $(".fstChoiceItemEx").map(function() {
        excdata.push($(this).data('text'));
    });
    
 var data = {
    'includeLocation':$('.multipleInput').val(),
    'excludeLocation':$('.multipleDynamic').val(),
  }
  $.ajax({
        type: 'POST',
        url: "setCitySessions",  
        data: {'data':data},
         headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
        success: function (response) {
            if(response.message=='set in session'){
                window.opener.document.getElementById('addrs').innerHTML = '';
                window.opener.document.getElementById('dists').innerHTML = '';
                if (incdata.length > 0) {
                   window.opener.document.getElementById('locationInclude').innerHTML = 'Include: '+incdata.join(',');
                } else {
                     window.opener.document.getElementById('locationInclude').innerHTML = '';
                }
                if (excdata.length > 0) {
                   window.opener.document.getElementById('locationExclude').innerHTML = 'Exclude: '+excdata.join(',');
                } else {
                    window.opener.document.getElementById('locationExclude').innerHTML = '';
                }  

                // for edit

                window.opener.document.getElementById('addrs_edit').innerHTML = '';
                window.opener.document.getElementById('dists_edit').innerHTML = '';
                if (incdata.length > 0) {
                   window.opener.document.getElementById('locationInclude_edit').innerHTML = 'Include: '+incdata.join(',');
                } else {
                     window.opener.document.getElementById('locationInclude_edit').innerHTML = '';
                }
                if (excdata.length > 0) {
                   window.opener.document.getElementById('locationExclude_edit').innerHTML = 'Exclude: '+excdata.join(',');
                } else {
                    window.opener.document.getElementById('locationExclude_edit').innerHTML = '';
                }  

                            
                
                window.close();
            }
        }
    });
});
</script>

</html>