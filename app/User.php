<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends Authenticatable
{
    use Notifiable;
	use EntrustUserTrait; // add this trait to your user model

    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'contactnumber','email', 'password','role_id','isactive','modifiedby'
    // ];

    protected $fillable = [
        'UserRole_ID','name','email',  'password', 'remember_token','isActive','Modifiedby'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
	 
    protected $hidden = [
        'password', 'remember_token','UserRole_ID','Modifiedby','isActive'
    ];
	
	public $table = "users";
	
	public function role() 
	{
		return $this->belongsTo('App\Models\Role');
	}
	
	
}
