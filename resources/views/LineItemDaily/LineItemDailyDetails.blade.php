<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <link rel="shortcut icon" href="../../image/favicon.ico" type="image/x-icon">
  <link rel="icon" href="./image/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
  <!-- <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script> -->
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <link data-require="datatables@*" data-semver="1.10.12" rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
  <link rel="stylesheet" href="//cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" />
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'IADC') }}</title>

  <link href="{{ url('/css/sweetalert.css')}}" rel="stylesheet">
  <style>

  .myrightalign{
        text-align: right;
        width: 70px;
  }
   .mycenteralign{
        text-align: center;
        width: 70px;
  }

  .myheadTextalign{
        text-align: center;
        width: 70px;
  }

  #example thead th
  {
    width: 100px !important;
  }

  .navbar-fixed-bottom, .navbar-fixed-top {
        width:100%;
        /* position: relative; */
        margin-top:30px;
  }

  </style>
</head>

<body>
  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header"></div>
      <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <ul class="nav navbar-nav">
          <img class="home_logo" src="{{asset('/image/logo_final.png') }}" width='60' height="60">
        </ul>
        <!-- Right Side Of Navbar -->
       
        <!-- ---- -->
        <ul class="nav navbar-nav navbar-right">
          <!-- Authentication Links -->
          <li><a href="">Publisher</a></li>
          <li class="dropdown">
              <a class=" dropdown-toggle" type="button" data-toggle="dropdown">{{ Auth::user()['name'] }}<span class="caret"></span></a>
              <ul class="dropdown-menu">
                  <li>
                      <a  href="{{ url('/logout') }}"
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">Logout
                      </a>

                      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                  </li>                          
              </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  @if ($message = Session::get('success'))
    <div class="alert alert-success">
      <p>{{ $message }}</p>
    </div>
  @endif

  <div class="container">
    <div class="row">
      <div class="col-lg-12">
            <div class="pull-left">
              <h2>Daily LineItem Details</h2>
               <h4>Campaign : {{$CampaignName}}</h4>
            </div>
            <div class="pull-right margin-top-20">
                <a class="btn btn-success" href="{{ url('/PublisherHome') }}">Back </a>
            </div>
        </div>
    </div>
    <div class="row" >
      <div class="col-lg-12">
          <div class="btn btn-danger" id="errorid" style='display:none;'></div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <!-- auto scroll start -->
        <div style="width: 100%; overflow-x:auto;">
          <div class="portlet-body">
            <div class="table-toolbar">
               <div style="width: 100%; overflow-x:auto;margin-bottom: 50px;">
                 @if($lineItemType=="1")
                <table width="100%" cellspacing="0" id="example" class="display"  >
                  <thead>
                   <tr>

                      <th class="myheadTextalign" width='46px'>Date</th>
                      <th class="myheadTextalign" width='46px'>Phase</th>
                      <th class="myheadTextalign" width='46px'>Objective</th>
                      <th class="myheadTextalign" width='46px'>Objective<br>Value</th>
                      <th class="myheadTextalign" width='46px'>Medium</th>

                      <th class="myheadTextalign" width='46px'>Format</th>
                      <th class="myheadTextalign" width='46px'>Deal<br>Type</th>
                      <th class="myheadTextalign" width='46px'>Delivery<br>Mode</th>

                      @if(in_array(1, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>Impressions</th>
                      @endif
                      @if(in_array(2, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>Clicks</th>
                      @endif
                      @if(in_array(3, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>Visits</th>
                      @endif
                      @if(in_array(4, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>Views</th>
                       @endif
                      @if(in_array(5, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>Leads</th>
                       @endif
                      @if(in_array(6, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>Engagement</th>
                       @endif
                      @if(in_array(7, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>NetRate</th>
                       @endif
                      @if(in_array(8, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>NetCost</th>
                       @endif
                      @if(in_array(9, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>Reach</th>
                       @endif
                      @if(in_array(10, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>Installs</th>
                       @endif
                      @if(in_array(11, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>Spots</th>
                       @endif
                      @if(in_array(12, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>SMS</th>
                       @endif
                      @if(in_array(13, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>Mailers</th>
                       @endif



                      <th class="myheadTextalign" width='46px'>Action</th>
                    </tr>
                  </thead>
                  <?php $i=0; ?>
                  @foreach($getAllDailyLineItem as $eachLine)
                  <tr align="center">
                    <td>
                      <input  type='text' class="mycenteralign form-control startDate" id="LID_date_{{$i}}" required="true" name='LID_date' value='{{ date("d-m-Y",strtotime($eachLine->Date))}}' style="width:100px" disabled="true" >
                    </td>
                    <td>
                      <label  class="mycenteralign"  id="LID_phase_{{$i}}" name='LID_phase' style="width:50px" >
                      {{$eachLine->PhaseName}}</label>
                    </td>
                    <td>
                      <label  class="mycenteralign"  id="LID_objectiveValue_{{$i}}" name='LID_objectiveValue' style="width:75px">
                      {{$eachLine->ObjectiveName}}</label>
                    </td>
                    <td>
                      <label class="mycenteralign" id="LID_objective_{{$i}}" name='LID_objective' style="width:50px">
                       {{$eachLine->ObjectiveValueName}}</label>
                    </td>
                    <td>
                        <label class="mycenteralign"  id="LID_medium_{{$i}}" name='LID_medium' style="width:50px">
                        {{$eachLine->MediumName}}</label>
                    </td>
                    <!--  -->
                    <td>
                      <label class="mycenteralign"  id="LID_format_{{$i}}" name='LID_format' style="width:50px">
                      {{$eachLine->FormatName}}</label>
                    </td>
                    <td>
                      <label class="mycenteralign"  id="LID_dealType_{{$i}}" name='LID_dealType' style="width:50px" >
                      {{$eachLine->DealTypeName}}</label>
                    </td>
                    <td>
                      <label class="mycenteralign" id="LID_deliveryMode_{{$i}}" name='LID_deliveryMode' style="width:50px">
                        {{$eachLine->DeliveryModeName }}
                      </label>
                    </td>


                    @if(in_array(1,$getAllDITManual))
                    <td>
                    <input class="myrightalign form-control get_value Impressions"  id="LID_Impressions_{{$i}}" type='text' name='LID_Impressions' value="{{$eachLine->Impression}}" placeholder = "Impressions" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                    @endif

                    @if(in_array(2, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value Clicks"  id="LID_Clicks_{{$i}}" type='text' name='LID_Clicks' value="{{$eachLine->Clicks}}" placeholder = "Clicks" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif
                      @if(in_array(3, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value Visits"  id="LID_Visits_{{$i}}" type='text' name='LID_Visits' value="{{$eachLine->Visits}}" placeholder = "Visits" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif
                      @if(in_array(4, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value Views"  id="LID_Views_{{$i}}" type='text' name='LID_Views' value="{{$eachLine->Views}}" placeholder = "Views" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif
                      @if(in_array(5, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value Leads"  id="LID_Leads_{{$i}}" type='text' name='LID_Leads' value="{{$eachLine->Leads}}" placeholder = "Leads" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif
                      @if(in_array(6, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value Engagement"  id="LID_Engagement_{{$i}}" type='text' name='LID_Engagement' value="{{$eachLine->Engagement}}" placeholder = "Engagement" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif
                      @if(in_array(7, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value NetRate"  id="LID_NetRate_{{$i}}" type='text' name='LID_NetRate' value="{{$eachLine->NetRate}}" placeholder = "NetRate" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif
                      @if(in_array(8, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value NetCost"  id="LID_NetCost_{{$i}}" type='text' name='LID_NetCost' value="{{$eachLine->NetCost}}" placeholder = "NetCost" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif
                      @if(in_array(9, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value Reach"  id="LID_Reach_{{$i}}" type='text' name='LID_Reach' value="{{$eachLine->Reach}}" placeholder = "Reach" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif
                      @if(in_array(10, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value Installs"  id="LID_Installs_{{$i}}" type='text' name='LID_Installs' value="{{$eachLine->Installs}}" placeholder = "Installs" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif
                      @if(in_array(11, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value Spots"  id="LID_Spots_{{$i}}" type='text' name='LID_Spots' value="{{$eachLine->Spots}}" placeholder = "Spots" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif
                      @if(in_array(12, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value SMS"  id="LID_SMS_{{$i}}" type='text' name='LID_SMS' value="{{$eachLine->SMS}}" placeholder = "SMS" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif
                      @if(in_array(13, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value Mailers"  id="LID_Mailers_{{$i}}" type='text' name='LID_Mailers' value="{{$eachLine->Mailers}}" placeholder = "Mailers" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif




                    <input type='hidden'  name='LineItem_ID' value="{{$eachLine->LineItem_ID}}" id="LineItem_ID_{{$i}}" data-LineItem_id="{{$eachLine->LineItem_ID}}">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" name="basePath" id="basePath" value="{{ url('/') }}">
                    <td>
                      <button type="button" class="btn green saveDailyLineItem" onclick="saveDailyLineItems({{$i}});"><i  class="fa fa-pencil-square" aria-hidden="true"></i></button>
                    </td>
                  </tr>

                  <?php $i++; ?>

                  @endforeach
                </table>
                @else
                <table width="100%" cellspacing="0" id="example" class="display">
                  <thead>
                   <tr>

                      <th class="myheadTextalign" width='60px'>Date</th>
                      <th class="myheadTextalign" width='60px'>Card</th>
                      <th class="myheadTextalign" width='60px'>Creative</th>
                      @if(in_array(1, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>Impressions</th>
                      @endif
                      @if(in_array(2, $getAllDITManual))
                      <th class="myheadTextalign" width='46px'>Clicks</th>
                      @endif
                      @if(in_array(5, $getAllDITManual))
                       <th class="myheadTextalign" width='46px'>Leads</th>
                      @endif
                      @if(in_array(14, $getAllDITManual))
                       <th class="myheadTextalign" width='46px'>Rate</th>
                      @endif
                      @if(in_array(15, $getAllDITManual))
                       <th class="myheadTextalign" width='46px'>Cost</th>
                      @endif
                      @if(in_array(16, $getAllDITManual))
                       <th class="myheadTextalign" width='46px'>Opens</th>
                      @endif




                      <th class="myheadTextalign" width='46px'>Action</th>
                    </tr>
                  </thead>
                  <!--new code -->
                  <?php $i=0; ?>
                  @foreach($getAllDailyLineItem as $eachLine)
                  <tr align="center">
                    <td>
                      <input  type='text' class="mycenteralign form-control startDate" id="LID_date_{{$i}}" required="true" name='LID_date' value='{{ date("d-m-Y",strtotime($eachLine->Date))}}' style="width:100px" disabled="true" >
                    </td>
                    <td>
                      <input  type='text' class="mycenteralign form-control card"  name='LID_card' value='{{ $eachLine->card}}' style="width:100px" disabled="true" >
                    </td>
                    <td>
                      <input  type='text' class="mycenteralign form-control creative"  name='LID_creative' value='{{ $eachLine->creative}}' style="width:100px" disabled="true" >
                    </td>

                    @if(in_array(1,$getAllDITManual))
                    <td>
                    <input class="myrightalign form-control get_value Impressions"  id="LID_Impressions_{{$i}}" type='text' name='LID_Impressions' value="{{$eachLine->Impression}}" placeholder = "Impressions" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                    @endif

                    @if(in_array(2, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value Clicks"  id="LID_Clicks_{{$i}}" type='text' name='LID_Clicks' value="{{$eachLine->Clicks}}" placeholder = "Clicks" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif

                      @if(in_array(5, $getAllDITManual))
                    <td>
                      <input class="myrightalign form-control get_value Leads"  id="LID_Leads_{{$i}}" type='text' name='LID_Leads' value="{{$eachLine->Leads}}" placeholder = "Leads" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                    </td>
                     @endif

                     @if(in_array(14, $getAllDITManual))
                   <td>
                     <input class="myrightalign form-control get_value RateCity"  id="LID_Rate_{{$i}}"  type='text' name='LID_Rate' value="{{$eachLine->Rate}}" placeholder = "Rate" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                   </td>
                    @endif
                    @if(in_array(15, $getAllDITManual))
                  <td>
                    <input class="myrightalign form-control get_value CostCity" id="LID_Cost_{{$i}}"   type='text' name='LID_Cost'  value="{{$eachLine->Cost}}" placeholder = "Cost" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                  </td>
                   @endif
                   @if(in_array(16, $getAllDITManual))
                 <td>
                   <input class="myrightalign form-control get_value OpensCity" id="LID_Opens_{{$i}}"  type='text' name='LID_Opens' value="{{$eachLine->Opens}}" placeholder = "Opens" style="width:75px"  data-value="" onkeypress="return isNumberKey(event)">
                 </td>
                  @endif




                    <input type='hidden'  name='LineItem_ID' value="{{$eachLine->LineItem_ID}}" id="LineItem_ID_{{$i}}" data-LineItem_id="{{$eachLine->LineItem_ID}}">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                    <input type="hidden" name="basePath" id="basePath" value="{{ url('/') }}">

                    <td>
                      <button type="button" class="btn green saveDailyLineItem" onclick="saveDailyLineItems({{$i}});"><i  class="fa fa-pencil-square" aria-hidden="true"></i></button>
                    </td>
                  </tr>

                  <?php $i++; ?>

                  @endforeach

                  <!--End new code-->
                </table>
                @endif
              </div>
            </div>
          </div>
          <div class="navbar-fixed-bottom pull-left navbar-default navbar-collapse" style="padding:-100px 0px; border-top:1px solid #ddd;">
            <strong> &nbsp; &nbsp;Powered by &nbsp;</strong><a href="http://www.e-stonetech.com" target="_blank"> e-Stone Information Technology Pvt. Ltd. &copy; 2016-17</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="../../../js/app.js"></script>
<script data-require="datatables@*" data-semver="1.10.12" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="../../../js/dataTables.rowReorder.js"></script>
<script>
  window.Laravel = <?php echo json_encode([
    'csrfToken' => csrf_token(),
  ]); ?>
</script>

<script src="{{ url('/js/sweetalert.js')}}"></script>

<script src="{{ url('/js/LineItemDaily.js') }}" type="text/javascript"></script>

<script>
    $(document).ready( function ()
    {
      //  $.noConflict();
        $('#example').css('border-bottom', 'none');
        $('#example').DataTable({
            "bProcessing" : true,
            "bPaginate" : true,
            "pagingType": "simple",
            "aaSorting": [],

        });


        $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd',
              minViewMode: 1,
              yearRange: '1999:2050',
              changeMonth: true,
                    changeYear: true }).val();
    });


</script>

</html>
