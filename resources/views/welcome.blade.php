@extends('layouts.login')

@section('content')
<style>
.width_button { 
		width:361px; 
		/* background-color: #1ab394;
		border-color: #1ab394; */
    }
.width_button:hover
	{  
		/* background-color: #18a689;
		border-color: #18a689; */
	}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-4">
            @if(Session::has('error'))
            <p class="bg-danger">{{$error}}</p>
            @endif

            @if (Session :: has('status'))
                <div class="alert alert-success">
                    {{ Session :: get('status') }}
                </div>
            @endif


            <!-- <div class="panel panel-default">
               <div class="panel-heading">Login</div>
                <div class="panel-body"> -->
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                           <!-- <label for="email" class="col-md-4 control-label">E-Mail Address</label> -->

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" placeholder="Username" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <!-- <label for="password" class="col-md-4 control-label" align='left'>Password</label> -->

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" placeholder='Password' name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
									 <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                                </div>
                            </div>
                        </div>
-->
                        <div class="form-group">
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-primary width_button">
                                    Login
                                </button>

                               
                            </div>
                        </div>
                    </form>
                    <a href="{{ URL::to('forgotPassword')}}">Forgot Password/Reset Password?</a>

              <!--  </div>
            </div> 
        </div> -->
    </div>

</div>
@endsection
