@extends('layouts.appResourcebrandEdit')
 <div class="container">
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Update Brand : {{$getClientDetails->ClientName}}</h2>
            </div>
            
        </div>   
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    
        {!! Form::model( $brandDetails_index, array('route' =>array('ClientBrand.updateBrand',$b_id, $c_id),'method'=>'PATCH')) !!}
            @include('ClientBrand.form')
        {!! Form::close() !!}	 
	</div>
@endsection
</div>