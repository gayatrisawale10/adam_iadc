<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\CampaignPublisher;
use App\Campaign;
use App\LineItem;
use App\LineItemDaily;
use App\Client;
use App\ClientBrand;
use App\DurationType;
use Session;
use validator;
use DB;
Use Redirect;
use Carbon\Carbon;
use DateTime;

use App\User;
use App\UserClient;

class LineItemDailyController extends Controller
{

  public function getCompaignDetails()
  {
    $u_id =Auth::user()->id;
    //dd($u_id);
    $getCompaignId = DB::table('UserPublisher')
                  ->where('UserPublisher.User_ID','=', $u_id)
                  ->join('Publisher','UserPublisher.Publisher_ID','Publisher.id')
                  ->join('CampaignPublisher','Publisher.id','CampaignPublisher.Publisher_ID')
                  ->join('Campaign','CampaignPublisher.Campaign_ID','Campaign.id')
          ->join('LineItem','CampaignPublisher.id','LineItem.CampaignPublisher_ID')
          ->join('LineItemMetrics','LineItem.id','LineItemMetrics.LineItem_ID')
                  ->where('Campaign.isActive','=',1)
          ->where('Publisher.PublisherType_ID','=',2)
          ->where('LineItemMetrics.DataInputType_ID','=',1)
                  ->select('Campaign_ID')
                  ->groupBy('Campaign_ID')
                  ->get();

    $getCompaignId=json_decode($getCompaignId);
    if($getCompaignId)
    {
      foreach ($getCompaignId as $key)
      {
        //dd($key);
        $getCompaignDetail = DB::table('Campaign')
                      ->where('Campaign.id','=',  $key->Campaign_ID)
                      ->where('Campaign.isActive','=', 1)
                      ->join('ClientBrand', 'Campaign.ClientBrand_ID', '=', 'ClientBrand.id')
                       ->join('Client', 'Client.id', '=', 'ClientBrand.Client_ID')
                      ->select('Campaign.id as campaign_ID','Campaign.CampaignName','Campaign.StartDate','Campaign.EndDate','ClientBrand.BrandName','Client.ClientName','Client.lineItemType')
                      ->first();

        //dd($getCompaignDetail);

        //$getCompaignDetail=json_decode($getCompaignDetail);

        if($getCompaignDetail)
        {
          $getCompaignDetails[]=$getCompaignDetail;
        }
      }

    }
    else
    {
      $getCompaignId=[];
      $getCompaignDetails =[];
    }

    $getPublisherId = DB::table('UserPublisher')
                    ->where('UserPublisher.User_ID','=', $u_id)
                    ->get();
    $getPublisherId=$getPublisherId[0]->Publisher_ID;
    $PublisherId=$getPublisherId;
    //dd($PublisherId);

    return view('LineItemDaily.displayAllCompaign',compact('getCompaignDetails','PublisherId'))->with('i');
  }

  public function getLineItemDailyDetails($cmp_id, $lineItemType,$PublisherId)
  {
     $getAllDailyLineItem=DB::select("exec SP_IADC_GetLineItemDaily ?,?", array( $cmp_id, $PublisherId ));

    if($lineItemType==2)
    {
     foreach ($getAllDailyLineItem as $key) {
       $cardAndCreative =DB::table('LineItem')
            ->join('creative','creative.id','=','LineItem.Creative_ID')
            ->join('Card','Card.id','=','creative.Card_ID')
            ->select('Card.CardName','Creative.CreativeName')
            ->where('LineItem.id','=',$key->LineItem_ID)
            ->get();

        $cardAndCreative=json_decode($cardAndCreative);
        $key->card=$cardAndCreative[0]->CardName;
        $key->creative=$cardAndCreative[0]->CreativeName;
     }
   }


    $getDITManual=DB::select("exec [SP_IADC_DITManual] ?,?", array( $cmp_id, $PublisherId));
    foreach($getDITManual as $mId) {
      $getAllDITManual[]=$mId->Metrics_ID;
    }

    $Campaign_list =DB::table('Campaign')->where('Campaign.id', '=',$cmp_id)
                                         ->select('Campaign.CampaignName')
                                         ->first();
    $CampaignName=$Campaign_list->CampaignName;                                    
    
    //dd($CampaignName);

    return view('LineItemDaily.LineItemDailyDetails', compact('cmp_id','PublisherId','getAllDailyLineItem','LineItem_ID', 'getAllDITManual','lineItemType','CampaignName'))
              ->with('i');

  }

  public function saveDailyLineItem(Request $request)
  {

    $data=$request->input();

    $date = date("Y-m-d", strtotime($data['StartDate']));
    $metricsValuesArray=$data['metricsValuesArray'];

    foreach ($metricsValuesArray as $key1 => $value1)
    {
      //if(empty($value1))
      if($value1=='')
      {
        $flag=1;
      }
      else
      {
        $flag=0;
      }
    }

    if($flag==1)
    {
      return response()->json(['status'=>'error'], 200);
    }
    else
    {
      //echo"<pre>"; print_r($flag); exit;
      $current_time = Carbon::now()->toDateTimeString();
      $lineItemDailyId=$request->input('LineItemDaily_id');

      foreach ($metricsValuesArray as $key => $value)
      {
        if(!is_null($value))
        {

          $LineItemDailydetails=DB::table('LineItemDaily')
              ->join('LineItemMetrics','LineItemDaily.LineItemMetrics_ID','=','LineItemMetrics.id')
              ->join('LineItem','LineItemMetrics.LineItem_ID','=','LineItem.id')

              ->where('LineItemDaily.Date', '=', $date)
              ->where('LineItemMetrics.LineItem_ID','=', $data['LineItem_ID'])
              ->where('LineItemMetrics.Metrics_ID', '=',($key+1))

              ->update([
                'Value' =>$value,
                'ipAddress' =>$_SERVER['REMOTE_ADDR'],

                'CreatedOn' =>  $current_time,
                'ModifiedOn' =>  $current_time,
                'ModifiedBy' => 2,
                'isActive' => True,
              ]);

        }

      }
    }

    if($LineItemDailydetails){
      return response()->json(['date'=>$data['StartDate'], 'status'=>'success'], 200);
    }

  }

    public function getCompaignDetailsForClient()
    {

      $u_id = (Auth::user()->id);

      //var_dump($u_id);

      $getCompaignDetails=User:: join('UserClient', 'UserClient.User_ID','=','Users.id')
                 ->join('Client', 'UserClient.Client_ID','=','Client.id')
                 ->join('ClientBrand', 'Client.id','=','ClientBrand.Client_ID')
                 ->join('Campaign', 'ClientBrand.id','=','Campaign.ClientBrand_ID')
                 ->where('UserClient.User_ID','=',$u_id)
                 ->where('Campaign.IsActive','=',1)
                 ->select('Users.name','Client.id as client_ID','Client.ClientName','ClientBrand.BrandName','Campaign.id as campaign_ID','Campaign.CampaignName','Campaign.StartDate','Campaign.EndDate')
                 ->get();

      $getCompaignDetails=json_decode($getCompaignDetails);
      $PublisherId=1;

      return view('LineItemDaily.displayAllCompaign',compact('getCompaignDetails','PublisherId'))->with('i');
  }


  // public function getDailyLineItemsCity()
  // {
  //   $date=$_GET['date'];
  //   $creative=$_GET['creative'];
  //   $getCampaignDetails=DB::table('LineItemCardCreativeDaily')
  //                       ->join('LineItemCardCreative', 'LineItemCardCreative.id','=','LineItemCardCreativeDaily.LineItemCardCreative_ID')
  //                       ->join('LineItemMetrics','LineItemMetrics.id','=','LineItemCardCreativeDaily.LineItemMetrics_ID')
  //                       ->select('LineItemCardCreativeDaily.*','LineItemMetrics.Metrics_ID')
  //                       ->whereIn('LineItemCardCreative.Creative_ID',$creative)
  //                       ->where('LineItemCardCreativeDaily.Date',$date)
  //                       ->get();
  //
  //   echo"<pre>"; print_r($getCampaignDetails);exit;
  //   if($getCampaignDetails)
  //   {
  //     return response()->json(['result'=>$getCampaignDetails, 'status'=>'success'], 200);
  //
  //   }
  //   //echo "<pre>"; print_r($getCampaignDetails);exit;
  // }


}
