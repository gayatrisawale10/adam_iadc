<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPublisher extends Model
{

	public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['Publisher_ID','User_ID','CreatedOn','ModifiedOn','ModifiedBy','isActive'];

	public $table = "dbo.UserPublisher";

}
