@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="pull-left">
        <h2> API's</h2>
      </div>

    </div>
  </div>

  @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
  @endif
  <?php $base_url= url('/');?>
  <div class="row processingWarning progrsWarn" style="display: none">
    <div class="col">
      <p class="alert alert-warning" id="msgWarning"> </p>
    </div>
  </div>
  <div class="row processingSuccess" style="display: none">
    <div class="col">
      <p class="alert alert-success" id="msgSuccess"> </p>
    </div>
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <!--=============================================old Api Excecution==========================================-->
     <!--  <table class="table table-bordered">
        <thead>
          <tr>
            <th>Start Date</th>
            <th width="280px" colspan="3">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr align="center">
            <td rowspan="5" >
              <input type='hidden' id="id" name='id' value="1">
              <input type="text" placeholder="Start Date" class="form-control" name='startDate' value="" class='datepicker' id="datepicker" style="background-color:white;" />
            </td>        
            <tr>
              <td style="border: none;"><button type="submit" class="btn btn-primary" onclick="addAdWords();">Ad Words</button></td>
              <td style="border: none;"><button type="submit" class="btn btn-primary" onclick="addBingAds();">BingAds</button></td>
              <td style="border: none;"><button type="submit" class="btn btn-primary" onclick="addTaboola();">Backstage Taboola</button></td>
            </tr>
            <tr>
              <td style="border: none;"><button type="submit" class="btn btn-primary" onclick="addDcm();">DCM</button></td>
              <td style="border: none;"><button type="submit" class="btn btn-primary" onclick="add();">Facebook</button></td>
              <td style="border: none;"><button type="submit" class="btn btn-primary" onclick="addOutBrain();">OutBrain</button></td>
            </tr>
            <tr>
              <td style="border: none;"><button type="submit" class="btn btn-primary" onclick="testSizMek();">SizMek</button></td>
              <td style="border: none;"> <button type="submit" class="btn btn-primary citybankprocess" onclick="addCityBank();">Citibank</button></td>
              <td style="border: none;"><button type="submit" class="btn btn-primary" onclick="addAdWordsY();">AdWordsY</button></td>
            </tr>
            <tr>
              <td style="border: none;"><form method="get"  action="{{url('addDBM_Display')}}" >
                  {{csrf_field()}}
                  <button type="submit" class="btn btn-primary">DBM_Others/Youtube</button>
                </form></td>
              <td style="border: none;"><form method="get"  action="{{url('addGoogleAnalytics')}}" >
                  {{csrf_field()}}
                  <button type="submit" class="btn btn-primary">GoogleAnalytics</button>
                  </form></td>
               <td style="border: none;">                       
          </tr>
        </tbody>
      </table> -->
      <!--=============================================End old Api Excecution==========================================-->
      <hr/>
      <!-- header row -->
      <form method="get"  action="{{url('dbmBasic')}}">{{csrf_field()}} 
      <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-6">
          <div class="row">
            <label class="col-md-4">Start Date :</label>
            <div class="col-md-5">
              <input type="text"  class='form-control datepicker' name='StartDate' value="" id="datepickerStart" placeholder="Start Date" style="background-color:white;"/>
            </div>
            <div class="col-md-3"></div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <label class="col-md-3">End Date :</label>
            <div class="col-md-5">
              <input type="text"  class='form-control datepicker' name='endDate' value="" id="datepickerEnd" placeholder="End Date" style="background-color:white;"/>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>
      </div> 
      <!-- header row -->
      <!-- ============================================================================================ -->
      <!-- DBM api rows -->
      <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-2">
          <label>DBM:</label>  
        </div>
        <div class="col-md-10">
            <div class="col-sm-2">
              <button type="submit" style="width: 100%" class="btn btn-sm btn-primary">DBM</button>
            </div>
          </form>   
        </div>
        <div></div>
        <br><br>
      </div>  
      <!-- end DBM api rows -->
      <!-- ============================================================================================ -->
      <!-- GoogleAnalytics api rows -->
      <div class="row" style="margin-bottom: 5px;">   
        <div class="col-md-2">
          <label>GoogleAnalytics:</label>  
        </div>
        <div class="col-md-10">
          <div class="col-sm-2">
            <form method="get"  action="{{url('adamGoogleAnalytics')}}" >
              {{csrf_field()}}
              <input type="hidden"  class='form-control' name='StartDate' id="datepickerStartGA"/>
              <input type="hidden"  class='form-control' name='endDate'  id="datepickerEndGA"/>
              <button type="submit" style="width: 100%"  class="btn btn-sm btn-primary">GoogleAnalytics</button>
            </form>
          </div>       
        </div>
        <div></div>
      </div>
      <!-- end GoogleAnalytics api rows -->  
      <!-- ============================================================================================ -->
      
      <!-- dailyProgrssCheck api single Button  -->
      <!-- <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-2">
          <label>daily Progrss Check:</label>
        </div>
        <div class="col-md-10">
          <div class="col-sm-2">
            <button type="submit" style="width: 100%" class="btn btn-sm btn-primary" value=""  onclick="dailyProgrssCheck(this);"> Daily Progrss Check</button>
          </div>
        </div>
        <div></div>
      </div> -->
      <!-- End dailyProgrssCheck api single Button  -->

      <!-- FB api single Button  -->
      <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-2">
          <label>Facebook:</label>
        </div>
        <div class="col-md-10">
          <div class="col-sm-2">
            <button type="submit" style="width: 100%" class="btn btn-sm btn-primary ageGenderBreak" value="AgeAndGender"  onclick="fbBreakDown(this);"> Facebook</button>
          </div>
        </div>
        <div></div>
      </div>
      <!-- End FB api single Button  -->

      <!-- DCM api rows -->
      <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-2">
          <label>DCM:</label>  
        </div>
        <div class="col-md-10">  
            <div class="col-sm-2">
             <button type="submit" style="width: 100%"  class="btn btn-sm btn-primary" onclick="adamDcm();">DCM</button>
            </div>
          
        </div>
        <div></div>
      </div>   
      <!-- end DCM api rows -->  

      <!-- Adword api rows -->
      <div class="row" style="margin-bottom: 5px;">   
        <div class="col-md-2">
          <label>Adword:</label>  
        </div>
        <div class="col-md-10">
            <div class="col-sm-2">
             <button type="submit" style="width: 100%"  class="btn btn-sm btn-primary" onclick="adamAdwords();">Adwords</button>
            </div>       
        </div>
        <div></div>
      </div>
      <!-- end DBM api rows --> 

      <!-- sizmek api rows -->
      <div class="row" style="margin-bottom: 5px;">   
        <div class="col-md-2">
          <label>Sizmek:</label>  
        </div>
        <div class="col-md-10">
            <div class="col-sm-2">
            <button type="submit" style="width: 100%"  class="btn btn-sm btn-primary" onclick="testAdamSizMek();">Sizmek</button>
            </div>       
        </div>
        <div></div>
      </div>
      <!-- end sizmek api rows -->  

      <!-- BingAds api rows -->
      <!-- <div class="row" style="margin-bottom: 5px;">   
        <div class="col-md-2">
          <label>Sizmek:</label>  
        </div>
        <div class="col-md-10">
          <div class="col-sm-2">
            <button type="submit" tyle="width: 100%"  class="btn btn-sm btn-primary" onclick="adamBingAds();">Get BingAds Report</button>
          </div>       
        </div>
        <div></div>
      </div> -->
      <!-- end BingAds api rows -->  

       <!-- Taboola api single Button  -->
      <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-2">
          <label>BackStage Taboola:</label>
        </div>
        <div class="col-md-10">
          <div class="col-sm-2">
            <button type="submit" style="width: 100%" class="btn btn-sm btn-primary" onclick="AdamTaboola(this);">BackStage Taboola</button>
          </div>
        </div>
        <div></div>
      </div>
      <!-- End Taboola api single Button  -->

       <!-- Outbrain api single Button  -->
      <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-2">
          <label>Outbrain:</label>
        </div>
        <div class="col-md-10">
          <div class="col-sm-2">
            <button type="submit" style="width: 100%" class="btn btn-sm btn-primary" onclick="AdamOutbrain(this);">Outbrain</button>
          </div>
        </div>
        <div></div>
      </div>
      <!-- End Outbrain api single Button  -->

      <!-- Yahoo reporting api Button  -->
      <!-- <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-2">
          <label>Yahoo:</label>
        </div>
        <div class="col-md-10">
          <div class="col-sm-2">
            <button type="button" style="width: 100%" class="btn btn-sm btn-primary" onclick="performance_stats_report()">Yahoo</button>
          </div>
        </div>
        <div></div>
      </div> -->
      <!-- End Yahoo reporting api Button  -->

      <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-2">
          <label>Hotstar:</label>
        </div>
        <div class="col-md-10">
          <div class="col-sm-2">
            <button type="submit" style="width: 100%" class="btn btn-sm btn-primary" onclick="Hotstar(this);">Hotstar</button>
          </div>
        </div>
        <div></div>
      </div>
      <!-- End Outbrain api single Button  -->
      @endsection


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<meta name="csrf-token" content=<?php echo "'".csrf_token()."'"; ?> >
<script type="text/javascript">
  $(function() {
    var todayTimeStamp = +new Date; // Unix timestamp in milliseconds
    var oneDayTimeStamp = 1000 * 60 * 60 * 24; // Milliseconds in a day
    var diff = todayTimeStamp - oneDayTimeStamp;
    var yesterdayDate = new Date(diff);
    var yesterdayString = yesterdayDate.getFullYear() + '-' + (yesterdayDate.getMonth() + 1) + '-' + yesterdayDate.getDate();

    $("#datepicker").datepicker({ dateFormat: 'yy-mm-dd',
          minViewMode: 1,
          maxDate: yesterdayString,
          yearRange: '1999:2050',
          changeMonth: true,
          changeYear: true }).val();

    $("#datepickerStart").datepicker({ dateFormat: 'yy-mm-dd',
      minViewMode: 1,
      maxDate: yesterdayString,
      yearRange: '1999:2050',
      changeMonth: true,
      changeYear: true }).val();

    $("#datepickerEnd").datepicker({ dateFormat: 'yy-mm-dd',
      minViewMode: 1,
      maxDate: yesterdayString,
      yearRange: '1999:2050',
      changeMonth: true,
      changeYear: true }).val();

    // ----------------------------------------------------
    $("#datepickerStart").datepicker();
    $("#datepickerStart").on("change",function(){
        var startDate = $(this).val();
        //alert(startDate);
        $('#datepickerStartGA').val(startDate);
    });
    $("#datepickerEnd").on("change",function(){
        var endDate = $(this).val();
        //alert(endDate);
        $('#datepickerEndGA').val(endDate);
    });

  });
  // ===================================================
   // ===================================================
  function dailyProgrssCheck(UniqueReportId) {
    $.ajax({
        type: 'POST',
        url: 'dailyProgrssCheck',
        data: {'daily': 'true'},
        headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        },
      success: function (data) {
        console.log(data);
      }
    });
  };
  // ===================================================
  function CheckProgress(UniqueReportId) {

    //alert("Check "+UniqueReportId);

    $.ajax({
        type: 'POST',
        url: 'getProgressStatus',
        //data: {'id': id,'StartDate':StartDate ,'endDate':endDate, 'breakdownElement':breakdownElement },
        data: {'UniqueReportId': UniqueReportId},
        headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        },
      success: function (response) {
        //console.log(response);
        var pubName=response.publisherName;
        //------------------------------------- 
        if(response.status =='ProcessingDoneString') {
            $(".processingSuccess").show();
            $(".processingWarning").hide();
            //$("#msgSuccess").html(""+response.ProcessingDoneString+"......!!!");
            $("#msgSuccess").append(""+response.ProcessingDoneString+"......!!!");
        }
      }
    });
  };


  function fbBreakDown(breakdownEle) {

    var StartDate = $('#datepickerStart').val();
    var endDate = $('#datepickerEnd').val();
    
    $(".processingWarning").show();
    $(".processingSuccess").hide();
    $("#msgWarning").html("Report for Facebook is in Processing......");

    var randomNumber=Math.floor(100000 + Math.random() * 900000);
    var UniqueReportId= 'FB_'+randomNumber;
    
    //var UniqueReportId='FB_475911';
    
    //alert("Brek "+UniqueReportId);

    if (StartDate && endDate ){
      var id = $('#id').val();
      $.ajax({
            type: 'POST',
            url: 'fbAPI',
            //data: {'id': id,'StartDate':StartDate ,'endDate':endDate, 'breakdownElement':breakdownElement },
            data: {'id': id,'StartDate':StartDate ,'endDate':endDate, 'UniqueReportId':UniqueReportId},
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
          success: function (data) {
            console.log(data);
            //------------------------------------- 
             // if(response.status =='success') {
             //      window.location.reload();
             //  } else {
             //      window.location.reload();
             //  }
          }
      });
      //setTimeout(CheckProgress(UniqueReportId), 10000);
      //setTimeout(function() {alert("hello");}, 3000);
      setTimeout(function() { CheckProgress(UniqueReportId) }, 1200000);// 5mins delay
    }
    else{
      alert("Please enter date");
    }
  };

  function adamDcm() {

    var StartDate = $('#datepickerStart').val();
    var endDate = $('#datepickerEnd').val();

    $(".processingWarning").show();
    $(".processingSuccess").hide();
    $("#msgWarning").html("Report for DCM is in Processing......");

    var randomNumber=Math.floor(100000 + Math.random() * 900000);
    var UniqueReportId= 'DCM_'+randomNumber;
    
    if (StartDate && endDate ){
      var id = $('#id').val();

      $.ajax({
            type: 'POST',
            url: 'adamDcm',
            data: {'id': id,'StartDate':StartDate ,'endDate':endDate,'UniqueReportId':UniqueReportId},
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
          success: function (data) {
             
            console.log(data); 
              // if(response.status =='success') {
              //   window.location.reload();
              //    // window.location = "{{url('/dcmRedirect?url=')}}"+response.url;
              // } else {
              //     window.location.reload();
              // }
            }
      });
      //setTimeout(CheckProgress(UniqueReportId), 5000);
      setTimeout(function() { CheckProgress(UniqueReportId) }, 900000);
    }
    else{
      alert("Please enter date");
    }
  };

  //Adword
  function adamAdwords() {

    var StartDate = $('#datepickerStart').val();
    var endDate = $('#datepickerEnd').val();

    $(".processingWarning").show();
    $(".processingSuccess").hide();
    $("#msgWarning").html("Report for Adwords is in Processing......");

    var randomNumber=Math.floor(100000 + Math.random() * 900000);
    var UniqueReportId= 'Adw_'+randomNumber;
    
    if (StartDate && endDate ){
      var id = $('#id').val();

        $.ajax({
              type: 'POST',
              url: 'adamAdwords',
              data: {'id': id,'StartDate':StartDate ,'endDate':endDate,'UniqueReportId':UniqueReportId},
              headers: {
                  'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
              },
            success: function (data) {
              console.log(data);
                  //if(response.status =='success') {
                  //  // window.location.reload();
                  //} 
                  //else {
                  //    //window.location.reload();
                 
                  //}
            }
        });
        //setTimeout(CheckProgress(UniqueReportId), 5000);
        setTimeout(function() { CheckProgress(UniqueReportId) }, 600000);
    }
    else{
      alert("Please enter date");
    }

  };

  function testAdamSizMek() {

    var StartDate = $('#datepickerStart').val();
    var endDate = $('#datepickerEnd').val();

    $(".processingWarning").show();
    $(".processingSuccess").hide();
    $("#msgWarning").html("Report for Sizmek is in Processing......");

    var randomNumber=Math.floor(100000 + Math.random() * 900000);
    var UniqueReportId= 'Siz_'+randomNumber;

    if (StartDate && endDate ){
        var id = $('#id').val();
        $.ajax({
          type: 'POST',
          url: 'testAdamSizMek',
          data: {'id': id,'StartDate':StartDate,'endDate':endDate, 'UniqueReportId':UniqueReportId},
          headers: {
              'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
          },
          success: function (data) {
            console.log(data);
            if(response.status =='success') {
                 //window.location.reload();
            } else {
                 //window.location.reload();
            }
          }
        });
        //setTimeout(CheckProgress(UniqueReportId), 5000);
        setTimeout(function() { CheckProgress(UniqueReportId) }, 300000);
    }
    else{
      alert("Please enter date");
    }

  };

  function adamBingAds() {
    var StartDate = $('#datepickerStart').val();
    var endDate = $('#datepickerEnd').val();

    //alert(StartDate);
    //alert(endDate);  
    $(".processingWarning").show();
    $(".processingSuccess").hide();
    $("#msgWarning").html("Report for BingAds is in Processing......");

    var randomNumber=Math.floor(100000 + Math.random() * 900000);
    var UniqueReportId= 'Bing_'+randomNumber;

    if (StartDate && endDate ){
        var id = $('#id').val();
        // ---------------------------------------------
        var start_dt = StartDate.split("-");
        var start_year = start_dt[0];
        var start_month = start_dt[1];
        var start_date = start_dt[2];
        // ---------------------------------------------
        var end_dt = endDate.split("-");
        var end_year = end_dt[0];
        var end_month = end_dt[1];
        var end_date = end_dt[2];
        // ---------------------------------------------

        $.ajax({
              type: 'POST',
              url: 'adamBingAdsApi',
              data: {'id': id,'start_year':start_year,'start_month':start_month,'start_date':start_date,'StartDate':StartDate,
                              'end_year':end_year,'end_month':end_month,'end_date':end_date,'endDate':endDate, 'UniqueReportId':UniqueReportId
                    },
              headers: {
                  'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
              },
            success: function (data) {
                
                console.log(data);

                // if(response.status =='success') {
                //     window.location.reload();
                // } else {
                //     window.location.reload();
                // }
            }
        });
        //setTimeout(CheckProgress(UniqueReportId), 5000);
        setTimeout(function() { CheckProgress(UniqueReportId) }, 300000);
    }
    else{
      alert("Please enter date");
    }
  };
  // ---------------taboola---------------------------
  function AdamTaboola(breakdownEle) {

    var StartDate = $('#datepickerStart').val();
    var endDate = $('#datepickerEnd').val();
    //var breakdownElement = $(breakdownEle).val();
    //alert(breakdownElement);

    $(".processingWarning").show();
    $(".processingSuccess").hide();
    $("#msgWarning").html("Report for Taboola is in Processing......");

    var randomNumber=Math.floor(100000 + Math.random() * 900000);
    var UniqueReportId= 'Taboola_'+randomNumber;

    if (StartDate && endDate ){
      var id = $('#id').val();
      $.ajax({
            type: 'POST',
            url: 'AdamTaboola',
            //data: {'id': id,'StartDate':StartDate ,'endDate':endDate, 'breakdownElement':breakdownElement },
            data: {'id': id,'StartDate':StartDate ,'endDate':endDate,'UniqueReportId':UniqueReportId},
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
          success: function (data) {
            console.log(data);
            //------------------------------------- 
             // if(response.status =='success') {
             //      window.location.reload();
             //  } else {
             //      window.location.reload();
             //  }
          }
      });
      //setTimeout(CheckProgress(UniqueReportId), 5000);
      setTimeout(function() { CheckProgress(UniqueReportId) }, 300000);
    }
    else{
      alert("Please enter date");
    }
  };
  //----------------end taboola-----------------------
  // ---------------bing---------------------------
  function AdamOutbrain(breakdownEle) {

    var StartDate = $('#datepickerStart').val();
    var endDate = $('#datepickerEnd').val();
    //var breakdownElement = $(breakdownEle).val();
    //alert(breakdownElement);
    $(".processingWarning").show();
    $(".processingSuccess").hide();
    $("#msgWarning").html("Report for Outbrain is in Processing......");

    var randomNumber=Math.floor(100000 + Math.random() * 900000);
    var UniqueReportId= 'Outbrain_'+randomNumber;

    if (StartDate && endDate ){
      var id = $('#id').val();
      $.ajax({
            type: 'POST',
            url: 'AdamOutbrain',
            //data: {'id': id,'StartDate':StartDate ,'endDate':endDate, 'breakdownElement':breakdownElement },
            data: {'id': id,'StartDate':StartDate ,'endDate':endDate,'UniqueReportId':UniqueReportId},
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
          success: function (data) {
            console.log(data);
            //------------------------------------- 
             // if(response.status =='success') {
             //      window.location.reload();
             //  } else {
             //      window.location.reload();
             //  }
          }
      });
      //setTimeout(CheckProgress(UniqueReportId), 5000);
      setTimeout(function() { CheckProgress(UniqueReportId) }, 300000);
    }
    else{
      alert("Please enter date");
    }
  };
  //----------------end bing-----------------------
  // ===================================================

  function add() {

    var StartDate = $('#datepicker').val();
    alert(StartDate);
    if (StartDate){
      var id = $('#id').val();
      $.ajax({
            type: 'POST',
            url: 'fbApis',
            data: {'id': id,'StartDate':StartDate},
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
          success: function (data) {
            console.log(data);
            //------------------------------------- 
             // if(response.status =='success') {
             //      window.location.reload();
             //  } else {
             //      window.location.reload();
             //  }
            //------------------------------------- 
            // if(response.status =='success') {
            //         if(response.message =='update') {
            //             swal(
            //                 '',
            //                 'Suceessfully Updated',
            //                 'success'
            //               );
            //         } else {
            //           swal(
            //               '',
            //               'Please try with some other date',
            //               'success'
            //             );
            //         }
            //         setTimeout(function(){
            //             window.location.reload();
            //         }, 4000);
            // } else {
            //         swal(
            //           '',
            //           'Please try with some other date',
            //           'success'
            //         );
            //         setTimeout(function(){
            //             window.location.reload();
            //         }, 4000);
            // }
          }
      });
    }
    else{
      alert("Please enter date");
    }
  };


  function addBingAds() {
    var StartDate = $('#datepicker').val();
    if (StartDate){
        var id = $('#id').val();
        var dt = StartDate.split("-");
        var year = dt[0];
        var month = dt[1];
        var date = dt[2];

        $.ajax({
              type: 'POST',
              url: 'bingAdsApi',
              data: {'id': id,'year':year,'month':month,'date':date,'StartDate':StartDate},
              headers: {
                  'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
              },
            success: function (response) {
                if(response.status =='success') {
                    window.location.reload();
                } else {
                    window.location.reload();
                }
            }
        });
    }
    else{
      alert("Please enter date");
    }
  };

  function addTaboola() {
    var StartDate = $('#datepicker').val();
    if (StartDate){        
      var id = $('#id').val();
      $.ajax({
            type: 'POST',
            url: 'taboolaApi',
            data: {'id': id,'StartDate':StartDate},
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
          success: function (data) {
            console.log(data);
            // if(response.status =='success') {
            //       window.location.reload();
            // } else {
            //      window.location.reload();
            // }
          }
      });
    }
    else{
      alert("Please enter date");
    }
  };


  function addDcm() {
    var StartDate = $('#datepicker').val();
    if (StartDate){
      var id = $('#id').val();

      $.ajax({
            type: 'POST',
            url: 'dcmRedirectApi',
            data: {'id': id,'StartDate':StartDate},
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
          success: function (data) {
             
            console.log(data); 
              // if(response.status =='success') {
              //   window.location.reload();
              //    // window.location = "{{url('/dcmRedirect?url=')}}"+response.url;
              // } else {
              //     window.location.reload();
              // }
            }
      });
    }
    else{
      alert("Please enter date");
    }
  };

  function addOutBrain() {
    var StartDate = $('#datepicker').val();
    if (StartDate){
        var id = $('#id').val();

        $.ajax({
              type: 'POST',
              url: 'outBrainApi',
              data: {'id': id,'StartDate':StartDate},
              headers: {
                  'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
              },
            success: function (data) {
                console.log(data);
                // if(response.status =='success') {
                //     window.location.reload();
                // } else {
                //    window.location.reload();
                // }
            }
        });
    }
    else{
      alert("Please enter date");
    }

  };

  //Adword
  function addAdWords() {

    var StartDate = $('#datepicker').val().replace(new RegExp('-', 'g'), '');
    if (StartDate){
        var id = $('#id').val();

        $.ajax({
              type: 'POST',
              url: 'adWordsApi',
              data: {'id': id,'StartDate':StartDate},
              headers: {
                  'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
              },
            success: function (data) {
              console.log(data);
                  //if(response.status =='success') {
                  //  // window.location.reload();
                  //} 
                  //else {
                  //    //window.location.reload();
                 
                  //}
            }
        });
    }
    else{
      alert("Please enter date");
    }

  };


  //SizMek
  function addSizMek() {

    var StartDate = $('#datepicker').val();
    if (StartDate){
        var id = $('#id').val();

        $.ajax({
          type: 'POST',
          url: 'addSizMek',
          data: {'id': id,'StartDate':StartDate},
          headers: {
              'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
          },
          success: function (data) {
            console.log(data);
            // if(response.status =='success') {
            //     window.location.reload();
            // } else {
            //     window.location.reload();
            // }
          }
        });
    }
    else{
      alert("Please enter date");
    }
  };

  function testSizMek() {

    var StartDate = $('#datepicker').val();
    if (StartDate){
        var id = $('#id').val();

        $.ajax({
          type: 'POST',
          url: 'testSizMek',
          data: {'id': id,'StartDate':StartDate},
          headers: {
              'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
          },
          success: function (data) {
            console.log(data);
            // if(response.status =='success') {
            //     window.location.reload();
            // } else {
            //     window.location.reload();
            // }
          }
        });
    }
    else{
      alert("Please enter date");
    }

  };


  function addCityBank() {

    alert('Processing Please Wait');
    $(".citybankprocess").prop('disabled',true);
     var StartDate = $('#datepicker').val();
     if (StartDate){
           var id = $('#id').val();
             //alert(id);
          //
          $.ajax({
            type: 'POST',
            url: 'addCityBank',
            data: {'id': id,'StartDate':StartDate},
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
            success: function (response) {
              //console.log(data);
              if(response.status =='success') {
                  window.location.reload();
              } else {
                  window.location.reload();
              }
            }
          });
     }
     else{
      alert("Please enter date");
    }

  };

//addGoogleAnalytics
// function addGoogleAnalytics() {

//   var StartDate = $('#datepicker').val();
//   var id = $('#id').val();

//   $.ajax({
//     type: 'POST',
//     url: 'addGoogleAnalytics',
//     data: {'id': id,'StartDate':StartDate},
//     headers: {
//         'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
//     },
//     success: function (data) {
//       console.log(data);
//       // if(response.status =='success') {
//       //     window.location.reload();
//       // } else {
//       //     window.location.reload();
//       // }
//     }
//   });
// };

// --------------------GoogleAnalytics-----------------------------
// function addGoogleAnalytics() {
//   var StartDate = $('#datepicker').val();
//   var id = $('#id').val();
//   $.ajax({
//     type: 'POST',
//     url: 'addGoogleAnalytics',
//     data: {'id': id,'StartDate':StartDate},
//     headers: {
//         'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
//     },
//     success: function (data) {
//       console.log(data);
//     }
//   });
//   // ----------again hit it after 5mins=300000----------------
//   setTimeout(function() {
//     $.ajax({
//     type: 'POST',
//     url: 'addGoogleAnalytics',
//     data: {'id': id,'StartDate':StartDate},
//     headers: {
//         'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
//     },
//     success: function (data) {
//       console.log(data);
//     }
//   });

//   }, 300000); 
// };
// -------
//-----------processGAReport

// function processGAReport()
// {
//   // -----------First TimeOut
//   setTimeout(function() {
//     var id = $('#id').val();
//     $.ajax({
//       type: 'POST',
//       url: 'processGAReport',
//       data: {'id': id},
//       headers: {
//           'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
//       },
//       success: function (data) {
//         console.log(data);
//       }
//     });
//   }, 600000);
//   // -----------First TimeOut
// }
// --------------------end GoogleAnalytics-----------------------------
// function addDBM_Display(){
//   var StartDate = $('#datepicker').val();
//   var id = $('#id').val();
//   //alert("first hit");
//   $.ajax({
//     type: 'POST',
//     url: 'addDBM_Display',
//     data: {'id': id,'StartDate':StartDate},
//     headers: {
//         'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
//     },
//     success: function (data) {
//       console.log(data);
//     }
//   });

//   // setTimeout(function() {
//   //   //alert("second hit 30");
//   //   $.ajax({
//   //     type: 'POST',
//   //     url: 'addDBM_Display',
//   //     data: {'id': id,'StartDate':StartDate},
//   //     headers: {
//   //         'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
//   //     },
//   //     success: function (data) {
//   //       console.log(data);
//   //     }
//   //   });
//   // }, 30000); 
// };

// function processDBM_DispalyReport()
// {
  // -----------First TimeOut
  //setTimeout(function() {

  //alert('processDBM_DispalyReport');
  //   var id = $('#id').val();
  //   $.ajax({
  //     type: 'POST',
  //     url: 'processDBM_DispalyReport',
  //     data: {'id': id},
  //     headers: {
  //         'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
  //     },
  //     success: function (data) {
  //       console.log(data);
  //     }
  //   });   
  // }, 60000);
  // -----------First TimeOut
//}


  function addAdWordsY() {

    var StartDate = $('#datepicker').val().replace(new RegExp('-', 'g'), '');
    if (StartDate){
        var id = $('#id').val();

        $.ajax({
              type: 'POST',
              url: 'addAdWordsY',
              data: {'id': id,'StartDate':StartDate},
              headers: {
                  'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
              },
            success: function (response) {
                console.log(response);
                if(response.status =='success') {
                     //window.location.reload();
                } else {
                    //window.location.reload();
                }
            }
        });
    }
    else{
      alert("Please enter date");
    }
  };

  
  function performance_stats_report(){

    if($("#datepickerStart").val() == ""){
      var from_date_timestamp = new Date(new Date().setDate(new Date().getDate()-2));
      var from_date_month = from_date_timestamp.getMonth() + 1;
      if(from_date_timestamp.getDate()<10){from_date_timestamp.getDate()='0'+from_date_timestamp.getDate()};
      if(from_date_month<10){from_date_month='0'+from_date_month};
      var from_date = from_date_timestamp.getFullYear() + '-' + from_date_month + '-' + from_date_timestamp.getDate();
    }else{
      var from_date = $("#datepickerStart").val();
    }

    if($("#datepickerEnd").val() == ""){
      var to_date_timestamp = new Date(new Date().setDate(new Date().getDate()-1));
      var to_date_month = to_date_timestamp.getMonth() + 1;
      if(to_date_timestamp.getDate()<10){to_date_timestamp.getDate()='0'+to_date_timestamp.getDate()};
      if(to_date_month<10){to_date_month='0'+to_date_month};
      var to_date = to_date_timestamp.getFullYear() + '-' + to_date_month + '-' + to_date_timestamp.getDate();
    }else{
      var to_date = $("#datepickerEnd").val();
    }


    var http_request = {'from_date' : from_date , 'to_date' : to_date};

    $.ajax({
      type: 'POST',
      url: "{{URL::to('yahoo_report_performance_stats')}}",
      data: http_request,
      headers: {
          'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
      },
      async:false,
      success: function (data) {  
        console.log(data);
      }  
    });
  }


  // ---------------Hotstar---------------------------
  function Hotstar(breakdownEle) {

    var StartDate = $('#datepickerStart').val();
    var endDate = $('#datepickerEnd').val();
    //var breakdownElement = $(breakdownEle).val();
    //alert(breakdownElement);

    $(".processingWarning").show();
    $(".processingSuccess").hide();
    $("#msgWarning").html("Report for Hotstar is in Processing......");

    var randomNumber=Math.floor(100000 + Math.random() * 900000);
    var UniqueReportId= 'Hotstar_'+randomNumber;

    if (StartDate && endDate ){
      var id = $('#id').val();
      $.ajax({
            type: 'POST',
            url: 'hotstar',
            //data: {'id': id,'StartDate':StartDate ,'endDate':endDate, 'breakdownElement':breakdownElement },
            data: {'id': id,'StartDate':StartDate ,'endDate':endDate, 'UniqueReportId':UniqueReportId},
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
          success: function (data) {
            console.log(data);
            //------------------------------------- 
             // if(response.status =='success') {
             //      window.location.reload();
             //  } else {
             //      window.location.reload();
             //  }
          }
      });
      //setTimeout(CheckProgress(UniqueReportId), 5000);
      setTimeout(function() { CheckProgress(UniqueReportId) }, 300000);
    }
    else{
      alert("Please enter date");
    }
  };
  //----------------end Hotstar-----------------------
  
</script>
