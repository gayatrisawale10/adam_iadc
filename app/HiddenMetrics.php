<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HiddenMetrics extends Model
{
	public $timestamps = false;
	
	protected $primaryKey = 'id';

    public $fillable = ['HiddenMetricsName','ApiMetricsName','Publisher_ID','PublisherMetricsParameter','typeFlag','APIParamColumnName'];

	public $table = "HiddenMetrics";
}
