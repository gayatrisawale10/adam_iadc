<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
    <script data-require="jquery@*" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script data-require="datatables@*" data-semver="1.10.12" src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  
  <link data-require="datatables@*" data-semver="1.10.12" rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
  <link rel="stylesheet" href="http://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" />
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

 <script src="../js/sweetalert.js"></script>
  <link href="../css/sweetalert.css" rel="stylesheet">
<style>
div.addRow{
      line-height: 45px;
      background-color: #fff;
      padding-left: 10px;
      border-bottom: 1px solid;
      border-top: 1px solid #e5e5e5;
}
.myrightalign{
      text-align: right;
      width: 70px;
}
</style>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'IADC') }}</title>
    
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
      <link href="/css/app.css" rel="stylesheet">
    <style type="text/css">
    
        .job_header {
                margin-left: 375px;
                font-size: 60px;
                /*position: absolute;*/
                
        }
        .job_logo{
            padding-top:10px;
        }
        .margin-top-20{
            margin-top: 20px;
        }
        .navbar-fixed-bottom, .navbar-fixed-top {
        width:100%;
        /* position: relative; */
        margin-top:30px;
        }
        
        th {
            text-align:center;
        }

        .btn-default.btn-on-3.active{background-color:#337ab7;color: #fff;}
        .btn-default.btn-off-3.active{background-color: #337ab7;color: #fff;}
        div.addRow{
          line-height: 45px;
        background-color: #fff;
        padding-left: 10px;
        border-bottom: 1px solid;
        border-top: 1px solid #e5e5e5;}
    </style>  
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header"></div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                       <img class="home_logo" src="{{asset('/image/logo_final.png') }}" width='60' height="60">
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        <li><a href="{{ url('/Campaign') }}">Campaign</a></li>
                        <li><a href="{{ url('/Publisher') }}">Publisher</a></li> 
                        <li><a href="{{ url('/Client') }}">Client</a></li> 
                 
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <table id="example" class="display" width="100%" cellspacing="0">
    <thead>
     <tr>
            <th width='46px'>Genre</th>
            <th width='46px'>Publisher Name</th>
            <th width='46px'>Ad Type</th>
            <th width='46px'>Ad Section</th>
            <th width='46px'>Deal Type</th>
            <th width='46px'>Delivery Mode</th>
            <th width='46px'>Planned <br/> Impression</th>
            <th width='46px'>Planned Impression DIT</th>
            <th width='46px'>Planned Clicks</th>
            <th width='46px'>Planned<br/>Clicks<br/>DIT</th>
            <th width='46px'>Planned<br/>Unique<br/>Reach</th>
            <th width='46px'>Planned<br/> Unique <br/>ReachDIT</th>
            <th width='46px'>Net <br/>Unit<br/> Rate</th>
            <th width='46px'>Net Cost</th>          
            <th width='46px'>Action</th>    </thead>
        </tr> <form method='post' action=''>
          <?php $i = 1; ?>
          @foreach($CampaignPublisherdetails_index as $CampaignPublisher_index_data)
          <tr align="center">
            <td>
          <select name="genreName" id = 'genre_{{$i}}' class="form-control" disabled>
             <option value="" data-for="selected-genre">Select</option>
                  @foreach($Genre as $bp)
                  <option {{ $CampaignPublisher_index_data->Genre_ID==$bp->id ? 'selected' : '' }} value="{{ $bp->id }}" data-for="selected-genre">{{ $bp->GenreName }}</option>
                  @endforeach
          </select>
        </td>
        <td>
          <select name="publisherName" id = 'publisher_{{$i}}' class="form-control " disabled>
             <option value="" data-for="selected-publisher">Select</option>
               @foreach($Publisher as $p)
                  <option {{ $CampaignPublisher_index_data->Publisher_ID==$p->id ? 'selected' : '' }} value="{{ $p->id }}" data-for="selected-genre">{{ $p->PublisherName }}</option>
                  @endforeach
        </td>
        <td>
           <select name="adTypes" id ="Types_{{$i}}" class="form-control">
             <option value="" data-for="adTypes">Select</option>
                  @foreach($AdTypes as $Atype)
                  <option {{ $CampaignPublisher_index_data->AdTypes_ID==$Atype->id ? 'selected' : '' }} value="{{ $Atype->id }}" data-for="selected-bp1">{{ $Atype->AdTypesName }}</option>
                  @endforeach
          </select>
        </td>
        <td>
           <select name="adSection" id='Section_{{$i}}' class="form-control">
             <option value="" data-for="adSection">Select</option>
                  @foreach($AdSection as $section)
                  <option {{ $CampaignPublisher_index_data->AdSection_ID==$section->id ? 'selected' : '' }} value="{{ $section->id }}" data-for="selected-bp1">{{ $section->AdSectionName }}</option>
                  @endforeach
          </select>
        </td>
        <td>
           <select  name="dType" id="dTypeID_{{$i}}" class="form-control">
             <option value="" data-for="dType">Select</option>
                  @foreach($DealType as $DType)
                  <option  {{ $CampaignPublisher_index_data->DealType_ID==$DType->id ? 'selected' : '' }} value="{{ $DType->id }}" data-for="selected-bp3">{{ $DType->DealTypeName }}</option>
                  @endforeach
          </select>
        </td>
        <td>
           <select name="mode" id="modeID_{{$i}}" class="form-control">
             <option value="" data-for="mode">Select</option>
                  @foreach($DeliveryMode as $mode)
                  <option {{ $CampaignPublisher_index_data->DeliveryMode_ID==$mode->id ? 'selected' : '' }} value="{{ $mode->id }}" data-for="selected-bp4">{{ $mode->DeliveryModeName }}</option>
                  @endforeach
          </select>
        </td>
        <td> <input class="myrightalign"  id="impression_{{$i}}" type='text' name='PlannedImpressions' value="{{$CampaignPublisher_index_data->PlannedImpressions}}" 
         placeholder = "Planned Impression">

        </td>
        <td>
          <select id='PIDitID_{{$i}}' name="PIDataInputTypeID" class="form-control" disabled>
             <option value="" data-for="PIDataInputTypeID">Select</option>
              @foreach($dataInputType as $pi)
                  <option {{ $CampaignPublisher_index_data->PC_DataInputType_ID==$pi->id ? 'selected' : '' }} value="{{ $pi->id }}" data-for="selected-genre">{{ $pi->DataInputTypeName }}</option>
              @endforeach
          </select>
        </td>       

        <td>
          <input type='text' class="myrightalign" id="Click_{{$i}}" name='PlannedClicks' value="{{$CampaignPublisher_index_data->PlannedClicks}}" placeholder = "PlannedClicks">
        </td>
        <td>
          <select id='PCDitID' name="PCDataInputTypeID" class="form-control" disabled>
             <option value="" data-for="PCDataInputTypeID">Select</option>
              @foreach($dataInputType as $pc)
                  <option {{ $CampaignPublisher_index_data->PI_DataInputType_ID==$pc->id ? 'selected' : '' }} value="{{ $pc->id }}" data-for="selected-genre">{{ $pc->DataInputTypeName }}</option>
              @endforeach
          </select>
        </td>
        <td>
          <input type='text'  class="myrightalign" id="uni_{{$i}}" name='PlannedUniqueReach' value="{{$CampaignPublisher_index_data->PlannedUniqueReach}}" placeholder = "Planned Unique Reach">
        </td>
        <td>
          <select id="PURDitID_{{$i}}" name="PURDataInputTypeID" class="form-control" disabled>
             <option value="" data-for="PURDataInputTypeID">Select</option>
              @foreach($dataInputType as $pur)
                  <option {{ $CampaignPublisher_index_data->PUR_DataInputType_ID==$pur->id ? 'selected' : '' }} value="{{ $pur->id }}" data-for="selected-genre">{{ $pur->DataInputTypeName }}</option>
              @endforeach
          </select>
        </td>
        <td>
          <input type='text'  class="myrightalign" id='UnitRate_{{$i}}' name='NetUnitRate' value="{{$CampaignPublisher_index_data->NetUnitRate}}"  placeholder = "Net Unit Rate">
        </td> 
        <td>
        <input type='text' class="myrightalign" id="Cost_{{$i}}" name='NetCost' value="{{$CampaignPublisher_index_data->NetCost}}" placeholder = "Net Cost">
        </td>     
        <input type='hidden' class="myrightalign" name='get_Campaign_id' value="{{$get_Campaign_id}}" id="getCampaignId_{{$i}}">
        <input type='hidden' class="myrightalign" name='CampaignPublisher_ID' value="{{$CampaignPublisher_index_data->CampaignPublisher_ID}}" id="getCampaignPubliserId_{{$i}}">
        <input type='hidden' class="myrightalign" name='LineItem_id' value="{{$CampaignPublisher_index_data->id}}" id="getLineitemId_{{$i}}">

        <td>
            <button type="submit" class="btn green " onclick="updateCP({{$i}});"><i  class="fa fa-pencil-square" aria-hidden="true"></i></button>
        <button type="submit" class="btn btn-default" onclick="Delete({{$i}});">  <i class="fa fa-minus-square" aria-hidden="true"></i> </button>
        </td>
        
    </tr>
    <?php $i++; ?>
   @endforeach  </form>
  </table>

   <form method='post' action=''>
  <table id="newRow" style="display:none;">
    <tbody>
      <tr>
        <td>
          <select id="genreName" name="genreName" class="form-control map-genre-publisher">
             <option value="" data-for="selected-genre">Select</option>
                  @foreach($Genre as $bp)
                  <option value="{{ $bp->id }}" data-for="selected-genre">{{ $bp->GenreName }}</option>
                  @endforeach
          </select>
        </td>
        <td>
          <select id="publisherName" name="publisherName" class="form-control map-dit-publisher publishers">
             <option value="" data-for="selected-publisher">Select</option>               
          </select>

        </td>
        <td>
           <select id="adTypes" name="adTypes" class="form-control">
             <option value="" data-for="adTypes">Select</option>
                  @foreach($AdTypes as $Atype)
                  <option value="{{ $Atype->id }}" data-for="selected-bp1">{{ $Atype->AdTypesName }}</option>
                  @endforeach
          </select>
        </td>
        <td>
           <select id="adSection" name="adSection" class="form-control">
             <option value="" data-for="adSection">Select</option>
                  @foreach($AdSection as $section)
                  <option value="{{ $section->id }}" data-for="selected-bp1">{{ $section->AdSectionName }}</option>
                  @endforeach
          </select>
        </td>
        <td>
           <select id="dType" name="dType" class="form-control">
             <option value="" data-for="dType">Select</option>
                  @foreach($DealType as $DType)
                  <option value="{{ $DType->id }}" data-for="selected-bp3">{{ $DType->DealTypeName }}</option>
                  @endforeach
          </select>
        </td>
        <td>
           <select id="mode" name="mode" class="form-control">
             <option value="" data-for="mode">Select</option>
                  @foreach($DeliveryMode as $mode)
                  <option value="{{ $mode->id }}" data-for="selected-bp4">{{ $mode->DeliveryModeName }}</option>
                  @endforeach
          </select>
        </td>
        <td> <input type='text' class="myrightalign" name='PlannedImpressions' value="" id="impressions" placeholder = "Planned Impression">

        </td>
        <td>
          <select id="PIDataInputTypeID" name="PIDataInputTypeID" class="form-control">
             <option value="" data-for="PIDataInputTypeID">Select</option>
          </select>
        </td>
        

        <td><input type='text' class="myrightalign" name='PlannedClicks' value=""  id="clicks" placeholder = "PlannedClicks"></td>
        <td>
          <select id="PCDataInputTypeID" name="PCDataInputTypeID" class="form-control">
             <option value="" data-for="PCDataInputTypeID">Select</option>
          </select>
        </td>
        <td><input type='text' class="myrightalign" name='PlannedUniqueReach' value="" id="unique" placeholder = "Planned Unique Reach">
        </td>
        <td>
          <select id="PURDataInputTypeID" name="PURDataInputTypeID" class="form-control">
             <option value="" data-for="PURDataInputTypeID">Select</option>
          </select>
        </td>
        <td><input class="myrightalign" type='text' name='NetUnitRate' value="" id="NetUnitRate" placeholder = "Net Unit Rate"></td> 
        <td><input  class="myrightalign" type='text' name='NetCost' value="" id="NetCost" placeholder = "Net Cost"></td>          
       
        <td><button type="submit" class="btn green saveCP"><i  class="fa fa-save" aria-hidden="true"></i></button>
        <button type="button" class="btn btn-default ">  <i class="fa fa-minus-square removerow" aria-hidden="true"></i> </button></td>
      </tr>
    </tbody>
  </table>
 <input type='hidden' name='get_Campaign_id' value="{{$get_Campaign_id}}" id="get_Campaign_id">
  </form>
        </div>
        </div>
        <div class="navbar-fixed-bottom pull-left navbar-default navbar-collapse" style="padding:-100px 0px; border-top:1px solid #ddd;">
            <strong> &nbsp; &nbsp;Powered by &nbsp;</strong><a href="http://www.e-stonetech.com" target="_blank"> e-Stone Information Technology Pvt. Ltd. &copy; 2016-17</a>
    
        </div>
         <script>
    $(document).ready(function() {
      var table;
      var url = '';
      table = $('#example').DataTable({

        ajax: url,
        rowReorder: {
          dataSrc: 'genre',
          selector: 'tr'
        },
        columns: [{
          data: 'genre'
        }, {
          data: 'publisher'
        }, {
          data: 'type'
        }, {
          data: 'section'
        },{
          data: 'Dtype'
        },{
          data: 'DMode'
        },{
          data: 'Impression'
        },{
          data: 'idit'
        },{
          data: 'click'
        },{
          data: 'cdit'
        },{
          data: 'Unique'
        },{
          data: 'udit'
        },{
          data: 'NetUnit'
        },{
          data: 'NetCost'
        }, {
          data: 'delete'
        }]
      });
      $("#example").on("click", "td .removerow", function(e) {
        table.row($(this).closest("tr")).remove().draw();
         $("#addRow").show();
      });
    $('#example').css('border-bottom', 'none');
    $('<div class="addRow"><button id="addRow">Add New Row</button></div>').insertAfter('#example');

      // add row
      $('#addRow').click(function() {
        var rowHtml = $("#newRow").find("tr")[0].outerHTML
        console.log(rowHtml);
        table.row.add($(rowHtml)).draw();
        $("#addRow").hide();

      });
    });



$(document).on('change', '.map-genre-publisher', function() {
  $('.publishers').empty();
  var selectedOption = $(this).val();
  $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });

  $.ajax({
        type: 'POST',
        url: '{{ url('/selectPublisherGenreMap') }}', 
        data: { "_token": "{{ csrf_token() }}",'genre_id':selectedOption},
        success: function (response ) {
              $(".publishers").append('<option value="" data-for="selected-publisher">Select</option>');
              $.each(response.publisher, function (key, value) {
           
                $(".publishers").append('<option value="'+value.id+'" data-for="selected-publisher">'+value.PublisherName+'</option>');
              });
        }
    });
});

$(document).on('change', '.map-dit-publisher', function() {
  $('#PIDataInputTypeID').empty();
  $('#PCDataInputTypeID').empty();
  $('#PURDataInputTypeID').empty();
  var selectedOption = $(this).val();
  $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });

  $.ajax({
        type: 'POST',
        url: '{{ url('/selectDataTypePublisherMap') }}', 
        data: { "_token": "{{ csrf_token() }}",'p_id':selectedOption},
        success: function (response ) {

              $("#PIDataInputTypeID").append('<option value="" data-for="selected-publisher">Select</option>');
              $.each(response.dataInputType, function (key, value) {
           
                $("#PIDataInputTypeID").append('<option value="'+value.id+'" data-for="selected-PIDataInputTypeID">'+value.DataInputTypeName+'</option>');
              });
              $("#PCDataInputTypeID").append('<option value="" data-for="selected-PCDataInputTypeID">Select</option>');
              $.each(response.dataInputType, function (key, value) {
           
                $("#PCDataInputTypeID").append('<option value="'+value.id+'" data-for="selected-PCDataInputTypeID">'+value.DataInputTypeName+'</option>');
              });
              $("#PURDataInputTypeID").append('<option value="" data-for="selected-PURDataInputTypeID">Select</option>');
              $.each(response.dataInputType, function (key, value) {
           
                $("#PURDataInputTypeID").append('<option value="'+value.id+'" data-for="selected-PURDataInputTypeID">'+value.DataInputTypeName+'</option>');
              });
        }
    });
});

$(document).on("click",".saveCP",function() {

  var genreID = $('#genreName').val();
  var publisherID = $('#publisherName').val();
  var adTypesID = $('#adTypes').val();  
  var adSectionID = $('#adSection').val();
  var dTypeID = $('#dType').val();
  var modeID = $('#mode').val();
  var impressions = $('#impressions').val();
  var clicks = $('#clicks').val();
  var unique = $('#unique').val();
  var piDit = $('#PIDataInputTypeID').val();
  var pcDit = $('#PCDataInputTypeID').val();
  var purDit = $('#PURDataInputTypeID').val();
  var CampaignID= $('#get_Campaign_id').val();
  var netUnit = $('#NetUnitRate').val();
  var netCost = $('#NetCost').val();

   $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') } 
      })                          

      $.ajax({
          type: 'POST',
          url: '../CampaignPublisherStore',
          data: {"_token": "{{ csrf_token() }}",'genreID': genreID,'publisherID':publisherID,'adTypesID':adTypesID,'adSectionID': adSectionID,'dTypeID':dTypeID,'modeID':modeID,'impressions': impressions,'clicks':clicks,'unique':unique,'CampaignID':CampaignID,'piDit':piDit,'pcDit': pcDit,'purDit':purDit,'netUnit':netUnit,'netCost': netCost },
          success: function (response) 
          {
            console.log(response);
            if(response.status =='success')
            {
              swal(
                    '',
                    'Saved Suceessfully',
                    'success'
                  );
              setTimeout(function(){
                      window.location.reload();
              }, 4000);              
            }
        }          
       });

});

function updateCP (id) {
  var genreID = $('#genre_'+id).val();
  var publisherID = $('#publisher_'+id).val();
  var adTypesID = $('#Types_'+id).val();  
  var adSectionID = $('#Section_'+id).val();
  var dTypeID = $('#dTypeID_'+id).val();
  var modeID = $('#modeID_'+id).val();
  var impressions = $('#impression_'+id).val();
  var clicks = $('#Click_'+id).val();
  var unique = $('#uni_'+id).val();
  var piDit = $('#PIDitID_'+id).val();
  var pcDit = $('#PCDitID_'+id).val();
  var purDit = $('#PURDitID_'+id).val();
  var CampaignID= $('#getCampaignId_'+id).val();
  var netUnit = $('#UnitRate_'+id).val();
  var netCost = $('#Cost_'+id).val();
  var getCampaignPubliserId = $('#getCampaignPubliserId_'+id).val();
  var getLineitemId = $('#getLineitemId_'+id).val();


   $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') } 
      })                          

      $.ajax({
          type: 'POST',
          url: '../CampaignPublisherUpdate',
          data: {"_token": "{{ csrf_token() }}",'genreID': genreID,'publisherID':publisherID,'adTypesID':adTypesID,'adSectionID': adSectionID,'dTypeID':dTypeID,'modeID':modeID,'impressions': impressions,'clicks':clicks,'unique':unique,'CampaignID':CampaignID,'piDit':piDit,'pcDit': pcDit,'purDit':purDit,'netUnit':netUnit,'netCost': netCost,'getCampaignPubliserId':getCampaignPubliserId,'getLineitemId':getLineitemId },
          success: function (response) 
          {
            console.log(response);
            if(response.status =='success')
            {
              swal(
                    '',
                    'Suceessfully Updated',
                    'success'
                  );
              setTimeout(function(){
                      window.location.reload();
              }, 4000);
            }
          }
       });
};

function Delete (id) {
  
  var getCampaignPubliserId = $('#getCampaignPubliserId_'+id).val();
  var getLineitemId = $('#getLineitemId_'+id).val();


   $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') } 
      })                          

      $.ajax({
          type: 'POST',
          url: '../CampaignPublisherDelete',
          data: {"_token": "{{ csrf_token() }}",'getCampaignPubliserId':getCampaignPubliserId,'getLineitemId':getLineitemId },
          success: function (response) 
          {
           if(response.status =='success')
            {
              swal(
                    '',
                    'Deleted Suceessfully',
                    'success'
                  );
              setTimeout(function(){
                      window.location.reload();
              }, 3000);
            }
          }
       });
};


  </script>
    </body> 
</html>