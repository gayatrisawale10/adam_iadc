<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserClient extends Model
{

	public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['Client_ID','User_ID','CreatedOn','ModifiedOn','ModifiedBy','isActive'];

	public $table = "dbo.UserClient";

}
