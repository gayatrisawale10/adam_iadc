<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublisherType extends Model
{

	public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['PublisherTypeName','CreatedOn','ModifiedOn','ModifiedBy','isActive'];

	public $table = "dbo.PublisherType";

}
