<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use App\Http\Requests;

use App\User;
use App\Role;

class UserCrudController extends Controller
{
    public function create()
    	{
    			 $role = role::all();
				 $selectrole=0;
				 
				return view('UserCrud.create',compact('role','selectrole'));
    	}

        public function store(Request $request)
        {	   
			
			  
			     $this->validate($request, [
							'name' => 'required', 
							'email' =>'required|email|unique:users',
							'password' => 'required',
							'contactnumber'=> 'required|min:8|max:12', 							
						 ]);
            
				    $u_id=Auth::user()->id;
						$user = user::create([
						'name' => $request->input('name'),
						'email' =>$request->get('email'),
						'password' => bcrypt($request->input('password')),
						'contactnumber'=>$request->input('contactnumber'),
						'role_id' => $request->input('role'),
						'modifiedby'=> $u_id,
						]);
           
            return redirect()->route('UserCrud.index')
                        ->with('success','User created successfully');
        
        }

        public function index(Request $request)
        { 

          
          $users= User::orderBy('name')->paginate(5);
          return view('UserCrud.index',compact('users'))
              ->with('i', ($request->input('page', 1) - 1) * 5);
        }

        public function edit($u_id)
        {

           $useredit= user::find($u_id);
		   $role = role::all();
		   $selectrole=0;
		   $result= $useredit->role_id;
		   $selectrole=$result;

    	     return view('UserCrud.edit',compact('useredit','role','selectrole'));
         }
		 
		 
		 /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $this->validate($request, [
            'name' => 'required',
			     'email' =>'required|email',
			     'password' => 'required', 
			     'contactnumber'=> 'required|min:8|max:12', 
          ]);
		
    
		  $u_id=Auth::user()->id;
       
		User::where([
        ['id','=',$id],
        ])->update(['name' =>$request->get('name'),
					'contactnumber'=>$request->get('contactnumber'),
					'email' =>$request->get('email'),
					'password' =>bcrypt($request->get('password')),
					'role_id' =>$request->get('role') ,
					'modifiedby' =>$u_id,]);

		
		// ->update(['role_id' =>$request->get('r') ,
        // 'status_id'=>$statusid,'u_id' =>$request->get('employeerole'),]
        return redirect()->route('UserCrud.index')
                        ->with('success','User updated successfully');
    }
		 
		 
		 
		 
         public function show($u_id)
        {

          $showuser= user::find($u_id);
          return view('UserCrud.show',compact('showuser'));
        }
         public function destroy($u_id)
      {
          User::find($u_id)->delete();
          return redirect()->route('UserCrud.index')
                        ->with('success','User deleted successfully');
      }
       
       
}
