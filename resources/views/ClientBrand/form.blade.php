<div class="row col-md-6">	
  <div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
      <strong>Brand Name:</strong>
    </div>
      <div class="form-group">       
        @if($brandDetails_index)
            {!! Form::text('BrandName', $brandDetails_index->BrandName, array('placeholder' => 'Brand name','class' => 'form-control','required'=>'', 'maxlength'=>'50')) !!}
        @else    
             {!! Form::text('BrandName', Null, array('placeholder' => 'Brand name','class' => 'form-control','required'=>'' , 'maxlength'=>'50')) !!}
        @endif     
      </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
        <strong>Client Name :</strong>

        {!! Form::hidden('Client_ID', $getClientDetails->id, array('placeholder' => '','class' => 'form-control','required'=>'')) !!}

        {!! Form::text('ClientName', $getClientDetails->ClientName, array('placeholder' => 'Brand name','class' => 'form-control','required'=>'' , 'readonly'=>'' )) !!}
    </div>
    <div class="form-group col-xs-6 col-sm-6 col-md-6"></div>  
  </div>   
  <div class="col-xs-12 col-sm-12 col-md-12">    
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary pull-left">Save</button>
      <div class="pull-left" style="padding-left:20px;">
          <a class="btn btn-primary" href="{{ URL::route('ClientBrand.getAllBrandsToDisplay',
                                                       $c_id)}}"> Back</a>
      </div>
    </div>
  </div>
</div>
