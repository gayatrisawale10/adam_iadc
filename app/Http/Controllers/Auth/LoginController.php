<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use DB;
use App\Users;
use Session;

use Validator;
Use Redirect;


use Auth;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
       
    
     use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    
    protected function authenticated($request, $user)
    {
        
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password , 'isActive'=>1])) {
            return redirect()->intended('home');
        }else{
            $check = Users::where('email', $request->email)->first();
            
            //convert object to array
            $check = (array) $check;

            if(count($check) > 0){
                Session::flash('error', 'Wrong Credentials');
            }else{
                Session::flash('error', 'Wrong email Id Provided');
            }

            return Redirect::back();
        }
        
        // if($user->email === 'admin@gmail.com') {
        //     return redirect()->intended('/Campaign');
        // } else {
        //     $user = DB::table('users')->where('email', '=', $user->email)->where('password','=',$user->password)->get();
        //     $user = json_decode($user);
        //     return redirect()->intended('/PublisherHome/'.$user[0]->id);
        // }   
    }
    
    public function logout(Request $request) {
        Auth::logout();
        session()->flush();
        session()->regenerate();      
        Session::forget('user');
      return redirect()->intended('/');
    }

}
