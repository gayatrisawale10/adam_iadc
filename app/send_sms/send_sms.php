<?php
namespace App\send_sms {    
    use Illuminate\Support\Facades\Auth;
    use Session;
    use App\sms\smsconfig;
    use App\sms\sms_log;
    class send_sms {     
        public function sys_send_sms($mobileno,$message) {

            $r=0;
            foreach($mobileno as $mob){
//                echo '<br/>';
//                echo '<br/>Job id= '.$job_id;
                echo '<br/>SMS Text=  '.$message;
                echo '<br/>Mobile No= '.$mob;
                echo '<br/>';
                $sent_result[$r]= $this->send_sms($mob,$message);
                 $r++;
            }    
           return $sent_result;
        }
        // private function set_parameter($paramList)
        // {            
        //     for($i=0; $i<10; $i++){
        //         if(isset($paramList[$i])){
        //             $parameter[$i] = $paramList[$i];
        //         }else
        //         {
        //             $parameter[$i] = '';   
        //         }
        //     }
        //     return $parameter;
        // }
        
        private function send_sms($mobileno,$message)
        {                   
            $return_value = null;
            
            // send sms to hspsms
            $url = "http://sms.hspsms.com/sendSMS?";
            $username = urlencode("rmp");
            $message = urlencode($message);        
            //$message = "XXXXXXXXXX";
            $sendername = urlencode("RMPRMP");
            $smstype = urlencode("TRANS");
            //$numbers = $mobile;
            $numbers = urlencode($mobileno);
            $apikey = urlencode("25bfd003-5600-4f0d-9c2b-779ae5601a55");
            $link= $url.'username='.$username.'&message='.$message.'&sendername='.$sendername.'&smstype='.$smstype.'&numbers='.$numbers.'&apikey='.$apikey;

            $result=""; 
            $responseCode='';
            $msgid='';
            try
            {
                $url = $link;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);  
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);            
                $result =curl_exec($ch);
                $read = json_decode($result, true);

                //dd($read);
                if($read)
                {   
                    foreach($read as $i => $v)
                    {
                        if(array_keys($v)[0]=='responseCode')
                            $responseCode= $v['responseCode'];                
                        if(array_keys($v)[0]=='msgid')
                            $msgid= $v['msgid'];    
                    }

                }
                else
                {
                    echo "Failed to send SMS. Error 455<br/>"; 
                    $return_value = 'error';
                }                
            }
            catch(Exception $e)
            {
                echo "Failed to send SMS. Error 456<br/>";
                echo($e);  
                $return_value = 'error';
            }
            $response=$responseCode;
            $msg_ref=$msgid; 
            // send sms done   

            //create sms log
            //$sent_by = Auth::user()->id;  

            if(isset(Auth::user()->id))
            {
               
                $sent_by = Auth::user()->id;  
               
            }   
            else
            {
                $sent_by =-1;  
                 //echo "<br/>Not set= ".$sent_by ;
            }
            //echo  $sent_by;
            //exit();     
            $requestData= [
            'mobile'=> $mobileno,
            'sent_text'=> $message,
            'job_id'=>null,
            'sent_by'=>$sent_by,
            'response'=> $response,
            'msg_ref'=> $msg_ref
            ];   

            sms_log::create($requestData);
            Session::flash('flash_message', 'sms_log added!');
            //done sms log 
            
            //return sms sent status
            if($return_value == 'error')
            {
                return false;
            }
            else
            {
                return true;
            }
             
        }
    }
}
?>
