<?php
namespace App\Http\Controllers;
use autoload;
//require_once "./vendor/autoload.php";

use Illuminate\Http\Request;
use validator;
use Illuminate\Support\Facades\Auth;
use DB;
Use Redirect;
use Carbon\Carbon;

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\FacebookRequest;

use FacebookAds\Api;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\User;

use File;
use Zipper;
use Maatwebsite\Excel\Facades\Excel;
use SoapVar;
use SoapFault;
use Exception;
use DateTime;
use Cookie;
use Microsoft\BingAds\Auth\AuthorizationData;
use Microsoft\BingAds\Auth\OAuthTokenRequestException;
use Microsoft\BingAds\Auth\OAuthWebAuthCodeGrant;
use Microsoft\BingAds\Auth\WebAuthHelper;
use Illuminate\Support\Facades\Storage;

// Specify the Microsoft\BingAds\V12\Reporting classes that will be used.
use Microsoft\BingAds\V12\Reporting\ReportRequestStatusType;

// Specify the Microsoft\BingAds\Auth classes that will be used.
use Microsoft\BingAds\Auth\ServiceClient;
use Microsoft\BingAds\Auth\ServiceClientType;

// Specify the Microsoft\BingAds\Samples classes that will be used.
use App\V12\AuthHelper;
use App\V12\ReportRequestLibrary;

//Adwords
//use App\Ads\AdWords\v201806\Reporting\DownloadCriteriaReportWithSelector;
use App\Ads\AdWords\v201809\Reporting\DownloadCriteriaReportWithSelector;
use App\Ads\AdWords\v201809\Reporting\ParallelReportDownload;
use Google\AdsApi\AdWords\AdWordsSession;

//DCM
use App\v3\examples\Reports\DownloadReportFile;
use App\v3\examples\Reports\GenerateReportFile;
use App\v3\examples\Reports\CreateStandardReport;
use App\v3\auth\AuthenticateUsingServiceAccount;
//DBM
use App\dbm\DBMReportProcessing;

//GA
use App\GA\GoogleAnalyticsReportProcessing;

use Google_Client;
use Google_Service_Dfareporting;
use Google_Service_Analytics;
use Google_Service_DoubleClickBidManager;
use Google_Service_DoubleClickBidManager_Resource_Queries;
use Google_Service_DoubleClickBidManager_RunQueryRequest;
use Google_Service_AnalyticsReporting;

use Google_Service_AnalyticsReporting_DateRange;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_Dimension;
use Google_Service_AnalyticsReporting_ReportRequest;
use Google_Service_AnalyticsReporting_GetReportsRequest;

//For SizMek
use SoapClient;
use SoapHeader;

//define('STDIN',fopen("php://stdin","r"));
define('STORE_ON_DISK', false, true);
define('TOKEN_FILENAME', 'tokens.dat', true);
ini_set('max_execution_time', '1000');

	
class ApiController extends Controller
{

  	// This redirect URI allows you to copy the token from the success screen.
  	const OAUTH_REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob';

  	// Location where authorization credentials will be cached.
  	const TOKEN_STORE = 'auth-sample.dat'; 


    public function addFbApi(Request $request ,$a='')
    {	
		ini_set('memory_limit', '-1');
		set_time_limit(0);
    	//this is check for wether startdate is oming from cron or by onclick.
     	if($a==null)
     	{
     		$startDate = $request['StartDate']; 
     	} else {
     		$startDate = $a['StartDate']; 
     	}
	   
	   	$PublisherID =  DB :: table('Publisher')
		 							->where('Publisher.PublisherName','=','Facebook')
		 							->select('Publisher.id')->get();
		$Publisher_ID = json_decode($PublisherID,true);
		$p_id = $Publisher_ID[0]['id'];
				
	    $hiddenMetricsID_Out = DB :: table('HiddenMetrics')                     
	                  		->where('HiddenMetrics.Publisher_ID' ,'=',$p_id)
	                  		->where('HiddenMetrics.typeFlag' ,'=',2)
	                      	->select('HiddenMetrics.PublisherMetricsParameter')->get();
	    $hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);
	    
	    //default parameter need to pass.
	    $arrayMetricNames =  array(
					'ad_id',
					'ad_name',
					'adset_id',
					'adset_name',
					'campaign_id',
					'campaign_name');                 
	    
	    foreach($hiddenMetricsID_Result as $key=>$value) {
	            array_push($arrayMetricNames,$value['PublisherMetricsParameter']);
	    }
	    
	    $fields = array_filter($arrayMetricNames);
	    //echo"<pre>"; print_r( $fields ); //exit;

	    $tillDate = date('Y-m-d');     	

	    // --------- multiple facebook App
	    $FBIDsPath = public_path('FBIDs.csv');
     	config(['excel.import.startRow' => 1 ]);
		$FBIDsData = \Excel::load($FBIDsPath)->get()->toArray();	    
	    //echo"<pre>"; print_r($FBIDsData); exit;

		
   		$app_id = '501471827045469';
		$app_secret = '913996bb865f39529a553a45ed0b2f0e';
		$app_name = 'Live_IADC';
		$app_token = 'EAAHIFgIdrF0BAIVm5Y2V3a3GM3mxw6rmtUtnEffdWhmoTp82gB6rN1HoWFHt4rZC2Qvr0J664jVtZAZCeLxZCGzVzWbR8DdPRZAavTRzQFMAi8Y3yRA8VWjgAad1rciZAtsDXUlBuE6PXO230sZBwJ6VIcZBFC28IiosZBmEq6W5agEy1sZBo3lWrc3S0zfOhHDtAZD';
		$app_url_id = '144137076586817';

		$adAccount_Id=[];
		$fb = new Facebook([ 
		  'app_id' => $app_id,
		  'app_secret' => $app_secret,
		]);

		$helper = $fb->getRedirectLoginHelper();
		if (!isset($_SESSION['facebook_access_token'])) {
		  $_SESSION['facebook_access_token'] = null;
		}

		if (!$_SESSION['facebook_access_token']) {
		  	$helper = $fb->getRedirectLoginHelper();
			try {
				$_SESSION['facebook_access_token'] = (string) $helper->getAccessToken();
			} catch(FacebookResponseException $e) {
				// When Graph returns an error
				echo 'Graph returned an error: ' . $e->getMessage();
			    exit;
			} catch(FacebookSDKException $e) {
			 	// When validation fails or other local issues
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
			    exit;
			}
		}

		if ($_SESSION['facebook_access_token']) {
		    echo "You are logged in!";
		} else {
			$permissions = ['ads_management'];
			$loginUrl = $helper->getLoginUrl('http://localhost:8888/marketing-api/', $permissions);
			echo '<a href="' . $loginUrl . '">Log in with Facebook</a>';
		}

		$access_token = $app_token ;			
		$api = Api::init($app_id, $app_secret, $access_token);

		$ch = curl_init();
			
		curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v3.2/".$app_url_id."?fields=adaccounts.limit(500)&access_token=".$access_token."");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
		    echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		$r = json_decode($result);

		//echo "<pre>ApiRes "; print_r($r); exit;

		$i = sizeOf($r->adaccounts->data);
		//echo"<pre>"; print_r($me->getAdAccounts()); 
		$adAccount_Id=[];
		while ( $i >= 1) {
		  array_push($adAccount_Id, $r->adaccounts->data[$i-1]->id);
		  $i--;
		  //echo "The first campaign in the cursor is: ".$cursor[0]->{CampaignFields::NAME}.PHP_EOL;
		}
		echo"<pre>accountIds by Curl : ";  print_r($app_name);  print_r($adAccount_Id);// exit;
		
		/****Curl Code to hit next url to get aal adaccountIds, Mukunda Sathe / Gayatri  Sawale 01-03-2019****/
		
		foreach ($adAccount_Id as  $ad_account_id) {
			//echo"<pre>"; print_r($ad_account_id); exit;

			$api->setLogger(new CurlLogger());
			$params = array(
			  'level' => 'adset',
			  'filtering' => array(),
			  'time_range' => array('since' => $startDate,'until' =>$tillDate),
			  'time_increment' => 1,
			  'limit' =>500,
			  'breakdowns'=> array('age', 'gender')
			);

			$result = json_encode((new AdAccount($ad_account_id))
						->getInsights($fields,$params)
						->getResponse()
						->getContent(),JSON_PRETTY_PRINT);
					
			//return $this->storeApivalue($result,'Facebook');
			$this->storeApivalue($result,'Facebook');

																  
			echo"<pre>After ForeachCall = "; print_r($result);
			
		}
		//exit;
		return response()->json(['status' => 'success'], 200);
	
    }

    //Add BingAds Api
    public function addBingAdsApi(Request $request,$a='')
    {
    	File::delete('reports/keywordperf.zip');
     	$tillDate = date('Y-m-d');

     	if($a==null)
     	{
     		$startDate = array('year'=> $request['year'],'month'=>$request['month'],'day'=>$request['date']);
     	} else {
     		$startDate = $a['StartDate']; 
     	}

     	if($request['id']==1) {
	   			AuthHelper::Authenticate();

    			$GLOBALS['ReportingProxy'] = new ServiceClient(ServiceClientType::ReportingVersion12, $GLOBALS['AuthorizationData'], AuthHelper::GetApiEnvironment());

 				$report = ReportRequestLibrary::GetKeywordPerformanceReportRequest('1306881',$startDate);
				$this->extractKeyZip();
		}

    }

	//extrat zip file of BingsAdv
    public function extractKeyZip()
    {

        $Path = public_path('/reports/keywordperf.zip');

        $extratFilename = \Zipper::make($Path)->listFiles('/\.csv$/i');
        $extratFilename  = substr($extratFilename[0], 0, -4);
		$filename = $extratFilename.'_'.date('ymdhms').'.csv';

		$filePath = public_path('/unzipReport/'.$extratFilename[0]);
        \Zipper::make($Path)->extractTo('unzipReport');
        $directory = public_path('unzipReport');
        $moveToDir = public_path('/reports/unZipReport');

		$i=1;
		if ($handle = opendir($directory)) {
		    while (true == ($file = readdir($handle))) {
		            if($file=="." || $file==".." || $file==$extratFilename[0]) { continue;}
			    $replace = $filename ;
			    File::copy($directory.'/'.$file,$moveToDir.'/'.$replace);
			    $i++;
		    }	//end while
		    closedir($handle);
		  }  //end handle

		$pathFile = 'public/reports/unZipReport/'.$filename;
		config(['excel.import.startRow' => 11 ]);

        $data = \Excel::load($pathFile)->get()->toArray();

		return $this->storeApivalue($data,'Bing');

    }
    //Add Taboola Api
    public function addTaboolaApi(Request $request,$a='')
    {
    	
	    if($a==null)
        {
            $startDate = $request['StartDate']; 
        } else {
            $startDate = $a['StartDate']; 
        }

    	//$url = 'https://backstage.taboola.com/backstage/oauth/token?client_id=b729d19e5715450c8ba4ccceeb5e09a8&client_secret=0c9631a502d74a17b47d95dddf7e6414&username=Abdul.b@interactiveavenues.com&password=​abdul123​&grant_type=client_credentials';

    	$url = 'https://backstage.taboola.com/backstage/oauth/token?client_id=fc8f6c5e3c7d444d8f09eee0144f0a64&client_secret=a56a12861c18428c8b62e6da1de6300f&username=gaanalytics.ia@gmail.com&password=IAnalytics@18&grant_type=client_credentials';
	    // Get cURL resource
	    $curl = curl_init();
	    // Set some options
	    curl_setopt_array($curl, array(
	        CURLOPT_RETURNTRANSFER => 1,
	        CURLOPT_URL => $url,
	        CURLOPT_POST => 1
	    ));
	    // Send the request & save response to $resp
	    $resp = curl_exec($curl);
	    //echo'<pre>cUrl'; print_r($resp); exit;

	    // Close request to clear up some resources
	    curl_close($curl);
	    if($resp){
	        $arr = array(json_decode($resp));
	        $access_token = $arr[0]->access_token;

		        
	        $url = "https://backstage.taboola.com/backstage/api/1.0/interactiveavenues-network/reports/campaign-summary/dimensions/campaign_day_breakdown?start_date=".$startDate."&end_date=".date('Y-m-d');


	        $ch = curl_init();

	        curl_setopt_array($ch, array(
	        CURLOPT_RETURNTRANSFER => 1,
	        CURLOPT_URL => $url,
	        CURLOPT_CUSTOMREQUEST => 'GET',
	        CURLOPT_HTTPHEADER => array('Content-Type:application/json',
	                      				'Authorization: Bearer '.$access_token)
	        ));

	        $resps = curl_exec($ch);

	        // Close request to clear up some resources
	        curl_close($ch);
	        if($resps){
	            $result = json_decode($resps,true);
	            //echo'<pre>res'; print_r($result); exit;
	            return $this->storeApivalue($result['results'],'Taboola');
	        } else {
	         echo "failed";
	        }

	    } else {
	     	echo "failed";
	    }

    }


    public function dcmRedirectApi(Request $request,$a='')
    {
     		
     	if($a==null)
     	{
     		define('STDIN',fopen("php://stdin","r"));
     		$_SESSION['startDate'] = $request['StartDate']; 
     	}else {
     		$_SESSION['startDate'] = $a['StartDate']; 
     	}
			//echo $request['startDate']; exit;
			//$startDate = $request['startDate'];        
		//	$_SESSION['startDate'] = $request['startDate'];        

		ob_start();
		
		ini_set('implicit_flush', 1);

		$_SESSION['user_profile_id'] = '5018824';
		$authAcct = new AuthenticateUsingServiceAccount();
		$authAcct->run(app_path('v3\dcmiadc-fcdf17511686.json'),'dcmiadc@dcmiadc.iam.gserviceaccount.com');

	    //$_SESSION['startDate'] = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') -DAYS, date('Y')));

	    $createStandardReport = new CreateStandardReport();
		$createStandardReport->run();

	    $generateReportFile = new GenerateReportFile();
		$generateReportFile->run();

		$downloadReportFile = new DownloadReportFile();
		$downloadReportFile->run();

		$filePath = public_path('/reports/dcmReports/example_report.csv');


	    //config(['excel.import.startRow' => 10 ]);
	    config(['excel.import.startRow' => 11 ]);
	    $data = \Excel::load($filePath)->get()->toArray();
	    //echo "<pre>data"; print_r($data); exit;
		return $this->storeDcmApivalue($data,'DCM');
	}


    //add Outbrain API
    public function addOutbrainApi(Request $request,$a='')
    {
		
    	if($a==null){
     		$startDate = $request['StartDate']; 
     	} else {
     		$startDate = $a['StartDate']; 
     	}
     	$limit=500;
     	
		//$url = "https://api.outbrain.com/amplify/v0.1/reports/marketers/0029d0114d57db0ca15ee9c9ac17d1e643/campaigns/periodic?from=".$startDate."&to=".date('Y-m-d');

		$url = "https://api.outbrain.com/amplify/v0.1/reports/marketers/003f64bfcd2829f3510f5d3e21787dab6e/campaigns/periodic?from=".$startDate."&to=".date('Y-m-d')."&limit=".$limit;

		//https://api.outbrain.com/amplify/v0.1/reports/marketers/003f64bfcd2829f3510f5d3e21787dab6e/campaigns/periodic?from=2019-01-01&to=2019-04-03&limit=500

		//https://api.outbrain.com/amplify/v0.1/reports/marketers/003f64bfcd2829f3510f5d3e21787dab6e/campaigns/periodic?from=2019-01-01&to=2019-04-03

        $ch = curl_init();

        curl_setopt_array($ch, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array('Content-Type:application/json',
                      //'Authorization: Basic bWFyc2hhbGwuZGF2aWRAY2FkcmVvbi5jb206SXZoYW5tdXNlXzA2')
                        'Authorization: Basic Z2FhbmFseXRpY3MuaWFAZ21haWwuY29tOklBbmFseXRpY3NAMTg=')
        ));

        $resps = curl_exec($ch);
        //echo '<pre>123'; print_r($resps); exit;
        curl_close($ch);
        if($resps){
            $result = json_decode($resps,true);
            //'<pre>'; print_r($result); exit;
            $data = [];
            foreach($result['campaignResults'] as $camp){
            	//echo'<pre>'; print_r($camp); exit;           		
				$url = "https://api.outbrain.com/amplify/v0.1/campaigns/".$camp['campaignId'];
                $ch = curl_init();

                curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array('Content-Type:application/json',
                              //'Authorization: Basic bWFyc2hhbGwuZGF2aWRAY2FkcmVvbi5jb206SXZoYW5tdXNlXzA2')
                			  'Authorization: Basic Z2FhbmFseXRpY3MuaWFAZ21haWwuY29tOklBbmFseXRpY3NAMTg=')	
                ));

                $campName = curl_exec($ch);
                curl_close($ch);
                if($campName){
                   $campName = json_decode($campName,true);
                }
                //echo'<pre>'; print_r($campName); exit;           
           		foreach($camp['results'] as $perDateResult){
           			//echo'<pre>'; print_r($perDateResult); exit;
           			$data['campaignName'] = $campName['name'];
                	$data['campaignId'] = $camp['campaignId'];           			
           			$data['metadata'] = $perDateResult['metadata'];
           			array_push($data['metadata'],$data['campaignId']);
                    array_push($data['metadata'],$data['campaignName']);
           			$data['metrics'] = $perDateResult['metrics'];
           			$results[] = array_merge($data['metadata'],$data['metrics']);
           		}	
           		//echo'<pre>'; print_r($results); exit;	
           	} 
           	//echo'<pre>'; print_r($results); exit;	
           	return $this->storeApivalue($results,'Outbrain');
            // foreach($result['campaignResults'] as $camp){
            //     foreach($camp['results'] as $res){
            //         $url = "https://api.outbrain.com/amplify/v0.1/campaigns/".$camp['campaignId'];
            //         $ch = curl_init();
            //         curl_setopt_array($ch, array(
            //         CURLOPT_RETURNTRANSFER => 1,
            //         CURLOPT_URL => $url,
            //         CURLOPT_CUSTOMREQUEST => 'GET',
            //         CURLOPT_HTTPHEADER => array('Content-Type:application/json',
            //                       //'Authorization: Basic bWFyc2hhbGwuZGF2aWRAY2FkcmVvbi5jb206SXZoYW5tdXNlXzA2')
            //         			  'Authorization: Basic Z2FhbmFseXRpY3MuaWFAZ21haWwuY29tOklBbmFseXRpY3NAMTg=')	
            //         ));
            //         $campName = curl_exec($ch);
            //         // Close request to clear up some resources
            //         curl_close($ch);
            //         if($campName){
            //             $campName = json_decode($campName,true);
            //         }
            //     }
            //         $data['campaignName'] = $campName['name'];
            //         $data['campaignId'] = $camp['campaignId'];
            //         $data['metadata'] = $res['metadata'];
            //         array_push($data['metadata'],$data['campaignId']);
            //         array_push($data['metadata'],$data['campaignName']);
            //         $data['metrics'] = $res['metrics'];
            //         $results[] = array_merge($data['metadata'],$data['metrics']);
            // }
            //echo '<pre>'; print_r($results); exit;
            //---------return $this->storeApivalue($results,'Outbrain');
        } else {
         echo "failed";
        }
    }

	//Add AsWords Api
    public function addAdWordsApi(Request $request,$a='')
    {
    	ini_set('MAX_EXECUTION_TIME', '-1');
    	set_time_limit(0);

    	$date= new DateTime();
    	$current_time=$date->getTimestamp();
    	$pubName='Adwords';

    	define('STDIN',fopen("php://stdin","r"));
    	if($a==null)
     	{
     		$startDate = $request['StartDate']; 
     	} else {
     		$startDate = $a['StartDate']; 
     	}

     	$accountManagerIDsPath = public_path('accountManagerIDs.csv');
     	config(['excel.import.startRow' => 1 ]);
		$accountManagerIDsData = \Excel::load($accountManagerIDsPath)->get()->toArray();
		//echo "<pre>data"; print_r($accountManagerIDsData); exit;
    	foreach ($accountManagerIDsData as $accountManagerID ) {
    		//echo "<pre>"; print_r($accountManagerID['clientcustomerid']); exit;
    	 	//$file = Storage::put( 'myfile.ini', $accountManagerID);
	    	$accountClientcustomerid=$accountManagerID['clientcustomerid'];		
	    	$dataAccountManagerID='
			[ADWORDS]
			; Required AdWords API properties. Details can be found at:
			; https://developers.google.com/adwords/api/docs/guides/basic-concepts#soap_and_xml
			;NEON Details
			developerToken = "7S0zFtOqIXJVP3ZqUtRYtg"

			;manager Id
			clientCustomerId ='.$accountClientcustomerid.'
		
			[OAUTH2]
			; NEON Details
			clientId = "666733807117-srfc7e5h1mnejcvrkdf1f73cu50cfhhi.apps.googleusercontent.com"
			clientSecret = "nNM1pGWZbBQgqWPaSa5KlRYQ"
			refreshToken = "1/_2Hjz77sQVIvPEqbt-RoLoL2VX5tOH0Ez3Eoc8DfMNI" 

			[SOAP]
			; Optional SOAP settings. See SoapSettingsBuilder.php for more information.
			; compressionLevel = <COMPRESSION_LEVEL>

			[CONNECTION]
			; Optional proxy settings to be used by requests.
			; If you dont have username and password, just specify host and port.
			; proxy = "protocol://user:pass@host:port"
			; Enable transparent HTTP gzip compression for all reporting requests.
			; enableReportingGzip = false

			[LOGGING]
			; Optional logging settings.
			; soapLogFilePath = "path/to/your/soap.log"
			; soapLogLevel = "INFO"
			; reportDownloaderLogFilePath = "path/to/your/report-downloader.log"
			; reportDownloaderLogLevel = "INFO"
			; batchJobsUtilLogFilePath = "path/to/your/bjutil.log"
			; batchJobsUtilLogLevel = "INFO" ';
	
    		 $file = Storage::disk('public_adwords')->put('adsapi_php.ini', $dataAccountManagerID);
    		//--------- download multiple report for a manager Acoount-------------------------------
	    	$url = new ParallelReportDownload();
	    	$customerIds=$url->main($startDate, $pubName);
	    	//echo "<pre>API customerIds "; print_r($customerIds); exit; 
	     	//--------- download multiple report for a manager Acoount-------------------------------    	
	    	
	    	//echo"<pre>for "; print_r($pubName); exit;
	    	foreach ($customerIds as $customerId ) {
	    		$adReportfilePath = public_path('reports\adWordReports\adwords\adgroup_'.$pubName.'_'.$customerId.'.csv');	
	    		//echo"<pre>"; print_r($adReportfilePath); exit;
	    		config(['excel.import.startRow' => 2 ]);
	        	$data = \Excel::load($adReportfilePath)->get()->toArray();
	        	$response =  $this->storeApivalue($data,'AdWords');    		
	    		//echo"<pre>"; print_r($response); //exit; 
	    		
	     		if ($response['status']=='success') {			
				 	$new_path=public_path('reports\adWordReports\adwords\adProcessedReport\P_adgroup_'.$accountClientcustomerid.'_'.$pubName.'_'.$customerId.'_'.$current_time.'.csv');	
				 	$move= File::move($adReportfilePath, $new_path);
				 }
				 else{
				 	$new_path=public_path('reports\adWordReports\adwords\adErroredReport\E_adgroup_'.$accountClientcustomerid.'_'.$pubName.'_'.$customerId.'_'.$current_time.'.csv');	
				 	$move= File::move($adReportfilePath, $new_path);
				 }	
	    	}

    	}
     	
    	return response()->json(['status' => 'success'], 200);    		
    }

    public function storeApivalue($result,$publisher)
    {
		//echo"<pre>"; print_r($result); exit;
		$current_time = Carbon::now()->toDateTimeString();
		$todaysDate = date('Y-m-d');

    	if(!is_array($result)){
    		$data = json_decode($result,true);
			$output = $data['data'];
    	}else{
    		$output = $result;
    	}

    	$PublisherID =  DB :: table('Publisher')
	 							->where('Publisher.PublisherName','=',$publisher)
	 							->select('Publisher.id')
	 							->first();
	 	$p_id = $PublisherID->id;
 		// ########################################################################################################
 		$HiddenMatricsArray=[]; 
 		//echo"<pre>out1 "; //print_r($out); //exit;
 		foreach ($output[0] as  $key=>$val) {	
			$hiddenMetricsID_Out = DB :: table('HiddenMetrics')
				            	->where('HiddenMetrics.ApiMetricsName' ,'=',$key)
								->where('HiddenMetrics.Publisher_ID' ,'=',$p_id)
				         	    ->select('HiddenMetrics.id as HiddenMetrics_id')
				         	    ->first();
			if(!empty($hiddenMetricsID_Out))
			{
				$HiddenMatricsArray[$key]=$hiddenMetricsID_Out->HiddenMetrics_id;
			}		  
		} 
		//echo"<pre>hdd "; //dd($HiddenMatricsArray);
		//exit;
 		// ########################################################################################################
    	foreach ($output as  $out)
    	{			
	 		if($publisher == 'Facebook')	 			
	 		{
	    		$campaign_name = $out['campaign_name'];
	    		$campaign = $out['campaign_id'];
	    		$lineItemName = $out['adset_name'];
	    		$lineItemID = $out['adset_id'];
	    		$date_start = $out['date_start'];
	    		$liID =  substr($out['adset_name'], 0, 11);

	    		$PublisherID =  DB :: table('Publisher')
	 							->whereIn('Publisher.PublisherName', [$publisher, 'Instagram'])
	 							->select('Publisher.id')
	 							//->get();
	 							->first();
	 			//$Publisher_ID = json_decode($PublisherID,true);
	 			//echo "<pre>"; print_r($Publisher_ID); exit;
	 			$pub_id=[]; 
	 			foreach ($Publisher_ID as $pubkey ) {
	 				//$pub_id[] = $pubkey['id'];
	 				$pub_id[] = $pubkey->id;
	 			}
    		}
    		//echo "<pre>"; print_r($pub_id); print_r($liID); //exit;

			if($publisher == 'Bing')
			{
	    		$campaign_name = $out['campaignname'];
	    		$date_start  =$out['timeperiod'];
	           	$lineItemName = $out['adgroupname'];
				$liID =  substr($out['adgroupname'], 0, 11);
	    	}
	    	if($publisher == 'AdWords')
	    	{
	    		$campaign_name = $out['campaign'];
	    		$date_start  =$out['day'];
	           	$lineItemName = $out['ad_group'];
				$liID =  substr($out['ad_group'], 0, 11);
	    	}
	    	if($publisher == 'Taboola') {
	    		//echo"<pre>"; print_r($out); //exit;
	    		$dt = new DateTime($out['date']);
				$date_start = $dt->format('Y-m-d');
	    		$campaign_name = $out['campaign_name'];
	    		$campaign = $out['campaign'];
	    				
 				if($campaign_name) {
 					$liID =  substr($campaign_name, 0, 11);
				} else {
					$liID = '';
				}
				//echo"<pre>"; print_r($liID); exit;
	    	}

	    	if($publisher == 'Outbrain') {
	    		$date_start = $out['fromDate'];
	    		$campaign_name = $out['1'];
	    		$campaign = $out['0'];
	    
 				if($campaign_name) {
 					$liID =  substr($campaign_name, 0, 11);
				} else {
					$liID = '';
				}
				//echo"<pre>"; print_r($liID); exit;
           	}

           	//for outbrain and taboola fetch lineitem.Name and set $lineItemName

	   		if (is_numeric($liID)) {
			       $lineItemIdApi = $liID;
			} else {
			    	$lineItemIdApi = '';
			}

			if($publisher == 'Facebook'){
				echo"inFB";	
				$lineItemID_Out = DB :: table('LineItem')
                            ->join('CampaignPublisher','LineItem.CampaignPublisher_ID','=','CampaignPublisher.id')
                            ->join('Campaign','CampaignPublisher.Campaign_ID','=','Campaign.id')
                            ->join('Publisher','Publisher.id','=','CampaignPublisher.Publisher_ID')
                     		->whereIn('Publisher.id', $pub_id)
                  			->where('LineItem.LineItemID' ,'=',$lineItemIdApi)
                  			->select('LineItem.id')
                  			->get();
 				$lineItem_Result = json_decode($lineItemID_Out,true);
			}
			else{
		  		$lineItemID_Out = DB :: table('LineItem')
	                            ->join('CampaignPublisher','LineItem.CampaignPublisher_ID','=','CampaignPublisher.id')
	                            ->join('Campaign','CampaignPublisher.Campaign_ID','=','Campaign.id')
	                            ->join('Publisher','Publisher.id','=','CampaignPublisher.Publisher_ID')
	                     		->where('Publisher.id' ,'=',$p_id)
	                  			->where('LineItem.LineItemID' ,'=',$lineItemIdApi)
	                  			->select('LineItem.id')
	                  			//->get();
	                  			->first();
	 			//$lineItem_Result = json_decode($lineItemID_Out,true);
	            if($lineItemID_Out!=Null){
	            	$lineItem_Result=$lineItemID_Out->id;      			
	            }
	 		}
 			//echo"<pre>lineItemIdApi "; print_r($lineItem_Result); exit;
	 		
 			foreach ($out as  $key=>$val) {		
 				$HiddenMetrics_id='';		
 				// For facebook, only to get data in Key=Val pair
 				if ($publisher == 'Facebook' && (is_array($val))) {
				 	$val = $val[0]['value'];
				} // End For facebook, only to get data in Key=Val pair
				elseif($key =='1' || $key=='0'){
					$key = 'not';
				} else {
					$key = $key;
				}
				// #####################################################
				//  reoved special characters from value
				if (is_numeric($val)) {
				    $val = $val;
				} else {
				    //$val = str_replace('%', '', $val);
					$val = preg_replace("/[^0-9.]/", "", $val  );
					if($val==Null){
						$val=0;
					}
				}
				// echo"<br>";
				// echo"<pre> value ";
				// print_r($val);

				//print_r($HiddenMatricsArray); 
				// #########################################
				if(array_key_exists($key, $HiddenMatricsArray) ){
					$HiddenMetrics_id=$HiddenMatricsArray[$key];
				}
				else{
					
				}					
				
				// #########################################
				// echo"<pre>key ";print_r($key); echo"<br>";
				// echo"<pre>val ";print_r($val); echo"<br>";
				// echo"<pre>Matrics ";print_r($HiddenMetrics_id);  
				
				if(!empty($HiddenMetrics_id))
				{	//echo"<br>";print_r('if HID');
					if(!empty($lineItem_Result))
					{	//print_r($lineItem_Result); exit;
	 					$LineitemID = $lineItem_Result;
	 					$getLineItemMetricID_Result = DB :: table('LineItemMetrics')
						           	->join('LineItem','LineItem.id','=','LineItemMetrics.LineItem_ID')
						           	->join('HiddenMetrics','HiddenMetrics.id','=','LineItemMetrics.HiddenMetrics_ID')
						           	->where('HiddenMetrics.id','=',$HiddenMetrics_id)
						           	->where('LineItemMetrics.LineItem_ID','=',$LineitemID)
						        	->select('LineItemMetrics.*')
						        	//->get();
						        	->first();
						// echo"<br>";
						// echo"getLineItemMetricID_Result ";
						// print_r($getLineItemMetricID_Result);        	

						if(!empty($getLineItemMetricID_Result))
						{
							$lineItemMetricID = $getLineItemMetricID_Result->id;
						}
						else
						{
							$lineItemMetricID=DB::table('LineItemMetrics')->insertGetId([
							  	'LineItem_ID' => $LineitemID,
							  	'Metrics_ID' => NULL,
							 	'HiddenMetrics_ID' =>$HiddenMetrics_id,
							  	'DataInputType_ID' =>'1',
							  	'PlannedValue' =>NULL,
							  	'CreatedOn' => $current_time,
							  	'ModifiedOn' => $current_time,
							  	'ModifiedBy' => '1',
							  	'isActive' => 1
							]);	
						}
						// echo"<br>";
						// print_r($date_start);

						// echo"LineItemMetrics ";
						// print_r($lineItemMetricID); 

						$getLineItemDaily_Result = DB :: table('LineItemDaily')
						           	->join('LineItemMetrics','LineItemMetrics.id','=','LineItemDaily.LineItemMetrics_ID')
						           	->where('LineItemDaily.Date','=',$date_start)
						           	->where('LineItemDaily.LineItemMetrics_ID','=',$lineItemMetricID)
						        	->select('LineItemDaily.*')
						        	//->get();
						        	->first();

						//$getLineItemDaily_Result = json_decode($getLineItemDaily_Out,true);
						// echo"<br>";
						// echo"getLineItemDaily_Result ";
						// print_r($getLineItemDaily_Result); //exit;

						if($getLineItemDaily_Result)
						{
								//echo"gsss  getLineItemDaily_Result "; //exit;
								$lineItemDailyID=$getLineItemDaily_Result->id;
								$LIDSavedValue=$getLineItemDaily_Result->Value;
								$CompleteLIDRow=$getLineItemDaily_Result;

								// echo"<br>";
								// echo"lineItemDailyID ";
								// print_r($lineItemDailyID);

								if($getLineItemDaily_Result->isLineNumberProcessed==0)
								{
									//print_r("backup");
									DB::table('LineItemDaily_PreviousData')->insert([
							                  'LineItemDaily_ID' => $CompleteLIDRow->id,
							                  'LineItemMetricsID' => $CompleteLIDRow->LineItemMetrics_ID,
							                  'Date' =>$CompleteLIDRow->Date,
							                  'Value' =>$CompleteLIDRow->Value,
							                  'CreatedOn' => $CompleteLIDRow->CreatedOn,
							                  'ModifiedOn' => $CompleteLIDRow->ModifiedOn,
							                  'ModifiedBy' => $CompleteLIDRow->ModifiedBy,
							                  'isLineNumberProcessed' =>$CompleteLIDRow->isLineNumberProcessed,
							                  'isActive' => $CompleteLIDRow->isActive,
							                  'MovedOn'=> $current_time
							        ]);
							        //$lineItemDailyIDArray=array_push($lineItemDailyIDArr, $CompleteLIDRow['id']);	        
								}	

								//check for publisher adword and metrics is cost, then divide by 1M
								if($publisher == 'AdWords' && ($key=='cost'))
								{	echo"<br>";print_r($val);
									$val=$val/(pow(10,6));
									echo"<br>";print_r($val);
								}	
								echo"<pre>Million ";print_r($key);
								

								if($getLineItemDaily_Result->isLineNumberProcessed==1)
								{	
									//print_r("gssss123");
									$getPriviousVal=DB::table('LineItemDaily')
										->where('LineItemDaily.Date' ,'=',$date_start)
		                                ->where('LineItemDaily.LineItemMetrics_ID','=',$lineItemMetricID)
		                                ->where('LineItemDaily.isLineNumberProcessed','=',1)
		                                ->select('LineItemDaily.Value')
		                                ->first();
		                                print_r($getPriviousVal->Value); echo"<br>"; print_r($val); //exit;

		                            $val=$getPriviousVal->Value+$val;

									DB::table('LineItemDaily')
	                                    ->where('LineItemDaily.Date' ,'=',$date_start)
							            ->where('LineItemDaily.LineItemMetrics_ID','=',$lineItemMetricID)

									    ->update([
							                  
							                  'Date' =>$date_start,
							                  'Value' =>$val,
							                  'CreatedOn' => $current_time,
							                  'ModifiedOn' => $current_time,
							                  'ModifiedBy' => '1',
							                  'isLineNumberProcessed' => 1,
							                  'isActive' => 1
							                ]);
								}
								else{
										DB::table('LineItemDaily')
		                                ->where('LineItemDaily.Date' ,'=',$date_start)
							            ->where('LineItemDaily.LineItemMetrics_ID','=',$lineItemMetricID)
									    ->update([
					                  
					                  'Date' =>$date_start,
					                  'Value' =>$val,
					                  'CreatedOn' => $current_time,
					                  'ModifiedOn' => $current_time,
					                  'ModifiedBy' => '1',
					                  'isLineNumberProcessed' => 1,
					                  'isActive' => 1
					                ]);

								}

						}
						else{
								//echo"else Daily insert"; exit;
								DB::table('LineItemDaily')->insert([
							                  'LineItemMetrics_ID' => $lineItemMetricID,
							                  'Date' =>$date_start,
							                  'Value' =>$val,
							                  'CreatedOn' => $current_time,
							                  'ModifiedOn' => $current_time,
							                  'ModifiedBy' => '1',
							                  'isLineNumberProcessed' => 1,
							                  'isActive' => 1
							                ]);
						}
					}					
				}
				else{
					//echo"<br>";print_r('else noID');
				}
	 	 	}
		} 
		DB::table('LineItemDaily')
        ->where('isLineNumberProcessed', '=' ,1)                                
		->update([
						                  
              'CreatedOn' => $current_time,
              'ModifiedOn' => $current_time,
              'ModifiedBy' => '1',
              'isLineNumberProcessed' => 0,
              'isActive' => 1
            ]);

		// //get entry from previous data table
	 // 	$getPreviousData=DB::table('LineItemDaily_PreviousData')->get();

	 // 	foreach ($getPreviousData as $key ) {
	 // 		//echo"<pre>"; print_r($key->Value); exit;
	 // 		$LineItemDaily_ID=$key->LineItemDaily_ID;
	 // 		$prevLineItemDaily_Value=$key->Value;

	 // 		$getNewData=DB::table('LineItemDaily')
	 // 						->where('id','=',$LineItemDaily_ID)
	 // 		                ->get();

	 // 		$newLineDaily_value=$getNewData[0]->Value;
	 		
	 // 		if($prevLineItemDaily_Value==$newLineDaily_value)
	 // 		{
	 // 			//echo"<pre>"; print_r('true'); exit; 
	 // 			DB::table('LineItemDaily_PreviousData')->where('LineItemDaily_ID', '=', $LineItemDaily_ID)->delete();

	 // 		}                
	 // 		//echo"<pre>"; print_r($getNewData[0]->Value); exit;                
	 // 	}
		// //echo"<pre>"; print_r($getPreviousData); exit;
		//return ['status'=> 'success'];
	}

	 public function storeDcmApivalue($result,$publisher)
    {
		$current_time = Carbon::now()->toDateTimeString();
		$todaysDate = date('Y-m-d');
		
    	if(!is_array($result)) {
    		$data = json_decode($result,true);
			$output = $data['data'];
    	} else {
    		$output = $result;
    	}
    	//echo"<pre>Out "; print_r($out); exit; 
    	foreach ($output as  $out) 
    	{
	 		//echo"<pre>Out "; print_r($out); exit; 
	 		$PublisherID =  DB :: table('Publisher')
	 							->where('Publisher.PublisherName','=',$publisher)
	 							->select('Publisher.id')->get();
	 		$Publisher_ID = json_decode($PublisherID,true);
	 		$p_id = $Publisher_ID[0]['id'];

	    	if($publisher == 'DCM') 
	    	{
	    		$date_start = substr($out['date'],0,10);
	    		
	    		$date_string = $out['date'];
	    		$time = strtotime($date_string);
				$date_start = date('Y-m-d',$time);

	    		$placement = $out['placement'];
	    		$apiLineItemId =  substr($placement, 0, 11);
	    	}
	    	//echo"<pre>Date"; print_r($date_start); print_r(gettype($date_start)); exit;
	   		if (is_numeric($apiLineItemId)) 
	   		{
			    $lineItemIdApi = $apiLineItemId;
			} 
			else {
			  	$lineItemIdApi = '';
			}
			//echo"<pre> lineItemIdApi"; print_r($lineItemIdApi); exit;
			
	  		$lineItemID_Out = DB :: table('LineItem')
                            ->join('CampaignPublisher','LineItem.CampaignPublisher_ID','=','CampaignPublisher.id')
                            ->join('Campaign','CampaignPublisher.Campaign_ID','=','Campaign.id')
                     		 ->join('LineItemMetrics','LineItemMetrics.LineItem_ID','=','LineItem.id')
                            ->join('DataInputType','LineItemMetrics.DataInputType_ID','=','DataInputType.id')
                  			->where('LineItem.LineItemID','=',$lineItemIdApi)
                  			->where('DataInputType.DataInputTypeName','=',$publisher)
                  			->select(DB::raw('distinct(LineItem.id)'))->get();
 			$lineItem_Result = json_decode($lineItemID_Out,true);

 			//echo"<pre> Line Result"; print_r($lineItem_Result);
 			//echo"<pre>out "; print_r($out);
 			foreach ($out as  $key=>$val) 
 			{
				$hiddenMetricsID_Out = DB :: table('HiddenMetrics')
					            	->where('HiddenMetrics.ApiMetricsName' ,'=',$key)
									->where('HiddenMetrics.Publisher_ID' ,'=',$p_id)
					         		->select('HiddenMetrics.id as HiddenMetrics_id')->get();
				$hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);

				//echo"<pre> hiddenMetricsID_Result "; print_r($hiddenMetricsID_Result);  //	 print_r($out); //exit;//

				if(!empty($hiddenMetricsID_Result)) 
				{
					//echo"if"; exit;
					$HiddenMetrics_id = $hiddenMetricsID_Result[0]['HiddenMetrics_id'];
					if(!empty($lineItem_Result))
					{						
 						//echo"Not if"; //exit;
 						$LineitemID = $lineItem_Result[0]['id'];
 						$getLineItemMetricID_Out = DB :: table('LineItemMetrics')
					           	->join('LineItem','LineItem.id','=','LineItemMetrics.LineItem_ID')
					           	->join('HiddenMetrics','HiddenMetrics.id','=','LineItemMetrics.HiddenMetrics_ID')
					           	->where('HiddenMetrics.id','=',$HiddenMetrics_id)
					           	->where('LineItemMetrics.LineItem_ID','=',$LineitemID)
					        	->select('LineItemMetrics.*')->get();
						$getLineItemMetricID_Result = json_decode($getLineItemMetricID_Out,true);

						if(!empty($getLineItemMetricID_Result)){
							//echo"Matrixlredy present"; //exit;
							$lineItemMetricID = $getLineItemMetricID_Result[0]['id'];
						} 
						else 
						{
							//echo"Matrixlredy need update"; //exit;
							$LineItemMetrics=DB::table('LineItemMetrics')->insert([
							  'LineItem_ID' => $LineitemID,
							  'Metrics_ID' => NULL,
							  'HiddenMetrics_ID' =>$HiddenMetrics_id,
							  'DataInputType_ID' =>'6',
							  'PlannedValue' =>NULL,
							  'CreatedOn' => $current_time,
							  'ModifiedOn' => $current_time,
							  'ModifiedBy' => '1',
							  'isActive' => 1
							]);

							$getLineItemMetricID_Out1 = DB :: table('LineItemMetrics')
						           	->join('LineItem','LineItem.id','=','LineItemMetrics.LineItem_ID')
						           	->join('HiddenMetrics','HiddenMetrics.id','=','LineItemMetrics.HiddenMetrics_ID')
						           	->where('HiddenMetrics.id','=',$HiddenMetrics_id)
						           	->where('LineItemMetrics.LineItem_ID','=',$LineitemID)
						        	->select('LineItemMetrics.*')->get();
							$getLineItemMetricID_Result1 = json_decode($getLineItemMetricID_Out1,true);
							if(!empty($LineItemMetrics)){
								$lineItemMetricID = $getLineItemMetricID_Result1[0]['id'];
							}
						}
						//echo"<pre>lineItemMetricID All "; print_r($lineItemMetricID); //exit;  

						$getLineItemDaily_Out = DB :: table('LineItemDaily')
					           	->join('LineItemMetrics','LineItemMetrics.id','=','LineItemDaily.LineItemMetrics_ID')
					           	->where('LineItemDaily.Date','=',$date_start)
					           	->where('LineItemDaily.LineItemMetrics_ID','=',$lineItemMetricID)
					        	->select('LineItemDaily.*')->get();

						$getLineItemDaily_Result = json_decode($getLineItemDaily_Out,true);

						//echo"<pre>daily "; print_r($date_start); print_r($getLineItemDaily_Result);
						//print_r($getLineItemDaily_Result);//exit;  
						// for existing entry update with sum, and set isLineItemProcessed=False;
						
						if($getLineItemDaily_Result)
						{
							//echo"entry in daily exist"; exit;

							$lineItemDailyID=$getLineItemDaily_Result[0]['id'];
							$LIDSavedValue=$getLineItemDaily_Result[0]['Value'];
							$CompleteLIDRow=$getLineItemDaily_Result[0];

							if($getLineItemDaily_Result[0]['isLineNumberProcessed']==0)
							{
								DB::table('LineItemDaily_PreviousData')->insert([
						                  'LineItemDaily_ID' => $CompleteLIDRow['id'],
						                  'LineItemMetricsID' => $CompleteLIDRow['LineItemMetrics_ID'],
						                  'Date' =>$CompleteLIDRow['Date'],
						                  'Value' =>$CompleteLIDRow['Value'],
						                  'CreatedOn' => $CompleteLIDRow['CreatedOn'],
						                  'ModifiedOn' => $CompleteLIDRow['ModifiedOn'],
						                  'ModifiedBy' => $CompleteLIDRow['ModifiedBy'],
						                  'isLineNumberProcessed' =>$CompleteLIDRow['isLineNumberProcessed'],
						                  'isActive' => $CompleteLIDRow['isActive'],
						                  'MovedOn'=> $current_time
						        ]);
							}	
													
							if($getLineItemDaily_Result[0]['isLineNumberProcessed']==1)
							{	
								//echo "process1";
								$getPriviousVal=DB::table('LineItemDaily')
									->where('LineItemDaily.Date' ,'=',$date_start)
	                                ->where('LineItemDaily.LineItemMetrics_ID','=',$lineItemMetricID)
	                                ->where('LineItemDaily.isLineNumberProcessed','=',1)
	                                ->select('LineItemDaily.Value')
	                                ->first();
	                                //print_r($getPriviousVal->Value); echo"<br>"; print_r($val); exit;

	                            $val=$getPriviousVal->Value+$val;

								DB::table('LineItemDaily')
                                    ->where('LineItemDaily.Date' ,'=',$date_start)
						            ->where('LineItemDaily.LineItemMetrics_ID','=',$lineItemMetricID)

								    ->update([
						                  
						                  'Date' =>$date_start,
						                  'Value' =>$val,
						                  'CreatedOn' => $current_time,
						                  'ModifiedOn' => $current_time,
						                  'ModifiedBy' => '1',
						                  'isLineNumberProcessed' => 1,
						                  'isActive' => 1
						                ]);
							}
							else{	
					    		DB::table('LineItemDaily')
                                ->where('LineItemDaily.Date' ,'=',$date_start)
					            ->where('LineItemDaily.LineItemMetrics_ID','=',$lineItemMetricID)

							    ->update([					                  
					                  'Date' =>$date_start,
					                  'Value' =>$val,
					                  'CreatedOn' => $current_time,
					                  'ModifiedOn' => $current_time,
					                  'ModifiedBy' => '1',
					                  'isLineNumberProcessed' => 1,
					                  'isActive' => 1
					                ]);

							}
							//echo"<pre>dailyId "; print_r($getLineItemDaily_Result[0]['id']); //exit;
							//array_push($lineItemDailyIDArr, $getLineItemDaily_Result[0]['id']);
						}
						else{
							//echo"else123"; exit;
							DB::table('LineItemDaily')->insert([
						                  'LineItemMetrics_ID' => $lineItemMetricID,
						                  'Date' =>$date_start,
						                  'Value' =>$val,
						                  'CreatedOn' => $current_time,
						                  'ModifiedOn' => $current_time,
						                  'ModifiedBy' => '1',
						                  'isLineNumberProcessed' => 1,
						                  'isActive' => 1
						                ]);
						}	
				 	} 
				}
				else{
				 	echo" elseLast "; //exit;
				}
	 	 	}	 	 	
		} 

		DB::table('LineItemDaily')                                    
	    ->update([						                  
          'CreatedOn' => $current_time,
          'ModifiedOn' => $current_time,
          'ModifiedBy' => '1',
          'isLineNumberProcessed' => 0,
          'isActive' => 1
        ]);//exit;//-----------after this false
	
		//echo"<pre>"; print_r("update done"); exit;
			
		//get entry from previous data table
		$getPreviousData=DB::table('LineItemDaily_PreviousData')->get();

	 	foreach ($getPreviousData as $key ) {
	 		//echo"<pre>"; print_r($key->Value); exit;
	 		$LineItemDaily_ID=$key->LineItemDaily_ID;
	 		$prevLineItemDaily_Value=$key->Value;

	 		$getNewData=DB::table('LineItemDaily')
	 						->where('id','=',$LineItemDaily_ID)
	 		                ->get();
	 		$newLineDaily_value=$getNewData[0]->Value;
	 		
	 		if($prevLineItemDaily_Value==$newLineDaily_value)
	 		{
	 			//echo"<pre>"; print_r('true'); exit; 
	 			DB::table('LineItemDaily_PreviousData')->where('LineItemDaily_ID', '=', $LineItemDaily_ID)->delete();
	 		}                
	 		//echo"<pre>"; print_r($getNewData[0]->Value); exit;                
		}
			//echo"<pre>"; print_r($getPreviousData); exit;
	}


	public function addSizMek()
	{
		//echo"<pre>in sizmek"; exit;
	    //some code will be here to hit the server to get excel sheet data
		ini_set('memory_limit', '-1');
       	set_time_limit(0);
		$filesInzipedSizmek = \File::files(public_path('/reports/Sizmek/zipedSizmek'));     
	   // echo"<pre>";  print_r($filesInzipedSizmek); exit;
	    foreach($filesInzipedSizmek as $zipFilePath) 
	    { 
			// -----------------------------------------------
			//$zipFilePath = public_path('/reports/Sizmek/zipedSizmek/Sizmek_IA_Summary_Feed_20181203_20181203.csv.zip');	
		
	        $extratFilename = \Zipper::make($zipFilePath)->listFiles('/\.csv$/i'); 		
	        $extratFilename = $extratFilename[0];
	        //echo"<pre>";  print_r($extratFilename); exit;
	        \Zipper::close($zipFilePath);
	        
	        \Zipper::make($zipFilePath)->extractTo(public_path('/reports/Sizmek/sizmekReports'));
	        \Zipper::close($zipFilePath);
	        $move= File::delete($zipFilePath);

	        $extractedFilePath = public_path('/reports/Sizmek/sizmekReports/'.$extratFilename);

			// -----------------------------------------------------------------------
			//$extractedFilePath = public_path('reports\Sizmek\sizmekReports\Sizmek_IA_Summary_Feed_20190112_20190112.csv');
			//echo"<pre>";  print_r($extractedFilePath); exit;
			config(['excel.import.startRow' => 1]);
			$data = \Excel::load($extractedFilePath)->get()->toArray();
	          //echo"<pre>";  print_r($data); exit;                                  
			//$response = $this->storeSizmekAndGA_DBM_ApiValue($data,'Sizmek');
			$response = ApiController:: storeSizmekAndGA_DBM__AdWordY_ApiValue($data,'Sizmek');

			if ($response['status']=='success') {			
				$new_path=public_path('/reports/Sizmek/ProcessedSizmek/ p_'.$extratFilename);	
				$move= File::move($extractedFilePath, $new_path);
			}
			else{
				$new_path=public_path('/reports/Sizmek/ErroredSizmek/ error_'.$extratFilename);	
				$move= File::move($extractedFilePath, $new_path);
			}	
		}//end foreach	
	}

	public function testSizMek()
	{
		ini_set('memory_limit', '-1');
       	set_time_limit(0);

       	// ------------ Files From D drive--------------	
       	$filesFromDrive=Storage::disk('partitionE')->allFiles();
       	//echo"<pre>fileD ";  print_r($filesFromDrive); //exit;
       	foreach($filesFromDrive as $filesFromDriveName) 
	    { 
	    	//---zipped file put from D Drive to public directory
	    	$contents=Storage::disk('partitionE')->get($filesFromDriveName);
	    			  Storage::disk('public_uploads')->put( $filesFromDriveName, $contents);
	    			  Storage::disk('partitionE')->delete( $filesFromDriveName);  
	    } 	
	    exit;
	    // ------- Files from Zipped Files----------------
		$filesInzipedSizmek = \File::files(public_path('/reports/Sizmek/zipedSizmek'));     
	   // echo"<pre>";  print_r($filesInzipedSizmek); exit;
	    foreach($filesInzipedSizmek as $zipFilePath) 
	    { 
			// -----------------------------------------------
			//$zipFilePath = public_path('/reports/Sizmek/zipedSizmek/Sizmek_IA_Summary_Feed_20181203_20181203.csv.zip');		
	        $extratFilename = \Zipper::make($zipFilePath)->listFiles('/\.csv$/i'); 		
	        $extratFilename = $extratFilename[0];	        
	        //echo"<pre>";  print_r($extratFilename); exit;
	        \Zipper::close($zipFilePath);
	        
	        \Zipper::make($zipFilePath)->extractTo(public_path('/reports/Sizmek/sizmekReports'));
	        \Zipper::close($zipFilePath);

	        //$move= File::delete($zipFilePath);
	        $new_pathZip=public_path('/reports/Sizmek/ProcessedZippedSizmek/'.$extratFilename.'.zip');	
			$move= File::move($zipFilePath, $new_pathZip);

	        $extractedFilePath = public_path('/reports/Sizmek/sizmekReports/'.$extratFilename);
			// -----------------------------------------------------------------------
			//$extractedFilePath = public_path('reports\Sizmek\sizmekReports\Sizmek_IA_Summary_Feed_20190112_20190112.csv');
			//echo"<pre>";  print_r($extractedFilePath); exit;
			
			config(['excel.import.startRow' => 1]);
			$data = \Excel::load($extractedFilePath)->get()->toArray();           
			//echo"<pre>data ";  print_r($data); exit;

			//$response = $this->storeSizmekAndGA_DBM__AdWordY_ApiValue($data,'Sizmek');
			$response = ApiController:: storeSizmekAndGA_DBM__AdWordY_ApiValue($data,'Sizmek');

			if ($response['status']=='success') {			
				$new_path=public_path('/reports/Sizmek/ProcessedSizmek/ p_'.$extratFilename);	
				$move= File::move($extractedFilePath, $new_path);
			}
			else{
				$new_path=public_path('/reports/Sizmek/	/ error_'.$extratFilename);	
				$move= File::move($extractedFilePath, $new_path);
			}	
		}//end foreach	
	}

	public static function storeSizmekAndGA_DBM__AdWordY_ApiValue($result,$publisher)
    {
   		ini_set('memory_limit', '-1');
       	set_time_limit(0);
   		//echo"<pre>GA "; print_r('BY ADy'); //exit;
    	$current_time = Carbon::now()->toDateTimeString();
		$todaysDate = date('Y-m-d');
    	if($publisher=="Sizmek")
	    {
	    	if(!is_array($result)) {
	    		$data = json_decode($result,true);
				$output = $data['data'];
	    	} else {
	    		$output = $result;
	    	}
	    }	
    	//==========change for GA result======================
  	    if($publisher=="GoogleAnalytics")
	    {	
	    	if(!is_array($result)) {
	    		$data = json_decode($result,true);
				$output = $data;
	    	} else {
	    		$output = $result;
	    	}
	    }
     	//==========change for GA result======================
     	//==========change for DBM result======================
  	    if($publisher=="DBM_Others")
	    {		
	    	if(!is_array($result)) {
	    		$data = json_decode($result,true);
				$output = $data['data'];
	    	} else {
	    		$output = $result;
	    	}		    	  		    	
	    }
	    if($publisher=="DBM_Youtube")
	    {
	    	if(!is_array($result)) {
	    		$data = json_decode($result,true);
				$output = $data['data'];
	    	} else {
	    		$output = $result;
	    	}		    	  		    	
	    }

	    if($publisher=="AdWordsY")
	    {
	    	if(!is_array($result)) {
	    		$data = json_decode($result,true);
				$output = $data['data'];
	    	} else {
	    		$output = $result;
	    	}		    	  		    	
	    }
	   //echo"<pre>Output ForLoop ";print_r($output); exit; 
     	//==========change for DBM result======================
     	foreach ($output as  $out)
    	{
	 		$PublisherID =  DB :: table('Publisher')
	 							->where('Publisher.PublisherName','=',$publisher)
	 							->select('Publisher.id')->get();
	 		$Publisher_ID = json_decode($PublisherID,true);
	 		$p_id = $Publisher_ID[0]['id'];

	    	if($publisher=="Sizmek") {
				$date_start = $out['date'];
	    		$placementname = $out['placementname'];
	    		$liID =  substr($out['placementname'], 0, 11);
	    		$DIT=7;
	    	}
	    	
	    	//-----------fot GA--------------------
	    	else if($publisher=="GoogleAnalytics") {	
	    		$date = $out['ga:date']; //20190108	    		 
	    		$yr=substr($date,0,4);
	    		$month=substr($date,4,2);
	    		$day=substr($date,6,2);
	    		$fullDate=$yr.'-'.$month.'-'.$day;

	    		$date_start = $fullDate;
	    		$placementname = $out['ga:medium'];
	    		$liID =  substr($out['ga:medium'], 0, 11);
	    		$DIT=8;
	    	}
	    	//-----------fot GA--------------------
	    	else if($publisher=="DBM_Others") {
				$date_start = $out['date'];
	    		$placementname = $out['line_item'];
	    		$liID =  substr($out['line_item'], 0, 11);
	    		$DIT=9;
	    	}

	    	else if($publisher=="DBM_Youtube") {
				$date_start = $out['date'];
	    		$placementname = $out['line_item'];
	    		$liID =  substr($out['line_item'], 0, 11);
	    		$DIT=10;
	    	}
	    	else if($publisher == 'AdWordsY')
	    	{
	    		$campaign_name = $out['campaign'];
	    		$date_start  =$out['day'];
	           	$lineItemName = $out['ad_group'];
				$liID =  substr($out['ad_group'], 0, 11);
				$DIT=11;
	    	}

	    	//echo"<pre>inner ForLoop ";print_r($out); print_r($liID); exit; 
	    	/* Here need to find out LineItem agianst LID,  
				Previously we are doing that against lineItemName	
	    	*/

			if (is_numeric($liID)) {
			       $lineItemIdApi = $liID;
			} else {
			    	$lineItemIdApi = '';
			}
			
	  		$lineItemID_Out = DB :: table('LineItem')
                ->join('CampaignPublisher','LineItem.CampaignPublisher_ID','=','CampaignPublisher.id')
                ->join('Campaign','CampaignPublisher.Campaign_ID','=','Campaign.id')
                ->join('LineItemMetrics','LineItemMetrics.LineItem_ID','=','LineItem.id')
                ->join('DataInputType','LineItemMetrics.DataInputType_ID','=','DataInputType.id')
               // ->where('LineItem.LineItemName','=',$placementname)
                ->where('LineItem.LineItemID','=',$lineItemIdApi)
                ->where('DataInputType.DataInputTypeName','=',$publisher)
                ->select(DB::raw('distinct(LineItem.id)'))
                ->get();
 			$lineItem_Result = json_decode($lineItemID_Out,true);

 			
 			foreach ($out as  $key=>$val)
 			{	
 				//echo"<pre>"; print_r($key);
 				if (is_numeric($val)) {
				    $val = $val;
				} else {
				    $val = str_replace('%', '', $val);
				}

				$hiddenMetricsID_Out = DB :: table('HiddenMetrics')
						            	->where('HiddenMetrics.ApiMetricsName' ,'=',$key)
										->where('HiddenMetrics.Publisher_ID' ,'=',$p_id)
						         		->select('HiddenMetrics.id as HiddenMetrics_id')->get();
				$hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);

				if(!empty($hiddenMetricsID_Result))
				{
					$HiddenMetrics_id = $hiddenMetricsID_Result[0]['HiddenMetrics_id'];

					if(!empty($lineItem_Result))
					{
	 					$LineitemID = $lineItem_Result[0]['id'];
	 					$getLineItemMetricID_Out = DB :: table('LineItemMetrics')
						    ->join('LineItem','LineItem.id','=','LineItemMetrics.LineItem_ID')
						    ->join('HiddenMetrics','HiddenMetrics.id','=','LineItemMetrics.HiddenMetrics_ID')
						    ->where('HiddenMetrics.id','=',$HiddenMetrics_id)
						    ->where('LineItemMetrics.LineItem_ID','=',$LineitemID)
						    ->select('LineItemMetrics.*')->get();
						$getLineItemMetricID_Result = json_decode($getLineItemMetricID_Out,true);

						if(!empty($getLineItemMetricID_Result))
						{
							$lineItemMetricID = $getLineItemMetricID_Result[0]['id'];
						}
						else
						{
							$LineItemMetrics=DB::table('LineItemMetrics')->insert([
							  'LineItem_ID' => $LineitemID,
							  'Metrics_ID' => NULL,
							  'HiddenMetrics_ID' =>$HiddenMetrics_id,
							  'DataInputType_ID' => $DIT,//'7',
							  'PlannedValue' =>NULL,
							  'CreatedOn' => $current_time,
							  'ModifiedOn' => $current_time,
							  'ModifiedBy' => '1',
							  'isActive' => 1
							]);

							$getLineItemMetricID_Out1 = DB :: table('LineItemMetrics')
						           	->join('LineItem','LineItem.id','=','LineItemMetrics.LineItem_ID')
						           	->join('HiddenMetrics','HiddenMetrics.id','=','LineItemMetrics.HiddenMetrics_ID')
						           	->where('HiddenMetrics.id','=',$HiddenMetrics_id)
						           	->where('LineItemMetrics.LineItem_ID','=',$LineitemID)
						        	->select('LineItemMetrics.*')->get();
							$getLineItemMetricID_Result1 = json_decode($getLineItemMetricID_Out1,true);
							if(!empty($getLineItemMetricID_Result1)){
								$lineItemMetricID = $getLineItemMetricID_Result1[0]['id'];
							}
						}

						$getLineItemDaily_Out = DB :: table('LineItemDaily')
						           	->join('LineItemMetrics','LineItemMetrics.id','=','LineItemDaily.LineItemMetrics_ID')
						           	->where('LineItemDaily.Date','=',$date_start)
						           	->where('LineItemDaily.LineItemMetrics_ID','=',$lineItemMetricID)
						        	->select('LineItemDaily.*')->get();

						$getLineItemDaily_Result = json_decode($getLineItemDaily_Out,true);

						
						if($getLineItemDaily_Result)
						{
							$lineItemDailyID=$getLineItemDaily_Result[0]['id'];
							$LIDSavedValue=$getLineItemDaily_Result[0]['Value'];
							$CompleteLIDRow=$getLineItemDaily_Result[0];

							if($getLineItemDaily_Result[0]['isLineNumberProcessed']==0)
							{
								DB::table('LineItemDaily_PreviousData')->insert([
						                  'LineItemDaily_ID' => $CompleteLIDRow['id'],
						                  'LineItemMetricsID' => $CompleteLIDRow['LineItemMetrics_ID'],
						                  'Date' =>$CompleteLIDRow['Date'],
						                  'Value' =>$CompleteLIDRow['Value'],
						                  'CreatedOn' => $CompleteLIDRow['CreatedOn'],
						                  'ModifiedOn' => $CompleteLIDRow['ModifiedOn'],
						                  'ModifiedBy' => $CompleteLIDRow['ModifiedBy'],
						                  'isLineNumberProcessed' =>$CompleteLIDRow['isLineNumberProcessed'],
						                  'isActive' => $CompleteLIDRow['isActive'],
						                  'MovedOn'=> $current_time
						        ]);
						    
							}	
							if($publisher == 'AdWordsY' && ($key=='cost'))
							{
								$val=$val/(pow(10,6));
							}
							if($getLineItemDaily_Result[0]['isLineNumberProcessed']==1)
							{	
								$getPriviousVal=DB::table('LineItemDaily')
									->where('LineItemDaily.Date' ,'=',$date_start)
	                                ->where('LineItemDaily.LineItemMetrics_ID','=',$lineItemMetricID)
	                                ->where('LineItemDaily.isLineNumberProcessed','=',1)
	                                ->select('LineItemDaily.Value')
	                                ->first();
	                                //print_r($getPriviousVal->Value); echo"<br>"; print_r($val); exit;
	                            $val=$getPriviousVal->Value+$val;

								DB::table('LineItemDaily')
                                    ->where('LineItemDaily.Date' ,'=',$date_start)
						            ->where('LineItemDaily.LineItemMetrics_ID','=',$lineItemMetricID)
								    ->update([					                  
						                'Date' =>$date_start,
						                'Value' =>$val,
						                'CreatedOn' => $current_time,
						                'ModifiedOn' => $current_time,
						                'ModifiedBy' => '1',
						                'isLineNumberProcessed' => 1,
						                'isActive' => 1
						            ]);
							}
							else{
								
								DB::table('LineItemDaily')
                                    ->where('LineItemDaily.Date' ,'=',$date_start)
						            ->where('LineItemDaily.LineItemMetrics_ID','=',$lineItemMetricID)
								    ->update([					                  
						                'Date' =>$date_start,
						                'Value' =>$val,
						                'CreatedOn' => $current_time,
						                'ModifiedOn' => $current_time,
						                'ModifiedBy' => '1',
						                'isLineNumberProcessed' => 1,
						                'isActive' => 1
						            ]);
							}
						}
						else{
							
							DB::table('LineItemDaily')->insert([
				                'LineItemMetrics_ID' => $lineItemMetricID,
				                'Date' =>$date_start,
				                'Value' =>$val,
				                'CreatedOn' => $current_time,
				                'ModifiedOn' => $current_time,
				                'ModifiedBy' => '1',
				                'isLineNumberProcessed' => 1,
				                'isActive' => 1
				            ]);
						}
					}
				}
	 	    }
	 	    //exit;
		}
		DB::table('LineItemDaily')                     
		->update([        
              'CreatedOn' => $current_time,
              'ModifiedOn' => $current_time,
              'ModifiedBy' => '1',
              'isLineNumberProcessed' => 0,
              'isActive' => 1
        ]);
		//return redirect::to('api');

		//get entry from previous data table
	 	$getPreviousData=DB::table('LineItemDaily_PreviousData')->get();

	 	foreach ($getPreviousData as $key ) {
	 		//echo"<pre>"; print_r($key->Value); exit;
	 		$LineItemDaily_ID=$key->LineItemDaily_ID;
	 		$prevLineItemDaily_Value=$key->Value;

	 		$getNewData=DB::table('LineItemDaily')
	 						->where('id','=',$LineItemDaily_ID)
	 		                ->get();

	 		$newLineDaily_value=$getNewData[0]->Value;
	 		
	 		if($prevLineItemDaily_Value==$newLineDaily_value)
	 		{
	 			//echo"<pre>"; print_r('true'); exit; 
	 			DB::table('LineItemDaily_PreviousData')->where('LineItemDaily_ID', '=', $LineItemDaily_ID)->delete();

	 		}                
	 		//echo"<pre>"; print_r($getNewData[0]->Value); exit;                
	 	}
		//echo"<pre>"; print_r($getPreviousData); exit;

		return ['status'=> 'success'];
	}

    //process city bank excel
    public function addCityBank(Request $request)
    {

       	ini_set('memory_limit', '-1');
       	set_time_limit(0);
       	$currentdate=Carbon::now()->toDateString();
       	$currentdate=str_replace('-','',$currentdate);
		//process called feedback excel city bank
      	$filePathCalledfeedback = public_path('/reports/CityBank/CalledFeedback_'.$currentdate.'.xlsx');
      	config(['excel.import.startRow' => 1]);
      	$data = \Excel::load($filePathCalledfeedback)->get()->toArray();
      	foreach ($data as $key) {
        	$called_feed=DB::table('CalledFeedBack')->insert([
	          	'month' =>$key['month'],
	          	'trandt' => $key['trandt'],
	          	'lkup_channel' =>$key['lkup_channel'],
	          	'lkup_site' =>$key['lkup_site'],
	          	'externaloffercode' =>$key['externaloffercode'],
	          	'externalreferersite' => $key['externalreferersite'],
	          	'section' =>$key['section'],
	          	'city' => $key['city'],
	          	'refid' => $key['refid'],
	          	't4refid' => $key['t4refid'],
	          	'status' => $key['status'],
	          	'disposition' => $key['disposition'],
	          	'profession' => $key['profession'],
	          	'campaign'=>$key['campaign']
        	]);
      	}
      	$new_path=public_path('/reports/ProcessedCityBank/CalledFeedback_'.$currentdate.'.xlsx');
      	$move= File::move($filePathCalledfeedback, $new_path);
		//process conversion excel city bank
      	$filePathConversion = public_path('/reports/CityBank/Conversion_'.$currentdate.'.xlsx');
      	config(['excel.import.startRow' => 1]);
      	$dataConversion = \Excel::load($filePathConversion)->get()->toArray();
      	foreach($dataConversion as $key1)
      	{
        	$add_conversion=DB::table('Conversion')->insert([
            	'v_application_id' =>$key1['v_application_id'],
            	'd_modified_dt' => $key1['d_modified_dt'],
            	'v_dsa_batch_no' =>$key1['v_dsa_batch_no'],
	            'd_app_rcvd_dt' =>$key1['d_app_rcvd_dt'],
	            'v_special_instr' =>$key1['v_special_instr'],
	            'subchannel' => $key1['subchannel'],
	            'status' =>$key1['status'],
	            'descstat' => $key1['descstat'],
	            'final_status' => $key1['final_status']

          	]);
      	}
      	$new_path=public_path('/reports/ProcessedCityBank/Conversion_'.$currentdate.'.xlsx');
      	$move= File::move($filePathConversion, $new_path);
		//process long lead excel city bank
      	$filePathLongLead = public_path('/reports/CityBank/LongLead_'.$currentdate.'.xlsx');
      	config(['excel.import.startRow' => 1]);
      	$dataLongLead = \Excel::load($filePathLongLead)->get()->toArray();
      	foreach ($dataLongLead as $key2) {
      	$addLongLead=DB::table('CBLongLead')->insert([
	        'TranDt' =>$key2['trandt'],
	        'externalOfferCode' => $key2['externaloffercode'],
	        'RefID' =>$key2['ref_id'],
	        'ResidenceCity' =>$key2['residence_city'],
	        'FormID' =>$key2['form_id'],
	        'AgencyCode' => $key2['agency_code'],
	        'Creative' =>$key2['creative'],
	        'Section' => $key2['section'],
	        'Source_Id' =>$key2['source_id'],
	        'Profession' => $key2['profession'],
	        'Site' => $key2['site'],
	        'RefID2' => $key2['ref_id2'],
	        'status' =>'' ,
	        'CBReject_id' => '',
	        'final_status' => '',
	        'lineItem_ID' => ''

      	    ]);
        }
    	$new_path=public_path('/reports/ProcessedCityBank/LongLead_'.$currentdate.'.xlsx');
    	$move= File::move($filePathLongLead, $new_path);
		//process short lead excel city bank
    	$filePathShortLead = public_path('/reports/CityBank/ShortLead_'.$currentdate.'.xlsx');
    	config(['excel.import.startRow' => 1]);
    	$dataShortLead = \Excel::load($filePathShortLead)->get()->toArray();
    	foreach ($dataShortLead as $key3) {
	      	$addShortLead=DB::table('CBShortLead')->insert([
		        'TranDt' =>$key3['trandt'],
		        'externalOfferCode' => $key3['externaloffercode'],
		        'RefID' =>$key3['refid'],
		        'city' =>$key3['city'],
		        'form_id' =>$key3['form_id'],
		        'AgencyCode' => $key3['agency_code'],
		        'Creative' =>$key3['creative'],
		        'ProductCode' => $key3['product_code'],
		        'Section' => $key3['section'],
		        'Source_Id' =>$key3['source_id'],
		        'Profession' => $key3['profession'],
		        'Site' => $key3['site'],
		        'T4RefId' => '',
		        'Feedback_Status' =>'' ,
		        'Disposition' => '',
		        'Conversion_status' => '',
		        'CBReject_id' => '',
		        'final_status' => '',
		        'lineItem_ID'=>''
	      	]);
    	}
	    $new_path=public_path('/reports/ProcessedCityBank/ShortLead_'.$currentdate.'.xlsx');
	    $move= File::move($filePathShortLead, $new_path);

	    $updateLongLeadLineItem=DB::table('CBLongLead')
	                        ->join('LineItem','LineItem.LineItemID','=','CBLongLead.Section')
	                        ->update(['CBLongLead.lineItem_ID'=>DB::raw('LineItem.id')]);

	    $updateLongLead=DB::table('CBLongLead')
	                          ->join('Conversion','Conversion.v_special_instr','=','CBLongLead.RefID2')
	                          ->join('CBReject','CBReject.RejectCode','=','Conversion.descstat')
	                          ->update(['CBLongLead.status'=>DB::raw('Conversion.status'),
	                                     'CBLongLead.CBReject_id'=>DB::raw('CBReject.id'),
	                                     'CBLongLead.final_status'=>DB::raw('Conversion.final_status')]);

	   	$updateShortLeadLineItem=DB::table('CBShortLead')
                          ->join('LineItem','LineItem.LineItemID','=','CBShortLead.Section')
                          ->update(['CBShortLead.lineItem_ID'=>DB::raw('LineItem.id')]);

   		$updateShortLead=DB::table('CBShortLead')
                      ->join('CalledFeedBack','CalledFeedBack.refid','=','CBShortLead.RefID')
                      ->join('Conversion','Conversion.v_special_instr','=','CalledFeedBack.t4refid')
                      ->join('CBReject','CBReject.RejectCode','=','Conversion.descstat')
                      ->update(['CBShortLead.T4RefId'=>DB::raw('CalledFeedBack.t4refid'),
                                'CBShortLead.Feedback_Status'=>DB::raw('CalledFeedBack.status'),
                                'CBShortLead.Disposition'=>DB::raw('CalledFeedBack.disposition'),
                                'CBShortLead.Conversion_status'=>DB::raw('Conversion.status'),
                                'CBShortLead.CBReject_id'=>DB::raw('CBReject.id'),
                                'CBShortLead.final_status'=>DB::raw('Conversion.final_status')]);

        DB::table('Conversion')->delete();
        DB::table('CalledFeedBack')->delete();
        return response()->json(['status' => 'success'], 200);

    }
    //end of function

    // ------- add oauth2CallBack for Ga and DBM Display and Youtube------------

    public function oauth2CallBack()
    {
   		session_start();
		$client = new Google_Client();
		$upTwo = dirname(__DIR__, 2);
		$client->setAuthConfig( $upTwo. '/dbm/client_secrets.json');

		$url = dirname(__FILE__);
		$array = explode('\\',$url);
		$count = count($array);
		//$authCallUrl='http://' . $_SERVER['HTTP_HOST'] . '/IADC/public/oauth2CallBack';
		$authCallUrl='http://' . $_SERVER['HTTP_HOST'] . '/oauth2CallBack';
		$client->setRedirectUri($authCallUrl);
		$client->addScope('https://www.googleapis.com/auth/doubleclickbidmanager');
		$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);

		// Handle authorization flow from the server.
		if (! isset($_GET['code'])) {
			echo"if";
		  	$auth_url = $client->createAuthUrl();
		  	
		  	header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL)); exit;
		} 
		else {
			//dd("else");
		  $client->authenticate($_GET['code']);
		  $_SESSION['access_token'] = $client->getAccessToken();
		  $this->addDBM_Display_Youtube();
		}
    }

    public function oauth2CallBackGA()
    {
   		session_start();
		$client = new Google_Client();
		$upTwo = dirname(__DIR__, 2);
		$client->setAuthConfig( $upTwo. '/GA/client_secrets.json');

		$url = dirname(__FILE__);
		$array = explode('\\',$url);
		$count = count($array);
		
		//$authCallUrl='http://' . $_SERVER['HTTP_HOST'] . '/IADC/public/oauth2CallBackGA';
		$authCallUrl='http://' . $_SERVER['HTTP_HOST'] . '/oauth2CallBackGA';
		$client->setRedirectUri($authCallUrl);
		$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);

		// Handle authorization flow from the server.
		if (! isset($_GET['code'])) {
			echo"if"; //exit;
		  	$auth_url = $client->createAuthUrl();
		  	
		  	header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL)); exit;
		} 
		else {
			//dd("else"); //exit;
		  $client->authenticate($_GET['code']);
		  $_SESSION['access_token'] = $client->getAccessToken();
		  $this->addGoogleAnalytics();
		}
    }
   
    // ------- end  add oauth2CallBack for Ga and DBM Display and Youtube------------
    //public function addGoogleAnalytics(Request $request)
    public function addGoogleAnalytics()
	{
		//echo"<pre>";  print_r($request->all()); exit;
		//$GAObj= new GoogleAnalyticsReportProcessing;
		//$exedbm=$GAObj->indexGA();				

		$analytics = $this->initializeAnalytics();
		$viewId = $this->getFirstProfileId($analytics);			
		// -------------------------------------------------------
		$client = new Google_Client();
	    $upTwo = dirname(__DIR__, 2);
		$client->setAuthConfig( $upTwo. '/GA/client_secrets.json');
	    $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);

	    if (isset($_SESSION['access_token']) && $_SESSION['access_token']) 
	    {
	      	$client->setAccessToken($_SESSION['access_token']);
	      	$analytics = new Google_Service_AnalyticsReporting($client);

	      	foreach ($viewId as $key => $viewIdValue) {
	        	$response = $this->getReport($analytics, $viewIdValue);
	        	$gaReport=$this->printResultsReport($response, $viewIdValue );
	        
	        	$serializedData = json_encode($gaReport);
	        	file_put_contents('GAReport_json_encode_'.$viewIdValue.'.json', $serializedData);
	        	$recoveredData = file_get_contents('GAReport_json_encode_'.$viewIdValue.'.json');

	        	ApiController::processGAReport($viewIdValue);
	        
	      	}  
	      	echo "<pre><h1>Report For GoogleAnalytics is Processed...!</h1>";
			echo"<a href='api' class='btn btn-info pull-right'>BACK</a>";
	    } 
	    else {
	      
	       	return redirect()->to('/oauth2CallBackGA');
	    }
	}
	//---------------------------------------------- 

	public function initializeAnalytics()
	{
	  
	  $upTwo = dirname(__DIR__, 2);
	  $KEY_FILE_LOCATION = $upTwo. '/GA/service-account-credentials.json';
	  $client = new Google_Client();
	  $client->setApplicationName("Hello Analytics Reporting");
	  $client->setAuthConfig($KEY_FILE_LOCATION);
	  $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	  $analytics = new Google_Service_Analytics($client);

	  return $analytics;
	}

	function getFirstProfileId($analytics) 
	{
	  //global $viewId;
	  $viewId=[];

	  //dd("in proID");
	  // Get the list of accounts for the authorized user.
	  $accounts = $analytics->management_accounts->listManagementAccounts();
	  if (count($accounts->getItems()) > 0) {
	    $items = $accounts->getItems();
	    for ( $rowIndex = 0; $rowIndex < count($items); $rowIndex++) {
	      $accountId = $items[$rowIndex]->getId();
	      $properties = $analytics->management_webproperties
	          ->listManagementWebproperties($accountId);

	      if (count($properties->getItems()) > 0) {
	        $items1 = $properties->getItems();

	        for ( $rowIndex1 = 0; $rowIndex1 < count($items1); $rowIndex1++) {
	          $propertyId = $items1[$rowIndex1]->getId();

	          $profiles = $analytics->management_profiles
	              ->listManagementProfiles($accountId, $propertyId);

	          if (count($profiles->getItems()) > 0) {
	            $items2 = $profiles->getItems();
	            for ( $rowIndex2 = 0; $rowIndex2 < count($items2); $rowIndex2++) {
	              array_push($viewId, $items2[$rowIndex2]->getId());
	              //
	            }
	          } 
	          else 
	          {
	            throw new Exception('No views (profiles) found for this user.');
	          }
	        }
	      } 
	      else 
	      {
	        throw new Exception('No properties found for this user.');
	      }
	    }
	  } 
	  else 
	  {
	    throw new Exception('No accounts found for this user.');
	  }
	  return $viewId; 
	}

	Public function getReport($analytics, $viewIdValue) {

		$VIEW_ID = $viewIdValue;
	    $dateRange = new Google_Service_AnalyticsReporting_DateRange();
	    //$dateRange->setStartDate("7daysAgo");
	    $dateRange->setStartDate("3daysAgo");//
	    $dateRange->setEndDate("today");

	    // Create the Metrics object.
	    $sessions = new Google_Service_AnalyticsReporting_Metric();
	    $sessions->setExpression("ga:sessions");
	    $sessions->setAlias("sessions");

	    $avgSessionDuration = new Google_Service_AnalyticsReporting_Metric();
	    $avgSessionDuration->setExpression("ga:avgSessionDuration");
	    $avgSessionDuration->setAlias("avgSessionDuration");

	    $bounceRate = new Google_Service_AnalyticsReporting_Metric();
	    $bounceRate->setExpression("ga:bounceRate");
	    $bounceRate->setAlias("bounceRate");

	    $users = new Google_Service_AnalyticsReporting_Metric();
	    $users->setExpression("ga:users");
	    $users->setAlias("users");

	    $pageviews = new Google_Service_AnalyticsReporting_Metric();
	    $pageviews->setExpression("ga:pageviews");
	    $pageviews->setAlias("pageviews");

	    $adClicks = new Google_Service_AnalyticsReporting_Metric();
	    $adClicks->setExpression("ga:adClicks");
	    $adClicks->setAlias("adClicks");

	    $adCost = new Google_Service_AnalyticsReporting_Metric();
	    $adCost->setExpression("ga:adCost");
	    $adCost->setAlias("adCost");

	    $impressions = new Google_Service_AnalyticsReporting_Metric();
	    $impressions->setExpression("ga:impressions");
	    $impressions->setAlias("impressions");

	    $timeOnPage = new Google_Service_AnalyticsReporting_Metric();
	    $timeOnPage->setExpression("ga:timeOnPage");
	    $timeOnPage->setAlias("timeOnPage");

	    $goalCompletionsAll = new Google_Service_AnalyticsReporting_Metric();
	    $goalCompletionsAll->setExpression("ga:goalCompletionsAll");
	    $goalCompletionsAll->setAlias("goalCompletionsAll");

	    //Add dimensions. Only medium can be considered as it's mapped to the line item
	    $date = new Google_Service_AnalyticsReporting_Dimension();
	    $date->setName("ga:date");

	    $medium = new Google_Service_AnalyticsReporting_Dimension();
	    $medium->setName("ga:medium");

	    // Create the ReportRequest object.
	    $request = new Google_Service_AnalyticsReporting_ReportRequest();
	    $request->setViewId($VIEW_ID);
	    $request->setDateRanges($dateRange);
	    $request->setMetrics(array($sessions, $avgSessionDuration, $bounceRate, $users, $pageviews, $adClicks, $adCost, $impressions, $timeOnPage, $goalCompletionsAll));
	    //  $request->setMetrics(array($pageviews));
	    $request->setDimensions(array($date, $medium));
	    //  $request->setMetrics(array($sessions, $avgSessionDuration, $bounceRate, $users));

	    $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
	    $body->setReportRequests( array( $request) );
	    return $analytics->reports->batchGet( $body );
	}

	Public function printResultsReport($reports, $viewIdValue) {

	    for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
	      
		    //echo"<pre>count "; print_r( count($reports) );// exit;
		    $report = $reports[ $reportIndex ];
		    $header = $report->getColumnHeader();
	      	$dimensionHeaders = $header->getDimensions();
	        $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
	      	$rows = $report->getData()->getRows();
	       	//echo"<pre> rowCount for each : "; print_r(count($rows)); //exit;
	      	$reportArray=[]; 
	      	$dimentionArr=[];
	      	$metricsArr=[];
	      
	      	for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
	        	//echo"<pre> rowCount "; print_r(count($rows)); //exit;
		        $row = $rows[ $rowIndex ];
		        $dimensions = $row->getDimensions();
		        $metrics = $row->getMetrics();
		        
		        $dimention=[];
		        $metricess=[];

		        for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
		          // echo "<br>---------------------------";
		          // print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
		          $dimention[$dimensionHeaders[$i]]=$dimensions[$i];
		        }
	        	array_push($dimentionArr, $dimention);

		        for ($j = 0; $j < count($metrics); $j++) {
		          $values = $metrics[$j]->getValues();
		          for ($k = 0; $k < count($values); $k++) {
		            $entry = $metricHeaders[$k];
		            // echo "<br>.......................";
		            // print($entry->getName() . ": " . $values[$k] . "\n");
		            $metricess[$entry->getName()]=$values[$k];        
		          }
		          array_push($metricsArr, $metricess);
		        
		        }
		        foreach($dimentionArr as $key=>$val){
		            $val2 = $metricsArr[$key]; 
		            $reportArray[$rowIndex] = $val + $val2; // combine 'em
		        }
		        //echo"<pre> return reportArray "; print_r($reportArray); //exit;
	      	}
	        //echo"<pre> return reportArray forOut"; print_r($reportArray); //exit;
	  	    return $reportArray;
	    }
	}
	// =============================================================================
	Public static function processGAReport($viewIdValue)
	{
		$currentdate=Carbon::now()->toDateString();
		
		$gaFilePublicPath=public_path('GAReport_json_encode_'.$viewIdValue.'.json');
		$dataGAFromPublic = file_get_contents($gaFilePublicPath);
		$GAarray = json_decode($dataGAFromPublic, true);
		
		// -------------process file----------------- 		
		// this method made static refer it while worling on GA
	    $response =ApiController:: storeSizmekAndGA_DBM__AdWordY_ApiValue($dataGAFromPublic,'GoogleAnalytics');
	    // -------------process file-----------------
	    // -------------move file in DBMReport Folder-----------------
	    $new_path=public_path('/reports/GA_Report/Processed_GA_Report_'.$currentdate.'_'.$viewIdValue.'.json');
       	$move= File::move($gaFilePublicPath, $new_path);
	    // -------------move file in DBMReport Folder-----------------

	}

	//public static function  addDBM_Display_Youtube(Request $request)
	public static function  addDBM_Display_Youtube()
	{
		//echo"in addDBM_Display_Youtube" ; exit;

		$client = new Google_Client();
		$upTwo = dirname(__DIR__, 2);
		
	    $client->setAuthConfig($upTwo . '/dbm/client_secrets.json');
	    $client->setApplicationName('DBM API PHP Samples');
	    $client->setAccessType('offline');
	    
	    if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
	      	$client->setAccessToken($_SESSION['access_token']);
	      	$service = new Google_Service_DoubleClickBidManager($client);
	    	$result = $service->queries->listqueries();
		
		  	// ------------------------------------------------------------------------------    
	  	  	$reportFileData_Dispaly = $service->queries->getquery('516012000');	  	  
	  	  	$CloudStoragePath_Display=$reportFileData_Dispaly->metadata->googleCloudStoragePathForLatestReport;
	      	file_put_contents("DBM_Display_Report.csv", fopen($CloudStoragePath_Display, 'r'));
	     	// -------------------------------------------------------------------------------
	      	$reportFileData_Youtube = $service->queries->getquery('522586603');
	      	$CloudStoragePath_Youtube=$reportFileData_Youtube->metadata->googleCloudStoragePathForLatestReport;
	      	file_put_contents("DBM_Youtube_Report.csv", fopen($CloudStoragePath_Youtube, 'r'));
	      	// --------------------------------------------------------------------------------

	      	echo "<pre><h1>Report For DBM_Others and DBM_Youtube is Processed...!</h1>";
			echo"<a href='api' class='btn btn-info pull-right'>BACK</a>";
	      	ApiController::processDBM_DispalyReport();

	    } else {
	 	    //echo"in else 12 "; //exit;
	        return redirect()->to('/oauth2CallBack');
	    }

					  
	}	
	public static function processDBM_DispalyReport(){

		$currentdate=Carbon::now()->toDateString();
		// -------------check process file file in Public directory For DBM_Dispaly
		$dbmFilePublicPath=public_path('DBM_Display_Report.csv');
		$dataDBMFromPublic = \Excel::load($dbmFilePublicPath)->get()->toArray();
		$response = ApiController::storeSizmekAndGA_DBM__AdWordY_ApiValue($dataDBMFromPublic,'DBM_Others');
	    // -------------end check process file file in Public directory For DBM_Dispaly
	    // -------------move file in DBMReport Folder-----------------
	    $new_path=public_path('/reports/DBM_Report/DBM_Display/Processed_DBM_display_Report_'.$currentdate.'.csv');
      	$move= File::move($dbmFilePublicPath, $new_path);
	    // -------------move file in DBMReport Folder-----------------
	
      	// YOUTUBE
      	// -------------check process file file in Public directory For DBM_Youtube
		$dbmFilePublicPath=public_path('DBM_Youtube_Report.csv');
		$dataDBMFromPublic = \Excel::load($dbmFilePublicPath)->get()->toArray();
		$response= ApiController::storeSizmekAndGA_DBM__AdWordY_ApiValue($dataDBMFromPublic,'DBM_Youtube');
	    // -------------end check process file file in Public directory For DBM_Youtube
	    // -------------move file in DBMReport Folder-----------------
	    $new_path=public_path('/reports/DBM_Report/DBM_Youtube/Processed_DBM_Youtube_Report_'.$currentdate.'.csv');
      	$move= File::move($dbmFilePublicPath, $new_path);
	    // -------------move file in DBMReport Folder-----------------

	}
	//Add AsWords Api for youtube
    public function addAdWordsYApi(Request $request,$a='')
    {	
    	
    	ini_set('MAX_EXECUTION_TIME', '-1');
    	set_time_limit(0);
    	
    	$date= new DateTime();
    	$current_time=$date->getTimestamp();
    	$pubName='AdwordsY';
    	
    	define('STDIN',fopen("php://stdin","r"));
    	if($a==null){
     		$startDate = $request['StartDate']; 
     	} else {
     		$startDate = $a['StartDate']; 
     	}
     	

     	$accountManagerIDsPath = public_path('accountManagerIDs.csv');
     	config(['excel.import.startRow' => 1 ]);
		$accountManagerIDsData = \Excel::load($accountManagerIDsPath)->get()->toArray();
		//echo "<pre>data"; print_r($accountManagerIDsData); exit;
    	foreach ($accountManagerIDsData as $accountManagerID ) {
    	//echo "<pre>"; print_r($accountManagerID['clientcustomerid']); exit;
    	 //$file = Storage::put( 'myfile.ini', $accountManagerID);
    	$accountClientcustomerid=$accountManagerID['clientcustomerid'];		
    	$dataAccountManagerID='
			[ADWORDS]
			; Required AdWords API properties. Details can be found at:
			; https://developers.google.com/adwords/api/docs/guides/basic-concepts#soap_and_xml
			;NEON Details
			developerToken = "7S0zFtOqIXJVP3ZqUtRYtg"

			;manager Id
			clientCustomerId ='.$accountClientcustomerid.'
		
			[OAUTH2]
			; NEON Details
			clientId = "666733807117-srfc7e5h1mnejcvrkdf1f73cu50cfhhi.apps.googleusercontent.com"
			clientSecret = "nNM1pGWZbBQgqWPaSa5KlRYQ"
			refreshToken = "1/_2Hjz77sQVIvPEqbt-RoLoL2VX5tOH0Ez3Eoc8DfMNI" 

			[SOAP]
			; Optional SOAP settings. See SoapSettingsBuilder.php for more information.
			; compressionLevel = <COMPRESSION_LEVEL>

			[CONNECTION]
			; Optional proxy settings to be used by requests.
			; If you dont have username and password, just specify host and port.
			; proxy = "protocol://user:pass@host:port"
			; Enable transparent HTTP gzip compression for all reporting requests.
			; enableReportingGzip = false

			[LOGGING]
			; Optional logging settings.
			; soapLogFilePath = "path/to/your/soap.log"
			; soapLogLevel = "INFO"
			; reportDownloaderLogFilePath = "path/to/your/report-downloader.log"
			; reportDownloaderLogLevel = "INFO"
			; batchJobsUtilLogFilePath = "path/to/your/bjutil.log"
			; batchJobsUtilLogLevel = "INFO" ';
	
    	 	$file = Storage::disk('public_adwords')->put('adsapi_php.ini', $dataAccountManagerID);
    		//--------- download multiple report for a manager Acoount-------------------------------
	    	$url = new ParallelReportDownload();
	    	$customerIds=$url->main($startDate, $pubName);
	    	//echo "<pre>API customerIds "; print_r($customerIds); exit; 
	     	//--------- download multiple report for a manager Acoount-------------------------------    	
	    	
	    	//echo"<pre>for "; print_r($pubName); exit;
	    	foreach ($customerIds as $customerId ) {
	    		$adReportfilePath = public_path('reports\adWordReports\adwords\adgroup_'.$pubName.'_'.$customerId.'.csv');	
	    		//echo"<pre>"; print_r($adReportfilePath); exit;
	    		config(['excel.import.startRow' => 2 ]);
	        	$data = \Excel::load($adReportfilePath)->get()->toArray();
	        	$response =  ApiController::storeSizmekAndGA_DBM__AdWordY_ApiValue($data,'AdWordsY');    		
	    		//echo"<pre>"; print_r($response); //exit; 
	    		
	    		if ($response['status']=='success') {			
				 	$new_path=public_path('reports\adWordReports\adwords\adProcessedReport\P_adgroup_'.$accountClientcustomerid.'_'.$pubName.'_'.$customerId.'_'.$current_time.'.csv');	
				 	$move= File::move($adReportfilePath, $new_path);
				 }
				 else{
				 	$new_path=public_path('reports\adWordReports\adwords\adErroredReport\E_adgroup_'.$accountClientcustomerid.'_'.$pubName.'_'.$customerId.'_'.$current_time.'.csv');	
				 	$move= File::move($adReportfilePath, $new_path);
				 }	
	    	}
    	} 
     
    	return response()->json(['status' => 'success'], 200); 
    
    }

    public function getDataforVariousApi(Request $request,$a='')
    {
    	//echo"<pre>"; print_r($request->all()); exit;
    	//identify which publisher is called
    	$selectedApiPublisherId=$request->getApiPublisherId;
    	$campStartDate=$request->campStartDate;
    	
    	//$Campaign_ID=$request->Campaign_ID;

    	$getPublisherName= DB::table('Publisher')
    	                   ->where('id','=', $selectedApiPublisherId)
    	                   ->select('PublisherName')
    	                   ->first();
    	//echo"<pre>"; print_r($getPublisherName);
    	if($getPublisherName->PublisherName='DCM'){
    	   	$Publisher_ID=$selectedApiPublisherId;               
    	   	//-----------------------------------------------------------------------
    	    
    		if($a==null){
	     		define('STDIN',fopen("php://stdin","r"));
	     		$_SESSION['startDate'] = $campStartDate;//$request['StartDate']; 
	     	}else {
	     		$_SESSION['startDate'] = $campStartDate;//$a['StartDate']; 
	     	}
			ob_start();
			ini_set('implicit_flush', 1);
			$_SESSION['user_profile_id'] = '5018824';  //5018824-Snag.Raju@interactiveavenues.com 5012293-Gaanalytics.ia 
			$authAcct = new AuthenticateUsingServiceAccount();
			$authAcct->run(app_path('v3\dcmiadc-fcdf17511686.json'),'dcmiadc@dcmiadc.iam.gserviceaccount.com');
		    $createStandardReport = new CreateStandardReport();
			$createStandardReport->run();
		    $generateReportFile = new GenerateReportFile();
			$generateReportFile->run();
			$downloadReportFile = new DownloadReportFile();
			$downloadReportFile->run();
			//------------------------------------------------------------------------*/
			//---Process  DCM excelReport--------------

    		//$filePath = public_path('/reports/dcmReports/example_report.csv');

		     //config(['excel.import.startRow' => 10 ]); // from site sheet
		     //config(['excel.import.startRow' => 11 ]); //  8821 sheet
		    // $excelData = \Excel::load($filePath)->get()->toArray();
		    // //echo"<pre>"; print_r($excelData); exit;
		     //$this->ProcesDCMExcelSheetData($Publisher_ID,$excelData );
    	}
    	//else{
    	//	echo('else');
    	//}  	                   
    }

    public function ProcesDCMExcelSheetData($Publisher_ID,$excelData ){
    	echo $Publisher_ID;
    	echo"<br>";
    	//echo"<pre>";
    	//print_r($excelData); 

    	foreach ($excelData as $keyExcelData) {

    		echo"<pre>";
    		print_r($keyExcelData); 

    		if($keyExcelData->campaign_id)
    		{
    			$LineItemMetrics=DB::table('CampaignAPIPublisherCampaign')->Update([
								  'Campaign_ID' => Null,
								  'Publisher_ID' => $Publisher_ID,
								  'PublisherCampaignID' =>$keyExcelData->campaign_id,
								  'PublisherCampaignName' => $keyExcelData->campaign,//'7',
								  
				]);
    		}
    		else{
	    		$LineItemMetrics=DB::table('CampaignAPIPublisherCampaign')->insert([
								  'Campaign_ID' => Null,
								  'Publisher_ID' => $Publisher_ID,
								  'PublisherCampaignID' =>$keyExcelData->campaign_id,
								  'PublisherCampaignName' => $keyExcelData->campaign,//'7',
								  
				]);
			}	
    	}
    }
}
