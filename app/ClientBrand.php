<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientBrand extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['BrandName','Client_ID','CreatedOn','ModifiedOn','ModifiedBy','isActive'];

	public $table = "dbo.ClientBrand";
}
