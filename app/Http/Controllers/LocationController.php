<?php namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Location;


class LocationController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
        $city = $request['query'];
		
		if($city!='') {
			$data= Location::where("LocationName","LIKE","%".$city."%")->get();
			$i=0;
			foreach($data as $key) {
			 	$result[$i]['text']=$key['LocationName'];
			 	$result[$i]['value']=$key['id'];
			 	$i++;
			}
		} else {
		    $result[0]['text']="No Result";
		    $result[0]['value']="No Result";
		}
		return response()->json($result,200);			 
    }

}

?>