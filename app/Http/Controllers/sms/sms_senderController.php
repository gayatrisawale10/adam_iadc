<?php
namespace App\Http\Controllers\sms;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;

use App\Product;
use App\customer_job; 
use App\work;
use App\sms\smsconfig;
use App\sms\sms_log;
use Illuminate\Support\Facades\Auth;
use \App\send_sms\send_sms;
use \App\send_sms\sms_trigger1;

class sms_senderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $sms_log = sms_log::paginate(10);       

        return view('sms.sms_log.index', compact('sms_log'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function trigger_sms1()
    {
        echo '<br/>SMS Triggering Started.........<br/>'; 
        echo "<br/>-----------------------------------------------------";
        
        $FmyFunctions2 = new sms_trigger1;        
        $send_trigger = ($FmyFunctions2->trigger_sms());
        
        echo '<br/>All Done..! <br/>';
        echo '<br/>SMS Triggered Successfully. <br/>';
        
        
    }
    
}
