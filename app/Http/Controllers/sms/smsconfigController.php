<?php

namespace App\Http\Controllers\sms;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\sms\smsconfig;
use Illuminate\Http\Request;
use Session;
use App\customer_job;
use App\work;
use Illuminate\Support\Facades\Auth;


class smsconfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $smsconfig = smsconfig::paginate(25);

        return view('sms.smsconfig.index', compact('smsconfig'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('sms.smsconfig.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'sms_type' => 'required'
		]);
        
        // TODO: assign session user id to $Modified_by
        $Modified_by=Auth::user()->id;
        $request->request->add(['modified_by' => $Modified_by]);    
        $requestData = $request->all();               
        smsconfig::create($requestData);

        Session::flash('flash_message', 'smsconfig added!');

        return redirect('sms/smsconfig');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $smsconfig = smsconfig::findOrFail($id);

        return view('sms.smsconfig.show', compact('smsconfig'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $smsconfig = smsconfig::findOrFail($id);
  
        return view('sms.smsconfig.edit', compact('smsconfig'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'sms_type' => 'required'
		]);
        $requestData = $request->all();
        
        $smsconfig = smsconfig::findOrFail($id);
        $smsconfig->update($requestData);

        Session::flash('flash_message', 'smsconfig updated!');

        return redirect('sms/smsconfig');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        smsconfig::destroy($id);

        Session::flash('flash_message', 'smsconfig deleted!');

        return redirect('sms/smsconfig');
    }
}
