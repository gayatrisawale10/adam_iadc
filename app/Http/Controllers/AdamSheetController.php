<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\CampaignPublisher;
use App\Publisher;
use App\Campaign;
use App\LineItem;
use App\LineItemDaily;
use App\Location;
use Session;
use DB;
use Validator;
Use Redirect;
use Carbon\Carbon;
use DateTime;
use Excel;
use App\RawExcelData;
use Geocode;
use App\TargettingValue;
use App\AdamsLineItem;
use App\AdamLineDCMMapping;
use App\AdamAlwaysOnCampaign;

class AdamSheetController extends Controller
{
  public function getAdamLinesDetails($camp_id)
  {
    //echo'adam'; dd($camp_id);
    $ditNamePerAdamLine="";
    $campClinetBrandDetails= DB:: select("
        Select  cm.id as camp_id,
        cm.CampaignName,
        cm.CampaignID, 
        cb.BrandName,
            c.ClientName,
            c.id as ClientId,
            c.specialClientEditAdamKey,
        CONVERT (date, cm.StartDate)  as camp_st, 
        CONVERT (date, cm.EndDate)  as camp_en, 
        cm.CreatedOn as camp_cr,
        cm.alwaysOn  
        from  Campaign cm      
        left outer join ClientBrand cb on cb.id=cm.ClientBrand_ID   
        left outer join Client c on c.id=cb.Client_ID        
        where 
          cm.IsActive=1 and cm.id=".$camp_id."
          order by cm.id DESC
    ");     
    $campClinetBrandDetails=$campClinetBrandDetails['0'];
    $DIT=DB::table('DataInputType')
                  ->where('DataInputType.IsActive','=', '1')
                  ->orderby('DataInputType.id','asc')
                  ->get();
    $DIT=json_decode($DIT);
    $getAdamLineItem=DB:: select("Select * 
                                  from AdamsLineItem 
                                  where IsActive=1 and campaign_ID=".$camp_id."");
    // dd($getAdamLineItem);
    //------------------get nonEmpty admineItemdata---------------------------------------------- 

    // $getAdamLineItem=DB::select("exec mysp_DropEmptyColumns ?", array($camp_id));

    // //dd(gettype($getAdamLineItem[0]));
    
    // $getAdamLineItemKeys=json_decode(json_encode($getAdamLineItem), true); //($);
    // //dd($getAdamLineItemKeys[0]);

    // $nonEmptyKey=array_keys($getAdamLineItemKeys[0]);
    //dd($nonEmptyKey);

    //------------------get nonEmpty admineItemdata---------------------------------------------- 
    $ditPerAdamLine=[];
    foreach ($getAdamLineItem as  $eachAdamLine) {
      $AdamsLineItem_ID=$eachAdamLine->id;
      $ditPerAdamLine=DB:: select("Select dit.DataInputTypeName 
                                 from AdamLineItemDIT as ald
                                 join DataInputType as dit on dit.id= ald.DataInputType_ID 
                                 where dit.IsActive=1 and  AdamsLineItem_ID=".$AdamsLineItem_ID."");      
      if(!empty($ditPerAdamLine)){
        $ditNamePerAdamLineArray=[];       
        foreach ($ditPerAdamLine as  $valueditPerAdamLine) {
           array_push($ditNamePerAdamLineArray,$valueditPerAdamLine->DataInputTypeName);
        } 
        $ditNamePerAdamLine=implode(",",$ditNamePerAdamLineArray); 
        $eachAdamLine->ditNamePerAdamLine=$ditNamePerAdamLine;
      }
      else{
        $eachAdamLine->ditNamePerAdamLine='';
      }
      //print_r(($eachAdamLine->ditNamePerAdamLine)); exit;
    }

    $specialClientEditAdamKeyArray=[];
    $getspecialClientEditAdamKeyList=DB :: table('Client')
                      ->select('Client.id as specifClientIDs')
                      ->where('Client.specialClientEditAdamKey','=',1)
                      ->get();                 
    $getspecialClientEditAdamKeyList=json_decode($getspecialClientEditAdamKeyList); 
    
    //dd($getspecialClientEditAdamKeyList);   
    foreach ($getspecialClientEditAdamKeyList as $key) {
       array_push($specialClientEditAdamKeyArray, $key->specifClientIDs);  
    } 
    //dd($specialClientEditAdamKeyArray);
    $specialClientEditAdamKeyString=implode(",",$specialClientEditAdamKeyArray);

    //dd($specialClientEditAdamKeyString);   
    //print_r($ditNamePerAdamLine);
    //exit;
    //$ditNamePerAdamLine="";
    //dd($getAdamLineItem);
    //dd($campClinetBrandDetails);

    $AllDropDownsList = $this->getDIT();

    // return view('AdamLines.getAdamLinesDetails',compact(
    //       'campClinetBrandDetails' ,'getAdamLineItem' ,'DIT' ,'ditNamePerAdamLine' ,'camp_id','specialClientEditAdamKeyString', 'AllDropDownsList', 'nonEmptyKey'
    // ))->with('i'); 

     return view('AdamLines.getAdamLinesDetails',compact(
          'campClinetBrandDetails' ,'getAdamLineItem' ,'DIT' ,'ditNamePerAdamLine' ,'camp_id','specialClientEditAdamKeyString', 'AllDropDownsList'
    ))->with('i'); 
  }

  public function alwaysOnCampaignDetails($adamUniqueKey, $id)
  {
    $alwaysOnCampaignData=DB::table('AdamAlwaysOnCampaign')
                  ->where('AdamAlwaysOnCampaign.AdamsLineItem_ID','=', $id)
                  ->get();

    //dd($alwaysOnCampaignData);
    return view('AdamLines.alwaysOnCampaignDetails',compact(
          'alwaysOnCampaignData', 'adamUniqueKey', 'id'
    ))->with('i'); 
  }

  public function updateAlwaysOnCampaign(Request $request)
  {
    //dd($request->FinalInputArray[0]);

    $current_time = Carbon::now()->toDateTimeString();

    DB::table('AdamAlwaysOnCampaign')
    ->where('id', $request->FinalInputArray[0])
    ->update([
      'FromDate' => $request->FinalInputArray[1],
      'ToDate' => $request->FinalInputArray[2],
      'Impressions' => $request->FinalInputArray[3],
      'Clicks' => $request->FinalInputArray[4],
      'VideoViews' => $request->FinalInputArray[5],
      'Engagement' => $request->FinalInputArray[6],
      'Visits' => $request->FinalInputArray[7],
      'Leads' => $request->FinalInputArray[8],
      'Reach' => $request->FinalInputArray[9],
      'Installs' => $request->FinalInputArray[10],
      'SMS' => $request->FinalInputArray[11],
      'Mailer' => $request->FinalInputArray[12],
      'Spots' => $request->FinalInputArray[13],
      'NetRate' => $request->FinalInputArray[14],
      'NetCost' => $request->FinalInputArray[15],
      'ModifiedOn' => $current_time,
      'IsActive' => 1,
      'ModifiedBy' => '1',
    ]);
    
    return response()->json(['res'=>'success'], 200);
  }

  public static function processAdamLineItem($adamData, $getLatestCampignID, $ClientID)
  {
    //echo"adam sheetttt: ";print_r($adamData);  exit;
    $getSpecialClientEditAdamKey=DB::table('Client')
                  ->where('Client.id','=', $ClientID)
                  ->select('Client.specialClientEditAdamKey')
                  ->get();
    $getSpecialClientEditAdamKey=json_decode($getSpecialClientEditAdamKey);  
    //print_r($getSpecialClientEditAdamKey[0]->specialClientEditAdamKey); exit;
    $duplicateAdamUniqueKeys=[];  
    $adamRowData=$adamData[0];
    $adamRowArray = json_decode(json_encode($adamRowData), true);
    //echo"<pre>";print_r($adamRowArray);
    $current_time = Carbon::now()->toDateTimeString();
    //foreach ($adamRowArray as $adamRowArrayKey => $adamRowArrayValue) {
    foreach ($adamRowArray as $eachAdamRow) {
      if($eachAdamRow['website_publisher']){
        // -----------------generate adamUniqueKey -----------------------------------
        //Brand_SubBrand_CampaignName_Objective_SubObjective_StartDate_EndDate_Website/Publisher_Targetting_DealType
        $startDate= date("Y-m-d", strtotime($eachAdamRow['from_date']['date']));
        $endDate=   date("Y-m-d", strtotime($eachAdamRow['to_date']['date']));

        // echo"<pre>"; print_r($startDate);
        // echo"<pre>"; print_r($endDate);
        // exit;        
        if($getSpecialClientEditAdamKey[0]->specialClientEditAdamKey==1){
          $AdamUniqueKeyExist=-1;
          $adamUniqueKey=NULL;          
        }
        else{

          $adamUniqueKey=$eachAdamRow['brand'].'_'.
                         $eachAdamRow['sub_brand'].'_'. 
                         $eachAdamRow['campaign_name'].'_'.
                         $eachAdamRow['objective'].'_'.
                         $eachAdamRow['sub_objective'].'_'.
                         $startDate.'_'.
                         $endDate.'_'.
                         $eachAdamRow['website_publisher'].'_'.
                         $eachAdamRow['geo_targeting'].'_'.
                         $eachAdamRow['buy_type'];
        }

        //Allow characters A - Z, a - z, 0-9, _ 
        $adamUniqueKey = preg_replace('/[^A-Za-z0-9\_]/', '', $adamUniqueKey);

        // -----------------generate adamUniqueKey -----------------------------------                
        //echo"<pre>Null"; print_r($adamUniqueKey);      
        //exit;     
        // ==============Check Duplicate AdamUniqueKey================================================================
        if($adamUniqueKey!=NUll){
          $AdamUniqueKeyExist=AdamSheetController::DuplicateAdamUniqueKey($adamUniqueKey);
          //dd($AdamUniqueKeyExist);  
        }    
        // ==============Check Duplicate AdamUniqueKey================================================================                 
        if($AdamUniqueKeyExist==-1 || $adamUniqueKey==Null){  
          $dataset = new AdamsLineItem;
          $dataset->campaign_ID             = $getLatestCampignID; 
          $dataset->adamUniqueKey           = $adamUniqueKey; //$eachAdamRow['platform'];
          $dataset->Type                    = $eachAdamRow['type'];
          $dataset->Website_Publisher       = $eachAdamRow['website_publisher'];
          $dataset->VendorEntityName        = $eachAdamRow['vendor_entity_name'];
          $dataset->FromDate                = $startDate;
          $dataset->ToDate                  = $endDate;
          $dataset->Platform                = $eachAdamRow['platform'];
          $dataset->Paid_OwnedMedia         = $eachAdamRow['paid_owned_media'];
          $dataset->Section                 = $eachAdamRow['section'];
          $dataset->AdUnit                  = $eachAdamRow['ad_unit'];
          $dataset->AdSize                  = $eachAdamRow['ad_size'];
          $dataset->Days                    = $eachAdamRow['days'];
          $dataset->GeoTargeting            = $eachAdamRow['geo_targeting'];
          $dataset->BuyType                 = $eachAdamRow['buy_type'];
          $dataset->BuySubType              = $eachAdamRow['buy_sub_type'];
          $dataset->Deliverables            = $eachAdamRow['deliverables'];
          $dataset->Quantity                = $eachAdamRow['quantity'];
          $dataset->GrossRate               = $eachAdamRow['gross_rate'];
          $dataset->GrossCost               = $eachAdamRow['gross_cost'];
          $dataset->NetRate                 = $eachAdamRow['net_rate'];
          $dataset->NetCost                 = $eachAdamRow['net_cost'];
          $dataset->Brand                   = $eachAdamRow['brand'];
          $dataset->Variant                 = $eachAdamRow['variant'];
          $dataset->ClientName              = $eachAdamRow['client_name'];
          $dataset->CampaignName            = $eachAdamRow['campaign_name'];
          $dataset->Vendor_PublisherEmail   = $eachAdamRow['vendor_publisher_email'];
          $dataset->SubBrand                = $eachAdamRow['sub_brand'];
          $dataset->Objective               = $eachAdamRow['objective'];
          $dataset->SubObjective            = $eachAdamRow['sub_objective'];
          $dataset->FormatType              = $eachAdamRow['format_type'];
          $dataset->Impressions             = $eachAdamRow['impressions'];
          $dataset->Clicks                  = $eachAdamRow['clicks'];
          $dataset->VideoViews              = $eachAdamRow['video_views'];
          $dataset->Engagement              = $eachAdamRow['engagement'];
          $dataset->Visits                  = $eachAdamRow['visits'];
          $dataset->Leads                   = $eachAdamRow['leads'];
          $dataset->Reach                   = $eachAdamRow['reach'];
          $dataset->Installs                = $eachAdamRow['installs'];
          $dataset->SMS                     = $eachAdamRow['sms'];
          $dataset->Mailer                  = $eachAdamRow['mailer'];
          $dataset->Spots                   = $eachAdamRow['spots'];
          $dataset->LineItemAdsetMap        =2;
          $dataset->CreatedOn               =$current_time;
          $dataset->CreatedBy               =1;
          $dataset->ModifiedOn              =$current_time;
          $dataset->ModifiedBy              =1;
          $dataset->IsActive                =1;  
          $dataset->save();
          
          array_push($duplicateAdamUniqueKeys, 1);
        }
        else{
          array_push($duplicateAdamUniqueKeys, $adamUniqueKey);
        }  
      }  
    }
    //echo"done"; exit;
    //return 1;
    return  $duplicateAdamUniqueKeys;
  }

  public function getDIT()
  {

      $DIT=DB::table('DataInputType')
                  ->where('DataInputType.IsActive','=', '1')
                  ->orderby('DataInputType.id','asc')
                  ->get();
      $DIT=json_decode($DIT);

      $Activity_type = DB::table('Mst_Activity_Type')
                  ->orderby('Mst_Activity_Type.id','asc')
                  ->get();
      $Activity_type = json_decode($Activity_type);

      $Platform = DB::table('Mst_Platform')
                  ->orderby('Mst_Platform.id','asc')
                  ->get();
      $Platform = json_decode($Platform);

      $PaidOwned = DB::table('Mst_Paid_Owned')
                  ->orderby('Mst_Paid_Owned.id','asc')
                  ->get();
      $PaidOwned = json_decode($PaidOwned);

      $Vendor = DB::table('Mst_Vendor')
                  ->orderby('Mst_Vendor.id','asc')
                  ->get();
      $Vendor = json_decode($Vendor);

      $Websites = DB::table('Mst_List_of_Sites')
                  ->orderby('Mst_List_of_Sites.id','asc')
                  ->get();
      $Websites = json_decode($Websites);

      $data = ['dit' => $DIT, 'activity_type' => $Activity_type, 'platform' => $Platform, 'paid_owned' => $PaidOwned, 'vendor' => $Vendor, 'websites' => $Websites];
      
      return $data;
  }

  public function editAdamLineItem(Request $request)
  {
    //echo"<pre> Test data: "; print_r($request->all()); exit(); 
    $current_time = Carbon::now()->toDateTimeString();

    if($request->specialClientEditAdamKey)
    {
      $adamUniqueKey=$request->specialClientEditAdamKey;
    }  
    else
    {
      $adamUniqueKey=$request->adamUniqueKey;
    }
    //Allow characters A - Z, a - z, 0-9, _ , (,),|,+,space,:,-    
    // [A-Za-z0-9_~\-\|\+\:\^&\(\)\ ]+$
    # $adamUniqueKey = preg_replace('/[^A-Za-z0-9\_]/', '', $adamUniqueKey);
    $adamUniqueKey = preg_replace('/[^A-Za-z0-9\_\-\|\+\:\^&\(\)\ ]+$/', '', $adamUniqueKey);
    // =============check DuplicateAdamUniqueKey on edit ecxelAddedLineitem ===========================
    if($adamUniqueKey == "_________"){
      return response()->json(['res'=>'validate'], 200);
    }else{
      if($request->oldAdamUniqueKey == $adamUniqueKey || $request->specialClientEditAdamKey == $adamUniqueKey){
        $this->UpdateAdamlineItem($request, $adamUniqueKey, $current_time);
        return response()->json(['res'=>'success'], 200);
      } else{
        $checkDuplicateAdamUniqueKey = AdamSheetController::DuplicateAdamUniqueKey($adamUniqueKey);

        if($checkDuplicateAdamUniqueKey == 1){
          return response()->json(['res'=>'error'], 200);
        }
        else{
          $this->UpdateAdamlineItem($request, $adamUniqueKey, $current_time);
          return response()->json(['res'=>'success'], 200);
        }
      }
    }
  }
  // ===============function to make entry in AdamLineItemMetrics and AdamLineItemDaily table====================
  public function addAdamLineItemMetrics($AdamsLineItem_ID,$AdamLineItemDIT_ID, $Mst_Metrics_ID,$campStartDate,$campEndDate)
  {
    $current_time = Carbon::now()->toDateTimeString();
    
    $AdamLineItemMetrics_ID=DB::table('AdamLineItemMetrics')->insertGetId([
      'AdamsLineItem_ID' => $AdamsLineItem_ID,
      'AdamLineItemDIT_ID' =>$AdamLineItemDIT_ID,
      'Mst_Metrics_ID' =>$Mst_Metrics_ID,
      
      'CreatedOn' => $current_time,
      'ModifiedOn' => $current_time,
      'ModifiedBy' => '1',
      'isActive' => 1
    ]);

    $camp_StartDate = new DateTime($campStartDate);
    $camp_EndDate = new DateTime($campEndDate);

    $noOfDaysOfCompaign = ($camp_EndDate->diff($camp_StartDate)->format("%a"))+1;
    // array for all dates
    for($i=0; $i<$noOfDaysOfCompaign; $i++) {
      $date = strtotime("+$i day", strtotime($campStartDate));
      $allDatesOfCompaign[]=date("Y-m-d", $date);
    }

    foreach($allDatesOfCompaign as $dates) {
      // DB::table('AdamLineItemDaily')->insert(
      //           [
      //             'AdamLineItemMetrics_ID' => $AdamLineItemMetrics_ID,
      //             'Date' =>$dates,

      //             'CreatedOn' => $current_time,
      //             'ModifiedOn' => $current_time,
      //             'ModifiedBy' => '1',
      //             'isActive' => 1
      //           ]
      // );

      // -------------------------changes to data in APIDataDaily Table---------------------------
      DB::table('APIDataDaily')->insert(
                [
                  'APIParamBreakDown_ID' => NULL,
                  'AdamLineItemMetrics_ID' => $AdamLineItemMetrics_ID,
                  'Date' =>$dates,

                  'CreatedOn' => $current_time,
                  'ModifiedOn' => $current_time,
                  'ModifiedBy' => '1',
                  'isActive' => 1
                ]
      );
      // -------------------------end changes to data in APIDataDaily Table---------------------------
    }
  }
  // ===============function to make entry in AdamLineItemMetrics and AdamLineItemDaily table====================
  public function insertAdamLineItem(Request $request){
    //dd($request->all());
    // ===========================================================================================
    $getSpecialClientEditAdamKey=DB::table('Client')
                  ->where('Client.id','=', $request->FinalInputArray[37])
                  ->select('Client.specialClientEditAdamKey')
                  ->get();

    $getSpecialClientEditAdamKey=json_decode($getSpecialClientEditAdamKey);
    
    // ---------------------------------------------------------------------------
    if($getSpecialClientEditAdamKey[0]->specialClientEditAdamKey==1){
      $adamUniqueKey=$request->FinalInputArray[0];          
    }
    else{
      $adamUniqueKey=$request->FinalInputArray[16].'_'.
                     $request->FinalInputArray[21].'_'. 
                     $request->FinalInputArray[19].'_'.
                     $request->FinalInputArray[22].'_'.
                     $request->FinalInputArray[23].'_'.
                     $request->FinalInputArray[1].'_'.
                     $request->FinalInputArray[2].'_'.
                     $request->FinalSelArrList[2][0].'_'.
                     $request->FinalInputArray[7].'_'.
                     $request->FinalInputArray[8];
    }
    // ---------------------------------------------------------------------------

    $adamUniqueKey = preg_replace('/[^A-Za-z0-9\_]/', '', $adamUniqueKey);

    $checkDuplicateAdamUniqueKeyIsPresent = AdamSheetController::DuplicateAdamUniqueKey($adamUniqueKey);
    
    if($adamUniqueKey == "_________"){
      return response()->json(['res'=>'validate'], 200);
    }else{
      if($checkDuplicateAdamUniqueKeyIsPresent == -1){
        $ccom = new AdamsLineItem;
        $ccom->campaign_ID            = $request->FinalInputArray[36];        
        $ccom->adamUniqueKey          = $adamUniqueKey;
        $ccom->Type                   = $request->FinalSelArrList[1][0];
        $ccom->Website_Publisher      = $request->FinalSelArrList[2][0];
        $ccom->VendorEntityName       = $request->FinalSelArrList[3][0];
        $ccom->Platform               = $request->FinalSelArrList[4][0];
        $ccom->Paid_OwnedMedia        = $request->FinalSelArrList[5][0];
        $ccom->FromDate               = $request->FinalInputArray[1];
        $ccom->ToDate                 = $request->FinalInputArray[2];
        $ccom->Section                = $request->FinalInputArray[3];
        $ccom->AdUnit                 = $request->FinalInputArray[4];
        $ccom->AdSize                 = $request->FinalInputArray[5];
        $ccom->Days                   = $request->FinalInputArray[6];
        $ccom->GeoTargeting           = $request->FinalInputArray[7];
        $ccom->BuyType                = $request->FinalInputArray[8];
        $ccom->BuySubType             = $request->FinalInputArray[9];
        $ccom->Deliverables           = $request->FinalInputArray[10];
        $ccom->Quantity               = $request->FinalInputArray[11];
        $ccom->GrossRate              = $request->FinalInputArray[12];
        $ccom->GrossCost              = $request->FinalInputArray[13];
        $ccom->NetRate                = $request->FinalInputArray[14];
        $ccom->NetCost                = $request->FinalInputArray[15];
        $ccom->Brand                  = $request->FinalInputArray[16];
        $ccom->Variant                = $request->FinalInputArray[17];
        $ccom->ClientName             = $request->FinalInputArray[18];
        $ccom->CampaignName           = $request->FinalInputArray[19];
        $ccom->Vendor_PublisherEmail  = $request->FinalInputArray[20];
        $ccom->SubBrand               = $request->FinalInputArray[21];
        $ccom->Objective              = $request->FinalInputArray[22];
        $ccom->SubObjective           = $request->FinalInputArray[23];
        $ccom->FormatType             = $request->FinalInputArray[24];
        $ccom->Impressions            = $request->FinalInputArray[25];
        $ccom->Clicks                 = $request->FinalInputArray[26];
        $ccom->VideoViews             = $request->FinalInputArray[27];
        $ccom->Engagement             = $request->FinalInputArray[28];
        $ccom->Visits                 = $request->FinalInputArray[29];
        $ccom->Leads                  = $request->FinalInputArray[30];
        $ccom->Reach                  = $request->FinalInputArray[31];
        $ccom->Installs               = $request->FinalInputArray[32];
        $ccom->SMS                    = $request->FinalInputArray[33];
        $ccom->Mailer                 = $request->FinalInputArray[34];
        $ccom->Spots                  = $request->FinalInputArray[35];
        
        $ccom->CreatedBy              = Auth::user()->id;
        $ccom->CreatedOn              = Carbon::now();
        $ccom->ModifiedBy             = Auth::user()->id;
        $ccom->ModifiedOn             = Carbon::now();
        $ccom->IsActive               = 1;
        $ccom->save();

        $AdamsLineItemID=DB::table('AdamsLineItem')->select('AdamsLineItem.id')
                      ->where('AdamsLineItem.IsActive','=', '1')
                      ->orderby('AdamsLineItem.id','desc')
                      ->limit(1)
                      ->first();
      
        $AdamsLineItem_ID=$AdamsLineItemID->id;
      
        $ditArray=($request->FinalSelArrList[0]);

        foreach ($ditArray as $eachDitid ) {   
          $AdamLineItemDIT_ID=DB::table('AdamLineItemDIT')->insertGetId([
              'AdamsLineItem_ID' => $AdamsLineItem_ID,
              'DataInputType_ID' => $eachDitid,     
              'ModifiedOn' => Carbon::now(),
              'isActive' => 1,
              'ModifiedBy' => '1',
              'CreatedBy' => Auth::user()->id,
              'CreatedOn' => Carbon::now()
          ]); 

          // ===============OnNewLine Add: if DIT is manual, make entry in AdamLineItemMetrics and AdamLineItemDaily table======== 
          if($eachDitid=='1')//1 is for Manual 
          {
            $getcampaign_ID = DB :: table('AdamsLineItem') 
                            ->join('Campaign', 'Campaign.id','=','AdamsLineItem.campaign_ID')  
                            ->where('AdamsLineItem.id', $AdamsLineItem_ID)
                            ->select('Campaign.StartDate', 'Campaign.EndDate')
                            ->first();
            $campStartDate=$getcampaign_ID->StartDate;
            $campEndDate=$getcampaign_ID->EndDate;               
            
            $AllMst_Metrics = DB :: table('Mst_Metrics')->get();
            foreach ($AllMst_Metrics as $Mst_Metrics) {
              $Mst_Metrics_ID=$Mst_Metrics->id;
              $AdamLineItemMetrics = $this->addAdamLineItemMetrics($AdamsLineItem_ID,$AdamLineItemDIT_ID, $Mst_Metrics_ID,$campStartDate,$campEndDate);
            }
          }
          // ===============OnNewLine Add: if DIT is manual, make entry in AdamLineItemMetrics and AdamLineItemDaily table========     
        }
        return response()->json(['res'=>'success'], 200);
      }
      else{
        return response()->json(['res'=>'error'], 200);
      }
    }
  }
  public function saveAdamDCMMApping(Request $request)
  {
    //dd($request->all());
    $isSpecificClient=DB::table('Client')
                  ->where('Client.id','=',$request->spClientId)
                  ->select('Client.specialClientEditAdamKey')
                  ->get();
    $isSpecificClient=json_decode($isSpecificClient); 
    //dd($isSpecificClient[0]->specialClientEditAdamKey);
    if(!is_null($request->DCMKey)  && !empty($request->Device))
    {
      $count = count($request->DCMKey);
      $AdamsLineItem_ID=$request->AdamsLineItem_ID;
      $adamUniqueKey=$request->adamUniqueKey;
    
      //dd($adamUniqueKey);
      for($i = 0; $i < $count; $i++){
        // ----------specific Clientcheck-------------------------
        if($isSpecificClient[0]->specialClientEditAdamKey==1){
          $DCMKey= $request->DCMKey[$i];
        }
        else{
          $DCMKey=$adamUniqueKey.'_'.$request->DCMKey[$i];
        }
        //dd($DCMKey);
        // ----------specific Clientcheck-------------------------      
        //============CheckUnique DCMKey============================================ 
        $CheckAllDCMKeyInEntireDB=AdamLineDCMMapping::
                                                    select('AdamLineDCMMapping.id')
                                                    ->where('AdamLineDCMMapping.DCMKey','=',$DCMKey)
                                                    ->first();
        $CheckAllDCMKeyInEntireDB=json_decode($CheckAllDCMKeyInEntireDB); 

        //dd($CheckAllDCMKeyInEntireDB);
        //============CheckUnique DCMKey============================================

        if($CheckAllDCMKeyInEntireDB){
          //echo"No Same";
          return redirect()->back()->with('error','DCM-Key must be unique.');
        }else{
          $ccom = new AdamLineDCMMapping;
          $ccom->AdamsLineItem_ID = $AdamsLineItem_ID;
          
          $ccom->DCMKey = $DCMKey;
          $ccom->CampaignType = $request->CampaignType[$i];
          $ccom->AdSection = $request->AdSection[$i];
          $ccom->BannerSize = $request->BannerSize[$i];
          $ccom->Device = $request->Device[$i];
          
          $ccom->CreatedBy = Auth::user()->id;
          $ccom->CreatedOn = Carbon::now();
          $ccom->ModifiedBy = Auth::user()->id;
          $ccom->ModifiedOn = Carbon::now();
          $ccom->IsActive = 1;

          $ccom->save();
        }
      }
      return redirect()->back()->with('message','DCM Multiple Placement Mapping done Successfully.');  
    }      
    //return Redirect::back();
  }

  public function saveAdamAlwaysOnCampaign(Request $request)
  {
    //dd($request->all());
    $ccom = new AdamAlwaysOnCampaign;
    $ccom->AdamsLineItem_ID = $request->AdamsLineItem_ID;  
    $ccom->FromDate = $request->fromDate;
    $ccom->ToDate = $request->toDate;
    $ccom->Impressions = $request->impressions;
    $ccom->Clicks = $request->clicks;
    $ccom->VideoViews = $request->videoView;
    $ccom->Engagement = $request->engagement;
    $ccom->Visits = $request->visits;
    $ccom->Leads = $request->leads;
    $ccom->Reach = $request->reach;
    $ccom->Installs = $request->installs;
    $ccom->SMS = $request->sms;
    $ccom->Mailer = $request->mailer;
    $ccom->Spots = $request->spots;
    $ccom->NetRate = $request->netRate;
    $ccom->NetCost = $request->netCost;
    
    $ccom->CreatedBy = Auth::user()->id;
    $ccom->CreatedOn = Carbon::now();
    $ccom->ModifiedBy = Auth::user()->id;
    $ccom->ModifiedOn = Carbon::now();
    $ccom->IsActive = 1;

    $ccom->save();
    return Redirect::back();
  }

  public function getAllDCMkey(Request $request){
    $AdamsLineItem_ID=$request->AdamsLineItem_ID; 
    $getAllDCMkeyData=DB::table('AdamLineDCMMapping')
                  ->where('AdamLineDCMMapping.AdamsLineItem_ID','=',  $AdamsLineItem_ID)              
                  ->get();          
    return response()->json(['getAllDCMkeyData'=>$getAllDCMkeyData], 200);  
  }

  public function uploadDCMKeyFile(Request $request)
  {
    //dd($request->all());
    $date = Carbon::now()->format('Ymdhis');
    $current_time = Carbon::now()->toDateTimeString();
    $AdamsLineItem_ID= $request->AdamsLineItem_ID;
    //dd($AdamsLineItem_ID);
    $this->validate($request, [
        'DCMKeyFile' => 'required|mimes:xlsx,xls',
    ]);

    if($isDCMKeyFile=$request->hasfile('DCMKeyFile')){      
      $DCMKeyFile=$request->file('DCMKeyFile');
      $DCMKeyFileData = \Excel::load($DCMKeyFile)->get();

      $DCMKeyfile=$DCMKeyFile->getClientOriginalName();           
      $DCMKeyFileName = pathinfo($DCMKeyfile, PATHINFO_FILENAME);
      $DCMKeyFileNameExtension = pathinfo($DCMKeyfile, PATHINFO_EXTENSION);
      $newDCMKeyFileName = $DCMKeyFileName.'-'.$date.'.'.$DCMKeyFileNameExtension;
      $DCMKeyFile->move(public_path('DCMKeyFiles'), $newDCMKeyFileName); 
      //print_r($DCMKeyFileData); exit;
      $DCMKeyFileDataArray = json_decode(json_encode($DCMKeyFileData), true);
      //echo"<pre>";print_r($DCMKeyFileDataArray);// exit;     
      foreach ($DCMKeyFileDataArray as $eachDCMKeyRow) {
        if($eachDCMKeyRow['dcm_key']){
          //============CheckUnique DCMKey============================================ 
          $CheckAllDCMKeyInEntireDB=AdamLineDCMMapping::
                                                    select('AdamLineDCMMapping.id')
                                                    ->where('AdamLineDCMMapping.DCMKey','=',$eachDCMKeyRow['dcm_key'])
                                                    ->first();
          $CheckAllDCMKeyInEntireDB=json_decode($CheckAllDCMKeyInEntireDB); 

          //dd($CheckAllDCMKeyInEntireDB);
          //============CheckUnique DCMKey============================================

          if(!$CheckAllDCMKeyInEntireDB){
            //echo"No Same";       
            $dataset = new AdamLineDCMMapping;
            $dataset->AdamsLineItem_ID = $AdamsLineItem_ID;
            $dataset->DCMKey           = $eachDCMKeyRow['dcm_key'];
            $dataset->CampaignType     = $eachDCMKeyRow['campaign_type'];
            $dataset->AdSection        = $eachDCMKeyRow['ad_section'];
            $dataset->BannerSize       = $eachDCMKeyRow['banner_size'];
            $dataset->Device           = $eachDCMKeyRow['device'];         
            $dataset->CreatedOn        =$current_time;
            $dataset->CreatedBy        =1;
            $dataset->ModifiedOn       =$current_time;
            $dataset->ModifiedBy       =1;
            $dataset->IsActive         =1;  
            $dataset->save();
            return redirect()->back()->with('message','DCM Multiple Placement Mapping done Successfully.');
          }
          else{
            //echo"Same";
            return redirect()->back()->with('error','DCM-Key must be unique.');
          } 
        }
      }  
    }  
    return redirect()->back()->with('message','DCM-Key File Uploaded Successfully.');
  }

  public function saveLineItemAdsetMapValue(Request $request){
    //echo"<pre>"; print_r($request->all());
    DB::table('AdamsLineItem')
      ->where('id', $request->AdamsLineItem_ID)
      ->update(['LineItemAdsetMap' => $request->radioValue,]);
    return response()->json(['res'=>'success'], 200);  
  }  

  public function deleteAdamLine(Request $request)
  {
    //echo"<pre>"; print_r($request->all());
    $AdamsLineItem_ID=$request->adamRowId;
    $records_count = DB::table('APIParams')
          ->where('APIParams.AdamsLineItem_ID','=',$AdamsLineItem_ID) 
          ->get()
          ->count();
    //echo"<pre>"; print_r($records_count);    exit;  
    if ($records_count==0) 
    {
      AdamsLineItem::where('AdamsLineItem.id','=', $AdamsLineItem_ID)
            ->update(['IsActive'=>0]);
      
      DB::statement("update AdamLineItemDIT set IsActive = 0  where AdamsLineItem_ID = $AdamsLineItem_ID");
      DB::statement("update AdamLineDCMMapping set IsActive = 0  where AdamsLineItem_ID  = $AdamsLineItem_ID");
      return response()->json(['res'=>'success'], 200);
    } else{
      //dd("in else");
    }

  }

  public function CheckUniqueAdamLineItemNameOnEdit(Request $request){

    //echo "<pre>"; print_r($request->all()); exit;
    $AdamsLineItem_ID=$request->adamRowId;

    // $getCampignID=AdamsLineItem::where('AdamsLineItem.id','=', $AdamsLineItem_ID)
    //         ->select('AdamsLineItem.campaign_ID')
    //         ->first();
    // $getCampignID=json_decode($getCampignID);        
    //echo "<pre>"; print_r($getCampignID->campaign_ID);        

    $CheckAllAdamUniqueKeyInEntireDB=AdamsLineItem::
                                                    //where('AdamsLineItem.campaign_ID','=', $getCampignID->campaign_ID)
                                                    select('AdamsLineItem.id')
                                                    ->where('AdamsLineItem.adamUniqueKey','=',$request->specialClientEditAdamKey)
                                                     ->where('AdamsLineItem.isActive','=',1)
                                                    ->first();
    $CheckAllAdamUniqueKeyInEntireDB=json_decode($CheckAllAdamUniqueKeyInEntireDB);                                                  
    //echo "<pre>"; print_r($CheckAllAdamUniqueKeyInEntireDB); // exit;
    // ----------------If No Row matched-----------------------------------------
    if($CheckAllAdamUniqueKeyInEntireDB){
      // ----------------If Row matched for previous adamLineItemID and new AdamUniqueKey : Not allowed to edit----------------------------------
      if($CheckAllAdamUniqueKeyInEntireDB->id!=$AdamsLineItem_ID)
      {
        //print_r('LineItemfoundNewRow'); exit;
        return response()->json(['res'=>'LineItemfoundNewRow'], 200);
      }
      // ----------------If Row matched for previous adamLineItemID and same AdamUniqueKey : Allowed to edit-------------------------------------
      if($CheckAllAdamUniqueKeyInEntireDB->id==$AdamsLineItem_ID){
        //print_r('LineItemfoundForSameRow'); exit;
        return response()->json(['res'=>'LineItemfoundForSameRow', 'specialClientEditAdamKey'=>$request->specialClientEditAdamKey], 200);
      }
    }  
    // ----------------If No Row matched for adamLineItemID and AdamUniqueKey : Allowed to edit-------------------------------------
    else{
      //print_r('LineItemNotfound'); exit;
      return response()->json(['res'=>'LineItemNotfound'], 200);
    }                                                   
  }

  public function mailAdamDataToUser(Request $request){
   
    $result=$request->all();
    
    //dd($result);

    foreach ($result['email'] as $key) {
      $data['email']=$key; 
      //dd($data['email']);exit;
     
      \Mail::send('NewAdamLineUpdate', $data, function ($m) use($data) {
          $m->from(getenv('MAIL_FROM_ADDRESS'), 'IADC');
          $m->to($data['email'], 'Hello User')->subject('New AdamLine Item Added');
      });           
    }
     return redirect()->back()->with('message','Email sent.');      
  }

  public static function DuplicateAdamUniqueKey($adamUniqueKey)
  {
    //dd($adamUniqueKey);
    $adamUniqueKeyExistCheck=DB::table('AdamsLineItem')
            ->where('AdamsLineItem.adamUniqueKey','=',$adamUniqueKey)
            ->where('AdamsLineItem.IsActive','=','1')
            ->get();
            
    $adamUniqueKeyExistCheck=json_decode($adamUniqueKeyExistCheck);

    if($adamUniqueKeyExistCheck){
      //dd("Present");
      return 1;
    }
    else{
      //dd("empty");
      return -1;
    } 
  }

  public function UpdateAdamlineItem($request, $adamUniqueKey, $current_time){
    DB::table('AdamsLineItem')
    ->where('id', $request->adamRowId)
    ->update([
      'adamUniqueKey' => $adamUniqueKey,
      'Type' => $request->Type,
      'Website_Publisher' => $request->Website_Publisher,
      'VendorEntityName' => $request->VendorEntityName,
      'Platform' => $request->Platform,
      'Paid_OwnedMedia' => $request->Paid_OwnedMedia,
      'FromDate' => $request->FromDate,
      'ToDate' => $request->ToDate,
      'Section' => $request->Section,
      'AdUnit' => $request->AdUnit,
      'AdSize' => $request->AdSize,
      'Days' => $request->Days,
      'GeoTargeting' => $request->GeoTargeting,
      'BuyType' => $request->BuyType,
      'BuySubType' => $request->BuySubType,
      'Deliverables' => $request->Deliverables,
      'Quantity' => $request->Quantity,
      'GrossRate' => $request->GrossRate,
      'GrossCost' => $request->GrossCost,
      'NetRate' => $request->NetRate,
      'NetCost' => $request->NetCost,
      'Brand' => $request->Brand,
      'Variant' => $request->Variant,
      'ClientName' => $request->ClientName,
      'CampaignName' => $request->CampaignName,
      'Vendor_PublisherEmail' => $request->Vendor_PublisherEmail,
      'SubBrand' => $request->SubBrand,
      'Objective' => $request->Objective,
      'SubObjective' => $request->SubObjective,
      'FormatType' => $request->FormatType,
      'Impressions' => $request->Impressions,
      'Clicks' => $request->Clicks,
      'VideoViews' => $request->VideoViews,
      'Engagement' => $request->Engagement,
      'Visits' => $request->Visits,
      'Leads' => $request->Leads,
      'Reach' => $request->Reach,
      'Installs'=>$request->Installs,
      'SMS' => $request->SMS,  
      'Mailer' => $request->Mailer,
      'Spots' => $request->Spots,
      'LineItemAdsetMap' =>  $request->radioValue,
      'ModifiedOn' => $current_time,
      'isActive' => 1,
      'ModifiedBy' => '1',
    ]);

    $ditArray=$request->DataInputTypeName;
    $adamRowId=$request->adamRowId;

    // -----for each selected Dit--------
    foreach ($ditArray as $eachDitName ) {
      //echo"<pre>DIT "; print_r($eachDitName); exit;  
      $getDitId = DB :: table('AdamLineItemDIT')
                        ->join('DataInputType', 'DataInputType.id', '=', 'AdamLineItemDIT.DataInputType_ID')                     
                        ->where('AdamLineItemDIT.AdamsLineItem_ID' ,'=',$adamRowId)
                        ->where('DataInputType.DataInputTypeName' ,'=',$eachDitName)
                        ->where('DataInputType.IsActive' ,'=',1)
                        ->select('DataInputType.id','AdamLineItemDIT.id as AdamLineItemDIT_ID')
                        ->first();        

      if(!$getDitId){
        //echo"<pre>inIf"; print_r($getDitId); exit;       
        $getDitID = DB :: table('DataInputType')                     
                        ->where('DataInputType.DataInputTypeName' ,'=',$eachDitName)
                        ->where('DataInputType.IsActive' ,'=',1)
                        ->select('DataInputType.id')
                        ->first();
        //echo"<pre>inIf"; print_r($getDitID->id); exit;       
        $AdamLineItemDIT_ID=DB::table('AdamLineItemDIT')->insertGetId([
            'AdamsLineItem_ID' => $adamRowId,
            'DataInputType_ID' => $getDitID->id,        
            'ModifiedOn' => $current_time,
            'isActive' => 1,
            'ModifiedBy' => '1',
        ]);                         
        //echo"<pre>IF "; print_r($AdamLineItemDIT_ID);
      }
      else{
        $AdamLineItemDIT_ID=$getDitId->AdamLineItemDIT_ID;
      }
      // ===============if DIT is manual, make entry in AdamLineItemMetrics and AdamLineItemDaily table======== 
      if($eachDitName=='Manual')
      {
        $getcampaign_ID = DB :: table('AdamsLineItem') 
                        ->join('Campaign', 'Campaign.id','=','AdamsLineItem.campaign_ID')  
                        ->where('AdamsLineItem.id', $adamRowId)
                        ->select('Campaign.StartDate', 'Campaign.EndDate')
                        ->first();
        $campStartDate=$getcampaign_ID->StartDate;
        $campEndDate=$getcampaign_ID->EndDate;               
        
        $AllMst_Metrics = DB :: table('Mst_Metrics')->get();
        
        foreach ($AllMst_Metrics as $Mst_Metrics) {
          $Mst_Metrics_ID=$Mst_Metrics->id;

          // *******************************************************************************
          $checkEntryInAdamLineItemMetrics= DB :: table('AdamLineItemMetrics') 
                        ->where('AdamsLineItem_ID', $adamRowId)
                        ->where('AdamLineItemDIT_ID', $AdamLineItemDIT_ID)
                        ->where('Mst_Metrics_ID', $Mst_Metrics_ID)
                        ->first();
          // *******************************************************************************
          if(!$checkEntryInAdamLineItemMetrics){              
            $AdamLineItemMetrics = $this->addAdamLineItemMetrics($adamRowId,$AdamLineItemDIT_ID, $Mst_Metrics_ID,$campStartDate,$campEndDate);
          }
        }
      }
      // ===============if DIT is manual, make entry in AdamLineItemMetrics and AdamLineItemDaily table========     
    }
  }

  public function updateDCMMulti(Request $request){
    //dd($request->all());

    $current_time = Carbon::now()->toDateTimeString();

    $DCMKey = $request->FinalInputArray[1]."_".$request->FinalInputArray[2]."_".$request->FinalInputArray[3]."_".$request->FinalInputArray[4]."_".$request->FinalInputArray[5];
    
    DB::table('AdamLineDCMMapping')
      ->where('id', $request->FinalInputArray[0])
      ->update([
        'DCMKey' => $DCMKey,
        'CampaignType' => $request->FinalInputArray[2],
        'AdSection' => $request->FinalInputArray[3],
        'BannerSize' => $request->FinalInputArray[4],
        'Device' => $request->FinalInputArray[5],
        'ModifiedOn' => $current_time,
        'isActive' => 1,
        'ModifiedBy' => '1',
      ]);

    return response()->json(['res'=>'success'], 200);
  }

  public function getManualPublisherExcelInputFile(Request $request){
    $AdamsLineItem_ID=$request->AdamsLineItem_ID; 
    $getManualPublisherExcelInputFile=DB::table('ManualPublisherExcelInputFile')
                  ->where('AdamsLineItem_ID','=',  $AdamsLineItem_ID)  
                  ->where('isActive','=', true)            
                  ->get();         

    //echo"<pre>"; print_r($getManualPublisherExcelInputFile); exit;               
    return response()->json(['getManualPublisherExcelInputFile'=>$getManualPublisherExcelInputFile], 200);  
  }

  public function getAdamLineItem_URLMapping(Request $request){
    $AdamsLineItem_ID=$request->AdamsLineItem_ID; 
    $getAdamLineItem_URLMapping=DB::table('AdamLineItem_URLMapping')
                  ->where('AdamsLineItem_ID','=',  $AdamsLineItem_ID)              
                  ->get();         

    //echo"<pre>"; print_r($getManualPublisherExcelInputFile); exit;               
    return response()->json(['getAdamLineItem_URLMapping'=>$getAdamLineItem_URLMapping], 200);  
  }

  public function deleteManulReport($manualReportId){
    //dd($manualReportId);

    DB::table('ManualPublisherExcelInputFile')
      ->where('id', $manualReportId)
      ->update(['isActive' => 0,]);

    return redirect::back();  
  }
}