<?php $base_url= url('/'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Adam's Table </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="{{asset('css/bootstrap_v3_0_3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/font-awesome-4.7.0.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/customStyles.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/customColours.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/sweetalert.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" />
    <meta name="_token" content="{{ csrf_token() }}">
  </head>
  <body>
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
              <img class="home_logo_main" src="{{asset('images/iadc-logo.png')}}" height="50">
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
              <!-- Authentication Links -->
              @if(Auth::user()->UserRole_ID == 1 ||  Auth::user()->UserRole_ID == 3)
                <li><a href="{{ url('/Campaign') }}">Campaign</a></li>
                <li><a  href="{{ url('/api') }}">API </a></li>
                <li><a href="{{ url('/Person') }}"> User</a></li>
                <li><a href="{{ url('/Client') }}">Client</a></li>
                 <li><a href="{{ url('/matricsMapping') }}">Matrices Mapping</a></li> 
                <li><a href="{{ url('/Card') }}">Card</a></li>
              @endif
              @if(Auth::user()->UserRole_ID == 1 ||  Auth::user()->UserRole_ID == 2)
                <li><a href="{{ url('/Publisher') }}">Publisher</a></li>
              @endif
              @if( Auth::user()->UserRole_ID == 4)
                <li><a href="{{ url('/ClientHome') }}">Campaign</a></li>
              @endif  

              <li class="dropdown">
                <a class=" dropdown-toggle" type="button" data-toggle="dropdown">{{ Auth::user()['name'] }}<span class="caret"></span></a>

                <ul class="dropdown-menu">
                  <li>
                    <a href="{{ url('/logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">Logout
                    </a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                  </li>                          
                </ul>
              </li>
              
            </ul>
        </div>
      </div>    
    </nav>
     <!-- ./container -->
    <div class="container cc_tableContainer">
            @yield('content')
    </div>

       <!-- jQuery 2.0.2 -->
    <script src="{{asset('../js/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <!--<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js" type="text/javascript"></script>-->
    <script src="{{asset('js/fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>    
    <script src="{{asset('js/adam.js')}}" type="text/javascript"></script>

     <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
  </body>
</html>
