<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineItemDaily_PreviousData extends Model
{

	public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['LineItemDaily_ID','LineItemMetricsID','Date','Value','ipAddress','CreatedOn','ModifiedOn','ModifiedBy','isActive'];

	public $table = "dbo.LineItemDaily_PreviousData";

}
