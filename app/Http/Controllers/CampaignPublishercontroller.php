<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\CampaignPublisher;
use App\Publisher;
use App\Campaign;
use App\LineItem;
use App\LineItemDaily;
use App\Location;
use Session;
use DB;
use Validator;
Use Redirect;
use Carbon\Carbon;
use DateTime;
use Excel;
use App\RawExcelData;
use Geocode;
use App\TargettingValue;


class CampaignPublishercontroller extends Controller
{

  private function pre($obj,$flag=1) {
    echo '<pre>';print_r($obj); if($flag===1){die;}
  }

  public function getCampaignPublisherdetails($cb_id)
  {
    if(session('includeLocation')||session('excludeLocation')||session('geoLocation'))
    {
      //session()->flush();
      //To be unset individually
    }
    $Genre=DB::table('Genre')
              ->orderby('GenreName','asc') 
              ->get();
    $selectGenre=0;
    $Genre=json_decode($Genre);

    $Publisher=DB::table('Publisher')
              ->where('isActive',1)
              //->orderby('id','asc')
              ->orderby('PublisherName','asc') 
              ->get();
    $Publisher=json_decode($Publisher);

    $PublisherType=DB::table('PublisherType')
                  ->orderby('id','asc')
                  ->get();
    $publisherType=json_decode($PublisherType);
    //card detail
    $card=DB::table('Card')
                  ->where('isActive',1)
                  ->orderby('id','asc')
                  ->get();
    $card=json_decode($card);

   //end of card
    $DealType=DB::table('DealType')
              //->orderby('id','asc') []
              ->orderby('DealTypeName','asc')  
              ->get();

    $DealType=json_decode($DealType);

    $DeliveryMode=DB::table('DeliveryMode')
                  ->orderby('id','desc')
                  ->get();
    $DeliveryMode=json_decode($DeliveryMode);
    $selectDeliveryMode=0;


    $DataInputType=DB::table('DataInputType')
                  ->orderby('id','asc')
                  ->get();
    $dataInputType=json_decode($DataInputType);

    $Phase=DB::table('Phase')
                  ->orderby('id','asc')
                  ->get();
    $Phase=json_decode($Phase);

    $Objective=DB::table('Objective')
                  ->orderby('id','asc')
                  ->get();
    $Objective=json_decode($Objective);

    $ObjectiveValues=DB::table('ObjectiveValues')
                  ->orderby('id','asc')
                  ->get();
    $ObjectiveValues=json_decode($ObjectiveValues);

    $Medium=DB::table('Medium')
                  //->orderby('id','asc')\
                  ->orderby('MediumName','asc') 
                  ->get();
    $Medium=json_decode($Medium);

    // $Targeting=DB::table('Targeting')->where('TargetingValueType','=','Fixed')
    //               ->where('TargetingName','!=','Gender')
    //               ->orderby('id','asc')
    //               ->get();
     $Targeting=DB::table('Targeting')
                  //->orderby('id','asc')
                  ->orderby('TargetingName','asc') 
                  ->get();

    $Targeting=json_decode($Targeting);

    $TargetingValue=DB::table('TargetingValue')
                  ->orderby('id','asc')
                  ->get();
    $TargetingValue=json_decode($TargetingValue);

    $TargetingValueGender=DB::table('TargetingValue')->where('Targeting_ID','=',3)
                  ->orderby('id','asc')
                  ->get();
    $TargetingValueGender=json_decode($TargetingValueGender);

    $Gender=DB::table('Gender')
                   ->orderby('id','asc')
                   ->get();
    $Gender=json_decode($Gender);

    $FormatType=DB::table('FormatType')
                  ->orderby('FormatTypeName','asc')
                  ->get();
    $FormatType=json_decode($FormatType);

    $Format=DB::table('Format')
                  ->orderby('id','asc')
                  ->get();
    $Format=json_decode($Format);

    $Metrics=DB::table('Metrics')
                  ->orderby('id','asc')
                  ->get();
    $Metrics=json_decode($Metrics);

    $Location=DB::table('Locations')
                  ->orderby('id','asc')
                  ->get();
    $Location=json_decode($Location);


     $Section=DB::table('Section')
                  ->orderby('SectionName','asc') 
                  ->get();

    $Section=json_decode($Section);

    $AdUnit=DB::table('AdUnit')
                  ->orderby('AdUnitName','asc') 
                  ->get();

    $AdUnit=json_decode($AdUnit);

    $CampaignPublisherdetails_index=DB::select('exec SP_IADC_gelAllLineItemDetails ? ', array($cb_id));
    $cityLoc=[];
    foreach ($CampaignPublisherdetails_index as $key )
    {
      if(($key->IsLocationBased==1))
      {

        $cityLoc=DB::select('exec [SP_IADC_getCityLocation] ? ', array($key->id));

        if($cityLoc)
        {
          $excludeArray=[];
          $includeArray=[];

          foreach ($cityLoc as $city ) {
            if($city->toExclude=='1')
            {
               array_push($excludeArray, $city->LocationName);
               $key->excludeLoc=$excludeArray;
            }
            else
            {
               array_push($includeArray, $city->LocationName);
               $key->includeLoc=$includeArray;
            }
          }
        }
      }
      else
      {

        $key->excludeLoc=null;
        $key->includeLoc=null;

      }


     //getTargetingId and Targeting Value
      $targetingValueIDArray=[];
      $targetingIDArray=[];
      $cardDetails=[];
      foreach ($CampaignPublisherdetails_index as $key)
      {
        if(($key->IsTargetingBased==1))
        {
          $TargetingValueIdArray=DB::table('LineItemTargetingValue')
                                ->where('LineItem_ID','=',$key->id)
                                ->where('IsActive','=',1)
                                ->orderby('id','asc')
                                ->pluck('TargetingValue_ID');
                                //->get();
          $TargetingValueIdArray=json_decode($TargetingValueIdArray);
          $getAllTargetingIDD=[];
          $getAllTargetingValueNames=[];
          foreach ($TargetingValueIdArray as $key1)
          {
            //pluckID-----------------------------------------
            $getAllTargetingID=DB::table('TargetingValue')
                                ->where('id','=',$key1)//->get();
                                ->pluck('Targeting_ID');
            $getAllTargetingID=json_decode($getAllTargetingID);
            
            if(array_key_exists(0,$getAllTargetingID)) {
                array_push($getAllTargetingIDD, $getAllTargetingID[0]);
            }

            //pluck Name-----------------------------------------
            $getTargetingValueNames=DB::table('TargetingValue')
                                ->where('id','=',$key1)//->get();
                                ->pluck('TargetingValueName');
            $getTargetingValueNames=json_decode($getTargetingValueNames);
            
            if(array_key_exists(0,$getTargetingValueNames)) {
              array_push($getAllTargetingValueNames, $getTargetingValueNames[0]);
            }
          }
          $key->Targeting_ID=array_unique($getAllTargetingIDD);
          $key->Targeting_ID=array_values($key->Targeting_ID);
          $key->TargetingValue_ID=$TargetingValueIdArray;
        
          // =================TaregtingNames================
          $trgtID=array_values($key->Targeting_ID);
        
          $getAllTargetingNm=[];
          foreach ($trgtID as $keyT)
          {
            //pluckID-----------------------------------------
            $getTargetingNm=DB::table('Targeting')
                                ->where('id','=',$keyT)//->get();
                                ->pluck('TargetingName');
            $getTargetingNm=json_decode($getTargetingNm);

            array_push($getAllTargetingNm, $getTargetingNm[0]);
          }
          $key->TargetingName=$getAllTargetingNm;
          $key->TargetingValueName=$getAllTargetingValueNames;
       
        }
        else
        {
          $key->Targeting_ID=null;
          $key->TargetingValue_ID=null;

          $key->TargetingName=null;
          $key->TargetingValueName=null;

        }
      }
    }

    Session::put('get_Campaign_id', $cb_id);
    $Campaign_ID=Session::get('get_Campaign_id');

    $Campaign_list =DB::table('Campaign')->where('Campaign.id', '=',$cb_id)
                                         //->select('Campaign.CampaignName', 'Campaign.CampaignID')
                                         ->get();

    $Campaign_list=json_decode($Campaign_list);

     
  if(!empty($Campaign_list))
    {
      $Campaign_name=$Campaign_list[0]->CampaignName;
      $CampaignID=$Campaign_list[0]->CampaignID;
       $CampStartDate=$Campaign_list[0]->StartDate;
    }
    else{

      $Campaign_name=Null;
      $CampaignID=Null;
      $CampStartDate=Null;
    }

     
    Session::put('CampaignName', $Campaign_name);
    $CampaignName=Session::get('CampaignName');

    $getClientDeatils= DB::table('Campaign')
                        //table('LineItem')
                      //->join('CampaignPublisher','CampaignPublisher.id','=','LineItem.CampaignPublisher_ID')
                      //->join('Campaign','Campaign.id','=','CampaignPublisher.Campaign_ID')
                      ->join('ClientBrand','ClientBrand.id','=','Campaign.ClientBrand_ID')
                      ->join('Client','Client.id','=','ClientBrand.Client_ID')
                      ->where('Campaign.id','=',$cb_id)
                      ->get();

    $getApiPublisher= DB:: select('select Publisher.id as pubId, Publisher.PublisherName, PublisherType.PublisherTypeName from Publisher
      inner join PublisherType on PublisherType.id = Publisher.PublisherType_ID 
      where Publisher.PublisherType_ID =1
      ORDER BY Publisher.PublisherName ASC');
                        
//echo '<pre>';print_r($CampaignPublisherdetails_index); exit;        
   return view('CampaignPublisher.getCampaignPublisherdetails',compact(
      'CampaignPublisherdetails_index',
      'Genre',        'Phase',  'Medium',   'Targeting',      'Gender',
      'FormatType',   'Format', 'Objective','ObjectiveValues','Metrics',
      'DeliveryMode', 'selectDeliveryMode', 'DealType',       'Campaign_name',
      'Campaign_ID',  'CampaignID',         'Publisher',      'publisherType',
      'dataInputType' ,'AdUnit' ,'Section','getClientDeatils','card', 'getApiPublisher','CampStartDate'
    ))->with('i'); 
  }

  public function storeCP(Request $request) {

    $arr=Input::all();
    $test = $this->storeCPByReqAndExcel($arr);
    return Redirect::back();
  }
  // --------------------------------------------------------------------------------------
  public function storeCPByReqAndExcel($arr) {
    // $arr=Input::all();

   // dd($arr);
    //-------------------------------------------------------------------------------------
    $includeLocation=session('includeLocation');
    $excludeLocation=session('excludeLocation');
    $geoLocation=session('geoLocation'); // its a array

    if(is_array($includeLocation)){
        $includeLocArray = $includeLocation;
        $excludeLocArray = $excludeLocation;
    }else{
      $includeLocArray = explode(',', $includeLocation);
      $excludeLocArray = explode(',', $excludeLocation);
    }

    $IsLocationBased=Null;
    if($geoLocation)
    {
      $xCoord=$geoLocation['lat'];
      $yCoord=$geoLocation['lng'];
      $googleLocation=$geoLocation['addresspicker_map'];
      $googleRadius=$geoLocation['distance'];
    }
    if($includeLocation || $excludeLocation)
    {
      $IsLocationBased=1;
    }
    //if( ( empty($arr['Genre_ID']) && empty($arr['Publisher_ID']) ) || empty($arr['Genre_ID']) ||  empty($arr['Publisher_ID']))
    if(empty($arr['Publisher_ID']) ||  $arr['Publisher_ID'] =="No Result")
    {
     // dd("if");
      //session()->flash('error_msg', 'Genre and Publisher Selection is compulsory');
      session()->flash('error_msg', 'Publisher Selection is compulsory');
      return Redirect::back();
    }
    else
    {
      //dd("else");
      $CampaignID= $arr['CampaignID'];
      $Campaign_ID= $arr['Campaign_ID'];

      $getCampaign = DB :: table('Campaign')->where('id','=',$Campaign_ID)->where('CampaignID','=',$CampaignID)->get();

      // create Uniuque CampaignID
      $getLineItemID= DB :: table('LineItem')
                            ->join('CampaignPublisher','LineItem.CampaignPublisher_ID','=','CampaignPublisher.id')

                            ->join('Campaign','CampaignPublisher.Campaign_ID','=','Campaign.id')
                            ->where('CampaignID','=',$CampaignID)
                            ->select(DB::raw('max(LineItemID) as LineItemID' ))->pluck('LineItemID');

      $getLineItemID=json_decode($getLineItemID);

      $digits = 5;
      $random5digit = rand(pow(10, $digits-1), pow(10, $digits)-1);
      $lineItemId = $CampaignID.$random5digit;

      
      //---- end create Uniuque CampaignID-------------------------------------------------------

      $Campaign_list =DB::table('Campaign')->where('Campaign.id', '=',$Campaign_ID)
                                           ->select('Campaign.CampaignName')->get();
      $Campaign_list=json_decode($Campaign_list);
      $Campaign_name=$Campaign_list[0]->CampaignName;

      Session::put('CampaignName', $Campaign_name);
      $CampaignName=Session::get('CampaignName');

      $current_time = Carbon::now()->toDateTimeString();

      $checkCampPubExist=DB::table('CampaignPublisher')
                  ->where('Campaign_ID' ,'=', $arr['Campaign_ID'])
                  ->where('Publisher_ID' ,'=',$arr['Publisher_ID'])
                  ->get();

      $checkCampPubExist =json_decode($checkCampPubExist);

      if($checkCampPubExist){
          $last_id = $checkCampPubExist;
      }
      else
      {
        DB::table('CampaignPublisher')->insert(
          [
            'Campaign_ID' => $arr['Campaign_ID'],
            'Publisher_ID' =>$arr['Publisher_ID'],
            'isActive' => '1',
            'ModifiedBy' => '1',
            'ModifiedOn' => $current_time,
            'CreatedOn' => $current_time
          ]
        );

        $last_id =  DB::table('CampaignPublisher')->orderBy('id', 'DESC')->get();
        $last_id = json_decode($last_id);
      }

     if(array_key_exists('TargetingValue_ID' , $arr))
      {
          $targetingValueArray= empty($arr['TargetingValue_ID']) ? Null : $arr['TargetingValue_ID'];
          $IsTargetingBased=1;
      }
      else{
        $targetingValueArray=[];
        $IsTargetingBased=Null;
      }    
      
      if(isset($arr['lineItemType'])&&$arr['lineItemType']==1)
      {
      DB::table('LineItem')->insert([

        'CampaignPublisher_ID' => $last_id[0]->id,
        'LineItemID'  =>$lineItemId,
        'LineItemName'  => $arr['LineItemName'],
        'Phase_ID'    =>empty($arr['Phase_ID'])?  Null : $arr['Phase_ID'],

        'ObjectiveValue_ID'=>empty($arr['ObjectiveValue_ID'])?  Null : $arr['ObjectiveValue_ID'],
        'Medium_ID'   =>empty($arr['Medium_ID'])? Null : $arr['Medium_ID'] ,
        'DealType_ID' => empty($arr['DealType_ID'])?  Null : $arr['DealType_ID'],
        'DeliveryMode_ID' => empty($arr['DeliveryMode_ID'])? Null : $arr['DeliveryMode_ID'],
        'AgeRangeFrom' => empty($arr['target_ageRangeFrom'])? Null : $arr['target_ageRangeFrom'],
        'AgeRangeTo' => empty($arr['target_ageRangeTo'])? Null : $arr['target_ageRangeTo'] ,
        'Gender_ID' => empty($arr['Gender_ID'])? Null : $arr['Gender_ID'] ,

        'Format_ID'  => empty($arr['Format_ID'])?  Null : $arr['Format_ID'],
        'FormatValue' => empty($arr['FormatValue'])?  Null : $arr['FormatValue'] ,

        'Section_ID' => empty($arr['Section_ID'])?  Null : $arr['Section_ID'] ,
        'AdUnit_ID' => empty($arr['AdUnit_ID'])?  Null : $arr['AdUnit_ID'] ,

        'GoogleXCoord' => isset($xCoord)? $xCoord : Null ,
        'GoogleYCoord' => isset($yCoord)? $yCoord : Null ,
        'GoogleLocation'=>isset($googleLocation)? $googleLocation : Null ,
        'GoogleRadius' => isset($googleRadius)? $googleRadius : Null ,
        'IsLocationBased' => $IsLocationBased ,
        'IsTargetingBased' => $IsTargetingBased,
        'ModifiedBy' => '1',
        'ModifiedOn' => $current_time,
        'CreatedOn' => $current_time,
        'isActive' => 1,
        'Creative_ID'=>Null
      ]);
    }
    else 
    {
      $k=0;
     
      foreach ($arr['creative_id'] as $id) 
      {
        $lastlineItem= DB::table('LineItem')->insertGetId([

          'CampaignPublisher_ID' => $last_id[0]->id,
          'LineItemID'  =>$lineItemId.$k,
          'LineItemName'  => $arr['LineItemName'],
          'Phase_ID'    =>empty($arr['Phase_ID'])?  Null : $arr['Phase_ID'],

          'ObjectiveValue_ID'=>empty($arr['ObjectiveValue_ID'])?  Null : $arr['ObjectiveValue_ID'],
          'Medium_ID'   =>empty($arr['Medium_ID'])? Null : $arr['Medium_ID'] ,
          'DealType_ID' => empty($arr['DealType_ID'])?  Null : $arr['DealType_ID'],
          'DeliveryMode_ID' => empty($arr['DeliveryMode_ID'])? Null : $arr['DeliveryMode_ID'],
          'AgeRangeFrom' => empty($arr['target_ageRangeFrom'])? Null : $arr['target_ageRangeFrom'],
          'AgeRangeTo' => empty($arr['target_ageRangeTo'])? Null : $arr['target_ageRangeTo'] ,
          'Gender_ID' => empty($arr['Gender_ID'])? Null : $arr['Gender_ID'] ,

          'Format_ID'  => empty($arr['Format_ID'])?  Null : $arr['Format_ID'],
          'FormatValue' => empty($arr['FormatValue'])?  Null : $arr['FormatValue'] ,

          'Section_ID' => empty($arr['Section_ID'])?  Null : $arr['Section_ID'] ,
          'AdUnit_ID' => empty($arr['AdUnit_ID'])?  Null : $arr['AdUnit_ID'] ,

          'GoogleXCoord' => isset($xCoord)? $xCoord : Null ,
          'GoogleYCoord' => isset($yCoord)? $yCoord : Null ,
          'GoogleLocation'=>isset($googleLocation)? $googleLocation : Null ,
          'GoogleRadius' => isset($googleRadius)? $googleRadius : Null ,
          'IsLocationBased' => $IsLocationBased ,
          'IsTargetingBased' => $IsTargetingBased,
          'ModifiedBy' => '1',
          'ModifiedOn' => $current_time,
          'CreatedOn' => $current_time,
          'isActive' => 1,
          'Creative_ID'=>$id
        ]);
        //code adding line item metrics
        if(isset($arr['Impressions']) && ($arr['Impressions']!=null))
        {
          $Metrics_ID=1;
          $DataInputType_ID=empty($arr['PIDataInputTypeID']) ? Null : ($arr['PIDataInputTypeID']);
          $PlannedValue=$arr['Impressions'];
          $lineItemMetrics_ID =$this->addLineItemMetrics($lastlineItem,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

        if(isset($arr['Clicks'])&&($arr['Clicks']!=null))
        {
          $Metrics_ID=2;
          $DataInputType_ID=empty($arr['PCDataInputTypeID']) ? Null : ($arr['PCDataInputTypeID']);
          $PlannedValue=$arr['Clicks'];
          $lineItemMetrics_ID = $this->addLineItemMetrics($lastlineItem,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

        if(isset($arr['Leads'])&&($arr['Leads']!=null))
        {
          $Metrics_ID=5;
          $DataInputType_ID=empty($arr['PLDataInputTypeID']) ? Null : ($arr['PLDataInputTypeID']);
          $PlannedValue=$arr['Leads'];
          $lineItemMetrics_ID = $this->addLineItemMetrics($lastlineItem,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }


        if(isset($arr['Rate'])&&($arr['Rate']!=null))
        {
          $Metrics_ID=14;
          $DataInputType_ID=empty($arr['PNRADataInputTypeID']) ? Null : ($arr['PNRADataInputTypeID']);
          $PlannedValue=$arr['Rate'];
          $lineItemMetrics_ID = $this->addLineItemMetrics($lastlineItem,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

        if(isset($arr['Cost'])&&($arr['Cost']!=null))
        {
          $Metrics_ID=15;
          $DataInputType_ID=empty($arr['PNCoDataInputTypeID']) ? Null : ($arr['PNCoDataInputTypeID']);
          $PlannedValue=$arr['Cost'];
          $lineItemMetrics_ID = $this->addLineItemMetrics($lastlineItem,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

        if(isset($arr['Opens'])&&($arr['Opens']!=null))
        {
          $Metrics_ID=16;
          $DataInputType_ID=empty($arr['PODataInputTypeID']) ? Null : ($arr['PODataInputTypeID']);
          $PlannedValue=$arr['Opens'];
          $lineItemMetrics_ID = $this->addLineItemMetrics($lastlineItem,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }
       //end
        $k++;
      }//end of foreach
    }
    
    $lastLineItem_id =  DB::table('LineItem')->orderBy('id', 'DESC')->get();
    $lastLineItem_id = json_decode($lastLineItem_id);

    if(!empty($targetingValueArray)){
      foreach ($targetingValueArray as $key => $value) {
          $LineItemLocation=DB::table('LineItemTargetingValue')->insert([
            'LineItem_ID' => $lastLineItem_id[0]->id,
            'TargetingValue_ID' => $value,

            'CreatedOn'=>$current_time,
            'ModifiedOn'=>$current_time,
            'ModifiedBy' => 1,
            'IsActive' =>True
          ]);
      }
    }

    //end targetingValueID mapping with LineItem
    if(isset($arr['lineItemType'])&& $arr['lineItemType']==1)
      {
		$trimmed_impressions = trim($arr['Impressions']);
        if($trimmed_impressions!='' && !is_null($trimmed_impressions) && array_key_exists('PIDataInputTypeID',$arr) && !is_null($arr['PIDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=1;
		  $DataInputType_ID=(array_key_exists('PIDataInputTypeID',$arr) && !is_null($arr['PIDataInputTypeID']) && $arr['PIDataInputTypeID']!='')?$arr['PIDataInputTypeID']:Null;
          $PlannedValue=$arr['Impressions'];

          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

		$trimmed_clicks = trim($arr['Clicks']);
        if($trimmed_clicks!='' && !is_null($trimmed_clicks) && array_key_exists('PCDataInputTypeID',$arr) && !is_null($arr['PCDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=2;
		  $DataInputType_ID=(array_key_exists('PCDataInputTypeID',$arr) && !is_null($arr['PCDataInputTypeID']) && $arr['PCDataInputTypeID']!='')?$arr['PCDataInputTypeID']:Null;
          $PlannedValue=$arr['Clicks'];

          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

		$trimmed_visits = trim($arr['Visits']);
        if($trimmed_visits!='' && !is_null($trimmed_visits) && array_key_exists('PVsDataInputTypeID',$arr) && !is_null($arr['PVsDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=3;
		  $DataInputType_ID=(array_key_exists('PVsDataInputTypeID',$arr) && !is_null($arr['PVsDataInputTypeID']) && $arr['PVsDataInputTypeID']!='')?$arr['PVsDataInputTypeID']:Null;
          $PlannedValue=$arr['Visits'];

          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

		$trimmed_views = trim($arr['Views']);
        if($trimmed_views!='' && !is_null($trimmed_views) && array_key_exists('PVwDataInputTypeID',$arr) && !is_null($arr['PVwDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=4;
		  $DataInputType_ID=(array_key_exists('PVwDataInputTypeID',$arr) && !is_null($arr['PVwDataInputTypeID']) && $arr['PVwDataInputTypeID']!='')?$arr['PVwDataInputTypeID']:Null;
          $PlannedValue=$arr['Views'];

          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

		$trimmed_leads = trim($arr['Leads']);
        if($trimmed_leads!='' && !is_null($trimmed_leads) && array_key_exists('PLDataInputTypeID',$arr) && !is_null($arr['PLDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=5;
		  $DataInputType_ID=(array_key_exists('PLDataInputTypeID',$arr) && !is_null($arr['PLDataInputTypeID']) && $arr['PLDataInputTypeID']!='')?$arr['PLDataInputTypeID']:Null;
          $PlannedValue=$arr['Leads'];

          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

		$trimmed_engagement = trim($arr['Engagement']);
        if($trimmed_engagement!='' && !is_null($trimmed_engagement) && array_key_exists('PEDataInputTypeID',$arr) && !is_null($arr['PEDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=6;
		  $DataInputType_ID=(array_key_exists('PEDataInputTypeID',$arr) && !is_null($arr['PEDataInputTypeID']) && $arr['PEDataInputTypeID']!='')?$arr['PEDataInputTypeID']:Null;
          $PlannedValue=$arr['Engagement'];

          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

		$trimmed_netrate = trim($arr['NetRate']);
        if($trimmed_netrate!='' && !is_null($trimmed_netrate) && array_key_exists('PNRDataInputTypeID',$arr) && !is_null($arr['PNRDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=7;
		  $DataInputType_ID=(array_key_exists('PNRDataInputTypeID',$arr) && !is_null($arr['PNRDataInputTypeID']) && $arr['PNRDataInputTypeID']!='')?$arr['PNRDataInputTypeID']:Null;
          $PlannedValue=$arr['NetRate'];

          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

		$trimmed_netcost = trim($arr['NetCost']);
        if($trimmed_netcost!='' && !is_null($trimmed_netcost) && array_key_exists('PNCDataInputTypeID',$arr) && !is_null($arr['PNCDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=8;
		  $DataInputType_ID=(array_key_exists('PNCDataInputTypeID',$arr) && !is_null($arr['PNCDataInputTypeID']) && $arr['PNCDataInputTypeID']!='')?$arr['PNCDataInputTypeID']:Null;
          $PlannedValue=$arr['NetCost'];

          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }
		
		$trimmed_reach = trim($arr['Reach']);
        if($trimmed_reach!='' && !is_null($trimmed_reach) && array_key_exists('PRDataInputTypeID',$arr) && !is_null($arr['PRDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=9;
		  $DataInputType_ID=(array_key_exists('PRDataInputTypeID',$arr) && !is_null($arr['PRDataInputTypeID']) && $arr['PRDataInputTypeID']!='')?$arr['PRDataInputTypeID']:Null;
          $PlannedValue=$arr['Reach'];

          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

		$trimmed_installs = trim($arr['Installs']);
        if($trimmed_installs!='' && !is_null($trimmed_installs) && array_key_exists('PInDataInputTypeID',$arr) && !is_null($arr['PInDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=10;
		  $DataInputType_ID=(array_key_exists('PInDataInputTypeID',$arr) && !is_null($arr['PInDataInputTypeID']) && $arr['PInDataInputTypeID']!='')?$arr['PInDataInputTypeID']:Null;
          $PlannedValue=$arr['Installs'];

          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

		$trimmed_spots = trim($arr['Spots']);
        if($trimmed_spots!='' && !is_null($trimmed_spots) && array_key_exists('PSpDataInputTypeID',$arr) && !is_null($arr['PSpDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=11;
		  $DataInputType_ID=(array_key_exists('PSpDataInputTypeID',$arr) && !is_null($arr['PSpDataInputTypeID']) && $arr['PSpDataInputTypeID']!='')?$arr['PSpDataInputTypeID']:Null;
          $PlannedValue=$arr['Spots'];
          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }
		
		$trimmed_SMS = trim($arr['SMS']);
        if($trimmed_SMS!='' && !is_null($trimmed_SMS) && array_key_exists('PSmDataInputTypeID',$arr) && !is_null($arr['PSmDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=12;
          $DataInputType_ID=(array_key_exists('PSmDataInputTypeID',$arr) && !is_null($arr['PSmDataInputTypeID']) && $arr['PSmDataInputTypeID']!='')?$arr['PSmDataInputTypeID']:Null;
          $PlannedValue=$arr['SMS'];
          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }

		$trimmed_mailers = trim($arr['Mailers']);
        if($trimmed_mailers!='' && !is_null($trimmed_mailers) && array_key_exists('PMDataInputTypeID',$arr) && !is_null($arr['PMDataInputTypeID']))
        {
          $LineItem_ID=$lastLineItem_id[0]->id;
          $Metrics_ID=13;
		  $DataInputType_ID=(array_key_exists('PMDataInputTypeID',$arr) && !is_null($arr['PMDataInputTypeID']) && $arr['PMDataInputTypeID']!='')?$arr['PMDataInputTypeID']:Null;
          $PlannedValue=$arr['Mailers'];

          $addLineItemMetrics = $this->addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
        }
      }
      
      if($IsLocationBased==1)
      {
        if($includeLocation)
        {
          foreach ($includeLocArray as $key => $value) {
            $LineItemLocation=DB::table('LineItemLocation')->insert([
                                  'LineItem_ID' => $lastLineItem_id[0]->id,
                                  'Location_ID' => $value,
                                  'toExclude' => Null,
                                  'CreatedOn'=>$current_time,
                                  'ModifiedOn'=>$current_time,
                                  'ModifiedBy' => 1,
                                  'IsActive' =>True
                                ]);
          }
        }
        
        if($excludeLocation)
        {
          foreach ($excludeLocArray as $key => $value) {
            $LineItemLocation=DB::table('LineItemLocation')->insert([
                                  'LineItem_ID' => $lastLineItem_id[0]->id,
                                  'Location_ID' => $value,
                                  'toExclude' => 1 ,
                                  'CreatedOn'=>$current_time,
                                  'ModifiedOn'=>$current_time,
                                  'ModifiedBy' => 1,
                                  'IsActive' =>True


                                ]);
          }
        }
      }
      //return Redirect::back();
  }
}

  // --------------------------------------------------------------------------------------
  public function addLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID)
  {
    $current_time = Carbon::now()->toDateTimeString();
    $LineItemMetrics=DB::table('LineItemMetrics')->insert([
      'LineItem_ID' => $LineItem_ID,
      'Metrics_ID' =>$Metrics_ID,
      'DataInputType_ID' =>$DataInputType_ID,
      'PlannedValue' =>round($PlannedValue),

      'CreatedOn' => $current_time,
      'ModifiedOn' => $current_time,
      'ModifiedBy' => '1',
      'isActive' => 1
    ]);

    $lastLineItemMetrics_id =  DB::table('LineItemMetrics')->orderBy('id', 'DESC')->get();
    $lastLineItemMetrics_id = json_decode($lastLineItemMetrics_id);
    //get compaingn days and dates
    $getCampaignDates = DB::table('Campaign')
                      ->where('Campaign.id','=',$Campaign_ID)
                      ->get();
    $getCampaignDates=json_decode($getCampaignDates);

    $campStartDate=$getCampaignDates[0]->StartDate;
    $campEndDate=$getCampaignDates[0]->EndDate;

    $camp_StartDate = new DateTime($campStartDate);
    $camp_EndDate = new DateTime($campEndDate);

    $noOfDaysOfCompaign = ($camp_EndDate->diff($camp_StartDate)->format("%a"))+1;
    // array for all dates
    for($i=0; $i<$noOfDaysOfCompaign; $i++) {
      $date = strtotime("+$i day", strtotime($campStartDate));
      $allDatesOfCompaign[]=date("Y-m-d", $date);
    }

    foreach($allDatesOfCompaign as $dates) {
      DB::table('LineItemDaily')->insert(
                [
                  'LineItemMetrics_ID' => $lastLineItemMetrics_id[0]->id,
                  'Date' =>$dates,
                  'CreatedOn' => $current_time,
                  'ModifiedOn' => $current_time,
                  'ModifiedBy' => '1',
                  'isLineNumberProcessed'=>0,
                  'isActive' => 1
                ]
      );
    }
  }


//===========================================================================================
  public function deleteCP(Request $request)
  {

    $arr=$request->input();
    $lineItemId=$arr['lineItemid'];

    $lineItemToDelete=DB::select("exec SP_IADC_LineItemDelete ? ", array($lineItemId));
    $lineItemDeleted=$lineItemToDelete[0]->flag;

    if($lineItemDeleted==1)
    {
       return response()->json(['status' => 'success'], 200);
    }
    elseif($lineItemDeleted==-1)
    {
       return response()->json(['status' => 'unsuccessful'], 200);
    }
  }

//-----------------------------------------------------------------------------------------------
  public function selectPublisherGenreMap(Request $request)
  {

    $publisher = DB::table('Publisher')
                ->where('Genre_ID', '=', $request->input('genre_id'))
                ->where('isActive', '=', 1)
                ->orderBy('PublisherName', 'asc')
                ->get();

    return response()->json(['status' => 'success','publisher'=>$publisher], 200);
  }
  // -- display list of all publisher when no Genre selection
  public function selectAllPublisherGenreMap(Request $request)
  {

    $publisher = DB::table('Publisher')
                ->orderBy('PublisherName', 'asc')
                ->get();

    return response()->json(['status' => 'success','publisher'=>$publisher], 200);
  }
  // -- end display list of all publisher when no Genre selection
  // public function selectGenreWRTPublisher(Request $request){
  //     $Genre =  DB::table('Genre')
  //               ->join('Publisher', 'Publisher.Genre_ID','=', 'Genre.id') 
  //               ->where('Publisher.id', '=', $request->input('pubId'))
  //               ->where('isActive', '=', 1)
  //               ->orderBy('GenreName', 'asc')
  //               ->get();

  //   return response()->json(['status' => 'success','Genre'=>$Genre], 200);
  // }

//function to get creative on basis of card
 public function selectCreative(Request $request)
 {
   $creative= DB::table('Creative')
                        ->join('Card', 'Creative.Card_ID', '=', 'Card.id')
                        ->select('Creative.*','Card.CardName')
                        ->whereIn('Card_ID',$request['card_id'])
                        ->get();

   return response()->json(['status' => 'success','creative'=>$creative], 200);
 }
//end of function

  public function selectDataTypePublisherMap(Request $request)
  {

    $publisher = DB::table('Publisher')->where('id', '=', $request->input('p_id'))->get();
    $publisher = json_decode($publisher);

    $publisherType = $publisher[0]->PublisherType_ID;

    if($publisherType==1) {
      // $pYId = array('1','4','6','7');

      $pYId = array('1','2','3','4','6','7','8','9','10','11');
    }
    else {
      //$pYId = array('1',$publisherType,'4','6');
      $pYId = array('1','2','3','4','6','7','8','9','10','11');

    }

    $dataInputType = DB::table('DataInputType')->whereIn('id',  $pYId)
                                            ->get();

    return response()->json(['status' => 'success','dataInputType'=>$dataInputType], 200);
  }
  public function  selectTargetingTargetingValueMap(Request $request)
  {

    $targetingValue = DB::table('TargetingValue')->whereIn('Targeting_ID',  $request->
      input('targeting_id'))
      ->orderby('TargetingValueName', 'asc')->get();
    return response()->json(['status' => 'success','targetingValue'=>$targetingValue], 200);
  }
  public function  selectFormatTypeFormatMap(Request $request)
  {
    $formatType_id = $request->input('formatType_id');
    $format = DB::table('Format')->where('FormatType_ID', '=', $request->input('formatType_id'))->orderBy('FormatName','asc')->get();
    return response()->json(['status' => 'success','format'=>$format, 'formatType_id'=>$formatType_id], 200);
  }
  public function  selectObjectiveObjectiveMap(Request $request)
  {
    $objectiveValue = DB::table('ObjectiveValues')
                      ->where('Objective_ID', '=', $request->input('objective_id'))
                      ->orderBy('ObjectiveValueName','asc')
                      ->get();
	

    return response()->json(['status' => 'success','objectiveValue'=>$objectiveValue], 200);
  }
  public function  getOtherTargetingDropdwn(Request $request)
  {
    $Targeting=DB::table('Targeting')->where('TargetingValueType','=','Fixed')
                  ->orderby('id','asc')
                  ->get();
    $Targeting=json_decode($Targeting);
    return response()->json(['status' => 'success','Targeting'=>$Targeting], 200);
  }
  public function getCampaignPublisherdetailsForReport($cb_id)
  {
    $getLineItemDetails=DB::select('exec SP_IADC_LineItemReport ? ', array($cb_id));
    if($getLineItemDetails)
    {
      $getNotNullMatrices=[];
      if($getLineItemDetails[0]->Impression)
      {
        array_push($getNotNullMatrices,1);
      }
      if($getLineItemDetails[0]->Clicks)
      {
        array_push($getNotNullMatrices,2);
      }
      if($getLineItemDetails[0]->Visits)
      {
        array_push($getNotNullMatrices,3);
      }
      if($getLineItemDetails[0]->Views)
      {
        array_push($getNotNullMatrices,4);
      }
      if($getLineItemDetails[0]->Engagement)
      {
        array_push($getNotNullMatrices,5);
      }
      if($getLineItemDetails[0]->Leads)
      {
        array_push($getNotNullMatrices,6);
      }
      if($getLineItemDetails[0]->NetRate)
      {
        array_push($getNotNullMatrices,7);
      }
      if($getLineItemDetails[0]->NetCost)
      {
        array_push($getNotNullMatrices,8);
      }
      if($getLineItemDetails[0]->Reach)
      {
        array_push($getNotNullMatrices,9);
      }
      if($getLineItemDetails[0]->Installs)
      {
        array_push($getNotNullMatrices,10);
      }
      if($getLineItemDetails[0]->Spots)
      {
        array_push($getNotNullMatrices,11);
      }
      if($getLineItemDetails[0]->SMS)
      {
        array_push($getNotNullMatrices,12);
      }
      if($getLineItemDetails[0]->Mailers)
      {
        array_push($getNotNullMatrices,13);
      }

      $getArrayCount=count($getNotNullMatrices);
    }
    else
    {
     $getLineItemDetails=[];
     $getNotNullMatrices=[0]; //no matric is present
     $getArrayCount=1; //To specify columnSapce value 1 is given
    }
    //-- start campaign name and lineitem name added in report by harshada hotwani 14/3/2019 ---
    Session::put('get_Campaign_id', $cb_id);
    $Campaign_ID=Session::get('get_Campaign_id');
    $Campaign_list =DB::table('Campaign')->where('Campaign.id', '=',$cb_id)
                                         ->select('Campaign.CampaignName', 'Campaign.CampaignID')->get();
    $Campaign_list=json_decode($Campaign_list);

    if(!empty($Campaign_list))
    {
      $Campaign_name=$Campaign_list[0]->CampaignName;
      $CampaignID=$Campaign_list[0]->CampaignID;
    }
    else{
      $Campaign_name=Null;
      $CampaignID=Null;
    }
    Session::put('CampaignName', $Campaign_name);
    $CampaignName=Session::get('CampaignName');
  
    $LineItemName= DB :: table('LineItem')
                            ->join('CampaignPublisher','LineItem.CampaignPublisher_ID','=','CampaignPublisher.id')
                            ->join('Campaign','CampaignPublisher.Campaign_ID','=','Campaign.id')
                            ->where('Campaign.id','=',$cb_id)
                            ->select('LineItem.LineItemName')->get();
    $LineItemName=json_decode($LineItemName);
    //-- end campaign name and lineitem name added in report by harshada hotwani 14/3/2019 ---

    return view('CampaignPublisher.getCampaignPublisherdetailsForReport',compact('getLineItemDetails', 'getNotNullMatrices','getArrayCount','Campaign_name','LineItemName'))->with('i');
  }
  public function setMapSession(Request $request)
  {
    if(isset($request->data)){
      session(['geoLocation'=>$request->data]);
      // session(['includeLocation'=>null]);
      //session(['excludeLocation'=>null]);
      return ['status' => 'success','message'=>'set in session'];
    } else {
      return ['status' => 'success','message'=>'Not set in session'];
    }
  }

  public function setCitySession(Request $request)
  {
      if(isset($request->data)){
      //unset(session('geoLocation'));
      //session(['geoLocation'=>null]);
      session(['includeLocation'=>$request->data['includeLocation']]);
      session(['excludeLocation'=>$request->data['excludeLocation']]);

      $includeLocation = $request->data['includeLocation'];
      $arrIncLocation = explode(",",$includeLocation);
      $excludeLocation = $request->data['excludeLocation'];
      $arrExLocation = explode(",",$excludeLocation);

      return ['status' => 'success','message'=>'set in session'];
      }
      else {
      return ['status' => 'success','message'=>'Not set in session'];
      }
  }
  public function editCP(Request $request)
  {

    $arr=$request->input();
    
    //dd($arr);

    $current_time = Carbon::now()->toDateTimeString();

    if(is_array(session('includeLocation'))) {
      $includeLocation=implode(',',session('includeLocation'));
    } else {
      $includeLocation=NULL;
    }

    if(is_array(session('excludeLocation'))) {
      $excludeLocation=implode(',',session('excludeLocation'));
    } else {
      $excludeLocation=NULL;
    }
    
    $geoLocation=session('geoLocation'); // its a array
    $includeLocArray = explode(',', $includeLocation);
    $excludeLocArray = explode(',', $excludeLocation);

    $IsLocationBased=Null;

    // on same save of googlelocation
    if(isset($arr['GoogleXCoord'])||isset($arr['GoogleYCoord']) || isset($arr['GoogleRadius']) )
    {
      $setByGoogleArray="GA";
      $GoogleXCoordv=$arr['GoogleXCoord'];
      $GoogleYCoordv=$arr['GoogleYCoord'];
      $GoogleLocationv=$arr['GoogleLocation'];
      $GoogleRadiusv=$arr['GoogleRadius'];
    }

    if(isset($geoLocation))
    {
      $GoogleXCoordv=$geoLocation['lat'];
      $GoogleYCoordv=$geoLocation['lng'];
      $GoogleLocationv=$geoLocation['addresspicker_map'];
      $GoogleRadiusv=$geoLocation['distance'];
      $setByGoogleSession="G";
    }
    // set IsLocationBased here
    if($includeLocation || $excludeLocation)
    {
      $setByCitySession="S";
    }

    // on same save of Citylocation
    if((array_key_exists('incLocId', $arr)&&isset($arr['incLocId']))||
       (array_key_exists('excLocId', $arr)&&isset($arr['excLocId'])))
    {
      $setByCityArray="CA";
      $incLocArray=isset($arr['incLocId'])? $arr['incLocId'] : Null;
      $excLocArray=isset($arr['excLocId'])? $arr['excLocId'] : Null;
    }

    if(isset($setByCitySession))
    {
       $IsLocationBased=1;
    }

    else if(isset($setByCityArray))
    {
      $IsLocationBased=1;
    }
    else
    {
      $IsLocationBased=Null;
    }

    if(empty($arr['Genre_ID']) && empty($arr['Publisher_ID']))
    {
      $request->session()->flash('error_msg', 'Genre and Publisher Slection is cumpulsory');
      return Redirect::back();
    }
    else
    {
      $getCampPubId=DB::table('CampaignPublisher')
                  ->where('Campaign_ID' ,'=', $arr['Campaign_ID'])
                  ->where('Publisher_ID' ,'=',$arr['Publisher_ID'])
                  ->first();

      //if given pair of Campain and Publisher is not present, create new entry in 'CampaignPublisher' table 

      if(!$getCampPubId)
      {
        $getCampPubId=DB::table('CampaignPublisher')->insertGetId(
          [
            'Campaign_ID' => $arr['Campaign_ID'],
            'Publisher_ID' =>$arr['Publisher_ID'],
            'isActive' => '1',
            'ModifiedBy' => '1',
            'ModifiedOn' => $current_time,
            'CreatedOn' => $current_time
          ]
        );
      }
      else{
        $getCampPubId=$getCampPubId->id;
      } 
      

      //dd($getCampPubId);

      // ---------------------------------------------------
      //tageting aaray
      if(array_key_exists('TargetingValue_ID' , $arr))
      {
          $targetingValueArray= empty($arr['TargetingValue_ID']) ? Null : $arr['TargetingValue_ID'];
          $IsTargetingBasedEdit=1;
      }
      else{
        $targetingValueArray=[];
        $IsTargetingBasedEdit=Null;
      }



      ////////////////////////UPDATE LINEITEM/////////////////////////////////////////
      $current_time = Carbon::now()->toDateTimeString();

      DB::table('LineItem')
       ->where('id', '=',$arr['LineItem_ID'])

        ->update([

        'CampaignPublisher_ID' => $getCampPubId,
        'LineItemID'  =>$arr['LineItemID'],
        'LineItemName' => $arr['LineItemName'],
        'Phase_ID'    =>empty($arr['Phase_ID'])?  Null : $arr['Phase_ID'],
        'ObjectiveValue_ID'=>empty($arr['ObjectiveValue_ID'])?  Null : $arr['ObjectiveValue_ID'],
        'Medium_ID'   =>empty($arr['Medium_ID'])? Null : $arr['Medium_ID'] ,
        'DealType_ID' => empty($arr['DealType_ID'])?  Null : $arr['DealType_ID'],
        'DeliveryMode_ID' => empty($arr['DeliveryMode_ID'])? Null : $arr['DeliveryMode_ID'],



        'AgeRangeFrom' =>empty($arr['target_ageRangeFrom'])? Null : ($arr['target_ageRangeFrom']) ,// $ageRangeFrom,
        'AgeRangeTo' =>  empty($arr['target_ageRangeTo'])? Null : $arr['target_ageRangeTo'], //$ageRangeTo,
        //genderAdd
        'Gender_ID' =>  empty($arr['Gender_ID'])? Null : $arr['Gender_ID'],

        'Format_ID'  => empty($arr['Format_ID'])?  Null : $arr['Format_ID'],
        'FormatValue' => empty($arr['FormatValue'])?  Null : $arr['FormatValue'] ,

        'Section_ID' => empty($arr['Section_ID'])?  Null : $arr['Section_ID'] ,
        'AdUnit_ID' => empty($arr['AdUnit_ID'])?  Null : $arr['AdUnit_ID'] ,


        'GoogleXCoord' => isset($GoogleXCoordv)? $GoogleXCoordv : Null ,
        'GoogleYCoord' => isset($GoogleYCoordv)? $GoogleYCoordv : Null ,
        'GoogleLocation'=>isset($GoogleLocationv)? $GoogleLocationv : Null ,
        'GoogleRadius' => isset($GoogleRadiusv)? $GoogleRadiusv : Null ,

        'IsLocationBased' => $IsLocationBased,
        'IsTargetingBased' => $IsTargetingBasedEdit,

        'ModifiedBy' => '1',
        'ModifiedOn' => $current_time,
        'CreatedOn' => $current_time,
        'isActive' => 1

      ]);

      //--------------------------------------------------------------------------
      if($IsLocationBased==1 )//&& ($includeLocation || $excludeLocation) )
      {

        $LineItemLocation=DB::table('LineItemLocation')
                      ->where('LineItemLocation.LineItem_ID','=', $arr['LineItem_ID'])
                      ->update(['IsActive' =>0]);

        //start array set by session for inc and exclude
        if(isset($includeLocation)){
          foreach ($includeLocArray as $key => $value) {
            $LineItemLocation=DB::table('LineItemLocation')->insert([
                                  'LineItem_ID' => $arr['LineItem_ID'],
                                  'Location_ID' => $value,
                                  'toExclude' => Null,
                                  'CreatedOn'=>$current_time,
                                  'ModifiedOn'=>$current_time,
                                  'ModifiedBy' => 1,
                                  'IsActive' =>True
                                   ]);
          }
        }

        if(isset($excludeLocation))
        {
          foreach ($excludeLocArray as $key => $value) {

             $LineItemLocation=DB::table('LineItemLocation')->insert([
                                'LineItem_ID' =>  $arr['LineItem_ID'],
                                'Location_ID' => $value,
                                'toExclude' => 1 ,
                                'CreatedOn'=>$current_time,
                                'ModifiedOn'=>$current_time,
                                'ModifiedBy' => 1,
                                'IsActive' =>True
                              ]);
          }
        }
        //end array set by session for inc and exclude
        // ------------------------------------------------------------------------
        //start array set by ARRAY for inc and exclude
        if(isset($incLocArray))
        {
          foreach ($incLocArray as $keyIA => $value) {
            $LineItemLocation=DB::table('LineItemLocation')->insert([
                                  'LineItem_ID' => $arr['LineItem_ID'],
                                  'Location_ID' => $value,
                                  'toExclude' => Null,
                                  'CreatedOn'=>$current_time,
                                  'ModifiedOn'=>$current_time,
                                  'ModifiedBy' => 1,
                                  'IsActive' =>True
                                   ]);
          }
        }

        if(isset($excLocArray))
        {

          foreach ($excLocArray as $keyEA => $value) {

             $LineItemLocation=DB::table('LineItemLocation')->insert([
                                'LineItem_ID' =>  $arr['LineItem_ID'],
                                'Location_ID' => $value,
                                'toExclude' => 1 ,
                                'CreatedOn'=>$current_time,
                                'ModifiedOn'=>$current_time,
                                'ModifiedBy' => 1,
                                'IsActive' =>True
                              ]);
          }
        }
        //end array set by ARRAY for inc and exclude
      }
      //--------------------------------------------------------------------------

      // targeting save
      if(!empty($targetingValueArray)){
         $LineItemTargetingValue=DB::table('LineItemTargetingValue')
                      ->where('LineItemTargetingValue.LineItem_ID','=', $arr['LineItem_ID'])
                      ->update(['IsActive' =>0]);


        foreach ($targetingValueArray as $key => $value) {

            $LineItemLocation=DB::table('LineItemTargetingValue')->insert([
                                    'LineItem_ID' =>$arr['LineItem_ID'],
                                    'TargetingValue_ID' => $value,

                                    'CreatedOn'=>$current_time,
                                    'ModifiedOn'=>$current_time,
                                    'ModifiedBy' => 1,
                                    'IsActive' =>True
                                  ]);
        }
      }
      // targeting save end

      //edit Full line item metrices
      $Campaign_ID=$arr['Campaign_ID'];

      if(isset($arr['Impressions']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=1;
		$DataInputType_ID=(array_key_exists('PIDataInputTypeID',$arr) && !is_null($arr['PIDataInputTypeID']) && $arr['PIDataInputTypeID']!='')?$arr['PIDataInputTypeID']:Null;
        $PlannedValue=$arr['Impressions'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,
          $Campaign_ID);
      }

      if(isset($arr['Clicks']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=2;
		$DataInputType_ID=(array_key_exists('PCDataInputTypeID',$arr) && !is_null($arr['PCDataInputTypeID']) && $arr['PCDataInputTypeID']!='')?$arr['PCDataInputTypeID']:Null;
        $PlannedValue=$arr['Clicks'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }

      if(isset($arr['Visits']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=3;
		$DataInputType_ID=(array_key_exists('PVsDataInputTypeID',$arr) && !is_null($arr['PVsDataInputTypeID']) && $arr['PVsDataInputTypeID']!='')?$arr['PVsDataInputTypeID']:Null;
        $PlannedValue=$arr['Visits'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }

      if(isset($arr['Views']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=4;
		$DataInputType_ID=(array_key_exists('PVwDataInputTypeID',$arr) && !is_null($arr['PVwDataInputTypeID']) && $arr['PVwDataInputTypeID']!='')?$arr['PVwDataInputTypeID']:Null;
        $PlannedValue=$arr['Views'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }
      if(isset($arr['Leads']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=5;
		$DataInputType_ID=(array_key_exists('PLDataInputTypeID',$arr) && !is_null($arr['PLDataInputTypeID']) && $arr['PLDataInputTypeID']!='')?$arr['PLDataInputTypeID']:Null;
        $PlannedValue=$arr['Leads'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }

      if(isset($arr['Engagement']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=6;
		$DataInputType_ID=(array_key_exists('PEDataInputTypeID',$arr) && !is_null($arr['PEDataInputTypeID']) && $arr['PEDataInputTypeID']!='')?$arr['PEDataInputTypeID']:Null;
        $PlannedValue=$arr['Engagement'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }

      if(isset($arr['NetRate']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=7;
		$DataInputType_ID=(array_key_exists('PNRDataInputTypeID',$arr) && !is_null($arr['PNRDataInputTypeID']) && $arr['PNRDataInputTypeID']!='')?$arr['PNRDataInputTypeID']:Null;
        $PlannedValue=$arr['NetRate'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }

      if(isset($arr['NetCost']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=8;
		$DataInputType_ID=(array_key_exists('PNCDataInputTypeID',$arr) && !is_null($arr['PNCDataInputTypeID']) && $arr['PNCDataInputTypeID']!='')?$arr['PNCDataInputTypeID']:Null;
        $PlannedValue=$arr['NetCost'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }

      if(isset($arr['Reach']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=9;
		$DataInputType_ID=(array_key_exists('PRDataInputTypeID',$arr) && !is_null($arr['PRDataInputTypeID']) && $arr['PRDataInputTypeID']!='')?$arr['PRDataInputTypeID']:Null;
        $PlannedValue=$arr['Reach'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }

      if(isset($arr['Installs']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=10;
		$DataInputType_ID=(array_key_exists('PInDataInputTypeID',$arr) && !is_null($arr['PInDataInputTypeID']) && $arr['PInDataInputTypeID']!='')?$arr['PInDataInputTypeID']:Null;
        $PlannedValue=$arr['Installs'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }

      if(isset($arr['Spots']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=11;
		$DataInputType_ID=(array_key_exists('PSpDataInputTypeID',$arr) && !is_null($arr['PSpDataInputTypeID']) && $arr['PSpDataInputTypeID']!='')?$arr['PSpDataInputTypeID']:Null;
        $PlannedValue=$arr['Spots'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }

      if(isset($arr['SMS']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=12;
        $DataInputType_ID=(array_key_exists('PSmDataInputTypeID',$arr) && !is_null($arr['PSmDataInputTypeID']) && $arr['PSmDataInputTypeID']!='')?$arr['PSmDataInputTypeID']:Null;
        $PlannedValue=$arr['SMS'];
        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }

      if(isset($arr['Mailers']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=13;
		$DataInputType_ID=(array_key_exists('PMDataInputTypeID',$arr) && !is_null($arr['PMDataInputTypeID']) && $arr['PMDataInputTypeID']!='')?$arr['PMDataInputTypeID']:Null;
        $PlannedValue=$arr['Mailers'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }
      if(isset($arr['Rate']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=14;
		$DataInputType_ID=(array_key_exists('PNRADataInputTypeID',$arr) && !is_null($arr['PNRADataInputTypeID']) && $arr['PNRADataInputTypeID']!='')?$arr['PNRADataInputTypeID']:Null;
        $PlannedValue=$arr['Rate'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }
      if(isset($arr['Cost']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=15;
		$DataInputType_ID=(array_key_exists('PNCoDataInputTypeID',$arr) && !is_null($arr['PNCoDataInputTypeID']) && $arr['PNCoDataInputTypeID']!='')?$arr['PNCoDataInputTypeID']:Null;
        $PlannedValue=$arr['Cost'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }
      if(isset($arr['Opens']))
      {
        $LineItem_ID=$arr['LineItem_ID'];
        $Metrics_ID=16;
		   $DataInputType_ID=(array_key_exists('PODataInputTypeID',$arr) && !is_null($arr['PODataInputTypeID']) && $arr['PODataInputTypeID']!='')?$arr['PODataInputTypeID']:Null;
        $PlannedValue=$arr['Opens'];

        $addLineItemMetrics = $this->editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID);
      }
       //edit Full line item metrices
    }

    return Redirect::back()->with('error_msg','LineItem Parameter edited Successfully.');
  }

  public function editLineItemMetrics($LineItem_ID,$Metrics_ID, $DataInputType_ID, $PlannedValue,$Campaign_ID)
  {
    $current_time = Carbon::now()->toDateTimeString();
   //get LineItem Id and matrix pair from DB
    $getLT_MetPair_DB = DB::table('LineItemMetrics')
                     ->where('LineItem_ID','=',$LineItem_ID)
                     ->get();
    $getLT_MetPair_DB=json_decode($getLT_MetPair_DB);

    //array for metrixfromDB
    $metrixArrayFromDB=[];
    //$metrixArrayFromPush=[];
    foreach ($getLT_MetPair_DB as $key ) {
      array_push($metrixArrayFromDB,$key->Metrics_ID);
    }
    if (in_array($Metrics_ID, $metrixArrayFromDB))
    {	
	    if($DataInputType_ID!=Null && $PlannedValue != "")
	    {  
	      	// update for given values
	      	$upadetLineItemMetrics=DB::table('LineItemMetrics')
		        ->where('LineItemMetrics.LineItem_ID','=', $LineItem_ID)
		        ->where('LineItemMetrics.Metrics_ID','=', $Metrics_ID)
		        ->update([

		          'DataInputType_ID' =>$DataInputType_ID,
		          'PlannedValue' =>$PlannedValue,
		          'CreatedOn' =>  $current_time,
		          'ModifiedOn' =>  $current_time,
		          'ModifiedBy' => 2,
		          'isActive' => True,
		        ]);
		  }
		  else{

			  $upadetLineItemMetrics=DB::table('LineItemMetrics')
		        ->where('LineItemMetrics.LineItem_ID','=', $LineItem_ID)
		        ->where('LineItemMetrics.Metrics_ID','=', $Metrics_ID)
		        ->update([
		          
		          'CreatedOn' =>  $current_time,
		          'ModifiedOn' =>  $current_time,
		          'ModifiedBy' => 2,
		          'isActive' => False,
		        ]);

		    $getLineItemMetricsId= DB::table('LineItemMetrics')
		    		->where('LineItemMetrics.LineItem_ID','=', $LineItem_ID)
		        	->where('LineItemMetrics.Metrics_ID','=', $Metrics_ID)
		        	->select('LineItemMetrics.id')
		        	->first();      

		    $upadetLineItemMetrics=DB::table('LineItemDaily')
		        ->where('LineItemDaily.LineItemMetrics_ID','=',  $getLineItemMetricsId->id)
		       
		        ->update([

		          'CreatedOn' =>  $current_time,
		          'ModifiedOn' =>  $current_time,
		          'ModifiedBy' => 2,
              'isLineNumberProcessed'=>False,
		          'isActive' => False,
		        ]);	
		  }        
    } 
    else
    {
      $Metrics_ID1=$Metrics_ID;

      //this not metrics insert entry into lineItemMetrics

      if(!is_null($LineItem_ID) && !is_null($Metrics_ID1) && !is_null($DataInputType_ID) && 
        !is_null($PlannedValue)&& !is_null($Campaign_ID))
      {
        $UpdateLineMetric_LID=DB::select("exec SP_IADC_UpdateLineMetric_LID ?,?,?,?,?", array( $LineItem_ID, $Metrics_ID1, $DataInputType_ID,$PlannedValue,$Campaign_ID ));
      }
    }
  }

  public function getCityLocation(Request $request)
  {

    $arr=$request->all();
    $getIncLocationArray=[];
    $getExcLocationArray=[];

    $getIncLocation=DB::table('LineItemLocation')
                  ->join('Locations', 'LineItemLocation.Location_ID', '=', 'Locations.id' )
                  ->where('LineItemLocation.LineItem_ID' ,'=', $arr['LineItem_ID'])
                  ->where('LineItemLocation.toExclude' ,'=', Null)
                  ->where('LineItemLocation.IsActive' ,'=', 1)
                  //->select('LineItemLocation.Location_ID')
                  ->select('Locations.id','Locations.LocationName')
                  ->get();
    $getExcLocation=DB::table('LineItemLocation')
                  ->join('Locations',  'LineItemLocation.Location_ID', '=','Locations.id' )
                  ->where('LineItemLocation.LineItem_ID' ,'=', $arr['LineItem_ID'])
                  ->where('LineItemLocation.toExclude' ,'=',1)
                ->where('LineItemLocation.IsActive' ,'=', 1)
                  //->select('LineItemLocation.Location_ID')
                  ->select('Locations.id','Locations.LocationName')
                  ->get();

     return response()->json(['status' => 'success','getIncLocation'=>$getIncLocation, 'getExcLocation'=>$getExcLocation], 200);

  }

  public function getCampaignExcel()
  {
      $publisher = Publisher::get()->toArray(); //return count($publisher); == 8
      $gener = DB::table('Genre')->orderby('id','desc')->get()->toArray();
      $phase=DB::table('Phase')->orderby('id','asc')->get()->toArray();
      $medium=DB::table('Medium')->orderby('id','asc')->get();
      $objective=DB::table('Objective')->orderby('id','asc')->get();
      $objectiveValues=DB::table('ObjectiveValues')->orderby('id','asc')->get();
      $dealType=DB::table('DealType')->orderby('id','asc')->get();
      $deliveryMode=DB::table('DeliveryMode')->orderby('id','desc')->get();

      $format=DB::table('Format')->orderby('id','asc')->get();

      $formatType=DB::table('FormatType')->orderby('id','asc')->get();

      $dataInputType=DB::table('DataInputType')->orderby('id','asc')->get();

      //changes
      $Section=DB::table('Section')->orderby('id','asc')->get();

       //dd($Section);

      $AdUnit=DB::table('AdUnit')->orderby('id','asc')->get();
                      // return $dealType;

      $locations  =DB::table('Locations')->orderby('id','asc')->get();
      
      //dd($locations);

      return \Excel::create('Campaign', function($excel) use ($publisher, $gener, $phase, $medium, $objective, $objectiveValues, $dealType, $deliveryMode , $formatType, $format, $dataInputType, $Section, $AdUnit, $locations) {

      $excel->sheet('Data', function($sheet) use ($gener, $publisher, $phase, $medium, $objective, $objectiveValues, $dealType, $deliveryMode, $formatType, $format, $dataInputType, $Section, $AdUnit, $locations)
      {
        //$objPHPExcel = new PHPExcel();
        $sheet->SetCellValue("A1", "LineItem Name");
        $sheet->SetCellValue("B1", "Publisher");
        $sheet->SetCellValue("C1", "Phase");
        $sheet->SetCellValue("D1", "Medium");
        $sheet->SetCellValue("E1", "Objective");
        $sheet->SetCellValue("F1", "Objective Values");
        $sheet->SetCellValue("G1", "Deal Type");
        $sheet->SetCellValue("H1", "Delivery Mode");
        $sheet->SetCellValue("I1", "Age From");
        $sheet->SetCellValue("J1", "Age To");
        $sheet->SetCellValue("K1", "Gender");
        $sheet->SetCellValue("L1", "Google Address");
        $sheet->SetCellValue("M1", "Google Radius");
        $sheet->SetCellValue("N1", "Include Location");
        $sheet->SetCellValue("O1", "Exclude Location");
        $sheet->SetCellValue("P1", "Target");
        $sheet->SetCellValue("Q1", "Tagetting Value");

        $sheet->SetCellValue("R1", "Format Type");
        $sheet->SetCellValue("S1", "Format");

        $sheet->getComment('L1')->getText()->createTextRun('Please Provide Complete Address from Google Location');
        //$sheet->getComment('N1')->getText()->createTextRun('Please Pickup the Location from "Location List" Excel Sheet and use comma seperation');
        //$sheet->getComment('O1')->getText()->createTextRun('Please Pickup the Location from "Locations List" Sheet and use comma seperation');


        $sheet->SetCellValue("T1", "L*B / Char");

        $sheet->SetCellValue("U1", "Section");
        $sheet->SetCellValue("V1", "Ad Unit");

        $sheet->SetCellValue("W1", "Impression Input");
        $sheet->SetCellValue("X1", "Impression Type");

        $sheet->SetCellValue("Y1", "Visits input");
        $sheet->SetCellValue("Z1", "Visits Type");

        $sheet->SetCellValue("AA1", "Engagement Input");
        $sheet->SetCellValue("AB1", "Engagement Type");

        $sheet->SetCellValue("AC1", "Net Rate Input");
        $sheet->SetCellValue("AD1", "Net Rate Type");


        $sheet->SetCellValue("AE1", "Reach Input");
        $sheet->SetCellValue("AF1", "Reach Type");


        $sheet->SetCellValue("AG1", "Spots Input");
        $sheet->SetCellValue("AH1", "Spots Type");


        $sheet->SetCellValue("AI1", "Mailers Input");
        $sheet->SetCellValue("AJ1", "Mailers Type");


        $sheet->SetCellValue("AK1", "Clicks Input");
        $sheet->SetCellValue("AL1", "Clicks Type");


        $sheet->SetCellValue("AM1", "Views Input");
        $sheet->SetCellValue("AN1", "Views Type");


        $sheet->SetCellValue("AO1", "Leads Input");
        $sheet->SetCellValue("AP1", "Leads Type");


        $sheet->SetCellValue("AQ1", "Net Cost Input");
        $sheet->SetCellValue("AR1", "Net Cost Type");


        $sheet->SetCellValue("AS1", "Installs Input");
        $sheet->SetCellValue("AT1", "Installs Type");


        $sheet->SetCellValue("AU1", "SMS Input");
        $sheet->SetCellValue("AV1", "SMS Type");



        $gn = 2;
        $pc = 2;
        $ph = 2;
        $md = 2;
        $ob = 2;
        $ov = 2;
        $dt = 2;
        $dm = 2;
        $ft = 2;
        $f  = 2;
        $dit = 2;

        $s = 2;
        $au = 2;
        $loc =2;
        

        foreach($locations as $Location){
          $sheet->SetCellValue("CV".$loc, $Location->LocationName);
          $loc++;
        }


        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'locations', $sheet, 'CV2:CV'.$loc
            )
        );
        // ----
        foreach($Section as $sections){
          $sheet->SetCellValue("CT".$s, $sections->SectionName);
          $s++;
        }


        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'size', $sheet, 'CT2:CT'.$s
            )
        );


        foreach($AdUnit as $addu){
          $sheet->SetCellValue("CU".$au, $addu->AdUnitName);
          $au++;
        }

        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'size', $sheet, 'CU2:CU'.$au
            )
        );




        foreach($gener as $gen){
          $sheet->SetCellValue("CB".$gn, $gen->GenreName);
          $gn++;
        }

        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'size', $sheet, 'CB2:CB'.$gn
            )
        );



        foreach($publisher as $pub){
          $sheet->SetCellValue("CA".$pc, $pub['PublisherName']);
          $pc++;
        }

        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'Gener', $sheet, 'CA2:CA'.$pc
            )
        );


        foreach($phase as $pha){
          $sheet->SetCellValue("CC".$ph, $pha->PhaseName);
          $ph++;
        }


        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'Gener', $sheet, 'CC2:CC'.$ph
            )
        );


        foreach($medium as $med){
          $sheet->SetCellValue("CD".$md, $med->MediumName);
          $md++;
        }


        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'Medium', $sheet, 'CD2:CD'.$ph
            )
        );

        foreach($objective as $obj){
          $sheet->SetCellValue("CE".$ob, $obj->ObjectiveName);
          $ob++;
        }

        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'Object', $sheet, 'CE2:CE'.$ob
            )
        );


        foreach($objectiveValues as $obv){
          $sheet->SetCellValue("CF".$ov, $obv->ObjectiveValueName);
          $ov++;
        }

        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'objectvalues', $sheet, 'CF2:CF'.$ov
            )
        );


        foreach($dealType as $dlt){
          $sheet->SetCellValue("CG".$dt, $dlt->DealTypeName);
          $dt++;
        }

        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'objectvalues', $sheet, 'CG2:CG'.$dt
            )
        );

        // foreach($dealType as $dlt){
        //   $sheet->SetCellValue("CG".$dt, $dlt->DealTypeName);
        //   $dt++;
        // }

        // $sheet->_parent->addNamedRange(
        //     new \PHPExcel_NamedRange(
        //     'objectvalues', $sheet, 'CG2:CG'.$dt
        //     )
        // );


        foreach($deliveryMode as $dlm){
          $sheet->SetCellValue("CH".$dm, $dlm->DeliveryModeName);
          $dm++;
        }

        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'objectvalues', $sheet, 'CH2:CH'.$dm
            )
        );



        $sheet->SetCellValue("CJ2", "Male");
        $sheet->SetCellValue("CJ3", "FeMale");
        $sheet->SetCellValue("CJ4", "All");


        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'objectvalues', $sheet, 'CJ2:CJ4'
            )
        );


        foreach($formatType as $fmt){
          $sheet->SetCellValue("CQ".$ft, $fmt->FormatTypeName);
          $ft++;
        }

        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'formattype', $sheet, 'CQ2:CQ'.$ft
            )
        );


        foreach($format as $ftp){
          $sheet->SetCellValue("CR".$f, $ftp->FormatName);
          $f++;
        }

        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'format', $sheet, 'CR2:CR'.$f
            )
        );

        foreach($dataInputType as $datatype){
          $sheet->SetCellValue("CS".$dit, $datatype->DataInputTypeName);
          $dit++;
        }

        $sheet->_parent->addNamedRange(
            new \PHPExcel_NamedRange(
            'format', $sheet, 'CS2:CS'.$dit
            )
        );

        $cont = 2;
        while(True)
        {
          $objValidationpc = $sheet->getCell('B'.$cont)->getDataValidation();
          $objValidationpc->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationpc->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationpc->setAllowBlank(false);
          $objValidationpc->setShowInputMessage(true);
          $objValidationpc->setShowErrorMessage(true);
          $objValidationpc->setShowDropDown(true);
          $objValidationpc->setErrorTitle('Input error');
          $objValidationpc->setError('Value is not in list.');
          $objValidationpc->setPromptTitle('Pick from list');
          $objValidationpc->setPrompt('Please pick a value from the drop-down list.');
          $objValidationpc->setFormula1('Data!$CA$2:$CA$'.$pc);


          $objValidationph = $sheet->getCell('C'.$cont)->getDataValidation();
          $objValidationph->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationph->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationph->setAllowBlank(false);
          $objValidationph->setShowInputMessage(true);
          $objValidationph->setShowErrorMessage(true);
          $objValidationph->setShowDropDown(true);
          $objValidationph->setErrorTitle('Input error');
          $objValidationph->setError('Value is not in list.');
          $objValidationph->setPromptTitle('Pick from list');
          $objValidationph->setPrompt('Please pick a value from the drop-down list.');
          $objValidationph->setFormula1('Data!$CC$2:$CC$'.$ph);


          $objValidationmd = $sheet->getCell('D'.$cont)->getDataValidation();
          $objValidationmd->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationmd->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationmd->setAllowBlank(false);
          $objValidationmd->setShowInputMessage(true);
          $objValidationmd->setShowErrorMessage(true);
          $objValidationmd->setShowDropDown(true);
          $objValidationmd->setErrorTitle('Input error');
          $objValidationmd->setError('Value is not in list.');
          $objValidationmd->setPromptTitle('Pick from list');
          $objValidationmd->setPrompt('Please pick a value from the drop-down list.');
          $objValidationmd->setFormula1('Data!$CD$2:$CD$'.$md);


          $objValidationob = $sheet->getCell('E'.$cont)->getDataValidation();
          $objValidationob->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationob->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationob->setAllowBlank(false);
          $objValidationob->setShowInputMessage(true);
          $objValidationob->setShowErrorMessage(true);
          $objValidationob->setShowDropDown(true);
          $objValidationob->setErrorTitle('Input error');
          $objValidationob->setError('Value is not in list.');
          $objValidationob->setPromptTitle('Pick from list');
          $objValidationob->setPrompt('Please pick a value from the drop-down list.');
          $objValidationob->setFormula1('Data!$CE$2:$CE$'.$ob);

          $objValidationobv = $sheet->getCell('F'.$cont)->getDataValidation();
          $objValidationobv->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationobv->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationobv->setAllowBlank(false);
          $objValidationobv->setShowInputMessage(true);
          $objValidationobv->setShowErrorMessage(true);
          $objValidationobv->setShowDropDown(true);
          $objValidationobv->setErrorTitle('Input error');
          $objValidationobv->setError('Value is not in list.');
          $objValidationobv->setPromptTitle('Pick from list');
          $objValidationobv->setPrompt('Please pick a value from the drop-down list.');
          $objValidationobv->setFormula1('Data!$CF$2:$CF$'.$ov);

          $objValidationodt = $sheet->getCell('G'.$cont)->getDataValidation();
          $objValidationodt->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationodt->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationodt->setAllowBlank(false);
          $objValidationodt->setShowInputMessage(true);
          $objValidationodt->setShowErrorMessage(true);
          $objValidationodt->setShowDropDown(true);
          $objValidationodt->setErrorTitle('Input error');
          $objValidationodt->setError('Value is not in list.');
          $objValidationodt->setPromptTitle('Pick from list');
          $objValidationodt->setPrompt('Please pick a value from the drop-down list.');
          $objValidationodt->setFormula1('Data!$CG$2:$CG$'.$dt);


          $objValidationodm = $sheet->getCell('H'.$cont)->getDataValidation();
          $objValidationodm->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationodm->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationodm->setAllowBlank(false);
          $objValidationodm->setShowInputMessage(true);
          $objValidationodm->setShowErrorMessage(true);
          $objValidationodm->setShowDropDown(true);
          $objValidationodm->setErrorTitle('Input error');
          $objValidationodm->setError('Value is not in list.');
          $objValidationodm->setPromptTitle('Pick from list');
          $objValidationodm->setPrompt('Please pick a value from the drop-down list.');
          $objValidationodm->setFormula1('Data!$CH$2:$CH$'.$dm);



          $objValidationodI = $sheet->getCell('I'.$cont)->getDataValidation();
          $objValidationodI->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodI->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodI->setAllowBlank(true);
          $objValidationodI->setShowInputMessage(true);
          $objValidationodI->setShowErrorMessage(true);
          $objValidationodI->setErrorTitle('Input error');
          $objValidationodI->setError('Only numbers between 1 and 100 are allowed!');
          $objValidationodI->setPromptTitle('Allowed input');
          $objValidationodI->setPrompt('Only numbers between 1 and 100 are allowed.');
          $objValidationodI->setFormula1(1);
          $objValidationodI->setFormula2(100);


          $objValidationodJ = $sheet->getCell('J'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(true);
          $objValidationodJ->setErrorTitle('Input error');
          $objValidationodJ->setError('Only numbers between 1 and 100 are allowed!');
          $objValidationodJ->setPromptTitle('Allowed input');
          $objValidationodJ->setPrompt('Only numbers between 1 and 100 are allowed.');
          $objValidationodJ->setFormula1(1);
          $objValidationodJ->setFormula2(100);

          $objValidationodJ = $sheet->getCell('M'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(true);
          $objValidationodJ->setErrorTitle('Input error');
          $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          $objValidationodJ->setPromptTitle('Allowed input');
          $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          $objValidationodJ->setFormula1(1);
          $objValidationodJ->setFormula2(999999);

          // ---------------------------------------------------------------------------
          $objValidationpc = $sheet->getCell('N'.$cont)->getDataValidation();
          $objValidationpc->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationpc->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationpc->setAllowBlank(false);
          $objValidationpc->setShowInputMessage(true);
          $objValidationpc->setShowErrorMessage(true);
          $objValidationpc->setShowDropDown(true);
          $objValidationpc->setErrorTitle('Input error');
          $objValidationpc->setError('Value is not in list.');
          $objValidationpc->setPromptTitle('Pick from list');
          $objValidationpc->setPrompt('Please pick a value from the drop-down list.');
          $objValidationpc->setFormula1('Data!$CV$2:$CV$'.$loc);


          $objValidationpc = $sheet->getCell('O'.$cont)->getDataValidation();
          $objValidationpc->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationpc->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationpc->setAllowBlank(false);
          $objValidationpc->setShowInputMessage(true);
          $objValidationpc->setShowErrorMessage(true);
          $objValidationpc->setShowDropDown(true);
          $objValidationpc->setErrorTitle('Input error');
          $objValidationpc->setError('Value is not in list.');
          $objValidationpc->setPromptTitle('Pick from list');
          $objValidationpc->setPrompt('Please pick a value from the drop-down list.');
          $objValidationpc->setFormula1('Data!$CV$2:$CV$'.$loc);
          // ---------------------------------------------------------------------------
          $objValidationodJ = $sheet->getCell('T'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(true);
          $objValidationodJ->setErrorTitle('Input error');
          $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          $objValidationodJ->setPromptTitle('Allowed input');
          $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          $objValidationodJ->setFormula1(1);
          $objValidationodJ->setFormula2(999999);


          $objValidationodJ = $sheet->getCell('W'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);

          $objValidationodJ = $sheet->getCell('Y'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);


          $objValidationodJ = $sheet->getCell('AA'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);

          $objValidationodJ = $sheet->getCell('AC'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);

          $objValidationodJ = $sheet->getCell('AE'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);

          $objValidationodJ = $sheet->getCell('AG'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);


          $objValidationodJ = $sheet->getCell('AI'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);


          $objValidationodJ = $sheet->getCell('AK'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);

          $objValidationodJ = $sheet->getCell('AM'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);


          $objValidationodJ = $sheet->getCell('AO'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);

          $objValidationodJ = $sheet->getCell('AQ'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);


          $objValidationodJ = $sheet->getCell('AS'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);

          $objValidationodJ = $sheet->getCell('AU'.$cont)->getDataValidation();
          $objValidationodJ->setType(\PHPExcel_Cell_DataValidation::TYPE_WHOLE);
          $objValidationodJ->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
          $objValidationodJ->setAllowBlank(true);
          $objValidationodJ->setShowInputMessage(true);
          $objValidationodJ->setShowErrorMessage(false);
          // $objValidationodJ->setErrorTitle('Input error');
          // $objValidationodJ->setError('Only numbers between 1 and 999999 are allowed!');
          // $objValidationodJ->setPromptTitle('Allowed input');
          // $objValidationodJ->setPrompt('Only numbers between 1 and 999999 are allowed.');
          // $objValidationodJ->setFormula1(1);
          // $objValidationodJ->setFormula2(999999);




          $objValidationg = $sheet->getCell('K'.$cont)->getDataValidation();
          $objValidationg->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationg->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationg->setAllowBlank(false);
          $objValidationg->setShowInputMessage(true);
          $objValidationg->setShowErrorMessage(true);
          $objValidationg->setShowDropDown(true);
          $objValidationg->setErrorTitle('Input error');
          $objValidationg->setError('Value is not in list.');
          $objValidationg->setPromptTitle('Pick from list');
          $objValidationg->setPrompt('Please pick a value from the drop-down list.');
          $objValidationg->setFormula1('Data!$CJ$2:$CJ$5');


          $objValidationft = $sheet->getCell('R'.$cont)->getDataValidation();
          $objValidationft->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationft->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationft->setAllowBlank(false);
          $objValidationft->setShowInputMessage(true);
          $objValidationft->setShowErrorMessage(true);
          $objValidationft->setShowDropDown(true);
          $objValidationft->setErrorTitle('Input error');
          $objValidationft->setError('Value is not in list.');
          $objValidationft->setPromptTitle('Pick from list');
          $objValidationft->setPrompt('Please pick a value from the drop-down list.');
          $objValidationft->setFormula1('Data!$CQ$2:$CQ$'.$ft);


          $objValidationf = $sheet->getCell('S'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CR$2:$CR$'.$f);

          $objValidationf = $sheet->getCell('U'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CT$2:$CT$'.$s);

          $objValidationf = $sheet->getCell('V'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CU$2:$CU$'.$au);


          // $objValidationf = $sheet->getCell('V'.$cont)->getDataValidation();
          // $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          // $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          // $objValidationf->setAllowBlank(false);
          // $objValidationf->setShowInputMessage(true);
          // $objValidationf->setShowErrorMessage(true);
          // $objValidationf->setShowDropDown(true);
          // $objValidationf->setErrorTitle('Input error');
          // $objValidationf->setError('Value is not in list.');
          // $objValidationf->setPromptTitle('Pick from list');
          // $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          // $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);

          $objValidationf = $sheet->getCell('X'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);


          $objValidationf = $sheet->getCell('Z'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);


          $objValidationf = $sheet->getCell('AB'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);

          $objValidationf = $sheet->getCell('AD'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);

          $objValidationf = $sheet->getCell('AF'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);


          $objValidationf = $sheet->getCell('AH'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);

          $objValidationf = $sheet->getCell('AJ'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);

          $objValidationf = $sheet->getCell('AL'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);

          $objValidationf = $sheet->getCell('AN'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);

          $objValidationf = $sheet->getCell('AP'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);


          $objValidationf = $sheet->getCell('AR'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);

          $objValidationf = $sheet->getCell('AT'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);

          $objValidationf = $sheet->getCell('AV'.$cont)->getDataValidation();
          $objValidationf->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
          $objValidationf->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
          $objValidationf->setAllowBlank(false);
          $objValidationf->setShowInputMessage(true);
          $objValidationf->setShowErrorMessage(true);
          $objValidationf->setShowDropDown(true);
          $objValidationf->setErrorTitle('Input error');
          $objValidationf->setError('Value is not in list.');
          $objValidationf->setPromptTitle('Pick from list');
          $objValidationf->setPrompt('Please pick a value from the drop-down list.');
          $objValidationf->setFormula1('Data!$CS$2:$CS$'.$dit);

          if($cont >= 100){
            return False;
          }

          $cont++;
        }

      });


      // $excel->sheet('Locations', function($sheet) use ($locations){
      //     $sheet->SetCellValue("A1", "Location Name");
      //     $l = 2;
      //     foreach($locations as $loc){
      //       $sheet->SetCellValue("A".$l, $loc->LocationName);
      //       $l++;
      //     }
      //  });

    })->download('xlsx');
  }


  public function ExcelSheetUpload(Request $request, $id, $cid) {

    $this->validate($request, [
        'import_file' => 'required|mimes:xlsx,xls',
    ]);

    //get compaign details
    $camp = Campaign::find($id);
    if($request->hasFile('import_file'))
    {
            $FileName = $request->file('import_file')->getClientOriginalName();

            $path = $request->file('import_file')->getRealPath();
            $data = \Excel::load($path)->get();
            if($data->count()) {
              DB::table('RawExcelData')->truncate();
              foreach($data as $da) {
                if($da['lineitem_name'] == null && empty($da['publisher'])) {
                  //No action required
                } else {
                  if($da['lineitem_name'] == null) {
                      $da['lineitem_name'] = 'blank';
                  }

                  if(empty($da['publisher'])) {
                      $da['publisher'] = 'NoPublisher';
                  }

                  $dataset = new RawExcelData;
                  $dataset->Campign_ID= $camp['id'];
                  $dataset->FileName= $FileName;
                  $dataset->LineItemName = $da['lineitem_name'];
                  $dataset->Publisher = $da['publisher'];
                  $dataset->Phase = $da['phase'];
                  $dataset->Medium = $da['medium'];
                  $dataset->Objective = $da['objective'];
                  $dataset->ObjectiveValues = $da['objective_values'];
                  $dataset->DealType = $da['deal_type'];
                  $dataset->DelieveryMode = $da['delivery_mode'];
                  $dataset->AgeFrom = $da['age_from'];
                  $dataset->AgeTo = $da['age_to'];
                  $dataset->Gender = $da['gender'];
                  $dataset->GoogleAddress = $da['google_address'];
                  $dataset->GoogleRadius = $da['google_radius'];

                  $dataset->IncludeLocation = $da['include_location'];
                  $dataset->ExcludeLocation = $da['exclude_location'];
                  $dataset->Other1 = $da['target'];
                  $dataset->Other2 = $da['tagetting_value'];
                  $dataset->FormatType = $da['format_type'];
                  $dataset->Format = $da['format'];
                  $dataset->LBChar = $da['lb_char'];

                  $dataset->ImpressionInput = $da['impression_input'];
                  $dataset->ImpressionType = $da['impression_type'];
                  $dataset->VisitsInput = $da['visits_input'];
                  $dataset->VisitsType = $da['visits_type'];
                  $dataset->EngagementInput = $da['engagement_input'];
                  $dataset->EngagementType = $da['engagement_type'];
                  $dataset->NetRateInput = $da['net_rate_input'];
                  $dataset->NetRateType = $da['net_rate_type'];
                  $dataset->ReachInput = $da['reach_input'];
                  $dataset->ReachType = $da['reach_type'];
                  $dataset->SpotsInput = $da['spots_input'];
                  $dataset->SpotsType = $da['spots_type'];

                  $dataset->MailersInput = $da['mailers_input'];
                  $dataset->MailersType = $da['mailers_type'];
                  $dataset->ClickInput = $da['clicks_input'];
                  $dataset->ClicksType = $da['clicks_type'];
                  $dataset->ViewInput = $da['views_input'];
                  $dataset->ViewsType = $da['views_type'];
                  $dataset->LeadsInput = $da['leads_input'];
                  $dataset->LeadsType = $da['leads_type'];
                  $dataset->NetCostInput = $da['net_cost_input'];
                  $dataset->NetCostType = $da['net_cost_type'];


                  $dataset->InstallsInput = $da['installs_input'];
                  $dataset->InstallsType = $da['installs_type'];
                  $dataset->SmsInput = $da['sms_input'];
                  $dataset->SmsType = $da['sms_type'];
                  $dataset->Sections = $da['section'];
                  $dataset->AddUnit = $da['ad_unit'];

                  $dataset->IsPerocessed = Null;
                  $dataset->save();
                }// if($da['lineitem_name'] == null && empty($da['publisher'])) END

              }// foreach($data as $da) end

              // excel uplod complete start reading from DB
              $exceldata= RawExcelData::where('IsPerocessed', Null)->where('Campign_ID', $camp['id'])->get();
              
              foreach($exceldata as $data) {
                $errorArray=[];

                if(Session::has('includeLocation')) {
                    Session::forget('includeLocation');
                }

                if(Session::has('excludeLocation')) {
                    Session::forget('excludeLocation');
                }

                //$includeLocation = $data->ExcludeLocation;
                $includeLocation = $data->IncludeLocation;
                if(!empty($includeLocation) && $includeLocation != null) {
                  $includeLocationArray=explode(", ",$includeLocation);
                  $locationIds=[];
                  foreach($includeLocationArray as $include) {
                    $locationId = DB::table('Locations')->where('LocationName' , $include)->first();
                    if(!empty($locationId )) {
                        $locationIds[] = $locationId->id;
                    }
                  }
                  //Session::put('includeLocation', explode(" ",$includeLocation));
                  Session::put('includeLocation', $locationIds);
                }

                //$excludeLocation = $data->IncludeLocation;
                $excludeLocation = $data->ExcludeLocation;
                if(!empty($excludeLocation) && $excludeLocation != null) {
                  $excludeLocationArray=explode(", ",$excludeLocation);
                  $exlocationIds=[];
                  foreach($excludeLocationArray as $exclude) {
                    $exlocationId = DB::table('Locations')->where('LocationName' , $exclude)->first();
                    if(!empty($exlocationId)) {
                        $exlocationIds[] = $exlocationId->id;
                    }
                  }
                  // Session::put('excludeLocation',  explode(" ",$excludeLocation));
                  Session::put('excludeLocation',  $exlocationIds);
                }
                // Session::push('excludeLocation', $excludeLocation);

                //creating geo session in array
                $lat = Null;
                $long = Null;
                $address = $data->GoogleAddress;
                
                if(!empty($address)) {
                  $url = "http://maps.google.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false&region=India";
                  $ch = curl_init();
                  curl_setopt($ch, CURLOPT_URL, $url);
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                  curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                  $response = curl_exec($ch);
                  curl_close($ch);
                  $response_a = json_decode($response);
                  //"status": "OVER_QUERY_LIMIT"
                  if(!empty($response_a)) {
                    if($response_a->status == "OVER_QUERY_LIMIT") {
                      $ch = curl_init();
                      curl_setopt($ch, CURLOPT_URL, $url);
                      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                      curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                      $response = curl_exec($ch);
                      curl_close($ch);
                      $response_a = json_decode($response);

                      if($response_a->status == "OVER_QUERY_LIMIT" ) {
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        $response = curl_exec($ch);
                        curl_close($ch);
                        $response_a = json_decode($response);
                      }
                    }
                    if($response_a->status == "OVER_QUERY_LIMIT") {
                       $lat = Null;
                       $long = Null;
                    } else {
                      $lat = Null;//$response_a->results[0]->geometry->location->lat;
                      $long = Null; // $response_a->results[0]->geometry->location->lng;
                    }
                  }
                }// end google address

                if(Session::has('geoLocation')) {
                  Session::forget('geoLocation');
                }
                //getting section id
                $sectionId= Null;
                $Section =DB::table('Section')->where('SectionName', $data->Sections)->first();
                if(!empty($Section)) {
                  $sectionId= $Section->id;
                }
                //return $sectionId;

                //getting addunit id
                $addunitId = Null;
                $AdUnit=DB::table('AdUnit')->where('AdUnitName', $data->AddUnit)->first();
                if(!empty($AdUnit)) {
                  $addunitId = $AdUnit->id;
                }

                $geolocation = array("addresspicker_map" => $data->GoogleAddress,
                    "distance" => $data->GoogleRadius,
                    "locality" => "",
                    "sublocality" => "",
                    "administrative_area_level_3" => "",
                    "administrative_area_level_2" => "",
                    "administrative_area_level_1" => "",
                    "country" => "",
                    "postal_code" => "",
                    "lat" => $lat,
                    "lng" => $long,
                    "zoom" => "",
                    "type" => "locality");

                Session::put('geoLocation', $geolocation);

                //getting data using publisher
                $publisher_data= Publisher::where('PublisherName', $data->Publisher)->first();

                $phaseId = Null;
                $phase=DB::table('Phase')->where('PhaseName',$data->Phase)->first();
                if(!empty($phase)) {
                  $phaseId = $phase->id;
                }

                $mediumId = Null;
                $medium=DB::table('Medium')->where('MediumName', $data->Medium)->first();
                if(!empty($medium)) {
                  $mediumId = $medium->id;
                }

                // ----------------------------------------------
                $objective=DB::table('Objective')->where('ObjectiveName',$data->Objective)->first();
                $objectiveValues=DB::table('ObjectiveValues')->where('ObjectiveValueName',$data->ObjectiveValues)->first();

                $objectiveId= Null;
                $objectiveValuesId = Null;
                if(!empty($objective)  && !empty($objectiveValues)) {
                  if($objectiveValues->Objective_ID == $objective->id){
                    $objectiveId= $objective->id;
                    $objectiveValuesId = $objectiveValues->id;
                  } else {
                    array_push($errorArray, 'Objective-ObjectiveValue Mismatch');
                  }
                } 
                // else {
                //   array_push($errorArray, 'Objective-ObjectiveValue');
                // }

                $gender_id = Null;
                if(!empty($data->Gender)) {
                  $gval = $data->Gender;
                  if($gval == "Male"){
                    $gender_id = 1;
                  }elseif($gval == "Female"){
                    $gender_id = 2;
                  }elseif($gval == "All"){
                    $gender_id = 3;
                  }
                }

                // if(!empty($lat) && !empty($long) && !empty($data->GoogleAddress)){
                //   $loctarget = 2;
                // }
                // else
                // {
                //   $loctarget ="";
                // }

                $targetarray = [];
                if(!empty($data->Other2)) {
                  // var_dump($data->Other2);die;
                  $o2arr=explode(",",$data->Other2);
                  $oi2 = 0;
                  foreach($o2arr as $o2) {
                    $tar = TargettingValue::where('TargetingValueName', $o2)->first();
                    if(!empty($tar)) {
                      array_push($targetarray, $tar['id']);
                      $oi2 = $oi2 +1 ;
                    } else {//if wrongly spell, No Record inDB
                      array_push($errorArray, 'Targeting-TargetingValue');
                    }
                  }
                  $targettingvalueid= $targetarray;
                  unset($targetarray);
                } else {
                  $targettingvalueid= array();
                }

                $dealTypeId = Null;
                $dealType=DB::table('DealType')->where('DealTypeName', $data->DealType)->first();
                if(!empty($dealType)) {
                  $dealTypeId =  $dealType->id;
                }

                $deliveryModeId = Null;
                $deliveryMode=DB::table('DeliveryMode')->where('DeliveryModeName',$data->DelieveryMode)->first();
                if(!empty($deliveryMode)) {
                  $deliveryModeId = $deliveryMode->id;
                }
            
                $fid = Null;
                $FormatType_ID = Null;
                $FormatType=DB::table('FormatType')->where('FormatTypeName',$data->FormatType)->first();
                $Format=DB::table('Format')->where('FormatName',$data->Format)->first();
                               
                if(!empty($Format)) {
                  $fid = $Format->id;
                  if($data->FormatType==='Videos' && ($data->Format==='Carousel' || $data->Format==='Canvas')) {
                    $FormatType_ID = 4;
                    if($data->Format==='Carousel') {
                      $fid = '23';
                    } else if($data->Format==='Canvas') {
                      $fid = '22';
                    }                   
                  } else if($data->FormatType!=='Videos' && $Format->FormatType_ID != $FormatType->id)  {
                    array_push($errorArray, 'Format-FormatType Mismatch');
                    $FormatType_ID = $Format->FormatType_ID;
                  }
                }

                $LBChar = Null;
                if(!empty($data->LBChar)){
                  $LBChar = $data->LBChar;
                }

                $AgeFrom = Null;
                if(!empty($data->AgeFrom)){
                  $AgeFrom = $data->AgeFrom;
                }

                $AgeTo = Null;
                if(!empty($data->AgeTo)){
                  $AgeTo = $data->AgeTo;
                }
                //-----------------------------------
                // $dataInputType=DB::table('DataInputType')->get();
                // $Format=DB::table('PublisherType')->where('FormatName',$data->Format)->first(); ///gettting publisher type data
                //checking publisher data
                if(isset($publisher_data->PublisherType_ID) &&  $publisher_data->PublisherType_ID  == 1) {
                  // $dataset = array(1, 4);
                  $dataset = array('1','2','3','4','6','7','8','9','10','11');
                } else if(isset($publisher_data->PublisherType_ID) &&  $publisher_data->PublisherType_ID  != 1)  {
                  // $dataset = array(1,$publisher_data->PublisherType_ID, 4);
                  $dataset = array('1','2','3','4','6','7','8','9','10','11');
                } else {
                  array_push($errorArray, 'Publisher');
                }
                // 1 = API  2 = Manual 3= Scrape  4= XLS

                $ImpressionType = $this->validateDataInputType($data->ImpressionType);
                $ClicksType = $this->validateDataInputType($data->ClicksType);
                $VisitsType = $this->validateDataInputType($data->VisitsType);
                $ViewsType = $this->validateDataInputType($data->ViewsType);
                $EngagementType = $this->validateDataInputType($data->EngagementType);
                $LeadsType = $this->validateDataInputType($data->LeadsType);
                $NetRateType = $this->validateDataInputType($data->NetRateType);
                $NetCostType = $this->validateDataInputType($data->NetCostType);
                $ReachType = $this->validateDataInputType($data->ReachType);
                $InstallsType = $this->validateDataInputType($data->InstallsType);
                $SpotsType = $this->validateDataInputType($data->SpotsType);
                $SmsType = $this->validateDataInputType($data->SmsType);
                $MailersType = $this->validateDataInputType($data->MailersType);


                $Imp = $this->isDataInputTypeValueNumeric($data->ImpressionInput);
                $click = $this->isDataInputTypeValueNumeric($data->ClickInput);
                $visit = $this->isDataInputTypeValueNumeric($data->VisitsInput);
                $view = $this->isDataInputTypeValueNumeric($data->ViewInput);
                $engagement = $this->isDataInputTypeValueNumeric($data->EngagementInput);
                $leads = $this->isDataInputTypeValueNumeric($data->LeadsInput);
                $netrate = $this->isDataInputTypeValueNumeric($data->NetRateInput);
                $netcost = $this->isDataInputTypeValueNumeric($data->NetCostInput);
                $reach = $this->isDataInputTypeValueNumeric($data->ReachInput);
                $install = $this->isDataInputTypeValueNumeric($data->InstallsInput);
                $spots = $this->isDataInputTypeValueNumeric($data->SpotsInput);
                $sms = $this->isDataInputTypeValueNumeric($data->SmsInput);
                $mailer = $this->isDataInputTypeValueNumeric($data->MailersInput);

                if($data->LineItemName== 'blank') {
                  array_push($errorArray, 'LineItemName');
                }
                
                if($data->Publisher == 'NoPublisher') {
                  array_push($errorArray, 'Publisher');
                }

                $this->processDataInput($Imp,$ImpressionType,$errorArray,'Impression-ImpressionType',$data);
                $this->processDataInput($click,$ClicksType,$errorArray,'click-ClicksType',$data);
                $this->processDataInput($visit,$VisitsType,$errorArray,'visit-VisitsInput',$data);
                $this->processDataInput($view,$ViewsType,$errorArray,'view-ViewsType',$data);
                $this->processDataInput($engagement,$EngagementType,$errorArray,'engagement-EngagementType',$data);
                $this->processDataInput($netrate,$NetRateType,$errorArray,'netrate-NetRateType',$data);
                $this->processDataInput($reach,$ReachType,$errorArray,'reach-ReachType',$data);
                $this->processDataInput($spots,$SpotsType,$errorArray,'spots-SpotsType',$data);
                $this->processDataInput($mailer,$MailersType,$errorArray,'mailer-MailersType',$data);
                $this->processDataInput($leads,$LeadsType,$errorArray,'leads-LeadsType',$data);
                $this->processDataInput($netcost,$NetCostType,$errorArray,'netcost-NetCostType',$data);
                $this->processDataInput($install,$InstallsType,$errorArray,'install-InstallsType',$data);
                $this->processDataInput($sms,$SmsType,$errorArray,'sms-SmsType',$data);

                if(empty($errorArray)) {
                  $finalarray= array("LineItemName" => $data->LineItemName,
                          "lineItemType"=>1,
                          "Genre_ID" => $publisher_data->Genre_ID,
                          "Publisher_ID" => $publisher_data->id,
                          "Phase_ID" => $phaseId,
                          "Medium_ID" => $mediumId,
                          "Objective_ID" => $objectiveId,
                          "ObjectiveValue_ID" =>$objectiveValuesId,
                          "DealType_ID" => $dealTypeId,
                          "DeliveryMode_ID" => $deliveryModeId,
                          "target_ageRangeFrom" => $AgeFrom,
                          "target_ageRangeTo" => $AgeTo,
                          "Gender_ID" => $gender_id,
                          "Targeting_ID"=> array(),
                          "TargetingValue_ID" => $targettingvalueid,
                          "FormatType_ID" => $FormatType_ID,
                          "Format_ID" => $fid,
                          "FormatValue" => $LBChar,

                          "Section_ID" => $sectionId,
                          "AdUnit_ID" => $addunitId,

                          "Impressions" => $Imp,
                          "PIDataInputTypeID" => $ImpressionType,

                          "Clicks" => $click ,
                          "PCDataInputTypeID" => $ClicksType,

                          "Visits" => $visit,
                          "PVsDataInputTypeID" => $VisitsType,

                          "Views" => $view ,
                          "PVwDataInputTypeID" => $ViewsType,

                          "Engagement" => $engagement,
                          "PEDataInputTypeID" => $EngagementType,

                          "Leads" => $leads,
                          "PLDataInputTypeID" => $LeadsType,

                          "NetRate" => $netrate,
                          "PNRDataInputTypeID" => $NetRateType,

                          "NetCost" => $netcost,
                          "PNCDataInputTypeID" => $NetCostType,

                          "Reach" => $reach,
                          "PRDataInputTypeID" => $ReachType,

                          "Installs" =>  $install,
                          "PInDataInputTypeID" => $InstallsType,

                          "Spots" => $spots,
                          "PSpDataInputTypeID" => $SpotsType,

                          "SMS" => $sms,
                          "PSmDataInputTypeID" => $SmsType,

                          "Mailers" => $mailer ,
                          "PMDataInputTypeID" => $MailersType,

                          "Campaign_ID" => $id,
                          "CampaignID" => $cid
                          );
                  // $this->pre($finalarray);
                  $test = $this->storeCPByReqAndExcel($finalarray);
                  $data->IsPerocessed = True; 
                  $data->save();
                } else {
                  Session::put('error_msg1','There was error in processing excel, please check the error excel');
                    $j=1;
                    $countError=count($errorArray);
                    $errorString ="Error in Fields: ";
                    foreach ($errorArray as $key => $value) {
                      $errorString.= $value;
                      if($j != $countError){
                         $errorString.= ",";
                      }
                      $j++;
                    }

                    $data->IsPerocessed = False;
                    $data->ErrorType = $errorString;
                    $data->save();
                }
              }// foreach($exceldata as $data) end


              $existdata = RawExcelData::get()->toArray();
              $previous =str_replace(url('/'), '', url()->previous());
              
              // if (Auth::attempt(['email' => Auth::user()->email, 'password' => 'admin@123'])) {
                      return redirect()->intended($previous);
              //   }
              //return Redirect::back();
              
              // return redirect()->route('Campaign');
              // return Redirect::route('Campaign');

              //return Redirect::to('publisher');
              //return redirect()->intended('/LineItem/68');

            }
            else {
              return 'no count';
            }

          }
          else {
           return 'No File';
          }
  }

  private function getInputTypeIDByTypeName($typeName='') {
      $response = NULL;
      if(!empty($typeName)) {
          $table_res = DB::table('DataInputType')->where('DataInputTypeName', $typeName)->pluck('id');
          $decode_res = json_decode($table_res,true);
          if(is_array($decode_res) && array_key_exists(0,$decode_res)) {
                $response = $decode_res[0];
          }
      }
      return $response;
  }

  private function validateDataInputType($typeName='') {
      $response = NULL;
      if(!empty($typeName)) {
        $response = $this->getInputTypeIDByTypeName($typeName);
      }
      return $response;
  }

  private function isDataInputTypeValueNumeric($typeName='') {
    $response = NULL;
    if($typeName!=='' && !is_null($typeName) && is_numeric($typeName)) {
      $response = $typeName;
    }
    return $response;
  }

  private function processDataInput($numericFlag,$typeVal,&$errorArray,$errFlag,$data) {
      $numericFlag = trim($numericFlag);
      if(($numericFlag!=='' && !is_null($numericFlag)) && (!empty($typeVal))) {
      } else if(($numericFlag==='' || is_null($numericFlag)) && (!empty($typeVal))) {
        array_push($errorArray, $errFlag);
      } else if(($numericFlag!=='' && !is_null($numericFlag)) && (empty($typeVal))) {
        array_push($errorArray, $errFlag);
      } else if(($numericFlag==='' || is_null($numericFlag)) && (empty($typeVal))) {
      } 
  }


  public function getErrorExcelFIle($Campaign_ID)
  {

    $existdata = RawExcelData::where('IsPerocessed', False)->where('IsDownloaded', Null)->where('Campign_ID', $Campaign_ID)->get()->toArray();


    if(count($existdata) > 0){

      $restdata = RawExcelData::where('IsPerocessed', False)->where('IsDownloaded', Null)->get();

       foreach($restdata as $rdata){
        //$rdata->delete();
        $rdata['IsDownloaded']=1;
        $rdata->save();
       }

      return Excel::create('ErrorLines', function($excel) use ($existdata) {
        $excel->sheet('mySheet', function($sheet) use ($existdata)
        {
            $sheet->fromArray($existdata);
        });
      })->download('xlsx');
    }
    else{

          return redirect::Back()->with('error_msg','No error File to download');
     }
    //return redirect::back();
  }

  public function getErrorExcelFIleCount()
  {
      $existdata = RawExcelData::get()->where('IsPerocessed', 0)->toArray();
      return count($existdata);
  }

  public function EditLineItemMetrices($lid,$Genre_ID, $Publisher_ID)
  {
    $Lineitem= DB::table('LineItem')
              ->select( 'LineItem.id as lineitemId', 'LineItem.LineItemName', 'CampaignPublisher.Campaign_ID')
              ->join('CampaignPublisher', 'CampaignPublisher.id', '=', 'LineItem.CampaignPublisher_ID' )
              ->join('Publisher', 'CampaignPublisher.Publisher_ID', '=', 'Publisher.id' )
              ->where('Publisher.id',$Publisher_ID)
              ->where('Lineitem.id',$lid)
              ->first();

    $Genre=DB::table('Genre')
              ->where('id',$Genre_ID)
              ->first();

    $Publisher=DB::table('Publisher')
              ->where('id',$Publisher_ID)
              ->first();

    $DIT=DB::table('DataInputType')
              ->orderby('id','asc')
              ->get();

    //savedDit is for selected DIT from DB
    $savedDit=DB::table('LineItemMetrics')
              ->where('LineItem_ID',$lid)
              ->get();

    //$sdit is array of size 13 and value Null
    $sdit= array_fill ( 0 , 13, Null );

    foreach ($savedDit as $key )
    {
      if($key->Metrics_ID==1){
          $sdit[0]=$key;
      }
      if($key->Metrics_ID==2){
          $sdit[1]=$key;
      }
      if($key->Metrics_ID==3){
          $sdit[2]=$key;
      }
      if($key->Metrics_ID==4){
          $sdit[3]=$key;
      }
      if($key->Metrics_ID==5){
          $sdit[4]=$key;
      }
      if($key->Metrics_ID==6){
          $sdit[5]=$key;
      }
      if($key->Metrics_ID==7){
          $sdit[6]=$key;
      }
      if($key->Metrics_ID==8){
          $sdit[7]=$key;
      }
      if($key->Metrics_ID==9){
          $sdit[8]=$key;
      }
      if($key->Metrics_ID==10){
          $sdit[9]=$key;
      }
      if($key->Metrics_ID==11){
          $sdit[10]=$key;
      }
      if($key->Metrics_ID==12){
          $sdit[11]=$key;
      }
      if($key->Metrics_ID==13){
          $sdit[12]=$key;
      }

    }
    return view('CampaignPublisher.editLineItemMetrices', compact(
      'Lineitem','Genre','Publisher','DIT','sdit'
    ))->with('i');
  }
//
public function EditLineItemMetricesCityBank($lid,$Genre_ID, $Publisher_ID)
{
  $Lineitem= DB::table('LineItem')
            ->select( 'LineItem.id as lineitemId', 'LineItem.LineItemName', 'CampaignPublisher.Campaign_ID')
            ->join('CampaignPublisher', 'CampaignPublisher.id', '=', 'LineItem.CampaignPublisher_ID' )
            ->join('Publisher', 'CampaignPublisher.Publisher_ID', '=', 'Publisher.id' )
            ->where('Publisher.id',$Publisher_ID)
            ->where('Lineitem.id',$lid)
            ->first();

  $Genre=DB::table('Genre')
            ->where('id',$Genre_ID)
            ->first();

  $Publisher=DB::table('Publisher')
            ->where('id',$Publisher_ID)
            ->first();

  $DIT=DB::table('DataInputType')
            ->orderby('id','asc')
            ->get();

  //savedDit is for selected DIT from DB
  $savedDit=DB::table('LineItemMetrics')
            ->where('LineItem_ID',$lid)
            ->get();
  $card=DB::table('Card')
                ->orderby('id','asc')
                ->get();
  $card=json_decode($card);

  $creative=DB::table('Creative')
                ->orderby('id','asc')
                ->get();
  $creative=json_decode($creative);
  //$cardDetails=[];

  $cardAndCreative =DB::table('LineItem')
       ->join('creative','creative.id','=','LineItem.Creative_ID')
       ->join('Card','Card.id','=','creative.Card_ID')
       ->select('Card.*','Creative.*')
       ->where('LineItem.id','=',$lid)
       ->get();


  //$sdit is array of size 13 and value Null
  $sdit= array_fill ( 0 , 16, Null );

  foreach ($savedDit as $key )
  {
    if($key->Metrics_ID==1){
        $sdit[0]=$key;
    }
    if($key->Metrics_ID==2){
        $sdit[1]=$key;
    }
    if($key->Metrics_ID==3){
        $sdit[2]=$key;
    }
    if($key->Metrics_ID==4){
        $sdit[3]=$key;
    }
    if($key->Metrics_ID==5){
        $sdit[4]=$key;
    }
    if($key->Metrics_ID==6){
        $sdit[5]=$key;
    }
    if($key->Metrics_ID==7){
        $sdit[6]=$key;
    }
    if($key->Metrics_ID==8){
        $sdit[7]=$key;
    }
    if($key->Metrics_ID==9){
        $sdit[8]=$key;
    }
    if($key->Metrics_ID==10){
        $sdit[9]=$key;
    }
    if($key->Metrics_ID==11){
        $sdit[10]=$key;
    }
    if($key->Metrics_ID==12){
        $sdit[11]=$key;
    }
    if($key->Metrics_ID==13){
        $sdit[12]=$key;
    }
    if($key->Metrics_ID==14){
        $sdit[13]=$key;
    }
    if($key->Metrics_ID==15){
        $sdit[14]=$key;
    }
    if($key->Metrics_ID==16){
        $sdit[15]=$key;
    }

  }
  return view('CampaignPublisher.editLineItemMetricesCityBank', compact(
    'Lineitem','Genre','Publisher','DIT','sdit','card','creative','cardAndCreative'
  ))->with('i');
}
//Enncode

  public function getLocationSheet()
  {

    $locations  =DB::table('Locations')->orderby('id','asc')->get();
    return \Excel::create('Locations', function($excel) use ($locations) {

      $excel->sheet('Locations', function($sheet) use ($locations)
      {
        $sheet->SetCellValue("A1", "Location Name");
        $l = 2;
        foreach($locations as $loc){
          $sheet->SetCellValue("A".$l, $loc->LocationName);
          $l++;
        }
      });
    })->download('xlsx');
  }


  public function getTargetingData(Request $request)
  {

    $arr=$request->all();
    //$getTargetingData=[];
    //$getTargetingValueData=[];

     $targetIDArray=$arr['Targeting_ID'];



        $getTargetingValueData=DB::table('TargetingValue')
                ->join('Targeting', 'Targeting.id','=', 'TargetingValue.Targeting_ID')

                ->whereIn('Targeting.id' , json_decode($arr['Targeting_ID']))

                ->select('Targeting.id','Targeting.TargetingName', 'TargetingValue.id', 'TargetingValue.Targeting_ID', 'TargetingValue.TargetingValueName')
                ->get();

    return response()->json(['status' => 'success','getTargetingData'=>$getTargetingValueData], 200);


  }

  public function selectCreativeView(Request $request)
  {

      $card =DB::table('LineItem')
           ->join('creative','creative.id','=','LineItem.Creative_ID')
           ->join('Card','Card.id','=','creative.Card_ID')
           ->select('Card.CardName','Creative.CreativeName')
           ->where('LineItem.id','=',$request['lineitmeId'])
           ->get();

  $cardCreative=json_decode($card);
  return response()->json(['status' => 'success','card'=>$cardCreative], 200);
  }

  public function autocomplete(Request $request){

    //echo"<pre>"; print_r($request->all()); exit;
   

    $publisherName = $request['query'];     
    if($publisherName!='') {
      $data= Publisher::where("PublisherName","LIKE","%".$publisherName."%")->get();
      $i=0;
      foreach($data as $key) {
        $result[$i]['text']=$key['PublisherName'];
        $result[$i]['value']=$key['id'];
        $i++;
      }
    } else {
        $result[0]['text']="No Result";
        $result[0]['value']="No Result";
    }
    return response()->json($result,200);


  }

  

  public function getCampaignPublisherdetailsInExcel($cb_id)
  {
    //dd(gettype($cb_id));
    ini_set('memory_limit', '-1');
    set_time_limit(0);

    // Excel::create('Export data', function($excel)  use ($cb_id)  {

    //   $excel->sheet('Sheet 1', function($sheet) use ($cb_id) {
    //     //dd($cb_id);
        $products=DB::select(

          "Select ClientName, BrandName, GenreName, PublisherName, StartDate, EndDate, CampaignName, LineItemName,Date, PhaseName, ObjectiveName, AgeRangeFrom, AgeRangeTo, 
            GenderName,

             GoogleLocation, DealTypeName, DeliveryModeName, FormatName, SectionName, AdUnitName, 

             max(PlannedImpressions) PlannedImpressions, max(PlannedClicks) PlannedClicks, max(Cost) NetCost, max(PlannedReach) PlannedReach,

             max(PlannedVisit) PlannedVisit,max(PlannedViews) PlannedViews,max(PlannedEngagement) PlannedEngagement,max(PlannedLeads) PlannedLeads,
             max(PlannedNetRate) PlannedNetRate,max(PlannedSpots) PlannedSpots,max(PlannedMailer) PlannedMailer,max(PlannedInstalls) PlannedInstalls,max(PlannedSMS) PlannedSMS,

              (max(IsNull(Impressions,0)) +  max(Isnull(MetricsImpression,0))) Impressions, (max(IsNull(Clicks,0)) +  max(Isnull(MetricsClicks,0))) Clicks,-- max(CTR) CTR, 

              max(video_p100_watched_actions) video_p100_watched_actions,

              (max(IsNull(reach,0)) +  max(Isnull(MetricsReach,0))) reach,

              max(IsNull(engagement,0)) Engagement,

              max(IsNull(MetricsVisit,0)) Visits,

              max(IsNull(MetricsLeads,0)) Leads, 

              max(IsNull(MetricsNetCost,0)) Spends, 

              max(IsNull(MetricsNetRate,0)) Net_Rate, 
              
              max(IsNull(MetricsSpots,0)) Spots,
               
              max(IsNull(MetricsMailer,0)) Mailer,   

              max(IsNull(MetricsInstalls,0)) Installs,

              max(IsNull(MetricsSMS,0)) SMS, 
                
              max(video_p25_watched_actions) video_p25_watched_actions,

              max(video_p50_watched_actions) video_p50_watched_actions, max(video_p75_watched_actions) video_p75_watched_actions,

              max(spend) spend,
               
              (max(IsNull(video_views,0)) +  max(Isnull(MetricsViews,0))) video_views,
               
              max(complete_views_video) complete_views_video,

              max(ImpressionReach) ImpressionReach
               
            from

            (Select ClientName, BrandName, GenreName, PublisherName, StartDate, EndDate, CampaignName, LineItemName,LID.Date as Date, PhaseName, ObjectiveName, AgeRangeFrom, AgeRangeTo, GenderName,

             GoogleLocation, DealTypeName, DeliveryModeName, FormatName, SectionName, AdUnitName, 

            Case when Metrics_ID = 1 then Max(PlannedValue) end PlannedImpressions,

            Case when Metrics_ID = 2 then Max(PlannedValue) end PlannedClicks,

            Case when Metrics_ID = 8 then Max(ISNULL(PlannedValue,0)) end Cost,

            Case when Metrics_ID = 9 then Max(PlannedValue) end PlannedReach,

            case when Metrics_ID = 3 then Max(ISNULL(PlannedValue,0)) end PlannedVisit,

            case when Metrics_ID = 4 then Sum(ISNULL(PlannedValue,0)) end PlannedViews,

            case when Metrics_ID = 6 then Sum(ISNULL(PlannedValue,0)) end PlannedEngagement,

            case when Metrics_ID = 5 then Sum(ISNULL(PlannedValue,0)) end PlannedLeads,

            case when Metrics_ID = 7 then Sum(ISNULL(PlannedValue,0)) end PlannedNetRate,

            case when Metrics_ID = 11 then Sum(ISNULL(PlannedValue,0)) end PlannedSpots,

            case when Metrics_ID = 13 then Sum(ISNULL(PlannedValue,0)) end PlannedMailer,

            case when Metrics_ID = 10 then Sum(ISNULL(PlannedValue,0)) end PlannedInstalls,

            case when Metrics_ID = 12 then Sum(ISNULL(PlannedValue,0)) end PlannedSMS,

            Case when HiddenMetricsGroupName = 'Impressions' then Sum(ISNULL(Value,0)) end Impressions,

            Case when HiddenMetricsGroupName = 'Clicks' then Sum(ISNULL(Value,0)) end Clicks,

            Case when HiddenMetricsGroupName = 'CTR' then Sum(ISNULL(Value,0)) end CTR,

            Case when HiddenMetricsGroupName = 'video_p100_watched_actions' then Sum(ISNULL(Value,0)) end video_p100_watched_actions,

            Case when HiddenMetricsGroupName = 'video_10_sec_watched_actions' then Sum(ISNULL(Value,0)) end video_10_sec_watched_actions,

            Case when HiddenMetricsGroupName = 'reach' then Sum(ISNULL(Value,0)) end reach,

            Case when HiddenMetricsGroupName = 'Post engagement' then Sum(ISNULL(Value,0)) end engagement,

            Case when HiddenMetricsGroupName = 'video_p25_watched_actions' then Sum(ISNULL(Value,0)) end video_p25_watched_actions,

            Case when HiddenMetricsGroupName = 'video_p50_watched_actions' then Sum(ISNULL(Value,0)) end video_p50_watched_actions,

            Case when HiddenMetricsGroupName = 'video_p75_watched_actions' then Sum(ISNULL(Value,0)) end video_p75_watched_actions,

            Case when HiddenMetricsGroupName = 'spend' then Sum(ISNULL(Value,0)) end spend,

            Case when HiddenMetricsGroupName = 'views' then Sum(ISNULL(Value,0)) end views,

            Case when HiddenMetricsGroupName = 'video_views' then Sum(ISNULL(Value,0)) end video_views,

            Case when HiddenMetricsGroupName = 'total_revenue' then Sum(ISNULL(Value,0)) end total_revenue,

            Case when HiddenMetricsGroupName = 'revenue_adv_currency' then Sum(ISNULL(Value,0)) end revenue_adv_currency,

            Case when HiddenMetricsGroupName = 'complete_views_video' then Sum(ISNULL(Value,0)) end complete_views_video,

            Case when HiddenMetricsGroupName = 'first_quartile_views_video' then Sum(ISNULL(Value,0)) end first_quartile_views_video,

            Case when HiddenMetricsGroupName = 'midpoint_views_video' then Sum(ISNULL(Value,0)) end midpoint_views_video,

            Case when HiddenMetricsGroupName = 'third_quartile_views_video' then Sum(ISNULL(Value,0)) end third_quartile_views_video,

            Case when HiddenMetricsGroupName = 'video_completions' then Sum(ISNULL(Value,0)) end video_completions,

            Case when HiddenMetricsGroupName = 'ImpressionReach' then Sum(ISNULL(Value,0)) end ImpressionReach,

            case when Metrics_ID = 1 then Sum(ISNULL(Value,0)) end MetricsImpression,

            case when Metrics_ID = 2 then Sum(ISNULL(Value,0)) end MetricsClicks,

            case when Metrics_ID = 4 then Sum(ISNULL(Value,0)) end MetricsViews,

            case when Metrics_ID = 9 then Sum(ISNULL(Value,0)) end MetricsReach,

            case when Metrics_ID = 6 then Sum(ISNULL(Value,0)) end MetricsEngagement,
            
            case when Metrics_ID = 3 then Sum(ISNULL(Value,0)) end MetricsVisit,

            case when Metrics_ID = 5 then Sum(ISNULL(Value,0)) end MetricsLeads,

            case when Metrics_ID = 8 then Sum(ISNULL(Value,0)) end MetricsNetCost,

            case when Metrics_ID = 7 then Sum(ISNULL(Value,0)) end MetricsNetRate,

            case when Metrics_ID = 11 then Sum(ISNULL(Value,0)) end MetricsSpots,

            case when Metrics_ID = 13 then Sum(ISNULL(Value,0)) end MetricsMailer,

            case when Metrics_ID = 10 then Sum(ISNULL(Value,0)) end MetricsInstalls,

            case when Metrics_ID = 12 then Sum(ISNULL(Value,0)) end MetricsSMS

                      
            from LineItemDaily LID

            inner join LineItemMetrics LIM on LID.LineItemMetrics_ID = Lim.ID

            left join HiddenMetrics HDM on LIM.HiddenMetrics_ID = HDM.id

            inner join LineItem LI on LIM.LineItem_ID = LI.id

            left join Phase Ph on LI.Phase_ID = ph.id

            left join Objective Obj on LI.ObjectiveValue_ID = Obj.id

            left join Gender Gd on LI.Gender_ID = Gd.id

            left join DealType DT on LI.DealType_ID = Dt.id

            left join DeliveryMode DM on LI.DeliveryMode_ID = DM.id

            left join [Format] F on LI.Format_ID = F.id 

            left join Section Sc on LI.Section_ID = Sc.id

            left join AdUnit Ad on Li.AdUnit_ID = Ad.id

            inner join CampaignPublisher CP on LI.CampaignPublisher_ID = CP.id

            inner join Campaign C on CP.Campaign_ID = C.id

            inner join ClientBrand CB on C.ClientBrand_ID = CB.id

            inner join Client Ct on CB.Client_ID = Ct.id

            inner join Publisher P on CP.Publisher_ID = p.id

            inner join Genre Ge on P.Genre_ID = Ge.id

            left join HiddenMetricsGroupMapping HMGM on HMGM.HiddenMetrics_ID = HDM.id

            left join HiddenMetricsGroup HMG on HMGM.HiddenMetricsGroup_ID = HMG.Id

            where C.id=$cb_id

            group by ClientName, BrandName, GenreName, PublisherName, StartDate, EndDate, CampaignName, LineItemName,LID.Date, PhaseName, ObjectiveName, AgeRangeFrom, AgeRangeTo, GenderName,

             GoogleLocation, DealTypeName, DeliveryModeName, FormatName, SectionName, AdUnitName, HiddenMetricsGroupName, Metrics_ID ) t



            group by ClientName, BrandName, GenreName, PublisherName, StartDate, EndDate, CampaignName, LineItemName,Date, PhaseName, ObjectiveName, AgeRangeFrom, AgeRangeTo, GenderName, 

              GoogleLocation, DealTypeName, DeliveryModeName, FormatName, SectionName, AdUnitName

            order by ClientName, BrandName, GenreName, PublisherName, StartDate, EndDate, CampaignName, LineItemName,Date

            "

        );
         
      if($products){  
        Excel::create('Export data', function($excel)  use ($products)  {

        $excel->sheet('Sheet 1', function($sheet) use ($products) {
             
          foreach($products as $product) {
            $data[] = array(
              $product->ClientName,
              $product->BrandName,
              $product->GenreName,
              $product->PublisherName,
              $product->StartDate,
              $product->EndDate,
              $product->CampaignName,
              $product->LineItemName,
              $product->Date,
              $product->PhaseName,
              $product->ObjectiveName,
              $product->AgeRangeFrom,
              $product->AgeRangeTo,
              $product->GenderName,
              $product->GoogleLocation,
              $product->DealTypeName,
              $product->DeliveryModeName,
              $product->FormatName,
              $product->SectionName,
              $product->AdUnitName,
              $product->PlannedImpressions,
              $product->PlannedClicks,
              $product->NetCost,
              $product->PlannedReach,
              $product->PlannedVisit,
              $product->PlannedViews,
              $product->PlannedEngagement,
              $product->PlannedLeads,
              $product->PlannedNetRate,
              $product->PlannedSpots,
              $product->PlannedMailer,
              $product->PlannedInstalls,
              $product->PlannedSMS,
              $product->Impressions,
              $product->Clicks,
              $product->video_p100_watched_actions,
              $product->reach,
              $product->Engagement,
              $product->Visits,
              $product->Leads,
              $product->Spends,
              $product->Net_Rate,
              $product->Spots,
              $product->Mailer,
              $product->Installs,
              $product->SMS,
              $product->video_p25_watched_actions,
              $product->video_p50_watched_actions,
              $product->video_p75_watched_actions,
              $product->spend,
              $product->video_views,
              $product->complete_views_video,
              $product->ImpressionReach,    
            );
          }
          $sheet->fromArray($data, null, 'A1', false, false);
          $headings = array('ClientName', 'BrandName', 'GenreName', 'PublisherName', 'StartDate', 'EndDate',
                  'CampaignName', 'LineItemName','Date','PhaseName','ObjectiveName','AgeRangeFrom','AgeRangeTo','GenderName','GoogleLocation','DealTypeName','DeliveryModeName','FormatName','SectionName','AdUnitName','PlannedImpressions','PlannedClicks','NetCost','PlannedReach','PlannedVisit','PlannedViews','PlannedEngagement','PlannedLeads','PlannedNetRate','PlannedSpots','PlannedMailer','PlannedInstalls','PlannedSMS','Impressions',
                  'Clicks','video_p100_watched_actions','reach','Engagement','Visits','Leads','Spends','Net_Rate','Spots','Mailer','Installs','SMS','video_p25_watched_actions','video_p50_watched_actions','video_p75_watched_actions','spend','video_views','complete_views_video','ImpressionReach'

                    );

          $sheet->prependRow(1, $headings);

        });
      })->export('xls');
    }
    else{

        //echo"No Data";
        return Redirect::back()->withErrors(['No Records Exist.', 'The Message']);
    }     
  }

}