<?php
namespace App\V12;

// For more information about installing and using the Bing Ads PHP SDK, 
// see https://go.microsoft.com/fwlink/?linkid=838593.
use autoload;
//require_once "../../../../autoload.php";


use SoapVar;
use SoapFault;
use Exception;
use DB;

// Specify the Microsoft\BingAds\V12\Reporting classes that will be used.
use Microsoft\BingAds\V12\Reporting\SubmitGenerateReportRequest;
use Microsoft\BingAds\V12\Reporting\PollGenerateReportRequest;
use Microsoft\BingAds\V12\Reporting\AccountPerformanceReportRequest;
use Microsoft\BingAds\V12\Reporting\AudiencePerformanceReportRequest;
use Microsoft\BingAds\V12\Reporting\KeywordPerformanceReportRequest;
use Microsoft\BingAds\V12\Reporting\ReportFormat;
use Microsoft\BingAds\V12\Reporting\ReportAggregation;
use Microsoft\BingAds\V12\Reporting\AccountThroughAdGroupReportScope;
use Microsoft\BingAds\V12\Reporting\CampaignReportScope;
use Microsoft\BingAds\V12\Reporting\AccountReportScope;
use Microsoft\BingAds\V12\Reporting\ReportTime;
use Microsoft\BingAds\V12\Reporting\ReportTimePeriod;
use Microsoft\BingAds\V12\Reporting\Date;
use Microsoft\BingAds\V12\Reporting\AccountPerformanceReportFilter;
use Microsoft\BingAds\V12\Reporting\KeywordPerformanceReportFilter;
use Microsoft\BingAds\V12\Reporting\DeviceTypeReportFilter;
use Microsoft\BingAds\V12\Reporting\AccountPerformanceReportColumn;
use Microsoft\BingAds\V12\Reporting\AudiencePerformanceReportColumn;
use Microsoft\BingAds\V12\Reporting\KeywordPerformanceReportColumn;
use Microsoft\BingAds\V12\Reporting\AdGroupPerformanceReportColumn;
use Microsoft\BingAds\V12\Reporting\ReportRequestStatusType;
use Microsoft\BingAds\V12\Reporting\KeywordPerformanceReportSort;
use Microsoft\BingAds\V12\Reporting\SortOrder;


// Specify the Microsoft\BingAds\Auth classes that will be used.
use Microsoft\BingAds\Auth\ServiceClient;
use Microsoft\BingAds\Auth\ServiceClientType;

final class ReportRequestLibrary {
    static function SubmitGenerateReport($report)
    {
          
        $GLOBALS['Proxy'] = $GLOBALS['ReportingProxy']; 

        $request = new SubmitGenerateReportRequest();
        $request->ReportRequest = $report;

        return $GLOBALS['ReportingProxy']->GetService()->SubmitGenerateReport($request);
    }

    static function PollGenerateReport($reportRequestId)
    {
        $GLOBALS['Proxy'] = $GLOBALS['ReportingProxy']; 

        $request = new PollGenerateReportRequest();
        $request->ReportRequestId = $reportRequestId;

        return $GLOBALS['ReportingProxy']->GetService()->PollGenerateReport($request);
    }

        
    static function GetKeywordPerformanceReportRequest($accountId,$date) 
    {
        $GLOBALS['AuthorizationData'] = null;
        $GLOBALS['Proxy'] = null;
        $GLOBALS['CampaignManagementProxy'] = null; 

        // Disable WSDL caching.

        ini_set("soap.wsdl_cache_enabled", "0");
        ini_set("soap.wsdl_cache_ttl", "0");

        // Specify the file to download the report to. Because the file is
        // compressed use the .zip file extension.

        //$DownloadPath = "c:\\reports\\keywordperf.zip";
        $DownloadPath = public_path('/reports/keywordperf.zip');
        // Confirm that the download folder exist; otherwise, exit.

        $length = strrpos($DownloadPath, '\\');
        $folder = substr($DownloadPath, 0, $length);
           
        if (!is_dir($folder))
        {
            printf("The output folder, %s, does not exist.\nEnsure that the " .
                "folder exists and try again.", $folder);
            return;
        }
    try {
            AuthHelper::Authenticate();
            $GLOBALS['ReportingProxy'] = new ServiceClient(ServiceClientType::ReportingVersion12, $GLOBALS['AuthorizationData'], AuthHelper::GetApiEnvironment());
     
                                   
            $PublisherID =  DB :: table('Publisher')
                                        ->where('Publisher.PublisherName','=','Bing')
                                        ->select('Publisher.id')->get();
            $Publisher_ID = json_decode($PublisherID,true);
            $p_id = $Publisher_ID[0]['id'];
                    
            $hiddenMetricsID_Out = DB :: table('HiddenMetrics')                     
                                ->where('HiddenMetrics.Publisher_ID' ,'=',$p_id)
                                ->select('HiddenMetrics.PublisherMetricsParameter')->get();
            $hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);
            
            




            $report = new KeywordPerformanceReportRequest();
            
            $report->Format = ReportFormat::Csv;
            $report->ReportName = 'My Keyword Performance Report';
            $report->ReturnOnlyCompleteData = false;
            $report->Aggregation = ReportAggregation::Daily;
            
            $report->Scope = new AccountThroughAdGroupReportScope();
            $report->Scope->AccountIds = array();
            $report->Scope->AccountIds[] = $accountId;
            $report->Scope->AdGroups = null;
            $report->Scope->Campaigns = null;
            
            $report->Time = new ReportTime();
           // $report->Time->PredefinedTime = ReportTimePeriod::LastThreeMonths;
            
            //  You may either use a custom date range or predefined time.
             date_default_timezone_set('UTC');
             $currentYear = date("Y");
             $currentMonth = date('m');
             $currentDay = date('d');

             $report->Time->CustomDateRangeStart = new Date();
             $report->Time->CustomDateRangeStart->Month = $date['month'];
             $report->Time->CustomDateRangeStart->Day = $date['day'];
             $report->Time->CustomDateRangeStart->Year = $currentYear;
             $report->Time->CustomDateRangeEnd = new Date();
             $report->Time->CustomDateRangeEnd->Month = $currentMonth;
             $report->Time->CustomDateRangeEnd->Day = $currentDay;
             $report->Time->CustomDateRangeEnd->Year = $currentYear;



             $arrayMetricNames =  array(
                        AdGroupPerformanceReportColumn::TimePeriod,
                        AdGroupPerformanceReportColumn::AccountId,
                        AdGroupPerformanceReportColumn::CampaignId,
                        AdGroupPerformanceReportColumn::CampaignName,
                        AdGroupPerformanceReportColumn::AdGroupName,
                    );                 
            
            foreach($hiddenMetricsID_Result as $key=>$value) {
                    array_push($arrayMetricNames,$value['PublisherMetricsParameter']);
            }
            
            $report->Columns = array_filter($arrayMetricNames);
        
           
           //echo "<pre>";print_r($report->Columns); exit;
                // 
            // You may optionally sort by any KeywordPerformanceReportColumn, and optionally
            // specify the maximum number of rows to return in the sorted report.
            
            $report->Sort = array ();
            $keywordPerformanceReportSort = new KeywordPerformanceReportSort();
            $keywordPerformanceReportSort->SortColumn = KeywordPerformanceReportColumn::Clicks;
            $keywordPerformanceReportSort->SortOrder = SortOrder::Ascending;
            $report->Sort[] = $keywordPerformanceReportSort;
            
            $report->MaxRows = 1000;
            
            $report = new SoapVar($report, SOAP_ENC_OBJECT, 'KeywordPerformanceReportRequest', $GLOBALS['ReportingProxy']->GetNamespace());

                // CampaignPerformanceReportRequest
                // SubmitGenerateReport helper method calls the corresponding Bing Ads service operation
                // to request the report identifier. The identifier is used to check report generation status
                // before downloading the report.  
    

                $reportRequestId = ReportRequestLibrary::SubmitGenerateReport($report)->ReportRequestId;
                // $reportRequestId = "30000001707163963";
                // $reportRequestId = "10000000008226541";
                 printf("Report Request ID: %s\n\n", $reportRequestId);
    
                    $waitTime = 30 * 1; 
                    $reportRequestStatus = null;
    
            // This sample polls every 30 seconds up to 5 minutes.
            // In production you may poll the status every 1 to 2 minutes for up to one hour.
            // If the call succeeds, stop polling. If the call or 
            // download fails, the call throws a fault.
        
        for ($i = 0; $i < 10; $i++) {
            sleep($waitTime);
        
            // PollGenerateReport helper method calls the corresponding Bing Ads service operation
            // to get the report request status.
            
            $reportRequestStatus = ReportRequestLibrary::PollGenerateReport($reportRequestId)->ReportRequestStatus;
        
            if ($reportRequestStatus->Status == ReportRequestStatusType::Success ||
                $reportRequestStatus->Status == ReportRequestStatusType::Error)
            {
                break;
            }
        }

        if ($reportRequestStatus != null)
        {
            if ($reportRequestStatus->Status == ReportRequestStatusType::Success)
            {
                $reportDownloadUrl = $reportRequestStatus->ReportDownloadUrl;
                
                if($reportDownloadUrl == null)
                {
                    print "No report data for the submitted request\n";
                }
                else
                {
                    printf("Downloading from %s.\n\n", $reportDownloadUrl);
                  
                    ReportRequestLibrary::DownloadFile($reportDownloadUrl, $DownloadPath);
                    printf("The report was written to %s.\n", $DownloadPath);

                }
                
            }
            else if ($reportRequestStatus->Status == ReportRequestStatusType::Error)
            {
                printf("The request failed. Try requesting the report " .
                        "later.\nIf the request continues to fail, contact support.\n");
            }
            else  // Pending
            {
                printf("The request is taking longer than expected.\n " .
                        "Save the report ID (%s) and try again later.\n",
                        $reportRequestId);
            }
        }
    
    }
catch (SoapFault $e)
{
    print "\nLast SOAP request/response:\n";
    printf("Fault Code: %s\nFault String: %s\n", $e->faultcode, $e->faultstring);
    print $GLOBALS['Proxy']->GetWsdl() . "\n";
    print $GLOBALS['Proxy']->GetService()->__getLastRequest()."\n";
    print $GLOBALS['Proxy']->GetService()->__getLastResponse()."\n";
    
    if (isset($e->detail->AdApiFaultDetail))
    {
        ReportingExampleHelper::OutputAdApiFaultDetail($e->detail->AdApiFaultDetail);
        
    }
    elseif (isset($e->detail->ApiFaultDetail))
    {
        ReportingExampleHelper::OutputApiFaultDetail($e->detail->ApiFaultDetail);
    }
}
catch (Exception $e)
{
    // Ignore fault exceptions that we already caught.
    if ($e->getPrevious())
    { ; }
    else
    {
        print $e->getCode()." ".$e->getMessage()."\n\n";
        print $e->getTraceAsString()."\n\n";
    }
}
    

      // return $encodedReport;
    }
    
// Using the URL that the PollGenerateReport operation returned,
// send an HTTP request to get the report and write it to the specified
// ZIP file.

static  function DownloadFile($reportDownloadUrl, $downloadPath)
{
    if (!$reader = fopen($reportDownloadUrl, 'rb'))
    {
        throw new Exception("Failed to open URL " . $reportDownloadUrl . ".");
    }

    if (!$writer = fopen($downloadPath, 'wb'))
    {
        fclose($reader);
        throw new Exception("Failed to create ZIP file " . $downloadPath . ".");
    }

    $bufferSize = 100 * 1024;

    while (!feof($reader))
    {
        if (false === ($buffer = fread($reader, $bufferSize)))
        {
             fclose($reader);
             fclose($writer);
             throw new Exception("Read operation from URL failed.");
        }

        if (fwrite($writer, $buffer) === false)
        {
             fclose($reader);
             fclose($writer);
             $exception = new Exception("Write operation to ZIP file failed.");
        }
    }

    fclose($reader);
    fflush($writer);
    fclose($writer);
       
}


    static function GetAccountPerformanceReportRequest($accountId) 
    {
        $report = new AccountPerformanceReportRequest();
        
        $report->Format = ReportFormat::Tsv;
        $report->ReportName = 'My Account Performance Report';
        $report->ReturnOnlyCompleteData = false;
        $report->Aggregation = ReportAggregation::Weekly;
        
        $report->Scope = new AccountReportScope();
        $report->Scope->AccountIds = array();
        $report->Scope->AccountIds[] = $accountId;
            
        $report->Time = new ReportTime();
        $report->Time->PredefinedTime = ReportTimePeriod::Yesterday;
        
        //  You may either use a custom date range or predefined time.
        //  date_default_timezone_set('UTC');
        //  $LastYear = date("Y") - 1;
        //   $report->Time->CustomDateRangeStart = new Date();
        //  $report->Time->CustomDateRangeStart->Month = 1;
        //  $report->Time->CustomDateRangeStart->Day = 1;
        //  $report->Time->CustomDateRangeStart->Year = $LastYear;
        //  $report->Time->CustomDateRangeEnd = new Date();
        //  $report->Time->CustomDateRangeEnd->Month = 12;
        //  $report->Time->CustomDateRangeEnd->Day = 31;
        //  $report->Time->CustomDateRangeEnd->Year = $LastYear;
        
        //  If you specify a filter, results may differ from data you see in the Bing Ads web application
        //  $report->Filter = new AccountPerformanceReportFilter();
        //  $report->Filter->DeviceType = array (
        //      DeviceTypeReportFilter::Computer,
        //      DeviceTypeReportFilter::SmartPhone
        //  );

        $report->Columns = array (
                AccountPerformanceReportColumn::TimePeriod,
                AccountPerformanceReportColumn::AccountId,
                AccountPerformanceReportColumn::AccountName,
                AccountPerformanceReportColumn::Clicks,
                AccountPerformanceReportColumn::Impressions,
                AccountPerformanceReportColumn::Ctr,
                AccountPerformanceReportColumn::AverageCpc,
                AccountPerformanceReportColumn::Spend,
        );
        
        $encodedReport = new SoapVar($report, SOAP_ENC_OBJECT, 'AccountPerformanceReportRequest', $GLOBALS['ReportingProxy']->GetNamespace());
        
        return $encodedReport;
    }

    static function GetAudiencePerformanceReportRequest($accountId) 
    {
        $report = new AudiencePerformanceReportRequest();
        
        $report->Format = ReportFormat::Tsv;
        $report->ReportName = 'My Audience Performance Report';
        $report->ReturnOnlyCompleteData = false;
        $report->Aggregation = ReportAggregation::Daily;
        
        $report->Scope = new AccountThroughAdGroupReportScope();
        $report->Scope->AccountIds = array();
        $report->Scope->AccountIds[] = $accountId;
        $report->Scope->AdGroups = null;
        $report->Scope->Campaigns = null;
        
        $report->Time = new ReportTime();
        $report->Time->PredefinedTime = ReportTimePeriod::Yesterday;
        
        //  You may either use a custom date range or predefined time.
        //  date_default_timezone_set('UTC');
        //  $LastYear = date("Y") - 1;
        //   $report->Time->CustomDateRangeStart = new Date();
        //  $report->Time->CustomDateRangeStart->Month = 1;
        //  $report->Time->CustomDateRangeStart->Day = 1;
        //  $report->Time->CustomDateRangeStart->Year = $LastYear;
        //  $report->Time->CustomDateRangeEnd = new Date();
        //  $report->Time->CustomDateRangeEnd->Month = 12;
        //  $report->Time->CustomDateRangeEnd->Day = 31;
        //  $report->Time->CustomDateRangeEnd->Year = $LastYear;
        
        $report->Columns = array (
                AudiencePerformanceReportColumn::TimePeriod,
                AudiencePerformanceReportColumn::AccountId,
                AudiencePerformanceReportColumn::CampaignId,
                AudiencePerformanceReportColumn::AudienceId,
                AudiencePerformanceReportColumn::Clicks,
                AudiencePerformanceReportColumn::Impressions,
                AudiencePerformanceReportColumn::Ctr,
                AudiencePerformanceReportColumn::AverageCpc,
                AudiencePerformanceReportColumn::Spend,
        );
        
        $encodedReport = new SoapVar($report, SOAP_ENC_OBJECT, 'AudiencePerformanceReportRequest', $GLOBALS['ReportingProxy']->GetNamespace());
        
        return $encodedReport;
    }
 

}
