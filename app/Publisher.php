<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{

	public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['PublisherName','Genre_ID','PublisherType_ID','CreatedOn','ModifiedOn','ModifiedBy','isActive'];

	public $table = "dbo.Publisher";

}
