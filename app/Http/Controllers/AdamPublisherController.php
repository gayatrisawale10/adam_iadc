<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Campaign;
use App\Client;
use App\ClientBrand;
use App\DurationType;
use Session;
use validator;
use DB;
Use Redirect;
use Carbon\Carbon;
use DateTime;

use App\User;
use App\UserClient;

class AdamPublisherController extends Controller
{

    public function getCompaignDetails()
    {
        $u_id =Auth::user()->id;  
        //dd($u_id);
        $getPublisherId = DB::table('UserPublisher')
                        ->where('UserPublisher.User_ID','=', $u_id)
                        ->get();
        $getPublisherId=$getPublisherId[0]->Publisher_ID;
        $PublisherId=$getPublisherId;
        $getCompaignDetails =  DB :: table('AdamsLineItem')
                        ->join('AdamLineItemDIT','AdamsLineItem.id',  '=', 'AdamLineItemDIT.AdamsLineItem_ID')
                        ->join('DataInputType','DataInputType.id',    '=', 'AdamLineItemDIT.DataInputType_ID')
                        ->join('Publisher','Publisher.PublisherName', '=', 'AdamsLineItem.Website_Publisher')
                        ->join('Campaign','Campaign.id',              '=', 'AdamsLineItem.campaign_ID')
                        ->join('ClientBrand','ClientBrand.id',        '=', 'Campaign.ClientBrand_ID')
                        ->join('Client','Client.id',                  '=', 'ClientBrand.Client_ID')               
                        ->where('DataInputType.DataInputTypeName',    '=','Manual')
                        ->where('Publisher.id',    '=',$PublisherId)
                        ->select('Campaign.id as Campaign_ID', 'Client.ClientName', 'ClientBrand.BrandName', 'Campaign.CampaignName','Campaign.CampaignID','Campaign.StartDate','Campaign.EndDate','Campaign.alwaysOn','Campaign.CreatedOn')
                        ->distinct('Campaign.id as Campaign_ID', 'Client.ClientName', 'ClientBrand.BrandName', 'Campaign.CampaignName','Campaign.CampaignID','Campaign.StartDate','Campaign.EndDate','Campaign.alwaysOn','Campaign.CreatedOn')
                        ->get();
        $getCompaignDetails = json_decode($getCompaignDetails,true);
        //dd($getCompaignDetails);
        return view('AdamLines.adamPublisherCampaign',compact('getCompaignDetails','PublisherId'))->with('i');
    }

    public function AdamPublisherAllAdamLines($Campaign_ID,$PublisherId)
    {
        //echo"<pre>Campaign_ID "; print_r($Campaign_ID);//echo"<pre>PublisherId "; print_r($PublisherId);
        $getCamapignDates=  DB :: table('Campaign')
                        ->where('Campaign.id',    '=',$Campaign_ID)
                        ->select('Campaign.StartDate', 'Campaign.EndDate')
                        ->get();
        $getCamapignDates = json_decode($getCamapignDates,true);
        //dd($getCamapignDates);

        $getAdamLineItemDetails =  DB :: table('AdamsLineItem')
                        ->join('AdamLineItemDIT','AdamsLineItem.id',  '=', 'AdamLineItemDIT.AdamsLineItem_ID')
                        ->join('DataInputType','DataInputType.id',    '=', 'AdamLineItemDIT.DataInputType_ID')
                        ->join('Campaign','Campaign.id',              '=', 'AdamsLineItem.campaign_ID')
                        ->join('Publisher','Publisher.PublisherName', '=', 'AdamsLineItem.Website_Publisher')
                        ->where('DataInputType.DataInputTypeName',    '=','Manual')
                        ->where('Campaign.id',    '=',$Campaign_ID)
                        ->where('Publisher.id',    '=',$PublisherId)             
                        ->select('AdamsLineItem.*', 'Campaign.CampaignName')
                        ->get();
        $getAdamLineItemDetails = json_decode($getAdamLineItemDetails,true);
        //dd($getAdamLineItemDetails);
        return view('AdamLines.adamPublisherAdamLines',compact('getCamapignDates','getAdamLineItemDetails'))->with('i');
    }

    public function AdamPublisherAdamLinesDailyData($AdamsLineItem_ID){
        $getAdamLineItemDailyDetails=DB::select("exec SP_IADC_GetAdamLineItemDailyData ?", array($AdamsLineItem_ID));
        //dd($getAdamLineItemDailyDetails);
        return view('AdamLines.adamPublisherAdamLinesDaily',compact('getAdamLineItemDailyDetails'))->with('i');
    }

    public function saveDailyAdamLineItems(Request $request)
    {
        //dd($request->all());
        $data=$request->input();
        $date = date("Y-m-d", strtotime($data['StartDate']));
        $metricsValuesArray=$data['metricsValuesArray'];

        foreach ($metricsValuesArray as $key1 => $value1)
        {          
            if($value1=='')
            {
                $flag=1;
            }
            else
            {
                $flag=0;
            }
        }
         
        if($flag)
        {
          return response()->json(['status'=>'error'], 200);
        }
        else
        {
          //echo"<pre>"; print_r($flag); exit;
          $current_time = Carbon::now()->toDateTimeString();
          //$adamLineItemDailyId=$request->input('AdamLineItem_ID');

            foreach ($metricsValuesArray as $key => $value)
            {
                if(!is_null($value))
                {

                    // $LineItemDailydetails=DB::table('AdamLineItemDaily')
                    //     ->join('AdamLineItemMetrics','AdamLineItemDaily.AdamLineItemMetrics_ID','=','AdamLineItemMetrics.id')
                    //     ->join('AdamsLineItem','AdamLineItemMetrics.AdamsLineItem_ID','=','AdamsLineItem.id')
                    //     ->where('AdamLineItemDaily.Date', '=', $date)
                    //     ->where('AdamLineItemMetrics.AdamsLineItem_ID','=', $data['AdamLineItem_ID'])
                    //     ->where('AdamLineItemMetrics.Mst_Metrics_ID', '=',($key+1))
                    //     ->update([
                    //         'Value' =>$value,                         
                    //         'ModifiedOn' =>  $current_time,
                    //         'ModifiedBy' => 2,
                    //         'isActive' => True,
                    //     ]);
                    $LineItemDailydetails=DB::table('APIDataDaily')
                        ->join('AdamLineItemMetrics','APIDataDaily.AdamLineItemMetrics_ID','=','AdamLineItemMetrics.id')
                        ->join('AdamsLineItem','AdamLineItemMetrics.AdamsLineItem_ID','=','AdamsLineItem.id')
                        ->where('APIDataDaily.Date', '=', $date)
                        ->where('AdamLineItemMetrics.AdamsLineItem_ID','=', $data['AdamLineItem_ID'])
                        ->where('AdamLineItemMetrics.Mst_Metrics_ID', '=',($key+1))
                        ->update([
                            'Value' =>$value,                         
                            'ModifiedOn' =>  $current_time,
                            'ModifiedBy' => 2,
                            'isActive' => True,
                        ]);
                        
                }

            }
        }
        if($LineItemDailydetails){
          return response()->json(['date'=>$data['StartDate'], 'status'=>'success'], 200);
        }
           
    }
}