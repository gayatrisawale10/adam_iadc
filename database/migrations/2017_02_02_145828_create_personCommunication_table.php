<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonCommunicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personCommunication', function (Blueprint $table) {
            $table->increments('PersonCommunication_ID');
            $table->integer('Person_ID')->unsigned();
            $table->integer('CommunicationType_ID')->unsigned();
            $table->string('Value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personCommunication');
    }
}
