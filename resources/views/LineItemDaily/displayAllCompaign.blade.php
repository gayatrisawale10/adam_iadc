@extends('layouts.appPublisher')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <h2> Campaign Details</h2>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
     <div class="portlet-body">
        <div class="table-toolbar">
            <div style="width: 100%; overflow-x:auto;margin-bottom: 50px;">
                <table class="table table-bordered display" id="sample_editable_1">
            	    <thead>
                    <tr>
                        <th>Client </th>
            			<th>Brand </th>
                        <th>Campaign</th>
            			<th>Start Date</th>
            			<th>End Date</th>
            			<th width="280px">Action</th>
                    </tr>
            	    </thead>
            	    <tbody>
                		@foreach ($getCompaignDetails as $cntr)
                			<tr align="center">
                                <td>{{$cntr->ClientName}}</td>
                   			    <td>{{$cntr->BrandName}}</td>
                                <td>{{$cntr->CampaignName}}</td>
                				<td>{{$cntr->StartDate}}</td>
                				<td>{{$cntr->EndDate}}</td>
                				<td>
                				   @if(Auth::user()->UserRole_ID==2)
                                  <a class="btn btn-primary"
                                    href="{{ route('LineItemDaily.getLineItemDailyDetails', [ $cntr->campaign_ID,$cntr->lineItemType,$PublisherId] ) }}">Daily Line Item</a>
                                     @endif

                                    @if(Auth::user()->UserRole_ID==4)
                                    <a class="btn btn-primary"
                                       href="{{ route('LineItemDaily.getLineItemDetailsForCity', [ $cntr->campaign_ID ] ) }}"> Line Item</a>
                                    @endif

                				</td>
                			 </tr>


                		@endforeach
            	    </tbody>
                </table>
            @endsection
        </div>
    </div>
</div>
