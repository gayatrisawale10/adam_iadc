<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineItemDaily extends Model
{

	public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['LineItemMetrics_ID','Date','Value','ipAddress','CreatedOn','ModifiedOn','ModifiedBy','isActive'];

	public $table = "dbo.LineItemDaily";

}
