<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use validator;
use DB;
Use Redirect;
use Carbon\Carbon;
use App\HiddenMetrics;
use Maatwebsite\Excel\Facades\Excel;

class AdamManualPublisherUploadController extends Controller
{
    public function getSheetListFromExcel(Request $request){
    	$pubReportFile=$request->file('manualPubReportFile');
    	
		 $sheetNames = Excel::load($pubReportFile)->getSheetNames();
		 return $sheetNames;
    }


  //  Public function  UploadMultiExcelForManualPublisher(Request $request ){
  //  	    // print_r($request->Selected_sheet_ids);die;
  //  	    $Selected_sheet_ids = $request->Selected_sheet_ids;
		//  $AdamsLineItem_ID=$request->AdamsLineItem_ID;
	 //     $PublisherName = $request->PublisherName;
		// $currentDate = Carbon::now()->format('Ymdhis');
		// // =======================get APISourceID================================================
		// $APIName=$PublisherName."Excel";
		// $APISource_ID= DB:: table('APISource') 
		// 				->where('APISource.APIName' ,'=', $APIName)	
		// 				->select('APISource.id')
		// 				->first();
		// $APISource_ID=$APISource_ID->id;
		// // =======================get APISourceID================================================
		// $arrayMetricDB=[];
		// if($isAdamFile=$request->hasfile('manualPubReportFile')){
		// 	$pubReportFile=$request->file('manualPubReportFile');
		// 	$pubReportData = Excel::selectSheetsByIndex(0,1,2)->load($pubReportFile)->get()->toArray();
		// 	// $pubReportData = \Excel::load($pubReportFile)->get();
		// 	// print_r($pubReportData);die;
		// 	//   	$pubReportData=$pubReportData[0];
		// 	// $pubReportArray = json_decode(json_encode($pubReportData), true);
		// 	//================ save file in reports================================================= 
		//   	$pubReportName=$pubReportFile->getClientOriginalName();
		//     $pubReportFilename = pathinfo($pubReportName, PATHINFO_FILENAME);
		// 	$apubReportExtension = pathinfo($pubReportName, PATHINFO_EXTENSION);
		// 	$newPubReportName = $pubReportFilename.'-'.$currentDate.'.'.$apubReportExtension;
		// 	$pubReportFilePath='reports\ManualPublisherExcelInputFile\\'. $newPubReportName;
		// 	$pubReportFile->move(public_path('reports/ManualPublisherExcelInputFile'), $newPubReportName);
		// 	//================ save file in reports=================================================
		// 	//================ save file in DB======================================================
		// 	$current_time = Carbon::now()->toDateTimeString();
		// 	$APIPrcoessingLog_ID=DB::table('ManualPublisherExcelInputFile')
		// 					->insertGetId([
		// 					  	'AdamsLineItem_ID' => $AdamsLineItem_ID,
		// 					  	'PubReportName' => $newPubReportName,
		// 					  	'PubReportPath' => $pubReportFilePath,

		// 					  	'CreatedOn' => $current_time,
		// 					  	'ModifiedOn' => $current_time,
		// 					  	'IsActive' => 1,
		// 					  	'ModifiedBy' => 1,
		// 					  	'CreatedBy' => 1
		// 					]);
		// 	//================ save file in DB======================================================
		//    	// ======================================ApiMetrics=====================================
		//     $hiddenMetricsID_Out = DB :: table('APIMetrics')  
		//     						->join('APISource',	 'APIMetrics.APISource_ID',	'=',   'APISource.id')			
		//     						->join('HiddenMetricsGroup', 		'APIMetrics.HiddenMetricsGroup_ID', '=',   'HiddenMetricsGroup.id')
		//     						->join('HiddenMetricsGroupMapping', 'HiddenMetricsGroup.id', '=',   'HiddenMetricsGroupMapping.HiddenMetricsGroup_ID')
		//     						->join('HiddenMetrics', 			'HiddenMetrics.id',		 '=',   'HiddenMetricsGroupMapping.HiddenMetrics_ID')
		//     						->where('HiddenMetrics.Publisher_ID' ,'=', -1)		    						
		//     						->select('HiddenMetricsGroup.HiddenMetricsGroupName', 'APISource.APIName', 'HiddenMetrics.HiddenMetricsName','HiddenMetrics.ApiMetricsName' )
		//     						->get();
		//     $hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);

	 //        foreach($hiddenMetricsID_Result as $key=>$value) {
	 //   	     	array_push($arrayMetricDB,$value['ApiMetricsName']);
	 //   	    } 	
	 //   	    // ====================================store in APIParam============================
	 //   	    $existingParamArray = array_fill(0, 19, NULL);
	 //   	    $AdamLineDCMMapping_ID=NULL;

	 //   	    $APIParams_ID_frmDB=DB::select("exec SP_IADC_GetAPIParams_ID ?,?,  ?,?,?, ?,?,?, ?,?,?, ?,?,?,  ?,?,?, ?,?,? ,?", array($AdamsLineItem_ID, 		
		// 					$AdamLineDCMMapping_ID,
		// 					$existingParamArray[0],		$existingParamArray[1], 	$existingParamArray[2],
		// 					$existingParamArray[3],		$existingParamArray[4],     $existingParamArray[5],
		// 					$existingParamArray[6],     $existingParamArray[7],     $existingParamArray[8],
		// 					$existingParamArray[9],		$existingParamArray[10],    $existingParamArray[11],
		// 					$existingParamArray[12],	$existingParamArray[13],	$existingParamArray[14],
		// 					$existingParamArray[15],	$existingParamArray[16],	$existingParamArray[17],
		// 					$existingParamArray[18]
		// 				));
  //           $APIParams_ID=$APIParams_ID_frmDB[0]->APIParams_ID;
		// 	//echo"<br>";echo"<pre>APIParams_ID: AfterDBHit ";print_r($APIParams_ID); 
		// 	// ====================================store in APIParam============================
		// 	// ====================================store in APIBreakDown============================
		// 	$existingBreakDownArray = array_fill(0, 25, NULL);
		// 	$APIParamBreakDown_ID=DB::select("exec SP_IADC_GetAPIParamBreakDown_ID ?,?,?,?,?,?,?,?,?, 
		// 									?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,  ?,?
		// 									", 
		// 									array($APIParams_ID,$APISource_ID,
		// 									$existingBreakDownArray[0],  $existingBreakDownArray[1],  $existingBreakDownArray[2],
		// 									$existingBreakDownArray[3],	 $existingBreakDownArray[4],  $existingBreakDownArray[5],	
		// 									$existingBreakDownArray[6],	 $existingBreakDownArray[7],  $existingBreakDownArray[8],
		// 									$existingBreakDownArray[9],	 $existingBreakDownArray[10], $existingBreakDownArray[11],
		// 									$existingBreakDownArray[12], $existingBreakDownArray[13], $existingBreakDownArray[14],
		// 									$existingBreakDownArray[15], $existingBreakDownArray[16], $existingBreakDownArray[17],
		// 									$existingBreakDownArray[18], $existingBreakDownArray[19], $existingBreakDownArray[20],
		// 									$existingBreakDownArray[21], $existingBreakDownArray[22], $existingBreakDownArray[23],
		// 									$existingBreakDownArray[24]  
											  
		// 								));
		// 	$APIParamBreakDown_ID=$APIParamBreakDown_ID[0]->APIParamBreakDown_ID;
		// 	// ====================================store in APIBreakDown============================

		//     // ====================================store in APIDataDaily============================
		//    $countPubReportData = count($pubReportData); 
				
		// 		$pubReportData=$pubReportData[0];
				
		// 		$pubReportArray = json_decode(json_encode($pubReportData), true);
		
	 //    	foreach ($pubReportArray as $eachRow) {  

	 //    		// $date=$eachRow['date']['date'];
	 //    	    $date='2019-12-11 00:00:00.000000';//$eachRow['date'];
	 //    		$startDate=date('Y-m-d', strtotime($date)); 
	 //    		$pub_id=-1;
	 //    		foreach($arrayMetricDB as $key)
		// 		{	
		// 			if(array_key_exists($key, $pubReportArray))
		// 		    { 
		// 		        $metricsNameInAPI = $key;
		// 		   		$value=$pubReportArray[$key];
				   		
		// 		  		DB::select("exec SP_IADC_GetAPIDataDaily_ID ?,?,?,?,?", array($APIParamBreakDown_ID ,$metricsNameInAPI,$value,$startDate,$pub_id));
		// 		    }
		// 		}
		// 	}
		// 	// ====================================end store in APIDataDaily============================	
		// 	 return Redirect::back();	
		// }
  //  }


	Public function  UploadMultiExcelForManualPublisher(Request $request ){


		//echo"<pre>"; print_r($request->all()); exit; 

		$AdamsLineItem_ID=$request->AdamsLineItem_ID;
		$PublisherName = $request->PublisherName;
		$currentDate = Carbon::now()->format('Ymdhis');

		//echo"<pre>"; print_r($PublisherName); exit;

		$Selected_sheet_ids = explode(",",$request->Selected_sheet_ids);
		
		if($Selected_sheet_ids!=""){
			// =======================get APISourceID================================================
			$APIName=$PublisherName."Excel";
			$APISource_ID=DB :: table('APISource') 
							->where('APISource.APIName' ,'=', $APIName)	
							->select('APISource.id')
							->first();
			//echo"<pre>"; print_r($APISource_ID); exit; 	
			
			
			
			if($APISource_ID){
				$APISource_ID=$APISource_ID->id;
				//echo"<pre>"; print_r("true"); exit;
				// =======================get APISourceID================================================
				$arrayMetricDB=[];
				if($isAdamFile=$request->hasfile('manualPubReportFile')){
					$pubReportFile=$request->file('manualPubReportFile');
					
					$Get_pubReportData = Excel::selectSheetsByIndex($Selected_sheet_ids)->load($pubReportFile)->get()->toArray();
					 $countSelectedSheet_ids = count($Selected_sheet_ids);
					if($countSelectedSheet_ids==1){
						$pubReportData[]=$Get_pubReportData;
					}else{
						$pubReportData = $Get_pubReportData;
					}
					// $pubReportData = \Excel::load($pubReportFile)->get();
			        //================ save file in reports================================================= 
				  	$pubReportName=$pubReportFile->getClientOriginalName();
				    $pubReportFilename = pathinfo($pubReportName, PATHINFO_FILENAME);
					$apubReportExtension = pathinfo($pubReportName, PATHINFO_EXTENSION);
					$newPubReportName = $pubReportFilename.'-'.$currentDate.'.'.$apubReportExtension;
					$pubReportFilePath='reports\ManualPublisherExcelInputFile\\'. $newPubReportName;
					$pubReportFile->move(public_path('reports/ManualPublisherExcelInputFile'), $newPubReportName);
				
					//================ save file in reports=================================================
			        foreach ($pubReportData as $key => $value) {

				        $New_pubReportData=$pubReportData[$key]; 
				      
					    $pubReportArray = json_decode(json_encode($New_pubReportData), true);
						
						//================ save file in DB======================================================
						$current_time = Carbon::now()->toDateTimeString();
						$APIPrcoessingLog_ID=DB::table('ManualPublisherExcelInputFile')
										->insertGetId([
										  	'AdamsLineItem_ID' => $AdamsLineItem_ID,
										  	'PubReportName' => $newPubReportName,
										  	'PubReportPath' => $pubReportFilePath,

										  	'CreatedOn' => $current_time,
										  	'ModifiedOn' => $current_time,
										  	'IsActive' => 1,
										  	'ModifiedBy' => 1,
										  	'CreatedBy' => 1
										]);
						//================ save file in DB======================================================
						$res=AdamManualPublisherUploadController::storeFileData($AdamsLineItem_ID,$APISource_ID, $pubReportArray);				
					   	
				  	} // Count ForEach Loop End;
					// ====================================end store in APIDataDaily============================	
					//return Redirect::back();
					return response()->json(['status'=>'success']);	
				}
			}
			else{
				//echo"<pre>"; print_r("false"); exit;
				return response()->json(['status'=>'wrongPublisher']);
			}				

		}else{
			return response()->json(['status'=>'failure']);
		}
	}

	// #######################################################################################################
	public static function storeFileData($AdamsLineItem_ID,$APISource_ID, $pubReportArray)
	{
		//echo"<pre>ff "; print_r($AdamsLineItem_ID); print_r($APISource_ID);print_r($pubReportArray); exit; 	
		$arrayMetricDB=[];
		// ======================================ApiMetrics=====================================
	   /* $hiddenMetricsID_Out = DB :: table('APIMetrics')  
	    						->join('APISource',	 'APIMetrics.APISource_ID',	'=',   'APISource.id')			
	    						->join('HiddenMetricsGroup', 		'APIMetrics.HiddenMetricsGroup_ID', '=',   'HiddenMetricsGroup.id')
	    						->join('HiddenMetricsGroupMapping', 'HiddenMetricsGroup.id', '=',   'HiddenMetricsGroupMapping.HiddenMetricsGroup_ID')
	    						->join('HiddenMetrics', 			'HiddenMetrics.id',		 '=',   'HiddenMetricsGroupMapping.HiddenMetrics_ID')
	    						->where('HiddenMetrics.Publisher_ID' ,'=', -1)		    						
	    						->select('HiddenMetricsGroup.HiddenMetricsGroupName', 'APISource.APIName', 'HiddenMetrics.HiddenMetricsName','HiddenMetrics.ApiMetricsName' )
	    						->get();*/

	    						 $hiddenMetricsID_Out = DB :: table('HiddenMetrics')  
	    						 ->where('HiddenMetrics.Publisher_ID' ,'=', -1)		    	
	    						 ->select('HiddenMetrics.ApiMetricsName' )
	    						->get();

	    							

	    $hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);

        foreach($hiddenMetricsID_Result as $key=>$value) {
   	     	array_push($arrayMetricDB,$value['ApiMetricsName']);
   	    } 	
	   
   	    // ====================================store in APIParam============================
   	    $existingParamArray = array_fill(0, 19, NULL);
   	    $AdamLineDCMMapping_ID=NULL;

   	    $APIParams_ID_frmDB=DB::select("exec SP_IADC_GetAPIParams_ID ?,?,  ?,?,?, ?,?,?, ?,?,?, ?,?,?,  ?,?,?, ?,?,? ,?", array($AdamsLineItem_ID, 		
					$AdamLineDCMMapping_ID,
					$existingParamArray[0],		$existingParamArray[1], 	$existingParamArray[2],
					$existingParamArray[3],		$existingParamArray[4],     $existingParamArray[5],
					$existingParamArray[6],     $existingParamArray[7],     $existingParamArray[8],
					$existingParamArray[9],		$existingParamArray[10],    $existingParamArray[11],
					$existingParamArray[12],	$existingParamArray[13],	$existingParamArray[14],
					$existingParamArray[15],	$existingParamArray[16],	$existingParamArray[17],
					$existingParamArray[18]
				));
        $APIParams_ID=$APIParams_ID_frmDB[0]->APIParams_ID;
		// ====================================store in APIParam============================
		// ====================================store in APIBreakDown============================
		$existingBreakDownArray = array_fill(0, 25, NULL);
		$APIParamBreakDown_ID=DB::select("exec SP_IADC_GetAPIParamBreakDown_ID ?,?,?,?,?,?,?,?,?, 
									?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,  ?,?
									", 
									array($APIParams_ID,$APISource_ID,
									$existingBreakDownArray[0],  $existingBreakDownArray[1],  $existingBreakDownArray[2],
									$existingBreakDownArray[3],	 $existingBreakDownArray[4],  $existingBreakDownArray[5],	
									$existingBreakDownArray[6],	 $existingBreakDownArray[7],  $existingBreakDownArray[8],
									$existingBreakDownArray[9],	 $existingBreakDownArray[10], $existingBreakDownArray[11],
									$existingBreakDownArray[12], $existingBreakDownArray[13], $existingBreakDownArray[14],
									$existingBreakDownArray[15], $existingBreakDownArray[16], $existingBreakDownArray[17],
									$existingBreakDownArray[18], $existingBreakDownArray[19], $existingBreakDownArray[20],
									$existingBreakDownArray[21], $existingBreakDownArray[22], $existingBreakDownArray[23],
									$existingBreakDownArray[24]  
									  
								));
		$APIParamBreakDown_ID=$APIParamBreakDown_ID[0]->APIParamBreakDown_ID;
		// ====================================store in APIBreakDown============================

	    // ====================================store in APIDataDaily============================
    	if (!empty($pubReportArray)) {
	    	foreach ($pubReportArray as $eachRow) { 
				
				//echo"<pre>"; print_r($eachRow);	    	    

				if(is_string( $eachRow['date']))
				{
					//echo"<pre>"; print_r($eachRow['date']);	 
					$date=$eachRow['date'];//'2019-12-11 00:00:00.000000';//$eachRow['date']['date'];//
	    			$startDate=date('Y-m-d', strtotime($date));
				}
				else{
	    	    	$date=$eachRow['date']['date'];//'2019-12-11 00:00:00.000000';//$eachRow['date']['date'];//
	    			$startDate=date('Y-m-d', strtotime($date));
	    		}	



	    		$pub_id=-1;
	    		foreach($arrayMetricDB as $key)
				{	
					if(array_key_exists($key, $eachRow))
				    {
				        $metricsNameInAPI = $key;
				   		$value=$eachRow[$key];
				   		DB::select("exec SP_IADC_GetAPIDataDaily_ID ?,?,?,?,?", array($APIParamBreakDown_ID ,$metricsNameInAPI,$value,$startDate,$pub_id));
				    }
				}
			}
    	}
    	return 1;
	}

	// #######################################################################################################
	public function MatricsMapping(Request $request){
		
		$getHiddenMetricsGroupName=DB::table('HiddenMetricsGroup')
				   				 ->get();
		//dd($getHiddenMetricsGroupName);	

		$getMappedMatrics=DB::table('HiddenMetricsGroupMapping')
                    ->join('HiddenMetricsGroup', 'HiddenMetricsGroup.id','=', 'HiddenMetricsGroupMapping.HiddenMetricsGroup_ID')
		    		->join('HiddenMetrics',  	 'HiddenMetrics.id',	 '=', 'HiddenMetricsGroupMapping.HiddenMetrics_ID')
		    		->where('HiddenMetrics.Publisher_ID','=', -1) 
      				->select('HiddenMetricsGroup.HiddenMetricsGroupName', 'HiddenMetrics.HiddenMetricsName','HiddenMetrics.Publisher_ID')
      				->orderBy('HiddenMetricsGroup.id', 'ASC')
     				->get();
     	//dd($getMappedMatrics);			
      	return view('AdamLines/MatricsMapping', compact('getHiddenMetricsGroupName','getMappedMatrics'));
	}

	public function MapMetrices(Request $request){
		//dd($request->all());

		$metriceName=$request->metriceName;
		$HiddenMetricsGroup_ID=$request->HiddenMetricsGroup_ID;
		// ----------- put data in to  HiddenMetrics table----------------------------------
		$ApiMetricsName = str_replace(' ', '_', $metriceName);
		$PublisherMetricsParameter=strtolower($ApiMetricsName);
		$Publisher_ID=-1;
		$typeFlag=2;
		$APIParamColumnName=Null;

		//echo"<pre>"; print_r($PublisherMetricsParameter); exit; 
		// ==============Check Duplicate Mapping================================================================
	    $MapMetricesExist=$this->DuplicateMapMetrices($metriceName, $HiddenMetricsGroup_ID);
	    //dd($camapaignNameExist);	
	    // ==============Check Duplicate Mapping================================================================
	    //-1 means not present
	    if($MapMetricesExist== -1){
		 	//dd("insert");
			$HiddenMetrics = HiddenMetrics::create([
						  'HiddenMetricsName' => $metriceName,
						  'ApiMetricsName' => $PublisherMetricsParameter,//$ApiMetricsName,
						  'Publisher_ID' => $Publisher_ID,
						  'PublisherMetricsParameter' => $PublisherMetricsParameter,
						  'typeFlag' => $typeFlag,
						  'APIParamColumnName' => $APIParamColumnName

			]);
			//dd($HiddenMetrics);
			//----------------last updated camapaign-------------------
			$getLatestHiddenMetricsID= DB::table('HiddenMetrics')
								->select('id') 	
								->orderby('id','DESC')
					            ->first();
	        $getLatestHiddenMetricsID=$getLatestHiddenMetricsID->id;
	        //----------------end last updated camapaign-------------------

	        // ----------- put data in to  HiddenMetrics table----------------------------------

	        //$HiddenMetricsGroupMapping=DB::select ('INSERT INTO HiddenMetricsGroupMapping VALUES (?, ?)', [ $HiddenMetricsGroup_ID, $getLatestHiddenMetricsID]);

	        $HiddenMetricsGroupMapping=DB::table('HiddenMetricsGroupMapping')->insert(
					    ['HiddenMetricsGroup_ID' => $HiddenMetricsGroup_ID, 'HiddenMetrics_ID' =>$getLatestHiddenMetricsID]
					);
		}
		else{
			//dd("out");
		}

		return redirect('matricsMapping')->with('status', 'Successfully Mapped!');
	}
	
	public function DuplicateMapMetrices($metriceName, $HiddenMetricsGroup_ID)
    {
    	//dd($campignName);
    	$campignNameExistCheck=DB::table('HiddenMetrics')
             	->where('HiddenMetrics.HiddenMetricsName','=',$metriceName)
             	->where('HiddenMetrics.Publisher_ID','=','-1')
				->get();
        $campignNameExistCheck=json_decode($campignNameExistCheck);
       	//dd($campignNameExistCheck);

       	if($campignNameExistCheck){
       		//dd("Present");
       		return 1;
       	}
       	else{
       		//dd("empty");
       		return -1;
       	}	
    }

    public function upload_URL_ForManualPublisher(Request $request){
    	//dd($request->all());

    	$AdamsLineItem_ID=$request->AdamsLineItem_ID;
    	$ApiName=$request->URLName;
    	$current_time = Carbon::now()->toDateTimeString();
    	// -----------------custom URL----------------------------------------------

    	$getKey=(explode("/",$ApiName));
		$getKey=array_pop($getKey);
		//print_r($getKey);

		// checksum
		$urlkey=(explode("?",$getKey));
		//print_r($urlkey);

		$URLKey=$urlkey[0];
		$checksum=$urlkey[1];

		// echo"<pre>"; print_r($URLKey); 
		// echo"<pre>"; print_r($checksum); 
		// exit;

		$customUrl='https://manager.videoplaza.com/custom-reporting/shared/report/'.$URLKey.'?&'.$checksum.'&columnFilter=0&pageNumber=1& pageSize=100&sortAscending=true&sortIndex=-1&subtotal=true&total=true';
		
		//dd($customUrl);
		//exit;
    	// -----------------custom URL----------------------------------------------
    	//dd($apiName); 
    	// ---Check lineItemId and URL pair is present in table------- 
    	$URLNameExistCheck=DB::table('AdamLineItem_URLMapping')
             	->where('AdamLineItem_URLMapping.AdamsLineItem_ID','=',$AdamsLineItem_ID)
             	->where('AdamLineItem_URLMapping.ApiName','=',$ApiName)
             	->where('AdamLineItem_URLMapping.CustomApiName','=',$customUrl)
				->get();
        $URLNameExistCheck=json_decode($URLNameExistCheck);


        if($URLNameExistCheck){
       		//dd("Present");
       		DB::table('AdamLineItem_URLMapping')
			    ->where('AdamsLineItem_ID', $AdamsLineItem_ID)
			    ->update([
			      	'ApiName' => $ApiName,  
			      	'CustomApiName'=>$customUrl,	
			      	'ModifiedOn' => $current_time,
			      	'IsActive' => 1,
			      	'ModifiedBy' => '1',
			    ]);
       		
       	}
       	else{
       		//dd("empty");
			$AdamLineItem_URLMapping =DB::table('AdamLineItem_URLMapping')
				->insert([
					'AdamsLineItem_ID' => $AdamsLineItem_ID,
					'ApiName' => $ApiName,
					'CustomApiName'=>$customUrl,
					'CreatedOn' => $current_time,
				  	'ModifiedOn' => $current_time,
				  	'IsActive' => 1,
				  	'ModifiedBy' => 1,
				  	'CreatedBy' => 1
				]);  
       	}

       	return Redirect::back();
    	// ---End Check lineItemId and URL pair is present in table------- 
    }		
}
