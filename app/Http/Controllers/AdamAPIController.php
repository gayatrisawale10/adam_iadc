<?php
namespace App\Http\Controllers;
use autoload;
//require_once "./vendor/autoload.php";
use Illuminate\Http\Request;
use validator;
use Illuminate\Support\Facades\Auth;
use DB;
Use Redirect;
use Carbon\Carbon;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\FacebookRequest;

use FacebookAds\Api;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\User;

use File;
use Zipper;
use Maatwebsite\Excel\Facades\Excel;
use SoapVar;
use SoapFault;
use Exception;
use DateTime;
use Cookie;
use Microsoft\BingAds\Auth\AuthorizationData;
use Microsoft\BingAds\Auth\OAuthTokenRequestException;
use Microsoft\BingAds\Auth\OAuthWebAuthCodeGrant;
use Microsoft\BingAds\Auth\WebAuthHelper;
use Illuminate\Support\Facades\Storage;

// Specify the Microsoft\BingAds\V12\Reporting classes that will be used.
use Microsoft\BingAds\V12\Reporting\ReportRequestStatusType;

// Specify the Microsoft\BingAds\Auth classes that will be used.
use Microsoft\BingAds\Auth\ServiceClient;
use Microsoft\BingAds\Auth\ServiceClientType;

// Specify the Microsoft\BingAds\Samples classes that will be used.
use App\V12\AuthHelper;
use App\V12\ReportRequestLibrary;

//Adwords
//use App\Ads\AdWords\v201806\Reporting\DownloadCriteriaReportWithSelector;
//use App\Ads\AdWords\v201809\Reporting\DownloadCriteriaReportWithSelector;
use App\Ads\AdWords\v201809\Reporting\ParallelReportDownload;
use Google\AdsApi\AdWords\AdWordsSession;
//DCM
use App\v3\examples\Reports\DownloadReportFile;
use App\v3\examples\Reports\GenerateReportFile;
use App\v3\examples\Reports\CreateStandardReport;
use App\v3\auth\AuthenticateUsingServiceAccount;
//DCM Unique
use App\v3\examples\Reports\CreateReachReport;
//DBM
use App\dbm\DBMReportProcessing;
//GA
use App\GA\GoogleAnalyticsReportProcessing;

use Google_Client;
use Google_Service_Dfareporting;
use Google_Service_Analytics;
use Google_Service_DoubleClickBidManager;
use Google_Service_DoubleClickBidManager_Resource_Queries;
use Google_Service_DoubleClickBidManager_RunQueryRequest;
use Google_Service_AnalyticsReporting;

use Google_Service_AnalyticsReporting_DateRange;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_Dimension;
use Google_Service_AnalyticsReporting_ReportRequest;
use Google_Service_AnalyticsReporting_GetReportsRequest;
//For SizMek
use SoapClient;
use SoapHeader;
use Session;
//define('STDIN',fopen("php://stdin","r"));
define('STORE_ON_DISK', false, true);
define('TOKEN_FILENAME', 'tokens.dat', true);
ini_set('max_execution_time', '1000');
	
class AdamAPIController extends Controller
{

  	// This redirect URI allows you to copy the token from the success screen.
  	const OAUTH_REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob';
  	// Location where authorization credentials will be cached.
  	const TOKEN_STORE = 'auth-sample.dat'; 

  	public function apiView()
  	{	
  		$DBMAPIName =  DB :: table('APISource')
  						->join('Publisher','Publisher.id', '=', 'APISource.Publisher_ID')
  		                ->where('Publisher.PublisherName','=','DBM')
  		                ->select('APISource.APIName')->get();
		$DBMAPIName = json_decode($DBMAPIName,true);
		return view('api',compact('DBMAPIName'));

  	}

  	Public function dailyProgrssCheck(Request $request=null){
  		//dd($request->all());
  		if($request!=null)
     	{
     		$dailyVal = $request['daily'];  
     	} 
     	else 
     	{	
     		$dailyVal=NULL;
     	}
     	
        $data=[];
        $current_Date = Carbon::now()->toDateString();
  		$todaysUnprocesedReport =  DB :: select("SELECT *
							FROM APIPrcoessingLog
							WHERE cast(ApiProcessingStartTime as date) = '".$current_Date."' and IsCron = 'False' and ApiProcessingEndTime IS NOT NULL"); 
  		//dd($todaysUnprocesedReport);
  		$unprocessedReportName=[];
  		
  		if($todaysUnprocesedReport){
	  		foreach ($todaysUnprocesedReport as $eachReport ) {
	  			//dd($eachReport);
	  			
	  				$unprocessedReportName[]=$eachReport->ApiName;
	  				$unprocessedReportNameString=implode(", ",$unprocessedReportName);	
	  		}
	  		//dd($unprocessedReportNameString);
	  		// ===========================================================
	        $data['ReportName']=$unprocessedReportNameString;
	        $data['email']="sampletwitter1234@gmail.com";

	        \Mail::send('sendDailyReportUpdate', $data, function ($m) use($data) {
	            $m->from(getenv('MAIL_FROM_ADDRESS'), 'IADC');
	            $m->to($data['email'], 'Hello User')->subject('Daily API Report Update');
	        });
	        //return response()->json(['status' => 'mailSent'], 200);              
	  	}
  	}
  	// ===================================================================================================
  	public function getProgressStatus(Request $request){
  		//dd($request['UniqueReportId']);
  		$current_Date = Carbon::now()->toDateString();
  		// $currentReport =  DB :: select("SELECT *
				// 			FROM APIPrcoessingLog
				// 			WHERE cast(ApiProcessingStartTime as date) = '".$current_Date."' and IsCron = 'False' and ApiProcessingEndTime IS NOT NULL"); 
  		$incompleteProcessReport=[];
  		$incompleteReportsString="No Report";
  		$processingDoneReport=[];
  		$completeReportsString="No Report";

  		$currentReport =  DB :: select("SELECT *
				FROM APIPrcoessingLog
				WHERE cast(ApiProcessingStartTime as date) = '".$current_Date."' and IsCron = 'False' and UniqueReportId= '".$request['UniqueReportId']."'");
  		//echo"<pre>"; print_r($currentReport); 
  		//exit;
  		foreach ($currentReport as $eachReport ) {
  			//dd($eachReport);
  			$publisherName=$eachReport->publisherName;
  			if($eachReport->ApiProcessingEndTime ==  Null)
  			{
  				$incompleteProcessReport[]=$eachReport->ApiName;
  				$incompleteReportsString=implode(", ",$incompleteProcessReport);
  			}
  			else
  			{
  				$processingDoneReport[]=$eachReport->ApiName;
  				$completeReportsString=implode(", ",$processingDoneReport);
  			}	
  		}
  		//echo"<pre>incompleteProcessReport "; print_r($incompleteProcessReport); echo"<pre>";print_r($incompleteReportsString);
  		//echo"<pre>processingDoneReport "; print_r($processingDoneReport);echo"<pre>";print_r($completeReportsString);
  		$ProcessingDoneString="Processing Done for ".$publisherName."......../   <b>Incomplete Reports</b> : ".$incompleteReportsString."......../   <b>Complete Reports</b>:  ".$completeReportsString;

  		//echo"<pre>ProcessingDoneString "; print_r($ProcessingDoneString);
  		// exit;
  		// if($processingDoneReport){
  		// 	return response()->json(['status' => 'ProcessingDone','publisherName'=>$publisherName], 200);
  		// }
		return response()->json(['status' => 'ProcessingDoneString','publisherName'=>$publisherName, 'ProcessingDoneString'=>$ProcessingDoneString], 200);		 
  	}
  	//###########Facebook##############################################################################
    public function fbAPIProcessing(Request $request=null)
    {	
    	// ############################################################################################
    	//print_r($request->all());exit;	
    	ini_set('memory_limit', '-1');
		set_time_limit(0);
    	//this is check for wether startdate is oming from cron or by onclick.
     	if($request!=null)
     	{
     		$startDate = $request['StartDate'];  
     		$tillDate  = $request['endDate'];
     		$UniqueReportId=$request['UniqueReportId'];
     		$IsCron    = 0;
     	} 
     	else 
     	{	
     		// yesterday's date
     		$startDate = date('Y-m-d',strtotime("-1 days"));; 
     		$tillDate  = date('Y-m-d',strtotime("-1 days"));;
     		$UniqueReportId=NULL;
     		$IsCron    = 1;
     	}
	   	
	   	$PublisherID =  DB :: table('Publisher')
		 							->where('Publisher.PublisherName','=','Facebook')
		 							->select('Publisher.id')->get(); 
		$Publisher_ID = json_decode($PublisherID,true);
		$p_id = $Publisher_ID[0]['id'];
		 
		$getAPISource_ID = DB :: table('APISource')  
							->join('Publisher',	 'APISource.publisher_ID',		'=',   'Publisher.id') 	
							->where('APISource.Publisher_ID' ,'=',$p_id)
							->select('APISource.id', 'APISource.APIName')
							->where('APISource.IsActive' ,'=',1)
							->get();
	    //echo"<pre>getAPISource_ID: "; print_r( $getAPISource_ID); exit;

	    $arrayMetricParamNames =[];
	    $BreakDown =[];
	    $arrayMetricDB =[];//used in API data popultaion in DB
	    
		foreach ($getAPISource_ID as $eachAPISource_ID) {
			$APISource_ID=$eachAPISource_ID->id;
			$ApiName=$eachAPISource_ID->APIName;

	    	//echo"<pre>ApiName: "; print_r($ApiName);
			//------------------insert start apiHit Log----------------------------------------------
			$current_time = Carbon::now()->toDateTimeString();
			$APIPrcoessingLog_ID=$this->APIPrcoessingLog($APISource_ID,$ApiName,'Facebook',$UniqueReportId,$startDate,$tillDate, $current_time,$IsCron);	
			//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
			//------------------End insert start apiHit Log------------------------------------------

			$hiddenMetricsID_Out = DB :: table('HiddenMetrics')  
							->join('APIMetrics', 'APIMetrics.HiddenMetrics_ID',	'=',   'HiddenMetrics.id')                   
	                  		->join('APISource',	 'APIMetrics.APISource_ID',		'=',   'APISource.id') 		
	                  		->where('APISource.APIName' ,'=',$ApiName)//'AgeAndGender'
	                  		->where('HiddenMetrics.Publisher_ID' ,'=',$p_id)
	                  		->where('HiddenMetrics.typeFlag' ,'!=',null)
	                  		->select('HiddenMetrics.ApiMetricsName', 'HiddenMetrics.typeFlag')
	                  		->orderBy('HiddenMetrics.typeFlag','ASC')
	                      	->orderBy('HiddenMetrics.APIParamColumnName','ASC')
	                      	->get();
		    $hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);
		    //echo"<pre>"; print_r( $hiddenMetricsID_Result ); exit;

		    foreach($hiddenMetricsID_Result as $key=>$value) {
		    	//echo"<pre>"; print_r( $key ); print_r( $value['ApiMetricsName'] ); exit; 
		        if($value['typeFlag']==1 ){
		        	array_push($arrayMetricParamNames,$value['ApiMetricsName']);
		    	}
		    	elseif($value['typeFlag']==2){
		        	array_push($arrayMetricParamNames,$value['ApiMetricsName']);
		        	array_push($arrayMetricDB,$value['ApiMetricsName']);
		    	}
		    	elseif($value['typeFlag']==3){
		        	array_push($BreakDown,$value['ApiMetricsName']);
		    	}
		    }
		    $fields = array_filter($arrayMetricParamNames);
		    $arrayMetricDB = array_filter($arrayMetricDB);   	
		    
		    //echo"<pre>fields2"; print_r($fields); echo"<pre>BreakDown"; print_r($BreakDown); echo"<pre>arrayMetricDB2"; print_r($arrayMetricDB); //exit; 
		    //===========hit Fcabook API====================================================== 
		    //$this->FbApi($arrayMetricDB,$APISource_ID,$startDate,$tillDate,$BreakDown,$fields);
		    //===========hit Fcabook API======================================================
		    //------------------insert end apiHit Log----------------------------------------------
			$current_timetoEnd = Carbon::now()->toDateTimeString();	
			$APIPrcoessingLog_ID=$this->UpdateAPIPrcoessingLog($APIPrcoessingLog_ID, $current_timetoEnd);	
			//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
			//------------------End insert end apiHit Log------------------------------------------
			// ---------unset all array empty for next breakdown-----------------------------------
			$arrayMetricParamNames =[];
		    $BreakDown =[];
		    $arrayMetricDB =[];
			// ---------End unset all array empty for next breakdown-----------------------------------
			//exit;
		}   //exit; 
		return response()->json(['status' => 'success'], 200);
    }
    public function FbApi($arrayMetricDB,$APISource_ID,$startDate,$tillDate,$BreakDown,$fields){
    	// =================== multiple facebook App ===================================================
	    $FBIDsPath = public_path('FBIDs.csv');
     	config(['excel.import.startRow' => 1 ]);
		$FBIDsData = \Excel::load($FBIDsPath)->get()->toArray();	    
	    //echo"<pre>"; print_r($FBIDsData); exit;

   		$app_id = '501471827045469';
		$app_secret = '913996bb865f39529a553a45ed0b2f0e';
		$app_name = 'Live_IADC';
		//$app_token = 'EAAHIFgIdrF0BAFhx1TUfJAf1HjaprhSyRy6GIP3aPFafmpR8CmTJZAZCJxOA5CFBEcFe9F3pgMiLWvdPdL4FIweHgAGdiRCzaPJ1mZCH4DioZBKYG6aPzy1hGZCwwuvas7tmvvxOJqsZAOPbaJVrO8cafwVyt3ULIuoP4ybtXZA4xakZAFtzZCTiPUTiC64YBcWQZD';
		$app_token = 'EAAHIFgIdrF0BAPeYdRvwpcw4zZAAy1ZAHSiMiZA38FATnVT8NnkqLN5L4rcWZB1iwMKOz8KPEdovwF5htBFUqwZCucE6h2ymcF2e6Na917M7PIClqgItTmTXo6nAA5UAXmJ4ksZA6S3UZAHw85Snl91Os30LdGhE7UJAltWFNPZBZBHs6dE3ohAkkSDrZBkBERfvsZD';
		$app_url_id = '144137076586817';

		$adAccount_Id=[];
		$fb = new Facebook([ 
		  'app_id' => $app_id,
		  'app_secret' => $app_secret,
		]);

		$helper = $fb->getRedirectLoginHelper();
		if (!isset($_SESSION['facebook_access_token'])) {
		  $_SESSION['facebook_access_token'] = null;
		}
		if (!$_SESSION['facebook_access_token']) {
		  	$helper = $fb->getRedirectLoginHelper();
			try {
				$_SESSION['facebook_access_token'] = (string) $helper->getAccessToken();
			} catch(FacebookResponseException $e) {
				// When Graph returns an error
				echo 'Graph returned an error: ' . $e->getMessage();
			    exit;
			} catch(FacebookSDKException $e) {
			 	// When validation fails or other local issues
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
			    exit;
			}
		}
		if ($_SESSION['facebook_access_token']) {
		    echo "You are logged in!";
		} else {
			$permissions = ['ads_management'];
			$loginUrl = $helper->getLoginUrl('http://localhost:8888/marketing-api/', $permissions);
			echo '<a href="' . $loginUrl . '">Log in with Facebook</a>';
			//echo "<pre>loginUrl "; print_r($loginUrl); exit;
		}

		$access_token = $app_token ;			
		$api = Api::init($app_id, $app_secret, $access_token);
		$ch = curl_init();			
		curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v5.0/".$app_url_id."?fields=adaccounts.limit(500)&special_ad_category=NONE&access_token=".$access_token."");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
		    echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		$r = json_decode($result);		
		//echo "<pre>ApiRes "; print_r($r); exit;
		$i = sizeOf($r->adaccounts->data);
		//echo"<pre>"; print_r($me->getAdAccounts()); 
		$adAccount_Id=[];
		while ( $i >= 1) {
		  array_push($adAccount_Id, $r->adaccounts->data[$i-1]->id);
		  $i--;
		}
		//echo"<pre>accountIds by Curl : ";  print_r($app_name);  print_r($adAccount_Id); //exit;

		/****Curl Code to hit next url to get aal adaccountIds, Mukunda Sathe / Gayatri  Sawale 01-03-2019****/
		foreach ($adAccount_Id as  $ad_account_id) {
			//$ad_account_id='act_625771244507568';
			//$ad_account_id='act_1298923860209091';
			//echo"<pre>"; print_r($ad_account_id); //exit;

			if($ad_account_id!='act_1298923860209091')
			{	
				$api->setLogger(new CurlLogger());
				$params = array(
				  'level' => 'ad',
				  'filtering' => array(),
				  'time_range' => array('since' => $startDate,'until' =>$tillDate),
				  'time_increment' => 1,
				  'limit' =>5000,
				  'breakdowns'=>$BreakDown
				  //'breakdowns'=> array('publisher_platform','platform_position') 
				);
				//echo"<pre>"; print_r($params); exit;

				$result = json_encode((new AdAccount($ad_account_id))
						->getInsights($fields,$params)
						->getResponse()
						->getContent(),JSON_PRETTY_PRINT);		
				echo"<pre>After ForeachCall = "; print_r($result);  
				//exit;
				//file_put_contents('GAReport_json_encode_'.$viewIdValue.'.json', $serializedData);
				AdamAPIController::storeApivalue($result,$arrayMetricDB,$APISource_ID,'Facebook');	
			}
		}//exit;
		// // ==================== multiple facebook App===================================================
    }
    //###########Facebook###############################################################################
    
    //#####################DBM#########################################################################
	//public static function dbmBasic( Request $request)
	public  function dbmAPI( Request $request){
		echo"<pre>"; print_r($request->all());
		$this->dbmBasic();
	}
	// ========================dbmBasic: works on btn Hit, oauthCallBack() function===========================
	public static function dbmCron(Request $request=NULL )
	{
		// echo"in dbmCron";
		// dd($request->all());
		// exit;
		if($request!=null)
     	{
     		$startDate = $request['StartDate'];  
     		$tillDate  = $request['endDate'];
     		$UniqueReportId="DBM_unq";
     		$IsCron    = 0;		
     		echo"<pre>startDate1: "; print_r($startDate);
     		echo"<pre>tillDate1: "; print_r($tillDate); 
     		echo"<pre>UniqueReportId: "; print_r($UniqueReportId); 
     		echo"<pre>IsCron1: "; print_r($IsCron);
     		//exit;
     	} 
     	else 
     	{	
     		// yesterday's date
     		$startDate = date('Y-m-d',strtotime("-1 days")); 
     		$tillDate  = date('Y-m-d',strtotime("-1 days"));
     		$UniqueReportId="DBM_Cron";
     		$IsCron    = 1;
     		echo"<pre>startDate2: "; print_r( $startDate);
     		echo"<pre>tillDate2: "; print_r( $tillDate); 
     		echo"<pre>UniqueReportId: "; print_r($UniqueReportId); 
     		echo"<pre>IsCron2: "; print_r($IsCron);
     		//exit;
     	}
     	// =====================================================
	
		ini_set('memory_limit', '-1');
       	set_time_limit(0);

		$client = new Google_Client();
		$upTwo = dirname(__DIR__, 2);
		//$DBMreportID= 629840613;
	    $client->setAuthConfig($upTwo . '/dbm/Cron20200418-80df66d01e69.json');
	    //$client->setAuthConfig($upTwo . '/dbm/neonVictoryServiceAccount.json');
	    $client->setApplicationName('DBM API PHP Samples');
	    $client->setAccessType('offline');
	    $client->addScope('https://www.googleapis.com/auth/doubleclickbidmanager');
	    
      	$service = new Google_Service_DoubleClickBidManager($client);
    	$result = $service->queries->listqueries();	
	  	// ------------------------------------------------------------------------------    
	  	// get all 20 DBM ReportIds from DB
	  	$getDBMReportId=DB :: table('APISource')
	  					->join('Publisher','APISource.Publisher_ID','=','Publisher.id')  
						->where('Publisher.PublisherName' ,'=', 'DBM')//'AgeAndGender'
						->where('APISource.IsActive' ,'=', '1')
						->select('APISource.id','APISource.APIName','APISource.DBMReportID')
						->get();
		//echo"DBMReport "; print_r($getDBMReportId); exit;				
		foreach ($getDBMReportId as $key ) {
		 	//echo"<pre>forLoop";
		 	//echo"<br>DBMReportID "; print_r($key->id); 				
		 	//echo"<br>APISourceID "; print_r($key->DBMReportID); 
		 	$DBMreportID=$key->DBMReportID;
		 	$APISource_ID=$key->id;				
	  	  	$APIName=$key->APIName;

	  	  	//------------------insert start apiHit Log----------------------------------------------
			$current_timetoStart = Carbon::now()->toDateTimeString();
			$APIPrcoessingLog_ID=AdamAPIController::APIPrcoessingLog($APISource_ID,$APIName,'DBM',$UniqueReportId, $startDate,$tillDate, $current_timetoStart,$IsCron);	
			//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
			//------------------End insert start apiHit Log------------------------------------------

	  	 	//$reportFileData_Dispaly = $service->queries->getquery('629840613');	  	  
	  	  	$reportFileData_Dispaly = $service->queries->getquery($DBMreportID);	  	  
	  	  	$CloudStoragePath_Display=$reportFileData_Dispaly->metadata->googleCloudStoragePathForLatestReport;
	      	//file_put_contents("Standered_Basic.csv", fopen($CloudStoragePath_Display, 'r'));
	      	echo"Api NAMES: ".$APIName;
	     	file_put_contents($APIName."_".$DBMreportID.".csv", fopen($CloudStoragePath_Display, 'r'));
	     	// --------------------------------------------------------------------------------
	      	//echo "<pre><h1>Report For DBM_Others and DBM_Youtube is Processed...!</h1>";
			//echo"<a href='api' class='btn btn-info pull-right'>BACK</a>";
	      	AdamAPIController::processDBMReport($APISource_ID,$DBMreportID,$APIName,$APIPrcoessingLog_ID);

	      	//echo"1st user"; exit;
        }    
    	echo "<pre><h1>Report For DBM_Others and DBM_Youtube is Processed...!</h1>";
		echo"<a href='api' class='btn btn-info pull-right'>BACK</a>";
	   				  
	}
	// ========================dbmBasic: works on btn Hit, oauthCallBack() function===========================
	public static function dbmBasic(Request $request=NULL )
	{
		// echo"in dbmBasic";
		// dd($request->all());
		// exit;
		if($request!=null)
     	{
     		$startDate = $request['StartDate'];  
     		$tillDate  = $request['endDate'];
     		$UniqueReportId="DBM_unq";
     		$IsCron    = 0;		
     		echo"<pre>startDate1: "; print_r($startDate);
     		echo"<pre>tillDate1: "; print_r($tillDate); 
     		echo"<pre>UniqueReportId: "; print_r($UniqueReportId); 
     		echo"<pre>IsCron: "; print_r($IsCron);
     		//exit;
     	} 
     	else 
     	{	
     		// yesterday's date
     		$startDate = date('Y-m-d',strtotime("-1 days")); 
     		$tillDate  = date('Y-m-d',strtotime("-1 days"));
     		$UniqueReportId="DBM_Cron";
     		$IsCron    = 1;
     		echo"<pre>startDate2: "; print_r( $startDate);
     		echo"<pre>tillDate2: "; print_r( $tillDate); 
     		echo"<pre>UniqueReportId: "; print_r($UniqueReportId); 
     		echo"<pre>IsCron: "; print_r($IsCron);
     		//exit;
     	}
     	// =====================================================
	
		ini_set('memory_limit', '-1');
       	set_time_limit(0);

		$client = new Google_Client();
		$upTwo = dirname(__DIR__, 2);
		//$DBMreportID= 629840613;
	    $client->setAuthConfig($upTwo . '/dbm/client_secrets.json');
	    $client->setApplicationName('DBM API PHP Samples');
	    $client->setAccessType('offline');
	    
	    if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
	    	//echo('in if');
	      	$client->setAccessToken($_SESSION['access_token']);
	      	$service = new Google_Service_DoubleClickBidManager($client);
	    	$result = $service->queries->listqueries();	
		  	// ------------------------------------------------------------------------------    
		  	// get all 20 DBM ReportIds from DB
		  	$getDBMReportId=DB :: table('APISource')
		  					->join('Publisher','APISource.Publisher_ID','=','Publisher.id')  
							->where('Publisher.PublisherName' ,'=', 'DBM')//'AgeAndGender'
							->where('APISource.IsActive' ,'=', '1')
							->select('APISource.id','APISource.APIName','APISource.DBMReportID')
							->get();
			//echo"DBMReport "; print_r($getDBMReportId); exit;				
			foreach ($getDBMReportId as $key ) {
			 	//echo"<pre>forLoop";
			 	//echo"<br>DBMReportID "; print_r($key->id); 				
			 	//echo"<br>APISourceID "; print_r($key->DBMReportID); 
			 	$DBMreportID=$key->DBMReportID;
			 	$APISource_ID=$key->id;				
		  	  	$APIName=$key->APIName;

		  	  	//------------------insert start apiHit Log----------------------------------------------
				$current_timetoStart = Carbon::now()->toDateTimeString();
				$APIPrcoessingLog_ID=AdamAPIController::APIPrcoessingLog($APISource_ID,$APIName,'DBM',$UniqueReportId, $startDate,$tillDate, $current_timetoStart,$IsCron);	
				//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
				//------------------End insert start apiHit Log------------------------------------------

		  	 	//$reportFileData_Dispaly = $service->queries->getquery('629840613');	  	  
		  	  	$reportFileData_Dispaly = $service->queries->getquery($DBMreportID);	  	  
		  	  	$CloudStoragePath_Display=$reportFileData_Dispaly->metadata->googleCloudStoragePathForLatestReport;
		      	//file_put_contents("Standered_Basic.csv", fopen($CloudStoragePath_Display, 'r'));
		      	echo"Api NAMES: ".$APIName;
		     	file_put_contents($APIName."_".$DBMreportID.".csv", fopen($CloudStoragePath_Display, 'r'));
		     	// --------------------------------------------------------------------------------
		      	//echo "<pre><h1>Report For DBM_Others and DBM_Youtube is Processed...!</h1>";
				//echo"<a href='api' class='btn btn-info pull-right'>BACK</a>";
		      	AdamAPIController::processDBMReport($APISource_ID,$DBMreportID,$APIName,$APIPrcoessingLog_ID);

		      	//echo"1st user"; exit;
	        }    
	    	echo "<pre><h1>Report For DBM_Others and DBM_Youtube is Processed...!</h1>";
			echo"<a href='api' class='btn btn-info pull-right'>BACK</a>";
	    } else {
	 	    //echo"in else 12 "; //exit;
	        return redirect()->to('/oauth2CallBackDBM');
	        //return redirect()->route('oauth2CallBackDBM');
	 	    //$redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/oauth2callbackDBM';
	        //echo $redirect_uri; exit;
	        //header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
	    }				  
	}	
	// ------- add oauth2CallBackDBM for Ga and DBM Display and Youtube------------

    public function oauth2CallBackDBM()
    {	
    	//$DBMreportID = Session::get('DBMreportID');
    	//echo"oo";
    	
    	session_start();
		
		$client = new Google_Client();
		$upTwo = dirname(__DIR__, 2);
		$client->setAuthConfig( $upTwo. '/dbm/client_secrets.json');

		$url = dirname(__FILE__);
		$array = explode('\\',$url);
		$count = count($array);
		$authCallUrl='http://' . $_SERVER['HTTP_HOST'] . '/oauth2CallBackDBM';
		$client->setRedirectUri($authCallUrl);
		$client->addScope('https://www.googleapis.com/auth/doubleclickbidmanager');
		$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
		//dd($DBMreportID);
		// Handle authorization flow from the server.
		if (! isset($_GET['code'])) {
			echo"if";
		  	$auth_url = $client->createAuthUrl();
		  	header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL)); exit;
		} 
		else {
		  $client->authenticate($_GET['code']);
		  $_SESSION['access_token'] = $client->getAccessToken();
		  //$this->dbmBasic( $DBMreportID);
		   $this->dbmBasic();
		}
    }
	public static function processDBMReport($APISource_ID,$DBMreportID,$APIName,$APIPrcoessingLog_ID){ 
		//echo"<pre>". $DBMreportID;
		$currentdate=Carbon::now()->toDateString();
		// -------------check process file file in Public directory For DBM_Dispaly
		$dbmFilePublicPath=public_path($APIName.'_'.$DBMreportID.'.csv');
		$dataDBMFromPublic = \Excel::load($dbmFilePublicPath)->get()->toArray();
		//$response = ApiController::storeSizmekAndGA_DBM__AdWordY_ApiValue($dataDBMFromPublic,'DBM_Others');
		//$arrayMetricDB= array('1' => 'imp','2' => 'click' );
		//$APISource_ID=5;
		$publisherName='DBM';
		// -----------get $p_id---------------
		$PublisherID =  DB :: table('Publisher')
		 							->where('Publisher.PublisherName','=',$publisherName)
		 							->select('Publisher.id')->get();
		$Publisher_ID = json_decode($PublisherID,true);
		$p_id = $Publisher_ID[0]['id'];
		// -----------end get $p_id---------------

		// -----------get $arrayMetricDB---------------
		$hiddenMetricsID_Out = DB :: table('HiddenMetrics')  
							->join('APIMetrics', 'APIMetrics.HiddenMetrics_ID',	'=',   'HiddenMetrics.id')                   
	                  		->join('APISource',	 'APIMetrics.APISource_ID',		'=',   'APISource.id') 		
	                  		->where('APISource.id' ,'=',$APISource_ID)//'AgeAndGender'
	                  		->where('HiddenMetrics.Publisher_ID' ,'=',$p_id)
	                  		
	                  		->where('HiddenMetrics.typeFlag' ,'!=',null)
	                  		->select('HiddenMetrics.ApiMetricsName', 'HiddenMetrics.typeFlag')
	                  		->orderBy('HiddenMetrics.typeFlag','ASC')
	                      	->orderBy('HiddenMetrics.APIParamColumnName','ASC')

	                      	->get();
	    $hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);

	   
	    $arrayMetricDB =[];//used in API data popultaion in DB
	    
	    foreach($hiddenMetricsID_Result as $key=>$value) {
	    	//echo"<pre>"; print_r( $key ); print_r( $value['ApiMetricsName'] ); exit; 
	        if($value['typeFlag']==2){ 	
	        	array_push($arrayMetricDB,$value['ApiMetricsName']);
	    	}
	    }
	    $arrayMetricDB = array_filter($arrayMetricDB);   
	    // -----------end get $arrayMetricDB---------------
	    //echo"<pre>";print_r($DBMreportID); echo"<br>"; print_r( $dataDBMFromPublic ); exit;

	    //$response = AdamAPIController:: storeApivalue($dataDBMFromPublic,$arrayMetricDB,$APISource_ID,'DBM');
	    // -------------end check process file file in Public directory For DBM_Dispaly
	    // -------------move file in DBMReport Folder-----------------
	    //$new_path=public_path('/reports/DBM_Report/DBM_Display/Processed_DBM_display_Report_'.$currentdate.'.csv');
      	$new_path=public_path('/reports/DBM_Report/Processed_DBM_'.$APIName.'_Report_'.$DBMreportID.'_'.$currentdate.'.csv');
      	$move= File::move($dbmFilePublicPath, $new_path);
	    // -------------move file in DBMReport Folder-----------------
	    //------------------insert end apiHit Log----------------------------------------------
		$current_timetoEnd = Carbon::now()->toDateTimeString();	
		$APIPrcoessingLog_ID=AdamAPIController::UpdateAPIPrcoessingLog($APIPrcoessingLog_ID, $current_timetoEnd);	
		//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
		//------------------End insert end apiHit Log------------------------------------------
	}
	//#####################DBM#########################################################################

	//###########DCM###################################################################################
    public function adamDcm(Request $request=Null)
    {
     	//echo"<pre>"; print_r($request->all()); exit;
     	ini_set('MAX_EXECUTION_TIME', '-1');
    	set_time_limit(0);

     	if($request!=null)
     	{
     		$_SESSION['startDate'] = $request['StartDate'];  
     		$_SESSION['endDate']  = $request['endDate'];
     		// ------------------------------------------------------------
     		$startDate = $request['StartDate'];  
     		$tillDate  = $request['endDate'];
     		$UniqueReportId=$request['UniqueReportId'];
     		$IsCron    = 0;
     		//echo"<pre>1: "; print_r($startDate); exit;
     	} 
     	else 
     	{	
     		// yesterday's date
     		$_SESSION['startDate'] = date('Y-m-d',strtotime("-1 days")); 
     		$_SESSION['endDate']  = date('Y-m-d',strtotime("-1 days"));
     		// ------------------------------------------------------------
     		$startDate = date('Y-m-d',strtotime("-1 days")); 
     		$tillDate  = date('Y-m-d',strtotime("-1 days"));
     		$UniqueReportId=NULL;
     		$IsCron    = 1;
     		//echo"<pre>2: "; print_r($startDate); exit;
     	}
     	// -----------------------------------------------------------------------

		ob_start();
		ini_set('implicit_flush', 1);

		$_SESSION['user_profile_id'] = '5018824';
		$authAcct = new AuthenticateUsingServiceAccount();
		$authAcct->run(app_path('v3\dcmiadc-fcdf17511686.json'),'dcmiadc@dcmiadc.iam.gserviceaccount.com');
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		// get Report Type
        $DcmReportType =  DB :: table('APISource')
                          ->join('Publisher','Publisher.id', '=', 'APISource.Publisher_ID')
                          ->where('Publisher.PublisherName','=','DCM')
                          ->select('APISource.APIName','APISource.id')
                          ->where('APISource.IsActive','=', 1)
                          ->get();
        $DcmReportType = json_decode($DcmReportType,true);
       	//echo"DcmReportType"; print_r($DcmReportType); exit;
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		foreach ($DcmReportType as $DcmReportTypeName) {
			//echo"<pre>DcmReportTypeName: ";  print_r($DcmReportTypeName['APIName']); 			
			$dcmReportType=$DcmReportTypeName['APIName'];
			$APISource_ID=$DcmReportTypeName['id'];
			$_SESSION['dcmReportType']=$dcmReportType;
			//echo"APISource_ID"; print_r($APISource_ID); exit;
			//------------------insert start apiHit Log----------------------------------------------
			$current_timetoStart = Carbon::now()->toDateTimeString();
			$APIPrcoessingLog_ID=AdamAPIController::APIPrcoessingLog($APISource_ID,$dcmReportType,'DCM',$UniqueReportId,$startDate,$tillDate, $current_timetoStart,$IsCron);	
			//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
			//------------------End insert start apiHit Log------------------------------------------
			// ######################################################################################
			$hiddenMetricsID_Out = DB :: table('HiddenMetrics')  
					->join('Publisher',	 'HiddenMetrics.Publisher_ID',		'=',   'Publisher.id')
					->join('APIMetrics', 'APIMetrics.HiddenMetrics_ID',	'=',   'HiddenMetrics.id')  
					->join('APISource',	 'APIMetrics.APISource_ID',		'=',   'APISource.id') 		
	                ->where('APISource.APIName' ,'=',$dcmReportType)
              		->where('Publisher.PublisherName' ,'=','DCM')
              		->select('HiddenMetrics.ApiMetricsName', 'HiddenMetrics.PublisherMetricsParameter', 'HiddenMetrics.typeFlag', 'HiddenMetrics.APIParamColumnName')
	               	->get();
			//echo"hiddenMetricsID_Result"; print_r($hiddenMetricsID_Out); //exit;
			$arrayMetricDB=[];
			$DcmReportType=[];
			foreach($hiddenMetricsID_Out as $key=>$value) {
				if($value->typeFlag==2){
			    	array_push($arrayMetricDB,$value->ApiMetricsName);
			    	$arrayMetricDB=	array_filter($arrayMetricDB);    	
				}
			}
			//echo"arrayMetricDBINDCM"; print_r($arrayMetricDB); exit;
			// ######################################################################################
			// **************************************************************************************
			if($dcmReportType=='STANDARD')			
			{
				$createStandardReport = new CreateStandardReport();			
				$createStandardReport->run();
			}	
			if($dcmReportType=='REACH')
			{
				$createStandardReport = new CreateReachReport();			
				$createStandardReport->run();
			}
			
				
			$generateReportFile = new GenerateReportFile();
			$generateReportFile->run();

			$downloadReportFile = new DownloadReportFile();
			$downloadReportFile->run();
			// **************************************************************************************
			$filePath = public_path('/reports/dcmReports/example_report_'.$dcmReportType.'.csv');

		    config(['excel.import.startRow' => 10 ]);
		    $result = \Excel::load($filePath)->get()->toArray();
		    //echo "<pre>result"; print_r($result); // exit;
			AdamAPIController::storeApivalue($result,$arrayMetricDB,$APISource_ID,'DCM');
			//=================================================================================
			// ----------------Move DCM file in ProcessedDCM directory----------------------------
			$date= new DateTime();
 		   	$current_time=$date->getTimestamp();
        	$new_path=public_path('reports\dcmReports\ProcessedDCMReport\P_'.$dcmReportType.'_'.$current_time.'.csv');	
			$move= File::move($filePath, $new_path);
			// ----------------End Move DCM file in ProcessedDCM directory------------------------
			//------------------insert end apiHit Log----------------------------------------------
			$current_timetoEnd = Carbon::now()->toDateTimeString();	
			$APIPrcoessingLog_ID=AdamAPIController::UpdateAPIPrcoessingLog($APIPrcoessingLog_ID, $current_timetoEnd);	
			//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
			//------------------End insert end apiHit Log------------------------------------------

			session_unset();
		}	
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	}
	//###########DCM####################################################################################
	//###########Adwords################################################################################
	public function adamAdwords(Request $request=Null)
    {
     	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     	//echo"<pre>"; print_r($request->all()); exit;
     	ini_set('MAX_EXECUTION_TIME', '-1');
    	set_time_limit(0);

     	if($request!=null)
     	{	
     		define('STDIN',fopen("php://stdin","r"));
     		$startDate = $request['StartDate'];  
     		$endDate  = $request['endDate'];
     		$UniqueReportId=$request['UniqueReportId'];
     		$IsCron    = 0;
     	} 
     	else 
     	{	
     		// yesterday's date
     		$startDate = date('Y-m-d',strtotime("-1 days")); 
     		$endDate  = date('Y-m-d',strtotime("-1 days"));
     		$UniqueReportId=Null;
     		$IsCron    = 1;
     	}
		// ---------------------------------------------------------------------

    	$date= new DateTime();
    	$current_time=$date->getTimestamp();
    	$pubName='AdWords';
    	// ======================================================================
		//===========get breakDown, parameter, metrices==========================	
		
	  	$getAdwordReportType=DB :: table('APISource')
	  					->join('Publisher','APISource.Publisher_ID','=','Publisher.id')  
						->where('Publisher.PublisherName' ,'=', $pubName)//'AgeAndGender'
						->where('APISource.IsActive' ,'=', '1')
						->select('APISource.id','APISource.APIName','APISource.DBMReportID')
						->get();
		//echo"getAdwordReportType "; print_r($getAdwordReportType); exit;				
		foreach ($getAdwordReportType as $key ) {
			//echo"getAdwordReportType "; print_r($key->APIName); exit;
			$arrayMetricDB=[];
			$metricesListPerReportType=[];

			$APISource_ID=$key->id;
			$ReportType=$key->APIName;

			//------------------insert start apiHit Log----------------------------------------------
			$current_timetoStart = Carbon::now()->toDateTimeString();
			$APIPrcoessingLog_ID=AdamAPIController::APIPrcoessingLog($APISource_ID,$ReportType,'Adwords',$UniqueReportId,$startDate,$endDate, $current_timetoStart,$IsCron);	
			//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
			//------------------End insert start apiHit Log------------------------------------------

			$hiddenMetricsID_Out = DB :: table('HiddenMetrics')  
							->join('APIMetrics', 'APIMetrics.HiddenMetrics_ID',	'=',   'HiddenMetrics.id')                   
	                  		->join('APISource',	 'APIMetrics.APISource_ID',		'=',   'APISource.id')
	                  		->join('Publisher','APISource.Publisher_ID','=','Publisher.id')   		
	                  		->where('APISource.APIName' ,'=',$key->APIName)//'AgeAndGender'
	                  		->where('Publisher.PublisherName' ,'=',$pubName)
	                  		//->select('HiddenMetrics.ApiMetricsName', 'HiddenMetrics.typeFlag')
	                      	->select('HiddenMetrics.typeFlag','HiddenMetrics.PublisherMetricsParameter', 'HiddenMetrics.ApiMetricsName')
	                      	->orderBy('HiddenMetrics.typeFlag','ASC')
	                      	->orderBy('HiddenMetrics.APIParamColumnName','ASC')
	                      	//->orderBy('HiddenMetrics.PublisherMetricsParameter', 'ASC')
	                      	//->orderBy('HiddenMetrics.ApiMetricsName', 'ASC')
	                      	->get();
	    	//$hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);
	    	//echo"hiddenMetricsID_Out "; print_r($hiddenMetricsID_Out); //exit;

	        array_push($metricesListPerReportType,'Date');              	
	    	foreach($hiddenMetricsID_Out as $key=>$value) {
				if($value->typeFlag==2){
			    	array_push($arrayMetricDB,$value->ApiMetricsName);
			    	$arrayMetricDB=	array_filter($arrayMetricDB); 
				}
				array_push($metricesListPerReportType,$value->PublisherMetricsParameter);
			}
			//echo"arrayMetricDB "; print_r($arrayMetricDB); 
			//echo"metricesListPerReportType "; print_r($metricesListPerReportType); 
			//exit;
			// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		  	//===========get breakDown, parameter, metrices========================================== 
		  	// ====================================================================================
			$accountManagerIDsData =  DB :: table('AdWords_accountManagerIDs')
			 							->get(); 
			$accountManagerIDsData = json_decode($accountManagerIDsData,true);
			// ====================================================================================
			//echo "<pre>data"; print_r($accountManagerIDsData); exit;
	    	foreach ($accountManagerIDsData as $accountManagerID ) {
	    		//echo "<pre>"; print_r($accountManagerID['clientCustomerId']); exit;
	    	 	//$file = Storage::put( 'myfile.ini', $accountManagerID);
		    	$accountClientcustomerid=$accountManagerID['clientCustomerId'];		
		    	//echo "<pre>"; print_r($accountClientcustomerid); exit;
		    	$dataAccountManagerID='
				[ADWORDS]
				; Required AdWords API properties. Details can be found at:
				; https://developers.google.com/adwords/api/docs/guides/basic-concepts#soap_and_xml
				;NEON Details
				developerToken = "7S0zFtOqIXJVP3ZqUtRYtg"

				;manager Id
				clientCustomerId ='.$accountClientcustomerid.'
			
				[OAUTH2]
				; NEON Details
				clientId = "666733807117-srfc7e5h1mnejcvrkdf1f73cu50cfhhi.apps.googleusercontent.com"
				clientSecret = "nNM1pGWZbBQgqWPaSa5KlRYQ"
				refreshToken = "1/_2Hjz77sQVIvPEqbt-RoLoL2VX5tOH0Ez3Eoc8DfMNI" 

				[SOAP]
				; Optional SOAP settings. See SoapSettingsBuilder.php for more information.
				; compressionLevel = <COMPRESSION_LEVEL>

				[CONNECTION]
				; Optional proxy settings to be used by requests.
				; If you dont have username and password, just specify host and port.
				; proxy = "protocol://user:pass@host:port"
				; Enable transparent HTTP gzip compression for all reporting requests.
				; enableReportingGzip = false

				[LOGGING]
				; Optional logging settings.
				; soapLogFilePath = "path/to/your/soap.log"
				; soapLogLevel = "INFO"
				; reportDownloaderLogFilePath = "path/to/your/report-downloader.log"
				; reportDownloaderLogLevel = "INFO"
				; batchJobsUtilLogFilePath = "path/to/your/bjutil.log"
				; batchJobsUtilLogLevel = "INFO" ';
		
	    	  	$file = Storage::disk('public_adwords')->put('adsapi_php.ini', $dataAccountManagerID);

	    		//--------- download multiple report for a manager Acoount-------------------------------
		    	$url = new ParallelReportDownload();
		    	$customerIds=$url->mainMultipleReports($startDate, $endDate,$metricesListPerReportType,$pubName,$ReportType);
		    	//echo "<pre>API customerIds "; print_r($customerIds); exit; 

		    	//--------- download multiple report for a manager Acoount-------------------------------    	
		    	//echo"<pre>for "; print_r($pubName); exit;
		    	foreach ($customerIds as $customerId ) {
		    		$adReportfilePath = public_path('reports\adWordReports\adwords\\'.$ReportType.'_'.$pubName.'_'.$customerId.'.csv');

		    		//echo "<pre>adReportfilePath "; print_r($adReportfilePath); exit; 

		    		if (file_exists($adReportfilePath)) {
					    //echo "The file exists";
					    config(['excel.import.startRow' => 2 ]);
			        	$result = \Excel::load($adReportfilePath)->get()->toArray();
			        	$response =AdamAPIController::storeApivalue($result,$arrayMetricDB,$APISource_ID,$pubName);
			    		//echo"<pre>"; print_r($response); //exit; 
			     		$new_path=public_path('reports\adWordReports\adwords\adProcessedReport\P_'.$ReportType.'_'.$accountClientcustomerid.'_'.$pubName.'_'.$customerId.'_'.$current_time.'.csv');	
						$move= File::move($adReportfilePath, $new_path);
					} 
					else {
					    echo "The file does not exist";
					}
		    	}
		    	//------------------insert end apiHit Log----------------------------------------------
				$current_timetoEnd = Carbon::now()->toDateTimeString();	
				$APIPrcoessingLog_ID=AdamAPIController::UpdateAPIPrcoessingLog($APIPrcoessingLog_ID, $current_timetoEnd);	
				//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
				//------------------End insert end apiHit Log------------------------------------------

	    	}
	    	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		}
		return response()->json(['status' => 'success'], 200);  
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	}
	//###########Adwords###############################################################################

	//###########Sizmek################################################################################

	public function testAdamSizMek(Request $request=Null)
	{	
		//ini_set('MAX_EXECUTION_TIME', '-1');
    	ini_set('memory_limit', '-1');
    	set_time_limit(0);

     	if($request!=null)
     	{
     		$_SESSION['startDate'] = $request['StartDate'];  
     		$_SESSION['endDate']  = $request['endDate'];
     		// ------------------------------------------------------------
     		$startDate = $request['StartDate'];  
     		$tillDate  = $request['endDate'];
     		$UniqueReportId=$request['UniqueReportId'];
     		$IsCron    = 0;
     		// echo"<pre>1: "; print_r($startDate);
     		// echo"<pre>1: "; print_r($tillDate);
     		//  exit;
     	} 
     	else 
     	{	
     		// yesterday's date
     		$_SESSION['startDate'] = date('Y-m-d',strtotime("-1 days")); 
     		$_SESSION['endDate']  = date('Y-m-d',strtotime("-1 days"));
     		// ------------------------------------------------------------
     		$startDate = date('Y-m-d',strtotime("-1 days")); 
     		$tillDate  = date('Y-m-d',strtotime("-1 days"));
     		$UniqueReportId=Null;
     		$IsCron    = 1;
     		//echo"<pre>2: "; print_r($startDate); exit;
     	}
     	// -----------------------------------------------------------------------
     	// get Report Type
        $SizmekReportType =  DB :: table('APISource')
                          ->join('Publisher','Publisher.id', '=', 'APISource.Publisher_ID')
                          ->where('Publisher.PublisherName','=','Sizmek')
                          ->select('APISource.APIName','APISource.id')
                          ->where('APISource.IsActive','=', 1)
                          ->get();
        $SizmekReportType = json_decode($SizmekReportType,true);
       	//echo"SizmekReportType"; print_r($SizmekReportType); exit;	
		
       	// --------------------------------------------------
       	foreach ($SizmekReportType as $SizmekReportTypeEach) {	
       		$sizmekReportType=$SizmekReportTypeEach['APIName'];
			$APISource_ID=$SizmekReportTypeEach['id'];
       		//print_r($APISource_ID); 
       		//echo"SizmekReportType"; print_r($sizmekReportType); //exit;

       		//------------------insert start apiHit Log----------------------------------------------
			$current_timetoStart = Carbon::now()->toDateTimeString();
			$APIPrcoessingLog_ID=AdamAPIController::APIPrcoessingLog($APISource_ID,$sizmekReportType,'Sizmek',$UniqueReportId,$startDate,$tillDate, $current_timetoStart,$IsCron);	
			//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
			//------------------End insert start apiHit Log------------------------------------------

       		// ######################################################################################
			$hiddenMetricsID_Out = DB :: table('HiddenMetrics')  
					->join('Publisher',	 'HiddenMetrics.Publisher_ID',		'=',   'Publisher.id')
					->join('APIMetrics', 'APIMetrics.HiddenMetrics_ID',	'=',   'HiddenMetrics.id')  
					->join('APISource',	 'APIMetrics.APISource_ID',		'=',   'APISource.id') 		
	                ->where('APISource.APIName' ,'=',$sizmekReportType)
              		->where('Publisher.PublisherName' ,'=','Sizmek')
              		->select('HiddenMetrics.ApiMetricsName', 'HiddenMetrics.PublisherMetricsParameter', 'HiddenMetrics.typeFlag', 'HiddenMetrics.APIParamColumnName')
              		->orderBy('HiddenMetrics.typeFlag','ASC')
	                ->orderBy('HiddenMetrics.APIParamColumnName','ASC')
	               	->get();
			//echo"hiddenMetricsID_Result"; print_r($hiddenMetricsID_Out); //exit;
			$arrayMetricDB=[];
			$sizmekReportType=[];
			foreach($hiddenMetricsID_Out as $key=>$value) {
				if($value->typeFlag==2){
			    	array_push($arrayMetricDB,$value->ApiMetricsName);
			    	$arrayMetricDB=	array_filter($arrayMetricDB);    	
				}
			}
			//echo"arrayMetricDBINSizmek "; print_r($arrayMetricDB); exit;
			// ######################################################################################
			// **************************************************************************************
			// ------------ Files From D drive--------------	
	       	$filesFromDrive=Storage::disk('partitionE')->allFiles();
	       	//echo"<pre>fileD ";  print_r($filesFromDrive); //exit;
	       	
	       	if($filesFromDrive){
	       		//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		       	foreach($filesFromDrive as $filesFromDriveName) 
			    { 
			    	//---zipped file put from D Drive to public directory
			    	$contents=Storage::disk('partitionE')->get($filesFromDriveName);
			    			  Storage::disk('public_uploads')->put( $filesFromDriveName, $contents);
			    			  Storage::disk('partitionE')->delete( $filesFromDriveName);  
			    } 	
			    //exit;
			    // ------- Files from Zipped Files----------------
				$filesInzipedSizmek = \File::files(public_path('/reports/Sizmek/zipedSizmek'));     
		   		//echo"<pre>";  print_r($filesInzipedSizmek); exit;
		    	foreach($filesInzipedSizmek as $zipFilePath) 
		    	{ 
					// -----------------------------------------------
					//$zipFilePath = public_path('/reports/Sizmek/zipedSizmek/Sizmek_IA_Summary_Feed_20181203_20181203.csv.zip');		
			        $extratFilename = \Zipper::make($zipFilePath)->listFiles('/\.csv$/i'); 		
			        $extratFilename = $extratFilename[0];	        
			        //echo"<pre>";  print_r($extratFilename); exit;
			        \Zipper::close($zipFilePath);
			        
			        \Zipper::make($zipFilePath)->extractTo(public_path('/reports/Sizmek/sizmekReports'));
			        \Zipper::close($zipFilePath);

			        $new_pathZip=public_path('/reports/Sizmek/ProcessedZippedSizmek/'.$extratFilename.'.zip');	
					$move= File::move($zipFilePath, $new_pathZip);

			        $extractedFilePath = public_path('/reports/Sizmek/sizmekReports/'.$extratFilename);
					// -----------------------------------------------------------------------
					//$extractedFilePath = public_path('reports\Sizmek\sizmekReports\Sizmek_IA_Summary_Feed_20190112_20190112.csv');
					//echo"<pre>";  print_r($extractedFilePath); exit;
					
					config(['excel.import.startRow' => 1]);
					$result = \Excel::load($extractedFilePath)->get()->toArray();           
					//echo"<pre>data ";  print_r($result); exit;

					//$response = ApiController:: storeSizmekAndGA_DBM__AdWordY_ApiValue($data,'Sizmek');
					$response =AdamAPIController::storeApivalue($result,$arrayMetricDB,$APISource_ID,'Sizmek');

					// ====After Processing move================================================
					$new_path=public_path('/reports/Sizmek/ProcessedSizmek/ p_'.$extratFilename);	
					$move= File::move($extractedFilePath, $new_path);
					// ====End After Processing move=============================================
					
				}//end foreach	
				// **************************************************************************************
				//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			}
			else{
				//print_r("Nofile");
			}

			//------------------insert end apiHit Log----------------------------------------------
			$current_timetoEnd = Carbon::now()->toDateTimeString();	
			$APIPrcoessingLog_ID=AdamAPIController::UpdateAPIPrcoessingLog($APIPrcoessingLog_ID, $current_timetoEnd);	
			//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
			//------------------End insert end apiHit Log------------------------------------------	
       	}	
       	// -------------------------------------------------
    }

	//###########Sizmek################################################################################

	//###########BingAds################################################################################
	public function adamBingAdsApi(Request $request=Null)
	{	
		//echo"<pre>"; print_r($request->all()); exit;
		File::delete('reports/keywordperf.zip');
     	//$tillDate = date('Y-m-d');

     	if($request!=null)
     	{
     		$startDate = array('start_year'=> $request['start_year'],'start_month'=>$request['start_month'],'start_day'=>$request['start_date']);
     		$tillDate  = array('end_year'=> $request['end_year'],'end_month'=>$request['end_month'],'end_day'=>$request['end_date']);
     		$UniqueReportId=$request['UniqueReportId'];
     		$IsCron    = 0;
     	} else {
     		
     		$startDate = date('Y-m-d',strtotime("-1 days")); 
     		$tillDate  = date('Y-m-d',strtotime("-1 days"));
     		$UniqueReportId=NULL;
     		$IsCron    = 1;
     		//echo"<pre>2: "; print_r($startDate); exit;
     	}

     	echo"<pre>1: "; print_r($startDate); 
     	echo"<pre>2: "; print_r($tillDate); exit;

     	if($request['id']==1) {
	   			AuthHelper::Authenticate();

    			$GLOBALS['ReportingProxy'] = new ServiceClient(ServiceClientType::ReportingVersion12, $GLOBALS['AuthorizationData'], AuthHelper::GetApiEnvironment());

 				$report = ReportRequestLibrary::GetKeywordPerformanceReportRequest('1306881',$startDate);
				$this->extractKeyZip();
		}	
    }

    //---------------------extrat zip file of BingsAdv------------------------------------------
    public function extractKeyZip()
    {

        $Path = public_path('/reports/keywordperf.zip');

        $extratFilename = \Zipper::make($Path)->listFiles('/\.csv$/i');
        $extratFilename  = substr($extratFilename[0], 0, -4);
		$filename = $extratFilename.'_'.date('ymdhms').'.csv';

		$filePath = public_path('/unzipReport/'.$extratFilename[0]);
        \Zipper::make($Path)->extractTo('unzipReport');
        $directory = public_path('unzipReport');
        $moveToDir = public_path('/reports/unZipReport');

		$i=1;
		if ($handle = opendir($directory)) {
		    while (true == ($file = readdir($handle))) {
		            if($file=="." || $file==".." || $file==$extratFilename[0]) { continue;}
			    $replace = $filename ;
			    File::copy($directory.'/'.$file,$moveToDir.'/'.$replace);
			    $i++;
		    }	//end while
		    closedir($handle);
		  }  //end handle

		$pathFile = 'public/reports/unZipReport/'.$filename;
		config(['excel.import.startRow' => 11 ]);

        $data = \Excel::load($pathFile)->get()->toArray();

		//return $this->storeApivalue($data,'Bing');

    }
	//###########BingAds################################################################################

	//###########GoogleAnalytics################################################################################
	public function adamGoogleAnalytics(Request $request=Null)
	{
		//echo"<pre>";  print_r($request->all()); exit;
		//$GAObj= new GoogleAnalyticsReportProcessing;
		//$exedbm=$GAObj->indexGA();				
		//=============================================================================================== 
		if($request!=null)
     	{
     		$startDate = $request['StartDate'];  
     		$tillDate  = $request['endDate'];
     		$IsCron    = 0;
     		echo"<pre>startDate1: "; print_r($startDate);
     		echo"<pre>tillDate1: "; print_r($tillDate); //exit;
     	} 
     	else 
     	{	
     		// yesterday's date
     		$startDate = date('Y-m-d',strtotime("-1 days"));; 
     		$tillDate  = date('Y-m-d',strtotime("-1 days"));;
     		$IsCron    = 1;	
     		echo"<pre>startDate2: "; print_r( $startDate);
     		echo"<pre>tillDate2: "; print_r( $tillDate); //exit;
     	}
     	// ==========================================================
		$getGAReportId=DB :: table('APISource')
		  					->join('Publisher','APISource.Publisher_ID','=','Publisher.id')  
							->where('Publisher.PublisherName' ,'=', 'GoogleAnalytics')//'AgeAndGender'
							->where('APISource.IsActive' ,'=', '1')
							->select('APISource.APIName', 'APISource.id as APISource_ID', 'Publisher.id as Publisher_ID' )
							->get();
		$getGAReportId = json_decode($getGAReportId,true);	
		$APISource_ID=$getGAReportId[0]['APISource_ID'];
		$APIName=$getGAReportId[0]['APIName'];
		//dd($APIName);				
		//------------------insert start apiHit Log----------------------------------------------
		$current_timetoStart = Carbon::now()->toDateTimeString();
		$APIPrcoessingLog_ID=AdamAPIController::APIPrcoessingLog($APISource_ID,$APIName,'GoogleAnalytics',$startDate,$tillDate, $current_timetoStart,$IsCron);	
		//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
		//------------------End insert start apiHit Log------------------------------------------
		$arrayMetricDB=[];
		foreach ($getGAReportId as $getGAReportIdeach ) {
			//dd($getGAReportIdeach);
			$hiddenMetricsID_Out = DB :: table('HiddenMetrics')  
					->join('Publisher',	 'HiddenMetrics.Publisher_ID',		'=',   'Publisher.id')
					->join('APIMetrics', 'APIMetrics.HiddenMetrics_ID',	'=',   'HiddenMetrics.id')  
					->join('APISource',	 'APIMetrics.APISource_ID',		'=',   'APISource.id') 		
	                ->where('APISource.APIName' ,'=',$getGAReportIdeach['APIName'])
              		->where('Publisher.PublisherName' ,'=','GoogleAnalytics')
              		->select('APISource.id as APISource_ID','HiddenMetrics.ApiMetricsName', 'HiddenMetrics.PublisherMetricsParameter', 'HiddenMetrics.typeFlag', 'HiddenMetrics.APIParamColumnName')
	               	->get();
			//echo"hiddenMetricsID_Result"; print_r($hiddenMetricsID_Out); exit;
			
			foreach($hiddenMetricsID_Out as $key=>$value) {
				if($value->typeFlag==2){
			    	array_push($arrayMetricDB,$value->ApiMetricsName);
			    	$arrayMetricDB=	array_filter($arrayMetricDB);    	
				}
			}
		}
		echo"APISource_ID"; print_r($APISource_ID);	
		echo"arrayMetricDB"; print_r($arrayMetricDB);
		//echo"hiddenMetricsID_Result"; print_r($hiddenMetricsID_Out);	
		//exit;
		//=============================================================================================== 

		$analytics = $this->initializeAnalytics();
		$viewId = $this->getFirstProfileId($analytics);	
		//echo"in initializeAnalytics"; print_r($viewId); exit; 		
		// -------------------------------------------------------
		$client = new Google_Client();
	    $upTwo = dirname(__DIR__, 2);
		$client->setAuthConfig( $upTwo. '/GA/client_secrets.json');
	    $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);

	    if (isset($_SESSION['access_token']) && $_SESSION['access_token']) 
	    {
	    	$client->setAccessToken($_SESSION['access_token']);
	      	$analytics = new Google_Service_AnalyticsReporting($client);

	      	foreach ($viewId as $key => $viewIdValue) {
	        	$response = $this->getReport($analytics, $viewIdValue);
	        	$gaReport=$this->printResultsReport($response, $viewIdValue );
	        
	        	$serializedData = json_encode($gaReport);
	        	file_put_contents('GAReport_json_encode_'.$viewIdValue.'.json', $serializedData);
	        	$recoveredData = file_get_contents('GAReport_json_encode_'.$viewIdValue.'.json');

	        	AdamAPIController::processGAReport($viewIdValue,$arrayMetricDB,$APISource_ID,$APIPrcoessingLog_ID);
	        
	      	}  
	      	echo "<pre><h1>Report For GoogleAnalytics is Processed...!</h1>";
			echo"<a href='api' class='btn btn-info pull-right'>BACK</a>";
	    } 
	    else {
	      	return redirect()->to('/oauth2CallBackGoogleAnalytics');
	    }

	}


	public function initializeAnalytics()
	{
	  
	  ///echo"in initializeAnalytics"; exit; 
	  $upTwo = dirname(__DIR__, 2);
	  $KEY_FILE_LOCATION = $upTwo. '/GA/service-account-credentials.json';
	  $client = new Google_Client();
	  $client->setApplicationName("Hello Analytics Reporting");
	  $client->setAuthConfig($KEY_FILE_LOCATION);
	  $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	  $analytics = new Google_Service_Analytics($client);

	  return $analytics;
	}

	function getFirstProfileId($analytics) 
	{
	  //global $viewId;
	  $viewId=[];

	  //dd("in proID");
	  // Get the list of accounts for the authorized user.
	  $accounts = $analytics->management_accounts->listManagementAccounts();
	  if (count($accounts->getItems()) > 0) {
	    $items = $accounts->getItems();
	    for ( $rowIndex = 0; $rowIndex < count($items); $rowIndex++) {
	      $accountId = $items[$rowIndex]->getId();
	      $properties = $analytics->management_webproperties
	          ->listManagementWebproperties($accountId);

	      if (count($properties->getItems()) > 0) {
	        $items1 = $properties->getItems();

	        for ( $rowIndex1 = 0; $rowIndex1 < count($items1); $rowIndex1++) {
	          $propertyId = $items1[$rowIndex1]->getId();

	          $profiles = $analytics->management_profiles
	              ->listManagementProfiles($accountId, $propertyId);

	          if (count($profiles->getItems()) > 0) {
	            $items2 = $profiles->getItems();
	            for ( $rowIndex2 = 0; $rowIndex2 < count($items2); $rowIndex2++) {
	              array_push($viewId, $items2[$rowIndex2]->getId());
	              //
	            }
	          } 
	          else 
	          {
	            throw new Exception('No views (profiles) found for this user.');
	          }
	        }
	      } 
	      else 
	      {
	        throw new Exception('No properties found for this user.');
	      }
	    }
	  } 
	  else 
	  {
	    throw new Exception('No accounts found for this user.');
	  }
	  return $viewId; 
	}
	public function oauth2CallBackGoogleAnalytics()
    {
   		//echo"Auth";exit;
   		session_start();
		$client = new Google_Client();
		$upTwo = dirname(__DIR__, 2);
		$client->setAuthConfig( $upTwo. '/GA/client_secrets.json');

		$url = dirname(__FILE__);
		$array = explode('\\',$url);
		$count = count($array);
		
		//$authCallUrl='http://' . $_SERVER['HTTP_HOST'] . '/IADC/public/oauth2CallBackGA';
		$authCallUrl='http://' . $_SERVER['HTTP_HOST'] . '/oauth2CallBackGoogleAnalytics';
		$client->setRedirectUri($authCallUrl);
		$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);

		// Handle authorization flow from the server.
		if (! isset($_GET['code'])) {
			echo"if"; //exit;
		  	$auth_url = $client->createAuthUrl();
		  	
		  	header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL)); exit;
		} 
		else {
			//dd("else"); //exit;
		  $client->authenticate($_GET['code']);
		  $_SESSION['access_token'] = $client->getAccessToken();
		  $this->adamGoogleAnalytics();
		}
    }
	Public function getReport($analytics, $viewIdValue) {

		//echo"getReport"; exit;
		$VIEW_ID = $viewIdValue;
	    $dateRange = new Google_Service_AnalyticsReporting_DateRange();
	    //$dateRange->setStartDate("7daysAgo");
	    $dateRange->setStartDate("3daysAgo");//
	    $dateRange->setEndDate("today");

	    // Create the Metrics object.
	    $sessions = new Google_Service_AnalyticsReporting_Metric();
	    $sessions->setExpression("ga:sessions");
	    $sessions->setAlias("sessions");

	    $avgSessionDuration = new Google_Service_AnalyticsReporting_Metric();
	    $avgSessionDuration->setExpression("ga:avgSessionDuration");
	    $avgSessionDuration->setAlias("avgSessionDuration");

	    $bounceRate = new Google_Service_AnalyticsReporting_Metric();
	    $bounceRate->setExpression("ga:bounceRate");
	    $bounceRate->setAlias("bounceRate");

	    $users = new Google_Service_AnalyticsReporting_Metric();
	    $users->setExpression("ga:users");
	    $users->setAlias("users");

	    $pageviews = new Google_Service_AnalyticsReporting_Metric();
	    $pageviews->setExpression("ga:pageviews");
	    $pageviews->setAlias("pageviews");

	    $adClicks = new Google_Service_AnalyticsReporting_Metric();
	    $adClicks->setExpression("ga:adClicks");
	    $adClicks->setAlias("adClicks");

	    $adCost = new Google_Service_AnalyticsReporting_Metric();
	    $adCost->setExpression("ga:adCost");
	    $adCost->setAlias("adCost");

	    $impressions = new Google_Service_AnalyticsReporting_Metric();
	    $impressions->setExpression("ga:impressions");
	    $impressions->setAlias("impressions");

	    $timeOnPage = new Google_Service_AnalyticsReporting_Metric();
	    $timeOnPage->setExpression("ga:timeOnPage");
	    $timeOnPage->setAlias("timeOnPage");

	    $goalCompletionsAll = new Google_Service_AnalyticsReporting_Metric();
	    $goalCompletionsAll->setExpression("ga:goalCompletionsAll");
	    $goalCompletionsAll->setAlias("goalCompletionsAll");

	    //Add dimensions. Only medium can be considered as it's mapped to the line item
	    $date = new Google_Service_AnalyticsReporting_Dimension();
	    $date->setName("ga:date");

	    $medium = new Google_Service_AnalyticsReporting_Dimension();
	    $medium->setName("ga:medium");

	    // Create the ReportRequest object.
	    $request = new Google_Service_AnalyticsReporting_ReportRequest();
	    $request->setViewId($VIEW_ID);
	    $request->setDateRanges($dateRange);
	    $request->setMetrics(array($sessions, $avgSessionDuration, $bounceRate, $users, $pageviews, $adClicks, $adCost, $impressions, $timeOnPage, $goalCompletionsAll));
	    //  $request->setMetrics(array($pageviews));
	    $request->setDimensions(array($date, $medium));
	    //  $request->setMetrics(array($sessions, $avgSessionDuration, $bounceRate, $users));

	    $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
	    $body->setReportRequests( array( $request) );
	    return $analytics->reports->batchGet( $body );
	}

	
	Public function printResultsReport($reports, $viewIdValue) {
		//echo"printResultsReport"; exit;

	    for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
	      
		    //echo"<pre>count "; print_r( count($reports) );// exit;
		    $report = $reports[ $reportIndex ];
		    $header = $report->getColumnHeader();
	      	$dimensionHeaders = $header->getDimensions();
	        $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
	      	$rows = $report->getData()->getRows();
	       	//echo"<pre> rowCount for each : "; print_r(count($rows)); //exit;
	      	$reportArray=[]; 
	      	$dimentionArr=[];
	      	$metricsArr=[];
	      
	      	for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
	        	//echo"<pre> rowCount "; print_r(count($rows)); //exit;
		        $row = $rows[ $rowIndex ];
		        $dimensions = $row->getDimensions();
		        $metrics = $row->getMetrics();
		        
		        $dimention=[];
		        $metricess=[];

		        for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
		          // echo "<br>---------------------------";
		          // print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
		          $dimention[$dimensionHeaders[$i]]=$dimensions[$i];
		        }
	        	array_push($dimentionArr, $dimention);

		        for ($j = 0; $j < count($metrics); $j++) {
		          $values = $metrics[$j]->getValues();
		          for ($k = 0; $k < count($values); $k++) {
		            $entry = $metricHeaders[$k];
		            // echo "<br>.......................";
		            // print($entry->getName() . ": " . $values[$k] . "\n");
		            $metricess[$entry->getName()]=$values[$k];        
		          }
		          array_push($metricsArr, $metricess);
		        
		        }
		        foreach($dimentionArr as $key=>$val){
		            $val2 = $metricsArr[$key]; 
		            $reportArray[$rowIndex] = $val + $val2; // combine 'em
		        }
		        //echo"<pre> return reportArray "; print_r($reportArray); //exit;
	      	}
	        //echo"<pre> return reportArray forOut"; print_r($reportArray); //exit;
	  	    return $reportArray;
	    }
	}
	Public static function processGAReport($viewIdValue,$arrayMetricDB,$APISource_ID,$APIPrcoessingLog_ID)
	{
		
		// ====================================================================================================
		$currentdate=Carbon::now()->toDateString();
		$gaFilePublicPath=public_path('GAReport_json_encode_'.$viewIdValue.'.json');
		$dataGAFromPublic = file_get_contents($gaFilePublicPath);
		$GAarray = json_decode($dataGAFromPublic, true);
		//echo"<pre>GA result: "; print_r($dataGAFromPublic); 
		// -------------process file----------------- 		
		// this method made static refer it while worling on GA
	    $response =AdamAPIController::storeApivalue($dataGAFromPublic,$arrayMetricDB,$APISource_ID,'GoogleAnalytics');
	    // -------------process file-----------------
	    // -------------move file in DBMReport Folder-----------------
	    $new_path=public_path('/reports/GA_Report/Processed_GA_Report_'.$currentdate.'_'.$viewIdValue.'.json');
       	$move= File::move($gaFilePublicPath, $new_path);
	    // -------------move file in DBMReport Folder-----------------
	    // ====================================================================================================
	    //------------------insert end apiHit Log----------------------------------------------
		$current_timetoEnd = Carbon::now()->toDateTimeString();	
		$APIPrcoessingLog_ID=AdamAPIController::UpdateAPIPrcoessingLog($APIPrcoessingLog_ID, $current_timetoEnd);	
		//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
		//------------------End insert end apiHit Log------------------------------------------

	}
	//###########GoogleAnalytics################################################################################

	//###########Taboola########################################################################################
	public function AdamTaboola(Request $request=Null)
    {
    	//echo"<pre>"; print_r($request->all()); exit;	
	    //=============================================================================================== 
		if($request!=null)
     	{
     		$startDate = $request['StartDate'];  
     		$tillDate  = $request['endDate'];
     		$UniqueReportId=$request['UniqueReportId'];
     		$IsCron    = 0;
     		echo"<pre>startDate1: "; print_r($startDate);
     		echo"<pre>tillDate1: "; print_r($tillDate); //exit;
     	} 
     	else 
     	{	
     		// yesterday's date
     		$startDate = date('Y-m-d',strtotime("-1 days")); 
     		$tillDate  = date('Y-m-d',strtotime("-1 days"));
     		$UniqueReportId=Null;
     		$IsCron    = 1;	
     		echo"<pre>startDate2: "; print_r( $startDate);
     		echo"<pre>tillDate2: "; print_r( $tillDate); //exit;
     	}
     	// ==========================================================
     	$arrayMetricDB=[];
     	$getAPISource_ID = DB :: table('APISource')  
							->join('Publisher',	 'APISource.publisher_ID',		'=',   'Publisher.id') 	
							->select('APISource.id', 'APISource.APIName')
							->where('APISource.IsActive' ,'=',1)
							->where('Publisher.PublisherName','=','Taboola')
							->first();
     	$APISource_ID=$getAPISource_ID->id;
     	$ApiName=$getAPISource_ID->APIName;
     	//echo"APISource_ID"; print_r($APISource_ID); //exit;
     	//------------------insert start apiHit Log----------------------------------------------
		$current_time = Carbon::now()->toDateTimeString();
		$APIPrcoessingLog_ID=$this->APIPrcoessingLog($APISource_ID,$ApiName,'Taboola',$UniqueReportId,$startDate,$tillDate, $current_time,$IsCron);	
		//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
		//------------------End insert start apiHit Log------------------------------------------
     	$hiddenMetricsID_Out = DB :: table('HiddenMetrics')  
     						->join('Publisher',	 'HiddenMetrics.publisher_ID',		'=',   'Publisher.id') 	
							->join('APIMetrics', 'APIMetrics.HiddenMetrics_ID',	'=',   'HiddenMetrics.id')                   
	                  		->join('APISource',	 'APIMetrics.APISource_ID',		'=',   'APISource.id') 		
	                  		->where('APISource.APIName' ,'=',$getAPISource_ID->APIName)//'AgeAndGender'
	                  		->where('Publisher.PublisherName' ,'=','Taboola')
	                  		->select('HiddenMetrics.ApiMetricsName', 'HiddenMetrics.typeFlag')
	                      	->get();
		$hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);
		//echo"api"; print_r($hiddenMetricsID_Result); //exit;
	    foreach($hiddenMetricsID_Result as $key=>$value) {
	    	//echo"<pre>"; print_r( $key ); print_r( $value['ApiMetricsName'] ); exit; 
	        if($value['typeFlag']==2){
	        	array_push($arrayMetricDB,$value['ApiMetricsName']);
	    	}
	    }
	    echo"<pre>"; print_r($arrayMetricDB); //exit; 
     	// ==========================================================
    	$url = 'https://backstage.taboola.com/backstage/oauth/token?client_id=fc8f6c5e3c7d444d8f09eee0144f0a64&client_secret=a56a12861c18428c8b62e6da1de6300f&username=gaanalytics.ia@gmail.com&password=IAnalytics@18&grant_type=client_credentials';
	    // Get cURL resource
	    $curl = curl_init();
	    // Set some options
	    curl_setopt_array($curl, array(
	        CURLOPT_RETURNTRANSFER => 1,
	        CURLOPT_URL => $url,
	        CURLOPT_POST => 1
	    ));
	    // Send the request & save response to $resp
	    $resp = curl_exec($curl);
	    //echo'<pre>cUrl'; print_r($resp); exit;

	    // Close request to clear up some resources
	    curl_close($curl);
	    if($resp){
	        $arr = array(json_decode($resp));
	        $access_token = $arr[0]->access_token;
        
	        $url = "https://backstage.taboola.com/backstage/api/1.0/interactiveavenues-network/reports/campaign-summary/dimensions/campaign_day_breakdown?start_date=".$startDate."&end_date=".$tillDate;

	        $ch = curl_init();

	        curl_setopt_array($ch, array(
	        CURLOPT_RETURNTRANSFER => 1,
	        CURLOPT_URL => $url,
	        CURLOPT_CUSTOMREQUEST => 'GET',
	        CURLOPT_HTTPHEADER => array('Content-Type:application/json',
	                      				'Authorization: Bearer '.$access_token)
	        ));

	        $resps = curl_exec($ch);

	        // Close request to clear up some resources
	        curl_close($ch);
	        if($resps){
	            $resultOfTaboola = json_decode($resps,true);
	            //echo'<pre>res'; print_r($resultOfTaboola['results']); exit;
	            $response =AdamAPIController::storeApivalue($resultOfTaboola['results'],$arrayMetricDB,$APISource_ID,'Taboola');

	            //------------------insert end apiHit Log----------------------------------------------
				$current_timetoEnd = Carbon::now()->toDateTimeString();	
				$APIPrcoessingLog_ID=$this->UpdateAPIPrcoessingLog($APIPrcoessingLog_ID, $current_timetoEnd);	
				//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
				//------------------End insert end apiHit Log------------------------------------------
				
	        } else {
	         echo "failed";
	        }

	    } else {
	     	echo "failed";
	    }

    }	
	//###########End Taboola####################################################################################

	//###########Outbrain#######################################################################################
	public function AdamOutbrain(Request $request=Null)
    {
		
    	//=============================================================================================== 
		if($request!=null)
     	{
     		$startDate = $request['StartDate'];  
     		$tillDate  = $request['endDate'];
     		$UniqueReportId=$request['UniqueReportId'];
     		$IsCron    = 0;
     		echo"<pre>startDate1: "; print_r($startDate);
     		echo"<pre>tillDate1: "; print_r($tillDate); //exit;
     	} 
     	else 
     	{	
     		// yesterday's date
     		$startDate = date('Y-m-d',strtotime("-1 days")); 
     		$tillDate  = date('Y-m-d',strtotime("-1 days"));
     		$UniqueReportId=Null;
     		$IsCron    = 1;	
     		echo"<pre>startDate2: "; print_r( $startDate);
     		echo"<pre>tillDate2: "; print_r( $tillDate); //exit;
     	}
     	// ==========================================================
     	$arrayMetricDB=[];
     	$getAPISource_ID = DB :: table('APISource')  
							->join('Publisher',	 'APISource.publisher_ID',		'=',   'Publisher.id') 	
							->select('APISource.id', 'APISource.APIName')
							->where('APISource.IsActive' ,'=',1)
							->where('Publisher.PublisherName','=','Outbrain')
							->first();
     	$APISource_ID=$getAPISource_ID->id;
     	$ApiName=$getAPISource_ID->APIName;
     	//echo"APISource_ID"; print_r($APISource_ID); //exit;
     	//------------------insert start apiHit Log----------------------------------------------
		$current_time = Carbon::now()->toDateTimeString();
		$APIPrcoessingLog_ID=$this->APIPrcoessingLog($APISource_ID,$ApiName,'Outbrain',$UniqueReportId,$startDate,$tillDate, $current_time,$IsCron);	
		//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
		//------------------End insert start apiHit Log------------------------------------------
     	$hiddenMetricsID_Out = DB :: table('HiddenMetrics')  
     						->join('Publisher',	 'HiddenMetrics.publisher_ID',		'=',   'Publisher.id') 	
							->join('APIMetrics', 'APIMetrics.HiddenMetrics_ID',	'=',   'HiddenMetrics.id')                   
	                  		->join('APISource',	 'APIMetrics.APISource_ID',		'=',   'APISource.id') 		
	                  		->where('APISource.APIName' ,'=',$getAPISource_ID->APIName)//'AgeAndGender'
	                  		->where('Publisher.PublisherName' ,'=','Taboola')
	                  		->select('HiddenMetrics.ApiMetricsName', 'HiddenMetrics.typeFlag')
	                      	->get();
		$hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);
		//echo"api"; print_r($hiddenMetricsID_Result); //exit;
	    foreach($hiddenMetricsID_Result as $key=>$value) {
	    	//echo"<pre>"; print_r( $key ); print_r( $value['ApiMetricsName'] ); exit; 
	        if($value['typeFlag']==2){
	        	array_push($arrayMetricDB,$value['ApiMetricsName']);
	    	}
	    }
	    //echo"<pre>"; print_r($arrayMetricDB); exit; 
     	// ==========================================================
     	// ====================outbrain ApiHit=====================================================================
     	$limit=500;
		$url = "https://api.outbrain.com/amplify/v0.1/reports/marketers/003f64bfcd2829f3510f5d3e21787dab6e/campaigns/periodic?from=".$startDate."&to=".$tillDate."&limit=".$limit;
        $ch = curl_init();

        curl_setopt_array($ch, array(
        	CURLOPT_RETURNTRANSFER => 1,
        	CURLOPT_URL => $url,
        	CURLOPT_CUSTOMREQUEST => 'GET',
        	CURLOPT_HTTPHEADER => array('Content-Type:application/json',
                      //'Authorization: Basic bWFyc2hhbGwuZGF2aWRAY2FkcmVvbi5jb206SXZoYW5tdXNlXzA2')
                        'Authorization: Basic Z2FhbmFseXRpY3MuaWFAZ21haWwuY29tOklBbmFseXRpY3NAMTg=')
        ));

        $resps = curl_exec($ch);
        echo '<pre>123emp '; print_r($resps); //exit;
        curl_close($ch);
        if(empty($resps)){
            $result = json_decode($resps,true);
            //echo'<pre>'; print_r($result); exit;
            $data = [];
            foreach($result['campaignResults'] as $camp){
            	//echo'<pre>'; print_r($camp); exit;           		
				$url = "https://api.outbrain.com/amplify/v0.1/campaigns/".$camp['campaignId'];
                $ch = curl_init();

                curl_setopt_array($ch, array(
	                CURLOPT_RETURNTRANSFER => 1,
	                CURLOPT_URL => $url,
	                CURLOPT_CUSTOMREQUEST => 'GET',
	                CURLOPT_HTTPHEADER => array('Content-Type:application/json',
                              //'Authorization: Basic bWFyc2hhbGwuZGF2aWRAY2FkcmVvbi5jb206SXZoYW5tdXNlXzA2')
                			  'Authorization: Basic Z2FhbmFseXRpY3MuaWFAZ21haWwuY29tOklBbmFseXRpY3NAMTg=')	
                ));

                $campName = curl_exec($ch);
                curl_close($ch);
                if($campName){
                   $campName = json_decode($campName,true);
                }
                //echo'<pre>'; print_r($campName); exit;           
           		foreach($camp['results'] as $perDateResult){
           			//echo'<pre>'; print_r($perDateResult); //exit;
           			$data['campaignName'] = $campName['name'];
                	$data['campaignId'] = $camp['campaignId'];           			
           			$data['metadata'] = $perDateResult['metadata'];
           			
           			// ====================outbrain:campaignId campaignName  at 0 and 1st place is removed =======================================	
           			//array_push($data['metadata'],$data['campaignId']);
                    //array_push($data['metadata'],$data['campaignName']);
                    // ====================outbrain:campaignId campaignName  at 0 and 1st place is removed =======================================	
           			// ===========================================================
           			// ====================outbrain:campaignId campaignName  at 0 and 1st place is added =======================================	
           			$data['metadata']= array_slice($data['metadata'], 0, 3, true) +
					    array("campaignId" => $data['campaignId']) +array("campaignName" => $data['campaignName']) +
					    array_slice($data['metadata'], 3, count($data['metadata']) - 1, true) ;
					//print_r($data);
    				// ====================outbrain:campaignId campaignName  at 0 and 1st place is added =======================================	
           			// ===========================================================
           			$data['metrics'] = $perDateResult['metrics'];
           			$results[] = array_merge($data['metadata'],$data['metrics']);
           		}	
           		// ====================end outbrain ApiHit=====================================================================
           		//echo'<pre>'; print_r($results); exit;	
           	 
	           	echo'<pre>Outbrain: '; print_r($results); //exit;	
	           	if($results){            
		            AdamAPIController::storeApivalue($results,$arrayMetricDB,$APISource_ID,'Outbrain');
		            //------------------insert end apiHit Log----------------------------------------------
					$current_timetoEnd = Carbon::now()->toDateTimeString();	
					$APIPrcoessingLog_ID=$this->UpdateAPIPrcoessingLog($APIPrcoessingLog_ID, $current_timetoEnd);	
					//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
					//------------------End insert end apiHit Log------------------------------------------
		        } else {
		         echo "No Result For Campaign";
		        }
		    }    
        } 
        else {
         echo "No Campaign data";
        }
    }
	//###########End Outbrain###################################################################################

  	public static function storeApivalue($result,$arrayMetricDB,$APISource_ID,$publisher)
    {
		// echo"hello";
		// echo"<pre>result123: "; print_r($result); //exit;
		// echo"<br> "; print_r($arrayMetricDB);
		// echo"<br> "; print_r($APISource_ID);
	 //    echo"<br> "; print_r($publisher); exit;
    	
    	if(!is_array($result)){
    		//echo"json";
    		if($publisher=="GoogleAnalytics"){
    			$output = json_decode($result,true);
    			//echo"GA Data: "; print_r($output);
    		}
    		else{
	    		$data = json_decode($result,true);
				$output = $data['data'];
				//echo"<br>Inner Else : "; print_r($output); //exit;
			}	
    	}else{
    		//echo"array";
    		$output = $result;
    		//echo"<br>outer else: "; print_r($output); //exit;
    	} 	
    	//echo"<br>output: "; print_r($output); exit;
    	$PublisherID =  DB :: table('Publisher')
	 							->where('Publisher.PublisherName','=',$publisher)
	 							->select('Publisher.id')
	 							->first();
	 	$pub_id = $PublisherID->id;
		
		// step1 : set inially setArray=[];
		$adsetLineItemArray=[];
		$P3P5Array=[];
		$breakdownArray=[];
			
		foreach ($output as $eachRow) {
			echo"<pre>eachRow: "; print_r($eachRow);// exit;
			if($publisher=='Facebook'){
				$lineItem_id=$eachRow['adset_id'];
				$lineItem_name=$eachRow['adset_name'];
				$campaign_name=$eachRow['campaign_name'];
			}
			if($publisher=='DBM'){// mapped at insertion_order
				// $lineItem_id  = array_key_exists('line_item_id', $eachRow)  ?  $eachRow['line_item_id'] : NULL;  
				// $lineItem_name= array_key_exists('line_item', $eachRow)     ?  $eachRow['line_item']    : NULL;  
				$lineItem_id  = array_key_exists('insertion_order_id', $eachRow)  ?  $eachRow['insertion_order_id'] : NULL;  
				$lineItem_name= array_key_exists('insertion_order', $eachRow)     ?  $eachRow['insertion_order']    : NULL;  
				$campaign_name= array_key_exists('campaign', $eachRow) ?  $eachRow['campaign']: NULL;  
			}
			if($publisher=='DCM'){
				$lineItem_id  = array_key_exists('placement_id', $eachRow)  ?  $eachRow['placement_id'] : NULL;  
				$lineItem_name= array_key_exists('placement', $eachRow)     ?  $eachRow['placement']    : NULL;  
				$campaign_name= array_key_exists('campaign', $eachRow) ?  $eachRow['campaign']: NULL;  
			}
			if($publisher=='AdWords'){
				$lineItem_id  = array_key_exists('ad_group', $eachRow) ?  $eachRow['ad_group'] : NULL;  
				$lineItem_name= array_key_exists('ad_group', $eachRow) ?  $eachRow['ad_group']    : NULL;  
				$campaign_name= array_key_exists('campaign', $eachRow) ?  $eachRow['campaign']: NULL;  
			}
			if($publisher=='Sizmek'){
				$lineItem_id  = array_key_exists('placementid', $eachRow)  ?  $eachRow['placementid'] : NULL;  
				$lineItem_name= array_key_exists('placementname', $eachRow)?  $eachRow['placementname']    : NULL;  
				$campaign_name= array_key_exists('campaignname', $eachRow) ?  $eachRow['campaignname']: NULL;  
			}
			if($publisher=='GoogleAnalytics'){
				$lineItem_id  = array_key_exists('ga:medium', $eachRow) ? $eachRow['ga:medium'] : NULL;  
				$lineItem_name= array_key_exists('ga:medium', $eachRow) ? $eachRow['ga:medium']    : NULL;  
				$campaign_name= array_key_exists('ga:medium', $eachRow) ? $eachRow['ga:medium']: NULL;  
			}
			if($publisher=='Taboola'){
				$lineItem_id  = array_key_exists('campaign', $eachRow)     ?  $eachRow['campaign'] : NULL;  
				$lineItem_name= array_key_exists('campaign_name', $eachRow)?  $eachRow['campaign_name']    : NULL;  
				$campaign_name= array_key_exists('campaign_name', $eachRow)?  $eachRow['campaign_name']: NULL;  
			}
			if($publisher=='Outbrain'){
				$lineItem_id  = array_key_exists('campaignId', $eachRow)     ?  $eachRow['campaignId'] : NULL;  
				$lineItem_name= array_key_exists('campaignName', $eachRow)?  $eachRow['campaignName']    : NULL;  
				$campaign_name= array_key_exists('campaignName', $eachRow)?  $eachRow['campaignName']: NULL;  
			}

			if($publisher=='Yahoo'){
				$lineItem_id  = array_key_exists('ad_id', $eachRow)     ?  $eachRow['ad_id'] : NULL;  
				$lineItem_name= array_key_exists('ad_id', $eachRow)?  $eachRow['ad_id']    : NULL;  
				$campaign_name= array_key_exists('ad_id', $eachRow)?  $eachRow['ad_id']: NULL;  
			}

			echo"<br><pre>lineItem_name: "; print_r($lineItem_name); //exit;
			if($lineItem_id && $lineItem_name)
			{
				//=========== check here  $AdamsLineItem_ID is already present before hitting database=======================
				$AdamsLineItem_IDFoundInMemArray=0;
				$AdamsLineItem_ID="";
				if($adsetLineItemArray){
					foreach ($adsetLineItemArray as $data)
					{
				        if ($data['lineItem_id'] == $lineItem_id && $data['campaign_name'] == $campaign_name  )
				   		{
				            $AdamsLineItem_ID= $data['AdamsLineItem_ID'];
				            $AdamLineDCMMapping_ID=$data['AdamLineDCMMapping_ID'];
				            $AdamsLineItem_IDFoundInMemArray=1;
				    		//echo"<pre>AdamsLineItem_ID InArray : ";print_r($AdamsLineItem_ID); //exit;
				    		break;
				    	}		
				    }
				}
				//==========end  check here  $AdamsLineItem_ID is already present before hitting database===========
				//===========stored procedure to add data in 'AdamsPublisherMapping' ===============================
				if($AdamsLineItem_ID==""){
					echo"<pre>AdamsLineItem_ID:Check for memoryArray:  ";
					if($publisher!='DCM'){
						$AdamsLineItem_IDD=DB::select("exec SP_IADC_GetAdamsLineItem_ID ?,?,?", array($pub_id,$lineItem_name,$campaign_name));
						$AdamsLineItem_ID=$AdamsLineItem_IDD[0]->AdamsLineItem_ID;
						$AdamLineDCMMapping_ID=NULL;
						//echo"<pre>AdamsLineItem_ID:AfterQuery:  "; print_r($AdamsLineItem_ID);
						//echo"<pre>AdamLineDCMMapping_ID:AfterQuery:  "; print_r($AdamLineDCMMapping_ID); //exit;
					}
					if($publisher=='DCM'){
						$AdamsLineItem_IDD=DB::select("exec SP_IADC_GetAdamLineDCMMapping_ID ?,?,?", array($pub_id,$lineItem_name,$campaign_name));
						$AdamsLineItem_ID=$AdamsLineItem_IDD[0]->AdamsLineItem_ID;
						$AdamLineDCMMapping_ID=$AdamsLineItem_IDD[0]->AdamLineDCMMapping_ID;
						//echo"<pre>AdamsLineItem_ID:AfterQuery:  "; print_r($AdamsLineItem_ID);
						///echo"<pre>AdamLineDCMMapping_ID:AfterQuery:  "; print_r($AdamLineDCMMapping_ID); //exit;
					}	
				}

				echo"<pre>AdamsLineItem_ID:AfterQuery123:  "; print_r($AdamsLineItem_ID); 
				echo"<pre>AdamLineDCMMapping_ID:AfterQuery123:  "; print_r($AdamLineDCMMapping_ID);
				//exit;
				//===========end stored procedure to add data in 'AdamsPublisherMapping' ===========================
				//===========set array here to check $adsetLineItemArray============================================
				if($AdamsLineItem_IDFoundInMemArray==0){
					$LineItemId_AdamId= array( 'lineItem_id' => $lineItem_id,'AdamsLineItem_ID' =>$AdamsLineItem_ID, 'AdamLineDCMMapping_ID'=>$AdamLineDCMMapping_ID, 'campaign_name' =>$campaign_name);
					array_push($adsetLineItemArray, $LineItemId_AdamId);
				}	
				//echo"<pre>AdamsLineItem_ID:AfterQuery:  "; print_r($AdamsLineItem_ID); exit;
				//===========end set array here to check $adsetLineItemArray=========================================

				// **************************************************************************************************
				if($AdamsLineItem_ID != '-1' && $AdamsLineItem_ID!= '-2' )// if AdamsLineItem_ID not found,sp returns -1
				{
					//echo"<pre>"; print_r("NoLine"); exit;
					$APIParams_ID="";
					$paramDBArray=[];
					$breakdownDBArray=[];
					//$P3P5Array=[];
					// ==========================breakdown and param array==============
					$getBreakdownWRTAPI=DB:: table('APIMetrics')
											->select('HiddenMetrics.ApiMetricsName','HiddenMetrics.typeFlag')		
											->join('HiddenMetrics','HiddenMetrics.id', 'APIMetrics.HiddenMetrics_ID')
											->join('Publisher','Publisher.id', 'HiddenMetrics.Publisher_ID')
											->where('Publisher.PublisherName','=', $publisher)
											//->where('APIMetrics.APISource_ID','=', $APISource_ID)
											//->groupBy('HiddenMetrics.ApiMetricsName','HiddenMetrics.typeFlag')
											->orderBy('HiddenMetrics.typeFlag')
											->orderBy('HiddenMetrics.APIParamColumnName')
											->get();
					$getBreakdownWRTAPI=json_decode($getBreakdownWRTAPI);
					//echo"<pre>getBreakdownWRTAPI "; print_r($getBreakdownWRTAPI); //exit;

					foreach ($getBreakdownWRTAPI as $value) {
					    if ($value->typeFlag==1) {
					        $paramDBArray[] = $value->ApiMetricsName;
					        $paramDBArray=array_unique($paramDBArray);
					    }
					    if ($value->typeFlag==3) {
					        $breakdownDBArray[] = $value->ApiMetricsName;
					        $breakdownDBArray=array_unique($breakdownDBArray);
					    }
					}						
					echo"<br>";echo"<pre>paramDBArray saved In DB: "; print_r($paramDBArray); //exit;
					echo"<br>";echo"<pre>breakdownDBArray saved In DB: "; print_r($breakdownDBArray); //exit;

					$existingParamArray = array_fill(0, 19, NULL);
					if($paramDBArray){	
						$i=0;
						foreach ($paramDBArray as $paramkey ) {
							$existingParamArray[$i]=array_key_exists($paramkey, $eachRow) ?  $eachRow[$paramkey]   	: NULL;
							$i++;
						}
					}
					//echo"<pre>existingParamArray "; print_r($existingParamArray);

					$existingBreakDownArray = array_fill(0, 25, NULL);
					if($breakdownDBArray){	
						$i=0;
						foreach ($breakdownDBArray as $breakDownkey ) {
							$existingBreakDownArray[$i]=array_key_exists($breakDownkey, $eachRow) ?  $eachRow[$breakDownkey]   	: NULL;
							$i++;
						}
					}
					echo"<br>";echo"<pre>existingParamArray from eachRow: "; print_r($existingParamArray);
					echo"<br>";echo"<pre>existingBreakDownArray from eachRow: "; print_r($existingBreakDownArray);
					//exit;
					// ========================== end breakdown and param array========================
					// ################################################################################
					$APIParams_ID="";
					//===========check here  $APIParams_ID  is already present before hitting database==========================
					$paramFoundInMemArray=0;
					$BreakDownFoundInMemArray=0;
					if($P3P5Array){
						echo"<br>";echo"inside P3P5Array if sizeofArray: ".count($P3P5Array);
						foreach ($P3P5Array as $data)
					    {
					        //if ($data['p3'] == $existingParamArray[2] && $data['p5'] == $existingParamArray[4] )
					        if ($data['p1'] == $existingParamArray[0] && $data['p2'] == $existingParamArray[1] && 
					        	$data['p3'] == $existingParamArray[2] && $data['p4'] == $existingParamArray[3] && 
					        	$data['p5'] == $existingParamArray[4] && $data['p6'] == $existingParamArray[5] && 
					        	$data['p7'] == $existingParamArray[6] && $data['p8'] == $existingParamArray[7] && 
					        	$data['p9'] == $existingParamArray[8] && $data['p10'] == $existingParamArray[9]&&

					        	$data['p11'] == $existingParamArray[10] && $data['p12'] == $existingParamArray[11]&&   
					        	$data['p13'] == $existingParamArray[12] && $data['p14'] == $existingParamArray[13]&&
					        	$data['p15'] == $existingParamArray[14] && $data['p16'] == $existingParamArray[15]&&
					        	$data['p17'] == $existingParamArray[16] && $data['p18'] == $existingParamArray[17]&& 
					        	$data['p19'] == $existingParamArray[18] 

					        )
					        {
					            $APIParams_ID= $data['APIParams_ID'];
					            $paramFoundInMemArray=1;
					    		echo"<br>";echo"<pre>APIParams_ID In IF ArrayP3P5  : ";print_r($APIParams_ID);
					    		//exit;
					    		break;
					    	}		
					    }
					}	
					//===========end check here  $APIParams_ID  is already present before hitting database=========
					//===========stored procedure to add data in 'APIParams' ======================================
					if($APIParams_ID==""){
						echo"<br>";echo"APIParams_ID is empty IF: ";
						$APIParams_ID_frmDB=DB::select("exec SP_IADC_GetAPIParams_ID ?,?,  ?,?,?, ?,?,?, ?,?,?, ?,?,?,  ?,?,?, ?,?,? ,?", array($AdamsLineItem_ID, 		
							$AdamLineDCMMapping_ID,
							$existingParamArray[0],		$existingParamArray[1], 	$existingParamArray[2],
							$existingParamArray[3],		$existingParamArray[4],     $existingParamArray[5],
							$existingParamArray[6],     $existingParamArray[7],     $existingParamArray[8],
							$existingParamArray[9],		$existingParamArray[10],    $existingParamArray[11],
							$existingParamArray[12],	$existingParamArray[13],	$existingParamArray[14],
							$existingParamArray[15],	$existingParamArray[16],	$existingParamArray[17],
							$existingParamArray[18]
						));
			            $APIParams_ID=$APIParams_ID_frmDB[0]->APIParams_ID;
						echo"<br>";echo"<pre>APIParams_ID: AfterDBHit ";print_r($APIParams_ID); //exit; 
			        } 
			        echo"<br>";echo"<pre>paramFoundInMemArray  : ";print_r($paramFoundInMemArray);   
			        //===========end stored procedure to add data in 'APIParams' ================================
					//===========set array here to check $P3P5Array==============================================
			       	if(!empty($existingParamArray[0] && $paramFoundInMemArray==0)){
			       		echo"data present existingParamArray[0] : ";
			       		//$P3P5=array( 'p3' => $existingParamArray[2],'p5' =>$existingParamArray[4] ,'APIParams_ID' =>$APIParams_ID );
			       		$P3P5=array( 'p1' =>$existingParamArray[0],    'p2' =>$existingParamArray[1],
			       					 'p3' =>$existingParamArray[2] ,   'p4' =>$existingParamArray[3], 
			       					 'p5' =>$existingParamArray[4] ,   'p6' =>$existingParamArray[5],
			       					 'p7' =>$existingParamArray[6] ,   'p8' =>$existingParamArray[7],
			       					 'p9' =>$existingParamArray[8] ,   'p10'=>$existingParamArray[9],

			       					 'p11' =>$existingParamArray[10] , 'p12' =>$existingParamArray[11],
			       					 'p13' =>$existingParamArray[12] , 'p14' =>$existingParamArray[13],
			       					 'p15' =>$existingParamArray[14] , 'p16' =>$existingParamArray[15],
			       					 'p17' =>$existingParamArray[16] , 'p18' =>$existingParamArray[17],
			       					 'p19' =>$existingParamArray[18],

			       					 'APIParams_ID' =>$APIParams_ID );
			       		array_push($P3P5Array, $P3P5);
			       	}	
			       	//echo"<pre>AdamsLineItem_ID:AfterQuery:  "; print_r($AdamsLineItem_ID);
			       	//echo"<pre>APIParams_ID:AfterQuery:  "; print_r($APIParams_ID);
			       	echo"<pre>P3P5Array:P3P5Array: SetIn Memory to Verify: "; print_r($P3P5Array); //exit;
			       	echo"<br>=========================================================================";
			       	//exit;
					//===========end set array here to check $P3P5Array==========================================
					// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					$APIParamBreakDown_ID="";
					//===========check here  $APIParamBreakDown_ID  is already present before hitting database=====
					if($breakdownArray){
						foreach ($breakdownArray as $data)
					    {
					        if ($data['b1'] == $existingBreakDownArray[0] && $data['b2'] == $existingBreakDownArray[1] &&  
					        	$data['b3'] == $existingBreakDownArray[2] && $data['b4'] == $existingBreakDownArray[3] && 
					        	$data['b5'] == $existingBreakDownArray[4] && $data['b6'] == $existingBreakDownArray[5] && 
					        	$data['b7'] == $existingBreakDownArray[6] && $data['b8'] == $existingBreakDownArray[7] &&  
					        	$data['b9'] == $existingBreakDownArray[8] && $data['b10'] == $existingBreakDownArray[9] && 
					        	$data['b11'] == $existingBreakDownArray[10] && $data['b12'] == $existingBreakDownArray[11] &&
					        	$data['b13'] == $existingBreakDownArray[12] && $data['b14'] == $existingBreakDownArray[13] && 
					        	$data['b15'] == $existingBreakDownArray[14] && $data['b16'] == $existingBreakDownArray[15] &&
					        	$data['b17'] == $existingBreakDownArray[16] && $data['b18'] == $existingBreakDownArray[17] &&
					        	$data['b19'] == $existingBreakDownArray[18] && $data['b20'] == $existingBreakDownArray[19] && 
					        	$data['b21'] == $existingBreakDownArray[20] && $data['b22'] == $existingBreakDownArray[21] &&
					        	$data['b23'] == $existingBreakDownArray[22] && $data['b24'] == $existingBreakDownArray[23] &&
					        	$data['b25'] == $existingBreakDownArray[24] &&
					        	
					        	$data['APISource_ID'] == $APISource_ID

					        	&&

					        	$data['APIParams_ID'] == $APIParams_ID)

					        {
					            $APIParamBreakDown_ID= $data['APIParamBreakDown_ID'];
					            $BreakDownFoundInMemArray=1;
					    		//echo"<br><pre>APIParamBreakDown_ID InArray : ";print_r($APIParamBreakDown_ID); //exit;
					    		break;
					    	}		
					    }
					}
					//===========end check here  $APIParamBreakDown_ID  is already present before hitting database===			
	 				//===========stored procedure to add data in 'APIParamBreakDown' =======================
					if($APIParamBreakDown_ID==""){
						$APIParamBreakDown_ID=DB::select("exec SP_IADC_GetAPIParamBreakDown_ID ?,?,?,?,?,?,?,?,?, 
											?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,  ?,?
											", 
											array($APIParams_ID,$APISource_ID,
											$existingBreakDownArray[0],  $existingBreakDownArray[1],  $existingBreakDownArray[2],
											$existingBreakDownArray[3],	 $existingBreakDownArray[4],  $existingBreakDownArray[5],	
											$existingBreakDownArray[6],	 $existingBreakDownArray[7],  $existingBreakDownArray[8],
											$existingBreakDownArray[9],	 $existingBreakDownArray[10], $existingBreakDownArray[11],
											$existingBreakDownArray[12], $existingBreakDownArray[13], $existingBreakDownArray[14],
											$existingBreakDownArray[15], $existingBreakDownArray[16], $existingBreakDownArray[17],
											$existingBreakDownArray[18], $existingBreakDownArray[19], $existingBreakDownArray[20],
											$existingBreakDownArray[21], $existingBreakDownArray[22], $existingBreakDownArray[23],
											$existingBreakDownArray[24]  
											  
										));
						$APIParamBreakDown_ID=$APIParamBreakDown_ID[0]->APIParamBreakDown_ID;
						//echo"<pre>APIParamBreakDown_ID: ";print_r($APIParamBreakDown_ID);//exit;
					}
					echo"<br>";echo"<pre>BreakDownFoundInMemArray  : ";print_r($BreakDownFoundInMemArray); 
			  	    // echo"<pre>APIParams_ID: "; print_r($APIParams_ID);
			       	// echo"<br>B1 : "; print_r($breakdownArray[0]);    // echo"<br>B2 : "; print_r($breakdownArray[1]);                       
                    // echo"<br>B3 : "; print_r($breakdownArray[2]);    // echo"<br>B4 : "; print_r($breakdownArray[3]);                       
                    // echo"<br>B5 : "; print_r($breakdownArray[4]);    // echo"<br>B6 : "; print_r($breakdownArray[5]);                       
                    // echo"<br>B7 : "; print_r($breakdownArray[6]);    // echo"<br>B8 : "; print_r($breakdownArray[7]);                       
                    // echo"<br>B9 : "; print_r($breakdownArray[8]);    // echo"<br>B10 : "; print_r($breakdownArray[9]);                       
                    // echo"<br>B11 : "; print_r($breakdownArray[10]);    // echo"<br>B12 : "; print_r($breakdownArray[11]);               
                    // echo"<br>B13 : "; print_r($breakdownArray[12]);    // echo"<br>B14 : "; print_r($breakdownArray[13]);                       
                    // echo"<br>B15 : "; print_r($breakdownArray[14]);    // echo"<br>B16 : "; print_r($breakdownArray[15]);                       
                    // echo"<br>B17 : "; print_r($breakdownArray[16]);    // echo"<br>B18 : "; print_r($breakdownArray[17]);                   
                    // echo"<br>B10 : "; print_r($breakdownArray[18]);    // echo"<br>B20 : "; print_r($breakdownArray[19]);                       
                    // echo"<br>B21 : "; print_r($breakdownArray[20]);    // echo"<br>B22 : "; print_r($breakdownArray[21]);                       
                    // echo"<br>B23 : "; print_r($breakdownArray[22]);    // echo"<br>B24 : "; print_r($breakdownArray[23]);                      
                    // echo"<br>B25 : "; print_r($breakdownArray[24]);      

			  		// exit;

					//===========stored procedure to add data in 'APIParamBreakDown' ===========================
					//===========set array here to check $P3P5Array=============================================
				    if(!empty($existingBreakDownArray[0]) && $BreakDownFoundInMemArray==0){
				       	$b1b2=array( 'b1'  =>$existingBreakDownArray[0],  'b2'  =>$existingBreakDownArray[1],  'b3'  =>$existingBreakDownArray[2], 
				       				 'b4'  =>$existingBreakDownArray[3],  'b5'  =>$existingBreakDownArray[4],  'b6'  =>$existingBreakDownArray[5], 
				       				 'b7'  =>$existingBreakDownArray[6],  'b8'  =>$existingBreakDownArray[7],  'b9'  =>$existingBreakDownArray[8], 
				       				 'b10' =>$existingBreakDownArray[9],  'b11' =>$existingBreakDownArray[10], 'b12' =>$existingBreakDownArray[11],
				       				 'b13' =>$existingBreakDownArray[12], 'b14' =>$existingBreakDownArray[13], 'b15' =>$existingBreakDownArray[14],
				       				 'b16' =>$existingBreakDownArray[15], 'b17' =>$existingBreakDownArray[16], 'b18' =>$existingBreakDownArray[17],
				       				 'b19' =>$existingBreakDownArray[18], 'b20' =>$existingBreakDownArray[19], 'b21' =>$existingBreakDownArray[20],
				       				 'b22' =>$existingBreakDownArray[21], 'b23' =>$existingBreakDownArray[22], 'b24' =>$existingBreakDownArray[23],
				       				 'b25' =>$existingBreakDownArray[24], 

				       				'APIParamBreakDown_ID' =>$APIParamBreakDown_ID, 'APISource_ID'=>$APISource_ID, 'APIParams_ID' => $APIParams_ID );
			       		array_push($breakdownArray, $b1b2);
					}
					//===========end set array here to check $P3P5Array========================================
					//===========stored procedure to add data in 'APIDataDaily' ===============================
					if($publisher=='Facebook'){
						$startDate=$eachRow['date_start']; 
					}		 				
					if($publisher=='DBM' || $publisher=='DCM'|| $publisher=='Sizmek'){
						$startDate=$eachRow['date']; 
					}

					if($publisher=='AdWords'){
						$startDate=$eachRow['day']; 
					}
					if($publisher=="GoogleAnalytics"){
						$date=$eachRow['ga:date']; 

						$yr=substr($date, 0, 4);echo"<br>";
						$mth=substr($date, 4, 2);echo"<br>";
						$day=substr($date, 6, 2);echo"<br>";
						$startDate=$yr."-".$mth."-".$day;
						//echo $startDate;
					}
					if($publisher=='Taboola'){
						$strdate=strtotime($eachRow['date']);
						$startDate=date("Y-m-d", $strdate);
					}
					if($publisher=='Outbrain'){
						$startDate=$eachRow['fromDate']; 
					}

					if($publisher=='Yahoo'){
						$date=$eachRow['day']; 
						$startDate=date('Y-m-d', strtotime($date));
					}

					//echo"<pre>Date123 "; print_r($startDate); //exit;	
					//echo"<pre>arrayMetricDBINFUNCTION  "; print_r($arrayMetricDB);
					
					foreach($arrayMetricDB as $key)
					{	
						if(array_key_exists($key, $eachRow))
					    {
					        $metricsNameInAPI = $key;
					   		$value=$eachRow[$key];

					   		echo"<pre>"; print_r($value); echo "<br>";
					   		// ====================================
							if(!is_array($value)){
								if(strpos($value, '%') !== false){
									$value = str_replace('%', '', $value);
								}	
							

								if($value=='-' || $value=='--' || $value==' --') {
									$value=Null;
								}

							}	
							if($publisher != 'Facebook'){
								if(strpos($value, '< ') !== false){
								    $value = str_replace('< ', '', $value);	 
								}

								if(strpos($value, '> ') !== false){
								    $value = str_replace('> ', '', $value);	 
								}
							}		

					        // "is_array($value)" condition checked bcz For FB few values are in ArrayFormat
					    	if($publisher == 'Facebook' && is_array($value)){
					    		$value=$value[0]['value'];
					    	}
						    
						    if (is_numeric($value)) {
					   		    $value = $value;
						    	//check for publisher adword and metrics is cost, then divide by 1M
								if($publisher == 'AdWords' && ($key=='cost'))
								{	echo"<br>";print_r($value);
									$value=$value/(pow(10,6));
									echo"<br>";print_r($value);
								}	
								echo"<pre>Million ";print_r($key);	
								echo"<br>DDD   : "; print_r($value);	
						    	// echo"<pre>metricsInAPI: ";   print_r($metricsNameInAPI); 
						    	// echo"<pre>APIParamBreakDown_ID: "; print_r($APIParamBreakDown_ID);
						    	// echo"<br>metricsNameInAPI: "; print_r($metricsNameInAPI);
						    	// echo"<br>value: "; print_r($value);
						    	// echo"<br>startDate: "; print_r($startDate);
						    	// echo"<br>pub_id: "; print_r($pub_id);
						    	//this ,$AdamLineItemMetrics_ID  value will be null for API call
						    	//$AdamLineItemMetrics_ID =Null;
						    	//exit;
						    	//DB::select("exec SP_IADC_GetAPIDataDaily_ID ?,?,?,?,?,?", array($APIParamBreakDown_ID,$AdamLineItemMetrics_ID ,$metricsNameInAPI,$value,$startDate,$pub_id));
						    	DB::select("exec SP_IADC_GetAPIDataDaily_ID ?,?,?,?,?", array($APIParamBreakDown_ID ,$metricsNameInAPI,$value,$startDate,$pub_id));
						    }	

					    }
					}//===========end stored procedure to add data in 'APIDataDaily'===========================
					//exit;
					// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
				}//end if($AdamsLineItem_ID!= '-1')
				else if($AdamsLineItem_ID== '-1')//if AdamsLineItem_ID not found,print bellow msg
				{
					//echo"<br>else NoLineItam in mapping";
					//$adamLineProcesStartTime = Carbon::now()->toDateTimeString();
					$MissingAdamLineItemID=DB::select("exec SP_IADC_InsertMissingAdamLineItemLogData ?,?,?,?,?", array($APISource_ID,$pub_id,$lineItem_name,$lineItem_id, $campaign_name));
					
					//echo"<pre>MissingAdamLineItemID: "; print_r($MissingAdamLineItemID);
				}
				else if($AdamsLineItem_ID== '-2')//if DIT not found,print bellow msg
				{
					//echo"<br>DIT not Found";
				}
				//echo"<br>============================================================================";	
			}
			else{
				echo"<br>No LineItem In ExcelReportArray";
			}
		}

	}

	public static function APIPrcoessingLog($Api_ID,$ApiName,$publisherName,$UniqueReportId,$startDate,$tillDate, $current_time,$IsCron)
	{

		$APIPrcoessingLog_ID=DB::table('APIPrcoessingLog')
							->insertGetId([
							  	'Api_ID' => $Api_ID,
							  	'ApiName' => $ApiName,
							  	'publisherName'=>$publisherName,
							  	'UniqueReportId'=>$UniqueReportId,
							  	'reportStartDate' => $startDate,
							  	'reportEndDate' => $tillDate,
							  	'ApiProcessingStartTime' => $current_time,
							  	'IsCron' => $IsCron
							]);
		return $APIPrcoessingLog_ID;
	}

	public static function UpdateAPIPrcoessingLog($APIPrcoessingLog_ID, $current_timetoEnd)
	{

		$APIPrcoessingLog_ForEnd=DB::table('APIPrcoessingLog')
	                                    ->where('APIPrcoessingLog.id' ,'=',$APIPrcoessingLog_ID)
							            ->update([
							                  'ApiProcessingEndTime' =>$current_timetoEnd
							                ]);
	}


	public function yahoo_credentials(){
		return array(
			'client_id' => 'dj0yJmk9Tkk3cmRzUnNFV3R3JmQ9WVdrOWRFMXRWamxsTlRBbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTQ4',
			'client_secret' => 'f6351a1879d2f662509180ef97d0c2b5f7b55eea',
			'redirect_uri' => 'oob',
			'response_type' => 'code',
			'new_token' => array(
				'code' => 'tepjpk4',
				'grant_type' => 'authorization_code'
			),
			'refresh_token' => array(
				'refresh_token' => 'AA.qMl5WCkKhQDHAxuQPRmzdIgf8oC84AfPkbbtWDr_w_BZwTS7kzJnG',
				'grant_type' => 'refresh_token'
			)
		);
	}

	public function authenticate_yahoo_user(){

		$yahoo_credentials = $this -> yahoo_credentials();

	    return "https://api.login.yahoo.com/oauth2/request_auth?client_id=".$yahoo_credentials['client_id']."&response_type=".$yahoo_credentials['response_type']."&redirect_uri=".$yahoo_credentials['redirect_uri'];
	}

	public function get_yahoo_access_token($token_type = 'refresh_token' , $debug = null){

		$response_array = array();
		$yahoo_credentials = $this -> yahoo_credentials();

	    $client_id = $yahoo_credentials['client_id'];
	    $client_secret = $yahoo_credentials['client_secret'];
	    $redirect_uri = urlencode($yahoo_credentials['redirect_uri']);

	    if($token_type == 'new_token'){
	    	$code = $yahoo_credentials[$token_type]['code'];
		    $grant_type = $yahoo_credentials[$token_type]['grant_type'];

		    $curl_data = "client_id=".$client_id."&client_secret=".$client_secret."&redirect_uri=".$redirect_uri."&code=".$code."&grant_type=".$grant_type;
	    }
	    else if($token_type == 'refresh_token'){
	    	$refresh_token = $yahoo_credentials[$token_type]['refresh_token'];
		    $grant_type = $yahoo_credentials[$token_type]['grant_type'];

		    $curl_data = "client_id=".$client_id."&client_secret=".$client_secret."&redirect_uri=".$redirect_uri."&refresh_token=".$refresh_token."&grant_type=".$grant_type;
	    }else{
	    	$response_array['status'] = 'error';
	    	$response_array['error'] = 'Invalid Token Type';
	    	//print_r($response_array);exit;
			return $response_array;
	    }
	    

	    $authorization_encoded_header = base64_encode($client_id.':'.$client_secret);

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.login.yahoo.com/oauth2/get_token",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $curl_data,
		  CURLOPT_HTTPHEADER => array(
		    "authorization: Basic ".$authorization_encoded_header,
		    "cache-control: no-cache",
		    "content-type: application/x-www-form-urlencoded"
		  ),
		));

		$curl_response = curl_exec($curl);
		$curl_error = curl_error($curl);

		curl_close($curl);

		if($debug == null or $debug == ''){

			if ($curl_error) {
			  $response_array['status'] = 'error';
			  $response_array['error'] = $curl_error;
			} else {

				if($curl_response != '' or $curl_response != null or !empty($curl_response)){
					$decoded_json_response = json_decode($curl_response , true);

					if(array_key_exists('error', $decoded_json_response)){
						$response_array['status'] = 'error';
						$response_array['error'] = $decoded_json_response['error'].' ('.$decoded_json_response['error_description'].')';
					}else{
						$response_array['status'] = 'success';
						$response_array['response'] = $decoded_json_response;
					}

				}
				else{
					$response_array['status'] = 'error';
					$response_array['error'] = 'No Response from Get Access Token Api';
				}
			}

			//print_r($response_array);
			return $response_array;

		}

		else if($debug == 'debug'){

			if ($curl_error) {
			  echo "cURL Error #:" . $curl_error;
			} else {
			  echo $curl_response;
			}
		}

		else{
			$response_array['status'] = 'error';
			$response_array['error'] = 'Unauthorised Access';
			return $response_array;
		}

	}


	public function yahoo_report_performance_stats(Request $request){

		//echo"<pre>"; print_r($request->all()); exit; 
		// =============================================================================
		if($request!=null)
     	{
     		$_SESSION['startDate'] = $request['StartDate'];  
     		$_SESSION['endDate']  = $request['endDate'];
     		// ------------------------------------------------------------
     		$startDate = $request['StartDate'];  
     		$tillDate  = $request['endDate'];
     		$IsCron    = 0;
     		//echo"<pre>1: "; print_r($startDate); exit;
     	} 
     	else 
     	{	
     		// yesterday's date
     		$_SESSION['startDate'] = date('Y-m-d',strtotime("-1 days")); 
     		$_SESSION['endDate']  = date('Y-m-d',strtotime("-1 days"));
     		// ------------------------------------------------------------
     		$startDate = date('Y-m-d',strtotime("-1 days")); 
     		$tillDate  = date('Y-m-d',strtotime("-1 days"));
     		$IsCron    = 1;
     		//echo"<pre>2: "; print_r($startDate); exit;
     	}
     	// =============================================================================
     	// ------- get $APISource_ID------------------------
        $getAPISource_ID =  DB :: table('APISource')
                          ->join('Publisher','Publisher.id', '=', 'APISource.Publisher_ID')
                          ->where('Publisher.PublisherName','=','Yahoo')
                          ->select('APISource.APIName','APISource.id')
                          ->where('APISource.IsActive','=', 1)
                          ->first();
        //$getAPISource_ID = json_decode($getAPISource_ID,true);
       	//echo"getAPISource_ID<pre>";  print_r($getAPISource_ID->id); exit;
       	$APISource_ID=$getAPISource_ID->id;

       	// ------- end get $APISource_ID------------------------
       	// ------- get $arrayMetricDB -----------------------
       	$hiddenMetricsID_Out = DB :: table('HiddenMetrics')  
				->join('Publisher',	 'HiddenMetrics.Publisher_ID',		'=',   'Publisher.id')
				->join('APIMetrics', 'APIMetrics.HiddenMetrics_ID',	'=',   'HiddenMetrics.id')  
				->join('APISource',	 'APIMetrics.APISource_ID',		'=',   'APISource.id') 		
                ->where('APISource.APIName' ,'=',$getAPISource_ID->APIName)
          		->where('Publisher.PublisherName' ,'=','Yahoo')
          		->select('HiddenMetrics.ApiMetricsName', 'HiddenMetrics.PublisherMetricsParameter', 'HiddenMetrics.typeFlag', 'HiddenMetrics.APIParamColumnName')
               	->get();
		//echo"hiddenMetricsID_Result"; print_r($hiddenMetricsID_Out); //exit;
		$arrayMetricDB=[];
		foreach($hiddenMetricsID_Out as $key=>$value) {
			if($value->typeFlag==2){
		    	array_push($arrayMetricDB,$value->ApiMetricsName);
		    	$arrayMetricDB=	array_filter($arrayMetricDB);    	
			}
		}
		// ------- end get $arrayMetricDB -----------------------
		//------------------insert start apiHit Log----------------------------------------------
		$current_timetoStart = Carbon::now()->toDateTimeString();
		$APIPrcoessingLog_ID=AdamAPIController::APIPrcoessingLog($APISource_ID,$getAPISource_ID->APIName ,$startDate,$tillDate, $current_timetoStart,$IsCron);	
		//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
		//------------------End insert start apiHit Log------------------------------------------
     	// =============================================================================

		$access_token_api_result = $this -> get_yahoo_access_token();
		$performance_stats_response_array = array();

		if(array_key_exists('error', $access_token_api_result)){
			$performance_stats_response_array['status'] = 'error';
			$performance_stats_response_array['error'] = $access_token_api_result['error'];
		}else{
			
			$post_request = $request->all();

			$request_array = array(
			   "cube" => "performance_stats",
			   "fields" => [
			       array(
			           "field" => "Ad ID",
			           "alias" => "Ad_ID"
			       ),
			       array(
			           "field" => "Day"
			       ),
			       array(
			           "field" => "Impressions"
			       ),
			       array(
			           "field" => "Clicks"
			       ),
			       array(
			           "field" => "CTR"
			       )
			      
			   	],
			   "filters" => [
			       array(
			           "field" => "Advertiser ID",
			           "operator" => "=",
			           "value" => 1845756
			       ),
			       array(
			           "field" => "Day",
			           "operator" => "between",
			           "from" => $post_request['from_date'],
			           "to" => $post_request['to_date']
			       )
			   	]
			);

			//------ first url to call performance_stats call-------------------------
			$curl = curl_init();
			$curl_data = json_encode($request_array);

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.gemini.yahoo.com/v2/rest/reports/custom",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => $curl_data,
			  CURLOPT_HTTPHEADER => array(
			    "authorization: Bearer ". $access_token_api_result['response']['access_token'] ,
			    "cache-control: no-cache",
			    "content-type: application/json"
			  ),
			));

			$curl_response = curl_exec($curl);
			$curl_error = curl_error($curl);

			curl_close($curl);
			$decoded_json_response = json_decode($curl_response , true);
			//echo"<pre>"; print_r($decoded_json_response['response']['jobId']); 
			//------ first url to call performance_stats call-------------------------

			//------ get jobId for seconf url-----------------------------------------
			$yahoo_jobId=$decoded_json_response['response']['jobId'];
			//----- second curl call to process jobId-------------------------------------
			// **************************************************************************************
			$curl2 = curl_init();

			curl_setopt_array($curl2, array(
			  CURLOPT_URL => "https://api.gemini.yahoo.com/v2/rest/reports/custom/".$yahoo_jobId.'?advertiserId=1845756',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "authorization: Bearer ". $access_token_api_result['response']['access_token'] ,
			    "cache-control: no-cache",
			  ),
			));

			$curl_response2 = curl_exec($curl2);
			$curl_error2 = curl_error($curl2);
			curl_close($curl2);
			$decode_curl_response2 = json_decode($curl_response2 , true);
			// -------=--- Initialize a file URL to the variable-------------------- 
			$url = $decode_curl_response2['response']['jobResponse']; 	  
			//echo"<pre>"; print_r($url); exit;
			$dir = public_path('/reports/Yahoo_Reports/');  
			// --------start a curl to generate excel report for given url in response--------------
			$ch = curl_init();
			$file_name = basename($url); 		// Use basename() function to return the base name of file  
			$save_file_loc = $dir . $file_name; // Save file into file location  
			$fp = fopen($save_file_loc, 'wb');  //Open file
			curl_setopt($ch, CURLOPT_FILE, $fp); 
			curl_setopt($ch, CURLOPT_HEADER, 0); 
			curl_exec($ch);   
			curl_close($ch); 
			fclose($fp);
			// **************************************************************************************
			//-----end second curl call to process jobId------------------------------------- 


			// ---yahoo report file processing logic------------------------------------------
			$filePath = public_path('/reports/Yahoo_Reports/'.$file_name);
			//$filePath = public_path('/reports/Yahoo_Reports/report-4c72ae7d0eb9f9472075e4084799536a-4288863827.csv');
		    config(['excel.import.startRow' => 1 ]);
		    $result = \Excel::load($filePath)->get()->toArray();
		    //echo "<pre>result"; print_r($result);  exit;
			AdamAPIController::storeApivalue($result,$arrayMetricDB,$APISource_ID,'Yahoo');  	
			// ---yahoo report file processing logic------------------------------------------

			//------------------insert end apiHit Log----------------------------------------------
			$current_timetoEnd = Carbon::now()->toDateTimeString();	
			$APIPrcoessingLog_ID=AdamAPIController::UpdateAPIPrcoessingLog($APIPrcoessingLog_ID, $current_timetoEnd);	
			//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
			//------------------End insert end apiHit Log------------------------------------------
		}

	}

	// -----------------------------hotstar-------------------------------------------------------------------
	public function hotstar(Request $request){
		//dd($request->all());
		//=============================================================================================== 
		if($request!=null)
     	{
     		$startDate = $request['StartDate'];  
     		$tillDate  = $request['endDate'];
     		$UniqueReportId=$request['UniqueReportId'];
     		$IsCron    = 0;
     		echo"<pre>startDate1: "; print_r($startDate);
     		echo"<pre>tillDate1: "; print_r($tillDate); //exit;
     	} 
     	else 
     	{	
     		// yesterday's date
     		$startDate = date('Y-m-d',strtotime("-1 days")); 
     		$tillDate  = date('Y-m-d',strtotime("-1 days"));
     		$UniqueReportId=Null;
     		$IsCron    = 1;	
     		echo"<pre>startDate2: "; print_r( $startDate);
     		echo"<pre>tillDate2: "; print_r( $tillDate); //exit;
     	}
     	// ==========================================================
     	$ApiName="HotStarExcel";
		$APISource_ID=DB :: table('APISource') 
						->where('APISource.APIName' ,'=', $ApiName)	
						->select('APISource.id')
						->first();
		//echo"<pre>"; print_r($APISource_ID); exit; 
		$APISource_ID=$APISource_ID->id;				
		//------------------insert start apiHit Log----------------------------------------------
		$current_time = Carbon::now()->toDateTimeString();
		$APIPrcoessingLog_ID=$this->APIPrcoessingLog($APISource_ID,$ApiName,'Hotstar',$UniqueReportId,$startDate,$tillDate, $current_time,$IsCron);	
		//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
		//------------------End insert start apiHit Log------------------------------------------				
		
		$AdamLineItem_URLMapping=DB :: table('AdamLineItem_URLMapping') 
						->get();
		$AdamLineItem_URLMapping=json_decode($AdamLineItem_URLMapping, true);
		//echo"<pre>"; print_r($AdamLineItem_URLMapping); exit; 

		foreach ($AdamLineItem_URLMapping as $key) {
			
			$AdamsLineItem_ID=$key['AdamsLineItem_ID'];
			$CustomApiName=$key['CustomApiName'];
			//echo"<pre>"; print_r($AdamsLineItem_ID);
			//echo"<pre>"; print_r($CustomApiName); exit; 
		

			// +++++++++++++++++++++++++++++++++++++++ HotStar CURL+++++++++++++++++++++++++++++++++++++++++++++++++++
			$url = "https://manager.videoplaza.com/custom-reporting/shared/report/1d0a7162-da63-4963-944c-c425686ab507?&checksum=JqOwTSNhopdRJ-sdUkKQ4n5BfGhZAE3aQsY6Y19Lvfw%3D&columnFilter=0&pageNumber=1&pageSize=100&sortAscending=true&sortIndex=-1&subtotal=true&total=true";
	    
		    $options = array(
		            CURLOPT_RETURNTRANSFER => true,   // return web page
		            CURLOPT_HEADER         => false,  // don't return headers
		            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
		            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
		            CURLOPT_ENCODING       => "",     // handle compressed
		            CURLOPT_USERAGENT      => "test", // name of client
		            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
		            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
		            CURLOPT_TIMEOUT        => 120,    // time-out on response
		    ); 

		    $ch = curl_init($url);
		    curl_setopt_array($ch, $options);
		    $content  = curl_exec($ch);
		    curl_close($ch);

		    //$resArr = array();
		    $resArr = json_decode($content);
		    //echo "<pre>"; print_r($resArr); echo "</pre>";
			    // --------------------------------rearrange array-----------------------------------
		    $headers=$resArr->headers;
		    $headers=array_slice($headers,-4,4);
		    
		    $headerName[]="date";
		    foreach ($headers as $key ) {    
		        $headerName[]=$key->name;
		    }
		    //echo "<pre>headerName"; print_r($headerName); echo "</pre>";

		    $rows=$resArr->rows;
		    $rowValues=[];
		    $resultArray=[];
		    foreach ($rows as $key ) {
		        if($key->type=="NORMAL"){
		            $rowValues=$key->values;
		        } 
		        $resultArray[]=array_combine( $headerName, $rowValues ) ;
		    }
		    //echo "<pre>resultArray"; print_r($resultArray); echo "</pre>"; exit;
		    // --------------------------------rearrange array-----------------------------------

			// +++++++++++++++++++++++++++++++++++++++ HotStar CURL+++++++++++++++++++++++++++++++++++++++++++++++++++
			if($APISource_ID){
				    $res=AdamManualPublisherUploadController::storeFileData($AdamsLineItem_ID,$APISource_ID, $resultArray);
			}	
			//------------------insert end apiHit Log----------------------------------------------
			$current_timetoEnd = Carbon::now()->toDateTimeString();	
			$APIPrcoessingLog_ID=$this->UpdateAPIPrcoessingLog($APIPrcoessingLog_ID, $current_timetoEnd);	
			//echo"<pre>"; print_r($APIPrcoessingLog_ID); exit;				
			//------------------End insert end apiHit Log------------------------------------------
		}	
	}
	// -----------------------------End hotstar-------------------------------------------------------------------
}
