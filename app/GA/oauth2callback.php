<?php
// Load the Google API PHP Client Library.

$upTwo = dirname(__DIR__, 2);
require_once $upTwo. '/vendor/autoload.php';

session_start();
// Create the client object and set the authorization configuration
// from the client_secrets.json you downloaded from the Developers Console.
$client = new Google_Client();
$client->setAuthConfig(__DIR__ . '/client_secrets.json');

$url = dirname(__FILE__);
$array = explode('\\',$url);
$count = count($array);
$authCallUrl='http://' . $_SERVER['HTTP_HOST'] . '/'.$array[$count-3] . '/'.$array[$count-2] . '/'.$array[$count-1] .'/oauth2callback.php'; //exit;
//echo  $authCallUrl; exit;

$client->setRedirectUri($authCallUrl);
$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);

// Handle authorization flow from the server.
if (! isset($_GET['code'])) {

  $auth_url = $client->createAuthUrl();
  //echo $auth_url ; exit;
  header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
} 
else {
  //echo "eslse"; exit;

  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  //$redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/GA1/App/ga_cred/updateDataForGA.php';
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/IADC/App/GA/GoogleAnalyticsReportProcessing.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}
