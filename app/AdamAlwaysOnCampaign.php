<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdamAlwaysOnCampaign extends Model
{

	public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['AdamsLineItem_ID','FromDate', 'ToDate', 'Impressions', 'Clicks', 'VideoViews','Engagement','Visits','Leads','Reach','Installs','SMS','Mailer','Spots','NetRate','NetCost','CreatedBy','CreatedOn','ModifiedBy','ModifiedOn','isActive'];

	public $table = "dbo.AdamAlwaysOnCampaign";

}
