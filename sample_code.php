<?php
require_once __DIR__ . '/vendor/autoload.php';

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

use FacebookAds\Api;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Fields\AdSetFields;
use FacebookAds\Logger\CurlLogger;


$t = 'EAAOqGjFVcNUBAKooR45EaS7GftrImmML84E4tjqfq7Ph1kYlKZCUbZBp2i5VA6952i4vUBvGUKcNFT1wUY6ERNuF26b0uuZB32KUQpIjSFhZCEVzDK9I5zLLZC40Jsj7y4Rnn27RAb2uQYDAzE6JZA3kkdX7uTedDyResqZBTKiMwZDZD';
$access_token = $t;

$ad_account_id = 'act_1375146402746156';
$app_secret = 'f049f57336bebf9fba7d7815aad985e7';
$app_id = '1031454403686613';

$api = Api::init($app_id, $app_secret, $access_token);
$api->setLogger(new CurlLogger());

$fields = array(
  'impressions',
);

  $params = array(
  'level' => 'adset',
  'filtering' => array(),
  'time_range' => array('since' => '2017-07-04','until' => '2017-07-15'),
  'time_increment' => 1
);
echo json_encode((new AdAccount($ad_account_id))->getInsights(
  $fields,
  $params
)->getResponse()->getContent(), JSON_PRETTY_PRINT);

