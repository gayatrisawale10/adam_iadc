<?php

namespace App\sms;

use Illuminate\Database\Eloquent\Model;

class sms_log extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sms_logs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['mobile', 'sent_text','job_id','sent_by', 'response', 'msg_ref'];

    
}
