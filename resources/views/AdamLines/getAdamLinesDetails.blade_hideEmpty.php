@extends('layouts.appAdam')
@section('content')   
<style type="text/css">  
.cc_pageButtons
{
  position: absolute;
  top: 55px;
  right: 4%;
}
</style>
<!-- Content Header (Page header) -->
<h4>
  {{$campClinetBrandDetails->ClientName}}  /  {{$campClinetBrandDetails->BrandName}}  /  {{$campClinetBrandDetails->CampaignName}} 
</h4>
<div class="cc_pageButtons">
  <!-- <button type="submit" class="btn btn-primary" title="Download">
    <i class="fa fa-arrow-down"></i>
  </button>
  <button type="submit" class="btn btn-success" title="Send mail">
    <i class="fa fa-envelope"></i>
  </button> -->
  <button type="submit" class="btn btn-primary" title="Add one more entry"  id="addOneMoreEntry">
    <input type='hidden' value="{{$camp_id}}" id="campID"> <i class="fa fa-plus"></i>
    <input type='hidden' value="{{$campClinetBrandDetails->ClientId}}" id="ClientId"> 
    <input type='hidden' value="{{$specialClientEditAdamKeyString}}" id="specialClientEditAdamKeyString">
  </button>
  <a class="btn btn-primary" href="{{ route('Campaign.index') }}"> <i class="fa fa-arrow-left"></i> </a>
</div>
<hr style="margin-bottom: 5px;"/>

<div class="pull-right margin-top-20">
  <div id="emailDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width:600px;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" style="text-align:center;" >Email Details</h4>
        </div>
         <form class="form-horizontal" role="form" method="POST" action="{{ url('/mailAdamDataToUser') }}">
          {{ csrf_field() }}  
          <!-- -------------------------------------------------------------------------------------------- -->
          <div class="modal-body appendEmail" style="min-height: 250px;">
            <div class="col-md-12">
              <div class="row form-group">
                <div class="col-md-2">
                  <h5><b>E-mail : <span style="color: red">*</span></b></h5> 
                </div>
                <div class="col-md-8">
                  <input type="email" name="email[]" class="form-control" id="emailId" required="true" />  
                </div>
                <div class="col-md-2">
                  <td align="center"><div class="btn btn-primary addMoreEmail "><i class="fa fa-plus" ></i></div></td>
                </div>
              </div>
            </div>
          </div>
          <!-- -------------------------------------------------------------------------------------------- -->
          <div class="modal-footer">
            <center>
              <button type="submit" class="btn btn-primary" id="" >Send mail</button>
            </center>
          </div>
        </form>  
      </div>
    </div>
  </div> 
</div>
<!-- -------------------------------------------------------------------------- -->
@if(session()->has('message'))
  <div class="alert alert-success">
    {{ session()->get('message') }}
  </div>
@endif

@if(session()->has('error'))
  <div class="alert alert-danger">
    {{ session()->get('error') }}
  </div>
@endif

@if ($message = Session::get('danger'))
      <div class="alert alert-danger">
          <p>{{ $message }}</p>
      </div>
@endif

<!-- -------------------------------------------------------------------------- -->
  <!-- content start -->
  <div class="cc_table-responsive">
  <table id="myTable" class="cc_Table display entryTable">
    <thead class="cc_fixedHeadTableContainer">
      <tr role="row">
        <td>Key</td>
        <td class="td">DIT</td>
        <td>Adset Level Mapping</td>
        <td>Actions</td>
        @if(in_array('Type', $nonEmptyKey))
        <td class="td">Type</td>
        @endif
        @if(in_array('Website_Publisher', $nonEmptyKey))
        <td class="td">Website / Publisher</td>
        @endif
        @if(in_array('VendorEntityName', $nonEmptyKey))
        <td class="td">Vendor Entity Name</td>
        @endif
        @if(in_array('FromDate', $nonEmptyKey))
        <td class="td">From Date</td>
        @endif
        @if(in_array('ToDate', $nonEmptyKey))
        <td class="td">To Date</td>
        @endif
        @if(in_array('Platform', $nonEmptyKey))
        <td class="td">Platform</td>
        @endif
        @if(in_array('Paid_OwnedMedia', $nonEmptyKey))
        <td class="td">Paid/ Owned Media</td>
        @endif
        @if(in_array('Section', $nonEmptyKey))
        <td class="td">Section</td>
        @endif
        @if(in_array('AdUnit', $nonEmptyKey))
        <td class="td">Ad Unit</td>
        @endif
        @if(in_array('AdSize', $nonEmptyKey))
        <td class="td">Ad Size</td>
        @endif
        @if(in_array('Days', $nonEmptyKey))
        <td class="td">Days</td>
        @endif
        @if(in_array('GeoTargeting', $nonEmptyKey))
        <td class="td">Geo Targeting</td>
        @endif
        @if(in_array('BuyType', $nonEmptyKey))
        <td class="td">Buy type</td>
        @endif
        @if(in_array('BuySubType', $nonEmptyKey))
        <td class="td">Buy SubType</td>
        @endif
        @if(in_array('Deliverables', $nonEmptyKey))
        <td class="td">Deliverables</td>
        @endif
        @if(in_array('Quantity', $nonEmptyKey))
        <td class="td">Quantity</td>
        @endif
        @if(in_array('GrossRate', $nonEmptyKey))
        <td class="td">Gross Rate</td>
        @endif
        @if(in_array('GrossCost', $nonEmptyKey))
        <td class="td">Gross Cost</td>
        @endif
        @if(in_array('NetRate', $nonEmptyKey))
        <td class="td">Net Rate</td>
        @endif
        @if(in_array('NetCost', $nonEmptyKey))
        <td class="td">Net Cost</td>
        @endif
        @if(in_array('Brand', $nonEmptyKey))
        <td class="td">Brand</td>
        @endif
        @if(in_array('Variant', $nonEmptyKey))
        <td class="td">Variant</td>
        @endif
        @if(in_array('ClientName', $nonEmptyKey))
        <td class="td">Client Name</td>
        @endif
        @if(in_array('CampaignName', $nonEmptyKey))
        <td class="td">Campaign Name</td>
        @endif
        @if(in_array('Vendor_PublisherEmail', $nonEmptyKey))
        <td class="td">Vendor/ Publisher Email</td>
        @endif
        @if(in_array('SubBrand', $nonEmptyKey))
        <td class="td">Sub Brand</td>
        @endif
        @if(in_array('Objective', $nonEmptyKey))
        <td class="td">Objective</td>
        @endif
        @if(in_array('SubObjective', $nonEmptyKey))
        <td class="td">Sub Objective</td>
        @endif
        @if(in_array('FormatType', $nonEmptyKey))
        <td class="td">Format Type</td>
        @endif
        @if(in_array('Impressions', $nonEmptyKey))
        <td class="td">Impressions</td>
        @endif
        @if(in_array('Clicks', $nonEmptyKey))
        <td class="td">Clicks</td>
        @endif
        @if(in_array('VideoViews', $nonEmptyKey))
        <td class="td">Video Views</td>
        @endif
        @if(in_array('Engagement', $nonEmptyKey))
        <td class="td">Engagement</td>
        @endif
        @if(in_array('Visits', $nonEmptyKey))
        <td class="td">Visits</td>
        @endif
        @if(in_array('Leads', $nonEmptyKey))
        <td class="td">Leads</td>
        @endif
        @if(in_array('Reach', $nonEmptyKey))
        <td class="td">Reach</td>
        @endif
        @if(in_array('Installs', $nonEmptyKey))
        <td class="td">Installs</td>
        @endif
        @if(in_array('SMS', $nonEmptyKey))
        <td class="td">SMS</td>
        @endif
        @if(in_array('Mailer', $nonEmptyKey))
        <td class="td">Mailer</td>
        @endif
        @if(in_array('Spots', $nonEmptyKey))
        <td class="td">Spots</td>
        @endif
      </tr>
    </thead>
    <tbody>          
      <!-- row -->
      @foreach($getAdamLineItem as $adamLine)
      <tr class="cc_tableRow" id="{{$adamLine->id}}">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}"> 


        @if(strstr($adamLine->ditNamePerAdamLine,"Excel/URL-Upload")!="Excel/URL-Upload")
        <!--------------------------- AdamUniquekey ---------------------------------------------------- -->
        @if($campClinetBrandDetails->specialClientEditAdamKey == 0) 
        <td style="word-break: break-all;" width="10px"> <!-- key -->
          <label class="setEllipsis copyadamUniqueKey_{{$adamLine->id}}"  title='{{$adamLine->adamUniqueKey}}'  
              onclick="copyToClipboard({{$adamLine->id}})" style="width: 150px;">{{$adamLine->adamUniqueKey}}</label>
          <input class="adamUniqueKey enableDisabled" id="adamUniqueKey_{{$adamLine->id}}" hidden value="{{$adamLine->adamUniqueKey}}" disabled="disabled"/>
        </td>
        <!-------------------------- End AdamUniquekey ------------------------------------------------ -->
        <!-- -------------------- For Specific Clints: AdamUniquekey -------------------------------------->
        @else
          @if($adamLine->adamUniqueKey)
         <!--  <td>g</td> -->
          <td style="word-break: break-all;" width="10px">
             <input class="form-control specialClientEditAdamKey disabled" id="inputNonEdit_{{$adamLine->id}}"  name="specialClientEditAdamKey" value="{{$adamLine->adamUniqueKey}}" disabled="disabled"/>
          </td>
          @else
         <!--  <td>s</td> -->
          <td style="word-break: break-all;" width="10px">
             <input class="form-control specialClientEditAdamKey disabled" id="input37_{{$adamLine->id}}"  name="specialClientEditAdamKey" value="{{$adamLine->adamUniqueKey}}" disabled="disabled"/>
          </td>
          @endif 
        <!-- ---------------------End For Specific Clints: AdamUniquekey ----------------------------------->
        @endif
        <!-- ----------------------------------------------------------------------------------------------->
        @else
        <td></td>
        @endif
        <!-- ----------------------------------------------------------------------------------------------->
        <td><!--DIT -->
           <label id="ditLabel1_{{$adamLine->id}}" class="ditLabel">{{$adamLine->ditNamePerAdamLine}}</label>  
           <select  class="form-control  enableDisabled" id="dd1_{{$adamLine->id}}" multiple="multiple" 
                    name="DataInputTypeName[]"           disabled="disabled"        size="5" style="width: 130px; display: none;">

            @foreach($DIT as $dit)
              <option>{{$dit->DataInputTypeName}}</option>
            @endforeach
          </select>
        </td> 
        @if(strstr($adamLine->ditNamePerAdamLine,"Facebook") || strstr($adamLine->ditNamePerAdamLine,"AdWords") || strstr($adamLine->ditNamePerAdamLine,"DBM"))
          <td><!--Facebook - Campaign Map -->
            <!-- In DB for Campaign level mapping value LineItemAdsetMap = 1-->
            @if($adamLine->LineItemAdsetMap==1)
            <input type="radio" name="facebook_{{$adamLine->id}}" class="check-form-control" value='1' data-AdamsLineItem_ID="{{$adamLine->id}}" checked="checked" disabled="disabled" />
            <label class="form-label-plaintext">C</label>
            @else
            <input type="radio" name="facebook_{{$adamLine->id}}" class="check-form-control" value='1' data-AdamsLineItem_ID="{{$adamLine->id}}"  disabled="disabled"/>
            <label class="form-label-plaintext">C</label>
            @endif
            &nbsp;
            <!-- In DB for AdSet level mapping value LineItemAdsetMap = 2-->
            @if($adamLine->LineItemAdsetMap==2)
            <input type="radio" name="facebook_{{$adamLine->id}}" class="check-form-control" value='2' data-AdamsLineItem_ID="{{$adamLine->id}}" checked="checked"  disabled="disabled"/>
            <label class="form-label-plaintext">A</label>
            @else
            <input type="radio" name="facebook_{{$adamLine->id}}" class="check-form-control" value='2' data-AdamsLineItem_ID="{{$adamLine->id}}"  disabled="disabled"/>
            <label class="form-label-plaintext">A</label>
            @endif
          </td>
          @else
            <td></td>
          @endif
        <td><!--Actions -->
          <div id="mainBtns_{{$adamLine->id}}" class="editBtn">
            <button type="submit" class="btn btn-primary btnEdit" title="Edit" 
            onclick='editAdamLine("{{$adamLine->id}}", "{{$adamLine->ditNamePerAdamLine}}")'>
              <i class="fa fa-pencil"></i>
            </button>   
            <!-- -------------------------------------Delete LineItem -------------------------------------  -->
            <button type="submit" class="btn btn-danger" title="Delete" onclick="deleteAdamLine({{$adamLine->id}})">
              <i class="fa fa-trash-o"></i>
            </button>
            <!-- -------------------------------------End Delete LineItem ---------------------------------  -->                
            @if($campClinetBrandDetails->alwaysOn == 1)
              <a href="{{ url('alwaysOnCampaignDetails/'.$adamLine->adamUniqueKey.'/'.$adamLine->id.'/') }}" >
                <button type="submit"  class="btn btn-success alwaysOnAdamBtn" title="Always On" 
                        value="{{$campClinetBrandDetails->alwaysOn}}"><i class="fa fa-toggle-on"></i>
                </button>
              </a>
            @endif                
            @if(strstr($adamLine->ditNamePerAdamLine,"DCM"))
            <button type="submit" 
                    class="btn btn-primary mapAdamDCM" 
                    title="DCM Mapping" 
                    data-toggle="modal" 
                    data-AdamsLineItem_ID="{{$adamLine->id}}"
                    data-adamUniqueKey="{{$adamLine->adamUniqueKey}}"

                    data-ClientId="{{$campClinetBrandDetails->ClientId}}"
                    data-specialClientEditAdamKeyString="{{$specialClientEditAdamKeyString}}"
                
                    data-target="#addMultpleLineDCM"><img class="home_logo" src="{{asset('/image/dcmSymbol.jpg')}}">
            </button>
            @endif

            @if(strstr($adamLine->ditNamePerAdamLine,"Excel/URL-Upload"))
              <button type="submit" 
                    class="btn btn-primary uploadExcelForManualPublisher" 
                    title="Upload File for Excel/URL-Upload Type Publisher" 
                    data-toggle="modal" 
                    data-AdamsLineItem_ID="{{$adamLine->id}}"
                    data-adamUniqueKey="{{$adamLine->adamUniqueKey}}"
                    data-PublisherName="{{$adamLine->Website_Publisher}}"
                    data-target="#uploadExcelForManualPublisher"><i class="fa fa-upload"></i>
              </button>

              <button type="submit" 
                    class="btn btn-primary upload_URL_ForManualPublisher" 
                    title="Upload URL for Excel/URL-Upload Type Publisher" 
                    data-toggle="modal" 
                    data-AdamsLineItem_ID="{{$adamLine->id}}"
                    data-adamUniqueKey="{{$adamLine->adamUniqueKey}}"
                    data-PublisherName="{{$adamLine->Website_Publisher}}"
                    data-target="#upload_URL_ForManualPublisher"><i class="fa fa-file-text-o"></i>
              </button>

            @endif  
          </div>


          <div id="secBtns_{{$adamLine->id}}" style="display: none;" class="secBtns" >
            <button type="submit" class="btn btn-success" onclick="saveAdamLine({{$adamLine->id}})"><i class="fa fa-check"></i></button>
            <button type="submit" class="btn btn-danger confirmCancel" data-currentRow="{{$adamLine->id}}"><i class="fa fa-times"></i></button>
          </div>
        </td>
        @if(in_array('Type', $nonEmptyKey))
        <td><!--Type -->
          <input class="form-control ditLabel"         id="ditLabel2_{{$adamLine->id}}"  value="{{$adamLine->Type}}"  disabled="disabled"  
                 title='{{$adamLine->Type}}'/>

          <select class="form-control enableDisabled" id="dd2_{{$adamLine->id}}"  name="DataInputTypeName[]" 
                  disabled="disabled"                 size="5"                    style="width: 130px; display: none;">
            @foreach($AllDropDownsList['activity_type'] as $activity_type)
              <option value="{{$activity_type->Activity_Name}}"
                <?php if ($activity_type->Activity_Name == $adamLine->Type){ ?>
                  selected='true'
                <?php } ?>
                >{{ $activity_type->Activity_Name}}</option>
            @endforeach
          </select>
        </td>
        @endif
        @if(in_array('Website_Publisher', $nonEmptyKey))
        <td><!--Website / Publisher -->
          <input class="form-control disabled" id="input1_{{$adamLine->id}}" value="{{$adamLine->Website_Publisher}}"
                 disabled="disabled"  title='{{$adamLine->Website_Publisher}}'/>
        </td>
        @endif
        @if(in_array('VendorEntityName', $nonEmptyKey))
        <td><!--Vendor Entity Name -->
          <input class="form-control ditLabel"  id="ditLabel3_{{$adamLine->id}}" value="{{$adamLine->VendorEntityName}}" 
                 disabled="disabled"            title='{{$adamLine->VendorEntityName}}'/>
          <select class="form-control enableDisabled" id="dd3_{{$adamLine->id}}" name="DataInputTypeName[]" 
                  disabled="disabled"                 size="5"                   style="width: 130px; display: none;">

          @foreach($AllDropDownsList['vendor'] as $vendor)
            <option
              <?php if ($vendor->Supplier_Name == $adamLine->VendorEntityName){ ?>
                selected='true'
              <?php } ?>
            >{{ $vendor->Supplier_Name }}</option>
          @endforeach
          </select>
        </td>
        @endif
        @if(in_array('FromDate', $nonEmptyKey))
        <td><!--From Date -->
          <input class="form-control disabled" type="date" id="input2_{{$adamLine->id}}" value="{{$adamLine->FromDate}}" disabled="disabled"   title='{{$adamLine->FromDate}}'/>
        </td>
        @endif
        @if(in_array('ToDate', $nonEmptyKey))
        <td><!--To Date -->
          <input  class="form-control disabled" type="date" id="input3_{{$adamLine->id}}" value="{{$adamLine->ToDate}}"  disabled="disabled"   title='{{$adamLine->ToDate}}'/>
        </td>
        @endif
        @if(in_array('Platform', $nonEmptyKey))
        <td><!--Platform -->

          <input class="form-control ditLabel"  id="ditLabel4_{{$adamLine->id}}" value="{{$adamLine->Platform}}"  disabled="disabled"  
                 title='{{$adamLine->Platform}}'/>
          <select class="form-control  enableDisabled" id="dd4_{{$adamLine->id}}" name="DataInputTypeName[]" 
                  disabled="disabled"                  size="5"                   style="width: 130px; display: none;">
          @foreach($AllDropDownsList['platform'] as $platform)
            <option
              <?php if ($platform->Platfform_Name == $adamLine->Platform){ ?>
                selected='true'
              <?php } ?>
            >{{ $platform->Platfform_Name }}</option>
          @endforeach
          </select>
        </td>
        @endif
        @if(in_array('Paid_OwnedMedia', $nonEmptyKey))
        <td><!--Paid/ Owned Media -->

          <input class="form-control ditLabel"  id="ditLabel5_{{$adamLine->id}}" value="{{$adamLine->Paid_OwnedMedia}}" disabled="disabled"  
                 title='{{$adamLine->Paid_OwnedMedia}}'/>
          <select class="form-control enableDisabled" id="dd5_{{$adamLine->id}}" name="DataInputTypeName[]" 
                  disabled="disabled"                 size="5"                   style="width: 130px; display: none;">
          @foreach($AllDropDownsList['paid_owned'] as $paid_owned)
            <option
              <?php if ($paid_owned->Paid_Owned_Name == $adamLine->Paid_OwnedMedia){ ?>
                selected='true'
              <?php } ?>
            >{{ $paid_owned->Paid_Owned_Name }}</option>
          @endforeach
          </select>
        </td>
        @endif
        @if(in_array('Section', $nonEmptyKey))
        <td><!--Section -->
          <input class="form-control disabled" id="input4_{{$adamLine->id}}" value="{{$adamLine->Section}}" disabled="disabled" 
                 title='{{$adamLine->Section}}'/>
        </td>
        @endif
        @if(in_array('AdUnit', $nonEmptyKey))
        <td><!--Ad Unit -->
          <input class="form-control disabled" id="input5_{{$adamLine->id}}" value="{{$adamLine->AdUnit}}" disabled="disabled" title='{{$adamLine->AdUnit}}'/>
        </td>
        @endif
        @if(in_array('AdSize', $nonEmptyKey))
        <td><!--Ad Size -->
          <input class="form-control disabled" id="input6_{{$adamLine->id}}" value="{{$adamLine->AdSize}}" disabled="disabled" title='{{$adamLine->AdSize}}'/>
        </td>
        @endif
        @if(in_array('Days', $nonEmptyKey))
        <td><!--Days -->
          <input class="form-control disabled" id="input7_{{$adamLine->id}}" value="{{$adamLine->Days}}" disabled="disabled" title='{{$adamLine->Days}}'/>
        </td>
        @endif
        @if(in_array('GeoTargeting', $nonEmptyKey))
        <td><!--Geo Targeting -->
          <input class="form-control disabled" id="input8_{{$adamLine->id}}" value="{{$adamLine->GeoTargeting}}" disabled="disabled" title='{{$adamLine->GeoTargeting}}'/>
        </td>
        @endif
        @if(in_array('BuyType', $nonEmptyKey))
        <td><!--Buy type -->
          <input class="form-control disabled"  id="input9_{{$adamLine->id}}" value="{{$adamLine->BuyType}}" disabled="disabled" title='{{$adamLine->BuyType}}'/>
        </td>
        @endif
        @if(in_array('BuySubType', $nonEmptyKey))
        <td><!--BuySubType -->
          <input class="form-control disabled" id="input10_{{$adamLine->id}}" value="{{$adamLine->BuySubType}}" disabled="disabled" title='{{$adamLine->BuySubType}}'/>
        </td>
        @endif
        @if(in_array('Deliverables', $nonEmptyKey))
        <td><!--Deliverables -->
          <input class="form-control disabled" id="input11_{{$adamLine->id}}" value="{{$adamLine->Deliverables}}" disabled="disabled" title='{{$adamLine->Deliverables}}'/>
        </td>
        @endif
        @if(in_array('Quantity', $nonEmptyKey))
        <td><!--Quantity -->
          <input class="form-control disabled" id="input12_{{$adamLine->id}}" value="{{$adamLine->Quantity}}" disabled="disabled" title="{{$adamLine->Quantity}}"/>
        </td>
        @endif
        @if(in_array('GrossRate', $nonEmptyKey))
        <td><!--Gross Rate -->
          <input class="form-control disabled" id="input13_{{$adamLine->id}}" value="{{$adamLine->GrossRate}}" disabled="disabled" title="{{$adamLine->GrossRate}}"/>
        </td>
        @endif
        @if(in_array('GrossCost', $nonEmptyKey))
        <td><!--Gross Cost -->
          <input class="form-control disabled" id="input14_{{$adamLine->id}}" value="{{$adamLine->GrossCost}}" disabled="disabled" title="{{$adamLine->GrossCost}}"/>
        </td>
        @endif
        @if(in_array('NetRate', $nonEmptyKey))
        <td><!--Net Rate -->
          <input class="form-control disabled" id="input15_{{$adamLine->id}}" value="{{$adamLine->NetRate}}" disabled="disabled" title="{{$adamLine->NetRate}}"/>
        </td>
        @endif
        @if(in_array('NetCost', $nonEmptyKey))
        <td><!--Net Cost -->
          <input class="form-control disabled" id="input16_{{$adamLine->id}}" value="{{$adamLine->NetCost}}" disabled="disabled" title="{{$adamLine->NetCost}}"/>
        </td>
        @endif
        @if(in_array('Brand', $nonEmptyKey))
        <td><!--Brand -->
          <input class="form-control disabled" id="input17_{{$adamLine->id}}" value="{{$adamLine->Brand}}" disabled="disabled" title="{{$adamLine->Brand}}"/>
        </td>
        @endif
        @if(in_array('Variant', $nonEmptyKey))
        <td><!--Variant -->
          <input class="form-control disabled" id="input18_{{$adamLine->id}}" value="{{$adamLine->Variant}}" disabled="disabled" title="{{$adamLine->Variant}}"/>
        </td>
        @endif
        @if(in_array('ClientName', $nonEmptyKey))
        <td><!--Client Name -->
          <input class="form-control disabled" id="input19_{{$adamLine->id}}" value="{{$adamLine->ClientName}}" disabled="disabled" title="{{$adamLine->ClientName}}"/>
        </td>
        @endif
        @if(in_array('CampaignName', $nonEmptyKey))
        <td><!--Campaign Name -->
          <input class="form-control disabled" id="input20_{{$adamLine->id}}" value="{{$adamLine->CampaignName}}" disabled="disabled" title="{{$adamLine->CampaignName}}"/>
        </td>
        @endif
        @if(in_array('Vendor_PublisherEmail', $nonEmptyKey))
        <td><!--Vendor/ Publisher Email -->
          <input class="form-control disabled" id="input21_{{$adamLine->id}}" value="{{$adamLine->Vendor_PublisherEmail}}" disabled="disabled" title="{{$adamLine->Vendor_PublisherEmail}}"/>
        </td>
         @endif
        @if(in_array('SubBrand', $nonEmptyKey))
        <td><!--Sub Brand -->
          <input class="form-control disabled" id="input22_{{$adamLine->id}}" value="{{$adamLine->SubBrand}}" disabled="disabled" title="{{$adamLine->SubBrand}}"/>
        </td>
         @endif
        @if(in_array('Objective', $nonEmptyKey))
        <td><!--Objective -->
          <input class="form-control disabled" id="input23_{{$adamLine->id}}" value="{{$adamLine->Objective}}" disabled="disabled" title="{{$adamLine->Objective}}"/>
        </td>
         @endif
        @if(in_array('SubObjective', $nonEmptyKey))
        <td><!--Sub Objective -->
          <input class="form-control disabled" id="input24_{{$adamLine->id}}" value="{{$adamLine->SubObjective}}" disabled="disabled" title="{{$adamLine->SubObjective}}"/>
        </td>
         @endif
        @if(in_array('FormatType', $nonEmptyKey))
        <td><!--Format Type -->
          <input class="form-control disabled" id="input25_{{$adamLine->id}}" value="{{$adamLine->FormatType}}" disabled="disabled" title="{{$adamLine->FormatType}}"/>
        </td>
        @endif
        @if(in_array('Impressions', $nonEmptyKey))
        <td><!--Impressions -->
          <input class="form-control disabled" id="input26_{{$adamLine->id}}" value="{{$adamLine->Impressions}}" disabled="disabled" title="{{$adamLine->Impressions}}"/>
        </td>
        @endif
        @if(in_array('Clicks', $nonEmptyKey))
        <td><!--Clicks -->
          <input class="form-control disabled" id="input27_{{$adamLine->id}}" value="{{$adamLine->Clicks}}" disabled="disabled" title="{{$adamLine->Clicks}}"/>
        </td>
        @endif
        @if(in_array('VideoViews', $nonEmptyKey))
        <td><!--Video Views -->
          <input class="form-control disabled" id="input28_{{$adamLine->id}}" value="{{$adamLine->VideoViews}}" disabled="disabled" title="{{$adamLine->VideoViews}}"/>
        </td>
         @endif
        @if(in_array('Engagement', $nonEmptyKey))
        <td><!--Engagement -->
          <input class="form-control disabled" id="input29_{{$adamLine->id}}" value="{{$adamLine->Engagement}}" disabled="disabled" title="{{$adamLine->Engagement}}"/>
        </td>
         @endif
        @if(in_array('Visits', $nonEmptyKey))
        <td><!--Visits -->
          <input class="form-control disabled" id="input30_{{$adamLine->id}}" value="{{$adamLine->Visits}}" disabled="disabled" title="{{$adamLine->Visits}}"/>
        </td>
         @endif
        @if(in_array('Leads', $nonEmptyKey))
        <td><!--Leads -->
          <input class="form-control disabled" id="input31_{{$adamLine->id}}" value="{{$adamLine->Leads}}" disabled="disabled" title="{{$adamLine->Leads}}"/>
        </td>
        @endif
        @if(in_array('Reach', $nonEmptyKey))
        <td><!--Reach -->
          <input class="form-control disabled" id="input32_{{$adamLine->id}}" value="{{$adamLine->Reach}}" disabled="disabled" title="{{$adamLine->Reach}}"/>
        </td>
         @endif
        @if(in_array('Installs', $nonEmptyKey))
        <td><!--Installs -->
          <input class="form-control disabled" id="input33_{{$adamLine->id}}" value="{{$adamLine->Installs}}" disabled="disabled" title="{{$adamLine->Installs}}"/>
        </td>
         @endif
        @if(in_array('SMS', $nonEmptyKey))
        <td><!--SMS -->
          <input class="form-control disabled" id="input34_{{$adamLine->id}}" value="{{$adamLine->SMS}}" disabled="disabled" title="{{$adamLine->SMS}}"/>
        </td>
        @endif
        @if(in_array('Mailer', $nonEmptyKey))
        <td><!--Mailer -->
          <input class="form-control disabled" id="input35_{{$adamLine->id}}" value="{{$adamLine->Mailer}}" disabled="disabled" title="{{$adamLine->Mailer}}"/>
        </td>
        @endif
        @if(in_array('Spots', $nonEmptyKey))
        <td><!--Spots -->
          <input class="form-control disabled" id="input36_{{$adamLine->id}}" value="{{$adamLine->Spots}}" disabled="disabled" title="{{$adamLine->Spots}}"/>
        </td>
        @endif
      </tr> 
      <!-- row -->
      @endforeach       
      <!-- ========================================================================================================== -->
     </tbody>
  </table>
  </div>
  <!-- content end -->
 @endsection  
<!-- Cancel Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div ><p class="adamLineId" ></div>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header"><h4 class="modal-title">Warning</h4></div>
      <div class="modal-body">
        <p>You may have not saved some changes. Would you like to continue or discard the changes done?</p>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success" id="btnModalCont">Continue</button>
      <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnModalCancel" value="">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- End Cancel Modal -->
<!-- DCM Placement Model -->
<div id="addMultpleLineDCM" class="modal fade " role="dialog">
  <div class="modal-dialog" style="width:800px;">
    <div ><p class="adamLineId" >
    </div>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <center>
          <h4 class="modal-title">DCM Multiple Placement Mapping</h4>
        </center>
      </div>
      <div class="modal-body">
        <!-- ------upaload DCM key-------------- -->
        <form method="POST" action="{{route('uploadDCMKeyFile')}}" enctype="multipart/form-data">
          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
          <input type="hidden" name="AdamsLineItem_ID" id="AdamsLineItem_ID">
          <div class="form-group col-md-12" >
            <div class="col-md-5" align="right"></div>
            <div class="col-md-3" align="right">
              <strong>DCM-Key File:</strong>
            </div>
            <div class="col-md-2 form-group" align="right">
              <input type="file" name="DCMKeyFile" required="true" />
            </div> 
            <div class="col-md-2 " align="right">
              <input type="submit" class="btn btn-success" value="upload"/>
            </div> 
          </div>  
        </form>
        <!-- ------upaload DCM key-------------- -->
        <div class='DCMKeyTable' style="width:750px; height:250px; overflow:scroll; margin-bottom: 50px;">
          <table width="100%" border="1" style='padding: 5px 10px;' id="dcmMappingTable">
            <thead>
              <th class="text-center" style='padding: 3px 0px;'>DCM Key</th>
              <th class="text-center" style='padding: 3px 0px;'>Campaign Type</th>
              <th class="text-center" style='padding: 3px 0px;'>Ad Section</th>
              <th class="text-center" style='padding: 3px 0px;'>Banner Size</th>
              <th class="text-center" style='padding: 3px 0px;'>Device</th>
              <th class="text-center" style='padding: 3px 0px;'>Action</th>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <form method="POST" action="{{route('saveAdamDCMMApping')}}" >
          <div>
            <table id="mapTable" class="cc_Table display">
              <thead class="cc_fixedHeadTableContainer">
                <tr role="row">
                  <td class="text-center">DCMKey</td>
                  <td class="text-center">CampaignType</td>
                  <td class="text-center">AdSection</td>
                  <td class="text-center">BannerSize</td>
                  <td class="text-center">Device</td>
                  <td class="text-center">Actions</td>
                </tr>
              </thead>
              <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
              <input type="hidden" name="AdamsLineItem_ID" id="AdamsLineItem_IDD"> 
              <input type="hidden" name="adamUniqueKey" id="adamUniqueKey">
              <input type="hidden" name="spClientId" id="spClientId"> 
              <tbody>   
                <tr>
                  <td class="DCMKeyInput"></td>
                  <td><input class="form-control map_val" id="CampaignType" name="CampaignType[]" autocomplete="off"/></td>
                  <td><input class="form-control map_val" id="AdSection" name="AdSection[]" autocomplete="off"/></td>
                  <td><input class="form-control map_val" id="BannerSize" name="BannerSize[]" autocomplete="off"/></td>
                  <td><input class="form-control map_val" id="Device" name="Device[]" autocomplete="off"/></td>
                  <td align="center"><p class="btn btn-primary insert-row"><i class="fa fa-plus" ></i></p></td>
                </tr>
              </tbody>
            </table>  
          </div>    
          <div class="modal-footer">
            <center><input type="submit" class="btn btn-success" value="Save"/></center>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>    
<!-- end DCM Placement Model -->

<!-- uploadExcelForManualPublisher Model -->
<div id="uploadExcelForManualPublisher" class="modal fade " role="dialog">
  <div class="modal-dialog" style="width:800px;">
    <div ><p class="adamLineId" >
    </div>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <Left>
          <h4 class="modal-title">Upload Excel File for Publisher :</h4>
        </left>
      </div>
      <div class="modal-body">
        <!-- ------upaload DCM key-------------- -->
        <!--  <form method="POST" action="{{route('uploadExcelForManualPublisher')}}" enctype="multipart/form-data"> -->
        <form method="POST"  enctype="multipart/form-data" id="fileUpload">
          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" /> 
          <input type="hidden" name="AdamsLineItem_ID" id="AdamsLineItem_IDF">
          <input type="hidden" name="PublisherName" id="PublisherName">
          
          <div class="form-group col-md-12" >
            <!-- <div class="col-md-5" align="right"></div> -->
            <div class="col-md-3" align="center">
              <strong>Report File:</strong>
            </div>
            <div class="col-md-3 form-group" align="center">
              <input type="file" name="manualPubReportFile" required="true" />
            </div> 
            <div class="col-md-3" align="right">
              <button id="sheetlist" class="btn btn-success">select sheet</button>
            </div> 
           <!--<div class="col-md-2 " align="right">
              <input type="submit" class="btn btn-success" value="upload"/>
            </div>  -->
          </div>  

          <div class='SheetListTable' style="width:750px; height:250px; overflow:scroll; margin-bottom: 50px;">
            <table width="100%" border="1" style='padding: 5px 10px;' id="SheetListTable">
              <thead>
                <!-- <th><input type="checkbox"></th> -->
                <th></th>
                <th class="text-center" style='padding: 3px 0px;'>sheet Name</th>
              </thead>
              <tbody>
              </tbody>
            </table>
            <div class="col-md-2 " align="center">
              <button id="SubmitSheet" class="btn btn-success">Upload</button>
              <!-- <input type="submit" class="btn btn-success" value="upload"/> -->
            </div> 
        </div>

        </form>

        <!-- ------display excel sheet-------------- -->
        <div class='manualPubReportTable' style="width:750px; height:250px; overflow:scroll; margin-bottom: 50px;">
          <table width="100%" border="1" style='padding: 5px 10px;' id="manualPubReportTable">
            <thead>
              <th class="text-center" style='padding: 3px 0px;'>File Name</th>
              <th class="text-center" style='padding: 3px 0px;'>Download</th>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
          
        
      </div>
    </div>
  </div>
</div>    
<!-- end uploadExcelForManualPublisher Model -->

<!-- upload_URL_ForManualPublisher -->
<div id="upload_URL_ForManualPublisher" class="modal fade " role="dialog">
  <div class="modal-dialog" style="width:800px;">
    <div ><p class="adamLineId" >
    </div>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <Left>
          <h4 class="modal-title">Upload URL for Publisher :</h4>
        </left>
      </div>
      <div class="modal-body">
        <!-- ------upaload DCM key-------------- -->
       <form method="POST" action="{{route('upload_URL_ForManualPublisher')}}" enctype="multipart/form-data"> 
          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" /> 
          <input type="hidden" name="AdamsLineItem_ID" id="AdamsLineItem_IDU">
          <input type="hidden" name="PublisherName" id="PublisherNameU">
          
          <div class="form-group col-md-12" >
            <div class="form-group col-md-2">
              <label><strong>Uplad URL: </strong></label>
            </div>
            <div class="form-group col-md-8">
              <input type="text"  class="form-control" required="" name="URLName" placeholder="Upload URL">
            </div>
            <div class="form-group col-md-2">
              <button id="sheetlist" class="btn btn-success">Save URL</button>
            </div>
          </div>   
        </form>

       <!-- ------display URL LIST-------------- -->
        <div class='urlList' style="width:750px; height:250px; overflow:scroll; margin-bottom: 50px;">
          <table width="100%" border="1" style='padding: 5px 10px;' id="urlList">
            <thead>
              <th class="text-center" style='padding: 3px 0px;'>URL Name</th>
              
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>  
        <!-- ------display URL LIST-------------- -->     
      </div>
    </div>
  </div>
</div>
<!-- END upload_URL_ForManualPublisher -->