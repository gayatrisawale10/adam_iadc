<?php $base_url= url('/'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="_token" content="{{ csrf_token() }}" />

  <link rel="shortcut icon" href="./image/favicon.ico" type="image/x-icon">
  <link rel="icon" href="./image/favicon.ico" type="image/x-icon">

  <link rel="stylesheet" href="../css/jquery.ui.all.css">
  <link rel="shortcut icon" href="./image/favicon.ico" type="image/x-icon">
  <link rel="icon" href="./image/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
  <!--   <link rel="stylesheet" type="text/css" href="public/css/bootstrap.min.css"> -->

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
  <script src="../js/app.js"></script>

  <script data-require="jquery@*" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

  <script data-require="datatables@*" data-semver="1.10.12" src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

  <link data-require="datatables@*" data-semver="1.10.12" rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
  <link rel="stylesheet" href="http://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" />
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <script src="../js/sweetalert.js"></script>
  <link href="../css/sweetalert.css" rel="stylesheet">
  <!--  <link href="assets/custom_css/customStyles.css" rel="stylesheet"> -->

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script src="../js/LineItemMappingValues.js" type="text/javascript"></script>
   <script src="{{asset('js/fastselect.standalone.js')}}"></script>
  <style>
    div.addRow{
          line-height: 45px;
          background-color: #fff;
          padding-left: 10px;
          border-bottom: 1px solid;
          border-top: 1px solid #e5e5e5;
    }
    .myrightalign{
          text-align: right;
             width: 145px;
    }
    .addMore{
    
    cursor: pointer;
  }
  </style>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'IADC') }}</title>
  <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
       ]); ?>
  </script>
  <style type="text/css">

    .margin-top-20{
        margin-top: 20px;
    }
    .navbar-fixed-bottom, .navbar-fixed-top {
    width:100%;
    /* position: relative; */
    margin-top:30px;
    }

    th {
        text-align:center;
    }
    div.addRow{
      line-height: 45px;
    background-color: #fff;
    padding-left: 10px;
    border-bottom: 1px solid;
    border-top: 1px solid #e5e5e5;}
    #example thead th
    {
      width: 100px !important;
    }

    .fstMultipleMode .fstControls {
    box-sizing: border-box;
    width: 11em;
    }

    .fstMultipleMode .fstQueryInput {
    /* font-size: 1.4em; */
    float: left;
    padding: .28571em 0;
    margin: 0 0 .35714em 0;
    width: 12em;
    color: #999;
}

  </style>
</head>
<body>
  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header"></div>
      <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <!-- Left Side Of Navbar -->
        <ul class="nav navbar-nav"><img class="home_logo" src="{{asset('/image/logo_final.png') }}" width='60' height="60"></ul>
        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
          <!-- Authentication Links -->
          @if(Auth::user()->UserRole_ID == 1 ||  Auth::user()->UserRole_ID == 3)
          <li><a href="{{ url('/Campaign') }}">Campaign</a></li>
          <li><a  href="{{ url('/api') }}">API </a></li>
          <li><a href="{{ url('/Person') }}">Person</a></li>
          <li><a href="{{ url('/Client') }}">Client</a></li>
          @endif

          @if(Auth::user()->UserRole_ID == 1 ||  Auth::user()->UserRole_ID == 2)
          <li><a href="{{ url('/Publisher') }}">Publisher</a></li>
          @endif
          
          @if( Auth::user()->UserRole_ID == 4)
          <li><a href="{{ url('/ClientHome') }}">Campaign</a></li>
          @endif          
          <li class="dropdown">
            <a class=" dropdown-toggle" type="button" data-toggle="dropdown">{{ Auth::user()['name'] }}<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a  href="{{ url('/logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Logout
                    </a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                </li>
              </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="col-md-8 col-md-offset-2">
  {{-- @include('flash::message') --}}
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-6 pull-left">
            <h2>LineItem Details</h2>
            <h4>Campaign : {{$Campaign_name}}</h4>
      </div>
      <div class="col-lg-6">
        @if($getClientDeatils[0]->LineItemType==1)
          <div class="pull-right margin-top-20">
            <button type="button" class="btn btn-success btn-lg addMore" title="Add Line Item" data-toggle="modal" data-target="#addLineItemModal"><i class="fa fa-plus-square"> </i>
            </button>

            <a class="btn btn-success btn-lg addMore" title="Sample Sheet" href="{{ Route('downloadlineitemexcel')}}"><i class="fa fa-download"></i>&nbsp;<i class="fa fa-file-excel-o"></i></a>          
            <!--  <a class="btn btn-success btn-lg addMore" title="Error Excel" id="errorSheet" href="{{url('/getErrorExcelFIle/'.$Campaign_ID)}}"><i class="fa fa-exclamation"></i>&nbsp;<i class="fa fa-file-excel-o"></i></a> -->
            <button type="button" class="btn btn-primary btn-lg addMore" title="Upload Excel" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-arrow-up"></i>&nbsp;<i class="fa fa-file-excel-o"></i></button>

            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" style="padding:15px;">
              <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content"  style="padding:20px;" >
                  <form method="POST" id="excelSheetUploadForm" action="{{Route('UploadLineItemExcel', ['id' => $Campaign_ID, 'cid'=>$CampaignID])}}" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Excel Sheet</label>
                          {{csrf_field()}}
                      <input type="file" id="result_file" class="form-control" name="import_file">
                    </div>
                    <input type="submit"  class="btn btn-info">
                  </form>
                </div>
              </div>
            </div>

             <a class="btn btn-success btn-lg addMore" title="Error Excel" id="errorSheet" href="{{url('/getErrorExcelFIle/'.$Campaign_ID)}}"><i class="fa fa-exclamation"></i>&nbsp;<i class="fa fa-file-excel-o"></i></a> 

             <a class="btn btn-success btn-lg addMore" title="Location List"  href="{{url('/getLocationSheet')}}"><i class="fa fa-map-marker"></i>&nbsp;<i class="fa fa-file-excel-o"></i></a>

            <a class="btn btn-success btn-lg addMore" title="Back" href="{{ url('/Campaign')}}"><i class="fa fa-arrow-left"></i></a>
          </div>
        @else
          <div class="pull-right margin-top-20"> 
            <button type="button" class="btn btn-success btn-lg addmore" title="Add Line Item" data-toggle="modal" data-target="#addLineItemModalForCityBank"><i class="fa fa-plus-square"></i>
            </button> &nbsp;&nbsp;&nbsp;
            <a class="btn btn-success btn-lg addmore" title="Back" href="{{ URL('/Campaign')}}"><i class="fa fa-arrow-left"></i></a>
          </div>
        @endif
      </div>
    </div>
   <!--  <div class="row">
       <div class="col-lg-6 pull-right">          
          <input type="Text" name="campStartDate" id="campStartDate" value="{{$CampStartDate}}">
          <input type="Text" name="Campaign_ID" id="Campaign_ID" value="{{$Campaign_ID}}">
          <select name="publisherAPI" id='publisherAPI' class="form-control selectApiPublisher" >
            <option value="" data-for="selected-genre">Select</option>
                  @foreach($getApiPublisher as $apiPub)
                    <option value="{{ $apiPub->pubId }}">{{ $apiPub->PublisherName }}</option>
                  @endforeach
            </select>
      </div>
    </div> -->
    <div class="container" >
      <div class="row" >
        <div class="col-lg-12">
            <div class="btn btn-danger" id="errorid" style='display:none;'></div>
             @if (Session::has('error_msg'))
               <div class="alert alert-success">
               <button class="close" data-close="alert"></button>
               <span>{{ Session::get('error_msg') }}</span>
               {{Session::forget('error_msg')}}
               </div>
            @endif
            @if (Session::has('error_msg1'))
               <div class="alert alert-danger">
               <button class="close" data-close="alert"></button>
               <span>{{ Session::get('error_msg1') }}</span>
               {{Session::forget('error_msg1')}}
               </div>
            @endif
            @if ($errors->has('import_file'))
                <span class="help-block bg-red">
                    <strong style="color: red;">{{ $errors->first('import_file') }}</strong>
                </span>
            @endif
        </div>
      </div>
      <!-- ----------------citybank modal ------------------------ -->
      <div class="pull-right margin-top-20">
        <div id="addLineItemModalForCityBank" class="modal fade" tabindex="-1" data-focus-on="input:first">
          <div class="modal-dialog" role="document">
            <div class="modal-content" style= "width: 800px;">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="gridModalLabel" style="text-align:center;" >Add Line Item Details</h4>
              </div>
              <div class="modal-body" id="register">
                {!!Form::open(['url' => '/CampaignPublisherStoreCity', 'id'=>"modal_add",'method' => 'post', 'class'=>'form-horizontal form-row-seperated'])!!}
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="form-group row ">
                  <!-- ---------------------------------------------------------------- -->
                  <input type="hidden" name="lineItemType" id="token" value="{{$getClientDeatils[0]->LineItemType}}">
                  <!-- ---------------------------------------------------------------- -->
                  <div class=" col-md-1"><strong>Name</strong></div>
                  <div class="col-md-10">
                    <input type="text" placeholder="Line Item Name" class="form-control" name='LineItemName' value=""  id="LineItemName" />
                  </div>
                </div>
                <hr/>

                <div class="form-group row ">
                  <div class=" col-md-1 "><strong>Genre</strong></div>
                  <div class="col-md-3">
                    <select name="Genre_ID" id='Genre_ID' class="form-control map-genre-publisher" >
                      <option value="" data-for="selected-genre">Select</option>
                        @foreach($Genre as $bp)
                          <option value="{{ $bp->id }}" data-for="selected-genre">{{ $bp->GenreName }}</option>
                        @endforeach
                    </select>
                  </div>

                  <div class="col-md-1 ">
                    <strong>Publisher<br>Name</strong>
                  </div>
                  <div class="col-md-3">
                    <select id="Publisher_ID" name="Publisher_ID" class="form-control map-dit-publisher publishers" >
                      <option value="" data-for="selected-brand">Select</option>
                    </select>
                  </div>
                </div>
                <hr/>
                <!--code start -->
                <div class="form-group row ">
                  <div class="col-md-2"></div>
                  <div class="col-md-2" style="text-align:center;"><strong>Plan </strong></div>
                  <div class="col-md-2"><strong>Data InputType</strong></div>
                  <div class="col-md-2"> </div>
                  <div class="col-md-2" style="text-align:center"><strong>Plan </strong></div>
                  <div class="col-md-2"><strong>Data InputType</strong></div>
                </div>
                <hr/>
                <!-- ---------------------------------------------------------------- -->
                <div class="form-group row ">
                  <div class="col-md-2"><strong>Impression:</strong></div>
                  <div class="col-md-2">
                    <input type="text" class="form-control" name="Impressions" id="impressions" placeholder = " Impression" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-2">
                    <select  id="PIDataInputTypeID" name="PIDataInputTypeID" class="form-control PIDataInputTypeID">
                        <option value="" data-for="PIDataInputTypeID">Select</option>
                    </select>
                  </div>
                  <div class="col-md-1"></div>
                <!-- ---------------------------------------------------------------- -->
                  <div class="col-md-1 "><strong>Clicks:</strong></div>
                  <div class="col-md-2">
                      <input type="text" class="form-control" name='Clicks' value=""  id="clicks" placeholder = "Clicks" onkeypress="return isNumberKey(event)"  />
                  </div>
                  <div class="col-md-2">
                    <select id="PCDataInputTypeID" name="PCDataInputTypeID" class="form-control PCDataInputTypeID" >
                      <option value="" data-for="PCDataInputTypeID">Select</option>
                    </select>
                  </div>
                </div>
                <!-- ---------------------------------------------------------------- -->
                <div class="form-group row ">
                  <div class="col-md-2 "><strong>Opens:</strong></div>
                  <div class="col-md-2">
                    <input type="text" placeholder="opens" class="form-control" name='Opens' value=""  id="netRates" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-2">
                    <select id="PODataInputTypeID" name="PODataInputTypeID" class="form-control PODataInputTypeID" >
                      <option value="" data-for="PODataInputTypeID">Select</option>
                    </select>
                  </div>
                  <div class="col-md-1"></div>
                  <div class="col-md-1 ">
                    <strong>Leads:</strong>
                  </div>
                  <div class="col-md-2">
                    <input type="text" placeholder="Leads" class="form-control" name='Leads' value=""  id="leads" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-2">
                    <select id="PLDataInputTypeID" name="PLDataInputTypeID" class="form-control PLDataInputTypeID" >
                      <option value="" data-for="PLDataInputTypeID">Select</option>
                    </select>
                  </div>
                </div>
                <!-- ---------------------------------------------------------------- -->
                <div class="form-group row ">
                  <div class="col-md-2 "><strong>Rate:</strong></div>
                  <div class="col-md-2">
                    <input type="text" placeholder="Rate" class="form-control" name='Rate' value=""  id="netRates" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-2">
                    <select id="PNRADataInputTypeID" name="PNRADataInputTypeID" class="form-control PNRADataInputTypeID" >
                      <option value="" data-for="PNRADataInputTypeID">Select</option>
                    </select>
                  </div>
                  <div class="col-md-1"></div>
                  <div class="col-md-1 ">
                    <strong>Cost:</strong>
                  </div>
                  <div class="col-md-2">
                    <input type="text" placeholder="Cost" class="form-control" name='Cost' value=""  id="netCost" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-2">
                    <select id="PNCoDataInputTypeID" name="PNCoDataInputTypeID" class="form-control PNCoDataInputTypeID" >
                      <option value="" data-for="PNCoDataInputTypeID">Select</option>
                    </select>
                  </div>
                </div>
                <hr>
                <!-- ---------------------------------------------------------------- -->
                <div class="form-group row after-add-more">
                  <div class=" col-md-1 "><strong>Card</strong></div>
                  <div class="col-md-3">
                    <select name="Card_ID[]" id='Card_ID' class="form-control map-card-creative" multiple required>
                      <option value="" data-for="selected-card">Select</option>
                        @foreach($card as $key)
                          <option value="{{ $key->id }}" data-for="selected-card">{{ $key->CardName }}</option>
                        @endforeach
                    </select>
                  </div>

                  <div class="col-md-1 "><strong>Creative</strong></div>
                  <div class="col-md-3">
                    <select id="creative_id" name="creative_id[]" class="form-control map-dit-creative creative" multiple required>
                      <option value="" data-for="selected-brand">Select</option>
                    </select>
                  </div>
                </div>

                <input type='hidden' name='Campaign_ID' value="{{$Campaign_ID}}" id="Campaign_id">
                <input type='hidden' name='CampaignID' value="{{$CampaignID}}" id="Campaign_id">

                <div class="form-group row text-center">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
                {!! Form::close() !!}
              </div>
          </div>
        </div>
        </div>
      </div>
      <!-- -----------------------------End Of citybank modal------------------------ -->
      <div class="pull-right margin-top-20">
        <div id="addLineItemModal" class="modal fade" tabindex="" data-focus-on="input:first">
          <div class="modal-dialog" role="document" style="width:1250px;">
            <div class="modal-content" style= "width: 1200px;">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="gridModalLabel" style="text-align:center;" >Add Line Item Details</h4>
              </div>
              <div class="modal-body" id="register">
                {!!Form::open(['url' => '/CampaignPublisherStore', 'id'=>"modal_add",'method' => 'post', 'class'=>'form-horizontal form-row-seperated'])!!}
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <!-- ===================================================================================================== -->
                <div class="form-group row ">
                  <input type="hidden" name="lineItemType" id="token" value="{{$getClientDeatils[0]->LineItemType}}">
                  <div class=" col-md-1"><strong>Name</strong></div>
                  <div class="col-md-5">
                    <input type="text" placeholder="Line Item Name" class="form-control" name='LineItemName' value=""  id="LineItemName" />
                  </div>

                  <!-- <div class=" col-md-1 "><strong>Genre</strong></div>
                  <div class="col-md-2">
                    <select name="Genre_ID" id='Genre_ID' class="form-control map-genre-publisher genreOnReverse" >
                      <option value="" data-for="selected-genre">Select</option>
                        @foreach($Genre as $bp)
                          <option value="{{ $bp->id }}" data-for="selected-genre">{{ $bp->GenreName }}</option>
                        @endforeach
                    </select>
                  </div> -->

                  <div class="col-md-1 "><strong>Publisher<br>Name</strong></div>
                  <div class="col-md-5">
                    <input type="text" multiple class="form-control pubfastSelect multipleInput pubId map-dit-publisher" name="Publisher_ID" placeholder="Publisher Name">
                    <!-- <select id="Publisher_ID" name="Publisher_ID1" class="form-control map-dit-publisher publishers" style="display: none" >
                      <option value="" data-for="selected-brand">Select</option>
                      @foreach($Publisher as $pub)
                        <option value="{{ $pub->id }}" data-for="selected-genre">{{ $pub->PublisherName }}</option>
                      @endforeach
                    </select> -->
                  </div>
                </div>
                <!-- ----------------------------------------------------------------------------------------------------- -->
                <div class="form-group row ">
                  <div class=" col-md-1 "><strong>Phase</strong></div>
                  <div class="col-md-2">
                    <select name="Phase_ID" id='Phase_ID' class="form-control " >
                      <option value="" >Select</option>
                        @foreach($Phase as $ph)
                          <option value="{{ $ph->id }}" >{{ $ph->PhaseName}}</option>
                        @endforeach
                    </select>
                  </div>

                  <div class=" col-md-1 "><strong>Medium</strong></div>
                  <div class="col-md-2">
                    <select name="Medium_ID" id='Medium_ID' class="form-control " >
                      <option value="" >Select</option>
                        @foreach($Medium as $medium)
                          <option value="{{ $medium->id }}" >{{ $medium->MediumName }}</option>
                        @endforeach
                    </select>
                  </div>

                  <div class="col-md-1 "><strong>Deal Type</strong></div>
                  <div class="col-md-2">
                    <select id="DealType_ID" name="DealType_ID" class="form-control">
                       <option value="" data-for="dType">Select</option>
                            @foreach($DealType as $DType)
                            <option value="{{ $DType->id }}" data-for="selected-bp3">{{ $DType->DealTypeName }}</option>
                            @endforeach
                    </select>
                  </div>

                  <div class="col-md-1 "><strong>Device</strong></div>
                  <div class="col-md-2">
                    <select id="DeliveryMode_ID" name="DeliveryMode_ID" class="form-control">
                       <option value="" data-for="mode">Select</option>
                            @foreach($DeliveryMode as $mode)
                            <option value="{{ $mode->id }}" data-for="selected-bp4">{{ $mode->DeliveryModeName }}</option>
                            @endforeach
                    </select>
                  </div>
                </div>
                <!-- ---------------------------------------------------------------------------------------------------- -->
                 <div class="form-group row ">
                  <div class=" col-md-1 "><strong>Objective </strong></div>
                  <div class="col-md-2">
                    <select name="Objective_ID" id='Objective_ID' class="form-control map-objective-objectiveValue " >
                      <option value="" >Select</option>
                        @foreach($Objective as $obj)
                          <option value="{{ $obj->id }}" >{{ $obj->ObjectiveName}}</option>
                        @endforeach
                    </select>
                  </div>

                  <div class="col-md-1 "><strong>Objective Value </strong></div>
                  <div class="col-md-2">
                   <select id="ObjectiveValue_ID" name="ObjectiveValue_ID" class="form-control objectiveValues" >
                      <option value="" data-for="">Select</option>
                    </select>
                  </div>

                  <div class=" col-md-1 "><strong>Section</strong></div>
                  <div class="col-md-2">
                    <select name="Section_ID" id='Section_ID' class="form-control" >
                      <option value="" data-for="selected-genre">Select</option>
                        @foreach($Section as $sect)
                          <option value="{{ $sect->id }}" data-for="selected-section">{{ $sect->SectionName }}</option>
                        @endforeach
                    </select>
                  </div>

                  <div class="col-md-1 "><strong>AdUnit</strong></div>
                  <div class="col-md-2">
                    <select id="AdUnit_ID" name="AdUnit_ID" class="form-control" >
                       <option value="" data-for="selected-genre">Select</option>
                        @foreach($AdUnit as $au)
                          <option value="{{ $au->id }}" data-for="selected-section">{{ $au->AdUnitName }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                 <div class="form-group row ">
                  <div class=" col-md-1"><strong>Format Type </strong></div>
                  <div class="col-md-2">
                    <select name="FormatType_ID" id='FormatType_ID' class="form-control map-formatType-format" >
                      <option value="" data-for="">Select</option>
                        @foreach($FormatType as $formatType)
                          <option value="{{ $formatType->id }}" data-for="">{{ $formatType->FormatTypeName }}</option>
                        @endforeach
                    </select>
                  </div>

                  <div class="col-md-1 "><strong>Format</strong></div>
                  <div class="col-md-2" >
                    <select id="Format_ID" name="Format_ID" class="form-control formats" >
                      <option value="" data-for="">Select</option>
                    </select>
                  </div>
                  <div class="col-md-1 "><strong><p class="formatValueName "></p></strong></div>
                  <div class="col-md-2"><p class="formatValue"></p></div>
                </div>
                <hr/>
                <!-- ===================================================================================================== -->
                <div class="form-group row ">
                  <div class=" col-md-1 "><strong>Targeting: </strong></div>
                  <div class="col-md-2">
                    <label  name="target" id='age' value="1">&nbsp; Age: </label>
                    <div  class="ageRange">
                      &nbsp;AgeFrom: &nbsp;<input type="text" class="form-control" name="target_ageRangeFrom">
                      &nbsp;AgeTo: &nbsp; <input type="text" class="form-control" name="target_ageRangeTo">
                    </div>
                  </div>

                  <div class="col-md-1">
                    <label  name="target" id='age' value="1">Gender:</label>
                    <select name="Gender_ID" id='Gender_ID' class="form-control  targetingGenderDropdwn " !style="display:none" >
                      <option value="" data-for="">Select</option>
                       @foreach($Gender as $gender)
                          <option value="{{ $gender->id }}" data-for="">{{ $gender->GenderName }}</option>
                        @endforeach
                    </select>
                  </div>
                  <!--purposly given same className  for Google and City Location-->
                  <div class="col-md-2">
                    <label  name="target" id='age' value="1">Google Location: </label>
                    <button name="target" id='gLocation' class="trgtLocation" value='gLoc' onclick="return popitup('<?php echo $base_url;?>/gmap')">addLocation</button>
                     <label id='addrs'></label>
                     <label id='dists'></label>
                  </div>

                  <div class="col-md-2">
                    <label  name="target" id='age' value="1">City Location: </label>
                    <button name="target" id='cLocation' class="trgtLocation" value="cLoc" onclick="return popup('<?php echo $base_url;?>/city')" >CityLoaction</button>

                    <label id='locationInclude'></label>
                    <label id='locationExclude'></label>
                  </div>

                  <div class="col-md-2">
                    <label  name="target" id='age' value="1"> Others:Targeting </label>
                    <select multiple name="Targeting_ID[]" id='Targeting_ID' class="form-control map-targeting-targetingValue  getMultipleTarget otherTargeting ">
                      @foreach($Targeting as $tgt)
                        <option value="{{ $tgt->id }}" data-for="">{{ $tgt->TargetingName }}</option>
                      @endforeach
                    </select>
                    <br>
                  </div> 
                  <div class="col-md-2">
                    <label  name="target" id='age' value="1">Targeting Value</label> 
                    <select multiple id="TargetingValue_ID" name="TargetingValue_ID[]" class="form-control targetingValues">
                    </select>  
                  </div>
                </div>
               
                <div class="form-group row ">
                  <div class="col-md-1"></div>
                  <div class="col-md-1" style="text-align:center;"><strong>Plan </strong></div>
                  <div class="col-md-1"><strong>DIT</strong></div>
                  <div class="col-md-1"> </div>
                  <div class="col-md-1" style="text-align:center"><strong>Plan </strong></div>
                  <div class="col-md-1"><strong>DIT</strong></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-1" style="text-align:center;"><strong>Plan </strong></div>
                  <div class="col-md-1"><strong>DIT</strong></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-1" style="text-align:center;"><strong>Plan </strong></div>
                  <div class="col-md-1"><strong>DIT</strong></div>
                </div>
            
                <div class="form-group row ">
                  <div class="col-md-1"><strong>Impression:</strong></div>
                  <div class="col-md-1">
                    <input type="text" class="form-control imp_val" name="Impressions" id="impressions" placeholder = " Impression" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-1">
                    <select  id="PIDataInputTypeID" name="PIDataInputTypeID" class="form-control PIDataInputTypeID imp_val_dit" >
                      <option value="" data-for="PIDataInputTypeID">Select</option>
                    </select>
                  </div>

                  <div class="col-md-1 "><strong>Clicks:</strong></div>
                  <div class="col-md-1">
                    <input type="text" class="form-control click_val" name='Clicks' value=""  id="clicks" placeholder = "Clicks" onkeypress="return isNumberKey(event)"  />
                  </div>
                  <div class="col-md-1">
                    <select id="PCDataInputTypeID" name="PCDataInputTypeID" class="form-control PCDataInputTypeID click_val_dit" >
                      <option value="" data-for="PCDataInputTypeID">Select</option>
                    </select>
                  </div>

                  <div class="col-md-1"><strong>Visits:</strong></div>
                  <div class="col-md-1">
                    <input type="text" placeholder="Visits" class="form-control visit_val" name='Visits' value=""  id="visits"  onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-1">
                    <select id="PVsDataInputTypeID" name="PVsDataInputTypeID" class="form-control PVsDataInputTypeID visit_val_dit" >
                      <option value="" data-for="PVsDataInputTypeID">Select</option>
                    </select>
                  </div>
                  <div class="col-md-1 "><strong>Views:</strong></div>
                  <div class="col-md-1">
                    <input type="text" placeholder="Views" class="form-control view_val" name='Views' value=""  id="views" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-1">
                    <select id="PVwDataInputTypeID" name="PVwDataInputTypeID" class="form-control PVwDataInputTypeID view_val_dit" >
                      <option value="" data-for="PVwDataInputTypeID">Select</option>
                    </select>
                  </div>
                </div>

                <div class="form-group row ">
                  <div class="col-md-1 "><strong>Engagement:</strong></div>
                  <div class="col-md-1">
                    <input type="text" placeholder="Engagement" class="form-control eng_val" name='Engagement' value=""  id="engagements" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-1">
                    <select id="PEDataInputTypeID" name="PEDataInputTypeID" class="form-control PEDataInputTypeID eng_val_dit" >
                      <option value="" data-for="PEDataInputTypeID">Select</option>
                    </select>
                  </div>
                  <div class="col-md-1 "><strong>Leads:</strong></div>
                  <div class="col-md-1">
                    <input type="text" placeholder="Leads" class="form-control lead_val" name='Leads' value=""  id="leads" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-1">
                    <select id="PLDataInputTypeID" name="PLDataInputTypeID" class="form-control PLDataInputTypeID lead_val_dit" >
                      <option value="" data-for="PLDataInputTypeID">Select</option>
                    </select>
                  </div>

                  <div class="col-md-1 "><strong>Net Rate:</strong></div>
                  <div class="col-md-1">
                    <input type="text" placeholder="Net Rate" class="form-control netRate_val" name='NetRate' value=""  id="netRates" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-1">
                    <select id="PNRDataInputTypeID" name="PNRDataInputTypeID" class="form-control PNRDataInputTypeID netRate_val_dit" >
                      <option value="" data-for="PNRDataInputTypeID">Select</option>
                    </select>
                  </div>
                  <div class="col-md-1 "><strong>Net Cost:</strong></div>
                  <div class="col-md-1">
                    <input type="text" placeholder="Net Cost" class="form-control netCost_val" name='NetCost' value=""  id="netCost" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-1">
                    <select id="PNCDataInputTypeID" name="PNCDataInputTypeID" class="form-control PNCDataInputTypeID netCost_val_dit" >
                      <option value="" data-for="PNCDataInputTypeID">Select</option>
                    </select>
                  </div>
                </div>

                <div class="form-group row ">
                  <div class="col-md-1"><strong>Reach:</strong></div>
                  <div class="col-md-1">
                    <input type="text" placeholder="Reach" class="form-control reach_val" name='Reach' value=""  id="reaches" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-1">
                    <select id="PRDataInputTypeID" name="PRDataInputTypeID" class="form-control PRDataInputTypeID reach_val_dit" >
                      <option value="" data-for="PRDataInputTypeID">Select</option>
                    </select>
                  </div>
                  <div class="col-md-1"><strong>Installs:</strong></div>
                  <div class="col-md-1">
                    <input type="text" placeholder="Installs" class="form-control install_val" name='Installs' value=""  id="installs" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-1">
                    <select id="PInDataInputTypeID" name="PInDataInputTypeID" class="form-control PInDataInputTypeID install_val_dit" >
                      <option value="" data-for="PInDataInputTypeID">Select</option>
                    </select>
                  </div>

                  <div class="col-md-1 "><strong>Spots:</strong></div>
                  <div class="col-md-1">
                    <input type="text" placeholder="Spots" class="form-control spot_val" name='Spots' value=""  id="spots" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-1">
                    <select id="PSpDataInputTypeID" name="PSpDataInputTypeID" class="form-control PSpDataInputTypeID spot_val_dit" >
                      <option value="" data-for="PSpDataInputTypeID">Select</option>
                    </select>
                  </div>
                  <div class="col-md-1 "><strong>SMS:</strong></div>
                  <div class="col-md-1">
                    <input type="text" placeholder="SMS" class="form-control sms_val" name='SMS' value=""  id="sms" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-1">
                    <select id="PSmDataInputTypeID" name="PSmDataInputTypeID" class="form-control PSmDataInputTypeID sms_val_dit" >
                      <option value="" data-for="PSmDataInputTypeID">Select</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row ">
                  <div class="col-md-1"><strong>Mailers:</strong></div>
                  <div class="col-md-1">
                    <input type="text" placeholder="Mailers" class="form-control mailer_val" name='Mailers' value=""  id="mailers" onkeypress="return isNumberKey(event)" />
                  </div>
                  <div class="col-md-1">
                    <select id="PMDataInputTypeID" name="PMDataInputTypeID" class="form-control PMDataInputTypeID mailer_val_dit" >
                      <option value="" data-for="PMDataInputTypeID">Select</option>
                    </select>
                  </div>
                </div>
                <hr/>

                <input type='hidden' name='Campaign_ID' value="{{$Campaign_ID}}" id="Campaign_id">
                <input type='hidden' name='CampaignID' value="{{$CampaignID}}" id="Campaign_id">

                <div class="form-group row text-center">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
                 {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-12">
      <!-- auto scroll start -->
      <div style="width: 100%; overflow-x:auto;margin-bottom: 50px;">
        <table id="example" class="display" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>LineItem Name</th>
              <th>Genre</th>
              <th>Publisher<br> Name</th>
              <th>Phase</th>
              <th>Objective</th>
              <th>Objective Value</th>
              <th>Medium</th>

              <th>Format Type</th>
              <th>Deal Type</th>
              <th>Delivery Mode</th>
              <th>Action</th>
            </tr>
          </thead>
          <form method='post' action=''>
          <?php $i = 1;  ?>
          @foreach($CampaignPublisherdetails_index as $CampaignPublisher_index_data)
            <tr align="center">
              <td>{{$CampaignPublisher_index_data->LineItemID}}_{{$CampaignPublisher_index_data->LineItemName}}</td>
              <td>{{$CampaignPublisher_index_data->GenreName}}</td>
              <td>{{$CampaignPublisher_index_data->PublisherName}}</td>
              <td>{{$CampaignPublisher_index_data->PhaseName}}</td>
              <td>{{$CampaignPublisher_index_data->ObjectiveName}}</td>
              <td>{{$CampaignPublisher_index_data->ObjectiveValueName}}</td>
              <td>{{$CampaignPublisher_index_data->MediumName}}</td>
              <td>{{$CampaignPublisher_index_data->FormatTypeName}}</td>
              <td>{{$CampaignPublisher_index_data->DealTypeName}}</td>
              <td>{{$CampaignPublisher_index_data->DeliveryModeName}}</td>

              <input type='hidden' class="myrightalign" name='Campaign_ID' value="{{$Campaign_ID}}" id="">

              <input type='hidden' class="myrightalign" name='LineItem_id' value="{{$CampaignPublisher_index_data->id}}" id="getLineitemId_{{$i}}">

              <td>
                <div class="row">
                  <div class="col-md-3">

                    @if($getClientDeatils[0]->LineItemType==1)

                    <!-- ---------------------single modal--------------- -->
                      <button type="submit" class="btn btn-info  editLineItem addmore" title="Edit Line Item"  !style="width:100%; margin-bottom: 3px; margin-right:16px;" style="margin-bottom: 3px; margin-right:100px;"

                        data-toggle="modal"
                        data-target="#editLineItemModal"

                        data-id                   ="{{$CampaignPublisher_index_data->id}}"
                        data-LineItemID           ="{{$CampaignPublisher_index_data->LineItemID}}"
                        data-LineItemName         ="{{$CampaignPublisher_index_data->LineItemName}}"
                        data-Campaign_ID          ="{{$CampaignPublisher_index_data->Campaign_ID}}"
                        data-Publisher_ID         ="{{$CampaignPublisher_index_data->Publisher_ID}}"data-GenreName            ="{{$CampaignPublisher_index_data->GenreName}}"
                        data-Genre_ID             ="{{$CampaignPublisher_index_data->Genre_ID}}"
                        data-PublisherName        ="{{$CampaignPublisher_index_data->PublisherName}}"

                        data-Phase_ID             ="{{$CampaignPublisher_index_data->Phase_ID}}"
                        data-PhaseName            ="{{$CampaignPublisher_index_data->PhaseName}}"

                        data-ObjectiveValue_ID    ="{{$CampaignPublisher_index_data->ObjectiveValue_ID}}"
                        data-ObjectiveValueName   ="{{$CampaignPublisher_index_data->ObjectiveValueName}}"

                        data-Objective_ID         ="{{$CampaignPublisher_index_data->Objective_ID}}"
                        data-ObjectiveName        ="{{$CampaignPublisher_index_data->ObjectiveName}}"

                        data-Medium_ID            ="{{$CampaignPublisher_index_data->Medium_ID}}"
                        data-MediumName           ="{{$CampaignPublisher_index_data->MediumName}}"

                        data-Format_ID            ="{{$CampaignPublisher_index_data->Format_ID}}"
                        data-FormatName           ="{{$CampaignPublisher_index_data->FormatName}}"

                        data-FormatType_ID        ="{{$CampaignPublisher_index_data->FormatType_ID}}"
                        data-FormatTypeName       ="{{$CampaignPublisher_index_data->FormatTypeName}}"

                        data-DealType_ID          ="{{$CampaignPublisher_index_data->DealType_ID}}"
                        data-DealTypeName         ="{{$CampaignPublisher_index_data->DealTypeName}}"

                        data-DeliveryMode_ID      ="{{$CampaignPublisher_index_data->DeliveryMode_ID}}"
                        data-DeliveryModeName     ="{{$CampaignPublisher_index_data->DeliveryModeName}}"

                        data-AgeRangeFrom         ="{{$CampaignPublisher_index_data->AgeRangeFrom}}"
                        data-AgeRangeTo           ="{{$CampaignPublisher_index_data->AgeRangeTo}}"
                        data-FormatValue          ="{{$CampaignPublisher_index_data->FormatValue}}"

                        data-GenderName          ="{{$CampaignPublisher_index_data->GenderName}}"
                        data-Gender_ID          ="{{$CampaignPublisher_index_data->Gender_ID}}"

                        data-SectionName          ="{{$CampaignPublisher_index_data->SectionName}}"
                        data-Section_ID          ="{{$CampaignPublisher_index_data->Section_ID}}"

                        data-AdUnitName          ="{{$CampaignPublisher_index_data->AdUnitName}}"
                        data-AdUnit_ID          ="{{$CampaignPublisher_index_data->AdUnit_ID}}"


                        data-GoogleXCoord         ="{{@$CampaignPublisher_index_data->GoogleXCoord}}"
                        data-GoogleYCoord         ="{{@$CampaignPublisher_index_data->GoogleYCoord}}"
                        data-GoogleLocation       ="{{@$CampaignPublisher_index_data->GoogleLocation}}"
                        data-GoogleRadius         ="{{@$CampaignPublisher_index_data->GoogleRadius}}"

                        data-IsLocationBased      ="{{@$CampaignPublisher_index_data->IsLocationBased}}"
                        data-cityINCLoc          ="{{(json_encode(@$CampaignPublisher_index_data->excludeLoc))}}"
                        data-cityEXLoc           ="{{(json_encode(@$CampaignPublisher_index_data->includeLoc))}}"

                        data-IsTargetingBased      ="{{@$CampaignPublisher_index_data->IsTargetingBased}}"
                        data-Targeting_ID     ="{{(json_encode(@$CampaignPublisher_index_data->Targeting_ID))}}"
                        data-TargetingValue_ID="{{(json_encode(@$CampaignPublisher_index_data->TargetingValue_ID))}}"


                        data-Impression          ="{{@$CampaignPublisher_index_data->Impression}}"  
                        data-DITImp               ="{{@$CampaignPublisher_index_data->DITImp}}"                          
                        data-Clicks               ="{{@$CampaignPublisher_index_data->Clicks}}"
                        data-DITClicks            ="{{@$CampaignPublisher_index_data->DitClicks}}"                         
                        data-Visits               ="{{@$CampaignPublisher_index_data->Visits}}"
                        data-DITVisits            ="{{@$CampaignPublisher_index_data->DITVisits}}"                         
                        data-Views                ="{{@$CampaignPublisher_index_data->Views}}"
                        data-DITViews             ="{{@$CampaignPublisher_index_data->DITViews}}"                         
                        data-Engagement           ="{{@$CampaignPublisher_index_data->Engagement}}"
                        data-DITEngagement        ="{{@$CampaignPublisher_index_data->DITEngagement}}" 
                        data-Leads                ="{{@$CampaignPublisher_index_data->Leads}}"
                        data-DITLeads             ="{{@$CampaignPublisher_index_data->DITLeads}}"                          
                        data-NetRate              ="{{@$CampaignPublisher_index_data->NetRate}}"
                        data-DITNetRate           ="{{@$CampaignPublisher_index_data->DITNetRate}}"                          
                        data-NetCost              ="{{@$CampaignPublisher_index_data->NetCost}}"
                        data-DITNetCost           ="{{@$CampaignPublisher_index_data->DITNetCost}}"                          
                        data-Reach                ="{{@$CampaignPublisher_index_data->Reach}}"
                        data-DITReach             ="{{@$CampaignPublisher_index_data->DITReach}}"                          
                        data-Installs             ="{{@$CampaignPublisher_index_data->Installs}}"
                        data-DITInstalls          ="{{@$CampaignPublisher_index_data->DITInstalls}}"                         
                        data-Spots                ="{{@$CampaignPublisher_index_data->Spots}}"
                        data-DITSpots             ="{{@$CampaignPublisher_index_data->DITSpots}}"                          
                        data-SMS                  ="{{@$CampaignPublisher_index_data->SMS}}"
                        data-DITSMS               ="{{@$CampaignPublisher_index_data->DITSMS}}"                          
                        data-Mailers              ="{{@$CampaignPublisher_index_data->Mailers}}" 
                        data-DITMailers           ="{{@$CampaignPublisher_index_data->DITMailers}}" 

                      >

                    <i class="fa fa-pencil"></i></button>
                    <!-- ---------------------single modal--------------- -->
                    @else
                    <button type="submit" class="btn btn-info  editLineItemCityBank"  !style="width:100%;" 
                     style="margin-bottom: 3px; margin-right:100px;" 

                        data-toggle="modal"
                        data-target="#editLineItemModalCityBank"

                        data-cityid                   ="{{$CampaignPublisher_index_data->id}}"
                        data-cityLineItemID           ="{{$CampaignPublisher_index_data->LineItemID}}"
                        data-cityLineItemName         ="{{$CampaignPublisher_index_data->LineItemName}}"
                        data-cityCampaign_ID          ="{{$CampaignPublisher_index_data->Campaign_ID}}"
                        data-cityPublisher_ID         ="{{$CampaignPublisher_index_data->Publisher_ID}}"
                        data-cityGenreName            ="{{$CampaignPublisher_index_data->GenreName}}"
                        data-cityGenre_ID             ="{{$CampaignPublisher_index_data->Genre_ID}}"
                        data-cityPublisherName        ="{{$CampaignPublisher_index_data->PublisherName}}"
                      >

                    <i class="fa fa-pencil"></i></button>
                    @endif

                    <!--  -->

                  </div>
                  <div  class="col-md-3" >
                      <button type="submit" style="margin-bottom: 3px;" class="btn btn-danger addmore"  
                        title="Delete" aria-hidden="true"

                       onclick="DeleteLineItem('<?php echo $CampaignPublisher_index_data->id; ?>');"
                       >
                       <i class="fa fa-minus-square"> </i>
                      </button>
                  </div>


                  <div  class="col-md-3">
                  @if($getClientDeatils[0]->LineItemType==1)
                    <button
                        type="submit" class="viewLineItem btn btn-warning addmore"  title="View" style="margin-left: 3px;"
                        data-toggle               ="modal"
                        data-target               ="#viewLineItemModal"
                        data-id                   ="{{$CampaignPublisher_index_data->id}}"
                        data-LineItemID           ="{{$CampaignPublisher_index_data->LineItemID}}"
                        data-LineItemName         ="{{$CampaignPublisher_index_data->LineItemID}}_{{$CampaignPublisher_index_data->LineItemName}}"
                        data-Campaign_ID          ="{{$CampaignPublisher_index_data->Campaign_ID}}"
                        data-GenreName            ="{{$CampaignPublisher_index_data->GenreName}}"
                        data-PublisherName        ="{{$CampaignPublisher_index_data->PublisherName}}"
                        data-PhaseName            ="{{$CampaignPublisher_index_data->PhaseName}}"
                        data-ObjectiveValueName   ="{{$CampaignPublisher_index_data->ObjectiveValueName}}"
                        data-ObjectiveName        ="{{$CampaignPublisher_index_data->ObjectiveName}}"
                        data-MediumName           ="{{$CampaignPublisher_index_data->MediumName}}"

                        data-FormatName           ="{{$CampaignPublisher_index_data->FormatName}}"
                        data-FormatTypeName       ="{{$CampaignPublisher_index_data->FormatTypeName}}"
                        data-DealTypeName         ="{{$CampaignPublisher_index_data->DealTypeName}}"
                        data-DeliveryModeName     ="{{$CampaignPublisher_index_data->DeliveryModeName}}"
                        data-AgeRangeFrom         ="{{$CampaignPublisher_index_data->AgeRangeFrom}}"
                        data-AgeRangeTo           ="{{$CampaignPublisher_index_data->AgeRangeTo}}"
                        data-FormatValue          ="{{$CampaignPublisher_index_data->FormatValue}}"

                        data-GenderName          ="{{$CampaignPublisher_index_data->GenderName}}"
                        data-Gender_ID          ="{{$CampaignPublisher_index_data->Gender_ID}}"

                        data-SectionName          ="{{$CampaignPublisher_index_data->SectionName}}"
                        data-Section_ID          ="{{$CampaignPublisher_index_data->Section_ID}}"

                        data-AdUnitName          ="{{$CampaignPublisher_index_data->AdUnitName}}"
                        data-AdUnit_ID          ="{{$CampaignPublisher_index_data->AdUnit_ID}}"


                        data-GoogleLocation      ="{{$CampaignPublisher_index_data->GoogleLocation}}"
                        data-GoogleRadius         ="{{@$CampaignPublisher_index_data->GoogleRadius}}"
                        data-IsLocationBased      ="{{@$CampaignPublisher_index_data->IsLocationBased}}"

                        data-Impression           ="{{$CampaignPublisher_index_data->Impression}}"
                        data-DITImp               ="{{$CampaignPublisher_index_data->DITImp}}"
                        data-Clicks               ="{{$CampaignPublisher_index_data->Clicks}}"
                        data-DitClicks            ="{{$CampaignPublisher_index_data->DitClicks}}"
                        data-Visits               ="{{$CampaignPublisher_index_data->Visits}}"
                        data-DITVisits            ="{{$CampaignPublisher_index_data->DITVisits}}"
                        data-Views                ="{{$CampaignPublisher_index_data->Views}}"
                        data-DITViews             ="{{$CampaignPublisher_index_data->DITViews}}"
                        data-Engagement           ="{{$CampaignPublisher_index_data->Engagement}}"
                        data-DITEngagement        ="{{$CampaignPublisher_index_data->DITEngagement}}"
                        data-Leads                ="{{$CampaignPublisher_index_data->Leads}}"
                        data-DITLeads             ="{{$CampaignPublisher_index_data->DITLeads}}"
                        data-NetRate              ="{{$CampaignPublisher_index_data->NetRate}}"
                        data-DITNetRate           ="{{$CampaignPublisher_index_data->DITNetRate}}"
                        data-NetCost              ="{{$CampaignPublisher_index_data->NetCost}}"
                        data-DITNetCost           ="{{$CampaignPublisher_index_data->DITNetCost}}"
                        data-Reach                ="{{$CampaignPublisher_index_data->Reach}}"
                        data-DITReach             ="{{$CampaignPublisher_index_data->DITReach}}"
                        data-Installs             ="{{$CampaignPublisher_index_data->Installs}}"
                        data-DITInstalls          ="{{$CampaignPublisher_index_data->DITInstalls}}"
                        data-Spots                ="{{$CampaignPublisher_index_data->Spots}}"
                        data-DITSpots             ="{{$CampaignPublisher_index_data->DITSpots}}"
                        data-SMS                  ="{{$CampaignPublisher_index_data->SMS}}"
                        data-DITSMS               ="{{$CampaignPublisher_index_data->DITSMS}}"
                        data-Mailers              ="{{$CampaignPublisher_index_data->Mailers}}"
                        data-DITMailers           ="{{$CampaignPublisher_index_data->DITMailers}}"

                        data-cityINCLoc          ="{{(json_encode(@$CampaignPublisher_index_data->includeLoc))}}"
                        data-cityEXLoc           ="{{(json_encode(@$CampaignPublisher_index_data->excludeLoc))}}"


                    data-Targeting_ID     ="{{(json_encode(@$CampaignPublisher_index_data->Targeting_ID))}}"
                    data-TargetingValue_ID="{{(json_encode(@$CampaignPublisher_index_data->TargetingValue_ID))}}"

                    data-TargetingName     ="{{(json_encode(@$CampaignPublisher_index_data->TargetingName))}}"
                    data-TargetingValueName="{{(json_encode(@$CampaignPublisher_index_data->TargetingValueName))}}"


                       >
                       <i class="fa fa-eye"> </i>

                         </button>
                           @else
                           <button
                               type="submit" class="viewLineItemCity btn btn-warning"  style="margin-left: 3px;"
                               data-toggle               ="modal"
                               data-target               ="#viewLineItemModalCity"
                               data-id                   ="{{$CampaignPublisher_index_data->id}}"
                               data-LineItemID           ="{{$CampaignPublisher_index_data->LineItemID}}"
                               data-LineItemName         ="{{$CampaignPublisher_index_data->LineItemID}}_{{$CampaignPublisher_index_data->LineItemName}}"
                               data-Campaign_ID          ="{{$CampaignPublisher_index_data->Campaign_ID}}"
                               data-GenreName            ="{{$CampaignPublisher_index_data->GenreName}}"
                               data-PublisherName        ="{{$CampaignPublisher_index_data->PublisherName}}"
                               data-PhaseName            ="{{$CampaignPublisher_index_data->PhaseName}}"
                               data-ObjectiveValueName   ="{{$CampaignPublisher_index_data->ObjectiveValueName}}"
                               data-ObjectiveName        ="{{$CampaignPublisher_index_data->ObjectiveName}}"
                               data-MediumName           ="{{$CampaignPublisher_index_data->MediumName}}"
                               data-GenderName          ="{{$CampaignPublisher_index_data->GenderName}}"
                               data-Gender_ID          ="{{$CampaignPublisher_index_data->Gender_ID}}"
                               data-SectionName          ="{{$CampaignPublisher_index_data->SectionName}}"
                               data-Section_ID          ="{{$CampaignPublisher_index_data->Section_ID}}"
                               data-AdUnitName          ="{{$CampaignPublisher_index_data->AdUnitName}}"
                               data-AdUnit_ID          ="{{$CampaignPublisher_index_data->AdUnit_ID}}"
                               data-Impression           ="{{$CampaignPublisher_index_data->Impression}}"
                               data-DITImp               ="{{$CampaignPublisher_index_data->DITImp}}"
                               data-Clicks               ="{{$CampaignPublisher_index_data->Clicks}}"
                               data-DitClicks            ="{{$CampaignPublisher_index_data->DitClicks}}"
                               data-Leads                ="{{$CampaignPublisher_index_data->Leads}}"
                               data-DITLeads             ="{{$CampaignPublisher_index_data->DITLeads}}"
                               data-Rate                ="{{$CampaignPublisher_index_data->Rate}}"
                               data-DITRate            ="{{$CampaignPublisher_index_data->DitRate}}"
                               data-Cost                ="{{$CampaignPublisher_index_data->Cost}}"
                               data-DITCost            ="{{$CampaignPublisher_index_data->DitCost}}"
                               data-Opens               ="{{$CampaignPublisher_index_data->Opens}}"
                               data-DITOpens            ="{{$CampaignPublisher_index_data->DitOpens}}"

                              >
                              <i class="fa fa-eye"> </i>
                          @endif

                  </div>

                  <div  class="col-md-3">
                    
                   
                  </div>

                </div>
              </td>
            </tr>
          <?php $i++; ?>
          @endforeach
          </form>
        </table>
        <div class="navbar-fixed-bottom pull-left navbar-default navbar-collapse" style="padding:-100px 0px; border-top:1px solid #ddd;">
            <strong> &nbsp; &nbsp;Powered by &nbsp;</strong><a href="http://www.e-stonetech.com" target="_blank"> e-Stone Information Technology Pvt. Ltd. &copy; 2016-17</a>
        </div>
      </div>
    </div>
  </div>
<!-- edit line item -->

    <div class="pull-right margin-top-20">
      <div id="editLineItemModal" class="modal fade" tabindex="-1" data-focus-on="input:first">
        <div class="modal-dialog" role="document">
          <div class="modal-content" style= "width: 800px;">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="gridModalLabel" style="text-align:center;" >Edit Line Item Details</h4>
            </div>
            <div class="modal-body" id="register"   >

              {!!Form::open(['url' => '/CampaignPublisherEdit', 'id'=>"modal_add",'method' => 'post', 'class'=>'form-horizontal form-row-seperated'])!!}
              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

              <input type="hidden" name="LineItem_ID" id="LineItem_ID1">
              <input type="hidden" name="Campaign_ID" id="Campaign_ID1">
              <input type="hidden" name="LineItemID" id="LineItemID1">

              <div class="form-group row ">
                <div class=" col-md-1"><strong>Name</strong></div>
                <div class="col-md-10">
                   <input type="text" placeholder="Line Item Name" class="form-control" name='LineItemName' value=""  id="LineItemName1" />
                </div>
              </div>
              <hr/>
               <!-- <div class="form-group row ">
                <div class=" col-md-1 "><strong>Genre</strong></div>
                <div class="col-md-3">
                  <select  id="genreDetails" name="Genre_ID"  class="form-control"></select>
                </div>
                <div class="col-md-1 "><strong>Publisher<br>Name</strong></div>
                <div class="col-md-3">
                  <select id="PublisherDetails" name="Publisher_ID" class="form-control publishers"></select>
                </div>
              </div>
              <hr/> -->

              <!-- ================edit Genre-Publisher selection================================ -->
              <div class="form-group row ">
                <div class=" col-md-1 "><strong>Genre</strong></div>
                <div class="col-md-3">
                  <select name="Genre_ID" id='Genre_ID1' class="form-control map-genre-publisher allGenre_ID" >
                    <option value="" data-for="selected-genre">Select</option>
                      @foreach($Genre as $bp)
                        <option value="{{ $bp->id }}" data-for="selected-genre">{{ $bp->GenreName }}</option>
                      @endforeach
                  </select>
                </div>
                <div class="col-md-1 "><strong>Publisher<br>Name</strong></div>
                <div class="col-md-3">
                  <select id="Publisher_ID" name="Publisher_ID" class="form-control publishers allPublisher selectedPublisher" >
                    <option value="" data-for="selected-brand">Select</option>
                  </select>
                </div>
              </div>
              <hr/>
               <!-- ================edit Genre-Publisher selection================================ -->
              <div class="form-group row ">
                <div class=" col-md-1 "><strong>Phase</strong></div>
                <div class="col-md-3">

                  <select name="Phase_ID" id='Phase_ID1' class="form-control allPhaseIds">
                      <option value="">Select</option>
                      @foreach($Phase as $ph)
                        <option value="{{ $ph->id }}">{{ $ph->PhaseName}}</option>
                      @endforeach
                  </select>
                </div>
                <div class=" col-md-1 "><strong>Objective </strong></div>
                <div class="col-md-3">
                  <select name="Objective_ID" id='Objective_ID1' class="form-control  allObjectiveIds map-objective-objectiveValue" >
                     <option value="">Select</option>
                    @foreach($Objective as $obj)
                      <option value="{{$obj->id}}" >{{ $obj->ObjectiveName}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="col-md-1"><strong>Objective Value</strong></div>
                <div class="col-md-3">
                  <select name="ObjectiveValue_ID" id="ObjectiveValue_ID1" class="form-control allObjectiveValueIds objectiveValuess objectiveValues" >
                  </select>
                </div>
              </div>
              <hr/>

              <div class="form-group row ">
                <div class="col-md-1"><strong>Medium</strong></div>
                <div class="col-md-3">
                 <select name="Medium_ID" id='Medium_ID1' class="form-control allMediumIds">
                    <option value="">Select</option>
                    @foreach($Medium as $medium)
                    <option value="{{$medium->id}}">{{$medium->MediumName}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-1 "><strong>Deal Type</strong></div>
                <div class="col-md-3">
                 <select id="DealType_ID1" name="DealType_ID" class="form-control allDealTypeIds">     <option value="">Select</option>
                      @foreach($DealType as $DType)
                      <option value="{{ $DType->id }}">{{ $DType->DealTypeName }}</option>
                      @endforeach
                  </select>
                </div>

                <div class="col-md-1 "><strong>Delivery Type</strong></div>
                <div class="col-md-3">
                  <select id="DeliveryMode_ID1" name="DeliveryMode_ID" class="form-control allDeliveryModeIds">
                       <option value="">Select</option>
                      @foreach($DeliveryMode as $mode)
                      <option value="{{ $mode->id }}">{{ $mode->DeliveryModeName }}</option>
                      @endforeach
                  </select>
                </div>

                <div class="col-md-1 ">

                </div>
                <div class="col-md-3">

                </div>
              </div>
              <hr/>

              <!-- ------------------------------------------------------------------- -->
              <div class="form-group row ">
                <div class=" col-md-1 "><strong>Targeting:</strong></div>
                <div class="col-md-2">
                  <label  name="target" id='age' value="1">&nbsp; Age: </label>
                  <div  class="ageRange" >
                    &nbsp;AgeFrom: &nbsp;<input type="text" class="form-control" id="AgeRangeFromE" name="target_ageRangeFrom">
                    &nbsp;AgeTo: &nbsp; <input type="text" class="form-control" id="AgeRangeToE" name="target_ageRangeTo">
                  </div>
                </div>

                <div class="col-md-2">
                  <label  name="target" id='age' value="1"> Gender: </label>
                  <select name="Gender_ID" id='Gender_ID' class="form-control allGenderIdds"  >
                    <option value="" data-for="">Select</option>
                     @foreach($Gender as $gender)
                        <option value="{{ $gender->id }}" data-for="">{{ $gender->GenderName }}</option>
                      @endforeach
                  </select>
                </div>
                  <!--purposly given same className  for Google and City Location-->

                <div class="col-md-2">
                  <label  name="target" id='age' value="1"> GoogleLocation: </label>
                  <button name="target" id='gLocation' class="trgtLocation" value='gLoc' onclick="return popitup('<?php echo $base_url;?>/gmap')">add Location</button>

                  <div>
                    <label id="Glocation" name="GoogleLocation">Loaction:</label><label id="GlocationValue"></label>
                    <label id="GDistance" name="GoogleRadius">Distance:</label><label id="GDistanceValue"></label>
                    <input type="hidden" class="form-control" name="GoogleXCoord" id="GoogleXCoord">
                    <input type="hidden" class="form-control" name="GoogleYCoord" id="GoogleYCoord">
                    <input type="hidden" class="form-control" name="GoogleLocation" id="GoogleLocation1">
                    <input type="hidden" class="form-control" name="GoogleRadius" id="GoogleRadius1">
                  </div>
                  <label id='addrs_edit'></label> <label id='dists_edit'></label>
                </div>

                <div class="col-md-2">
                  <label  name="target" id='age' value="1"> City Location: </label>
                  <button name="target" id='cLocation' class="trgtLocation" value="cLoc" onclick="return popup('<?php echo $base_url;?>/city')" >City Loaction</button>
                  <div>
                    <label id="incLoc">Included:</label><label class="incLocValue"></label>
                    <label id="excLoc">Excluded:</label><label class="excLocValue"></label>
                    <!-- onCompulsary selction hidden fields for city -->
                  </div>
                  <label id='locationInclude_edit'></label> <label id='locationExclude_edit'></label>
                </div>

                <div class="col-md-3">
                  <label  name="target" id='age' value="1"> Others: </label>
                  <select multiple name="Targeting_ID[]" id='Targeting_ID' class="form-control map-targeting-targetingValue">
                      <!-- <option value="" data-for="">Select</option> -->
                      @foreach($Targeting as $tgt)
                        <option value="{{ $tgt->id }}" class="targetSearch{{$tgt->id}}" data-for="">{{ $tgt->TargetingName }}</option>
                      @endforeach
                  </select>
                  <br>
                  <select multiple name="TargetingValue_ID[]" class="form-control TargetingValuesEdit targetingValuesEdit">
                  </select>
                </div>
              </div>
              <hr/>
              <!--  ----------------------------------------------------------->
              <div class="form-group row ">
                <div class=" col-md-1 "><strong>Format Type </strong></div>
                <div class="col-md-3">
                  <select name="FormatType_ID" id='FormatType_ID1' class="form-control allFormatTypeIds map-formatType-format" >
                       <option value="">Select</option>
                      @foreach($FormatType as $formatType)
                      <option value="{{$formatType->id}}"  >{{ $formatType->FormatTypeName }}</option>
                      @endforeach
                  </select>
                </div>

                <div class="col-md-1 "><strong>Format</strong></div>
                <div class="col-md-3" >
                  <select id="Format_ID" name="Format_ID" class="form-control formatss formats" >
                     @foreach($Format as $format)
                    <option id="FormatName1" data-for="">{{ $format->FormatName }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="col-md-1 ">
                  <strong>Format Value</strong>
                  <strong><p class="formatValueName"></p></strong>
                </div>
                <div class="col-md-3">
                  <input type="text" name="FormatValue" class="form-control" id="FormatValue1" />
                </div>

              </div>
              <hr/>
              <!-- --------------------------------------------------------- -->
              <div class="form-group row ">
                <div class=" col-md-1 ">
                  <strong>Section</strong>
                </div>
                <div class="col-md-3">
                  <select name="Section_ID" id='Section_ID' class="form-control allSectionIds" >
                    <option value="" data-for="selected-genre">Select</option>
                      @foreach($Section as $sect)
                        <option value="{{ $sect->id }}" data-for="selected-section">{{ $sect->SectionName }}</option>
                      @endforeach
                  </select>
                </div>

                <div class="col-md-1 ">
                  <strong>AdUnit</strong>
                </div>
                <div class="col-md-3">
                  <select id="AdUnit_ID" name="AdUnit_ID" class="form-control allAdUnitIds" >
                     <option value="" data-for="selected-genre">Select</option>
                      @foreach($AdUnit as $au)
                        <option value="{{ $au->id }}" data-for="selected-section">{{ $au->AdUnitName }}</option>
                      @endforeach
                  </select>
                </div>
                </div>
                <hr/>

                <!-- -------------------------------------------------------------------- -->
                <div class="form-group row ">
                <div class="col-md-2"></div>
                <div class="col-md-2" style="text-align:center;"><strong>Plan </strong></div>
                <div class="col-md-2"><strong>Data InputType</strong></div>
                <div class="col-md-2"> </div>
                <div class="col-md-2" style="text-align:center"><strong>Plan </strong></div>
                <div class="col-md-2"><strong>Data InputType</strong></div>
              </div>
              <hr/>

              <div class="form-group row ">
                <div class="col-md-2">
                  <strong>Impression:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" class="form-control imp_val" name="Impressions" id="Impression1" placeholder = "Impression" onkeypress="return isNumberKey(event)" />
                </div>
                <div class="col-md-2">
                  <select  id="PIDataInputTypeID1" name="PIDataInputTypeID" class="form-control PIDataInputTypeID imp_val_dit"  >
                    <option value="" data-for="PIDataInputTypeID">Select</option>
                  </select>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-1 ">
                  <strong>Clicks:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" class="form-control click_val" name='Clicks' value=""  id="Clicks1" placeholder = "Clicks" onkeypress="return isNumberKey(event)"  />
                </div>
                <div class="col-md-2">
                  <select id="PCDataInputTypeID1" name="PCDataInputTypeID" class="form-control PCDataInputTypeID click_val_dit" >
                    <option value="" data-for="PCDataInputTypeID">Select</option>
                  </select>
                </div>
              </div>


              <div class="form-group row ">
                <div class="col-md-2">
                  <strong>Visits:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" placeholder="Visits" class="form-control visit_val" name='Visits' value=""  id="Visits1"  onkeypress="return isNumberKey(event)" />
                </div>
                <div class="col-md-2">
                  <select id="PVsDataInputTypeID1" name="PVsDataInputTypeID" class="form-control PVsDataInputTypeID visit_val_dit" >
                    <option value="" data-for="PVsDataInputTypeID">Select</option>
                  </select>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-1 ">
                  <strong>Views:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" placeholder="Views" class="form-control view_val" name='Views' value=""  id="Views1" onkeypress="return isNumberKey(event)" />
                </div>
                <div class="col-md-2">
                  <select id="PVwDataInputTypeID1" name="PVwDataInputTypeID" class="form-control PVwDataInputTypeID view_val_dit" >
                    <option value="" data-for="PVwDataInputTypeID">Select</option>
                  </select>
                </div>
              </div>

              <div class="form-group row ">
                <div class="col-md-2 ">
                  <strong>Engagement:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" placeholder="Engagement" class="form-control eng_val" name='Engagement' value=""  id="Engagement1" onkeypress="return isNumberKey(event)" />
                </div>
                <div class="col-md-2">
                  <select id="PEDataInputTypeID1" name="PEDataInputTypeID" class="form-control PEDataInputTypeID eng_val_dit" >
                    <option value="" data-for="PEDataInputTypeID">Select</option>
                  </select>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-1 ">
                  <strong>Leads:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" placeholder="Leads" class="form-control lead_val" name='Leads' value=""  id="Leads1" onkeypress="return isNumberKey(event)" />
                </div>
                <div class="col-md-2">
                  <select id="PLDataInputTypeID1" name="PLDataInputTypeID" class="form-control PLDataInputTypeID lead_val_dit" >
                    <option value="" data-for="PLDataInputTypeID">Select</option>
                  </select>
                </div>
              </div>

              <div class="form-group row ">
                <div class="col-md-2 ">
                  <strong>Net Rate:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" placeholder="Net Rate" class="form-control netRate_val" name='NetRate' value=""  id="NetRate1" onkeypress="return isNumberKey(event)" />
                </div>
                <div class="col-md-2">
                  <select id="PNRDataInputTypeID1" name="PNRDataInputTypeID" class="form-control PNRDataInputTypeID netRate_val_dit"  >
                    <option value="" data-for="PNRDataInputTypeID">Select</option>
                  </select>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-1 ">
                  <strong>Net Cost:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" placeholder="Net Cost" class="form-control netCost_val" name='NetCost' value=""  id="NetCost1" onkeypress="return isNumberKey(event)" />
                </div>
                <div class="col-md-2">
                  <select id="PNCDataInputTypeID1" name="PNCDataInputTypeID" class="form-control PNCDataInputTypeID netCost_val_dit" >
                    <option value="" data-for="PNCDataInputTypeID">Select</option>
                  </select>
                </div>
              </div>

              <div class="form-group row ">
                <div class="col-md-2">
                  <strong>Reach:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" placeholder="Reach" class="form-control reach_val" name='Reach' value=""  id="Reach1" onkeypress="return isNumberKey(event)" />
                </div>
                <div class="col-md-2">
                  <select id="PRDataInputTypeID1" name="PRDataInputTypeID" class="form-control PRDataInputTypeID reach_val_dit" >
                    <option value="" data-for="PRDataInputTypeID">Select</option>
                  </select>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-1 ">
                  <strong>Installs:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" placeholder="Installs" class="form-control install_val" name='Installs' value=""  id="Installs1" onkeypress="return isNumberKey(event)" />
                </div>
                <div class="col-md-2">
                  <select id="PInDataInputTypeID1" name="PInDataInputTypeID" class="form-control PInDataInputTypeID install_val_dit" >
                    <option value="" data-for="PInDataInputTypeID">Select</option>
                  </select>
                </div>
              </div>

              <div class="form-group row ">
                <div class="col-md-2 ">
                  <strong>Spots:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" placeholder="Spots" class="form-control spot_val" name='Spots' value=""  id="Spots1" onkeypress="return isNumberKey(event)" />
                </div>
                <div class="col-md-2">
                  <select id="PSpDataInputTypeID1" name="PSpDataInputTypeID" class="form-control PSpDataInputTypeID spot_val_dit" >
                    <option value="" data-for="PSpDataInputTypeID">Select</option>
                  </select>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-1 ">
                  <strong>SMS:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" placeholder="SMS" class="form-control sms_val" name='SMS' value=""  id="SMS1" onkeypress="return isNumberKey(event)" />
                </div>
                <div class="col-md-2">
                  <select id="PSmDataInputTypeID1" name="PSmDataInputTypeID" class="form-control PSmDataInputTypeID sms_val_dit" >
                    <option value="" data-for="PSmDataInputTypeID">Select</option>
                  </select>
                </div>
              </div>
              <div class="form-group row ">
                <div class="col-md-2">
                  <strong>Mailers:</strong>
                </div>
                <div class="col-md-2">
                  <input type="text" placeholder="Mailers" class="form-control mailer_val" name='Mailers' value=""  id="Mailers1" onkeypress="return isNumberKey(event)" />

                </div>
                <div class="col-md-2">
                  <select id="PMDataInputTypeID1" name="PMDataInputTypeID" class="form-control PMDataInputTypeID mailer_val_dit" >
                    <option value="" data-for="PMDataInputTypeID">Select</option>
                  </select>
                </div>
              </div>
              <hr/>
                <!-- -------------------------------------------------------------------- -->

               <input type='hidden' name='CampaignID' value="{{$CampaignID}}" id="Campaign_id">

              <div class="form-group row text-center">
                <button type="submit" class="btn btn-primary checkAllMatrixDitPair">Save</button>
              </div>
               {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- end edit full line item -->

  
<!-- start of edit city bank modal -->
<div class="pull-right margin-top-20">
  <div id="editLineItemModalCityBank" class="modal fade" tabindex="-1" data-focus-on="input:first">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style= "width: 800px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="gridModalLabel" style="text-align:center;" >Edit Line Item Details</h4>
        </div>
        <div class="modal-body" id="register"   >

          {!!Form::open(['url' => '/CampaignPublisherEdit', 'id'=>"modal_add",'method' => 'post', 'class'=>'form-horizontal form-row-seperated'])!!}
          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

          <input type="hidden" name="LineItem_ID" id="cityLineItem_ID1">
          <input type="hidden" name="Campaign_ID" id="cityCampaign_ID1">
          <input type="hidden" name="LineItemID" id="cityLineItemID1">

          <div class="form-group row ">
            <div class=" col-md-1"><strong>Name</strong></div>
            <div class="col-md-10">
               <input type="text" placeholder="Line Item Name" class="form-control" name='LineItemName' value=""  id="cityLineItemName1" />
            </div>
          </div>
          <hr/>
           <div class="form-group row ">
            <div class=" col-md-1 "><strong>Genre</strong></div>
            <div class="col-md-3">
              <select  id="citygenreDetails" name="Genre_ID"  class="form-control"></select>
            </div>
            <div class="col-md-1 "><strong>Publisher<br>Name</strong></div>
            <div class="col-md-3">
              <select id="cityPublisherDetails" name="Publisher_ID" class="form-control publishers"></select>
            </div>
          </div>
          <hr/>


           <input type='hidden' name='CampaignID' value="{{$CampaignID}}" id="Campaign_id">

          <div class="form-group row text-center">
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
           {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--End of edit city bank modal-->
  <!-- view line item -->
  <div class="pull-right margin-top-20">
      <div id="viewLineItemModal" class="modal fade" tabindex="-1" data-focus-on="input:first">
        <div class="modal-dialog" role="document">
          <div class="modal-content" style= "width: 800px;">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="gridModalLabel" style="text-align:center;" >Line Item Details</h4>
            </div>
            <div class="modal-body" id="register"   >

              {!!Form::open(['url' => '', 'id'=>"modal_add",'method' => 'post', 'class'=>'form-horizontal form-row-seperated'])!!}
              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

              <div class="form-group row ">


                <div class=" col-md-1 ">Name:</div>
                <div class="col-md-11"><label  id="lineItemName"></label></div>


              </div>
              <hr/>
              <div class="form-group row ">
                  <div class=" col-md-1 ">Genre:</div>
                  <div class="col-md-3"><label  id="GenreName"></label> </div>
                  <div class="col-md-1 ">Publisher Name:</div>
                  <div class="col-md-3"><label  id="PublisherName"></label></div>
              </div>
              <hr/>





              <div class="form-group row ">
                <div class=" col-md-1 ">Phase:</div>
                <div class="col-md-3"><label  id="PhaseName"></label></div>
                <div class="col-md-1 ">Objective:</div>
                <div class="col-md-3"><label  id="ObjectiveName"></label></div>
                <div class="col-md-1 ">Objective Value:</div>
                <div class="col-md-3"><label id="ObjectiveValueName"></label></div>
              </div>
              <hr/>

              <div class="form-group row ">
                <div class="col-md-1 ">Medium:</div>
                <div class="col-md-3"><label  id="MediumName"></label> </div>
                <div class="col-md-1 ">Deal Type:</div>
                <div class="col-md-3"><label  id="DealTypeName"></label></div>
                <div class="col-md-1 ">Delivery Type:</div>
                <div class="col-md-3"><label  id="DeliveryModeName"></label></div>
                <div class="col-md-1 "></div>
                <div class="col-md-3"></div>
              </div>
              <hr/>

              <div class="form-group row ">
                <div class="col-md-1"> Targeting:</div>

                <div class="col-md-2">
                  <label>Age:</label>
                  <div  class="ageRange" >
                    <label id="ageFrmLabel" >Age From:</label> <label  id="AgeRangeFrom"></label>
                    <label id="ageToLabel" >Age To:</label> <label  id="AgeRangeTo"></label>
                  </div>
                </div>

                <div class="col-md-1">
                 <label>Gender:</label>  <br>
                  <label  id="targetingValueGender"></label>
                </div>
                  <!--purposly given same className  for Google and City Location-->
                <div class="col-md-2">
                  <label>Google Location: </label><br>
                  <label  id="gooleLoc"></label> <br>
                  <label>Distance:</label><label  id="googleRadius"></label>
                </div>

                <div class="col-md-3">
                  <label>City Location: </label><br>
                  <label id="incLocLabel" >Included:</label><label  id="cityInLoc"></label>
                  <label id="exLocLabel" >Excluded:</label><label  id="cityExLoc"></label>
                </div>

                <div class="col-md-3">
                  <label>Others: </label>
                  <label >Targeting:</label><label  id="TargetingNameOther"></label>
                  <label>Targeting Value:</label><label  id="targetingValueOthers"></label>
                </div>
              </div>
              <hr/>

              <div class="form-group row ">
                <div class=" col-md-2 ">Format Type:</div>
                <div class="col-md-2"><label id="FormatTypeName"></label></div>

                <div class="col-md-1 ">Format:</div>
                <div class="col-md-3" ><label  id="FormatName"></label> </div>

                <div class="col-md-1 ">Format Value:</div>
                <div class="col-md-3"><label id="FormatValue"></label>
                </div>
              </div>
              <hr/>



              <div class="form-group row ">


                <div class="col-md-1 ">Section:</div>
                <div class="col-md-3" ><label  id="Section"></label> </div>

                <div class="col-md-1 ">AdUnit:</div>
                <div class="col-md-3"><label id="AdUnit"></label>
                </div>
              </div>
              <hr/>



              <div class="form-group row ">
                <div class="col-md-1"></div>
                <div class="col-md-3" style="text-align:center;">Plan </div>
                <div class="col-md-2">Data InputType</div>
                <div class="col-md-1"> </div>
                <div class="col-md-3" style="text-align:center">Plan </div>
                <div class="col-md-2">Data InputType</div>
              </div>
              <hr/>

              <div class="form-group row ">
                <div class="col-md-2">Impression:</div>
                <div class="col-md-2"><label id="Impression"></div>
                <div class="col-md-2"><label id="DitIMP"></div>

                <div class="col-md-1"></div>
                <div class="col-md-1 ">Clicks:</div>
                <div class="col-md-2"><label id="Clicks"></div>
                <div class="col-md-2"><label id="DitClicks"></div>
              </div>


              <div class="form-group row ">
                <div class="col-md-2">Visits:</div>
                <div class="col-md-2"><label id="Visits"></div>
                <div class="col-md-2"><label id="DITVisits"></div>

                <div class="col-md-1"></div>
                <div class="col-md-1 ">Views: </div>
                <div class="col-md-2"><label id="Views"></div>
                <div class="col-md-2"><label id="DITViews"></div>
              </div>

              <div class="form-group row ">
                <div class="col-md-2 ">Engagement:</div>
                <div class="col-md-2"><label id="Engagement"></div>
                <div class="col-md-2"><label id="DITEngagement"></div>

                <div class="col-md-1"></div>
                <div class="col-md-1 ">Leads:</div>
                <div class="col-md-2"><label id="Leads"></div>
                <div class="col-md-2"><label id="DITLeads"></div>
              </div>

              <div class="form-group row ">
                <div class="col-md-2 ">Net Rate:</div>
                <div class="col-md-2"><label id="NetRate"></div>
                <div class="col-md-2"><label id="DITNetRate"></div>

                <div class="col-md-1"></div>
                <div class="col-md-1 ">Net Cost:</div>
                <div class="col-md-2"><label id="NetCost"></div>
                <div class="col-md-2"><label id="DITNetCost"></div>
              </div>

              <div class="form-group row ">
                <div class="col-md-2">Reach:</div>
                <div class="col-md-2"><label id="Reach"></div>
                <div class="col-md-2"><label id="DITReach"></div>

                <div class="col-md-1"></div>
                <div class="col-md-1 ">Installs:</div>
                <div class="col-md-2"><label id="Installs"></div>
                <div class="col-md-2"><label id="DITInstalls"></div>
              </div>

              <div class="form-group row ">
                <div class="col-md-2 ">Spots:</div>
                <div class="col-md-2"><label id="Spots"></div>
                <div class="col-md-2"><label id="DITSpots"></div>

                <div class="col-md-1"></div>
                <div class="col-md-1 ">SMS:</div>
                <div class="col-md-2"><label id="SMS"></div>
                <div class="col-md-2"><label id="DITSMS"></div>
              </div>

              <div class="form-group row ">
                <div class="col-md-2">Mailers:</div>
                <div class="col-md-2"><label id="Mailers"></div>
                <div class="col-md-2"><label id="DITMailers"></div>
              </div>
              <hr/>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
  </div>
  <!--view line item city-->
  <div class="pull-right margin-top-20">
      <div id="viewLineItemModalCity" class="modal fade" tabindex="-1" data-focus-on="input:first">
        <div class="modal-dialog" role="document">
          <div class="modal-content" style= "width: 800px;">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="gridModalLabel" style="text-align:center;" >View Line Item Details</h4>
            </div>
            <div class="modal-body" id="register"   >

              {!!Form::open(['url' => '', 'id'=>"modal_add",'method' => 'post', 'class'=>'form-horizontal form-row-seperated'])!!}
              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

              <div class="form-group row ">


                <div class=" col-md-1 ">Name:</div>
                <div class="col-md-11"><label  id="lineItemNameCity"></label></div>


              </div>
              <hr/>
              <div class="form-group row ">
                  <div class=" col-md-1 ">Genre:</div>
                  <div class="col-md-3"><label  id="GenreNameCity"></label> </div>
                  <div class="col-md-1 ">Publisher Name:</div>
                  <div class="col-md-3"><label  id="PublisherNameCity"></label></div>
              </div>
              <hr/>

              <div class="form-group row ">
                <div class="col-md-1"></div>
                <div class="col-md-3" style="text-align:center;">Plan </div>
                <div class="col-md-2">Data InputType</div>
                <div class="col-md-1"> </div>
                <div class="col-md-3" style="text-align:center">Plan </div>
                <div class="col-md-2">Data InputType</div>
              </div>
              <hr/>

              <div class="form-group row ">
                <div class="col-md-2">Impression:</div>
                <div class="col-md-2"><label id="ImpressionCity"></div>
                <div class="col-md-2"><label id="DitIMPCity"></div>

                <div class="col-md-1"></div>
                <div class="col-md-1 ">Clicks:</div>
                <div class="col-md-2"><label id="ClicksCity"></div>
                <div class="col-md-2"><label id="DitClicksCity"></div>
              </div>

              <div class="form-group row ">
                <div class="col-md-2 ">Opens:</div>
                <div class="col-md-2"><label id="OpensCity"></div>
                <div class="col-md-2"><label id="DITOpensCity"></div>

                <div class="col-md-1"></div>    
                <div class="col-md-1">Leads:</div>
                <div class="col-md-2"><label id="LeadsCity"></div>
                <div class="col-md-2"><label id="DITLeadsCity"></div>                 
              </div>

              <div class="form-group row ">              
                <div class="col-md-2 ">Rate:</div>
                <div class="col-md-2"><label id="RateCity"></div>
                <div class="col-md-2"><label id="DITRateCity"></div>

                <div class="col-md-1"></div> 
                <div class="col-md-1 ">Cost:</div>
                <div class="col-md-2"><label id="CostCity"></div>
                <div class="col-md-2"><label id="DITCostCity"></div>                  
              </div>
              <hr/>

              <div class="form-group row">
                <div class=" col-md-1 ">
                  <strong>Card</strong>
                </div>
                <div class="col-md-3">
                  <select class="form-control cardview" multiple disabled>

                  </select>
                </div>

                <div class="col-md-1 ">
                  <strong>Creative</strong>
                </div>
                <div class="col-md-3">
                  <select  class="form-control map-dit-creative creativeview" multiple disabled>

                  </select>
                </div>

                </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
  </div>


  <link rel="stylesheet" href="{{asset('css/fastselect.minPublisherSelect.css')}}">
 




  <!-- -->
</div>





<script>
  $(document).ready(function() {
    var table;
    var url = '';

    table = $('#example').DataTable({
      "bProcessing" : true,
      "bPaginate" : true,
      "pagingType": "simple",
      "aaSorting": [],
    });

    if( {{$excelCount = DB::table('RawExcelData')->count()}} > 0){
      // console.log('{{$excelCount = DB::table('RawExcelData')->count()}} ');
        $('#errorSheet').show();
    }else{
       $('#errorSheet').hide();
    }
  });

$(document).on('change', '.selectApiPublisher', function() {
  var getApiPublisherId = $(this).val();
  var campStartDate = $("#campStartDate").val();
  var Campaign_ID = $("#Campaign_ID").val();
  //alert(getApiPublisherId); alert(campStartDate); 
  //alert(Campaign_ID);
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('meta[name=_token]').attr('content')
    }
  });

  $.ajax({
    type: 'POST',
    url: '{{ url('/getDataforVariousApi') }}', 
    //data: { "_token": "{{ csrf_token() }}",'getApiPublisherId':getApiPublisherId, 'campStartDate':campStartDate,'Campaign_ID':Campaign_ID},
     data: { "_token": "{{ csrf_token() }}",'getApiPublisherId':getApiPublisherId,'campStartDate':campStartDate},
    success: function (data ) {
      console.log(data);
    }
  }); 
});

$(document).ready(function () {
    $('.attireMainNav').hide();

    $('.multipleInput').fastselect({url: '<?php echo $base_url;?>/pubFastSelect',
        loadOnce: false,
        apiParam: 'query',
        clearQueryOnSelect: true,
        minQueryLength: 1,
        focusFirstItem: true,
        flipOnBottom: true,
        typeTimeout: 150,
        userOptionAllowed: true,
        valueDelimiter: ',',  
        onItemSelect:null,
        maxItems:1,
        placeholder: 'Choose option',
        searchPlaceholder: 'Search options',
        noResultsText: 'No results',
        userOptionPrefix: 'Add '}).bind(this);  
  });
</script>

</body>
</html>
