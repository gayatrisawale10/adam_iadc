<div class="row col-md-6">	
  <div class="col-xs-6 col-sm-6 col-md-6">
    <div class="form-group">
      <strong>Client Name:</strong>
    </div>
      <div class="form-group">
        {!! Form::text('ClientName', $getClientName, array('placeholder' => 'Client name','class' => 'form-control','required'=>'', 'maxlength'=>'50')) !!}
      </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">    
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary pull-left">Save</button>
      <div class="pull-left" style="padding-left:20px;">
          <a class="btn btn-primary" href="{{ route('Client.index') }}"> Back</a>
      </div>
    </div>
  </div>

</div>
