@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left"><h2>Publisher Details</h2></div>
            <div class="pull-right margin-top-20">
                <a class="btn btn-success" href="{{route('Publisher.create') }}">Create Publisher </a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="portlet-body">
        <div class="table-toolbar">
            <div style="width: 100%; overflow-x:auto;margin-bottom: 50px;">
                <table class="table table-bordered display" id="sample_editable_1">
                    <thead> 
                        <tr>
                            <th>Publisher Name</th>
                            <th>Genre Name</th>
                            <th>Publisher Type</th>
                            <th width="280px">Action</th>
                        </tr>
                    </thead> 
                    <tbody> 
                       @foreach ($Publisherdetails_index as $Publisherdetails_index_data)
                        <tr align="center">
                            <td>{{$Publisherdetails_index_data->PublisherName}}</td>
                            <td>{{$Publisherdetails_index_data->GenreName}}</td>
                            <td>{{$Publisherdetails_index_data->PublisherTypeName}}</td>
                            <td>
                               <a class="btn btn-primary" href="{{ route('Publisher.edit',$Publisherdetails_index_data->id) }}">Edit</a>

                               <a class="btn btn-primary" href="{{route('Publisher.credentials',['publisherId'=>$Publisherdetails_index_data->id])}}">Credentials</a> 
                              
                               {!! Form::open(['method' => 'DELETE','route' => ['Publisher.destroy',$Publisherdetails_index_data->id],'style'=>'display:inline']) !!}
                               {!! Form::submit('Delete', ['class' => 'btn btn-danger', 'onclick'=>'return confirm("Are you sure you want to delete?")']) !!}
                               {!! Form::close() !!}


                                 

                            </td>
                        </tr>
                       @endforeach
                    </tbody>
                </table>
            @endsection
        </div>
    </div>
</div>