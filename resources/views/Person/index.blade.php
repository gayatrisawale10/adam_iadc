@extends('layouts.app')

@section('content')
<div class="container"> 
	<div class="row">
		<div class="col-md-5">
			@if(Session::has('error'))
				<div class="alert alert-danger">
				  {{ Session::get('error')}}
				</div>
			@endif

			@if(Session::has('success'))
				<div class="alert alert-success">
				  {{ Session::get('success')}}
				</div>
			@endif
		</div>

		
	    <div class="col-lg-12 pull-left">
	            <h2>Person Details</h2>        
	    </div>

		<div class="col-md-12">
			<button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target=".bs-example-modal-sm">Create Person</button>
		</div>
	</div>
	<div class="row">
		<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
			<div class="modal-dialog modal-sm" role="document">
			    <div class="modal-content" style="padding: 12px;">
			    	<form method="POST" action="{{route('savePerson')}}">
			    		{{ csrf_field() }}

					  	<div class="form-group">
						  	<label for="exampleInputEmail1">Name</label>
						    <input type="text"  class="form-control" required="" name="name" placeholder="Full Name">
					  	</div>

						<div class="form-group">
						    <label for="exampleInputEmail1">Email address</label>
						    <input type="email"  class="form-control" required="" name="email"  placeholder="Email">
						</div>		  
					  	<div class="form-group">
					  		<label for="exampleInputEmail1">Role</label>
						    <select class="form-control" name="role" required="">
						    	<option value="">Select User Role</option>
						    	@foreach($role as $r)
								  	<option value="{{$r->id}}">{{$r->UserRoleName}}</option>
								@endforeach
						    </select>
					  	</div>
						<div class="form-group">
						    <label for="exampleInputPassword1">Password</label>
						    <input  type="password" required="" name="password" class="form-control" placeholder="Password">
						</div>
					  	<button type="submit" class="btn btn-success btn-block">Submit</button>
					</form>
			    </div>
			</div>
		</div>
		

		<div class="row" style="margin-top: 15px;">
			<table  class="table table-bordered display" id="sample_editable_1">
				<thead class="success">
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Email</th>
						<th>Role</th>
						<th>Action</th>
					</tr>
				</thead>
            	<?php $i= 1; ?> 
				<tbody>
				@foreach($users as $user)
				<tr class="column{{$user->id}}">
					<td>{{$i}}</td>
					<td class="columnName">{{$user->name}}</td>
					<td class="columnEmail">{{$user->email}}</td>
					<td class="columnRole" roleid="{{$user->UserRole_ID}}">{{$user->UserRoleName}}</td>
					<td>
						<button type="button" btnid="{{$user->id}}" class="btn btn-primary btnEditUser" data-toggle="modal" data-target=".user-edit-button">Edit</button>
						<a href="{{url('/person/delete/'.$user->id)}}" class="btn btn-danger">Delete</a>
					</td>				
				</tr>
				<?php  $i++; ?>
				@endforeach
				</tbody>
			</table>
		</div>

		<div class="modal fade user-edit-button" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  		<div class="modal-dialog modal-sm" role="document">
    		<div class="modal-content">
      			<form method="POST" action="{{route('editPerson')}}" style="padding: 15px;">
    			{{ csrf_field() }}

    				<input type="hidden" name="user_id" class="user_id">

					<div class="form-group">
					  	<label for="exampleInputEmail1">Name</label>
					    <input type="text"  class="form-control editName" required="" name="name" value="" placeholder="Full Name">
					</div>

			  		<div class="form-group">
					    <label for="exampleInputEmail1">Email address</label>
					    <input type="email"  class="form-control editEmail" required="" name="email" value=""  placeholder="Email">
			  		</div>
		  

			  		<div class="form-group">
			  			<label for="exampleInputEmail1">Role</label>
					    <select class="form-control editRole" name="role" required="">
					    	<option value="">Select User Role</option>
					    	@foreach($role as $r)
							  	<option value="{{$r->id}}">{{$r->UserRoleName}}</option>
							@endforeach
					    </select>
			  		</div>

			  		<div class="form-group">
			    		<label for="exampleInputPassword1">Password</label>
			    		<input  type="password" name="password" class="form-control editPassword" placeholder="Password">
			  		</div>
		  			<button type="submit" class="btn btn-default btn-block">Submit</button>
				</form>
    		</div>
  		</div>	
		</div>
	</div>	
</div>	

<script>
$(document).on('click', '.btnEditUser', function(){

elem = $(this).attr('btnid');
parentTR = $(this).parent('td').parent('column'+elem);
name =  $(this).parent('td').parent('tr').find('.columnName').html();
email =  $(this).parent('td').parent('tr').find('.columnEmail').html();
roleId =  $(this).parent('td').parent('tr').find('.columnRole').attr('roleid');
roleName =  $(this).parent('td').parent('tr').find('.columnRole').html();

$('.user_id').val(elem);
$('.editName').val(name);
$('.editEmail').val(email);
$('.editRole').val(roleId);
$('.editPassword').val('');

$("select option[value='"+roleId+"']").attr("selected","selected");
console.log(elem);

});
</script>


@endsection