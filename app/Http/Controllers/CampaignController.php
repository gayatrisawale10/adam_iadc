<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Campaign;
use App\AdamsLineItem;
use validator;
use Illuminate\Support\Facades\Auth;
use App\Client;
use DB;
Use Redirect;
use Carbon\Carbon;
use DateTime;
use Session;
//use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use App\RawExcelData;

//use App\Http\Controller\AdamSheetController;


class CampaignController extends Controller
{

    public function index(Request $request) {

    	$userRoleID=Auth::user()->UserRole_ID;
		$userID=Auth::user()->id;

  	    // these deatils are for admin
    	if($userRoleID==1)
    	{
	    	$campaign_list=DB::select('exec SP_IADC_getAllCampaignDetails');
	        //dd($campaign_list);
	        $client_list = DB::table('Client')->get();
	        $client_list=json_decode(json_encode($client_list,true),true);
	        // $campaign_list=json_decode($campaign_list);
        }
       //for CM
        else
        {
        	$campaign_list=DB::select("exec SP_IADC_getAllCampaignDetailsforPerson ?", array( $userID));
	        //dd($campaign_list);
	        $client_list = DB::table('Client')->get();
	        $client_list=json_decode(json_encode($client_list,true),true);
	        // $campaign_list=json_decode($campaign_list);
		}
				//dd($campaign_list);
        $getApiPublisher= DB:: select('select Publisher.id as pubId, Publisher.PublisherName, PublisherType.PublisherTypeName from Publisher
			inner join PublisherType on PublisherType.id = Publisher.PublisherType_ID 
			where Publisher.PublisherType_ID =1
			ORDER BY Publisher.PublisherName ASC');
        //dd($getApiPublisher);
        return view('Campaign.index',compact('campaign_list','client_list', 'getApiPublisher'))
							->with('i', ($request->input('page', 1) - 1) * 5);

    }



   
    public function create() {
    		$client =  DB::table('Client')->get();
			return view('Campaign.create',compact('client'));
	}

    public function store(Request $request) {

    	//dd($request->all());
    	//String is converted in to Date

    	if($request->input('alwaysOn')){
    		$alwaysOn = 1;
    	} else{
    		$alwaysOn = 0;
    	}

    	$startDt=$request->input('StartDate');
    	$endDt=$request->input('EndDate');
    	$time = strtotime($startDt);
		$startDt1 = date('Y-m-d',$time);
		$time = strtotime($endDt);
		$endDt1 = date('Y-m-d',$time);
		$duplicateAdamKeyArray=[];
		if($endDt1 > $startDt1)
		{
	    	$campID=DB :: table('Campaign')->select(DB::raw('max(CampaignID) as CampID' ))->pluck('CampID');
	    	$campID = json_decode($campID);

	    	//dd($campID[0]);
	    	if(is_null($campID[0]))
		    {
		        $CampaignID=100000;
		    }
		    else
		    {
		        $CampaignID=$campID[0]+1;
		    }
		    // ==============Check Duplicate Camapign Name================================================================
		    $camapaignNameExist=$this->DuplicateCamapignName($request->input('campaign_name'));
		    //dd($camapaignNameExist);	
		    // ==============Check Duplicate Camapign Name================================================================
		    //-1 means not present
		    if($camapaignNameExist== -1){
			    $createdBy = Auth::user()->id;
				$current_time = Carbon::now()->toDateTimeString();
				$campaign_name = Campaign::create([
							  'ClientBrand_ID' => $request->input('ClientBrand'),
							  'CampaignName' => $request->input('campaign_name'),
							  'CampaignID' => $CampaignID,
							  'StartDate' => $startDt1,
							  'EndDate' => $endDt1,
							  //'adamFilePath' => public_path()."\ ".'adamFiles'."\ ".$newAdamname,
							  //'campaignPlanFilePath' => public_path()."\ ".'campaignPlanFiles'."\ ".$newcampaignPlanname,
							  'alwaysOn' => $alwaysOn,
							  'CreatedOn'=>$current_time,
							  'ModifiedOn'=>$current_time,
							  'ModifiedBy' => 1,
							  'IsActive' =>True,
							  'CreatedBy' => $createdBy
							  

				]);
				//dd($campaign_name);
				//----------------last updated camapaign-------------------
				$getLatestCampignID= DB::table('Campaign')
									->select('id') 	
									->orderby('id','DESC')
						            ->first();
		        $getLatestCampignID=$getLatestCampignID->id;
		        //----------------end last updated camapaign-------------------
		        
		        $date = Carbon::now()->format('Ymdhis');
		    	//dd(gettype($startDt1),gettype($endDt1));
				// ------------adam and CampaignPlan-----------------------

				$this->validate($request, [
				    'adamFile' => 'required|mimes:xlsx,xls',
				]);

				if($isAdamFile=$request->hasfile('adamFile')){
					$adamFile=$request->file('adamFile');
					$adamData = \Excel::load($adamFile)->get();

					$adamname=$adamFile->getClientOriginalName();           
		        	$adamfilename = pathinfo($adamname, PATHINFO_FILENAME);
					$adamextension = pathinfo($adamname, PATHINFO_EXTENSION);
					$newAdamname = $adamfilename.'-'.$date.'.'.$adamextension;
		        	$adamFile->move(public_path('adamFiles'), $newAdamname);
	        	
		        	//get data from adam excel sheet
		        	//echo"<pre>";
					//print_r($adamData); exit;
		        	$returnOfAdamLine=AdamSheetController::processAdamLineItem($adamData, $getLatestCampignID, $request->client);	
				}
						
				//dd($returnOfAdamLine);
				
				// ============DuplicateKeyString================================
				foreach ($returnOfAdamLine as $eachKey) {
				    if($eachKey!=1)
				    {
				        array_push($duplicateAdamKeyArray, $eachKey);  
				    }	
				}
				$duplicateAdamKeyStr=implode($duplicateAdamKeyArray, ',  ');
				$duplicateAdamKeyString="Duplicate AdamUniuqueKey: ".$duplicateAdamKeyStr;
				//echo strlen($duplicateAdamKeyStr); 
				if(!$duplicateAdamKeyStr){
				      $duplicateAdamKeyString="No Duplicate AdamUniqueKey";
				}
				//print_r($duplicateAdamKeyString);
				// ============DuplicateKeyString================================	
				if($isCampaignPlanFile=$request->hasfile('campaignPlan')){
					$campaignPlanFile=$request->file('campaignPlan');
					$campPlanName=$campaignPlanFile->getClientOriginalName();           
		        	$campPlanfilename = pathinfo($campPlanName, PATHINFO_FILENAME);
					$campPlanextension = pathinfo($campPlanName, PATHINFO_EXTENSION);
					$newcampaignPlanname = $campPlanfilename.'-'.$date.'.'.$campPlanextension;
			        $campaignPlanFile->move(public_path('campaignPlanFiles'), $newcampaignPlanname);

				}	

				DB::table('Campaign')
		        ->where('Campaign.id' ,'=',$getLatestCampignID)
				->update([
				    'adamFilePath' => public_path()."\ ".'adamFiles'."\ ".$newAdamname,
					'campaignPlanFilePath' => public_path()."\ ".'campaignPlanFiles'."\ ".$newcampaignPlanname
				]);

				// ------------end adam and CampaignPlan-------------------------

		        // return redirect()->route('Campaign.index')
				// 				->with('success','Campaign is created successfully');

				return redirect()->route('AdamSheet.getAdamLines', [$getLatestCampignID]) ->with('danger',$duplicateAdamKeyString);
			}
			else{
				return redirect()->route('Campaign.index')
							->with('danger','Campaign Name must be Uniuqe');	
			}	
		}
		else
		{
			return redirect()->route('Campaign.index')
							->with('danger','End Date should be greater than Start Date');
		}
	}

	public function edit($cm_id) {

        $Campaign_edit=DB::table('Campaign')
             	->where('Campaign.id','=',$cm_id)
				->orderby('id','DESC')
	            ->get();
        $Campaign_edit=json_decode($Campaign_edit);
        $Campaign_edit = $Campaign_edit[0];
        return view('Campaign.edit',compact('Campaign_edit'));
    }

    public function update(Request $request, $cm_id)
    {

     	$arr=$request->input();

     	//dd($arr['CampaignName']);

     	$current_time = Carbon::now()->toDateTimeString();
     	$ReqNewStart=$request->StartDate;
     	$NewStart1=date("m-d-Y", strtotime($ReqNewStart) );
     	$ReqNewEnd  =$request->EndDate;
     	$NewEnd1  =date("m-d-Y", strtotime($ReqNewEnd) );

     	$NewStart = new DateTime($ReqNewStart);
    	$NewEnd = new DateTime($ReqNewEnd);

     	//dd($NewStart1, $NewEnd1);

       	//get previous date of campign from DB
       	$campignDate=DB::table('Campaign')
             	->where('Campaign.id','=',$cm_id)
				->orderby('id','DESC')
	            ->get();

	    $campignDate=json_decode($campignDate);
	     //dd($campignDate);
	    $DBoldStart=$campignDate[0]->StartDate;
	    $oldStart1  =date("m-d-Y", strtotime($DBoldStart) );
	    $DBoldEnd=$campignDate[0]->EndDate;
	    $oldEnd1  =date("m-d-Y", strtotime($DBoldEnd) );

	    $oldStart = new DateTime($DBoldStart);
    	$oldEnd = new DateTime($DBoldEnd);

	    //dd($oldStart1, $oldEnd1);

       	if($NewEnd>$NewStart)
       	{
       		//dd("valid");
	       	if($NewStart>$oldStart && $NewEnd<$oldEnd)
	        {
	        	//dd("both are greator than old");
	        	return redirect()->route('Campaign.index')
                        ->with('success','Invalid Date Selection');
	        }
		    else if($NewStart<$oldStart && $NewEnd<$oldEnd)
		    {
		    	//dd("towards leff");
		    	return redirect()->route('Campaign.index')
                        ->with('success','Invalid Date Selection');
		    }
		    else if($NewStart>$oldStart && $NewEnd>$oldEnd)
		    {
		    	//dd("towards right");
		    	return redirect()->route('Campaign.index')
                        ->with('success','Invalid Date Selection');
		    }
		    else if($NewStart<$oldStart && $NewEnd>$oldEnd)
		    {
		    	//dd("two array");
		    	$StartDateA1=$NewStart1;
		    	$EndDate = strtotime("-1 day", strtotime($DBoldStart));
		    	$EndDate=date('m-d-Y',$EndDate);
		    	$EndDateA1=$EndDate;

		    	$getAllDailyLineItem=DB::select("exec SP_IADC_UpdateLineItemDailyForNewDates ?,?,?", array( $cm_id, $StartDateA1, $EndDateA1 ));

		    	$StartDate = strtotime("+1 day", strtotime($DBoldEnd));
		    	$StartDateA2=date('m-d-Y',$StartDate);
		    	$EndDateA2=$NewEnd1;

		    	$getAllDailyLineItem=DB::select("exec SP_IADC_UpdateLineItemDailyForNewDates ?,?,?", array( $cm_id, $StartDateA2, $EndDateA2 ));

		    	//dd($StartDateA1,$EndDateA1,$StartDateA2,$EndDateA2);
		    }
		    else if($NewStart<$oldStart && $NewEnd==$oldEnd)
		    {
		    	$StartDate=$NewStart1;
		    	$EndDate = strtotime("-1 day", strtotime($DBoldStart));
		    	$EndDate=date('m-d-Y',$EndDate);
		    	$EndDate=$EndDate;
		    	//dd('1',$StartDate,$EndDate);

		    	$getAllDailyLineItem=DB::select("exec SP_IADC_UpdateLineItemDailyForNewDates ?,?,?", array( $cm_id, $StartDate, $EndDate));
		    }
		    else if($NewStart==$oldStart && $NewEnd>$oldEnd)
		    {
		    	$StartDate = strtotime("+1 day", strtotime($DBoldEnd));
		    	$StartDate=date('m-d-Y',$StartDate);
		    	$EndDate=$NewEnd1;
		    	//dd('2', $StartDate,$EndDate);

		    	$getAllDailyLineItem=DB::select("exec SP_IADC_UpdateLineItemDailyForNewDates ?,?,?", array( $cm_id, $StartDate, $EndDate));
		    }

		    else if($NewStart==$oldStart && $NewEnd==$oldEnd)
		    {
		    	//dd("No Action");
		    }

		}
		else
		{
			//dd("invalid1");
			return redirect()->route('Campaign.index')
                         ->with('success','Invalid Date Selection. EndDate must be greator than StartDate');
		}

	    //update campignDetails
       //	Campaign::find($cm_id)->update($request->all());
		// change of date format on edit

		$startDt=$request->input('StartDate');
    	$endDt=$request->input('EndDate');
    	$time = strtotime($startDt);
		$startDt11 = date('Y-m-d',$time);
		$time = strtotime($endDt);
		$endDt11 = date('Y-m-d',$time);

		//	dd($startDt11, $endDt11);


		$update=  DB::table('Campaign')
			       ->where('id', '=',$cm_id)

			       ->update([
			       		'CampaignName' => $arr['CampaignName'],
					   	'StartDate' => $startDt11,
					  	'EndDate' => $endDt11,

					  	'CreatedOn'=>$current_time,
					  	'ModifiedOn'=>$current_time,
					  	'ModifiedBy' => 1,
					  	'IsActive' =>True
			       ]);


       	return redirect()->route('Campaign.index')
                         ->with('success','Campaign updated successfully');


    }

    public function destroy($cntr)
	{

		$records_count = DB::table('APIParams')
					->where('AdamsLineItem.campaign_ID','=',$cntr) 
					->join('AdamsLineItem', 'AdamsLineItem.id','=','APIParams.AdamsLineItem_ID')
					->get()
					->count();

		//dd($records_count);			
		//if ($records_count)
		if ($records_count==0) 
		{
			//dd("in if123");
			Campaign::find($cntr)
					->update(['IsActive'=>0]);
			
			AdamsLineItem::where('AdamsLineItem.campaign_ID','=', $cntr)
						->update(['IsActive'=>0]);
			
			DB::statement("update AdamLineItemDIT set IsActive = 0  where AdamsLineItem_ID in(select AdamsLineItem.id from AdamsLineItem where campaign_ID = $cntr)");

			DB::statement("update AdamLineDCMMapping set IsActive = 0  where AdamsLineItem_ID in(select AdamsLineItem.id from AdamsLineItem where campaign_ID = $cntr)");

			return redirect()->route('Campaign.index')
      		->with('success','Campaign deleted successfully');

		} else{
			//dd("in else");

			return redirect()->route('Campaign.index')
      		->with('success','Adam Line Item is Present, unable to delete');
      		
		}

	}

	public function selectBrand(Request $request) {

        $Client_ID = $request->Client_ID;
        $ClientBrand = DB::table('ClientBrand')->where('Client_ID','=',$Client_ID)->get();
        $ClientBrand = json_decode(json_encode($ClientBrand,true),true);

        return response()->json(['status' => 'success','ClientBrand'=>$ClientBrand], 200);
    }


    public function DuplicateCamapignName($campignName)
    {
    	//dd($campignName);
    	$campignNameExistCheck=DB::table('Campaign')
             	->where('Campaign.CampaignName','=',$campignName)
             	->where('Campaign.IsActive','=','1')
				->get();
        $campignNameExistCheck=json_decode($campignNameExistCheck);
       	//dd($campignNameExistCheck);

       	if($campignNameExistCheck){
       		//dd("Present");
       		return 1;
       	}
       	else{
       		//dd("empty");
       		return -1;
       	}	
    }



  public function getPlannedValuesReportInExcel($cb_id)
  {
    //dd(gettype($cb_id));
    ini_set('memory_limit', '-1');
    set_time_limit(0);

      
    $products=DB::select("exec SP_Insert_TempAdamTableau_Planned ?", array($cb_id));
     
    //dd($products);
         
      if($products){  
        Excel::create('Palnned Value Report', function($excel)  use ($products)  {

        $excel->sheet('Sheet 1', function($sheet) use ($products) {
        
		    foreach($products as $product) {    
		   		$headers=[];
		    	$data1=[];
		        foreach ($product as $key => $value) {
		        	$headers[]=$key;
		        	$data1[] = $value;
		        }
		        $data[] = $data1;
		    }

		    // echo"<pre>"; print_r($headers);
		    // echo"<pre>"; print_r($data);
		    $sheet->fromArray($data, null, 'A1', false, false);
	        $sheet->prependRow(1, $headers);
        });
      })->export('xls');
    }
    else{
        //echo"No Data";
        return Redirect::back()->withErrors(['No Records Exist.', 'The Message']);
    }     
  }

}
