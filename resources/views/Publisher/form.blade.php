<div class="row col-md-3">
	
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group"><strong>Publisher Name:</strong></div>
    <div class="form-group">
        {!! Form::text('PublisherName', $PublisherName, array('placeholder' => 'Publisher name','class' => 'form-control','required'=>'', 'maxlength'=>'50')) !!}
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group"><strong>Genre :</strong></div>
      <div class="form-group col-xs-13 col-sm-13 col-md-13">
          <select class="form-control" name="Genre_ID" id="Genre_ID" data-parsley-required="true">
            @foreach ($getGenre as $getGenre_val) 
            {
              <option value="{{ $getGenre_val->id }}" 
                {{$selectGenreID == $getGenre_val->id ? 'selected="selected"' : '' }}>
                {{ $getGenre_val->GenreName }}
              </option>  
            }
            @endforeach
          </select>
      </div>  
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group"><strong>Publisher Type :</strong></div>
    <div class="form-group col-xs-13 col-sm-13 col-md-13">
        <select class="form-control" name="PublisherType_ID" id="PublisherType_ID" data-parsley-required="true">
          @foreach ($getPublisherType as $getPublisherType) 
          {
            <option value="{{ $getPublisherType->id }}" 

              {{$PublisherType_ID == $getPublisherType->id ? 'selected="selected"' : '' }}>
              {{ $getPublisherType->PublisherTypeName }}
            </option>  
          }
          @endforeach
        </select>
    </div>  
  </div>
 
  </div>
  <div class="col-xs-12 col-sm-12 col-md-12">    
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
      <button type="submit" class="btn btn-primary pull-left">Save</button>
      <div class="pull-left" style="padding-left:20px;">
          <a class="btn btn-primary" href="{{ route('Publisher.index') }}"> Back</a>
      </div>
    </div>
  </div>
</div>
