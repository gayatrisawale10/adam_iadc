<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(get_class_methods($request));
        $path = $request->route()->getCompiled()->getStaticPrefix(); 
        $path = str_replace('/','',$path); 
        //dd($path);
        $getRole = Session::get('getRole');
        
        //dd($getRole);



        //dd($getRole!='admin');

        $doNotShowAdminUrls = array(
            'Campaign',
            'Publisher',
            'Client',
            'api',
            'Person',
            'LineItem'
        );
        
        $doNotShowPublisherUrls = array(
            'PublisherHome',
            'LineItemDaily'
        );

         $doNotShowCampiagnManagerUrls = array(
            'Campaign',
            'Publisher',
            'Client',
            'api',
            'LineItem'
        );

        //dd(in_array($path, $doNotShowCampiagnManagerUrls));
         
        //dd($doNotShowCampiagnManagerUrls);
       

        if($getRole!='CampaignManager' && $getRole!='admin')
        { 
            if(in_array($path, $doNotShowCampiagnManagerUrls))
            {
                //print_r('if cm');
                return redirect()->back();
            }
        }

        if($getRole!='admin' && $getRole!='CampaignManager')
        { 
          if(in_array($path, $doNotShowAdminUrls))
          {
            
            //print_r('admin');
           return redirect()->back();
          }
        
        }



        // if( ($getRole!='CampaignManager') && (in_array($path, $doNotShowCampiagnManagerUrls)))
        // {
        //     print_r('if cm');
        //     //return redirect()->back();
        // }
        // else
        // {
        //     print_r(' else cm');
        //     //return redirect()->back();
        // }
        

        // if(($getRole!='admin' && in_array($path, $doNotShowAdminUrls))){
            
        //     print_r('admin');
        //    // return redirect()->back();
        // }
        // else
        // {
        //     print_r(' else admin');
        //     //return redirect()->back();
        // }

        if($getRole!= 'Publisher' && in_array($path, $doNotShowPublisherUrls)){
         
           // print_r('Pub');
           return redirect()->back();
        }
           

       
        
         return $next($request);
    }
}
