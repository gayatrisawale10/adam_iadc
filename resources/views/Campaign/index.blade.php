@extends('layouts.app')
@section('content')
<style type="text/css">
  .addMore{
    
  cursor: pointer;
  }
  
  .campTable {
    table-layout: fixed;
  }  
  .setEllipsis
  {
    white-space: nowrap; 
    overflow: hidden;
    text-overflow: ellipsis;
    cursor: pointer;
  }
  .campTable
  {
    user-select: none;
  }
  .full-width{
    width: 100% !important;
  }
</style>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <div class="row">
    <div class="col-lg-12">
      <div class="pull-left">
        <h2>Campaign Details</h2>
      </div>
      <div class="pull-right margin-top-20">
        <!--  start button for  Pop up box code -->  
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#gridSystemModal" data-backdrop="static" data-keyboard="false">
          Add Campaign
        </button> 
      </div>

            <!--  start Pop up box code -->         
      <div class="pull-right margin-top-20">
        <div id="gridSystemModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document" style="width:600px;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              
                <h4 class="modal-title" id="gridModalLabel" style="text-align:center;" >Campaign Details</h4>
              </div>
              <div class="modal-body" id="register" >            
                  {!! Form::open(array('class' => 'form-inline', 'method' =>'post', 
                    'route' =>array('Campaign.store'), "enctype"=>"multipart/form-data")) !!}
                <div class="row">
                  <div class="col-md-9"></div>
                  <div class="col-md-3 checkbox">
                    <h5 style="display: inline;">Always On</h5>
                    <input type="checkbox" data-toggle="toggle" name='alwaysOn'>
                  </div>
                </div>
                <div class="row margin-top-20">
                  <div class="col-md-4">
                    <h5>Client Name: <span style="color: red">*</span></h5> 
                  </div>
                  <div class="col-md-8">
                    <select name="client" id='client' class="form-control map-client-brand full-width" required> 
                        <option value="" data-for="selected-client">Select</option>
                        @foreach($client_list as $client)
                        <option value="{{ (string)$client['id'] }}" data-for="selected-client">{{ $client['ClientName'] }}</option>
                        @endforeach
                    </select>                  
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <h5>Client Brand Name: <span style="color: red">*</span></h5>
                  </div>
                  <div class="col-md-8">
                   <select name="ClientBrand" class="form-control  map-brand-client brandName full-width" required> 
                      <option value="" data-for="selected-brand">Select</option>
                    </select>                 
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <h5>Campaign Name: <span style="color: red">*</span></h5>
                  </div>
                  <div class="col-md-8">
                    {!! Form::text('campaign_name', null, array('placeholder' => 'Campaign Name','class' => 'form-control full-width','required' => '', 'maxlength'=>'150','onblur' =>'checkLength(this)','onkeypress' => 'return isNumberKey(event)')) !!}                      
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <h5>Start Date:<span style="color: red">*</span></h5>
                  </div>
                  <div class="col-md-8">
                    {!! Form::text('StartDate', null, array('id' => 'datepicker','placeholder' => 'Start Date','class' => 'form-control full-width',  'required' => 'true', 'style'=>'background-color:white;')) !!}                      
                  </div>
                </div>
                <div class="row"> 
                  <div class="col-md-4">
                    <h5>End Date:<span style="color: red">*</span></h5>
                  </div>
                  <div class="col-md-8">
                    {!! Form::text('EndDate', null, array('id' => 'datepicker1','placeholder' => 'End Date','class' => 'form-control full-width', 'required' => 'true', 'style'=>'background-color:white;' )) !!}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <h5>Upload Adam Sheet <span style="color: red">*</span></h5>
                  </div>
                  <div class="col-md-8">
                    <input type="file" name="adamFile" required="true" />
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <h5>Upload Campaign Plan <span style="color: red">*</span></h5>
                  </div>
                  <div class="col-md-8">
                    <input type="file" name="campaignPlan" required="true" />
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 text-center">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                  </div>
                </div> 
              </div>            
            </div>
          </div>
        </div>
        {!! Form::close() !!}
      </div>
      <!--  End Model Pop up box code -->
    </div>        
  </div>
    
  @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
  @endif

  @if ($message = Session::get('danger'))
      <div class="alert alert-danger">
          <p>{{ $message }}</p>
      </div>
  @endif

  @if($errors->any())
  <div class="alert alert-danger">
    <p>{{$errors->first()}}</p>
  </div>
  @endif
  
  <div class="portlet-body">
    <div class="table-toolbar">
      <div style="width: 100%; overflow-x:auto;margin-bottom: 50px;">
        <table class="table table-bordered campTable" id="sample_editable_1" >
          <thead>   
                     
              <th style="width: 10%;">Client Name</th>
              <th style="width: 9%;">Brand Name</th>
              <th style="width: 15%;">Campaign Name</th>
              <th style="width: 9%;">Campaign ID</th>
              <th style="width: 7%;">Always On</th>
              <th style="width: 10%;">Start Date</th>
              <th style="width: 10%;" >End Date</th>
              <th style="width: 10%;">Created On</th>
              <!-- <th style="width: 10%;">Select API</th>    -->           
              <th style="width: 20%;">Action</th>
            
          </thead>
          <tbody>         
            @foreach ($campaign_list as $cntr)
            <tr align="center">       
              <td class="setEllipsis" title='{{$cntr->ClientName}}'>{{$cntr->ClientName}}</td>
              <td class="setEllipsis" title='{{$cntr->BrandName}}'>{{$cntr->BrandName}}</td>
              <td class="setEllipsis" title='{{$cntr->CampaignID}}_{{$cntr->CampaignName}}'>{{$cntr->CampaignID}}_{{$cntr->CampaignName}}</td> 
              <td class="setEllipsis" title='{{$cntr->CampaignID}}'>{{$cntr->CampaignID}}</td>

              @if($cntr->alwaysOn==1)
                <td>Yes</td>
              @else  
                <td>No</td>
              @endif  

              <td>{{(new DateTime($cntr->camp_st))->format('Y-m-d')}}</td>
              <td>{{(new DateTime($cntr->camp_en))->format('Y-m-d')}}</td>
              <td>{{(new DateTime($cntr->camp_cr))->format('Y-m-d')}}</td>
             <!--  <td>
                       
                <input type="Text" name="campSatrtDate" id="campSatrtDate" value="{{(new DateTime($cntr->camp_st))->format('Y-m-d')}}">
                <select name="publisherAPI" id='publisherAPI' class="form-control selectApiPublisher" >
                    <option value="" data-for="selected-genre">Select</option>
                      @foreach($getApiPublisher as $apiPub)
                        <option value="{{ $apiPub->pubId }}">{{ $apiPub->PublisherName }}</option>
                      @endforeach
                </select>
                
              </td> -->
              
              <td>
                <div class="row" >
                  <div  class="col-md-3">
                    <a class="btn btn-primary btn-md addMore" title="Edit" !style="margin-right:120px;" href="{{ URL::route('Campaign.edit', $cntr->camp_id)}}">
                      <i class="fa fa-pencil"> </i>
                    </a> 
                  </div>
                  <!-- <div  class="col-md-3">
                    <a class="btn btn-primary btn-md addMore" title="Line Item" href="{{URl::route('CampaignPublisher.getCampaignPublisherdetails',['Campaign_ID' =>$cntr->camp_id]) }}">
                      <i class="fa fa-plus-square"> </i>
                    </a>
                  </div> -->
                  <div  class="col-md-3">
                    <a class="btn btn-primary btn-md addMore" title="AdamLine Item" href="{{URl::route('AdamSheet.getAdamLines',['Campaign_ID' =>$cntr->camp_id]) }}">
                      <i class="fa fa-plus-square"> </i>
                    </a>
                  </div>

                  <!-- URL for previous report CampaignPublisher.getCampaignPublisherdetailsForReport -->
                  <!-- <div  class="col-md-3">
                    <a class="btn btn-primary btn-md  addMore" title="Report"  
                     href="{{URl::route('CampaignPublisher.getCampaignPublisherdetailsInExcel',['Campaign_ID' =>$cntr->camp_id]) }}">
                        <i class="fa fa-file-excel-o"> </i>
                     </a>
                  </div> -->

                   <div  class="col-md-3">
                    <a class="btn btn-primary btn-md  addMore" title="Report"  
                     href="{{URl::route('Campaign.getPlannedValuesReportInExcel',['Campaign_ID' =>$cntr->camp_id]) }}">
                        <i class="fa fa-file-excel-o"> </i>
                     </a>
                  </div>


                  <div  class="col-md-3 addMore" title="Delete">
                  {!! Form::open(['method' => 'DELETE','route' => ['Campaign.destroy',$cntr->camp_id],'style'=>'display:inline']) !!}

                   <!-- {!! Form::submit('-',['class' => 'btn btn-sm btn-danger']) !!} -->
                    {!! Form::submit('-', ['class' => 'btn btn-sm btn-danger','onclick'=>'return confirm("Are you sure you want to delete?")']) !!}
                   {!! Form::close() !!}
                  </div>
              </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        @endsection
      </div>
    </div>
  </div>
  
<script type="text/javascript">

      // $('#keyup-limit-11').keyup(function() {
    
      //   $('span.error-keyup-3').remove();
      //   var inputVal = $(this).val();
      //   var characterReg = /^([a-zA-Z0-9]{11})$/;
      //   if(!characterReg.test(inputVal)) {
      //       $(this).after('<span style="color:red" class="error error-keyup-3">Maximum 11 digits for Landline.</span>');
      //   }
      // });
   

    function onlyNumbers(event) {
      var Key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
      if (Key == 13 || (Key >= 48 && Key <= 57)) return true;
      else return false;
    } 
    
    // $('#keyup-limit-10').keyup(function() {
    
    //   $('span.error-keyup-3').remove();
    //   var inputVal = $(this).val();
    //   var characterReg = /^([a-zA-Z0-9]{10})$/;
    //   if(!characterReg.test(inputVal)) {
    //     $(this).after('<span style="color:red" class="error error-keyup-3">Maximum 10 digits for Mobile.</span>');
    //   }
    // });
 

</script>
<script type="text/javascript">

  // $(function(){
  //   $('#keyup-limit-emailvalid').keyup(function() {
    
  //     $('span.error-keyup-3').remove();
  //     var inputVal = $(this).val();
  //     var characterReg =/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      
  //     if(!characterReg.test(inputVal)) {
  //         $(this).after('<span style="color:red" class="error error-keyup-3">Email-id is not in valid format .</span>');
  //     }
  //   });
  // });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

<script type="text/javascript">  
$(function() {
   
  $("#datepicker").datepicker({ dateFormat: 'dd-mm-yy',
        minViewMode: 1,
        yearRange: '1999:2050',
        changeMonth: true,
              changeYear: true }).val();

   $("#datepicker1").datepicker({ dateFormat: 'dd-mm-yy',
       minViewMode: 1,
        yearRange: '1999:2050',
        changeMonth: true,
              changeYear: true }).val();
});

$(document).on('change', '.map-client-brand', function() {
    $('.brandName').empty();
    var selectedOption = $(this).val();
    $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });

    $.ajax({
        type: 'POST',
        url: '{{ url('/selectBrandClient') }}', 
        data: { "_token": "{{ csrf_token() }}",'Client_ID':selectedOption},
        success: function (response ) {
              $(".brandName").append('<option value="" data-for="selected-brand">Select</option>');
              $.each(response.ClientBrand, function (key, value) {
                  $(".brandName").append('<option value="'+value.id+'" data-for="selected-brand" id="empBrandList">'+value.BrandName+'</option>');
              });
        }
    });     
  }); 

// $(document).on('change', '.selectApiPublisher', function() {

//     var getApiPublisherId = $(this).val();
//     var campSatrtDate = $("#campSatrtDate").val();
//     //var getApiPublisherId = $(this).val();
   

//     alert(getApiPublisherId); alert(campSatrtDate); 

//     $.ajaxSetup({
//             headers: {
//                 'X-CSRF-Token': $('meta[name=_token]').attr('content')
//             }
//         });

//     $.ajax({
//         type: 'POST',
//         url: '{{ url('/getDataforVariousApi') }}', 
//         data: { "_token": "{{ csrf_token() }}",'getApiPublisherId':getApiPublisherId, 'campSatrtDate':campSatrtDate},
//         success: function (data ) {
//               console.log(data);
//         }
//     }); 
// });
</script>