<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseBatchPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CourseBatchPerson', function (Blueprint $table) 
        {
            
            $table->increments('CourseBatchPerson_ID');
            $table->integer('Coursebatch_ID')->unsigned();;
            $table->integer('Person_ID')->unsigned();
            $table->integer('PersonType_ID')->unsigned();
            $table->integer('PersonCourseStatusType_ID')->unsigned();
            $table->timestamps();
        });
  
       Schema::table('CourseBatchPerson', function($table) {
       $table->foreign('Coursebatch_ID')->references('CourseBatch_ID')->on('CourseBatch');

   });
   
    Schema::table('CourseBatch', function($table) {
       $table->foreign('TrainingCenter_ID')->references('TrainingCenter_ID')->on('Training_center');

   });

    Schema::table('CourseBatch', function($table) {
       $table->foreign('PersonType_ID')->references('PersonType_ID')->on('persons_Type');

   });

     Schema::table('CourseBatch', function($table) {
       $table->foreign('PersonCourseStatusType_ID')->references('PersonCourseStatusType_ID')->on('CreateCourseStatusType');

   });



}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CourseBatchPerson');
    }
}
