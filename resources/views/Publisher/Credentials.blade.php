@extends('layouts.appResourcePublisher')
 <div class="container">
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Log-In Credentials For :</h2>
                <h4>Publisher Name : {{$PublisherName}}</h4>
            </div>
        </div>
    
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    {!! Form::open(['method' => 'post','route' => ['Publisher.saveCredentials', $p_id]]) !!} 

       <div class="row col-md-3"> 
		  <div class="col-xs-12 col-sm-12 col-md-12">
		  <div class="form-group"><strong>User Name:</strong></div>
		    <div class="form-group">
		        {!! Form::email('UserName', $UserName, array('placeholder' => 'User name','class' => 'form-control','required'=>'')) !!}
		      </div>
		  </div> 
 
		  <div class="col-xs-12 col-sm-12 col-md-12">
		    <div class="form-group"><strong>Password:</strong></div>
		    <div class="form-group"> 
		       
		      {!! Form::text('Password', $Password, array('placeholder' => 'Password','class' => 'form-control','required'=>'')) !!}
		 
		    </div>
		</div>    
		<div class="col-xs-12 col-sm-12 col-md-12">    
    		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
      			<button type="submit" class="btn btn-primary pull-left">Save</button>
      			<div class="pull-left" style="padding-left:20px;">
          			<a class="btn btn-primary" href="{{ route('Publisher.index') }}"> Back</a>
      			</div>
    		</div>
  		</div>



     {!! Form::close() !!}
	 
	</div>
@endsection
</div>













