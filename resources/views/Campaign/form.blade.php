<div class="row col-md-6">
	 <div class="col-xs-12 col-sm-12 col-md-12">
	  <div class="form-group">
                <strong>Campaign Name:</strong>
			</div>
	        <div class="form-group">
	           {!! Form::text('CampaignName', null, array('placeholder' => 'Campaign Name','class' => 'form-control' ,'onkeypress' => 'return isAlpha(event)' ,'required'=>'true','maxlength'=>'50')) !!}
	        </div>
	 </div>

     <div class="col-xs-12 col-sm-12 col-md-6">
      <div class="form-group">
                <strong>Start Date:</strong>
            </div>
            <div class="form-group">
               {!! Form::text('StartDate', null, array('id' => 'datepicker','placeholder' => 'Start Date','class' => 'form-control','onkeypress' => 'return isNumber(event)', 'readonly'=>'true', 'style'=>'background-color:white;')) !!}
            </div>
     </div>


     <div class="col-xs-12 col-sm-12 col-md-6">
      <div class="form-group">
                <strong>End Date:</strong>
            </div>
            <div class="form-group">
              {!! Form::text('EndDate', null, array('id' => 'datepicker1','placeholder' => 'End Date','class' => 'form-control','onkeypress' => 'return isNumber(event)' , 'readonly'=>'true', 'style'=>'background-color:white;')) !!}
            </div>
     </div>


   <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary pull-left">Save</button>
			 <div class="pull-left" style="padding-left:20px;">
                <a class="btn btn-primary" href="{{ route('Campaign.index') }}"> Back</a>
            </div>
    </div>
</div>
<script type = "text/javascript">


function isNumber(evt)
{
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    {
        return false;
    }
    return true;
}

// function isAlpha(e)
// {
//    e = (e) ? e : window.event;
//     var eCode = (e.which) ? e.which : e.keyCode;
//    if (eCode <= 90 && eCode >= 48) {
//     return false;
//   }
//    return true;
// }

</script>

<script type="text/javascript">

 $(function() {

$("#datepicker").datepicker({ dateFormat: 'dd-mm-yy',
      minViewMode: 1,
      yearRange: '1999:2050',
      changeMonth: true,
            changeYear: true }).val();

 $("#datepicker1").datepicker({ dateFormat: 'dd-mm-yy',
     minViewMode: 1,
      yearRange: '1999:2050',
      changeMonth: true,
            changeYear: true }).val();
});

</script>
