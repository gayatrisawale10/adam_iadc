<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
	public $timestamps = false;
	
	protected $primaryKey = 'id';

    public $fillable = ['ClientBrand_ID','CampaignName','CampaignID','StartDate','EndDate','adamFilePath','campaignPlanFilePath','alwaysOn','CreatedOn','ModifiedOn','ModifiedBy','IsActive', 'CreatedBy'];

	public $table = "Campaign";
}
