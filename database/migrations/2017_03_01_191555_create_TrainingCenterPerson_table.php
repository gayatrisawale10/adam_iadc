<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingCenterPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TrainingCenterPerson', function (Blueprint $table) {
            $table->increments('TrainingCenterPerson_ID');
             $table->integer('TrainingCenter_ID')->unsigned();
            $table->integer('Person_ID')->unsigned();
           $table->integer('PersonType_ID')->unsigned();
          $table->timestamps();
        });

           Schema::table('TrainingCenterPerson', function($table) {
       $table->foreign('TrainingCenter_ID')->references('TrainingCenter_ID')->on('Training_center');

   });
   
    Schema::table('TrainingCenterPerson', function($table) {
       $table->foreign('Person_ID')->references('Person_ID')->on('Person');

   });
    Schema::table('TrainingCenterPerson', function($table) {
       $table->foreign('PersonType_ID')->references('PersonType_ID')->on('persons_Type');

   });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TrainingCenterPerson');
    }
}
