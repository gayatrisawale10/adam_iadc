@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		
	        <h2>Mapped Matrices</h2>
	    

		<div class="row text-right form-right">
			<button type="button" 	
					class="btn btn-success" 
					data-toggle="modal" 
					data-target=".mapMetrices">Matrics Mapping</button>
																																															 
            <a class="btn btn-danger" href="{{ URL::previous() }}">Back </a>
		</div>
	</div>
	 @if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
	@endif
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<table class="table table-bordered display" id="sample_editable_1">
				<thead class="success">
					<tr>
						<th>HiddenMetrics Group Name</th>
						<th>HiddenMetrics Name</th>
						<!-- <th>Publisher_ID</th> -->
					</tr>
				</thead>
            	<tbody>
				@foreach($getMappedMatrics as $eachmetrics)
				<tr >
					<td>{{$eachmetrics->HiddenMetricsGroupName}}</td>
					<td>{{$eachmetrics->HiddenMetricsName}}</td>
					<!-- <td>{{$eachmetrics->Publisher_ID}}</td> -->
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-md-2"></div>
	</div>
</div>
@endsection


<div class="modal fade mapMetrices" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">	
	<div class="modal-dialog" role="document" style="width:600px;">
        <div class="modal-content">
          	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            	<h4 class="modal-title" id="gridModalLabel" style="text-align:center;" >Map Metrices</h4>
          	</div>
          	<div class="modal-body" id="register" >            
           	 	{!! Form::open(array('class' => 'form-inline', 'method' =>'post', 'route' =>array('mapMetrices'),)) !!}
                <div class="row">
	              	<div class="col-md-4">
	                	<h5>Metrice Name: <span style="color: red">*</span></h5>
	              	</div>
	              	<div class="col-md-8">
	                	{!! Form::text('metriceName', null, array('placeholder' => 'Metrice Name','class' => 'form-control full-width','required' => '', 'maxlength'=>'150','onblur' =>'checkLength(this)','onkeypress' => 'return isNumberKey(event)')) !!}                      
	              	</div>
	            </div>

	            <div class="row margin-top-20">
	              	<div class="col-md-4">
	                	<h5>HiddenMetricsGroup Name: <span style="color: red">*</span></h5> 
	              	</div>
	              	<div class="col-md-8">
		                <select name="HiddenMetricsGroup_ID" id='HiddenMetricsGroupName' class="form-control full-width" required> 
		                    <option value="" data-for="selected-HiddenMetricsGroupName">Select</option>
		                    @foreach($getHiddenMetricsGroupName as $hgn)
		                    <option value="{{ (string)$hgn->Id }}" data-for="selected-HiddenMetricsGroupName">{{ $hgn->HiddenMetricsGroupName}}</option>
		                    @endforeach
		                </select>                  
	              	</div>
	            </div>

	          	<div class="col-md-12 text-center">
	            	{!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
	          	</div>
            </div> 
          </div>  
        </div>
		{!! Form::close() !!}            
    </div>
</div>