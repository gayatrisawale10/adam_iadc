@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <h2> Type Of Work</h2>
            </div>
            @if (Auth::user()->role_id == '1')
           <div class="pull-right margin-top-20">
                <a class="btn btn-success" href="{{route('workcrud.create') }}"> Create Work </a>
            </div>
            @endif
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>Sr.No</th>
            <th>Work</th>
			<th>Days of Completion</th>
             @if (Auth::user()->role_id == '1') <th width="280px">Action</th>@endif
        </tr>
    @foreach ($works as $workdata)
    <tr align="center">
        <td>{{ ++$i }}</td>
        <td>{{ $workdata->work}}</td>
		 <td>{{ $workdata->completion_days}}</td>
       @if(Auth::user()->role_id == '1')
        <td>
          
           <!--  <a class="btn btn-info" href="{{ route('workcrud.show',$workdata->work_id) }}">Show</a> -->
            <a class="btn btn-primary" href="{{ route('workcrud.edit',$workdata->work_id) }}">Edit</a>
            {!! Form::open(['method' => 'DELETE','route' => ['workcrud.destroy', $workdata->work_id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger', 'onclick'=>'return confirm("Are you sure you want to delete?")']) !!}
            {!! Form::close() !!}
           
        </td> @endif
    </tr>
    @endforeach
    </table>
     {!! $works->render() !!}

@endsection

