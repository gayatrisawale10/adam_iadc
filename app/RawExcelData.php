<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RawExcelData extends Model
{
    public $timestamps = false;
    
    protected $table = 'dbo.RawExcelData';
}
