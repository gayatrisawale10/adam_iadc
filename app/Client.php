<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['ClientName','CreatedOn','ModifiedOn','ModifiedBy','isActive','LineItemType'];

	public $table = "dbo.Client";
}
