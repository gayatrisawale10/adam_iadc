<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignAPIPublisherCampaign extends Model
{
	public $timestamps = false;
	
	protected $primaryKey = 'id';

    public $fillable = ['Campaign_ID','Publisher_ID','PublisherCampaignID','PublisherCampaignName'];

	public $table = "CampaignAPIPublisherCampaign";
}
