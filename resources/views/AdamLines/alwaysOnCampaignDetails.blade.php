<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <title>Adam's Table </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="{{asset('css/bootstrap_v3_0_3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/font-awesome-4.7.0.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/customStyles.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/customColours.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/sweetalert.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{asset('js/adam.js')}}" type="text/javascript"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
  </head>
    <body>
		<nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                      <img class="home_logo_main" src="{{asset('images/iadc-logo.png')}}" height="50">
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                       	<li><a href="{{ url('/Campaign') }}">Campaign</a></li>
		                <li><a href="{{ url('/api') }}">API </a></li>
		                <li><a href="{{ url('/Person') }}"> Person</a></li>
		                <li><a href="{{ url('/Client') }}">Client</a></li>
		                <li><a href="{{ url('/Card') }}">Card</a></li>
                        
                        
                        <li class="dropdown">
                            <a class=" dropdown-toggle" type="button" data-toggle="dropdown">admin@gmail.com<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>                          
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>    
        </nav>
		<!-- ./container -->
        <div class="container cc_tableContainer">
			<!-- Content Header (Page header) -->
			<h4>{{$adamUniqueKey}}</h4>
			<div class="cc_pageButtons">
				<button type="submit" class="btn btn-primary" title="Add" data-toggle="modal" data-target="#mySaveModal">
					<i class="fa fa-plus"></i>
				</button>
				 <!-- Modal -->
				<div class="modal fade" id="mySaveModal" role="dialog">
					<div class="modal-dialog">
				<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<center>
									<h4 class="modal-title">Adam Always On Campaign</h4>
								</center>
							</div>
							<form class="form-inline" method="POST" action="{{URL('saveAdamAlwaysOnCampaign')}}" >
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="AdamsLineItem_ID" value="{{$id}}">
								<div class="modal-body">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="usr">From Date:</h5>
												<div class="col-sm-7">
													<input type="date" class="form-control inputField" id="fromDate" name="fromDate" required="true">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="usr">To Date:</h5>
												<div class="col-sm-7">
													<input type="date" class="form-control inputField" id="toDate" name="toDate" required="true">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputImpressions">Impressions:</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="impressions" name="impressions" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputClick">Clicks:</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="clicks" name="clicks" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputVideoView">Video Views:</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="videoView" name="videoView" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputEngagement">Engagement:</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="engagement" name="engagement" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputVisits">Visits:</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="visits" name="visits" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputLeads">Leads:</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="leads" name="leads" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputReach">Reach:</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="reach" name="reach" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputInstalls">Installs:</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="installs" name="installs" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputSMS">SMS :</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="sms" name="sms" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputMailer">Mailer:</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="mailer" name="mailer" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputSpots">Spots:</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="sports" name="spots" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputNetRate">Net Rate:</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="netRate" name="netRate" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<h5 class="col-sm-5" for="inputNetCost">Net Cost:</h5>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="netCost" name="netCost" autocomplete="off" onkeypress="return isNumberKey(event)">
												</div>
											</div>
										</div>
										<div class="col-md-6">
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<center>
										<input type="submit" class="btn btn-primary" value="Save">
									</center>
								</div>
							</form>	
						</div>
					</div>
				</div>
  
				<!-- <button type="submit" class="btn btn-success" title="mail">
					<i class="fa fa-envelope"></i>
				</button> -->
			</div>
			<hr style="margin-bottom: 5px;"/>
			<!-- content start -->
			
			<table id="myTable" class="cc_Table display">
				<thead class="cc_fixedHeadTableContainer">
					<tr role="row">
						<td class="td">FromDate</td>
						<td class="td">ToDate</td>
						<td class="td">Impressions</td>
						<td class="td">Clicks</td>
						<td class="td">Video Views</td>
						<td class="td">Engagement</td>
						<td class="td">Visits</td>
						<td class="td">Leads</td>
						<td class="td">Reach</td>
						<td class="td">Installs</td>
						<td class="td">SMS</td>
						<td class="td">Mailer</td>
						<td class="td">Spots</td>
						<td class="td">Net Rate</td>
						<td class="td">Net Cost</td>
						<td class="td" style="min-width: 55px !important;">Action</td>	
					</tr>
				</thead>
				<tbody>
					<!-- row -->
					@foreach($alwaysOnCampaignData as $alwaysOnCampaign)
					<tr class="cc_tableRow">
						<td style="word-break: break-all;">
							<input type="hidden" id="alwaysOnCampaignID" value="{{$alwaysOnCampaign->id}}">
							<input type="date" value="{{$alwaysOnCampaign->FromDate}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->FromDate}}" >{{$alwaysOnCampaign->FromDate}}</label>
						</td>
						<td>
							<input type="date" value="{{$alwaysOnCampaign->ToDate}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->ToDate}}" >{{$alwaysOnCampaign->ToDate}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->Impressions}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->Impressions}}" >{{$alwaysOnCampaign->Impressions}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->Clicks}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->Clicks}}" >{{$alwaysOnCampaign->Clicks}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->VideoViews}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->VideoViews}}" >{{$alwaysOnCampaign->VideoViews}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->Engagement}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->Engagement}}" >{{$alwaysOnCampaign->Engagement}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->Visits}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->Visits}}" >{{$alwaysOnCampaign->Visits}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->Leads}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->Leads}}" >{{$alwaysOnCampaign->Leads}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->Reach}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->Reach}}" >{{$alwaysOnCampaign->Reach}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->Installs}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->Installs}}" >{{$alwaysOnCampaign->Installs}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->SMS}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->SMS}}" >{{$alwaysOnCampaign->SMS}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->Mailer}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->Mailer}}" >{{$alwaysOnCampaign->Mailer}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->Spots}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->Spots}}" >{{$alwaysOnCampaign->Spots}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->NetRate}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->NetRate}}" >{{$alwaysOnCampaign->NetRate}}</label>
						</td>
						<td>
							<input type="text" value="{{$alwaysOnCampaign->NetCost}}" class="form-control" style="display: none;">
							<label class="cc-labelWidth setEllipsis" title="{{$alwaysOnCampaign->NetCost}}" >{{$alwaysOnCampaign->NetCost}}</label>
						</td>
						<td>
							<button class="btn btn-primary btnEditAlwaysOn"><i class="fa fa-pencil"></i></button>
							<!-- <button class="btn btn-danger btnDeleteAlwaysOn"><i class="fa fa-trash"></i></button> -->
							<button class="btn btn-success btnSaveAlwaysOn" style="display: none;"><i class="fa fa-check"></i></button>
							<button class="btn btn-danger btnCancelAlwaysOn"  style="display: none;"><i class="fa fa-times"></i></button>
						</td>
					</tr>
					<!-- row -->
					@endforeach
					
				</tbody>
			</table>

			<!-- content end -->
			
		</div><!-- ./container -->

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<!--
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				-->
				<h4 class="modal-title">Warning</h4>
			  </div>
			  <div class="modal-body">
				<p>You may have not saved some changes. Would you like to continue or discard the changes done?</p>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-success" id="btnModalCont">Continue</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="btnModalCancel">Cancel</button>
			  </div>
			</div>

		  </div>
		</div>
    </body>
    <!-- jQuery 2.0.2 -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <!--<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js" type="text/javascript"></script>-->
    <script src="{{asset('js/fastselect.standalone.js')}}"></script>
    <script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>
    

	<script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>
	
   <script src="js/plugins/daterangepicker/daterangepicker.js"></script>
</html>