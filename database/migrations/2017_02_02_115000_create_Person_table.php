<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Person', function (Blueprint $table) {
            $table->increments('Person_ID');
            $table->string('PersonName');
			$table->string('Fathers_name');
            $table->string('Mothers_name');
            $table->string('Husbands_name');
            $table->string('Birth_Place');
            $table->enum('Is_employeed', array('Y', 'N'));
            $table->integer('Gender_ID')->unsigned();
			$table->integer('Salutation_ID')->unsigned();
            $table->integer('Marital_status_id')->unsigned();
			$table->date('DOB');
			$table->integer('Aadhar_ID') ; 
            $table->timestamps();
        });
		
		 Schema::table('Person', function($table) {
       $table->foreign('Gender_ID')->references('Gender_ID')->on('Gender');

   });
   
    Schema::table('Person', function($table) {
       $table->foreign('Salutation_ID')->references('Salutation_ID')->on('Salutation');

   });

     Schema::table('Person', function($table) {
       $table->foreign('Marital_status_id')->references('Marital_status_id')->on('Marital_Status');

   });
   
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Person');
    }
}
