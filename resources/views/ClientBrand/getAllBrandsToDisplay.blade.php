@extends('layouts.appResourcePublisher')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left"><h2>{{$ClientName}} : Brand Details</h2></div>
            <div class="pull-right margin-top-20">
                <a class="btn btn-primary" href="{{ URL::route('ClientBrand.createBrand', $c_id)}}">
                    Create Brand
                </a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered display" id="sample_editable_1">
        <thead> 
            <tr>
                <th>Brand Name</th>
                <th width="200px">Action</th>
            </tr>
        </thead> 
        <tbody> 
           @foreach ($brandDetails_index as $brandDetails_index_data) 
            <tr align="center">
                <td>{{$brandDetails_index_data->BrandName}}</td>
                <td>
                    <a class="btn btn-primary" href="{{ URL::route('ClientBrand.editBrand',
                                                       [$brandDetails_index_data->id, $c_id])}}">
                                                       Edit
                    </a>
                  
                     {!! Form::open(array('route'=>array('ClientBrand.destroyBrand',$brandDetails_index_data->id,$c_id),'method' => 'DELETE','style'=>'display:inline')) !!}
                    
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger', 'onclick'=>'return confirm("Are you sure you want to delete?")']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
           @endforeach
        </tbody>
    </table>
@endsection

