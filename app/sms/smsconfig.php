<?php

namespace App\sms;

use Illuminate\Database\Eloquent\Model;

class smsconfig extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'smsconfigs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['sms_type', 'isactive', 'modified_by', 'sms_text_1', 'sms_text_2', 'sms_text_3', 'sms_text_4', 'sms_text_5', 'sms_text_6', 'sms_text_7', 'sms_text_8', 'sms_text_9', 'sms_text_10'];

    
}
