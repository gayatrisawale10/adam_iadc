<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use \App\Http\Controllers\ApiController;
use \App\Http\Controllers\AdamAPIController; 
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    protected $dts = [
        //
    ];
    
    
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // $schedule->call(function () {
        //     $adamApi = new AdamAPIController;        
        //     $adamApi->fbAPIProcessing();
        //     $adamApi->adamDcm();
        //     $adamApi->adamAdwords();
        //     $adamApi->testAdamSizMek();
        //     $adamApi->dbmBasic();
        //     $adamApi->adamGoogleAnalytics();
        //     $adamApi->AdamTaboola();
        //     $adamApi->AdamOutbrain();
        // });
        // 1518===74
        // ============================================================================================================        
        // // ------------------fbAPIProcessing------------------------------------
        // $schedule->call(function () {
        //     $adamApi = new AdamAPIController;        
        //     $adamApi->fbAPIProcessing();   
        // })->everyMinute();//->dailyAt('22:30');
        //  // ------------------adamDcm------------------------------------ 
        // $schedule->call(function () {
        //     $adamApi = new AdamAPIController;        
        //     $adamApi->adamDcm();
        // })->everyMinute();//->dailyAt('22:45');
        // // ------------------adamAdwords------------------------------------
        // $schedule->call(function () {
        //     $adamApi = new AdamAPIController;        
        //     $adamApi->adamAdwords();   
        // })->everyMinute();//->everyThirtyMinutes();//->dailyAt('23:00');
       
        // //------------------testAdamSizMek------------------------------------
        // $schedule->call(function () {
        //     $adamApi = new AdamAPIController;        
        //     $adamApi->testAdamSizMek();
        // })->everyMinute();//->everyThirtyMinutes();//->dailyAt('21:48');   
        // // ------------------AdamTaboola------------------------------------
        // $schedule->call(function () {
        //     $adamApi = new AdamAPIController;        
        //     $adamApi->AdamTaboola();   
        // })->everyMinute();//->dailyAt('21:51');
        // // ------------------AdamOutbrain------------------------------------
        // $schedule->call(function () {
        //     $adamApi = new AdamAPIController;        
        //     $adamApi->AdamOutbrain();
        // })->everyMinute();//->dailyAt('21:52');
        // // ------------------dbmCron------------------------------------
        // $schedule->call(function () {
        //     $adamApi = new AdamAPIController;        
        //     $adamApi->dbmCron();   
        // })->everyMinute();//->dailyAt('22:30');
        // //============================================================================================================     
         
        // // ------------------dbmBasic-------------------------------------
        // $schedule->call(function () {
        //     $adamApi = new AdamAPIController;        
        //     $adamApi->dbmBasic();   
        // })->everyMinute();
        // //->dailyAt('12:01');//->everyMinute();
        //  // ------------------adamGoogleAnalytics-------------------------
        // $schedule->call(function () {
        //     $adamApi = new AdamAPIController;        
        //     $adamApi->adamGoogleAnalytics();
        // })->everyMinute();///->dailyAt('21:50');

        // //------------------dailyProgrssCheck-----------------------------
        //     $schedule->call(function () {
        //     $adamApi = new AdamAPIController;        
        //     $adamApi->dailyProgrssCheck();
        // })->everyMinute();//->dailyAt('23:15');
    
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
