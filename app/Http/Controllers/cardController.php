<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Card;
use App\Creative;
use App\UserRole;
use Session;
use Redirect;
use Validator;
use Carbon;

class cardController extends Controller
{
    //
    public function index()
    {
      $get_card=DB::table('Card')
                    ->where('isActive',1)->get();
      $listofCreative=[];
      
      if($get_card){
        foreach ($get_card as $key) {
          $get_craetive=DB::table('Creative')
                        ->where('Card_ID',$key->id)->get();
            $creativeList=[];
            $res="";
            if(!empty($get_craetive)){
              foreach ($get_craetive as $key1) {
                  array_push($creativeList,$key1->CreativeName);
                  $res=implode(",",$creativeList);
              }
              array_push($listofCreative,$res);
            } 
        }
      }  
      return view('Card.index', compact('get_card','listofCreative'));
    }

    public function saveCard(Request $request)
    {

    	$card = new Card;
    	$card->CardName = $request->cardName;
      $card->isActive = 1;
    	$card->save();
    	Session::flash("success", "Card Created Successfully");
    	return Redirect::back();
    }


    public function editCard(Request $request)
    {
      $card = Card::find($request->user_id);
      $card->CardName = $request->name;
      $card->save();
      Session::flash("success", "Card Edited Successfully");
      return Redirect::back();
    }

    public function deleteCard($id)
    {
      $card = Card::findOrFail($id);
      $card->isActive =0;
      $card->save();
      Session::flash("success", "Card Removed Successfully");
      return Redirect::back();
    }

    public function addCreative(Request $request)
    {
      $creative = new Creative;
    	$creative->CreativeName = $request->creative_name;
      $creative->Card_ID = $request->user_id;
    	$creative->save();
    	Session::flash("success", "creative Created Successfully");
    	return Redirect::back();
    }

}
