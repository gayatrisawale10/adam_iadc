@extends('layouts.appAdamPublisher')
@section('content')
<style type="text/css">
  
  .campTable {
    table-layout: fixed;
  }  
  .setEllipsis
  {
    white-space: nowrap; 
    overflow: hidden;
    text-overflow: ellipsis;
    cursor: pointer;
  }
  .campTable
  {
    user-select: none;
  }
</style>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <div class="row">
    <div class="col-lg-12">
      <div class="pull-left">
        <h2> Campaign Details</h2>
      </div>
      <div class="pull-right margin-top-20">
      </div>
    </div>        
  </div>
  <div class="portlet-body">
    <div class="table-toolbar">
      <div style="width: 100%; overflow-x:auto;margin-bottom: 50px;">
        <table class="table table-bordered campTable" id="sample_editable_1" >
          <thead>                  
              <th style="width: 10%;">Client Name</th>
              <th style="width: 10%;">Brand Name</th>
              <th style="width: 20%;">Campaign Name</th>
              <th style="width: 10%;">Campaign ID</th>
              <th style="width: 10%;">Always On</th>
              <th style="width: 10%;">Start Date</th>
              <th style="width: 10%;" >End Date</th>
              <th style="width: 10%;">Created On</th> 
              <th style="width: 10%;">Action</th>
          </thead>
          <tbody> 
            @foreach( $getCompaignDetails as $eachCampaign )  
             <tr align="center">       
              <td class="setEllipsis" title=''>{{$eachCampaign['ClientName']}}</td>
              <td class="setEllipsis" title=''>{{$eachCampaign['BrandName']}}</td>
              <td class="setEllipsis" title=''>{{$eachCampaign['CampaignName']}}</td> 
              <td class="setEllipsis" title=''>{{$eachCampaign['CampaignID']}}</td>
              @if($eachCampaign['alwaysOn']==1)
                <td>Yes</td>
              @else  
                <td>No</td>
              @endif  
              <td>{{$eachCampaign['StartDate']}}</td>
              <td>{{$eachCampaign['EndDate']}}</td>
              <td>{{$eachCampaign['CreatedOn']}}</td>  
              <td>
                <a class="btn btn-primary btn-md" title="AdamLine Item" href="{{URL::route('adamPublihser.adamPublihserAdamLine',['Campaign_ID' => $eachCampaign['Campaign_ID'], 'PublisherId' => $PublisherId ]) }}">
                  <i class="fa fa-plus-square"> </i>
                </a>
              </td>    
            </tr>
            @endforeach  
          </tbody>
        </table>
        @endsection
      </div>
    </div>
  </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

