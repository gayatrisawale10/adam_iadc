<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Users;
use App\UserRole;
use Session;
use Redirect;
use Validator;
use Carbon;


class PersonController extends Controller
{
    


    public function index()
    {
    	$users = Users::where('isActive', 1)
    			->select('users.id as id', 'users.name as name', 'users.email as email', 'UserRole.UserRoleName', 'users.UserRole_ID',
    					 'UserRole.IsPerson'	
    					)
    			->rightjoin('UserRole', 'UserRole.id', '=', 'users.UserRole_ID')
    			->where('IsPerson', 1)
    			->get();
    	//dd($users);
    	$role = UserRole::where('IsPerson', true)->get();

    	return view('Person.index', compact('role', 'users'));
    }


    public function savePerson(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255|unique:users'
        ]);

        if ($validator->fails()) {
        	Session::flash("error", "Email Id already Exist");
            return Redirect::back();
        }

        $time = Carbon\Carbon::now();


    	$user = new Users;
    	$user->UserRole_ID = $request->role;
    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->password = bcrypt($request->password);
    	$user->CreatedOn = $time->toDateTimeString();
    	$user->ModifiedOn = $time->toDateTimeString();
    	$user->ModifiedBy = Auth::user()->id;
    	$user->isActive = True;

    	$user->save();
    	Session::flash("success", "User Created Successfully");
    	return Redirect::back();
    }


    public function editPerson(Request $request)
    {
    	// return $request;

    	$validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255|'
        ]);

        

        $time = Carbon\Carbon::now();


    	$user = Users::find($request->user_id);
    	$user->UserRole_ID = $request->role;
    	$user->name = $request->name;
    	$user->email = $request->email;
    	if(!empty($request->password)){
    		$user->password = bcrypt($request->password);
    	}
    	$user->CreatedOn = $time->toDateTimeString();
    	$user->ModifiedOn = $time->toDateTimeString();
    	$user->ModifiedBy = Auth::user()->id;
    	$user->isActive = True;

    	$user->save();
    	Session::flash("success", "User Data Edited Successfully");
    	return Redirect::back();
    }


    public function deletePerson($id)
    {
    	$user = Users::findOrFail($id);
    	$user->isActive = false;
    	$user->save();
    	Session::flash("success", "User Removed Successfully");
    	return Redirect::back();
    }
}
