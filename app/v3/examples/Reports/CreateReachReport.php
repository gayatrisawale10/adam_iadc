<?php
namespace App\v3\examples\Reports;
/*
 * Copyright 2015 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Require the base class.
use App\v3\examples\BaseExample;

use Google_Service_Dfareporting_Report;
use Google_Service_Dfareporting_DateRange;
use Google_Service_Dfareporting_SortedDimension;
use Google_Service_Dfareporting_ReportCriteria;
use Google_Service_Dfareporting_ReportCompatibleFields;
use Google_Service_Dfareporting;
use Google_Service;
use Google_Service_Dfareporting_DimensionValueRequest;
use Google_Service_Dfareporting_ReportReachCriteria;

use DB;

/*
 * An end-to-end example of how to create a standard report.
 */
class CreateReachReport extends BaseExample {
  /**
   * {@inheritdoc}
   * @see BaseExample::getInputParameters()
   * @return array
   */
  protected function getInputParameters() {
    return [['name' => 'user_profile_id',
             'display' => 'User Profile ID',
             'required' => true]];
  }

  /**
   * {@inheritdoc}
   * @see BaseExample::run()
   */
  public function run() {

    echo"<pre> Adam Report";  //exit;
   // $values = $this->formValues;

    print '<h2>Creating a new standard report</h2>';
    $this->flush();

    //$userProfileId = $values['user_profile_id'];
    $userProfileId = $_SESSION['user_profile_id'];

    $dcmReportType=$_SESSION['dcmReportType'];
    //echo"in createFile"; print_r($dcmReportType); exit;

    // 1. Create a report resource.
    $report = $this->createReportResource($dcmReportType);

    // 2. Define the report criteria.
    $this->defineReportCriteria($report, $dcmReportType);

    // 3. (optional) Look up compatible fields.
    $this->findCompatibleFields($userProfileId, $report);

    // 4. Add dimension filters to the report criteria.
    $this->addDimensionFilters($userProfileId, $report);
   
    // 5. Save the report resource.
    $report = $this->insertReportResource($userProfileId, $report);

    //echo"report: "; print_r($report); //exit;

    $_SESSION['report_id'] = $report->id;

    $this->printResultsTable('Standard Report', [$report]);
  }

  private function createReportResource($dcmReportType) {
    $report = new Google_Service_Dfareporting_Report();

    // Set the required fields "name" and "type".
    $report->setName('Example standard report');
    //$report->setType('STANDARD');
    $report->setType($dcmReportType);

    // Set optional fields.
    $report->setFileName('example_report_'.$dcmReportType);
    $report->setFormat('CSV');

    return $report;
  }

  private function defineReportCriteria($report,$dcmReportType) {
      
    $PublisherID =  DB :: table('Publisher')
                ->where('Publisher.PublisherName','=','DCM')
                ->select('Publisher.id')->get();
    $Publisher_ID = json_decode($PublisherID,true);
    $p_id = $Publisher_ID[0]['id'];
  
    $hiddenMetricsID_Out = DB :: table('HiddenMetrics')          
                  ->join('APIMetrics', 'APIMetrics.HiddenMetrics_ID', '=', 'HiddenMetrics.id')          
                  ->join('APISource', 'APIMetrics.APISource_ID','=', 'APISource.id')  
                  ->where('HiddenMetrics.Publisher_ID' ,'=',$p_id) 
                  ->where('APISource.APIName','=',$dcmReportType )
                  ->get();
    $hiddenMetricsID_Result = json_decode($hiddenMetricsID_Out,true);
    //echo "hiddemMetrics: "; print_r($hiddenMetricsID_Result); exit;
    // ###################################################################
    $arrayParamsBreakdownNames =[];
    $arrayMetricNames =  [];

    foreach($hiddenMetricsID_Result as $key=>$value) {
      //echo"<pre>"; print_r( $key ); print_r( $value['ApiMetricsName'] ); exit; 
      if($value['typeFlag']==1 || $value['typeFlag']==3 ){
          array_push($arrayParamsBreakdownNames,$value['PublisherMetricsParameter']);
      }
      else
      if($value['typeFlag']==2 ){
          array_push($arrayMetricNames,$value['PublisherMetricsParameter']);
        }
    }
    // ###################################################################
    //echo "<pre>arrayParamsBreakdownNames: "; print_r($arrayParamsBreakdownNames);
    //echo "<pre><br>arrayMetricNames: "; print_r($arrayMetricNames);
    //exit;
                    
    // Define a date range to report on. This example uses explicit start and
    // end dates to mimic the "LAST_30_DAYS" relative date range.
    $dateRange = new Google_Service_Dfareporting_DateRange();    
    $dateRange->setStartDate($_SESSION['startDate']);
    $dateRange->setEndDate($_SESSION['endDate']);
    //$dateRange->setEndDate(date('Y-m-d'));
    // ###########################################################################################################################
    $dimensionDate = new Google_Service_Dfareporting_SortedDimension();
    $dimensionDate->setName('dfa:date');

    $dimensionArray=[];
    array_push($dimensionArray,$dimensionDate);
    // ############################################
    foreach ($arrayParamsBreakdownNames as $ParamBreakdown ) {      
          //dd($ParamNames);
          $dimension = new Google_Service_Dfareporting_SortedDimension();
          $dimension->setName($ParamBreakdown);
          array_push($dimensionArray,$dimension);
    }     
    //echo"dimensionArray: "; print_r($dimensionArray); //exit;
    //echo"############################################";
    $criteria = new Google_Service_Dfareporting_ReportReachCriteria();
    
     //$criteria = new Google_Service_Dfareporting_Report();
    $criteria->setDateRange($dateRange);
    $criteria->setDimensions($dimensionArray);
      
    //https://developers.google.com/doubleclick-advertisers/v3.2/dimensions#standard-filters
    //https://developers.google.com/doubleclick-advertisers/v3.2/dimensions#standard-metrics

    $criteria->setMetricNames($arrayMetricNames);
    // Add the criteria to the report resource.
    $report->setReachCriteria($criteria);
    //echo "<pre>"; print_r($report); exit;
    // ###########################################################################################################################
  }

  private function findCompatibleFields($userProfileId, $report) {
    $fields = BaseExample::$service->reports_compatibleFields->query($userProfileId,$report);

    $reportFields = $fields->getReachReportCompatibleFields();

    //echo "<pre>reportFields "; print_r($reportFields); exit;

    if (!empty($reportFields->getDimensions())) {
      // Add a compatible dimension to the report.
      $dimension = $reportFields->getDimensions()[0];
      $sortedDimension = new Google_Service_Dfareporting_SortedDimension();
      $sortedDimension->setName($dimension->getName());
      $report->getReachCriteria()->setDimensions(
          array_merge($report->getReachCriteria()->getDimensions(),
              [$sortedDimension]));
    } else if (!empty($reportFields->getMetrics())) {
      // Add a compatible metric to the report.
      $metric = $reportFields->getMetrics()[0];
      $report->getReachCriteria()->setMetricNames(
          array_merge($report->getReachCriteria()->getMetricNames(),
              [$metric->getName()]));
    }
  }

  private function addDimensionFilters($userProfileId, $report) {
    // Query advertiser dimension values for report run dates.
    $request = new Google_Service_Dfareporting_DimensionValueRequest();
    $request->setStartDate(
    $report->getReachCriteria()->getDateRange()->getStartDate());
    $request->setEndDate(
    $report->getReachCriteria()->getDateRange()->getEndDate());
    $request->setDimensionName('dfa:advertiser');

    $values =
        BaseExample::$service->dimensionValues->query($userProfileId, $request);

    if (!empty($values->getItems())) {
      // Add a value as a filter to the report criteria.
      // $report->getCriteria()->setDimensionFilters([$values->getItems()[0]]);
      $report->getReachCriteria()->setDimensionFilters([$values->getItems()]);
    }
  }

  private function insertReportResource($userProfileId, $report) {
    $insertedReport =
        BaseExample::$service->reports->insert($userProfileId, $report);
    return $insertedReport;
  }

  /**
   * {@inheritdoc}
   * @see BaseExample::getName()
   * @return string
   */
  public function getName() {
    return 'Create Reach Report';
  }
  /**
   * {@inheritdoc}
   * @see BaseExample::getResultsTableHeaders()
   * @return array
   */
  public function getResultsTableHeaders() {
    return ['id' => 'Report ID',
            'name' => 'Report Name'];
  }
}
