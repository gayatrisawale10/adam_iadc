<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class Users extends Authenticatable
{
    use Notifiable;
	use EntrustUserTrait; // add this trait to your user model
    public $timestamps = false;
    protected $primaryKey = 'id';
   
    protected $fillable = [
        'UserRole_ID','name','email',  'password', 'CreatedOn','ModifiedOn','ModifiedBy','isActive'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];
	
	public $table = "users";
	
	
}
