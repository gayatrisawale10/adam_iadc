<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdamLineDCMMapping extends Model
{

	public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['AdamsLineItem_ID','DCMKey','CampaignType','AdSection','BannerSize','Device','CreatedBy','CreatedOn','ModifiedBy','ModifiedOn','isActive'];

	public $table = "dbo.AdamLineDCMMapping";

}
