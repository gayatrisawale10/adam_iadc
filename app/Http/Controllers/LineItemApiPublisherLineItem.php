<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineItemApiPublisherLineItem extends Model
{
	public $timestamps = false;
	
	protected $primaryKey = 'id';

    public $fillable = ['CampaignAPIPublisherCampaign_ID','LineItem_ID','Publisher_ID','PublisherLineItemID','PublisherLineItemName'];

	public $table = "LineItemApiPublisherLineItem";
}
