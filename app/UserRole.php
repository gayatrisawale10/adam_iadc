<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{

	public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['UserRoleName','CreatedOn','ModifiedOn','ModifiedBy','isActive'];

	public $table = "dbo.UserRole";

}
