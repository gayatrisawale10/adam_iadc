<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignPublisher extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'id';

    public $fillable = ['Campaign_ID','Publisher_ID','AdTypes_ID','isActive','ModifiedBy','ModifiedOn','CreatedOn'];

	public $table = "CampaignPublisher";
}
