// -------------variable Defiened for adamDCMMApping----------------- 
var mult_placement_mapp_val = "";
var CampaignType_val = "";
var Ad_section_val = "";
var BannerSize = "";
var Device = "";
// -------------end variable Defiened for adamDCMMApping-----------------
$(document).ready(function()
{
  // ###############################################################
  $(".addMoreEmail").click(function()
  {
    //alert("email");
    //mailCounter++;
    var markup = "<div class='col-md-12'><div class='row form-group'><div class='col-md-2'><h5><b>E-mail : <span style='color: red'>*</span></b></h5></div><div class='col-md-8'><input type='email' name='email[]' class='form-control' required='true' /></div><div class='col-md-2'><div class='btn btn-danger removeEmail'><i class='fa fa-times'></i></div></div></div></div>"; 
    $(".appendEmail").append(markup);   
    $(".removeEmail").click(function (){
      $(this).parent().parent().remove();
    }); 
  }); 
  // ###############################################################
  //$('#myTable').DataTable({
  $('.myTable').DataTable({
    // fixedHeader: true,
    // dom: 'Bfrtip',
    // buttons: ['excel'],
    // paging: false,
    // searching: false
  });
  // ----------------refresh on click---------------------
  $( ".refreshOnClose" ).click(function() {
    location.reload(true);
  });
  // ----------------end refresh on click---------------------
  // -------------function-------------------------------
   // -------------function Defiened for uploadExcelForManualPublisher-----------------
  $(document).on('click', '.upload_URL_ForManualPublisher', function() {

    //alert("dd");
    AdamsLineItem_ID = $(this).attr('data-AdamsLineItem_ID');
    $('#AdamsLineItem_IDU').val(AdamsLineItem_ID); 
    //alert(AdamsLineItem_ID);

    PublisherName = $(this).attr('data-PublisherName');
    //PublisherName=$.trim(Publisher)+'Excel'; 
    //alert(PublisherName);
    $('#PublisherNameU').val(PublisherName);
    // ------------------------------------------------------------------------------------------------------------------------------
    var CSRF_TOKEN=$("#token").val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-Token': $('meta[name=_token]').attr('content')
      }
    });
    $.ajax({
      type: 'POST',
      url: '../getAdamLineItem_URLMapping',
      data: { "_token": CSRF_TOKEN, 'AdamsLineItem_ID':AdamsLineItem_ID },
      success: function (data ) {
        urlList =  data.getAdamLineItem_URLMapping;
        $('.urlList tbody').html('');

         for($i=0; $i<urlList.length; $i++){
           $('.urlList tbody').append("<tr><td class='text-center' ><label class='setEllipsis' title='"+urlList[$i].URLName+"'>"+urlList[$i].URLName+"</td></tr>");
         }
        $('#urlList').DataTable({
          fixedHeader: true
        });
      }
    }); 
    // ------------------------------------------------------------------------------------------------------------------------------
  });  
  // -------------function uploadExcelForManualPublisher-------------------------------
  // -------------function Defiened for uploadExcelForManualPublisher-----------------
  $(document).on('click', '.uploadExcelForManualPublisher', function() {

    AdamsLineItem_ID = $(this).attr('data-AdamsLineItem_ID');
    $('#AdamsLineItem_IDF').val(AdamsLineItem_ID); 
    //alert(AdamsLineItem_ID);

    PublisherName = $(this).attr('data-PublisherName');
    //PublisherName=$.trim(Publisher)+'Excel'; 
    //alert(PublisherName);
    $('#PublisherName').val(PublisherName);
    // ------------------------------------------------------------------------------------------------------------------------------
    var CSRF_TOKEN=$("#token").val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-Token': $('meta[name=_token]').attr('content')
      }
    });
    $.ajax({
      type: 'POST',
      url: '../getManualPublisherExcelInputFile',
      data: { "_token": CSRF_TOKEN, 'AdamsLineItem_ID':AdamsLineItem_ID },
      success: function (data ) {
        getManualPublisherExcelInputFileDataArray =  data.getManualPublisherExcelInputFile;
        $('.manualPubReportTable tbody').html('');

         for($i=0; $i<getManualPublisherExcelInputFileDataArray.length; $i++){
           $('.manualPubReportTable tbody').append("<tr><td class='text-center' ><label class='setEllipsis' title='"+getManualPublisherExcelInputFileDataArray[$i].PubReportName+"'>"+getManualPublisherExcelInputFileDataArray[$i].PubReportName+"</label> </td><td><a href='../"+getManualPublisherExcelInputFileDataArray[$i].PubReportPath+"'>"+getManualPublisherExcelInputFileDataArray[$i].PubReportName +"<i class='fa fa-download'></i> </a></td><td><a href='../deleteManulReport/"+getManualPublisherExcelInputFileDataArray[$i].id+"'>Delete</a></td></tr>");
         }
        $('#manualPubReportTable').DataTable({
          fixedHeader: true
        });
      }
    }); 
    // ------------------------------------------------------------------------------------------------------------------------------
  });  
  // -------------function uploadExcelForManualPublisher-------------------------------
  // -------------function Defiened for adamDCMMApping-----------------
  $(document).on('click', '.mapAdamDCM', function() {
    $('.DCMKeyInput').empty();
    AdamsLineItem_ID = $(this).attr('data-AdamsLineItem_ID');
    $('#AdamsLineItem_IDD').val(AdamsLineItem_ID);

    ClientId = $(this).attr('data-ClientId');
    $('#spClientId').val(ClientId);
     
    specialClientEditAdamKeyString = $(this).attr('data-specialClientEditAdamKeyString');
    
    // ---------------------------------------- Editable DCMKey --------------------------------------
    if(specialClientEditAdamKeyString.indexOf(ClientId) != -1){
        //alert(ClientId + " found");
        var markup = "<input type='text' class='form-control' id='DCMKey'  name='DCMKey[]'/>";    
        $('.DCMKeyInput').append(markup);
    } 
    // ----------------------------------------End Editable DCMKey -----------------------------------
    else
    {
      //alert(ClientId + " NOT found");
      var markup = "<td><input type='text' class='form-control map_val' id='DCMKey' name='DCMKey[]' readonly='true'/></td>";
      $('.DCMKeyInput').append(markup);
      $(".map_val").blur(function() {
        CampaignType_val = $("#CampaignType").val();
        Ad_section_val = $("#AdSection").val();
        BannerSize = $("#BannerSize").val();
        Device = $("#Device").val();

        mult_placement_mapp_val = CampaignType_val+"_"+Ad_section_val+"_"+BannerSize+"_"+Device;
        console.log("mult_placement_mapp_val : ",mult_placement_mapp_val);
        $("#DCMKey").val(mult_placement_mapp_val);
      });
    }
    // ---------------------------- add new blank Line Item------------------------------------------
    var k=1;
    $(".insert-row").click(function()
    {
      if(specialClientEditAdamKeyString.indexOf(ClientId) != -1){
        var markup = "<tr><td><input type='text' class='form-control map_val' id='DCMKey_"+k+"' name='DCMKey[]'></td><td><input type='text' class='form-control map_val' id='CampaignType_"+k+"' name='CampaignType[]'></td><td><input type='text' class='form-control map_val' id='AdSection_"+k+"' name='AdSection[]'></td><td><input type='text' class='form-control map_val' id='BannerSize_"+k+"' name='BannerSize[]'></td><td><input type='text' class='form-control map_val' id='Device_"+k+"' name='Device[]'></td><td align='center'><button type='submit' class='btn btn-danger remove-row'><i class='fa fa-times'></i></button></td></tr>"; 
        $("#mapTable tbody").append(markup);
        $(".remove-row").click(function (){
          $(this).parents("tr").remove();
        });
      } 
      else{
        var markup = "<tr><td><input type='text' class='form-control map_val' id='DCMKey_"+k+"' name='DCMKey[]' readonly='true'></td><td><input type='text' class='form-control map_val' id='CampaignType_"+k+"' name='CampaignType[]'></td><td><input type='text' class='form-control map_val' id='AdSection_"+k+"' name='AdSection[]'></td><td><input type='text' class='form-control map_val' id='BannerSize_"+k+"' name='BannerSize[]'></td><td><input type='text' class='form-control map_val' id='Device_"+k+"' name='Device[]'></td><td align='center'><button type='submit' class='btn btn-danger remove-row'><i class='fa fa-times'></i></button></td></tr>";
        $("#mapTable tbody").append(markup); 
        $(".remove-row").click(function (){
          $(this).parents("tr").remove();
        });

        var DCMKey_val = "#DCMKey_"+k;
        var CampaignType_val = "#CampaignType_"+k;
        var Ad_section_val = "#AdSection_"+k;
        var BannerSize_val = "#BannerSize_"+k;
        var Device_val = "#Device_"+k;
        
        $(".map_val").blur(function() {
          //alert('blur');
          $(DCMKey_val).val($(CampaignType_val).val()+"_"+$(Ad_section_val).val()+"_"+$(BannerSize_val).val()+"_"+$(Device_val).val());
        });
      }
      k++;
    });
    // ----------------------------End add new blank Line Item------------------------------------------
  });
  // -------------end function Defiened for adamDCMMApping-----------------    
  // --------------add single adamLine-------------------------------------
  $('#addOneMoreEntry').click(function(){
    $.ajax({
      type: 'get',
      url: '/getDitData',
      data: {},
      success: function (response) {
        var campID=$("#campID").val();
        var ClientId=$("#ClientId").val();
        var specialClientEditAdamKeyString=$("#specialClientEditAdamKeyString").val();

        //=====================String will converted to Array===============
        var specialClient = specialClientEditAdamKeyString.split(',');
        var checkSpecialClient=false;

        for(i=0; i<specialClient.length; i++){
          if(specialClient[i]==ClientId){
            checkSpecialClient=true;
          }
        }
        //==========================================================
        
        if(checkSpecialClient){
            //alert(ClientId + " found");
            var markup = "<tr><td style='border-right: 1px solid #CCC;'><input class='form-control' value='' /></td><td><label id='ditLabel_-1' class='setEllipsis'></label><select class='form-control DataInputTypeName_-1' id='dd1_-1' multiple='multiple' name='DataInputTypeName[]' size='5' style='width: 130px;'></select></td><td></td><td><div id='secBtns_-1'><button type='submit' class='btn btn-success saveAdamLine'><i class='fa fa-check'></i></button><button type='submit' class='btn btn-danger remove-row'><i class='fa fa-times'></i></button></div></td><td><select class='form-control ActivityType' size='5' style='width: 130px;'></select></td><td><select class='form-control Websites' size='5' style='width: 130px;'></select></td><td><select class='form-control Vendor' size='5' style='width: 130px;'></select></td><td><input type='date' class='form-control' value=''/></td><td><input type='date' class='form-control' value=''/></td><td><select class='form-control Platform' size='5' style='width: 130px;'></select></td><td><select class='form-control PaidOwned' size='5' style='width: 130px;'></select></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td></tr>";
        }
        else{
           //alert(ClientId + " NOT found");
           var markup = "<tr><td style='border-right: 1px solid #CCC;'><input type='hidden' class='form-control' value=''/></td><td><label id='ditLabel_-1' class='setEllipsis'></label><select class='form-control DataInputTypeName_-1' id='dd1_-1' multiple='multiple' name='DataInputTypeName[]' size='5' style='width: 130px;'></select></td><td></td><td><div id='secBtns_-1'><button type='submit' class='btn btn-success saveAdamLine'><i class='fa fa-check'></i></button><button type='submit' class='btn btn-danger remove-row'><i class='fa fa-times'></i></button></div></td><td><select class='form-control ActivityType' size='5' style='width: 130px;'></select></td><td><select class='form-control Websites' size='5' style='width: 130px;'></select></td><td><select class='form-control Vendor' size='5' style='width: 130px;'></select></td><td><input type='date' class='form-control' value=''/></td><td><input type='date' class='form-control' value=''/></td><td><select class='form-control Platform' size='5' style='width: 130px;'></select></td><td><select class='form-control PaidOwned' size='5' style='width: 130px;'></select></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td><td><input class='form-control' value=''/></td></tr>";
        } 
        //var markup = "<tr><td style='border-right: 1px solid #CCC;'><label class='setEllipsis'></label></td><td><label id='ditLabel_-1' class='setEllipsis'></label><select class='form-control DataInputTypeName_-1 enableDisabled' id='dd1_-1' multiple='multiple' name='DataInputTypeName[]' size='5' style='width: 130px;'></select></td><td></td><td><div id='secBtns_-1'><button type='submit' class='btn btn-success saveAdamLine'><i class='fa fa-check'></i></button><button type='submit' class='btn btn-danger confirmCancel'><i class='fa fa-times'></i></button><button type='submit' class='btn btn-danger remove-row'><i class='fa fa-trash'></i></button></div></td><td><select class='form-control ActivityType enableDisabled' size='5' style='width: 130px;'></select></td><td><select class='form-control Websites enableDisabled' size='5' style='width: 130px;'></select></td><td><select class='form-control Vendor enableDisabled' size='5' style='width: 130px;'></select></td><td><input type='date' class='form-control enableDisabled' value=''/></td><td><input type='date' class='form-control enableDisabled' value=''/></td><td><select class='form-control Platform enableDisabled' size='5' style='width: 130px;'></select></td><td><select class='form-control PaidOwned enableDisabled' size='5' style='width: 130px;'></select></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td><td><input class='form-control enableDisabled' value=''/></td></tr>";

        $('.entryTable tbody').append(markup);
          
        $(".remove-row").click(function (){
          $(this).parents("tr").remove();
        });

        // ------------------------------ all select input--------------------------------------------
        $.each(response.dit, function(index){
          $(".DataInputTypeName_-1").append($("<option></option>")
            .attr('id',response.dit[index].id)
            .text(response.dit[index].DataInputTypeName)
          );
        });

        $.each(response.activity_type, function(index){
          $(".ActivityType").append($("<option></option>")
              .attr('id',response.activity_type[index].id)
              .text(response.activity_type[index].Activity_Name)
          );
        });

        $.each(response.platform, function(index){
          $(".Platform").append($("<option></option>")
              .attr('id',response.platform[index].id)
              .text(response.platform[index].Platfform_Name)
          );
        });

        $.each(response.paid_owned, function(index){
          $(".PaidOwned").append($("<option></option>")
              .attr('id',response.paid_owned[index].id)
              .text(response.paid_owned[index].Paid_Owned_Name)
          );
        });

        $.each(response.vendor, function(index){
          $(".Vendor").append($("<option></option>")
              .attr('id',response.vendor[index].id)
              .text(response.vendor[index].Supplier_Name)
          );
        });

        $.each(response.websites, function(index){
          $(".Websites").append($("<option></option>")
              .attr('id',response.websites[index].id)
              .text(response.websites[index].Site_Name)
          );
        });
        // ------------------------------end all select input--------------------------------------------  
        $(".confirmCancel").click(function(){
          showAlert();
        });
        // ------------------------------ --------------------------------------------

        $(".saveAdamLine").click(function(){
          //----------- Selected Option Array List------------------//
          var sel = new Array();
          var data = $(this).closest("tr").find('td').find("select");
          $.each(data, function(index){
            sel[index] = data[index].selectedOptions;
          });

          var FinalSelArrList = new Array();
          //------------Select Filed Values------------------//
          $.each(sel, function(index){
            var ArrList = new Array();
            if(index === 0){
              if(sel[index].length != 0){
                $.each(sel[index], function(index2){
                  ArrList.push(sel[index][index2].id);
                });
                FinalSelArrList.push(ArrList);
              } else{
                ArrList.push('');
                FinalSelArrList.push(ArrList);
              }
            } else{
              if(sel[index].length != 0){
                $.each(sel[index], function(index2){
                  ArrList.push(sel[index][index2].value);
                });
                FinalSelArrList.push(ArrList);
              } else{
                ArrList.push('');
                FinalSelArrList.push(ArrList);
              }
            }
          });
          //------------Input Filed Values------------------//
          var inputFieldValues = $(this).closest('tr').find('td').find('input');
          var FinalInputArray = new Array();

          $.each(inputFieldValues, function(index){
            FinalInputArray[index] = inputFieldValues[index].value;
          });

          FinalInputArray.push(campID);
          FinalInputArray.push(ClientId);

          var CSRF_TOKEN = $('meta[name="_token"]').attr('content');

          if(checkSpecialClient){
            if(FinalInputArray[0]!=''){
              $.ajax({
                type: 'POST',
                url: '../saveAdamLineItemRow',
                data: { "FinalSelArrList": FinalSelArrList, 'FinalInputArray' : FinalInputArray, "_token": CSRF_TOKEN},
                success: function (response) {
                  if(response.res == 'success') {
                    $("#emailDialog").modal("show");
                    swal(
                      '',
                      'Successfully saved',
                      'success'
                    );
                    setTimeout(function(){ location.reload(true); }, 3000);
                  }
                  if(response.res == 'error') {
                    swal(
                      '',
                      'AdamUniqueKey is already exist.',
                      'error'
                    );
                  }
                }
              });
            }else{
              swal(
                '',
                'AdamUniqueKey is required',
                'warning'
              );
            }
          }else{
            $.ajax({
              type: 'POST',
              url: '../saveAdamLineItemRow',
              data: { "FinalSelArrList": FinalSelArrList, 'FinalInputArray' : FinalInputArray, "_token": CSRF_TOKEN},
              success: function (response) {
                if(response.res == 'validate'){
                  swal(
                    '',
                    'Please enter AdamUniqueKey fields',
                    'warning'
                  );
                }
                else if(response.res == 'success') {
                  $("#emailDialog").modal("show");
                  swal(
                    '',
                    'Successfully saved',
                    'success'
                  );
                  setTimeout(function(){ location.reload(true); }, 3000);
                }
                else if(response.res == 'error') {
                  swal(
                    '',
                    'AdamUniqueKey is already exist.',
                    'error'
                  );
                }
              }
            });
          }
        });


      }
    }); 
  }); 
  // --------------end add single adamLine-------------------------------------

  // $("input[name='facebook']").change(function(){
  //   var radioValue = $("input[name='facebook']:checked").val();
    
  //   //alert("Your are a" + radioValue);

  //   AdamsLineItem_ID = $(this).attr('data-AdamsLineItem_ID');

  //    alert(AdamsLineItem_ID);

  //   var CSRF_TOKEN=$("#token").val();
  //   $.ajaxSetup({
  //     headers: {
  //       'X-CSRF-Token': $('meta[name=_token]').attr('content')
  //     }
  //   });
  //   $.ajax({
  //     type: 'POST',
  //     url: '../saveLineItemAdsetMapValue',
  //     data: { "_token": CSRF_TOKEN, 'AdamsLineItem_ID':AdamsLineItem_ID, 'radioValue':radioValue },
  //     success: function (response ) {
  //       if(response.res =='success')
  //       {
  //         swal(
  //           'Success',
  //           'Mapping done succesfully',
  //           'success'
  //         );
  //       }
  //       setTimeout(function(){ location.reload(true); }, 3000);
  //     }
  //   }); 

  // });
});
// -----------------------------------------------------
var _isEdit = false;
var _currRowId;
var _totalDropDowns = 5;
var _totalInputs = 37;

function editAdamLine(adamRowId, adamLine)
{ 
  _currRowId = adamRowId;
  if (!_isEdit)
  {
    $("#mainBtns_"+_currRowId).hide();
    $("#secBtns_"+_currRowId).show();

    enableElements(_currRowId);
  
    _isEdit = true;
  }
  else
  {
    showAlert(_currRowId);
  }

  var separatedArray = new Array();
  var separatedArray = adamLine.split(',');

  $('#dd1_'+_currRowId+' option').each(function(){
    for(var i=0; i<separatedArray.length; i++) {
      var option = $(this).val();
      if(separatedArray[i]==option){
        $(this).attr('selected', true);
      }
    }  
  });
  
}
// -----------------------------------------------------
$(".confirmCancel").click(function(){
  _currRowId = $(this).attr('data-currentRow');
  showAlert(_currRowId);
});
// -----------------------------------------------------
$("#btnModalCancel").click(function(){
  _currRowId = $('#btnModalCancel').val();
  cancelEdit(_currRowId);
});
// -----------------------------------------------------
$("#btnModalCont").click(function()
{
  console.log('modal cont click....');
  hideAlert();
});
// -----------------------------------------------------
function enableElements(_currRowId)
{
  for (var i=1; i<=_totalDropDowns; i++)
  {
    $("#dd"+i+"_"+_currRowId).prop("disabled", false);
    $("#dd"+i+"_"+_currRowId).show();
    $("#ditLabel"+i+"_"+_currRowId).hide();
  }
  /* end for dropdowns */
  /* for input fields */
  for (var j=1; j<=_totalInputs; j++)
  {
    $("#input"+j+"_"+_currRowId).prop("disabled", false);
  }

  $("#inputNonEdit_"+_currRowId).prop("disabled", false);
  /* end for input fields */

  $(".enableDisabled").prop("disabled", false);
  //$("input[name='facebook_"+_currRowId+"']:checked").val();
  $("input[name='facebook_"+_currRowId+"']").prop("disabled", false);
}
// -----------------------------------------------------
function disableElements(_currRowId)
{
  /* for dropdowns */

  for (var i=1; i<=_totalDropDowns; i++)
  {
    $("#dd"+i+"_"+_currRowId).prop("disabled", true);
    $("#dd"+i+"_"+_currRowId).hide();
    $("#ditLabel"+i+"_"+_currRowId).show();
    $("#ditLabel"+i+"_"+_currRowId).prop("disabled", true);
  }
  /* for dropdowns */

  /* for input fields */
  for (var j=1; j<=_totalInputs; j++)
  {
    $("#input"+j+"_"+_currRowId).prop("disabled", true);
  }

  $("#inputNonEdit_"+_currRowId).prop("disabled", true);
  /* for input fields */

  $(".disabled").prop("disabled", true);
  $(".enableDisabled").hide();
  $(".ditLabel").show();
  $(".editBtn").show();
  $(".secBtns").hide();
}
// -----------------------------------------------------
function showAlert(_currRowId)
{
  $('#myModal').modal('show');
  $('#btnModalCancel').val(_currRowId);
  //$(".modal-body").val(_currRowId);
}
// -----------------------------------------------------
function hideAlert()
{
  $('#myModal').modal('hide');
}
// -----------------------------------------------------
function cancelEdit(_currRowId)
{
  _isEdit = false;

  $("#mainBtns_"+_currRowId).show();
  $("#secBtns_"+_currRowId).hide();
  disableElements(_currRowId);
  _currRowId = -10;
}
// -----------------------------------------------------
function saveAdamLine(adamRowId)
{ 
  //alert(adamRowId);
  // ------------------------------------------------------------------------------------------------
  //this first specialClientEditAdamKey variable is Used, to edit AdamUniuque key first time, on edit. 
  var specialClientEditAdamKey = $('#input37_'+adamRowId).val();//alert("specialClientEditAdamKey: "+specialClientEditAdamKey);

  //this second specialClientEditAdamKey variable is Used, on edit,if AdamUniuque key has already set, No Edit of AdamUniuque key is Allowed
  var specialClientEditAdamKeyNoEdit = $('#inputNonEdit_'+adamRowId).val();//alert("specialClientEditAdamKeyNoEdit: "+specialClientEditAdamKeyNoEdit);
  // ------------------------------------------------------------------------------------------------

  var DataInputTypeName     = $('#dd1_'+adamRowId).val();
  var Type                  = $('#dd2_'+adamRowId).val();
  var Website_Publisher     = $('#input1_'+adamRowId).val();
  var VendorEntityName      = $('#dd3_'+adamRowId).val();
  var FromDate              = $('#input2_'+adamRowId).val();
  var ToDate                = $('#input3_'+adamRowId).val();
  var Platform              = $('#dd4_'+adamRowId).val();
  var Paid_OwnedMedia       = $('#dd5_'+adamRowId).val();
  var Section               = $('#input4_'+adamRowId).val();
  var AdUnit                = $('#input5_'+adamRowId).val();
  var AdSize                = $('#input6_'+adamRowId).val();
  var Days                  = $('#input7_'+adamRowId).val();
  var GeoTargeting          = $('#input8_'+adamRowId).val();
  var BuyType               = $('#input9_'+adamRowId).val();
  var BuySubType            = $('#input10_'+adamRowId).val();
  var Deliverables          = $('#input11_'+adamRowId).val();
  var Quantity              = $('#input12_'+adamRowId).val();
  var GrossRate             = $('#input13_'+adamRowId).val();
  var GrossCost             = $('#input14_'+adamRowId).val();
  var NetRate               = $('#input15_'+adamRowId).val();
  var NetCost               = $('#input16_'+adamRowId).val();
  var Brand                 = $('#input17_'+adamRowId).val();
  var Variant               = $('#input18_'+adamRowId).val();
  var ClientName            = $('#input19_'+adamRowId).val();
  var CampaignName          = $('#input20_'+adamRowId).val();
  var Vendor_PublisherEmail = $('#input21_'+adamRowId).val();
  var SubBrand              = $('#input22_'+adamRowId).val();
  var Objective             = $('#input23_'+adamRowId).val();
  var SubObjective          = $('#input24_'+adamRowId).val();
  var FormatType            = $('#input25_'+adamRowId).val();
  var Impressions           = $('#input26_'+adamRowId).val();
  var Clicks                = $('#input27_'+adamRowId).val();
  var VideoViews            = $('#input28_'+adamRowId).val();
  var Engagement            = $('#input29_'+adamRowId).val();
  var Visits                = $('#input30_'+adamRowId).val();
  var Leads                 = $('#input31_'+adamRowId).val();
  var Reach                 = $('#input32_'+adamRowId).val();
  var Installs              = $('#input33_'+adamRowId).val();
  var SMS                   = $('#input34_'+adamRowId).val();  
  var Mailer                = $('#input35_'+adamRowId).val();
  var Spots                 = $('#input36_'+adamRowId).val();

  var radioValue            = $('input[name="facebook_'+adamRowId+'"]:checked').val();

  //alreday saved key is used to check uniqueness on edit-------------
  var oldAdamUniqueKey      = $('#adamUniqueKey_'+adamRowId).val();

  var adamUniqueKey         = Brand+"_"+SubBrand+"_"+CampaignName+"_"+Objective+"_"+SubObjective+"_"+FromDate+"_"+ToDate+"_"+Website_Publisher+"_"+GeoTargeting+"_"+BuyType;

  var CSRF_TOKEN            = $("#token").val();

  // -----------------------special edit key Present -----------------------------------------

  if(specialClientEditAdamKey || specialClientEditAdamKeyNoEdit)
  {
    if(specialClientEditAdamKey){
      specialClientEditAdamKey=specialClientEditAdamKey;
    }
    else{
      specialClientEditAdamKey=specialClientEditAdamKeyNoEdit; 
    }

    var CSRF_TOKEN=$("#token").val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-Token': $('meta[name=_token]').attr('content')
      }
    });
    $.ajax({
      type: 'POST',
      url: '../CheckUniqueAdamLineItemNameOnEdit',
      data: { "_token": CSRF_TOKEN, 'adamRowId':adamRowId, 'specialClientEditAdamKey':specialClientEditAdamKey },
      success: function (response ) {
        if(response.res =='LineItemfoundNewRow')
        {
          swal(
            'Cancelled',
            'Please Add Unique adamUniqueKey',
            'error'
          );
        }
        // -----------special key ins Unique-----------------------
        else if(response.res =='LineItemNotfound' || 'LineItemfoundForSameRow')
        {        
          if(DataInputTypeName){
            $.ajaxSetup({
              headers: {
                  'X-CSRF-Token': $('meta[name=_token]').attr('content')
              }
            });
            $.ajax({
              type: 'POST',
              url: '../saveAdamLineItem',
              data: { '_token': CSRF_TOKEN, 
                'adamRowId':adamRowId , 
                'specialClientEditAdamKey':specialClientEditAdamKey,
                'adamUniqueKey':adamUniqueKey ,
                'DataInputTypeName':DataInputTypeName,
                'Type':Type,
                'Website_Publisher':Website_Publisher,
                'VendorEntityName':VendorEntityName,
                'Platform':Platform,
                'Paid_OwnedMedia':Paid_OwnedMedia,
                'FromDate':FromDate,
                'ToDate':ToDate,
                'Section':Section,
                'AdUnit':AdUnit,
                'AdSize':AdSize,
                'Days':Days,
                'GeoTargeting':GeoTargeting,
                'BuyType':BuyType,
                'BuySubType':BuySubType,
                'Deliverables':Deliverables,
                'Quantity':Quantity,
                'GrossRate':GrossRate,
                'GrossCost':GrossCost,
                'NetRate':NetRate,
                'NetCost':NetCost,
                'Brand':Brand,
                'Variant':Variant,
                'ClientName':ClientName,
                'CampaignName':CampaignName,
                'Vendor_PublisherEmail':Vendor_PublisherEmail,
                'SubBrand':SubBrand,
                'Objective':Objective,
                'SubObjective':SubObjective,
                'FormatType':FormatType,
                'Impressions':Impressions,
                'Clicks':Clicks,
                'VideoViews':VideoViews,
                'Engagement':Engagement,
                'Visits':Visits,
                'Leads':Leads,
                'Reach':Reach,
                'SMS':SMS,
                'Installs':Installs,
                'Mailer':Mailer,
                'Spots':Spots,

                'radioValue':radioValue,

                'oldAdamUniqueKey':oldAdamUniqueKey
              },
              success: function (response) {
                if(response.res =='success')
                {
                  swal(
                    '',
                    'Successfully Updated.',
                    'success'
                  );
                  setTimeout(function(){ location.reload(true); }, 3000);
                }
                if(response.res =='error')
                {
                  swal(
                    '',
                    'AdamUniqueKey is already exist.',
                    'error'
                  );
                }
              }
            });
          }
          else{
            swal(
              '',
              'Please select DIT',
              'error'
            );
          }
        }
      }
    });
  }
  // ----------------------- end special edit key Present-----------------------------------------
  else
  {
    if(DataInputTypeName){
      $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
      });
      $.ajax({
        type: 'POST',
        url: '../saveAdamLineItem',
        data: { '_token': CSRF_TOKEN, 'adamRowId':adamRowId , 
          'specialClientEditAdamKey':specialClientEditAdamKey,
          'adamUniqueKey':adamUniqueKey,
          'DataInputTypeName':DataInputTypeName,
          'Type':Type,
          'Website_Publisher':Website_Publisher,
          'VendorEntityName':VendorEntityName,
          'Platform':Platform,
          'Paid_OwnedMedia':Paid_OwnedMedia,
          'FromDate':FromDate,
          'ToDate':ToDate,
          'Section':Section,
          'AdUnit':AdUnit,
          'AdSize':AdSize,
          'Days':Days,
          'GeoTargeting':GeoTargeting,
          'BuyType':BuyType,
          'BuySubType':BuySubType,
          'Deliverables':Deliverables,
          'Quantity':Quantity,
          'GrossRate':GrossRate,
          'GrossCost':GrossCost,
          'NetRate':NetRate,
          'NetCost':NetCost,
          'Brand':Brand,
          'Variant':Variant,
          'ClientName':ClientName,
          'CampaignName':CampaignName,
          'Vendor_PublisherEmail':Vendor_PublisherEmail,
          'SubBrand':SubBrand,
          'Objective':Objective,
          'SubObjective':SubObjective,
          'FormatType':FormatType,
          'Impressions':Impressions,
          'Clicks':Clicks,
          'VideoViews':VideoViews,
          'Engagement':Engagement,
          'Visits':Visits,
          'Leads':Leads,
          'Reach':Reach,
          'SMS':SMS,
          'Installs':Installs,
          'Mailer':Mailer,
          'Spots':Spots,

          'radioValue':radioValue,

          'oldAdamUniqueKey':oldAdamUniqueKey
        },
        success: function (response) {
          if(response.res == 'validate'){
            swal(
              '',
              'Please enter AdamUniqueKey fields',
              'warning'
            );
          }
          else if(response.res =='success')
          {
            swal(
              '',
              'Successfully Updated',
              'success'
            );
            setTimeout(function(){ location.reload(true); }, 3000);
          }
          else if(response.res =='error')
          {
            swal(
              '',
              'AdamUniqueKey is already exist.',
              'error'
            );
          }
        }
      });
    }
    else{
      swal(
        '',
        'Please select DIT',
        'error'
      );
    }
  }  
  // -----------------------------------------------------------------------
  // ##############################################################################################
  // var adamUniqueKey         = $('#adamUniqueKey_'+adamRowId).val();  //alert("adamUniqueKey: "+adamUniqueKey);  
  // var DataInputTypeName     = $('#dd1_'+adamRowId).val();
  // var Type                  = $('#dd2_'+adamRowId).val();
  // //var Website_Publisher     = $('#input2_'+adamRowId).val();
  // var VendorEntityName      = $('#dd3_'+adamRowId).val();
  // // var FromDate              = $('#input4_'+adamRowId).val();
  // // var ToDate                = $('#input5_'+adamRowId).val();
  // var Platform              = $('#dd4_'+adamRowId).val();
  // var Paid_OwnedMedia       = $('#dd5_'+adamRowId).val();
  // // ========================================================================
  // var Section               = $('#input5_'+adamRowId).val();
  // var AdUnit                = $('#input6_'+adamRowId).val();
  // var AdSize                = $('#input7_'+adamRowId).val();
  // var Days                  = $('#input8_'+adamRowId).val();
  // // var GeoTargeting          = $('#input12_'+adamRowId).val();
  // // var BuyType               = $('#input13_'+adamRowId).val();
  // var BuySubType            = $('#input9_'+adamRowId).val();
  // var Deliverables          = $('#input10_'+adamRowId).val();
  // var Quantity              = $('#input11_'+adamRowId).val();
  // var GrossRate             = $('#input12_'+adamRowId).val();
  // var GrossCost             = $('#input13_'+adamRowId).val();
  // var NetRate               = $('#input14_'+adamRowId).val();
  // var NetCost               = $('#input15_'+adamRowId).val();
  // //var Brand                 = $('#input21_'+adamRowId).val();
  // var Variant               = $('#input16_'+adamRowId).val();
  // var ClientName            = $('#input17_'+adamRowId).val();
  // //var CampaignName          = $('#input24_'+adamRowId).val();
  // var Vendor_PublisherEmail = $('#input18_'+adamRowId).val();
  // var SubBrand              = $('#input19_'+adamRowId).val();
  // //var Objective             = $('#input27_'+adamRowId).val();
  // //var SubObjective          = $('#input28_'+adamRowId).val();
  // var FormatType            = $('#input20_'+adamRowId).val();
  // var Impressions           = $('#input21_'+adamRowId).val();
  // var Clicks                = $('#input22_'+adamRowId).val();
  // var VideoViews            = $('#input23_'+adamRowId).val();
  // var Engagement            = $('#input24_'+adamRowId).val();
  // var Visits                = $('#input25_'+adamRowId).val();
  // var Leads                 = $('#input26_'+adamRowId).val();
  // var Reach                 = $('#input27_'+adamRowId).val();
  // var Installs              = $('#input28_'+adamRowId).val();
  // var SMS                   = $('#input29_'+adamRowId).val();  
  // var Mailer                = $('#input30_'+adamRowId).val();
  // var Spots                 = $('#input31_'+adamRowId).val();

  // var CSRF_TOKEN            = $("#token").val();

  // if(DataInputTypeName){
  //   $.ajaxSetup({
  //     headers: {
  //         'X-CSRF-Token': $('meta[name=_token]').attr('content')
  //     }
  //   });
  //   $.ajax({
  //     type: 'POST',
  //     url: '../saveAdamLineItem',
  //     data: { '_token': CSRF_TOKEN, 'adamRowId':adamRowId , 
  //       'specialClientEditAdamKey':specialClientEditAdamKey,
  //       'adamUniqueKey':adamUniqueKey ,
        
  //       'DataInputTypeName':DataInputTypeName,
  //       'Type':Type,
  //       //'Website_Publisher':Website_Publisher,
  //       'VendorEntityName':VendorEntityName,
  //       'Platform':Platform,
  //       'Paid_OwnedMedia':Paid_OwnedMedia,
  //       //'FromDate':FromDate,
  //       //'ToDate':ToDate,
  //       'Section':Section,
  //       'AdUnit':AdUnit,
  //       'AdSize':AdSize,
  //       'Days':Days,
  //       //'GeoTargeting':GeoTargeting,
  //       //'BuyType':BuyType,
  //       'BuySubType':BuySubType,
  //       'Deliverables':Deliverables,
  //       'Quantity':Quantity,
  //       'GrossRate':GrossRate,
  //       'GrossCost':GrossCost,
  //       'NetRate':NetRate,
  //       'NetCost':NetCost,
  //       //'Brand':Brand,
  //       'Variant':Variant,
  //       'Variant':Variant,
  //       //'CampaignName':CampaignName,
  //       'Vendor_PublisherEmail':Vendor_PublisherEmail,
  //       'SubBrand':SubBrand,
  //       //'Objective':Objective,
  //       //'SubObjective':SubObjective,
  //       'FormatType':FormatType,
  //       'Impressions':Impressions,
  //       'Clicks':Clicks,
  //       'VideoViews':VideoViews,
  //       'Engagement':Engagement,
  //       'Visits':Visits,
  //       'Leads':Leads,
  //       'Reach':Reach,
  //       'SMS':SMS,
  //       'Installs':Installs,
  //       'Mailer':Mailer,
  //       'Spots':Spots
  //    },
  //     success: function (response ) {        
  //       //console.log(response);
  //       // if(response.res =='success')
  //       // {
  //       //   swal(
  //       //     '',
  //       //     'Successfully Updated',
  //       //     'success'
  //       //   );
  //       // }
  //       // setTimeout(function(){ location.reload(true); }, 3000);
  //     }
  //   });
  // }
  // else{
  //   alert("Please select DIT");
  // }
}  

// ----copy on clipboard--------------
function copyToClipboard(id) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(".copyadamUniqueKey_"+id).text()).select();
  document.execCommand("copy");
  swal({
    title: "Copied AdamUniqueKey",
    text: $temp.val(),
    timer: 1800,
    showConfirmButton: false
  });
  $temp.remove();
}
// ----end copy on clipboard-------------- 

// -------assign AdaMLineIts to map DCM extended Key-----------
$(document).on('click', '.mapAdamDCM', function() {
  AdamsLineItem_ID = $(this).attr('data-AdamsLineItem_ID');
  $('#AdamsLineItem_ID').val(AdamsLineItem_ID);
  
  adamUniqueKey = $(this).attr('data-adamUniqueKey');
  $('#adamUniqueKey').val(adamUniqueKey);

  var CSRF_TOKEN=$("#token").val();
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('meta[name=_token]').attr('content')
    }
  });
  $.ajax({
    type: 'POST',
    url: '../getAllDCMkey',
    data: { "_token": CSRF_TOKEN, 'AdamsLineItem_ID':AdamsLineItem_ID },
    success: function (data ) {
      getAllDCMkeyDataArray =  data.getAllDCMkeyData;
      //$adamUniqueKeyValue = data.adamUniqueKeyValue;

      $('.DCMKeyTable tbody').html('');

      for($i=0; $i<getAllDCMkeyDataArray.length; $i++){
        $('.DCMKeyTable tbody').append("<tr><td class='text-center' ><input type='hidden' value='"+getAllDCMkeyDataArray[$i].id+"'/><input type='hidden' value='"+adamUniqueKey+"'/><p class='setEllipsis' title='"+getAllDCMkeyDataArray[$i].DCMKey+"'>"+getAllDCMkeyDataArray[$i].DCMKey+"</p></td><td class='text-center' ><input type='text' class='form-control' value='"+getAllDCMkeyDataArray[$i].CampaignType+"' style='display: none;' /><label class='setEllipsis' title='"+getAllDCMkeyDataArray[$i].CampaignType+"'>"+getAllDCMkeyDataArray[$i].CampaignType+"</label></td><td class='text-center' ><input type='text' class='form-control' value='"+getAllDCMkeyDataArray[$i].AdSection+"' style='display: none;' /><label class='setEllipsis' title='"+getAllDCMkeyDataArray[$i].AdSection+"'>"+getAllDCMkeyDataArray[$i].AdSection+"</label></td><td class='text-center'><input type='text' class='form-control' value='"+getAllDCMkeyDataArray[$i].BannerSize+"' style='display: none;' /><label class='setEllipsis' title='"+getAllDCMkeyDataArray[$i].BannerSize+"'>"+getAllDCMkeyDataArray[$i].BannerSize+"</label></td><td class='text-center'  ><input type='text' class='form-control' value='"+getAllDCMkeyDataArray[$i].Device+"' style='display: none;' /><label class='setEllipsis' title='"+getAllDCMkeyDataArray[$i].Device+"'>"+getAllDCMkeyDataArray[$i].Device+"</label></td><td><button class='btn btn-primary btnEditDCMMulti'><i class='fa fa-pencil'></i></button><button class='btn btn-success btnSaveDCMMulti' style='display: none;'><i class='fa fa-check'></i></button><button class='btn btn-danger btnCancelDCMMulti' style='display: none;'><i class='fa fa-times'></i></button></td></tr>");
      }

    //delete button code : <button class='btn btn-danger btnDeleteDCMMulti'><i class='fa fa-trash'></i></button>
    
      $('#dcmMappingTable').DataTable({
        fixedHeader: true,
        dom: 'Bfrtip',
        buttons: ['excel'],
        paging: false,
        searching: false
      });
    }
  }); 
  // ------------------------------------------
});

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

/*$('.dateDiv.date').datepicker({
    todayBtn: "linked",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
});*/

function deleteAdamLine(adamRowId)
{
  var CSRF_TOKEN=$("#token").val();
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('meta[name=_token]').attr('content')
    }
  });
  $.ajax({
    type: 'POST',
    url: '../deleteAdamLine',
    data: { "_token": CSRF_TOKEN, 'adamRowId':adamRowId },
    success: function (response ) {
      //console.log(response);
      if(response.res =='success'){
        swal(
          'Success',
          'Successfully Deleted',
          'success'
        );
        setTimeout(function(){ location.reload(true); }, 3000);
      }else{
        swal(
          'Cancelled',
          'Data is present for AdamLineItem, Unable to Delete AdamLineItem.',
          'error'
        );
      }  
    }
  }); 
}

$(document).on('click', '.btnEditAlwaysOn', function() {
  $(this).closest('tr').find('td').find('input').show();
  $(this).closest('tr').find('td').find('label').hide();

  //hide buttons
  $(this).closest('tr').children('td').children('.btnEditAlwaysOn').hide();
  $(this).closest('tr').children('td').children('.btnDeleteAlwaysOn').hide();

  //show buttons
  $(this).closest('tr').children('td').children('.btnSaveAlwaysOn').show();
  $(this).closest('tr').children('td').children('.btnCancelAlwaysOn').show();
});

$(document).on('click', '.btnCancelAlwaysOn', function() {
  $(this).closest('tr').find('td').find('input').hide();
  $(this).closest('tr').find('td').find('label').show();

  //show buttons
  $(this).closest('tr').children('td').children('.btnEditAlwaysOn').show();
  $(this).closest('tr').children('td').children('.btnDeleteAlwaysOn').show();

  //hide buttons
  $(this).closest('tr').children('td').children('.btnSaveAlwaysOn').hide();
  $(this).closest('tr').children('td').children('.btnCancelAlwaysOn').hide();

});

$(document).on('click', '.btnSaveAlwaysOn', function() {

  var inputFieldValues = $(this).closest('tr').find('td').find('input');

  var FinalInputArray = new Array();

  $.each(inputFieldValues, function(index){
    FinalInputArray[index] = inputFieldValues[index].value;
  });

  //console.log("FinalInputArray: ", FinalInputArray)
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
    type: 'POST',
    url: '/updateAlwaysOnCampaign',
    data: {'FinalInputArray' : FinalInputArray, "_token": CSRF_TOKEN},
    success: function (response) {
      if(response.res == 'success') {
        swal(
          '',
          'Successfully updated',
          'success'
        );
        setTimeout(function(){location.reload(true); }, 2000);
      }
      else{
        swal(
          '',
          'Not successfully updated',
          'error'
        );
      }
    }
  });
});


//DCM Multiple Mapping-------------------------------------------
$(document).on('click', '.btnEditDCMMulti', function() {
  $(this).closest('tr').find('td').find('input').show();
  $(this).closest('tr').find('td').find('label').hide();

  //hide buttons
  $(this).closest('tr').children('td').children('.btnEditDCMMulti').hide();
  $(this).closest('tr').children('td').children('.btnDeleteDCMMulti').hide();

  //show buttons
  $(this).closest('tr').children('td').children('.btnSaveDCMMulti').show();
  $(this).closest('tr').children('td').children('.btnCancelDCMMulti').show();
});

$(document).on('click', '.btnCancelDCMMulti', function() {
  $(this).closest('tr').find('td').find('input').hide();
  $(this).closest('tr').find('td').find('label').show();

  //hide buttons
  $(this).closest('tr').children('td').children('.btnEditDCMMulti').show();
  $(this).closest('tr').children('td').children('.btnDeleteDCMMulti').show();

  //show buttons
  $(this).closest('tr').children('td').children('.btnSaveDCMMulti').hide();
  $(this).closest('tr').children('td').children('.btnCancelDCMMulti').hide();
});

$(document).on('click', '.btnSaveDCMMulti', function() {

  var inputFieldValues = $(this).closest('tr').find('td').find('input');

  var FinalInputArray = new Array();

  $.each(inputFieldValues, function(index){
    FinalInputArray[index] = inputFieldValues[index].value;
  });

  //console.log("FinalInputArray: ", FinalInputArray)
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
    type: 'POST',
    url: '/updateDCMMulti',
    data: {'FinalInputArray' : FinalInputArray, "_token": CSRF_TOKEN},
    success: function (response) {
      if(response.res == 'success') {
        swal(
          '',
          'Successfully updated',
          'success'
        );
        setTimeout(function(){location.reload(true); }, 2000);
      }else{
        swal(
          '',
          'Not successfully updated',
          'error'
        );
      }
    }
  });
});


$("form#fileUpload").submit(function(e) {
  e.preventDefault();    
  var formData = new FormData(this);
 
  var CSRF_TOKEN=$("#token").val();
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('meta[name=_token]').attr('content')
    }
  });
  $.ajax({
    type: 'post',
    url: "../getSheetListFromExcel", // point to server-side PHP script
    data: formData,
    processData: false,
    contentType: false,
    success: function(response) {
      console.log(response);
        
      $('.SheetListTable tbody').html('');

       for($i=0; $i<response.length; $i++){
       
         $('.SheetListTable tbody').append("<tr><td><input type='checkbox' name='sheetIds' value='"+$i+"'></td><td id ='"+$i+"'class='text-center'  title='"+response[$i]+"'>"+response[$i]+"</td></tr>");
       }
      //$('#SheetListTable').DataTable({
        //fixedHeader: true
      //});

    }
  });
});

$(document).on('click', '#SubmitSheet', function() {
  var fileUpload =$('#fileUpload')[0];
  var formData = new FormData(fileUpload);

  var AdamsLineItem_ID = $('#AdamsLineItem_IDF').val(); 
  var PublisherName =  $("#PublisherName").val();
  var sheetIds = [];
  
  $.each($("input[name='sheetIds']:checked"), function(){
      sheetIds.push($(this).val());
  });
  
  //alert(sheetIds);

  formData.append('Selected_sheet_ids', sheetIds);

  // alert("My favourite sports are: " + sheetIds.join(", "));
  var CSRF_TOKEN=$("#token").val();
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('meta[name=_token]').attr('content')
    }
  });
  $.ajax({
    type: 'post',
    url: "../UploadMultiExcelForManualPublisher", 
    data : formData,
    // data: {'SelectedSheetIds':sheetIds,'AdamsLineItem_ID':AdamsLineItem_ID,'PublisherName':PublisherName},
    processData: false,
    contentType: false,
    success: function(response) {

      console.log(response);

      if(response.status=='failure'){
        swal(
          '',
          'Please Select Atleast one WorkSheet',
          'error'
        );
      }
      else if(response.status=='wrongPublisher'){
        swal(
          '',
          'Select proper publisher.',
          'error'
        );
        setTimeout(function(){ location.reload(true); }, 2000);
      }else {
        swal(
          '',
          'Successfully saved',
          'success'
        );
        setTimeout(function(){ location.reload(true); }, 2000);
      } 
    }
  });
});