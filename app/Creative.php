<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creative extends Model
{
  public $timestamps = false;

    protected $primaryKey = 'id';

    public $fillable = ['CreativeName','Card_ID'];

	public $table = "dbo.Creative";
}
