<?php

namespace App\Http\Controllers;

// use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use DB;
use Validator;
Use Redirect;
use Carbon\Carbon;
use DateTime;

class Forgot_PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    //use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }

    public function forgotPassword(Request $request )
    {
        //dd($request->all());
        
        $data=$request->all();
        //dd($data['email']);
        $email=$request->email;


        $checkEmailExist=DB::table('users')
              ->where('email','=',$email)
              ->get();
        $checkEmailExist=json_decode($checkEmailExist);

           
        if($checkEmailExist)
        {
            
            
        
                \Mail::send('genratePassword', $data, function ($m) use($data) {
                    $m->from(getenv('MAIL_FROM_ADDRESS'), 'IADC');
                    $m->to($data['email'], 'Hello User')->subject('change password');
                });           
           
            

                session()->flash('status', 'Reset password link has been sent to your email id');
                return Redirect()->intended('/login');;  
        } 

        
        else
        {
            session()->flash('error_msg', 'Given Email Id is not registered with us !');
            return Redirect::back();
        }



    }

     public function resetPassword(Request $request)
    {
      
        //dd($request->all());
        $email=$request->email;
        $pswd=$request->password;
        $cnf_pswd=$request->password_confirmation; 

        if($pswd ===  $cnf_pswd )
        {
           
            $checkEmailExist=DB::table('users')
              ->where('email','=',$email)
              ->get();
            $checkEmailExist=json_decode($checkEmailExist);

           
            if($checkEmailExist)
            {
                
                $current_time = Carbon::now()->toDateTimeString(); 
                DB::table('users')
                   ->where('email', '=',$email)
                    ->update([   
                    'name'  =>$email,
                    'email' => $email,
                    'password'    => bcrypt($pswd),
                    'ModifiedOn'   => $current_time,
                ]);
                 
                session()->flash('status', 'Password Updated Succesfully ! LogIn Now !');
                return Redirect()->intended('/login');;    

            }
            else
            {
                session()->flash('error_msg', 'Given Email Id is not registered with us !');
                return Redirect::back();
            }  

        }    
        else
        {
            //dd("Not Same");
            session()->flash('error_msg', 'Password is not matched ');
            return Redirect::back();
        }

         
    }

    
}
