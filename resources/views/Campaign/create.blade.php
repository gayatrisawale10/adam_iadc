@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Create TrainingCenter</h2>
            </div>
          <!--  <div class="pull-right margin-top-20">
                <a class="btn btn-primary" href="{{ route('TrainingCenter.index') }}"> Back</a>
            </div> -->
        </div>
    
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(array('route' =>'TrainingCenter.store','method'=>'POST')) !!}
         @include('TrainingCenter.form')
    {!! Form::close() !!}
	</div>
	
@endsection
