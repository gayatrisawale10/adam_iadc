<?php $base_url= url('/'); ?>  
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="./image/favicon.ico" type="image/x-icon">
  <link rel="icon" href="../../../image/favicon.ico" type="image/x-icon">    

  <link rel="stylesheet" href="../../../css/jquery.ui.all.css">
  <link rel="shortcut icon" href="./image/favicon.ico" type="image/x-icon">
  <link rel="icon" href="./image/favicon.ico" type="image/x-icon">    
  <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
  <script src="../../../js/app.js"></script>

  <script data-require="jquery@*" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script data-require="datatables@*" data-semver="1.10.12" src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  
  <link data-require="datatables@*" data-semver="1.10.12" rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
  <link rel="stylesheet" href="http://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css" />
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <script src="../../../js/sweetalert.js"></script>
  <link href="../../../css/sweetalert.css" rel="stylesheet">

  <script src="../../../js/LineItemMappingValues.js" type="text/javascript"></script>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'IADC') }}</title>
  <script>
    window.Laravel = <?php echo json_encode([
      'csrfToken' => csrf_token(),
    ]); ?>
  </script>
  
</head>
<body> 
  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header"></div>
      <div class="collapse navbar-collapse" id="app-navbar-collapse">
      <!-- Left Side Of Navbar -->
        <ul class="nav navbar-nav">
          <img class="home_logo" src="{{asset('/image/logo_final.png') }}" width='60' height="60">
        </ul>
        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
          <!-- Authentication Links -->
          <li><a href="{{ url('/Campaign') }}">Campaign</a></li>
          <li><a href="{{ url('/Publisher') }}">Publisher</a></li> 
          <li><a href="{{ url('/Client') }}">Client</a></li> 
          @if(Auth::user()->UserRole_ID == 1)
          <li><a href="{{ url('/Person') }}">Person</a></li> 
          @endif
          <li>                                              
            <a  href="{{ url('/api') }}"">API </a>
          </li>
          <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()['name']}} <span class=""></span>
            </a>
            <li>
              <a href="{{ url('/logout') }}"
                 onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                 Logout
              </a>
              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
          </li> -->
          <li class="dropdown">
            <a class=" dropdown-toggle" type="button" data-toggle="dropdown"> 
                {{ Auth::user()['name'] }}
                <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a  href="{{ url('/logout') }}" 
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">Logout
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                        </li>
                        <!-- <li><a href="#">Reset Password</a></li> -->
                        <!-- <li><a href="#">JavaScript</a></li> -->
                    </ul>
          </li>

        </ul>
      </div>
    </div>
  </nav>
  <!-- --------------------------------------------------------------------- -->
  <div class="container"> 
    <div class="row">
      <div class="col-lg-6 pull-left">
        <h2>Edit LineItem Metrices: </h2>
      </div>      
       <div class="col-lg-12" align="right">
         <a class="btn btn-success" href="{{ url('/LineItem/'.$Lineitem->Campaign_ID)}}">Back </a>
      </div>
    </div>
  </div>
  <hr/>
  <!-- --------------------------------------------------------------------- -->
  <div class="container">
    <div class="row" >
      <div class="col-lg-12">
        <div class="btn btn-danger" id="errorid" style='display:none;'></div>     
        @if (Session::has('error_msg'))
          <div class="alert alert-success">
            <button class="close" data-close="alert"></button>
            <span>{{ Session::get('error_msg') }}</span>
          </div>
        @endif       
      </div>
    </div>            
    {!!Form::open(['url' => '/CampaignPublisherEditMetrices', 'id'=>"modal_add",'method' => 'post', 'class'=>'form-horizontal form-row-seperated'])!!}  
      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">          
      <input type="hidden" name="LineItem_ID" id="LineItem_IDM" value="{{$Lineitem->lineitemId}}">
      <input type="hidden" name="Campaign_ID" id="Campaign_IDM" value="{{$Lineitem->Campaign_ID}}">
              
      <div class="form-group row ">
        <div class=" col-md-1"><strong>Name</strong></div>
        <div class="col-md-12">
          <input type="text" placeholder="Line Item Name" class="form-control" name='LineItemName' value="{{$Lineitem->LineItemName}}"  id="LineItemName1" /> 
        </div>
      </div>
      <hr/>
      <div class="form-group row ">
        <div class=" col-md-1 "><strong>Genre</strong></div>
        <div class="col-md-5">
          <select class="form-control">
            <option id="genreDetails" name="Genre_ID"   value="{{$Genre->id}}" >{{$Genre->GenreName}}</option>
          </select> 
        </div>                
        <div class="col-md-2 "><strong>Publisher Name </strong></div>
        <div class="col-md-4">
          <select class="form-control">
            <option id="PublisherDetails" name="Publisher_ID"  value="{{$Publisher->id}}">{{$Publisher->PublisherName}}
            </option>
          </select>
        </div>
      </div>
      <hr/>                 
      <div class="form-group row ">
        <div class="col-md-2"></div>
        <div class="col-md-2" style="text-align:center;"><strong>Plan </strong></div>
        <div class="col-md-2"><strong>Data InputType</strong></div>
        <div class="col-md-2"> </div>
        <div class="col-md-2" style="text-align:center"><strong>Plan </strong></div>
        <div class="col-md-2"><strong>Data InputType</strong></div>
      </div>
      <hr/>                
      <div class="form-group row ">
        <div class="col-md-2"><strong>Impression:</strong></div>
        <div class="col-md-2">
          <input type="text" class="form-control" name="Impressions" id="Impression1" placeholder = " Impression" onkeypress="return isNumberKey(event)" value="{{@$sdit[0]->PlannedValue}}" />                          
        </div>             
        <div class="col-md-2"> 
          <select  id="PIDataInputTypeIDM" name="PIDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PIDataInputTypeIDM" value="" {{ old('PIDataInputTypeID', @$sdit[0]->DataInputType_ID) == "" ? 'selected' : '' }} >Select</option>
            <?php $i=1; ?>
              @foreach($DIT as $dit)                 
                @if($i==4)
                  <optgroup label="API">
                    <option id="DITImp" data-for="PIDataInputTypeIDM" value="{{$dit->id}}" {{ old('PIDataInputTypeID', @$sdit[0]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                    </option>     
                @else
                  <option id="DITImp" data-for="PIDataInputTypeIDM" value="{{$dit->id}}" {{ old('PIDataInputTypeID', @$sdit[0]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                  </option>
                @endif
            <?php $i++; ?>
              @endforeach
                  </optgroup>
          </select>  
        </div>
        <div class="col-md-1"></div>        
        <div class="col-md-1 "><strong>Clicks:</strong></div>
        <div class="col-md-2">
          <input type="text" class="form-control" name='Clicks'  id="Clicks1" placeholder = "Clicks" onkeypress="return isNumberKey(event)" value="{{@$sdit[1]->PlannedValue}}" />                          
        </div>              
        <div class="col-md-2">              
          <select  id="PCDataInputTypeIDM" name="PCDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PCDataInputTypeIDM" value="" {{ old('PCDataInputTypeID', @$sdit[1]->DataInputType_ID) == "" ? 'selected' : '' }} >Select</option>
            <?php $i=1; ?>
              @foreach($DIT as $dit)                 
                @if($i==4)
                  <optgroup label="API">
                    <option id="DITImp" data-for="PCDataInputTypeIDM" value="{{$dit->id}}" {{ old('PCDataInputTypeID', @$sdit[1]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                    </option>     
                @else
                  <option id="DITImp" data-for="PCDataInputTypeIDM" value="{{$dit->id}}" {{ old('PCDataInputTypeID', @$sdit[1]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                  </option>
                @endif
            <?php $i++; ?>
              @endforeach
                  </optgroup>
          </select>                    
        </div>
      </div>
      <div class="form-group row ">
        <div class="col-md-2"><strong>Visits:</strong></div>
        <div class="col-md-2">
          <input type="text" class="form-control" name='Visits'  id="Visits1" placeholder="Visits" onkeypress="return isNumberKey(event)" value="{{@$sdit[2]->PlannedValue}}" />                          
        </div>             
        <div class="col-md-2">               
          <select  id="PVsDataInputTypeIDM" name="PVsDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PVsDataInputTypeIDM" value="" {{ old('PVsDataInputTypeID', @$sdit[2]->DataInputType_ID) == "" ? 'selected' : '' }} >Select</option>
            <?php $i=1; ?>
              @foreach($DIT as $dit)                 
                @if($i==4)
                  <optgroup label="API">
                    <option id="DITImp" data-for="PVsDataInputTypeIDM" value="{{$dit->id}}" {{ old('PVsDataInputTypeID', @$sdit[2]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                    </option>     
                @else
                    <option id="DITImp" data-for="PVsDataInputTypeIDM" value="{{$dit->id}}" {{ old('PVsDataInputTypeID', @$sdit[2]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                    </option>
                @endif
            <?php $i++; ?>
              @endforeach
                  </optgroup>
          </select>                                  
        </div>
        <div class="col-md-1"></div>       
        <div class="col-md-1"><strong>Views:</strong></div>
        <div class="col-md-2">
          <input type="text" placeholder="Views" class="form-control" name='Views'  id="Views1" onkeypress="return isNumberKey(event)" value="{{@$sdit[3]->PlannedValue}}" />                          
        </div>
        <div class="col-md-2">
          <select  id="PVwDataInputTypeIDM" name="PVwDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PVwDataInputTypeIDM" value="" {{ old('PVwDataInputTypeID', @$sdit[3]->DataInputType_ID) == "" ? 'selected' : '' }} >Select</option>
            <?php $i=1; ?>
            @foreach($DIT as $dit)                 
              @if($i==4)
                <optgroup label="API">
                  <option id="DITImp" data-for="PVwDataInputTypeIDM" value="{{$dit->id}}" {{ old('PVwDataInputTypeID', @$sdit[3]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                  </option>     
              @else
                  <option id="DITImp" data-for="PVwDataInputTypeIDM" value="{{$dit->id}}" {{ old('PVwDataInputTypeID', @$sdit[3]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                  </option>
              @endif
            <?php $i++; ?>
              @endforeach
                </optgroup>
          </select> 
        </div>
      </div>           
      <div class="form-group row ">             
        <div class="col-md-2 "><strong>Leads:</strong></div>
        <div class="col-md-2">
          <input type="text" placeholder="Leads" class="form-control" name='Leads'   id="Leads1" onkeypress="return isNumberKey(event)" value="{{@$sdit[4]->PlannedValue}}" />  
        </div>
        <div class="col-md-2">
          <select  id="PLDataInputTypeIDM" name="PLDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PLDataInputTypeIDM" value="" {{ old('PLDataInputTypeID', @$sdit[4]->DataInputType_ID) == "" ? 'selected' : '' }} >Select</option>
            
            <?php $i=1; ?>
              @foreach($DIT as $dit)                 
                @if($i==4)
                  <optgroup label="API">
                    <option id="DITImp" data-for="PLDataInputTypeIDM" value="{{$dit->id}}" {{ old('PLDataInputTypeID', @$sdit[4]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                    </option>     
                @else
                    <option id="DITImp" data-for="PLDataInputTypeIDM" value="{{$dit->id}}" {{ old('PEDataInputTypeID', @$sdit[4]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                    </option>
                @endif
            <?php $i++; ?>
              @endforeach
                  </optgroup>
          </select>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-1 "><strong>Engagement:</strong></div>
        <div class="col-md-2">
          <input type="text" placeholder="Engagement" class="form-control" name='Engagement'  id="Engagement1" onkeypress="return isNumberKey(event)" value="{{@$sdit[5]->PlannedValue}}" />                                                  
        </div>              
        <div class="col-md-2">              
          <select  id="PEDataInputTypeIDM" name="PEDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PEDataInputTypeIDM" value="" {{ old('PEDataInputTypeID', @$sdit[5]->DataInputType_ID) == "" ? 'selected' : '' }} >Select</option>
            <?php $i=1; ?>
              @foreach($DIT as $dit)                 
                @if($i==4)
                  <optgroup label="API">
                    <option id="DITImp" data-for="PEDataInputTypeIDM" value="{{$dit->id}}" {{ old('PEDataInputTypeID', @$sdit[5]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                    </option>     
                @else
                    <option id="DITImp" data-for="PEDataInputTypeIDM" value="{{$dit->id}}" {{ old('PEDataInputTypeID', @$sdit[5]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                    </option>
                @endif
            <?php $i++; ?>
              @endforeach
                  </optgroup>
          </select> 
        </div>
      </div>       
      <div class="form-group row ">
        <div class="col-md-2 "><strong>Net Rate:</strong></div>
        <div class="col-md-2">
          <input type="text" placeholder="Net Rate" class="form-control" name='NetRate' id="NetRate1" onkeypress="return isNumberKey(event)" value="{{@$sdit[6]->PlannedValue}}" />                          
        </div>
        <div class="col-md-2">
          <select  id="PNRDataInputTypeIDM" name="PNRDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PNRDataInputTypeIDM" value="" {{ old('PNRDataInputTypeID', @$sdit[6]->DataInputType_ID) == "" ? 'selected' : '' }} >Select
            </option>
            <?php $i=1; ?>
              @foreach($DIT as $dit)                 
                @if($i==4)
                  <optgroup label="API">
                    <option id="DITImp" data-for="PNRDataInputTypeIDM" value="{{$dit->id}}" {{ old('PNRDataInputTypeID', @$sdit[6]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                    </option>     
                @else
                  <option id="DITImp" data-for="PNRDataInputTypeIDM" value="{{$dit->id}}" {{ old('PNRDataInputTypeID', @$sdit[6]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                      </option>
                @endif
            <?php $i++; ?>
              @endforeach
                  </optgroup>
          </select>                    
        </div>           
        <div class="col-md-1"></div>
        <div class="col-md-1 "><strong>Net Cost:</strong></div>
        <div class="col-md-2">
          <input type="text" placeholder="Net Cost" class="form-control" name='NetCost'  id="NetCost1" onkeypress="return isNumberKey(event)" value="{{@$sdit[7]->PlannedValue}}" />                          
        </div>
        <div class="col-md-2">        
          <select  id="PNCDataInputTypeIDM" name="PNCDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PNCDataInputTypeIDM" value="" {{ old('PNCDataInputTypeID', @$sdit[7]->DataInputType_ID) == "" ? 'selected' : '' }} >Select
            </option>
            <?php $i=1; ?>
              @foreach($DIT as $dit)                 
                @if($i==4)
                  <optgroup label="API">
                    <option id="DITImp" data-for="PNCDataInputTypeIDM" value="{{$dit->id}}" {{ old('PNCDataInputTypeID', @$sdit[7]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                    </option>     
                @else
                    <option id="DITImp" data-for="PNCDataInputTypeIDM" value="{{$dit->id}}" {{ old('PNCDataInputTypeID', @$sdit[7]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                    </option>
                @endif
              <?php $i++; ?>
              @endforeach
                  </optgroup>
          </select> 
        </div>
      </div> 
      <div class="form-group row ">
        <div class="col-md-2"><strong>Reach:</strong></div>
        <div class="col-md-2">
          <input type="text" placeholder="Reach" class="form-control" name='Reach'  id="Reach1" onkeypress="return isNumberKey(event)" value="{{@$sdit[8]->PlannedValue}}" />                          
        </div>
        <div class="col-md-2">
          <select  id="PRDataInputTypeIDM" name="PRDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PRDataInputTypeIDM" value="" {{ old('PRDataInputTypeID', @$sdit[8]->DataInputType_ID) == "" ? 'selected' : '' }} >Select</option>
              <?php $i=1; ?>
              @foreach($DIT as $dit)                 
                @if($i==4)
                  <optgroup label="API">
                    <option id="DITImp" data-for="PRDataInputTypeIDM" value="{{$dit->id}}" {{ old('PLDataInputTypeID', @$sdit[8]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                    </option>     
                @else
                    <option id="DITImp" data-for="PRDataInputTypeIDM" value="{{$dit->id}}" {{ old('PRDataInputTypeID', @$sdit[8]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                    </option>
                @endif
              <?php $i++; ?>
              @endforeach
                  </optgroup>
          </select>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-1 "><strong>Installs:</strong></div>
        <div class="col-md-2">
          <input type="text" placeholder="Installs" class="form-control" name='Installs'  id="Installs1" onkeypress="return isNumberKey(event)" value="{{@$sdit[9]->PlannedValue}}" />                          
        </div>
        <div class="col-md-2">
          <select  id="PInDataInputTypeIDM" name="PInDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PInDataInputTypeIDM" value="" {{ old('PInDataInputTypeID', @$sdit[9]->DataInputType_ID) == "" ? 'selected' : '' }} >Select</option>
              <?php $i=1; ?>
                @foreach($DIT as $dit)                 
                  @if($i==4)
                    <optgroup label="API">
                      <option id="DITImp" data-for="PInDataInputTypeIDM" value="{{$dit->id}}" {{ old('PInDataInputTypeID', @$sdit[9]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                      </option>     
                  @else
                     <option id="DITImp" data-for="PInDataInputTypeIDM" value="{{$dit->id}}" {{ old('PInDataInputTypeID', @$sdit[9]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                     </option>
                  @endif
              <?php $i++; ?>
                @endforeach
                    </optgroup>
          </select> 
        </div>
      </div>          
      <div class="form-group row ">
        <div class="col-md-2 "><strong>Spots:</strong></div>
        <div class="col-md-2">
          <input type="text" placeholder="Spots" class="form-control" name='Spots'   id="Spots1" onkeypress="return isNumberKey(event)" value="{{@$sdit[10]->PlannedValue}}" />                          
        </div>
        <div class="col-md-2">
          <select  id="PSpDataInputTypeIDM" name="PSpDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PSpDataInputTypeIDM" value="" {{ old('PSpDataInputTypeID', @$sdit[10]->DataInputType_ID) == "" ? 'selected' : '' }} >Select</option>
              <?php $i=1; ?>
              @foreach($DIT as $dit)                 
                @if($i==4)
                  <optgroup label="API">
                    <option id="DITImp" data-for="PSpDataInputTypeIDM" value="{{$dit->id}}" {{ old('PSpDataInputTypeID', @$sdit[10]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                    </option>     
                @else
                    <option id="DITImp" data-for="PSpDataInputTypeIDM" value="{{$dit->id}}" {{ old('PSpDataInputTypeID', @$sdit[10]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                    </option>
                @endif
              <?php $i++; ?>
              @endforeach
                  </optgroup>
          </select> 
        </div>           
        <div class="col-md-1"></div>
        <div class="col-md-1 "><strong>SMS:</strong></div>
        <div class="col-md-2">
          <input type="text" placeholder="SMS" class="form-control" name='SMS' id="SMS1" onkeypress="return isNumberKey(event)" value="{{@$sdit[11]->PlannedValue}}"  />                          
        </div>
        <div class="col-md-2">
          <select  id="PSmDataInputTypeIDM" name="PSmDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PSmDataInputTypeIDM" value="" {{ old('PSmDataInputTypeID', @$sdit[11]->DataInputType_ID) == "" ? 'selected' : '' }} >Select</option>
              <?php $i=1; ?>
              @foreach($DIT as $dit)                 
                @if($i==4)
                  <optgroup label="API">
                    <option id="DITImp" data-for="PSmDataInputTypeIDM" value="{{$dit->id}}" {{ old('PSmDataInputTypeID', @$sdit[11]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                    </option>     
                @else
                    <option id="DITImp" data-for="PSmDataInputTypeIDM" value="{{$dit->id}}" {{ old('PSmDataInputTypeID', @$sdit[11]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                    </option>
                @endif
              <?php $i++; ?>
              @endforeach
                   </optgroup>
          </select> 
        </div>
      </div>           
      <div class="form-group row ">
        <div class="col-md-2"><strong>Mailers:</strong></div>
        <div class="col-md-2">
          <input type="text" placeholder="Mailers" class="form-control" name='Mailers' id="Mailers1" onkeypress="return isNumberKey(event)" value="{{@$sdit[12]->PlannedValue}}" />                            
        </div>
        <div class="col-md-2">
          <select  id="PMDataInputTypeIDM" name="PMDataInputTypeID" class="form-control " > 
            <option id="DITImp" data-for="PMDataInputTypeIDM" value="" ="" {{ old('PMDataInputTypeID', @$sdit[12]->DataInputType_ID) == "" ? 'selected' : '' }} >Select</option>
              <?php $i=1; ?>
              @foreach($DIT as $dit)                 
                @if($i==4)
                  <optgroup label="API">
                    <option id="DITImp" data-for="PMDataInputTypeIDM" value="{{$dit->id}}" {{ old('PMDataInputTypeID', @$sdit[12]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}                            
                    </option>     
                @else
                    <option id="DITImp" data-for="PMDataInputTypeIDM" value="{{$dit->id}}" {{ old('PMDataInputTypeID', @$sdit[12]->DataInputType_ID) == $dit->id ? 'selected' : '' }} >{{$dit->DataInputTypeName}}
                    </option>
                @endif
              <?php $i++; ?>
              @endforeach
                  </optgroup>
          </select>
        </div>
      </div>
      <hr/>
      <div class="form-group row text-center">            
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    {!! Form::close() !!}           
  </div>            
  <!-- --------------------------------------------------------------------- -->
</body> 
</html>  