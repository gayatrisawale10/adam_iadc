<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineItem extends Model
{
	public $timestamps = false;

	protected $primaryKey = 'id';

    // public $fillable = ['CampaignPublisher_ID','AdSection_ID','DealType_ID','DeliveryMode_ID','PlannedImpressions','PlannedClicks','PlannedUniqueReach','UnitRate','ModifiedBy','IsActive','ModifiedOn','CreatedOn'];



     public $fillable = [

						'CampaignPublisher_ID',
						'LineItemID',
						'Phase_ID',
						'ObjectiveValue_ID',
						'Medium_ID',
						'Targeting_ID',
						'TargetingValue_ID',
						'Format_ID',
						'DealType_ID',
						'DeliveryMode_ID',
						'AgeRangeFrom',
						'AgeRangeTo',
						'FormatValue',
						'GoogleXCoord',
						'GoogleYCoord',
						'GoogleRadius',
						'IsLocationBased'
						,'ModifiedBy',
						'IsActive',
						'ModifiedOn',
						'CreatedOn'];

	public $table = "LineItem";
}
