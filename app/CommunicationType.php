<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommunicationType extends Model
{
     protected $primaryKey = 'CommunicationType_ID';

     public $fillable = ['CommunicationTypeName','CommunicationTypeApplicability'];

	 public $table = "communication_types";
}
