<!DOCTYPE html>
<html lang="en">
<head>
  <!-- <script>
        window.Laravel = <?php //echo json_encode([
            //'csrfToken' => csrf_token(),
       //]); ?>
  </script> -->
   <meta name="csrf-token" content=<?php echo "'".csrf_token()."'"; ?> >
  <meta charset="utf-8">
  <title>jquery-addresspicker demo (a jQuery UI widget)</title>
  <link rel="stylesheet" href="./css/jquery.ui.all.css">
  <link rel="stylesheet" href="./css/demo.css">
  <link rel="stylesheet" href="./css/bootstrap.css">
  <script src="http://maps.google.com/maps/api/js?sensor=false"></script>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
  <script src="./js/jquery.ui.addresspicker.js"></script>
 
  <script>
  $(function() {
    var addresspicker = $( "#addresspicker" ).addresspicker({
      componentsFilter: 'country:IN'
    });
    var addresspickerMap = $( "#addresspicker_map" ).addresspicker({
      regionBias: "in",
      language: "in",
      updateCallback: showCallback,
      mapOptions: {
        zoom: 4,
        center: new google.maps.LatLng(20.593684, 78.96288000000004),
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      },
      elements: {
        map:      "#map",
        lat:      "#lat",
        lng:      "#lng",
        street_number: '#street_number',
        route: '#route',
        locality: '#locality',
        sublocality: '#sublocality',
        administrative_area_level_3: '#administrative_area_level_3',
        administrative_area_level_2: '#administrative_area_level_2',
        administrative_area_level_1: '#administrative_area_level_1',
        country:  '#country',
        postal_code: '#postal_code',
        type:    '#type'
      }
    });

    var gmarker = addresspickerMap.addresspicker( "marker");
    gmarker.setVisible(true);
    addresspickerMap.addresspicker( "updatePosition");

    $('#reverseGeocode').change(function(){
      $("#addresspicker_map").addresspicker("option", "reverseGeocode", ($(this).val() === 'true'));
    });

    function showCallback(geocodeResult, parsedGeocodeResult){
      $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
    }
    // Update zoom field
    var map = $("#addresspicker_map").addresspicker("map");
    google.maps.event.addListener(map, 'idle', function(){
      $('#zoom').val(map.getZoom());
    });

  });
  </script>
</head>
<body>
  
<div class="demo">
  <div class='clearfix'>
    <div class='input input-positioned'>
      <label>Address : </label> <input id="addresspicker_map" />     
     <input type="hidden" id="locality">
     <input type="hidden" id="sublocality" >
     <input type="hidden" id="administrative_area_level_3">
     <input type="hidden" id="administrative_area_level_2" >
     <input type="hidden" id="administrative_area_level_1">
     <input type="hidden" id="country">
     <input type="hidden" id="postal_code">
     <input type="hidden" id="lat">
     <input type="hidden" id="lng"> 
     <input type="hidden" id="zoom"> 
     <input type="hidden" id="type"/>

    </div>
</div>
<div>
 <label>Distance : </label> 
 <input type="text" name="distance" id="distance" placeholder="Please Distance in km"/>
 </div>

<div><button style="margin-top:20px;" class='btn btn-primary' id="storeDetail">Apply</button></div>
</div><!-- End demo -->
<script>
$(document).on('click', '#storeDetail', function() {
  var data = {
    'addresspicker_map':$('#addresspicker_map').val(),
    'distance':$('#distance').val(),
    'locality':$('#locality').val(),
    'sublocality':$('#sublocality').val(),
    'administrative_area_level_3':$('#administrative_area_level_3').val(),
    'administrative_area_level_2':$('#administrative_area_level_2').val(),    
    'administrative_area_level_1':$('#administrative_area_level_1').val(),
    'country':$('#country').val(),
    'postal_code':$('#postal_code').val(),
    'lat':$('#lat').val(),
    'lng':$('#lng').val(),
    'zoom':$('#zoom').val(),    
    'type':$('#type').val()
  }
  // Get the value of the name field.
    localStorage.clear();
    var addr = document.getElementById('addresspicker_map').value;
    var dist = document.getElementById('distance').value;

    //---Get value of namefield for edit
    var addr_edit = document.getElementById('addresspicker_map').value;
    var dist_edit = document.getElementById('distance').value;

    // Save the name in localStorage.
    localStorage.setItem('addr', addr);
    localStorage.setItem('dist', dist);

    //----- Save the name in localStorage for edit.
    localStorage.setItem('addr_edit', addr_edit);
    localStorage.setItem('dist_edit', dist_edit);


    // Save the name in localStorage.
    var addrs = localStorage.getItem('addr');
    var distance = localStorage.getItem('dist');
    window.opener.document.getElementById('addrs').innerHTML = 'Address: '+localStorage.getItem('addr');
    window.opener.document.getElementById('dists').innerHTML = 'Distance: '+localStorage.getItem('dist');
    window.opener.document.getElementById('locationInclude').innerHTML = '';
    window.opener.document.getElementById('locationExclude').innerHTML = '';

    // Save the name in localStorage for edit.

    var addrs_edit = localStorage.getItem('addr_edit');
    var distance_edit = localStorage.getItem('dist_edit');
    window.opener.document.getElementById('addrs_edit').innerHTML = 'Address: '+localStorage.getItem('addr_edit');
    window.opener.document.getElementById('dists_edit').innerHTML = 'Distance: '+localStorage.getItem('dist_edit');
    window.opener.document.getElementById('locationInclude_edit').innerHTML = '';
    window.opener.document.getElementById('locationExclude_edit').innerHTML = '';




  $.ajax({
        type: 'POST',
        url: "setMapSessions" ,  
        data: {'data':data},
         headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
        success: function (response) {
            if(response.message=='set in session'){
              window.close();
            }
        }
    });
});

 </script>
</body>
</html>
