@extends('layouts.appAdamPublisher')
@section('content')
<style type="text/css">
  
  .campTable {
    table-layout: fixed;
  }  
  .setEllipsis
  {
    white-space: nowrap; 
    overflow: hidden;
    text-overflow: ellipsis;
    cursor: pointer;
  }
  .campTable
  {
    user-select: none;
  }
  .cc_tableContainer
  {
    height: 620px;
  }
  .dataTables_wrapper
  {
    overflow: hidden;
    height: auto;
  }
</style>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <div class="row">
    <div class="col-lg-12">
      <div class="pull-left">
        <h2> AdamLineItem Daily Data</h2>
      </div>
      <div class="pull-right margin-top-20">
      </div>
    </div>        
  </div>

  <table class="table table-bordered" id="sample_editable_1" >
    <thead>                  
        <th style="width: 120px;">Date</th>
        <th>Impressions</th>
        <th>Clicks</th>
        <th>VideoViews</th>
        <th>Engagement</th>
        <th>Visits</th>
        <th>Leads</th>
        <th>Reach</th>
        <th>Installs</th>
        <th>SMS</th>
        <th>Mailer</th>
        <th>Spots</th>
        <th>NetRate</th>
        <th>NetCost</th>
        <th>Action</th>
    </thead>
    <tbody> 
      <?php $i=0; ?>
      @foreach( $getAdamLineItemDailyDetails as $eachAdamLineItemData )  
       <tr align="center">       
        <td class="setEllipsis" title=''>
          <input  type='text' class="form-control startDate" id="aLID_date_{{$i}}" required="true" name='aaLID_date' value="{{ date('d-m-Y',strtotime($eachAdamLineItemData->Date))}}" disabled="true"></input>

        </td>
        <td class="setEllipsis" title=''>
          <input class=" form-control  Impressions"  id="aLID_Impressions_{{$i}}" type='text' name='aLID_Impressions' value="{{$eachAdamLineItemData->Impressions}}" placeholder = "Impressions" data-value="" onkeypress="return isNumberKey(event)">
        </td>
        <td class="setEllipsis" title=''>
          <input class=" form-control  Clicks"  id="aLID_Clicks_{{$i}}" type='text' name='aLID_Clicks' value="{{$eachAdamLineItemData->Clicks}}" placeholder = "Clicks" data-value="" onkeypress="return isNumberKey(event)">
        </td> 
        <td class="setEllipsis" title=''>
          <input class=" form-control VideoViews"  id="aLID_VideoViews_{{$i}}" type='text' name='aLID_VideoViews' value="{{$eachAdamLineItemData->VideoViews}}" placeholder = "VideoViews" data-value="" onkeypress="return isNumberKey(event)">
        </td>
        <td class="setEllipsis" title=''>
          <input class=" form-control  Engagement"  id="aLID_Engagement_{{$i}}" type='text' name='aLID_Engagement' value="{{$eachAdamLineItemData->Engagement}}" placeholder = "Engagement" data-value="" onkeypress="return isNumberKey(event)">
        </td>
        <td class="setEllipsis" title=''>
          <input class=" form-control  Visits"  id="aLID_Visits_{{$i}}" type='text' name='aLID_Visits' value="{{$eachAdamLineItemData->Visits}}" placeholder = "Visits" data-value="" onkeypress="return isNumberKey(event)">
        </td>
        <td class="setEllipsis" title=''>
          <input class=" form-control  Leads"  id="aLID_Leads_{{$i}}" type='text' name='aLID_Leads' value="{{$eachAdamLineItemData->Leads}}" placeholder = "Leads" data-value="" onkeypress="return isNumberKey(event)">
        </td>
        <td class="setEllipsis" title=''>
          <input class=" form-control  Reach"  id="aLID_Reach_{{$i}}" type='text' name='aLID_Reach' value="{{$eachAdamLineItemData->Reach}}" placeholder = "Reach" data-value="" onkeypress="return isNumberKey(event)">
        </td>
        <td class="setEllipsis" title=''>
          <input class=" form-control  Installs"  id="aLID_Installs_{{$i}}" type='text' name='aLID_Installs' value="{{$eachAdamLineItemData->Installs}}" placeholder = "Installs" data-value="" onkeypress="return isNumberKey(event)">
        </td>
        <td class="setEllipsis" title=''>
          <input class=" form-control  SMS"  id="aLID_SMS_{{$i}}" type='text' name='aLID_SMS' value="{{$eachAdamLineItemData->SMS}}" placeholder = "SMS" data-value="" onkeypress="return isNumberKey(event)">
        </td> 
        <td class="setEllipsis" title=''>
          <input class=" form-control  Mailers"  id="aLID_Mailers_{{$i}}" type='text' name='aLID_Mailers' value="{{$eachAdamLineItemData->Mailer}}" placeholder = "Mailers" data-value="" onkeypress="return isNumberKey(event)">
        </td>
        <td class="setEllipsis" title=''>
          <input class=" form-control  Spots"  id="aLID_Spots_{{$i}}" type='text' name='aLID_Spots' value="{{$eachAdamLineItemData->Spots}}" placeholder = "Spots" data-value="" onkeypress="return isNumberKey(event)">
        </td>
        <td class="setEllipsis" title=''>
          <input class=" form-control  NetRate"  id="aLID_NetRate_{{$i}}" type='text' name='aLID_NetRate' value="{{$eachAdamLineItemData->NetRate}}" placeholder = "NetRate" data-value="" onkeypress="return isNumberKey(event)">
        </td>
        <td class="setEllipsis" title=''>
          <input class=" form-control  NetCost"  id="aLID_NetCost_{{$i}}" type='text' name='aLID_NetCost' value="{{$eachAdamLineItemData->NetCost}}" placeholder = "NetCost" data-value="" onkeypress="return isNumberKey(event)">
        </td>
        <input type='hidden'  name='AdamLineItem_Id' value="{{$eachAdamLineItemData->AdamLineItem_Id}}" id="AdamLineItem_Id_{{$i}}" data-AdamLineItem_Id="{{$eachAdamLineItemData->AdamLineItem_Id}}">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <input type="hidden" name="basePath" id="basePath" value="{{ url('/') }}">

        <td>
          <a class="btn btn-sm btn-success" title="AdamLineItem Daily" onclick="saveDailyAdamLineItems({{$i}})";>
            <i class="fa fa-check"> </i>
          </a>
        </td>    
      </tr>
      <?php $i++; ?>
      @endforeach  
    </tbody>
  </table>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>


<script src="{{ url('/js/adamLineItemDaily.js') }}" type="text/javascript"></script>
