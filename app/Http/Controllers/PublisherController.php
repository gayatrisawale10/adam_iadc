<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Publisher;
use App\DurationType;
use App\UserRole;
use App\Users;
use App\UserPublisher;
use App\PublisherType;
use validator;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PublisherController extends Controller
{
  public function index(Request $request)
  { 
    
    $Publisherdetails_index = DB::table('Publisher')
      ->join('Genre', 'Publisher.Genre_ID', '=', 'Genre.id')
      ->join('PublisherType', 'Publisher.PublisherType_ID', '=', 'PublisherType.id')
      ->where('Publisher.isActive','=', 1)
      ->select('Publisher.*', 'Genre.GenreName','PublisherType.PublisherTypeName')
      ->get();
     
    //dd($Publisherdetails_index);      
      return view('Publisher.index',compact('Publisherdetails_index'))
                ->with('i', ($request->input('page', 1) - 1) * 5);
  }

  public function create()
  {
    $PublisherName='';
    $getGenre = DB::table('Genre')->get();
    $selectGenreID=0;
    $PublisherType_ID=0; 
 
    $getPublisherType = DB::table('PublisherType')->get();

    return view('Publisher.create',compact('getGenre','selectGenreID', 'PublisherName', 
      'getPublisherType','PublisherType_ID'));

  }

  public function edit($p_id)
  {

    $getGenre = DB::table('Genre')->get();

    $getGenre_id=DB::table('Publisher')
    ->where('Publisher.id','=',$p_id)
    ->join('Genre', 'Publisher.Genre_ID', '=', 'Genre.id')
    ->join('PublisherType', 'Publisher.PublisherType_ID', '=', 'PublisherType.id')
    ->select('Publisher.id','Publisher.PublisherName','Publisher.Genre_ID','Publisher.PublisherType_ID')
    ->get();

    //dd($getGenre_id);

    $id = $getGenre_id[0]->id;

    $getGenreName=$getGenre_id[0]->Genre_ID;
    $selectGenreID=$getGenreName;

    $getPublisherName=$getGenre_id[0]->PublisherName;
    $PublisherName=$getPublisherName;

    $getPublisherType_ID=$getGenre_id[0]->PublisherType_ID;
    $PublisherType_ID=$getPublisherType_ID; ///------------------

    $getGenre_id=json_decode($getGenre_id);
    $getGenre_id = $getGenre_id[0];

      //dd($getGenre_id);
    $getPublisherType = DB::table('PublisherType')->get();
    $getPublisherType=json_decode($getPublisherType);
   

    return view('Publisher.edit',compact('getGenre','selectGenreID','getGenre_id','id','PublisherName','getPublisherType','PublisherType_ID'));

  }

  public function store(Request $request)
  {	   

    //dd($request->input('PublisherType_ID'));

    if($request->input('PublisherType_ID')==3)
    {
      $APIName=$request->input('PublisherName').'Excel';
      $this->saveApiSourceName($APIName); 
    } 
    
    $Publisherdetails=Publisher::create([
    'PublisherName' => $request->input('PublisherName'),
    'PublisherType_ID' => $request->input('PublisherType_ID'),
    'Genre_ID' => $request->input('Genre_ID'),
       
    'CreatedOn' => Carbon::now()->format('m-d-Y'),
    'ModifiedOn' => Carbon::now()->format('m-d-Y'),
    'ModifiedBy' => 1,
    'isActive' => True,
    ]);

    $Publisher_ID=$Publisherdetails->id;

    return redirect()->route('Publisher.index')
    ->with('success','Publisher created successfully');
    

  }

  public function saveApiSourceName($APIName)
  {

    $APINameExistCheck=DB::table('APISource')
              ->where('APIName','=',$APIName)
              ->get();
    $APINameExistCheck=json_decode($APINameExistCheck);

    if(!$APINameExistCheck){
      $AdamLineItem_URLMapping =DB::table('APISource')
          ->insert([
            'publisher_ID' => -1,
            'APIName' => $APIName,
            'DBMReportID'=>NULL,
            'IsActive' => true
          ]);  
    }      
  }
  public function update(Request $request, $p_id)
  {

    if($request->get('PublisherType_ID')==3)
    {
      $APIName=$request->input('PublisherName').'Excel';
      $this->saveApiSourceName($APIName); 
    } 
    //dd($request->all());
    $Publisher_update=Publisher::where([ ['id','=',$p_id],])
                              ->update(['PublisherName'=>$request->get('PublisherName'), 
                                        'Genre_ID' =>$request->get('Genre_ID'),
                                        'PublisherType_ID'=>$request->get('PublisherType_ID'),
                                      ]);

    return redirect()->route('Publisher.index')
                     ->with('success','Publisher updated successfully');
  }
		 
  public function show($work_id)
  {
     return view('Publisher.show',compact('workshow'));
  }

  public function destroy($p_id)
  {
    //Publisher::find($p_id)->delete();
    $gePublisher=Publisher::where([ ['id','=',$p_id],])->get();

    $gePublisher=json_decode($gePublisher);

    $PublisherName = $gePublisher[0]->PublisherName;
    $Genre_ID =$gePublisher[0]->Genre_ID;
    $PublisherType_ID = $gePublisher[0]->PublisherType_ID;  
    

    $Publisher_delete=Publisher::where([ ['id','=',$p_id],])
                              ->update(['PublisherName'=>$PublisherName, 
                                        'Genre_ID' =>$Genre_ID,
                                        'PublisherType_ID'=>$PublisherType_ID,
                                      
                                        'CreatedOn' => Carbon::now()->format('m-d-Y'),
                                        'ModifiedOn' => Carbon::now()->format('m-d-Y'),
                                        'ModifiedBy' => 'Admin',
                                        'isActive' => False,
                                      ]);

    //dd($gePublisher);                          
    return redirect()->route('Publisher.index')
    ->with('success','Publisher deleted successfully');
  }
   
  public function getCredentials($p_id)
  {   
    $getUserDetails=DB::table('UserPublisher')
                  ->join('users','users.id','=','UserPublisher.User_ID')
                  ->where('UserPublisher.Publisher_ID','=',$p_id)
                  ->get(); 
    $getUserDetails=json_decode($getUserDetails);
    //dd($getUserDetails);

    $getPublisherDetails=DB::table('Publisher')
                  ->where('id','=',$p_id)
                  ->get(); 
    $getPublisherDetails=json_decode($getPublisherDetails);
    //dd($getPublisherDetails[0]->PublisherName);

    $PublisherName=$getPublisherDetails[0]->PublisherName;


    if($getUserDetails)
    {
      $getUserName=$getUserDetails[0]->name;
      $UserName=$getUserName;
      $Password='**********'; 
    }
    else
    {
      $UserName=Null;
      $Password=Null;
    }
   // dd($UserName,$Password);
    return view('Publisher.Credentials',compact('p_id','UserName','Password','PublisherName'));
  } 

  public function saveCredentials( Request $request, $p_id)
  {   
    
    $arr=$request->all();
    //dd($arr);
    $getUserRole=UserRole::where('UserRoleName','=','Publisher')->get();
    $getUserRole=json_decode($getUserRole);
    $getUserRole_id=$getUserRole[0]->id;
    $UserRole_ID=$getUserRole_id;                           

    $checkCredentialexist=UserPublisher::where('Publisher_ID','=',$p_id)
              ->join('users','users.id','=','UserPublisher.User_ID')
              ->get();
    $checkCredentialexist=json_decode($checkCredentialexist);
    //dd($checkCredentialexist);
    
    if($checkCredentialexist)
    {
      $UserCredentials=users::where([ ['Publisher_ID','=',$p_id],])
        ->join('UserPublisher as up','up.User_ID','=','users.id')
        ->update(
          [ 'UserRole_ID' => $UserRole_ID,
            'name' => $arr['UserName'],
            'password' => bcrypt($arr['Password']),
            'email'=> $arr['UserName'],
            'remember_token'=>$arr['_token'],

            'CreatedOn' => Carbon::now()->format('m-d-Y'),
            'ModifiedOn' => Carbon::now()->format('m-d-Y'),
            'ModifiedBy' => 1,
            'isActive' => True,
          ]);     
    }
    else
    {
      $UserCredentials=Users::create([
        'UserRole_ID' => $UserRole_ID,
        'name' => $arr['UserName'],
        'password' => bcrypt($arr['Password']),
        'email'=> $arr['UserName'],
        'remember_token'=>$arr['_token'],

        'CreatedOn' => Carbon::now()->format('m-d-Y'),
        'ModifiedOn' => Carbon::now()->format('m-d-Y'),
        'ModifiedBy' => 1,
        'isActive' => True,    
      ]);
  
      $User_ID=$UserCredentials->id;

      $UserPublisherDetails=UserPublisher::insert([
        'User_ID' => $User_ID,
        'Publisher_ID' => $p_id,
        
        'CreatedOn' => Carbon::now()->format('m-d-Y'),
        'ModifiedOn' => Carbon::now()->format('m-d-Y'),
        'ModifiedBy' => 1,
        'isActive' => True,
     
      ]);
    }  

    return redirect()->route('Publisher.index')
    ->with('success','Log-In Credential Updated successfully');
  } 

}
