<?php
namespace App\dbm;
use Google_Client;
use Google_Service_DoubleClickBidManager;
use Google_Service_Analytics;
use Google_Service_AnalyticsReporting;
use Google_Service_AnalyticsReporting_DateRange;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_Dimension;
use Google_Service_AnalyticsReporting_ReportRequest;
use Google_Service_AnalyticsReporting_GetReportsRequest;

$upTwo = dirname(__DIR__, 2);
//echo "<pre>"; print_r($upTwo); exit;
require_once $upTwo. '/vendor/autoload.php';
require_once('getViewId_ForGA.php');
//echo "<pre>"; print_r($viewId); exit;
Class GoogleAnalyticsReportProcessing
{
  Public function indexGA()
  {
    //echo "in GA";
    Global $viewId;
    Global $gaReport;
    session_start();

    $client = new Google_Client();
    $client->setAuthConfig(__DIR__ . '/client_secrets.json');
    $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);

    if (isset($_SESSION['access_token']) && $_SESSION['access_token']) 
    {
      echo "<h1>Processing Google Analytics Result.....</h1>"; //exit;
      $client->setAccessToken($_SESSION['access_token']);
      $analytics = new Google_Service_AnalyticsReporting($client);

      foreach ($viewId as $key => $viewIdValue) {
        $response = $this->getReport($analytics, $viewIdValue);
        
        //echo "<br>====".$viewIdValue."============================================<br>";
        
        $gaReport=$this->printResultsReport($response, $viewIdValue );
        //echo"<pre>"; print_r($gaReport); //exit;
        //=================creating report as jsonFile==================
          $serializedData = json_encode($gaReport);
          // save serialized data in a text file
          file_put_contents('GAReport_json_encode.json', $serializedData);
          // at a later point, you can convert it back to array like:
          $recoveredData = file_get_contents('GAReport_json_encode.json');
          // unserializing to get actual array
          //$recoveredArray = unserialize($recoveredData);
        //=================end creating report as jsonFile==================
      }  
    } 
    else {
      //echo"in else  "; exit;

      $url = dirname(__FILE__);
      $array = explode('\\',$url);
      $count = count($array);

      //$redirect_uri = 'http://localhost/ga1/App/ga_cred/oauth2callback.php';
      $redirect_uri='http://' . $_SERVER['HTTP_HOST'] . '/'.$array[$count-3] . '/'.$array[$count-2] . '/'.$array[$count-1] .'/oauth2callback.php';

      //echo $redirect_uri; exit;
      $hUrl=header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));

      echo"<pre>inElse GA hUrl "; print_r($hUrl); exit;
    }

    //echo"<pre>"; print_r($gaReport); 
  }  
  // ======================================================================
  Public function getReport($analytics, $viewIdValue) {

    $VIEW_ID = $viewIdValue;
    $dateRange = new Google_Service_AnalyticsReporting_DateRange();
    //$dateRange->setStartDate("7daysAgo");
    $dateRange->setStartDate("3daysAgo");//
    $dateRange->setEndDate("today");

    // Create the Metrics object.
    $sessions = new Google_Service_AnalyticsReporting_Metric();
    $sessions->setExpression("ga:sessions");
    $sessions->setAlias("sessions");

    $avgSessionDuration = new Google_Service_AnalyticsReporting_Metric();
    $avgSessionDuration->setExpression("ga:avgSessionDuration");
    $avgSessionDuration->setAlias("avgSessionDuration");

    $bounceRate = new Google_Service_AnalyticsReporting_Metric();
    $bounceRate->setExpression("ga:bounceRate");
    $bounceRate->setAlias("bounceRate");

    $users = new Google_Service_AnalyticsReporting_Metric();
    $users->setExpression("ga:users");
    $users->setAlias("users");

    $pageviews = new Google_Service_AnalyticsReporting_Metric();
    $pageviews->setExpression("ga:pageviews");
    $pageviews->setAlias("pageviews");

    $adClicks = new Google_Service_AnalyticsReporting_Metric();
    $adClicks->setExpression("ga:adClicks");
    $adClicks->setAlias("adClicks");

    $adCost = new Google_Service_AnalyticsReporting_Metric();
    $adCost->setExpression("ga:adCost");
    $adCost->setAlias("adCost");

    $impressions = new Google_Service_AnalyticsReporting_Metric();
    $impressions->setExpression("ga:impressions");
    $impressions->setAlias("impressions");

    $timeOnPage = new Google_Service_AnalyticsReporting_Metric();
    $timeOnPage->setExpression("ga:timeOnPage");
    $timeOnPage->setAlias("timeOnPage");

    $goalCompletionsAll = new Google_Service_AnalyticsReporting_Metric();
    $goalCompletionsAll->setExpression("ga:goalCompletionsAll");
    $goalCompletionsAll->setAlias("goalCompletionsAll");

    //Add dimensions. Only medium can be considered as it's mapped to the line item
    $date = new Google_Service_AnalyticsReporting_Dimension();
    $date->setName("ga:date");

    $medium = new Google_Service_AnalyticsReporting_Dimension();
    $medium->setName("ga:medium");

    // Create the ReportRequest object.
    $request = new Google_Service_AnalyticsReporting_ReportRequest();
    $request->setViewId($VIEW_ID);
    $request->setDateRanges($dateRange);
    $request->setMetrics(array($sessions, $avgSessionDuration, $bounceRate, $users, $pageviews, $adClicks, $adCost, $impressions, $timeOnPage, $goalCompletionsAll));
    //  $request->setMetrics(array($pageviews));
    $request->setDimensions(array($date, $medium));
    //  $request->setMetrics(array($sessions, $avgSessionDuration, $bounceRate, $users));

    $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
    $body->setReportRequests( array( $request) );
    return $analytics->reports->batchGet( $body );
  }

  Public function printResultsReport($reports, $viewIdValue) {

    for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
      
      //echo"<pre>count "; print_r( count($reports) );// exit;
      $report = $reports[ $reportIndex ];
      $header = $report->getColumnHeader();
      $dimensionHeaders = $header->getDimensions();
      $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
      $rows = $report->getData()->getRows();
       //echo"<pre> rowCount for each : "; print_r(count($rows)); //exit;
      $reportArray=[]; 
      $dimentionArr=[];
      $metricsArr=[];
      
      for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
        //echo"<pre> rowCount "; print_r(count($rows)); //exit;
        $row = $rows[ $rowIndex ];
        $dimensions = $row->getDimensions();
        $metrics = $row->getMetrics();
        
        $dimention=[];
        $metricess=[];

        for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
          // echo "<br>---------------------------";
          // print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
          $dimention[$dimensionHeaders[$i]]=$dimensions[$i];
        }
        array_push($dimentionArr, $dimention);

        for ($j = 0; $j < count($metrics); $j++) {
          $values = $metrics[$j]->getValues();
          for ($k = 0; $k < count($values); $k++) {
            $entry = $metricHeaders[$k];
            // echo "<br>.......................";
            // print($entry->getName() . ": " . $values[$k] . "\n");
            $metricess[$entry->getName()]=$values[$k];        
          }
          array_push($metricsArr, $metricess);
        
        }
        foreach($dimentionArr as $key=>$val){
            $val2 = $metricsArr[$key]; 
            $reportArray[$rowIndex] = $val + $val2; // combine 'em
        }
        //echo"<pre> return reportArray "; print_r($reportArray); //exit;

      }
      //echo"<pre> return reportArray forOut"; print_r($reportArray); //exit;
      return $reportArray;
    }
  }
}

$gaObj= new GoogleAnalyticsReportProcessing;
$exeGa=$gaObj->indexGA();