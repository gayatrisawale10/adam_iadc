<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Client;
use App\ClientBrand;
use App\DurationType;
use validator;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ClientBrandController extends Controller
{
  
  public function getAllBrandsToDisplay($c_id)
  {
    $brandDetails_index = DB::table('ClientBrand')
       ->where('ClientBrand.Client_ID','=', $c_id)
      ->select('ClientBrand.*', 'ClientBrand.BrandName')
      ->get();       
    $getClientDetails= DB:: table('Client')
            ->where('id','=', $c_id)
            ->get();

    $getClientName=$getClientDetails[0]->ClientName;
    $ClientName=$getClientName;         

    return view('ClientBrand.getAllBrandsToDisplay',compact('brandDetails_index', 'c_id', 'ClientName'))->with('i');
  }  

  public function createBrand($c_id)
  {
    $BrandName='';
    $brandDetails_index=0; 
     
    $getClientDetails= DB:: table('Client')
            ->where('id','=', $c_id)
            ->get();

    $getClientDetails=json_decode($getClientDetails);
    $getClientDetails = $getClientDetails[0];        

    return view('ClientBrand.create',compact('BrandName','getClientDetails', 'brandDetails_index','c_id'));
  }

  public function storeBrand(Request $request)
  {     
    $c_id=$request->input('Client_ID');
    $ClientBranddetails=ClientBrand::create([    
      'BrandName' => $request->input('BrandName'),
      'Client_ID' => $request->input('Client_ID'),
      
      'CreatedOn' => Carbon::now()->format('m-d-Y'),
      'ModifiedOn' => Carbon::now()->format('m-d-Y'),
      'ModifiedBy' => 'Admin',
      'isActive' => True,
    ]);
    return redirect()->action('ClientBrandController@getAllBrandsToDisplay', ['c_id' => $c_id])
                     ->with('success','Brand created successfully');
  }

  public function editBrand($b_id, $c_id)
  {
    $brandDetails_index = DB::table('ClientBrand')
       ->where('ClientBrand.id','=', $b_id)
      ->select('ClientBrand.*', 'ClientBrand.BrandName')
      ->get();
        
    $getClientDetails= DB:: table('Client')
            ->where('id','=', $c_id)
            ->get();

    $getClientDetails=json_decode($getClientDetails);
    $getClientDetails = $getClientDetails[0];        

    $getBrandName=$brandDetails_index[0]->BrandName;
    $BrandName=$getBrandName; 

    $brandDetails_index=json_decode($brandDetails_index);
    $brandDetails_index = $brandDetails_index[0];

    return view('ClientBrand.edit',compact('BrandName','getClientDetails', 'brandDetails_index','c_id','b_id'));
  }

  public function updateBrand(Request $request, $b_id, $c_id)
  {
    $Brand_update=ClientBrand::where([ ['id','=',$b_id],])
                              ->update(['BrandName'=>$request->get('BrandName'), 
                                        'Client_ID' =>$request->get('Client_ID'),
                                      ]);

    return redirect()->action('ClientBrandController@getAllBrandsToDisplay', ['c_id' => $c_id])
                     ->with('success','Brand Updated successfully');
  }  
  public function destroyBrand($b_id, $c_id)
  {
    ClientBrand::find($b_id)->delete();
    
    return redirect()->action('ClientBrandController@getAllBrandsToDisplay', ['c_id' => $c_id])
                     ->with('success','Brand deleted successfully');
  }
     
}
